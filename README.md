# Project de Bachelor

## Overview

This Flask application allows you to upload Scala assignments, automatically clusters them using an unsupervised learning model, and then compare the clustered files side by side.

## Installation

1. Install dependencies:
    ```bash
    pip install Flask scikit-learn
    ```

2. Run the application:
    ```bash
    python run.py
    ```

3. Access the app in your browser at `http://127.0.0.1:5000/`.

## Features

- **Upload Page**: Upload multiple Scala files.
- **Clustering**: Automatically cluster uploaded files into groups.
- **Comparison**: Compare two Scala files side by side, with navigation to switch files.

## Future Improvements

- Highlight differences between compared files.
- Support for more file types.
