import re

def mark_infinite_while_loops(scala_code):
    infinite_loop_marker = "🔄"
    
    while_pattern = re.compile(r'^\s*while\s*\(([^)]+)\)\s*{')
    
    code_lines = scala_code.splitlines()
    
    loops = set()
    
    for i, line in enumerate(code_lines):
        stripped_line = line.strip()
        
        while_match = while_pattern.match(stripped_line)
        if while_match:
            condition = while_match.group(1).strip()
            if 'true' in condition.lower() or not condition:
                loops.add(i)
    
    modified_lines = []
    for i, line in enumerate(code_lines):
        if i in loops:
            modified_lines.append(f"{infinite_loop_marker} {line}")
        else:
            modified_lines.append(line)
    
    return "\n".join(modified_lines)
