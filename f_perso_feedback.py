import pymysql
db_config = {
        'host': 'localhost',
        'user': 'root',
        'db': 'projet_b'
}


def get_feedback(name):
   
    conn = pymysql.connect(**db_config)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    
    try:
    
        cursor.execute("""
            SELECT Num_Lines, Num_Ifs, Num_Methods, Num_Loops, Num_Var, unused_variables, unused_methods, match_if_else, missing_brackets, Content
            FROM assignement
        """)
        all_assignments = cursor.fetchall()

     
        cursor.execute("""
            SELECT Num_Lines, Num_Ifs, Num_Methods, Num_Loops, Num_Var, unused_variables, unused_methods, match_if_else, missing_brackets, Content
            FROM assignement WHERE Name = %s
        """, (name,))
        current_assignment = cursor.fetchone()

        if not current_assignment:
            print("Assignment not found in the database.")
            return

       
        num_lines_current = current_assignment['Num_Lines']
        num_ifs_current = current_assignment['Num_Ifs']
        num_methods_current = current_assignment['Num_Methods']
        num_loops_current = current_assignment['Num_Loops']
        num_var_current = current_assignment['Num_Var']
        code_content = current_assignment['Content']

        # Find the top 90% thresholds for each metric
        thresholds = {
            'Num_Lines': sorted([a['Num_Lines'] for a in all_assignments])[-int(0.1 * len(all_assignments))],
            'Num_Ifs': sorted([a['Num_Ifs'] for a in all_assignments])[-int(0.1 * len(all_assignments))],
            'Num_Methods': sorted([a['Num_Methods'] for a in all_assignments])[-int(0.75 * len(all_assignments))],
            'Num_Loops': sorted([a['Num_Loops'] for a in all_assignments])[-int(0.25 * len(all_assignments))],
            'Num_Var': sorted([a['Num_Var'] for a in all_assignments])[-int(0.25 * len(all_assignments))]
        }

        feedback = ['']

        if num_ifs_current > thresholds['Num_Ifs']:
            feedback.append(f"If/Else vs Match: You have more if/else statements than most assignments. Sometimes `match` can be helpful. Check these links for more information: {get_resource_links('If/Else vs Match')} \n")

        if num_methods_current > thresholds['Num_Methods']:
            feedback.append("Well done! You have divided your code into functions. Not all students achieve that.")
        else:
            feedback.append(f"Number of methods low compared to other assignments: Consider creating functions to simplify your code. Functions have a single purpose and can make the code simpler. Check these links for more information: {get_resource_links('How to use functions')}\n")

        if num_var_current > thresholds['Num_Var']:
            feedback.append(f"Number of Variables: You have more `var` and `val` than most assignments. When you declare with 'var', the data can be reassigned. For more information: {get_resource_links('Val vs Var')}\n")
        
        if num_loops_current > thresholds['Num_Loops']:
            feedback.append(f"Use of Loops: Your code contains more loops than most assignments. Consider using higher-order functions like `map`, `filter`, and `reduce` for a more functional approach. Check these links for more information: {get_resource_links('Loops vs Higher-order Functions')}\n")

  
        
        if not has_sufficient_comments(code_content, num_lines_current):
            feedback.append(f"Commenting: Your code could benefit from more comments. Aim to include at least one comment every 15 lines to improve readability. Here's why : {get_resource_links('Why comment the code')}\n")
        
   
        cursor.execute("UPDATE assignement SET Feedback = %s WHERE Name = %s", ('\n'.join(feedback), name))
        conn.commit()
    
    finally:
        cursor.close()
        conn.close()
    
    return feedback

def has_sufficient_comments(code_content, num_lines):
    """
    Check if there is at least one comment line for every 15 lines of code.
    """
    comment_lines = [line for line in code_content.splitlines() if line.strip().startswith("//")]
    return len(comment_lines) >= (num_lines / 15)

def get_resource_links(error_type):
  
    conn = pymysql.connect(**db_config)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    links = []

    try:
        cursor.execute("SELECT Resource_Link FROM resources WHERE Error_Type = %s", (error_type,))
        resources = cursor.fetchall()
        links = [resource['Resource_Link'] for resource in resources]
    
    finally:
        cursor.close()
        conn.close()
    
    return ', '.join(links)

