def check_missing_brackets(content):
    lines = content.split('\n')
    bracket_stack = []
    missing_bracket_lines = set()
    
    matching_bracket = {'}': '{', ']': '['}
    opening_brackets = "{["
    closing_brackets = "}]"
    
    line_stack = []

    for line_number, line in enumerate(lines, start=1):
        for char in line:
            if char in opening_brackets:
                bracket_stack.append(char)
                line_stack.append(line_number)
            elif char in closing_brackets:
                if bracket_stack and bracket_stack[-1] == matching_bracket[char]:
                    bracket_stack.pop()
                    line_stack.pop()
                else:
                    missing_bracket_lines.add(line_number)
    
    for _ in bracket_stack:
        missing_bracket_lines.add(line_stack.pop())
    
    return sorted(missing_bracket_lines)
