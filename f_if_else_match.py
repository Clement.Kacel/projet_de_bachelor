import re

def check_if_replacement(content):
    """
    This function checks if there are `if` statements in the Scala code
    that can be replaced by `match` cases.
    """

    lines = content.split('\n')
    
  
    replaceable_ifs = []

   
    if_pattern = re.compile(r'\bif\s*\(\s*(\w+)\s*==\s*\d+\s*\)')

  
    if_statements = {}

    for line_number, line in enumerate(lines, start=1):
        match = if_pattern.search(line)
        if match:
            var_name = match.group(1)  
            if var_name not in if_statements:
                if_statements[var_name] = []
            if_statements[var_name].append(line_number)

    for var_name, line_numbers in if_statements.items():
        if len(line_numbers) > 1:
            replaceable_ifs.extend(line_numbers)

    return replaceable_ifs
