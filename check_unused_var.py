import re, os

def check_var(content):
    lines = content.split('\n')
    
    declarations = []
    
    for line_number, line in enumerate(lines, start=1):
        matches = re.findall(r'\b(val|var)\s+(\w+)', line)
        for match in matches:
            declarations.append((line_number, match[1])) 
    
    unused_vars = []
    for declaration in declarations:
        line_number, var_name = declaration
        
        pattern = r'\b' + re.escape(var_name) + r'\b'
        if not any(re.search(pattern, lines[i]) for i in range(line_number, len(lines))):
            unused_vars.append(line_number)
    
    return unused_vars



