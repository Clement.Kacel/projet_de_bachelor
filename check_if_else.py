import re

def suggest_match_usage(scala_code):
    marker = "// Consider using match instead of if/else"
    
    last_condition_var = None
    
    modified_lines = []
    
    pattern_if = re.compile(r'^\s*(if|else\s+if)\s*\(([^)]+)\)\s*{')
    
    code_lines = scala_code.splitlines()
    
    for line in code_lines:
        stripped_line = line.strip()

        match_if = pattern_if.match(stripped_line)
        if match_if:
            condition = match_if.group(2).strip()

            current_condition_var = re.split(r'\s*[!=<>]=?\s*', condition)[0]

            if current_condition_var == last_condition_var:
                modified_lines.append(marker)

            modified_lines.append(line)
            last_condition_var = current_condition_var
        elif stripped_line.startswith('else'):
            if last_condition_var is not None:
                modified_lines.append(marker)
            modified_lines.append(line)
            last_condition_var = None
        else:
            last_condition_var = None
            modified_lines.append(line)
    
    return "\n".join(modified_lines)
