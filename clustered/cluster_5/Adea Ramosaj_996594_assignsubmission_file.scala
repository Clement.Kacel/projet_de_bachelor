//Assignment: Adea Ramosaj_996594_assignsubmission_file

import scala.io.StdIn.{readInt, readLine}



object Main {





//METHODE CODE PIN

  def changepin(id: Int, codespin : Array[String]): Unit = {



    println ("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")

    codespin(id) = readLine()



    while (codespin(id).length < 8) {

      println("Votre code pin ne contient pas au moins 8 caractères")

      println("Saisissez votre nouveau code pin>")

      codespin(id) = readLine()



    }

  }



//METHODE DEPOT

  def depot(id : Int, comptes : Array[Double]): Unit = {



    var deviseDepot = 3 // variable qui permet de savoir quelle devise le client veut utiliser



    while (deviseDepot != 2 && deviseDepot != 1) {

      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")

      deviseDepot = readLine().toInt



      if (deviseDepot != 1 && deviseDepot != 2) {

      }

    }

    var p = 0

    var d = 0

    var m = 0

    var test = 0



    var tentativesessaie = 0

    var is_correct = false

    var montantDepot = 0;



    while (is_correct == false) {

      println("Indiquez le montant du dépôt >")

      montantDepot = readLine().toInt



      if (montantDepot % 10 == 0 && montantDepot > 0) {

        is_correct = !false



      } else {

        println("Le montant doit être un multiple de 10")

      }

    }





    if (deviseDepot == 1) { // si le client veut déposer en CHF

      comptes(id) += montantDepot



    } else { // si le client veut déposer en EUR

      comptes(id) += montantDepot * 0.95

    }

    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id))



  }



//METHODE RETRAIT

  def retrait (id: Int, comptes: Array[Double]): Unit = {



    var selectionBillet_INT500 = 0

    var selectionBillet_INT200 = 0

    var selectionBillet_INT100 = 0

    var selectionBillet_INT50 = 0

    var selectionBillet_INT20 = 0

    var selectionBillet_INT10 = 0

    var deviseRetrait = 3 // variable qui permet de savoir quelle devise le client veut utiliser



    while (deviseRetrait != 1 && deviseRetrait != 2) {

      println("Indiquez la devise :1 CHF, 2: EUR")

      deviseRetrait = readLine().toInt



      if (deviseRetrait != 1 && deviseRetrait != 2) {

        println("opération invalide")

      }

    }



    var is_correct = false

    var montant = 0; //montant du retrait

    val dixPourcent = comptes(id) * 0.1



    while (!is_correct) {

      println("Indiquez le montant du retrait >")

      montant = readInt()



      if (montant % 10 == 0) {



        if (montant <= dixPourcent && montant > 0) {

          is_correct = true



        } else {

          println("Votre plafond de retrait autorisé est de :" + dixPourcent)

        }



      } else {

        println("Le montant doit être un multiple de 10")

      }

    }

    var p = 0

    var d = 0

    var m = 0

    var test = 0

    var coupure = 3 // variable qui permet de savoir quelle coupure le client veut utiliser



    if (deviseRetrait == 1) { // Retrait en CHF



      if (montant >= 200) { // si le client veut retirer plus de 200 CHF



        while (coupure != 1 && coupure != 2) { // tant que le client n'a pas choisi une coupure valide

          println("En, 1) Grosses coupures 2) Petites coupures >")

          coupure = readInt()



          if (coupure != 1 && coupure != 2) {

            println("choix invalide")

          }

        }

      }

    }

    println("Veuillez retirer la somme demandée :")

    var montant_restant = montant



    // CHF

    if (deviseRetrait == 1) {



      if (coupure == 1) { // grosse coupure



        while (montant_restant > 0) { //Tant qu'il reste de l'argent à distribuer

          //on affiche combien il reste



          if (montant_restant / 500 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 500)

            println(" billet(s) de 500 CHF")

            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine() //lecture de l'entrée pour le nombre de billets voulu



              if (selectionbillet != "o") { //si l'entrée n'est pas o

                selectionBillet_INT500 = selectionbillet.toInt //alors on la converti en entier



                if (selectionBillet_INT500 < 0 || selectionBillet_INT500 > (montant_restant / 500)) { //cas d'une mauvaise saisie

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT500 * 500) //on soustrait la somme

                  selectionbillet = "o" //pour sortir du do while on met seletionbillet à o

                }

              }



              else { //sinon si l'utilisateur accepte on stocke le nombre de billets maximum proposée et on soustrait la somme

                selectionBillet_INT500 = montant_restant / 500

                montant_restant -= ((montant_restant / 500) * 500)

              }



            } while (selectionbillet != "o")

          }

          //pareil mais pour 200

          if (montant_restant / 200 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 200)

            println(" billet(s) de 200 CHF")

            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT200 = selectionbillet.toInt



                if (selectionBillet_INT200 < 0 || selectionBillet_INT200 > (montant_restant / 200)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT200 * 200)

                  selectionbillet = "o"

                }

              } else {

                selectionBillet_INT200 = montant_restant / 200

                montant_restant -= ((montant_restant / 200) * 200)

              }



            } while (selectionbillet != "o")

          }

          //pour 100

          if (montant_restant / 100 >= 1) {



            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 100)

            println(" billet(s) de 100 CHF")

            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT100 = selectionbillet.toInt



                if (selectionBillet_INT100 < 0 || selectionBillet_INT100 > (montant_restant / 100)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT100 * 100)

                  selectionbillet = "o"

                }

              }

              else {

                selectionBillet_INT100 = montant_restant / 100

                montant_restant -= ((montant_restant / 100) * 100)

              }



            } while (selectionbillet != "o")

          }

          //pour 50

          if (montant_restant / 50 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 50)

            println(" billet(s) de 50 CHF")

            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT50 = selectionbillet.toInt



                if (selectionBillet_INT50 < 0 || selectionBillet_INT50 > (montant_restant / 50)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT50 * 50)

                  selectionbillet = "o"

                }

              } else {

                selectionBillet_INT50 = montant_restant / 50

                montant_restant -= ((montant_restant / 50) * 50)

              }



            } while (selectionbillet != "o")

          }

          //pour 20

          if (montant_restant / 20 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 20)

            println(" billet(s) de 20 CHF")

            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT20 = selectionbillet.toInt



                if (selectionBillet_INT20 < 0 || selectionBillet_INT20 > (montant_restant / 20)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT20 * 20)

                  selectionbillet = "o"

                }

              }

              else {

                selectionBillet_INT20 = montant_restant / 20

                montant_restant -= ((montant_restant / 20) * 20)

              }



            } while (selectionbillet != "o")

          }



          if (montant_restant / 10 >= 1) {

            //pour 10, on ne demande pas le nombre de billets car c'est demandé dans l'énonce

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 10)

            selectionBillet_INT10 = (montant_restant / 10)

            println(" billet(s) de 10 CHF")

            montant_restant -= ((montant_restant / 10) * 10)

          }

        }

      } else {

        // petite coupure



        while (montant_restant > 0) { //TANT QUE LE MONTANT RESTANT EST SUPERIEUR



          //pour 100

          if (montant_restant / 100 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 100)

            println(" billet(s) de 100 CHF")



            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT100 = selectionbillet.toInt



                if (selectionBillet_INT100 < 0 || selectionBillet_INT100 > (montant_restant / 100)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT100 * 100)

                  selectionbillet = "o"

                }

              } else {

                selectionBillet_INT100 = montant_restant / 100

                montant_restant -= ((montant_restant / 100) * 100)

              }

            } while (selectionbillet != "o")

          }

          //pour 50

          if (montant_restant / 50 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 50)

            println(" billet(s) de 50 CHF")



            var selectionbillet = ""

            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT50 = selectionbillet.toInt



                if (selectionBillet_INT50 < 0 || selectionBillet_INT50 > (montant_restant / 50)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT50 * 50)

                  selectionbillet = "o"

                }



              } else {

                selectionBillet_INT50 = montant_restant / 50

                montant_restant -= ((montant_restant / 50) * 50)

              }



            } while (selectionbillet != "o")



          }

          //pour 20

          if (montant_restant / 20 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 20)

            println(" billet(s) de 20 CHF")

            var selectionbillet = ""



            do {

              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              selectionbillet = readLine()



              if (selectionbillet != "o") {

                selectionBillet_INT20 = selectionbillet.toInt



                if (selectionBillet_INT20 < 0 || selectionBillet_INT20 > (montant_restant / 20)) {

                  println("Veuillez saisir une valeur valide.")



                } else {

                  montant_restant -= (selectionBillet_INT20 * 20)

                  selectionbillet = "o"

                }

              } else {



                selectionBillet_INT20 = montant_restant / 20

                montant_restant -= ((montant_restant / 20) * 20)

              }



            } while (selectionbillet != "o")

          }

          //pour 10

          if (montant_restant / 10 >= 1) {

            println(s"Il reste $montant_restant CHF à distribuer")

            print("Vous pouvez obtenir au maximum ")

            print(montant_restant / 10)

            selectionBillet_INT10 = (montant_restant / 10)

            println(" billet(s) de 10 CHF")

            montant_restant -= ((montant_restant / 10) * 10)

          }

        }

      }

      //affichage des billets

      //on affiche seulement si le nombre de billets est positif

      println("Veuillez retirer la somme demandée : ")



      if (selectionBillet_INT500 > 0) {

        print(selectionBillet_INT500)

        println(" billet(s) de 500 CHF")

      }



      if (selectionBillet_INT200 > 0) {

        print(selectionBillet_INT200)

        println(" billet(s) de 200 CHF")



      }



      if (selectionBillet_INT100 > 0) {

        print(selectionBillet_INT100)

        println(" billet(s) de 100 CHF")

      }



      if (selectionBillet_INT50 > 0) {

        print(selectionBillet_INT50)

        println(" billet(s) de 50 CHF")

      }



      if (selectionBillet_INT20 > 0) {

        print(selectionBillet_INT20)

        println(" billet(s) de 20 CHF")

      }



      if (selectionBillet_INT10 > 0) {

        print(selectionBillet_INT10)

        println(" billet(s) de 10 CHF")

      }



    } else { // EUR



      while (montant_restant > 0) { //SI MONTANT RESTANT SUPERIEUR A 0



        if (montant_restant / 100 >= 1) {

          println(s"Il reste $montant_restant CHF à distribuer")

          print("Vous pouvez obtenir au maximum ")

          print(montant_restant / 100)

          println(" billet(s) de 100 EUR")



          var selectionbillet = ""

          do { //UTILISONS DO

            println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

            selectionbillet = readLine()



            if (selectionbillet != "o") {

              selectionBillet_INT100 = selectionbillet.toInt



              if (selectionBillet_INT100 < 0 || selectionBillet_INT100 > (montant_restant / 100)) {

                println("Veuillez saisir une valeur valide.")



              } else {

                montant_restant -= (selectionBillet_INT100 * 100)

                selectionbillet = "o"

              }

            } else {

              selectionBillet_INT100 = montant_restant / 100

              montant_restant -= ((montant_restant / 100) * 100)

            }



          } while (selectionbillet != "o")

        }



        if (montant_restant / 50 >= 1) {

          println(s"Il reste $montant_restant CHF à distribuer")

          print("Vous pouvez obtenir au maximum ")

          print(montant_restant / 50)

          println(" billet(s) de 50 EUR")

          var selectionbillet = ""



          do {

            println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

            selectionbillet = readLine()



            if (selectionbillet != "o") {

              selectionBillet_INT50 = selectionbillet.toInt



              if (selectionBillet_INT50 < 0 || selectionBillet_INT50 > (montant_restant / 50)) {

                println("Veuillez saisir une valeur valide.")



              } else {

                montant_restant -= (selectionBillet_INT50 * 50)

                selectionbillet = "o"

              }

            } else {

              selectionBillet_INT50 = montant_restant / 50

              montant_restant -= ((montant_restant / 50) * 50)

            }

          } while (selectionbillet != "o")

        }



        if (montant_restant / 20 >= 1) {

          println(s"Il reste $montant_restant CHF à distribuer")

          print("Vous pouvez obtenir au maximum ")

          print(montant_restant / 20)

          println(" billet(s) de 20 EUR")



          var selectionbillet = ""

          do {

            println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

            selectionbillet = readLine()



            if (selectionbillet != "o") {

              selectionBillet_INT20 = selectionbillet.toInt



              if (selectionBillet_INT20 < 0 || selectionBillet_INT20 > (montant_restant / 20)) {

                println("Veuillez saisir une valeur valide.")



              } else {

                montant_restant -= (selectionBillet_INT20 * 20)

                selectionbillet = "o"

              }

            } else {

              selectionBillet_INT20 = montant_restant / 20

              montant_restant -= ((montant_restant / 20) * 20)

            }



          } while (selectionbillet != "o")

        }



        if (montant_restant / 10 >= 1) {

          println(s"Il reste $montant_restant CHF à distribuer")

          print("Vous pouvez obtenir au maximum ")

          print(montant_restant / 10)

          selectionBillet_INT10 = (montant_restant / 10)

          println(" billet(s) de 10 EUR")

          montant_restant -= ((montant_restant / 10) * 10)

        }

      }

    }



    if (deviseRetrait == 1) { //si en CHF

      comptes(id) -= montant



    } else { //si en EUR

      println("Veuillez retirer la somme demandée : ")

      //affichage des billets

      //on affiche seulement si le nombre de billets est positif



      if (selectionBillet_INT100 > 0) {

        print(selectionBillet_INT100)

        println(" billet(s) de 100 EUR")

      }

      if (selectionBillet_INT50 > 0) {

        print(selectionBillet_INT50)

        println(" billet(s) de 50 EUR")

      }

      if (selectionBillet_INT20 > 0) {

        print(selectionBillet_INT20)

        println(" billet(s) de 20 EUR")

      }

      if (selectionBillet_INT10 > 0) {

        print(selectionBillet_INT10)

        println(" billet(s) de 10 EUR")

      }

      comptes(id) -= montant * 0.95

    }

    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est : %.2f CHF\n", comptes(id))



  }



  def main(args: Array[String]): Unit = {

    var nbclients = 100 // taille des array

    val comptes = Array.fill(nbclients)(1200.0)

    val codespin = Array.fill(nbclients)("INTRO1234")



    var PIN_correct = false

    var PINdemande = false

    var choix = 0

    var tentativePin = 3



    println("Saisissez votre code identifiant ")

    var id = readInt()



    while (id > nbclients) {

      println("Cet identifiant n'est pas valable. ")

      return

    }



    while (tentativePin > 0) {



      while (!PIN_correct && tentativePin > 0) {

        println("Saisissez votre code pin ")

        var pin = readLine()

        choix = 0



        if (codespin(id) == pin) {

          PIN_correct = true

          PINdemande = false



        } else {

          tentativePin -= 1



          if (tentativePin == 0) {

            println("Trop d’erreurs, abandon de l’identification")

            // Réinitialiser les variables pour une nouvelle tentative

            PIN_correct = false

            PINdemande = false

            tentativePin = 3



            println("Saisissez votre code identifiant ")

            id = readInt()



            while (id > nbclients) {

              println("Cet identifiant n'est pas valable")

              return

            }

          } else {

            println("Code pin erroné, il vous reste " + tentativePin + "tentatives")

          }

        }

      }



      while (choix != 5) {

        // Authentification réussie

        if (PIN_correct) {

          println("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code pin \n5) Terminer \n Votre choix : ")

          choix = readInt()

          if (choix == 1) {

            depot(id, comptes)

          }

          if (choix == 2) {

            retrait(id, comptes)

          }

          if (choix == 3) {

            println("Le montant disponible sur votre compte est de : " + comptes(id))

          }

          if (choix == 4) {

            changepin(id, codespin)

          }

        }



        if (choix == 5) {

          println("Fin des opérations, n’oubliez pas de récupérer votre carte. ")

          // Demande l'identification pour un nouvel utilisateur

          println("Saisissez votre code identifiant ")

          id = readInt()

          PIN_correct = false

          tentativePin = 3



        }

      }

    }

  }







}