//Assignment: Kamel Sammar_996583_assignsubmission_file

import scala.io.StdIn._
import scala.math._
object Main {
  var CHF_500 = 500
  var CHF_200 = 200
  var CHF_100 = 100
  var CHF_50 = 50
  var CHF_20 = 20
  var CHF_10 = 10
  var EUR_100 = 100
  var EUR_50 = 50
  var EUR_20 = 20
  var EUR_10 = 10
  var montantinitial = 1200.0
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
    var pasdivisible = false
    //montantinitial = comptes(id)
    while (pasdivisible == false) {
      var montant = readLine("Indiquez le montant du dépôt >").toInt

      // Si le montant est un multiple de 10 ou supérieur ou égale à 10
      if ((montant % 10 == 0) && (montant >= 10)) {
        pasdivisible = true
        var montantfinal = 0.0
        if (devise == 2) {
          //transformer en double
          var montant1 = montant.toDouble // converti les euros en francs
          montant1 = montant1 * 0.95
          comptes(id) = comptes(id) + montant1 // modifie le montant du compte
          var montantconvertit = printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
        } else if (devise == 1) {
          comptes(id) = montant + comptes(id) // on ajoute le montant des francs au montant de la consultation du compte, l'opération 3
          //comptes(id) = montantinitial
          var montantcompte = printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
        }
      }
      // Sinon si le montant n'est pas un multiple de 10
      else if (!(montant % 10 == 0) || (montant < 10)) {
        println("Le montant doit être un multiple de 10")
      }
    }
  }
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    // vérifie la devise
    var choix_est_CHF = false
    var choix_est_EUR = false
    while ((choix_est_CHF == false) && (choix_est_EUR == false)) {
      var devise = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
      // Si la devise est en francs, on sort de la boucle
      if (devise == 1) {
        choix_est_CHF = true
      }
      // Sinon si la devise est en euros, on sort de la boucle
      else if (devise == 2) {
        choix_est_EUR = true
      }
    }
    var retraitpasdivisible = false
    var montantretraitautorise = false
    retraitpasdivisible = false
    var plafondretrait = 0.10 * comptes(id)
    var montantretrait = 0

    while ((retraitpasdivisible == false) && (montantretraitautorise == false)) {

      montantretrait = readLine("Indiquez le montant du retrait >").toInt

      // vérifie si le montant de retrait est divisible par 10
      // vérifie aussi si le montant de retrait ne dépasse pas le montant de retrait autorisé
      if ((montantretrait % 10 == 0) && (montantretrait <= plafondretrait)) {
        retraitpasdivisible = true
        montantretraitautorise = true

      }
      // Sinon si le montant de retrait n'est pas divisible par 10 et que le montant du retrait est supérieur au plafond de retrait, on affiche les messages
      else if (!(montantretrait % 10 == 0) && (montantretrait > plafondretrait)) {
        println("Le montant doit être un multiple de 10")
        printf("Votre plafond de retrait autorisé est de : %.2f CHF \n", plafondretrait)
      }
      // Sinon si le montant de retrait n'est pas divisible par 10
      else if (!(montantretrait % 10 == 0)) {
        println("Le montant doit être un multiple de 10")

      }
      // Sinon si le montant du retrait est supérieur au plafond de retrait
      else if (montantretrait > plafondretrait) {
        printf("Votre plafond de retrait autorisé est de : %.2f CHF \n", plafondretrait)
      }




    }
    //println("Montant : " + montantretrait)
    // Si la devise choisi est en franc et que le montant du retrait est supérieur ou égale à 100
    if ((choix_est_CHF == true) && (montantretrait >= 200.0)) {
      var choixcoupure = false // Initialise qu'il a choisi la mauvaise coupure
      var idcoupure = 0 // initialise la variable qui va poser la question de coupure en Int
      // Tant qu'il ne choisi pas entre 1 ou 2
      while (choixcoupure == false) {
        idcoupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt

        if ((idcoupure == 1) || (idcoupure == 2)) {
          choixcoupure = true // On sort de la boucle ligne 163
        }
      }
      if (idcoupure == 1) {
        // Pour 500 Francs
        var choix_pas_500 = false
        var nombreutilisationCHF = 0
        var montantretrait1 = montantretrait
        var nombre_Billet_500 = ""
        var affiche_nombre_utilisation_500_CHF = 0
        while (choix_pas_500 == false) {
          // Si le montant du retrait est supérieur ou égale à 500
          if (montantretrait >= CHF_500) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            nombreutilisationCHF = montantretrait / CHF_500
            var nombre_Billet_500 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF + " billet(s) de " + CHF_500 + " CHF")

            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait - (nombreutilisationCHF * CHF_500)
              affiche_nombre_utilisation_500_CHF = nombreutilisationCHF
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait - (OK_ou_Pas1 * CHF_500)
                affiche_nombre_utilisation_500_CHF = OK_ou_Pas1

                choix_pas_500 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait1 = montantretrait
                affiche_nombre_utilisation_500_CHF = 0
                choix_pas_500 = true
              }
            }
            if (montantretrait1 < CHF_500) {
              choix_pas_500 = true
            }
          }
          else {
            choix_pas_500 = true
          }


        }
        // 1ère vérification du ok

        // pour 200
        var choix_pas_200 = false
        var nombreutilisationCHF1 = 0
        var nombre_Billet_200 = 0
        var montantretrait2 = 0
        var affiche_nombre_utilisation_200_CHF = 0
        while (choix_pas_200 == false) {
          if (montantretrait1 >= CHF_200) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF1 = montantretrait1 / CHF_200
            var nombre_Billet_200 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF1 + " billet(s) de " + CHF_200 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF1)) {
                OK = true
              }

            }
            montantretrait2 = montantretrait1 - (nombreutilisationCHF1 * CHF_200)

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationCHF1 * CHF_200)
              affiche_nombre_utilisation_200_CHF = nombreutilisationCHF1
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF1) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_200)
                nombreutilisationCHF1 = OK_ou_Pas1
                affiche_nombre_utilisation_200_CHF = OK_ou_Pas1


                choix_pas_500 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait2 = montantretrait1
                affiche_nombre_utilisation_200_CHF = 0
                choix_pas_200 = true
              }
            }
            if (montantretrait2 < CHF_200) {
              choix_pas_200 = true
            }
          }
          else {
            choix_pas_200 = true
          }

        }

        // pour 100
        var choix_pas_100 = false
        var nombreutilisationCHF2 = 0
        var nombre_Billet_100 = 0
        var montantretrait3 = 0
        var affiche_nombre_utilisation_100_CHF = 0
        while (choix_pas_100 == false) {
          if (montantretrait1 >= CHF_100) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF2 = montantretrait1 / CHF_100
            var nombre_Billet_100 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF2 + " billet(s) de " + CHF_100 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF2)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationCHF2 * CHF_100)
              affiche_nombre_utilisation_100_CHF = nombreutilisationCHF2
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF2) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_100)
                affiche_nombre_utilisation_100_CHF = OK_ou_Pas1

                choix_pas_100 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait3 = montantretrait1
                affiche_nombre_utilisation_100_CHF = 0
                choix_pas_100 = true
              }
            }
            if (montantretrait3 < CHF_100) {
              choix_pas_100 = true
            }
          }
          else {
            choix_pas_100 = true
          }

        }

        // pour 50
        var choix_pas_50 = false
        var nombreutilisationCHF3 = 0
        var nombre_Billet_50 = 0
        var montantretrait4 = 0
        var affiche_nombre_utilisation_50_CHF = 0
        while (choix_pas_50 == false) {
          if (montantretrait1 >= CHF_50) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF3 = montantretrait1 / CHF_50
            var nombre_Billet_50 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF3 + " billet(s) de " + CHF_50 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF3)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationCHF3 * CHF_50)
              affiche_nombre_utilisation_50_CHF = nombreutilisationCHF3
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF2) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_50)
                affiche_nombre_utilisation_50_CHF = OK_ou_Pas1

                choix_pas_50 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait4 = montantretrait1
                affiche_nombre_utilisation_50_CHF = 0
                choix_pas_50 = true
              }
            }
            if (montantretrait4 < CHF_50) {
              choix_pas_50 = true
            }
          }
          else {
            choix_pas_50 = true
          }

        }
        // pour 20
        var choix_pas_20 = false
        var nombreutilisationCHF4 = 0
        var nombre_Billet_20 = 0
        var montantretrait5 = 0
        var affiche_nombre_utilisation_20_CHF = 0
        while (choix_pas_20 == false) {
          if (montantretrait1 >= CHF_20) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF4 = montantretrait1 / CHF_20
            var nombre_Billet_20 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF4 + " billet(s) de " + CHF_20 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF4)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationCHF4 * CHF_20)
              affiche_nombre_utilisation_20_CHF = nombreutilisationCHF4
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF4) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_20)
                affiche_nombre_utilisation_20_CHF = OK_ou_Pas1

                choix_pas_20 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait5 = montantretrait1
                affiche_nombre_utilisation_20_CHF = 0
                choix_pas_20 = true
              }
            }
            if (montantretrait5 < CHF_20) {
              choix_pas_20 = true
            }
          }
          else {
            choix_pas_20 = true
          }

        }

        // pour 10
        var choix_pas_10 = false
        var nombreutilisationCHF5 = 0
        var nombre_Billet_10 = 0
        var montantretrait6 = 0
        var affiche_nombre_utilisation_10_CHF = 0
        while (choix_pas_10 == false) {
          if (montantretrait1 >= CHF_10) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF5 = montantretrait1 / CHF_10
            var nombre_Billet_10 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF5 + " billet(s) de " + CHF_10 + " CHF")

            montantretrait1 = montantretrait1 - (nombreutilisationCHF5 * CHF_10)
            affiche_nombre_utilisation_10_CHF = nombreutilisationCHF5
            choix_pas_10 = true

          }
          else {
            choix_pas_10 = true
          }

        }
        println("Veuillez retirer la somme demandée : ")

        if (!(affiche_nombre_utilisation_500_CHF == 0)) {
          println(affiche_nombre_utilisation_500_CHF + " billet(s) de " + CHF_500 + " CHF")

        }
        if (!(affiche_nombre_utilisation_200_CHF == 0)) {
          println(affiche_nombre_utilisation_200_CHF + " billet(s) de " + CHF_200 + " CHF")

        }
        if (!(affiche_nombre_utilisation_100_CHF == 0)) {
          println(affiche_nombre_utilisation_100_CHF + " billet(s) de " + CHF_100 + " CHF")

        }
        if (!(affiche_nombre_utilisation_50_CHF == 0)) {
          println(affiche_nombre_utilisation_50_CHF + " billet(s) de " + CHF_50 + " CHF")

        }
        if (!(affiche_nombre_utilisation_20_CHF == 0)) {
          println(affiche_nombre_utilisation_20_CHF + " billet(s) de " + CHF_20 + " CHF")

        }
        if (!(affiche_nombre_utilisation_10_CHF == 0)) {
          println(affiche_nombre_utilisation_10_CHF + " billet(s) de " + CHF_10 + " CHF")
        }
        // Si le nombre d'utilisation de chaque billet est différents de 0 on affiche le nombre de billets utilisé pour chaque billets
        comptes(id) = comptes(id) - (affiche_nombre_utilisation_500_CHF * CHF_500 + affiche_nombre_utilisation_200_CHF * CHF_200 + affiche_nombre_utilisation_100_CHF * CHF_100 + affiche_nombre_utilisation_50_CHF * CHF_50 + affiche_nombre_utilisation_20_CHF * CHF_20 + affiche_nombre_utilisation_10_CHF * CHF_10)
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))


      }  
      // Pour la petite coupure :
      else if (idcoupure == 2) {
        // pour 100
        var choix_pas_100 = false
        var nombreutilisationCHF2 = 0
        var nombre_Billet_100 = 0
        var montantretrait3 = 0
        var montantretrait1 = 0
        var affiche_nombre_utilisation_100_CHF = 0
        while (choix_pas_100 == false) {
          if (montantretrait >= CHF_100) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            nombreutilisationCHF2 = montantretrait / CHF_100
            var nombre_Billet_100 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF2 + " billet(s) de " + CHF_100 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF2)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait - (nombreutilisationCHF2 * CHF_100)
              affiche_nombre_utilisation_100_CHF = nombreutilisationCHF2
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF2) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait - (OK_ou_Pas1 * CHF_100)
                affiche_nombre_utilisation_100_CHF = OK_ou_Pas1

                choix_pas_100 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait1 = montantretrait
                affiche_nombre_utilisation_100_CHF = 0
                choix_pas_100 = true
              }
            }
            if (montantretrait3 < CHF_100) {
              choix_pas_100 = true
            }
          }
          else {
            choix_pas_100 = true
          }

        }

        // pour 50
        var choix_pas_50 = false
        var nombreutilisationCHF3 = 0
        var nombre_Billet_50 = 0
        var montantretrait4 = 0
        var affiche_nombre_utilisation_50_CHF = 0
        while (choix_pas_50 == false) {
          if (montantretrait1 >= CHF_50) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF3 = montantretrait1 / CHF_50
            var nombre_Billet_50 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF3 + " billet(s) de " + CHF_50 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF3)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationCHF3 * CHF_50)
              affiche_nombre_utilisation_50_CHF = nombreutilisationCHF3
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF2) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_50)
                affiche_nombre_utilisation_50_CHF = OK_ou_Pas1

                choix_pas_50 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait4 = montantretrait1
                affiche_nombre_utilisation_50_CHF = 0
                choix_pas_50 = true
              }
            }
            if (montantretrait4 < CHF_50) {
              choix_pas_50 = true
            }
          }
          else {
            choix_pas_50 = true
          }

        }
        // pour 20
        var choix_pas_20 = false
        var nombreutilisationCHF4 = 0
        var nombre_Billet_20 = 0
        var montantretrait5 = 0
        var affiche_nombre_utilisation_20_CHF = 0
        while (choix_pas_20 == false) {
          if (montantretrait1 >= CHF_20) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF4 = montantretrait1 / CHF_20
            var nombre_Billet_20 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF4 + " billet(s) de " + CHF_20 + " CHF")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF4)) {
                OK = true
              }

            }

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationCHF4 * CHF_20)
              affiche_nombre_utilisation_20_CHF = nombreutilisationCHF4
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationCHF4) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_20)
                affiche_nombre_utilisation_20_CHF = OK_ou_Pas1

                choix_pas_20 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait5 = montantretrait1
                affiche_nombre_utilisation_20_CHF = 0
                choix_pas_20 = true
              }
            }
            if (montantretrait5 < CHF_20) {
              choix_pas_20 = true
            }
          }
          else {
            choix_pas_20 = true
          }

        }

        // pour 10
        var choix_pas_10 = false
        var nombreutilisationCHF5 = 0
        var nombre_Billet_10 = 0
        var montantretrait6 = 0
        var affiche_nombre_utilisation_10_CHF = 0
        while (choix_pas_10 == false) {
          if (montantretrait1 >= CHF_10) {
            println("Il reste " + montantretrait1 + " CHF à distribuer")
            nombreutilisationCHF5 = montantretrait1 / CHF_10
            var nombre_Billet_10 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF5 + " billet(s) de " + CHF_10 + " CHF")

            montantretrait1 = montantretrait1 - (nombreutilisationCHF5 * CHF_10)
            affiche_nombre_utilisation_10_CHF = nombreutilisationCHF5

            choix_pas_10 = true

          }
          else {
            choix_pas_10 = true
          }

        }
        // Si le nombre d'utilisation de chaque billet est différents de 0 on affiche le nombre de billets utilisé pour chaque billets
        println("Veuillez retirer la somme demandée : ")

        if (!(affiche_nombre_utilisation_100_CHF == 0)) {
          println(affiche_nombre_utilisation_100_CHF + " billet(s) de " + CHF_100 + " CHF")

        }
        if (!(affiche_nombre_utilisation_50_CHF == 0)) {
          println(affiche_nombre_utilisation_50_CHF + " billet(s) de " + CHF_50 + " CHF")

        }
        if (!(affiche_nombre_utilisation_20_CHF == 0)) {
          println(affiche_nombre_utilisation_20_CHF + " billet(s) de " + CHF_20 + " CHF")

        }
        if (!(affiche_nombre_utilisation_10_CHF == 0)) {
          println(affiche_nombre_utilisation_10_CHF + " billet(s) de " + CHF_10 + " CHF")
        }
        comptes(id) = comptes(id) - (affiche_nombre_utilisation_100_CHF * CHF_100 + affiche_nombre_utilisation_50_CHF * CHF_50 + affiche_nombre_utilisation_20_CHF * CHF_20 + affiche_nombre_utilisation_10_CHF * CHF_10)
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

      }

    }
    else if ((choix_est_CHF == true) && (montantretrait < 200)) {
      // pour 100
      var choix_pas_100 = false
      var nombreutilisationCHF2 = 0
      var nombre_Billet_100 = 0
      var montantretrait3 = 0
      var montantretrait1 = montantretrait
      var affiche_nombre_utilisation_100_CHF = 0
      while (choix_pas_100 == false) {
        if (montantretrait >= CHF_100) {
          println("Il reste " + montantretrait + " CHF à distribuer")
          nombreutilisationCHF2 = montantretrait / CHF_100
          var nombre_Billet_100 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF2 + " billet(s) de " + CHF_100 + " CHF")

          var OK = false
          var OK_ou_Pas = ""
          while (OK == false) {
            OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF2)) {
              OK = true
            }

          }

          if (OK_ou_Pas == "o") {
            montantretrait1 = montantretrait - (nombreutilisationCHF2 * CHF_100)
            affiche_nombre_utilisation_100_CHF = nombreutilisationCHF2
          }
          else {
            var OK_ou_Pas1 = OK_ou_Pas.toInt
            if ((OK_ou_Pas1 <= nombreutilisationCHF2) && !(OK_ou_Pas1 == 0)) {
              montantretrait1 = montantretrait - (OK_ou_Pas1 * CHF_100)
              affiche_nombre_utilisation_100_CHF = OK_ou_Pas1

              choix_pas_100 = true
            }
            else if (OK_ou_Pas1 == 0) {
              montantretrait1 = montantretrait
              affiche_nombre_utilisation_100_CHF = 0
              choix_pas_100 = true
            }
          }
          if (montantretrait3 < CHF_100) {
            choix_pas_100 = true
          }
        }
        else {
          choix_pas_100 = true
        }

      }

      // pour 50
      var choix_pas_50 = false
      var nombreutilisationCHF3 = 0
      var nombre_Billet_50 = 0
      var montantretrait4 = 0
      var affiche_nombre_utilisation_50_CHF = 0
      while (choix_pas_50 == false) {
        if (montantretrait1 >= CHF_50) {
          println("Il reste " + montantretrait1 + " CHF à distribuer")
          nombreutilisationCHF3 = montantretrait1 / CHF_50
          var nombre_Billet_50 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF3 + " billet(s) de " + CHF_50 + " CHF")
          var OK = false
          var OK_ou_Pas = ""
          while (OK == false) {
            OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF3)) {
              OK = true
            }

          }

          if (OK_ou_Pas == "o") {
            montantretrait1 = montantretrait1 - (nombreutilisationCHF3 * CHF_50)
            affiche_nombre_utilisation_50_CHF = nombreutilisationCHF3
          }
          else {
            var OK_ou_Pas1 = OK_ou_Pas.toInt
            if ((OK_ou_Pas1 <= nombreutilisationCHF2) && !(OK_ou_Pas1 == 0)) {
              montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_50)
              affiche_nombre_utilisation_50_CHF = OK_ou_Pas1

              choix_pas_50 = true
            }
            else if (OK_ou_Pas1 == 0) {
              montantretrait4 = montantretrait1
              affiche_nombre_utilisation_50_CHF = 0
              choix_pas_50 = true
            }
          }
          if (montantretrait4 < CHF_50) {
            choix_pas_50 = true
          }
        }
        else {
          choix_pas_50 = true
        }

      }
      // pour 20
      var choix_pas_20 = false
      var nombreutilisationCHF4 = 0
      var nombre_Billet_20 = 0
      var montantretrait5 = 0
      var affiche_nombre_utilisation_20_CHF = 0
      while (choix_pas_20 == false) {
        if (montantretrait1 >= CHF_20) {
          println("Il reste " + montantretrait1 + " CHF à distribuer")
          nombreutilisationCHF4 = montantretrait1 / CHF_20
          var nombre_Billet_20 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF4 + " billet(s) de " + CHF_20 + " CHF")
          var OK = false
          var OK_ou_Pas = ""
          while (OK == false) {
            OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationCHF4)) {
              OK = true
            }

          }


          if (OK_ou_Pas == "o") {
            montantretrait1 = montantretrait1 - (nombreutilisationCHF4 * CHF_20)
            affiche_nombre_utilisation_20_CHF = nombreutilisationCHF4
          }
          else {
            var OK_ou_Pas1 = OK_ou_Pas.toInt
            if ((OK_ou_Pas1 <= nombreutilisationCHF4) && !(OK_ou_Pas1 == 0)) {
              montantretrait1 = montantretrait1 - (OK_ou_Pas1 * CHF_20)
              affiche_nombre_utilisation_20_CHF = OK_ou_Pas1

              choix_pas_20 = true
            }
            else if (OK_ou_Pas1 == 0) {
              montantretrait5 = montantretrait1
              affiche_nombre_utilisation_20_CHF = 0
              choix_pas_20 = true
            }
          }
          if (montantretrait5 < CHF_20) {
            choix_pas_20 = true
          }
        }
        else {
          choix_pas_20 = true
        }

      }

      // pour 10
      var choix_pas_10 = false
      var nombreutilisationCHF5 = 0
      var nombre_Billet_10 = 0
      var montantretrait6 = 0
      var affiche_nombre_utilisation_10_CHF = 0
      while (choix_pas_10 == false) {
        if (montantretrait1 >= CHF_10) {
          println("Il reste " + montantretrait1 + " CHF à distribuer")
          nombreutilisationCHF5 = montantretrait1 / CHF_10
          var nombre_Billet_10 = println("Vous pouvez obtenir au maximum " + nombreutilisationCHF5 + " billet(s) de " + CHF_10 + " CHF")

          montantretrait1 = montantretrait1 - (nombreutilisationCHF5 * CHF_10)
          affiche_nombre_utilisation_10_CHF = nombreutilisationCHF5


          choix_pas_10 = true

        }
        else {
          choix_pas_10 = true
        }

      }
      // Si le nombre d'utilisation de chaque billet est différents de 0 on affiche le nombre de billets utilisé pour chaque billets
      println("Veuillez retirer la somme demandée : ")

      if (!(affiche_nombre_utilisation_100_CHF == 0)) {
        println(affiche_nombre_utilisation_100_CHF + " billet(s) de " + CHF_100 + " CHF")

      }
      if (!(affiche_nombre_utilisation_50_CHF == 0)) {
        println(affiche_nombre_utilisation_50_CHF + " billet(s) de " + CHF_50 + " CHF")

      }
      if (!(affiche_nombre_utilisation_20_CHF == 0)) {
        println(affiche_nombre_utilisation_20_CHF + " billet(s) de " + CHF_20 + " CHF")

      }
      if (!(affiche_nombre_utilisation_10_CHF == 0)) {
        println(affiche_nombre_utilisation_10_CHF + " billet(s) de " + CHF_10 + " CHF")
      }
      comptes(id) = comptes(id) - (affiche_nombre_utilisation_100_CHF * CHF_100 + affiche_nombre_utilisation_50_CHF * CHF_50 + affiche_nombre_utilisation_20_CHF * CHF_20 + affiche_nombre_utilisation_10_CHF * CHF_10)
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))


    }
    else if (choix_est_EUR == true) {
      // pour 100
      var choix_pas_100 = false
      var nombreutilisationEUR2 = 0
      var nombre_Billet_100 = 0
      var montantretrait1 = montantretrait
      var montantretrait3 = 0
      var affiche_nombre_utilisation_100_EUR = 0
      var EUR_100 = 100
      while (choix_pas_100 == false) {
        if (montantretrait >= EUR_100) {
          println("Il reste " + montantretrait + " EUR à distribuer")
          nombreutilisationEUR2 = montantretrait / EUR_100
          var nombre_Billet_100 = println("Vous pouvez obtenir au maximum " + nombreutilisationEUR2 + " billet(s) de " + EUR_100 + " EUR")

          var OK_ou_inferieur = false
          var OK_ou_Pas = ""
          while (OK_ou_inferieur == false) {
          OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

            if (OK_ou_Pas == "o") {
              OK_ou_inferieur = true
            }
            else if (!(OK_ou_Pas == "o")) {
              OK_ou_inferieur = false
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if (OK_ou_Pas1 < nombreutilisationEUR2) {
                OK_ou_inferieur = true
              }
            }


          }

          if (OK_ou_Pas == "o") {
            montantretrait1 = montantretrait - (nombreutilisationEUR2 * EUR_100)
            affiche_nombre_utilisation_100_EUR = nombreutilisationEUR2
          }
          else {
            var OK_ou_Pas1 = OK_ou_Pas.toInt
            if ((OK_ou_Pas1 <= nombreutilisationEUR2) && !(OK_ou_Pas1 == 0)) {
              montantretrait1 = montantretrait - (OK_ou_Pas1 * EUR_100)
              affiche_nombre_utilisation_100_EUR = OK_ou_Pas1

              choix_pas_100 = true
            }
            else if (OK_ou_Pas1 == 0) {
              montantretrait1 = montantretrait
              affiche_nombre_utilisation_100_EUR = 0
              choix_pas_100 = true
            }
          }
          if (montantretrait3 < EUR_100) {
            choix_pas_100 = true
          }
        }
        else {
          choix_pas_100 = true
        }

      }

        // pour 50
        var choix_pas_50 = false
        var nombreutilisationEUR3 = 0
        var nombre_Billet_50 = 0
        var montantretrait4 = 0
        var affiche_nombre_utilisation_50_EUR = 0
        while (choix_pas_50 == false) {
          if (montantretrait1 >= EUR_50) {
            println("Il reste " + montantretrait1 + " EUR à distribuer")
            nombreutilisationEUR3 = montantretrait1 / EUR_50
            var nombre_Billet_50 = println("Vous pouvez obtenir au maximum " + nombreutilisationEUR3 + " billet(s) de " + EUR_50 + " EUR")
            var OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationEUR3 * CHF_50)
              affiche_nombre_utilisation_50_EUR = nombreutilisationEUR3
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationEUR3) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * EUR_50)
                affiche_nombre_utilisation_50_EUR = OK_ou_Pas1

                choix_pas_50 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait4 = montantretrait1
                affiche_nombre_utilisation_50_EUR = 0
                choix_pas_50 = true
              }
            }
            if (montantretrait4 < EUR_50) {
              choix_pas_50 = true
            }
          }
          else {
            choix_pas_50 = true
          }

        }
        // pour 20
        var choix_pas_20 = false
        var nombreutilisationEUR4 = 0
        var nombre_Billet_20 = 0
        var montantretrait5 = 0
        var affiche_nombre_utilisation_20_EUR = 0
        while (choix_pas_20 == false) {
          if (montantretrait1 >= EUR_20) {
            println("Il reste " + montantretrait1 + " EUR à distribuer")
            nombreutilisationEUR4 = montantretrait1 / EUR_20
            var nombre_Billet_20 = println("Vous pouvez obtenir au maximum " + nombreutilisationEUR4 + " billet(s) de " + EUR_20 + " EUR")
            var OK = false
            var OK_ou_Pas = ""
            while (OK == false) {
              OK_ou_Pas = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if ((OK_ou_Pas == "o") || (OK_ou_Pas.toInt < nombreutilisationEUR4)) {
                OK = true
              }

            }


            if (OK_ou_Pas == "o") {
              montantretrait1 = montantretrait1 - (nombreutilisationEUR4 * EUR_20)
              affiche_nombre_utilisation_20_EUR = nombreutilisationEUR4
            }
            else {
              var OK_ou_Pas1 = OK_ou_Pas.toInt
              if ((OK_ou_Pas1 <= nombreutilisationEUR4) && !(OK_ou_Pas1 == 0)) {
                montantretrait1 = montantretrait1 - (OK_ou_Pas1 * EUR_20)
                affiche_nombre_utilisation_20_EUR = OK_ou_Pas1

                choix_pas_20 = true
              }
              else if (OK_ou_Pas1 == 0) {
                montantretrait5 = montantretrait1
                affiche_nombre_utilisation_20_EUR = 0
                choix_pas_20 = true
              }
            }
            if (montantretrait5 < EUR_20) {
              choix_pas_20 = true
            }
          }
          else {
            choix_pas_20 = true
          }

        }

        // pour 10
        var choix_pas_10 = false
        var nombreutilisationEUR5 = 0
        var nombre_Billet_10 = 0
        var montantretrait6 = 0
        var affiche_nombre_utilisation_10_EUR = 0
        while (choix_pas_10 == false) {
          if (montantretrait1 >= EUR_10) {
            println("Il reste " + montantretrait1 + " EUR à distribuer")
            nombreutilisationEUR5 = montantretrait1 / EUR_10
            var nombre_Billet_10 = println("Vous pouvez obtenir au maximum " + nombreutilisationEUR5 + " billet(s) de " + EUR_10 + " EUR")


            montantretrait1 = montantretrait1 - (nombreutilisationEUR5 * EUR_10)
            affiche_nombre_utilisation_10_EUR = nombreutilisationEUR5
            choix_pas_10 = true

          }
          else {
            choix_pas_10 = true
          }

        }
      // Si le nombre d'utilisation de chaque billet est différents de 0 on affiche le nombre de billets utilisé pour chaque billets
      println("Veuillez retirer la somme demandée : ")

      if (!(affiche_nombre_utilisation_100_EUR == 0)) {
        println(affiche_nombre_utilisation_100_EUR + " billet(s) de " + EUR_100 + " EUR")

      }
      if (!(affiche_nombre_utilisation_50_EUR == 0)) {
        println(affiche_nombre_utilisation_50_EUR + " billet(s) de " + EUR_50 + " EUR")

      }
      if (!(affiche_nombre_utilisation_20_EUR == 0)) {
        println(affiche_nombre_utilisation_20_EUR + " billet(s) de " + EUR_20 + " EUR")

      }
      if (!(affiche_nombre_utilisation_10_EUR == 0)) {
        println(affiche_nombre_utilisation_10_EUR + " billet(s) de " + EUR_10 + " EUR")
      }
      var montant_retrait_EUR_converti = 0.95 * (affiche_nombre_utilisation_100_EUR * EUR_100 + affiche_nombre_utilisation_50_EUR * EUR_50 + affiche_nombre_utilisation_20_EUR * EUR_20 + affiche_nombre_utilisation_10_EUR * EUR_10)
      comptes(id) = comptes(id) - montant_retrait_EUR_converti 
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))


    }
  }
  def consultation_compte(id: Int, comptes : Array[Double]) : Unit = {
    montantinitial = comptes(id)
    printf("Le montant disponible sur votre compte est de : %.2f CHF \n", montantinitial)
    //comptes(id) = montantinitial
  }


  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var change_Code_PIN = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var longueur = change_Code_PIN.length
    var nombre_caractere_change_Code_PIN = false
    while (nombre_caractere_change_Code_PIN == false) {
      if (longueur >= 8) {
        codespin(id) = change_Code_PIN
        nombre_caractere_change_Code_PIN = true
      }
      else {
        println("Votre code pin ne contient pas au moins 8 caractères")
        change_Code_PIN = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        longueur = change_Code_PIN.length
      }
    }
  }

  def main(args: Array[String]): Unit = {




    // Initialisation des variables
    var choixoperation4 = false
    var codePINcorrect = false
    var montantretrait = 0
    var nombretentative = 3


    var nbclients = 100
    var montantinitial = 1200.0
    var id = 0
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    //comptes(i) = codespin(i)

    // initialisation du mots de passe
    // tant que l'utilisateur choisi soit l'opération 1, soit la 2, soit la 3 et que le code Pin n'est toujours pas correct on exécute la boucle while
    
    var codePIN = ""
    var choixoperation5 = false
    while (choixoperation5 == false) {


      while (codePINcorrect == false) {
        nombretentative = 3
        id = readLine("Saisissez votre identifiant >").toInt
        if (id >= nbclients) {
          println("Cet identifiant n’est pas valable.")
          sys.exit(0)
        }
        //println(comptes(id) + codespin(id))
        var codePIN = codespin(id)
        // var nombretentative = 3
        // vérification du code PIN
        var verification = false
        while ((verification == false) && (nombretentative > 0)) {

          var verifiecodePIN = readLine("Saisissez votre code pin >")
          // Si le code PIN est correct
          if (verifiecodePIN == codePIN) {
            verification = true
            codePINcorrect = true
          }
          //Sinon si le code Pin est incorrect, on enlève à chaque fois une tentative jusqu'à ce que ce soit égale à 0. Arrivé à 0 tentative, on arrêtre le programme
          else if ((verification == false) && (!(nombretentative == 0))) {
            nombretentative -= 1
            // Si le nombre de tentative est différent de 0 on affiche le message
            if (!(nombretentative == 0)) {
              println("Code pin erroné, il vous reste " + nombretentative + " tentatives >")
            }
            if (nombretentative == 0) {
              println("Trop d’erreurs, abandon de l’identification")
              codePINcorrect = false
                //choixoperation4 = true
                // Forcer l'arrêt du programme : la carte est avalé après 3 tentatives erronées du Code PIN
                //sys.exit(0)
            }
          }
        }
      }
      var choixoperation4 = false
      while (choixoperation4 == false) {


        // Choisissez votre opération :
        var choixoperation = readLine("Choisissez votre opération : " + '\n' + " 1) Dépôt" + '\n' + " 2) Retrait" + '\n' + " 3) Consultation du compte" + '\n' + " 4) Changement du code pin" + '\n' + " 5) Terminer" + '\n' + "Votre choix :").toInt

        // Si l'opération 4 est choisi, on arrête le programme
        if (choixoperation == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          choixoperation4 = true
          codePINcorrect = false
          //codePIN = codespin(id)
          //choixoperation5 = false
          //nombretentative = 3
        }
        else if (choixoperation == 4) {
          changepin(id,codespin)
          //val char_change_Code_PIN = change_Code_PIN.toCharArray
          // codePINcorrect = true
        }
        else if ((choixoperation == 1)) {
          // Opération de dépôt
          depot(id,comptes)
          //id = nbclients
          //var test = depot(id)
        }
        // choix de l'opération de consultation des comptes
        else if (choixoperation == 3) {
          consultation_compte(id,comptes)
        }
        // choix de l'opération de retrait
        else if (choixoperation == 2) {
          retrait(id,comptes)
        }
      }
    }
  }
}