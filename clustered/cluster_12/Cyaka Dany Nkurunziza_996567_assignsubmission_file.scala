//Assignment: Cyaka Dany Nkurunziza_996567_assignsubmission_file

import io.StdIn._

object Main {
  //Liste de toutes les variables 

  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")
  var operation = 0
  var code_pin = "INTRO1234"
  var id = 0
  var pin_entré = " "
  var tentatives = 0 
  var essais = 0
  var devise = 0
  var depots = 0
  var retrait = 0.0
  var retrait_total = 0.0
  var montant_total = 0.0
  var plafond_retrait = 0.0
  var coupures = 0
  var nbr_coupures = 0
  var validation = " "
  var nbr_billets_500 = 0
  var nbr_billets_200 = 0
  var nbr_billets_100 = 0
  var nbr_billets_50 = 0
  var nbr_billets_20 = 0
  var nbr_billets_10 = 0

  //liste des méthodes utilisés

  //méthode de dépôt

  def depot(id : Int, comptes : Array[Double]) : Unit = {
    if(operation == 1){
      devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
      //vérification que le choix soit bien des euros ou CHF
      while((devise != 1)&&(devise!=2)){
        println("Insérez la valeur 1 ou 2.")
        devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
      }
      depots = readLine("Indiquez le montant du dépôt > ").toInt
      //vérification que le dépôt soit bien un multiple de 10
      while(depots%10 != 0){
        println("Le montant doit être un multiple de 10")
        depots = readLine("Indiquez le montant du dépôt > ").toInt
      }

      //conversion euros en francs

      if(devise==2){
        comptes(id) = comptes(id) + (depots*0.95)
      }
      //dépot en CHF
      else{
        comptes(id) = comptes(id) + depots
      }

      //affichage de la somme totale
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

      //retour sur le menu après le dépôt
      operation = readLine("Choisissez votre opération : \n" + "1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt 

      //contrôle du choix sur le menu
      while((operation > 5)||(operation<1)){
        println("saisissez un numéro entre 1 et 5 compris" )
        operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt

        }    
    }

  }

  //méthode de consultation

  def consultation(id : Int, comptes : Array[Double]) : Unit = {
    printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))

    //retour sur le menu
    operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt 

    //contrôle du choix sur le menu
    while((operation > 5)||(operation<1)){
      println("saisissez un numéro entre 1 et 5 compris" )
      operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt

      }
  }

  //méthode de changement de pin

  def changement_pin(id : Int, codespin : Array[String]) : Unit = {
    codespin(id) = readLine("Indiquez votre nouveau code pin > ")
      while(codespin(id).length<8){
        println("Le code pin doit comporter au moins 8 caractères")
        codespin(id) = readLine("Indiquez votre nouveau code pin > ")
      }

      //retour sur le menu
      operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt 

      //contrôle du choix sur le menu
      while((operation > 5)||(operation<1)){
        println("saisissez un numéro entre 1 et 5 compris" )
        operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt
    }
  }
    

  //méthode de retrait

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    
    //initialisation-réinitialisation du nbr des billets et du plafond de retrait
      nbr_billets_500 = 0
      nbr_billets_200 = 0
      nbr_billets_100 = 0
      nbr_billets_50 = 0
      nbr_billets_20 = 0
      nbr_billets_10 = 0

      plafond_retrait = comptes(id) / 10

      //demande de devise
      devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
      //contrôle devise, vérifier si c'est bien des CHF ou EUR
      while((devise != 1)&&(devise!=2)){
        println("Insérez la valeur 1 ou 2.")
        devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
      }
      //demande montant du retrait
      retrait = readLine("Indiquez le montant du retrait > ").toInt
      //vérification que c'est un multiple de 10 et si le plafond est respecté
      while(retrait%10 != 0 || retrait > plafond_retrait){
        if(retrait%10 != 0){
          println("Le montant doit être un multiple de 10")
        } 
        if(retrait > plafond_retrait){
            println("Votre plafond de retrait est de : " + plafond_retrait)

          }
        retrait = readLine("Indiquez le montant du retrait > ").toInt
        }

      retrait_total = retrait


      //retrait en CHF
      if(devise == 1){
        //Si le retrait est inférieure à 200 CHF, on ne demande pas grandes ou petites coupures, c'est automatiquement petites coupures
        if(retrait<200){
          coupures = 2
        }
        //demande grandes ou petites coupures
        else{
          coupures = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
          //vérification si grandes ou petites coupures choisies
          while((coupures != 1)&&(coupures!= 2)){
            println("Veuillez choisir entre 1 et 2")
            coupures = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
          }

        }

        //calcul du montant total dans le compte suite au retrait
        comptes(id) = comptes(id) - retrait

        //retrait en grandes coupures en CHF

        if((coupures == 1)&&(devise == 1)){

          //demande si on veut des billets de 500 et calcul le nombre de billets de 500 qu'on peut obtenir au maximum
          if(retrait>=500){
            nbr_coupures = (retrait/500).toInt
            validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 500 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            //vérification du choix du nombre de billets
            while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                  validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 500 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
            if(validation == "o"){
              //calcul du nombre de billets de 500
              nbr_billets_500 = nbr_coupures
            }
            //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
            else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
              nbr_billets_500 = (validation).toInt
            }
            //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 500
            if(validation == "o"){
              retrait = retrait - 500 * nbr_coupures
            }
            else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
              retrait = retrait - 500 * (validation).toInt
            }



         }
          //demande si on veut des billets de 200 et calcul le nombre de billets de 200 qu'on peut obtenir au maximum
          if(((validation == "o")&&(retrait>200))||(retrait>=200)){
            nbr_coupures = (retrait/200).toInt
            validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 200 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            //vérification du choix du nombre de billets
            while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                  validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 200 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
            if(validation == "o"){
              //calcul du nombre de billets de 200
              nbr_billets_200 = nbr_coupures
            }
            //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
            else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
              nbr_billets_200 = (validation).toInt
            }
            //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 200
            if(validation == "o"){
              retrait = retrait - 200 * nbr_coupures
            }
            else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
              retrait = retrait - 200 * (validation).toInt
            }
            if(validation == "o"){
              nbr_billets_200 = nbr_coupures
            }
          }

          //demande si on veut des billets de 100 et calcul le nombre de billets de 100 qu'on peut obtenir au maximum
          if(((validation == "o")&&(retrait>100))||(retrait>=100)){
            nbr_coupures = (retrait/100).toInt
            validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            //vérification du choix du nombre de billets
            while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                  validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
            if(validation == "o"){
              //calcul du nombre de billets de 100
              nbr_billets_100 = nbr_coupures
            }
            //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
            else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
              nbr_billets_100 = (validation).toInt
            }
            //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 100
            if(validation == "o"){
              retrait = retrait - 100 * nbr_coupures
            }
            else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
              retrait = retrait - 100 * (validation).toInt
            }
            if(validation == "o"){
              nbr_billets_100 = nbr_coupures
            }
          }

          //demande si on veut des billets de 50 et calcul le nombre de billets de 50 qu'on peut obtenir au maximum
          if(((validation == "o")&&(retrait>50))||(retrait>=50)){
            nbr_coupures = (retrait/50).toInt
            validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            //vérification du choix du nombre de billets
            while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                  validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
            if(validation == "o"){
              //calcul du nombre de billets de 50
              nbr_billets_50 = nbr_coupures
            }
            //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
            else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
              nbr_billets_50 = (validation).toInt
            }
            //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 50
            if(validation == "o"){
              retrait = retrait - 50 * nbr_coupures
            }
            else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
              retrait = retrait - 50 * (validation).toInt
            }
            if(validation == "o"){
              nbr_billets_50 = nbr_coupures
            }
          }

          //demande si on veut des billets de 20 et calcul le nombre de billets de 20 qu'on peut obtenir au maximum
          if(((validation == "o")&&(retrait>20))||(retrait>=20)){   
            nbr_coupures = (retrait/20).toInt
            validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            //vérification du choix du nombre de billets
            while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                  validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
            if(validation == "o"){
              //calcul du nombre de billets de 20
              nbr_billets_20 = nbr_coupures
            }
            //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
            else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
              nbr_billets_20 = (validation).toInt
            }
            //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 20
            if(validation == "o"){
              retrait = retrait - 20 * nbr_coupures
            }
            else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
              retrait = retrait - 20 * (validation).toInt
            }
            if(validation == "o"){
              nbr_billets_20 = nbr_coupures
            }
          }

          //Le reste du montant a retiré est distribué sous forme de billets de 10
          if(((validation == "o")&&(retrait>10))||(retrait>=10)){
            nbr_coupures = (retrait/10).toInt
            println("Il reste " + retrait + " CHF à distribuer \nVous obtenez " + nbr_coupures + " billet(s) de 10 CHF > ")
            nbr_billets_10 = nbr_coupures
          }

          //affichage du nombre de billets 
          if(nbr_billets_500 != 0){
              println(nbr_billets_500 + " billet(s) de 500 CHF")
          }
          if(nbr_billets_200 != 0){
            println(nbr_billets_200 + " billet(s) de 200 CHF")
          }
          if(nbr_billets_100 != 0){
            println(nbr_billets_100 + " billet(s) de 100 CHF")
          }
          if(nbr_billets_50 != 0){
            println(nbr_billets_50 + " billet(s) de 50 CHF")
          }
          if(nbr_billets_20 != 0){
            println(nbr_billets_20 + " billet(s) de 20 CHF")
          }
          if(nbr_billets_10 != 0){
            println(nbr_billets_10 + " billet(s) de 10 CHF")
          }         
        }         
      }

        // retrait avec petite coupure en CHF

        if((coupures == 2)&&(devise == 1)){

          //demande si on veut des billets de 100 et calcul le nombre de billets de 100 qu'on peut obtenir au maximum
            if(((validation == "o")&&(retrait>100))||(retrait>=100)){
              nbr_coupures = (retrait/100).toInt
              validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              //vérification du choix du nombre de billets
              while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                  println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                    validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              }
              //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
              if(validation == "o"){
                //calcul du nombre de billets de 100
                nbr_billets_100 = nbr_coupures
              }
              //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
              else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
                nbr_billets_100 = (validation).toInt
              }
              //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 100
              if(validation == "o"){
                retrait = retrait - 100 * nbr_coupures
              }
              else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
                retrait = retrait - 100 * (validation).toInt
              }
              if(validation == "o"){
                nbr_billets_100 = nbr_coupures
              }
            }

          //demande si on veut des billets de 50 et calcul le nombre de billets de 50 qu'on peut obtenir au maximum
            if(((validation == "o")&&(retrait>50))||(retrait>=50)){
              nbr_coupures = (retrait/50).toInt
              validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              //vérification du choix du nombre de billets
              while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                  println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                    validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              }
              //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
              if(validation == "o"){
                //calcul du nombre de billets de 50
                nbr_billets_50 = nbr_coupures
              }
              //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
              else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
                nbr_billets_50 = (validation).toInt
              }
              //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 50
              if(validation == "o"){
                retrait = retrait - 50 * nbr_coupures
              }
              else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
                retrait = retrait - 50 * (validation).toInt
              }
              if(validation == "o"){
                nbr_billets_50 = nbr_coupures
              }
            }

          //demande si on veut des billets de 20 et calcul le nombre de billets de 20 qu'on peut obtenir au maximum
            if(((validation == "o")&&(retrait>20))||(retrait>=20)){   
              nbr_coupures = (retrait/20).toInt
              validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              //vérification du choix du nombre de billets
              while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                  println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                    validation = readLine("Il reste " + retrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              }
              //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
              if(validation == "o"){
                //calcul du nombre de billets de 20
                nbr_billets_20 = nbr_coupures
              }
              //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
              else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
                nbr_billets_20 = (validation).toInt
              }
              //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 20
              if(validation == "o"){
                retrait = retrait - 20 * nbr_coupures
              }
              else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
                retrait = retrait - 20 * (validation).toInt
              }
              if(validation == "o"){
                nbr_billets_20 = nbr_coupures
              }
            }

          //Le reste du montant a retiré est distribué sous forme de billets de 10
            if(((validation == "o")&&(retrait>10))||(retrait>=10)){
              nbr_coupures = (retrait/10).toInt
              println("Il reste " + retrait + " CHF à distribuer \nVous obtenez " + nbr_coupures + " billet(s) de 10 CHF > ")

              nbr_billets_10 = nbr_coupures    
            }

          //affichage du nombre de billets 
          if(nbr_billets_100 != 0){
            println(nbr_billets_100 + " billet(s) de 100 CHF")
          }
          if(nbr_billets_50 != 0){
            println(nbr_billets_50 + " billet(s) de 50 CHF")
          }
          if(nbr_billets_20 != 0){
            println(nbr_billets_20 + " billet(s) de 20 CHF")
          }
          if(nbr_billets_10 != 0){
            println(nbr_billets_10 + " billet(s) de 10 CHF")
          }





        }



        // retrait avec coupures en EUR

        if(devise == 2){
          //demande si on veut des billets de 100 et calcul le nombre de billets de 100 qu'on peut obtenir au maximum
          if(((validation == "o")&&(retrait>100))||(retrait>=100)){
                nbr_coupures = (retrait/100).toInt
                validation = readLine("Il reste " + retrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 100 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            //vérification du choix du nombre de billets
                while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                    println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                      validation = readLine("Il reste " + retrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 100 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                }
            //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
                if(validation == "o"){
                  //calcul du nombre de billets de 100
                  nbr_billets_100 = nbr_coupures
                }
            //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
                else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
                  nbr_billets_100 = (validation).toInt
                }
            //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 100
                if(validation == "o"){
                  retrait = retrait - 100 * nbr_coupures
                }
                else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
                  retrait = retrait - 100 * (validation).toInt
                }
                if(validation == "o"){
                  nbr_billets_100 = nbr_coupures
                }
              }

          //demande si on veut des billets de 50 et calcul le nombre de billets de 50 qu'on peut obtenir au maximum
              if(((validation == "o")&&(retrait>50))||(retrait>=50)){
                nbr_coupures = (retrait/50).toInt
                validation = readLine("Il reste " + retrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 50 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                //vérification du choix du nombre de billets
                while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                    println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                      validation = readLine("Il reste " + retrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 50 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                }
                //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
                if(validation == "o"){
                  //calcul du nombre de billets de 50
                  nbr_billets_50 = nbr_coupures
                }
                //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
                else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
                  nbr_billets_50 = (validation).toInt
                }
                //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 50
                if(validation == "o"){
                  retrait = retrait - 50 * nbr_coupures
                }
                else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
                  retrait = retrait - 50 * (validation).toInt
                }
                if(validation == "o"){
                  nbr_billets_50 = nbr_coupures
                }
              }

          //demande si on veut des billets de 20 et calcul le nombre de billets de 20 qu'on peut obtenir au maximum
              if(((validation == "o")&&(retrait>20))||(retrait>=20)){   
                nbr_coupures = (retrait/20).toInt
                validation = readLine("Il reste " + retrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de 20 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                //vérification du choix du nombre de billets
                while((validation !="o")&&(((validation).toInt<0)||((validation).toInt>=nbr_coupures))){
                    println("Veuillez choisir o ou une autre valeur inférieur à celle proposée s'il vous plaît")
                      validation = readLine("Il reste " + retrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + nbr_coupures + " billet(s) de EUR CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                }
                //Si on appuie sur o, on obtient le maximum de billets autorisés (de 500 dans ce cas)
                if(validation == "o"){
                  //calcul du nombre de billets de 20
                  nbr_billets_20 = nbr_coupures
                }
                //On peut choisir un nombre de billets entre 0 et le nombre maximum autorisé
                else if((validation == "0")||((validation).toInt<nbr_coupures)&&((validation).toInt>0)){
                  nbr_billets_20 = (validation).toInt
                }
                //calcul du montant restant à retirer après avoir fait le choix du nombre de billets de 20
                if(validation == "o"){
                  retrait = retrait - 20 * nbr_coupures
                }
                else if((validation == "0")||(((validation).toInt<nbr_coupures)&&((validation).toInt>0))){
                  retrait = retrait - 20 * (validation).toInt
                }
                if(validation == "o"){
                  nbr_billets_20 = nbr_coupures
                }
              }

          //Le reste du montant a retiré est distribué sous forme de billets de 10
              if(((validation == "o")&&(retrait>10))||(retrait>=10)){
                nbr_coupures = (retrait/10).toInt
                println("Il reste " + retrait + " EUR à distribuer \nVous obtenez " + nbr_coupures + " billet(s) de 10 EUR > ")
                nbr_billets_10 = nbr_coupures
              }

          //affichage du nombre de billets 
            if(nbr_billets_100 != 0){
              println(nbr_billets_100 + " billet(s) de 100 EUR")
            }
            if(nbr_billets_50 != 0){
              println(nbr_billets_50 + " billet(s) de 50 EUR")
            }
            if(nbr_billets_20 != 0){
              println(nbr_billets_20 + " billet(s) de 20 EUR")
            }
            if(nbr_billets_10 != 0){
              println(nbr_billets_10 + " billet(s) de 10 EUR")
            }

          comptes(id) = comptes(id) - (retrait_total * 0.95)          
        }

      //affichage  du nouveau montant du compte
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

      //retour sur le menu
      operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt 

      //vérification du choix de l'opération
      while((operation > 4)||(operation<1)){
        println("saisissez un numéro entre 1 et 4 compris" )
        operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt
        }
    }    
  

  def main(args: Array[String]): Unit = {

    // Saisie de l'identifiant

    
    do{
      id = readLine("Saisissez votre code identifiant : ").toInt
      if(id<nbclients){
        
        //vérification du code Pin
        
        pin_entré = readLine("Saisissez votre code pin > ").toString
        while((codespin(id) != pin_entré)&&(tentatives<3)&&(essais!=1)){
          tentatives += 1
          essais = 4 - (tentatives + 1)
          println("Code pin erroné, il vous reste " + essais + " tentatives >")
          pin_entré = readLine("Saisissez votre code pin > ").toString

        }
        if(tentatives>2){
          operation = 5
          
        }
 
       //arrivée sur le menu après le dépôt

        if(tentatives<2){
          operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt 
        }
        else{
          operation = 5
        }

        
        

        if(operation != 5){
            //contrôle du choix sur le menu
            while((operation > 5)||(operation<1)){
              println("saisissez un numéro entre 1 et 5 compris" )
              operation = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de code pin \n5) Terminer \nVotre choix : ").toInt
              }
          }
          
          while((operation !=5)&&(tentatives<3)){


          //Opération de dépôt de l'argent

            if(operation == 1){
              depot(id, comptes)
            }

          // Opération de consultation

          if(operation == 3){
            consultation(id, comptes)
          }

          // Opération de changement de code pin
          if(operation == 4){
              changement_pin(id, codespin)
            }

          //Opération de retrait

          if(operation == 2){
            retrait(id, comptes)
          }



        }
           
              
      }
       //Lorsque les utilisateurs appuient sur 5, cela termine la session
      if((operation == 5)||(id >= nbclients)||(tentatives>1)){
        if(id >= nbclients){
          println("Cet identifiant n'est pas valable.")
        }
        if(tentatives>1){
          println("Trop d'erreurs, abandon de l'identification.")
        }
        if((operation==5)&&(id<nbclients)&&(tentatives<2)){
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
        }

      }
    }while(id<nbclients)
  }
}
