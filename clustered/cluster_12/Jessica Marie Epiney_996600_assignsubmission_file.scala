//Assignment: Jessica Marie Epiney_996600_assignsubmission_file

//importation des librairies pour la lecture d'entier et de string
import scala.io.StdIn.readInt
import scala.io.StdIn.readLine
import scala.math

object Main {
  var inferieur="" // Variable pour stocker la valeur saisie, initialisée à 0
  def main(args: Array[String]): Unit = {

    var idd = -1
    val nbclients=100
    var demandePIN = 0 
    var comptes = Array.fill(nbclients)(1200.00) // Solde total des nbclients comptes au début
    var PIN_valide = false
    var essaisRestants = 3 
    var utilisateurChoix = 0 // Le choix de l'utilisateur
    var codespin = Array.fill(nbclients)("INTRO1234") // Code pin des nbclients comptes
    var sortir=true
    var meme_utilisateur=0
    
    while (sortir) {
      if(meme_utilisateur==0)
      {
        idd = -1
        essaisRestants=3
        PIN_valide=false
        while(idd<0)
        {
          idd = readLine("Saisissez votre code identifiant >").toInt
          if(idd>=nbclients)
          {
            println("Cet identifiant n’est pas valable.")
            System.exit(0)  //seule et unique option pour sortir du programme : idd invallide
          }
        }
        while (!PIN_valide && essaisRestants > 0) { // Tant que le client n'a pas entré le bon code secret et qu'il a encore des essais
          println("Saisissez votre code PIN >")
          var codepinInput = readLine()

          if (codepinInput == codespin(idd)) {
            PIN_valide = true // Si le code secret est correct, définir la variable à true
            meme_utilisateur=1
            demandePIN = 1 // Définir la variable à 1 pour ne plus demander le code secret
          } else {

            essaisRestants = essaisRestants - 1 // Décrémenter le nombre d'essais restants

            if(essaisRestants == 0){ // Si le client n'a plus d'essais

              println("Trop d’erreurs, abandon de l’identification")  //plus de sortie de programme, maintenant on retourne juste à l'identification
              essaisRestants=3  //on fait comme si l'utilisateur avait réussi en lui redonnant 3 tentatives 
              PIN_valide=true
            } else {
              //Affichage du nombre de tentatives restants 
              print("Code pin erroné, il vous reste ")
              print(essaisRestants)
              println(" tentatives >")

            }
          }
        }
        
      }
      if(meme_utilisateur>0){
        // Affichage des options pour l'utilisateur
        println("Choisissez votre opération :")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")

        // Lecture du choix de l'utilisateur
        utilisateurChoix = readLine().toInt

        // Vérification si le choix de l'utilisateur est valide
        if(utilisateurChoix!=1 && utilisateurChoix != 2 && utilisateurChoix != 3 && utilisateurChoix != 4 && utilisateurChoix != 5){
          // Si le choix n'est pas valide, afficher un message d'erreur
          println("Choix invalide. Veuillez entrer un numéro entre 1 et 5.")
        }
        else {
          if(utilisateurChoix==5){
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            meme_utilisateur=0
          }
          else if(PIN_valide && utilisateurChoix == 1){ 
            depot(idd,comptes)//depot d'argent avec appel de la fonction dépot comme dans le sujet
          }
          else if(PIN_valide && utilisateurChoix == 3){ //consultation du compte 
            printf("Le montant disponible sur votre compte est : %.2f", comptes(idd))
            println(" CHF")
          }
          else if(PIN_valide && utilisateurChoix == 4){ // changement du code PIN
            changepin(idd, codespin)
          }
          else if(PIN_valide && utilisateurChoix == 2)
          {
            retrait(idd,comptes)
          }
        }  
      }
    }
  }
  def retrait(id : Int, comptes : Array[Double]) : Unit =   //prototyê de la fonction avec deux paramètres
  {
    //retrait d'argent
      //declaration des variables de tous les billets pour les enregistrer après
      var billet_de_500 = 0
       var billet_de_200 = 0
       var billet_de_100 = 0
       var billet_de_50 = 0
       var billet_de_20 = 0
       var billet_de_10 = 0
      var input="a"


      var coupure_petite_indic=false

      //on demande la devise du depot 
        var devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR >").toInt
         while (devise != 1 && devise != 2) {
              devise = readLine("Indiquez la devise du retrait: 1)CHF ; 2) EUR >").toInt
         }
              //variable du montant du retrait
              var retirer_montant = readLine("Indiquez le montant du retrait: ").toInt
          while (retirer_montant%10!=0 || retirer_montant<0) {
            retirer_montant = readLine("Le montant doit être un multiple de 10 >").toInt
          }
          while (retirer_montant > (comptes(id)*0.1) || retirer_montant<0 || retirer_montant%10!=0) {
            println("Votre plafond de retrait autorisé est de : " + comptes(id)*0.1)
            retirer_montant = readLine("Indiquez le montant du retrait: ").toInt
          }
          if(devise == 1){
           comptes(id) -= retirer_montant
         }
          if(devise == 2){
            var montant_conversion = retirer_montant*0.95
                comptes(id)-=montant_conversion
          }
          //devise1 : retrait en franc suisse
          if(devise==1){
            if (retirer_montant >= 200) {
              var coupures=0
              do
              {
                 coupures = readLine("En 1) grosses coupures; 2) petites coupures >").toInt
              }while ((coupures != 1) && (coupures != 2))

            if (coupures == 1) { 
              var retrait_mt=retirer_montant
             while (retirer_montant != 0) {
               if (retirer_montant >= 500){
                 println("Il reste " + retirer_montant + " CHF à distribuer")
                 billet_de_500 = retirer_montant/500
                 println("Vous pouvez obtenir au maximum " + billet_de_500 + " billet(s) de 500 CHF")
                 while (input != "o") {
                   print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                   input = readLine()
                   if (input != "o") {
                     // Si ce n'est pas égal à 'o', c'est une valeur
                     inferieur = input
                     while (inferieur.toInt > billet_de_500 || inferieur.toInt < 0) {
                       // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                       print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                       inferieur = readLine()
                       if(inferieur=="o"){
                         inferieur=(billet_de_500.toString)
                       }
                       else{

                       }
                     }
                     billet_de_500 = inferieur.toInt
                     retirer_montant -= billet_de_500 * 500
                     input="o"
                   } else {
                     // Sinon, on garde le reste de la division par 500
                     retirer_montant -= billet_de_500 * 500
                     input="o" // Pour sortir de la boucle
                   }
                 }
                 input="a"
               }
                if (retirer_montant >= 200){
                  println("Il reste " + retirer_montant + " CHF à distribuer")
                 billet_de_200 = retirer_montant/200
                  println("Vous pouvez obtenir au maximum " + billet_de_200 + " billet(s) de 200 CHF")
                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()
                     if (input != "o") {
                        // Si ce n'est pas égal à 'o', c'est une valeur
                        inferieur = input

                        while (inferieur.toInt > billet_de_200 || inferieur.toInt < 0) {
                          // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                          print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                          inferieur = readLine()
                          if(inferieur=="o"){
                            inferieur=(billet_de_200.toString)
                          }
                          else{

                          }
                        }

                        billet_de_200 = inferieur.toInt


                        retirer_montant -= billet_de_200 * 200
                        input="o"
                      } else {
                       // Sinon, on garde le reste de la division par 500
                        retirer_montant -= billet_de_200 * 200
                       input="o"
                     }
                   }
                  input="a"

               }
                 if (retirer_montant >= 100){
                   println("Il reste " + retirer_montant + " CHF à distribuer")
                 billet_de_100 = retirer_montant/100
                  println("Vous pouvez obtenir au maximum " + billet_de_100 + " billet(s) de 100 CHF")


                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()

                     if (input != "o") {
                        // Si ce n'est pas égal à 'o', c'est une valeur
                        inferieur = input

                        while (inferieur.toInt > billet_de_100 || inferieur.toInt < 0) {
                          // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                          print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                          inferieur = readLine()
                          if(inferieur=="o"){
                            inferieur=(billet_de_100.toString)
                          }
                          else{

                          }
                        }

                        billet_de_100 = inferieur.toInt


                        retirer_montant -= billet_de_100 * 100
                        input="o"
                      } else {
                       // Sinon, on garde le reste de la division par 100
                        retirer_montant -= billet_de_100 * 100    
                       input="o" // Pour sortir de la boucle
                     }
                   }
                   input="a"

               }
                 if (retirer_montant >= 50){
                   println("Il reste " + retirer_montant + " CHF à distribuer")
                 billet_de_50 = retirer_montant/50
                  println("Vous pouvez obtenir au maximum " + billet_de_50 + " billet(s) de 50 CHF")


                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()

                   if (input != "o") {
                      // Si ce n'est pas égal à 'o', c'est une valeur
                      inferieur = input

                      while (inferieur.toInt > billet_de_50 || inferieur.toInt < 0) {
                        // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                        print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                        inferieur = readLine()
                        if(inferieur=="o"){
                          inferieur=(billet_de_50.toString)
                        }
                        else{

                        }
                      }

                      billet_de_50 = inferieur.toInt


                      retirer_montant -= billet_de_50 * 50
                      input="o"
                    } else {
                       // Sinon, on garde le reste de la division par 50
                       retirer_montant %= 50
                       input="o" // Pour sortir de la boucle
                     }
                   }
                   input="a"

               }
                 if (retirer_montant >= 20){
                   println("Il reste " + retirer_montant + " CHF à distribuer")
                 billet_de_20 = retirer_montant/20
                  println("Vous pouvez obtenir au maximum " + billet_de_20 + " billet(s) de 20 CHF")


                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()

                   if (input != "o") {
                      // Si ce n'est pas égal à 'o', c'est une valeur
                      inferieur = input

                      while (inferieur.toInt > billet_de_20 || inferieur.toInt < 0) {
                        // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                        print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                        inferieur = readLine()
                        if(inferieur=="o"){
                          inferieur=(billet_de_20.toString)
                        }
                        else{

                        }
                      }

                      billet_de_20 = inferieur.toInt


                      retirer_montant -= billet_de_20 * 20
                      input="o"
                    } else {
                       // Sinon, on garde le reste de la division par 500
                       retirer_montant %= 20
                       input="o" // Pour sortir de la boucle
                     }
                   }
                   input="a"

               }
                 if (retirer_montant >= 10){
                 billet_de_10 = retirer_montant/10
                  retirer_montant = retirer_montant - (billet_de_10 * 10)

               }

               //affichage des billets pris en compte seulement si c'est >0
               if(billet_de_500>0){
                 println(billet_de_500 + " billet(s) de 500 CHF")
               }
               if(billet_de_200>0){
                 println(billet_de_200 + " billet(s) de 200 CHF")
               }
               if(billet_de_100>0){
                 println(billet_de_100 + " billet(s) de 100 CHF")
               }
               if(billet_de_50>0){
                 println(billet_de_50 + " billet(s) de 50 CHF")
               }
               if(billet_de_20>0){
                 println(billet_de_20 + " billet(s) de 20 CHF")
               }
               if(billet_de_10>0){
                 println(billet_de_10 + " billet(s) de 10 CHF")
               }
               printf("Votre retrait a été pris en compte,Le nouveau montant disponible sur votre compte est > %.2f chf \n", comptes(id))
             }
            }
            if (coupures==2){
              coupure_petite_indic=true;  //afin de retirer après
            }        
          }
          if(coupure_petite_indic || retirer_montant<200){
             while (retirer_montant!= 0){
                if (retirer_montant >= 100 ){
                  println("Il reste " + retirer_montant + " CHF à distribuer")
                  billet_de_100 = retirer_montant/100

                  println("Vous pouvez obtenir au maximum " + billet_de_100 + " billet(s) de 100 CHF")
                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()

                   if (input != "o") {
                      // Si ce n'est pas égal à 'o', c'est une valeur
                      inferieur = input

                      while (inferieur.toInt > billet_de_100 || inferieur.toInt < 0) {
                        // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                        print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                        inferieur = readLine()
                        if(inferieur=="o"){
                          inferieur=(billet_de_100.toString)
                        }
                        else{

                        }
                      }
                      billet_de_100 = inferieur.toInt
                      retirer_montant -= billet_de_100 * 100
                      input="o"
                    } else {
                       // Sinon, on garde le reste de la division par 100
                       retirer_montant %= 100
                       input="o"// Pour sortir de la boucle
                     }
                   }
                  input="a"
                }
                if (retirer_montant >= 50){
                  println("Il reste " + retirer_montant + " CHF à distribuer")
                  billet_de_50 = retirer_montant/50
                  println("Vous pouvez obtenir au maximum " + billet_de_50 + " billet(s) de 50 CHF")
                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()
                   if (input != "o") {
                      // Si ce n'est pas égal à 'o', c'est une valeur
                      inferieur = input

                      while (inferieur.toInt > billet_de_50 || inferieur.toInt < 0) {
                        // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                        print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                        inferieur = readLine()
                        if(inferieur=="o"){
                          inferieur=(billet_de_50.toString)
                        }
                        else{
                        }
                      }
                      billet_de_50 = inferieur.toInt
                      retirer_montant -= billet_de_50 * 50
                      input="o"
                    } else {
                       // Sinon, on garde le reste de la division par 500
                       retirer_montant %= 50
                       input="o" // Pour sortir de la boucle
                     }
                   }
                  input="a"
                }
                if (retirer_montant >= 20){
                  println("Il reste " + retirer_montant + " CHF à distribuer")
                  billet_de_20 = retirer_montant/20
                  println("Vous pouvez obtenir au maximum " + billet_de_20 + " billet(s) de 20 CHF")


                   while (input != "o") {
                     print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                     input = readLine()

                   if (input != "o") {
                      // Si ce n'est pas égal à 'o', c'est une valeur
                      inferieur = input

                      while (inferieur.toInt > billet_de_20 || inferieur.toInt < 0) {
                        // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                        print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                        inferieur = readLine()
                        if(inferieur=="o"){
                          inferieur=(billet_de_20.toString)
                        }
                        else{

                        }
                      }

                      billet_de_20 = inferieur.toInt


                      retirer_montant -= billet_de_20 * 20
                      input="o"
                    } else {
                       // Sinon, on garde le reste de la division par 500
                       retirer_montant %= 20
                       input="o"// Pour sortir de la boucle
                     }
                   }
                  input="a"
                }  
                if (retirer_montant >= 10){
                  billet_de_10 = retirer_montant/10
                  retirer_montant = retirer_montant - (billet_de_10 * 10)
                }

                  if (billet_de_100>0){
                    println(billet_de_100 + " billet(s) de 100 CHF ")
                  }
                  if (billet_de_50>0){
                    println(billet_de_50 + " billet(s) de 50 CHF ")
                  }
                  if (billet_de_20>0){
                    println(billet_de_20 + " billet(s) de 20 CHF " )
                  }
                  if (billet_de_10>0){
                    println(billet_de_10 + " billet(s) de 10 CHF ")
                  }

               printf("Votre retrait a été pris en compte,Le nouveau montant disponible sur votre compte est > %.2f chf \n", comptes(id))

              }   
            }
          }
          if (devise==2){
            while (retirer_montant!= 0){
              if (retirer_montant >= 100 ){
                println("Il reste " + retirer_montant + " CHF à distribuer")
                billet_de_100 = retirer_montant/100

                println("Vous pouvez obtenir au maximum " + billet_de_100 + " billet(s) de 100 CHF")


                 while (input != "o") {
                   print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                   input = readLine()

                 if (input != "o") {
                    // Si ce n'est pas égal à 'o', c'est une valeur
                    inferieur = input

                    while (inferieur.toInt > billet_de_100 || inferieur.toInt < 0) {
                      // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                      print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                      inferieur = readLine()
                      if(inferieur=="o"){
                        inferieur=(billet_de_100.toString)
                      }
                      else{

                      }
                    }

                    billet_de_100 = inferieur.toInt


                    retirer_montant -= billet_de_100 * 100
                    input="o"
                  } else {
                     // Sinon, on garde le reste de la division par 500
                     retirer_montant %= 100
                     input="o" // Pour sortir de la boucle
                   }
                 }
                input="a"
              }

              if (retirer_montant >= 50){
                println("Il reste " + retirer_montant + " CHF à distribuer")
                billet_de_50 = retirer_montant/50
                println("Vous pouvez obtenir au maximum " + billet_de_50 + " billet(s) de 50 CHF")


                 while (input != "o") {
                   print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                   input = readLine()

                 if (input != "o") {
                    // Si ce n'est pas égal à 'o', c'est une valeur
                    inferieur = input

                    while (inferieur.toInt > billet_de_50 || inferieur.toInt < 0) {
                      // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                      print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                      inferieur = readLine()
                      if(inferieur=="o"){
                        inferieur=(billet_de_50.toString)
                      }
                      else{

                      }
                    }

                    billet_de_50 = inferieur.toInt


                    retirer_montant -= billet_de_50 * 50
                    input="o"
                  } else {
                     // Sinon, on garde le reste de la division par 500
                     retirer_montant %= 50
                     input="o" // Pour sortir de la boucle
                   }
                 }
                input="a"
              }
              if (retirer_montant >= 20){
                println("Il reste " + retirer_montant + " CHF à distribuer")
                billet_de_20 = retirer_montant/20
                println("Vous pouvez obtenir au maximum " + billet_de_20 + " billet(s) de 20 CHF")


                 while (input != "o") {
                   print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                   input = readLine()

                 if (input != "o") {
                    // Si ce n'est pas égal à 'o', c'est une valeur
                    inferieur = input

                    while (inferieur.toInt > billet_de_20 || inferieur.toInt < 0) {
                      // Vérification que la saisie n'ne soit pas négative et pas supérieure au maximum de billets possible
                      print("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
                      inferieur = readLine()
                      if(inferieur=="o"){
                        inferieur=(billet_de_20.toString)
                      }
                      else{

                      }
                    }

                    billet_de_20 = inferieur.toInt


                    retirer_montant -= billet_de_20 * 20
                    input="o"
                  } else {
                     // Sinon, on garde le reste de la division par 500
                     retirer_montant %= 20
                     input="o" // Pour sortir de la boucle
                   }
                 }
                input="a"
              }  
              if (retirer_montant >= 10){
                billet_de_10 = retirer_montant/10
                retirer_montant = retirer_montant - (billet_de_10 * 10)
              }

                if (billet_de_100>0){
                  println(billet_de_100 + " billet(s) de 100 CHF ")
                }
                if (billet_de_50>0){
                  println(billet_de_50 + " billet(s) de 50 CHF ")
                }
                if (billet_de_20>0){
                  println(billet_de_20 + " billet(s) de 20 CHF " )
                }
                if (billet_de_10>0){
                  println(billet_de_10 + " billet(s) de 10 CHF ")
                }

               printf("Votre retrait a été pris en compte,Le nouveau montant disponible sur votre compte est > %.2f chf \n", comptes(id))
            }
          }
  }
  
  def depot(id : Int, comptes : Array[Double]) : Unit = 
  {
    //demander la devise tant qu'elle n'est pas bonne
    var choixDevise=0
    do
    {
      choixDevise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt

      if(choixDevise != 2 && choixDevise != 1){
        println("Opération non valide ")
      }

    }while (choixDevise != 1 && choixDevise != 2)

    //demander le moontant tant qu'il n'est pas bon
    var depotvaleur=0
    do
    {

      depotvaleur = readLine("Indiquez le montant du dépôt > ").toInt

      if(depotvaleur<0)
      {
        println("Le montant doit être positif")
      }
      else if(depotvaleur % 10 != 0){
        println("Le montant doit être un multiple de 10")
      }

    }
    while (depotvaleur % 10 != 0 || depotvaleur < 0)



    // Si le client veut déposer en CHF
    if(choixDevise == 1) {

      comptes(id) = comptes(id) + depotvaleur

    } else { // Si le client veut déposer en EUR


        //conversion en euro
      comptes(id) = comptes(id) + (depotvaleur.toDouble)*0.95


    }

    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > %.2f", comptes(id))
    println(" CHF")
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = 
  {  //fonction de l'option 4 pour changer de pin
    var motdepasse:String = ""  //Déclaration d'un nouveau pin = ""
    do{  //demander le mdp tant que il ne respecte pas la condition <8
      motdepasse=readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      if(motdepasse.length<8){  //si la taille de mot de passe <8, dire qu'il y a un problème
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
      else{
        codespin(id)=motdepasse  //sinon si c'est >=, changer le mot de passe
      }
    }while(motdepasse!=codespin(id))
    
  }
}

