//Assignment: Malika Célia Gobet_996389_assignsubmission_file

import io.StdIn._
 object Main {
   def depot(identifiant : Int, comptes : Array[Double]) : Unit = {
     var operation = 1
     var devise = 0
     var montantdepot = 0
     var valeurCHF = 0.0
     var valeurEUR = 0.0
     // ______________________________________

      //Début dépôt
                devise = readLine("Indiquez la devise du dépôt: 1)CHF ; 2)EUR >").toInt

                while(!(devise == 1) && !(devise == 2)) {
                  println("  ")
                  devise = readLine("Devise pas connue. Veuillez indiquer une devise; 1 ou 2. >").toInt
                }

                    if(devise == 2) {
                      println("  ")
                      montantdepot = readLine("Indiquez le montant du dépôt >").toInt

                      while(montantdepot < 10 || !(montantdepot % 10 == 0)) { 
                         montantdepot = readLine("Le montant doit être un multiple de 10. Veuillez indiquer un nouveau montant >").toInt
                      }

                      valeurCHF = montantdepot*0.95
                      comptes(identifiant) += valeurCHF
                      println("  ")
                      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(identifiant) + "CHF")
                    } else { 

                      montantdepot = readLine("Indiquez le montant du dépôt >").toInt

                      while(montantdepot < 10 || !(montantdepot % 10 == 0)) { 
                         println("  ")
                         montantdepot = readLine("Le montant doit être un multiple de 10. Veuillez indiquer un nouveau montant >").toInt
                      }

                      valeurEUR = valeurCHF*1.05
                      comptes(identifiant) += montantdepot
                      println("  ")
                      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(identifiant) + "CHF")
                    }

      //fin dépôt
   } 
   //______________________________________________
   def retrait(identifiant : Int, comptes : Array[Double]) : Unit = {
      var valeurCHF = 0.0
      var valeurEUR = 0.0
      var devise = 0
      var montantretrait = 0
      var plafond = 0.0
      var choixcoupures = 2
      var billet500CHF = 0
      var billet200CHF = 0
      var billet100CHF = 0
      var billet50CHF = 0
      var billet20CHF = 0
      var billet10CHF = 0
      var billet100EUR = 0
      var billet50EUR = 0
      var billet20EUR = 0
      var billet10EUR = 0
      var grossecoupure = true
      var reste = 0
      var validationclient = "o"
      var x = 0
     //Début retrait
                                    valeurCHF = valeurEUR*0.95
                                    valeurEUR = valeurCHF*1.05

                                    devise = readLine("Indiquez la devise du retrait: 1)CHF ; 2)EUR >").toInt

                                    while(!(devise == 1) && !(devise == 2)) {
                                      println("  ")
                                      devise = readLine("Devise pas connue. Veuillez indiquer une devise; 1 ou 2. >").toInt
                                    }

                                    //retrait en CHF ____________

                                    if(devise == 1) {

                                    println("  ")
                                    montantretrait = readLine("Indiquez le montant du retrait > ").toInt

                                    plafond = comptes(identifiant)*10/100

                                    while(montantretrait < 10 || !(montantretrait % 10 == 0) || (montantretrait > plafond)) { 
                                      if(montantretrait < 10 || !(montantretrait % 10 == 0)){
                                        println("  ")
                                        montantretrait = readLine("Le montant doit être un multiple de 10. Veuillez indiquer un nouveau montant > ").toInt
                                      } 
                                      else { 
                                        if(!(montantretrait <= plafond)){
                                          println("  ")
                                        printf("Votre plafond de retrait autorisé est de : %.2f \n ", plafond)
                                        montantretrait = readLine("Indiquez une nouveau montant > ").toInt
                                      } else { }
                                      }
                                    }
                                    comptes(identifiant)-= montantretrait

                                    if(montantretrait <= 200){
                                      println("Le retrait se fera en petite coupure.")
                                    }
                                    else {
                                      println("  ")
                                   choixcoupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt

                                    while(!(choixcoupures == 1) && !(choixcoupures == 2)) {
                                      println("  ")
                                      choixcoupures = readLine("Choix incorrect. Veuillez indiquer le choix de coupures; 1 ou 2. >").toInt
                                    }
                                    }
                                  if(choixcoupures == 1) {
                                    grossecoupure = true 
                                  }
                                    else { grossecoupure = false }

                                  if(grossecoupure == true){
                                    reste = montantretrait 

                                    billet500CHF = reste/500
                                    reste -= billet500CHF*500


                                    if(!(billet500CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet500CHF + " billet(s) de 500 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet500CHF)){
                                         validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }
                                    }

                                      if(validationclient == "o" ){
                                        reste = montantretrait 
                                        billet500CHF = reste/500
                                        montantretrait -= (500*billet500CHF)
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*500)
                                        reste = montantretrait 
                                        billet500CHF = x
                                      }


                                    reste = montantretrait 

                                      billet200CHF = reste/200
                                      reste -= billet200CHF*200

                                    if(!(billet200CHF == 0)){

                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet200CHF + " billet(s) de 200 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet200CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o" ){
                                        reste = montantretrait 
                                        billet200CHF = reste/200
                                        montantretrait -= (200*billet200CHF)
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*200)
                                        reste = montantretrait 
                                        billet200CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet100CHF = reste/100
                                    reste -= billet100CHF*100

                                    if(!(billet100CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet100CHF + " billet(s) de 100 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet100CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o" ){
                                        reste = montantretrait 
                                        billet100CHF = reste/100
                                        montantretrait -= (100*billet100CHF)
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*100)
                                        reste = montantretrait 
                                        billet100CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet50CHF = reste/50
                                    reste -= billet50CHF*50

                                    if(!(billet50CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet50CHF + " billet(s) de 50 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet50CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o"){
                                        reste = montantretrait 
                                        billet50CHF = reste/50
                                        montantretrait -= (50*billet50CHF) 
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*50)
                                        reste = montantretrait 
                                        billet50CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet20CHF = reste/20
                                    reste -= billet20CHF*20

                                    if(!(billet20CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet20CHF + " billet(s) de 20 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet20CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o"){
                                        reste = montantretrait 
                                        billet20CHF = reste/20
                                        montantretrait -= (20*billet20CHF)
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*20)
                                        reste = montantretrait 
                                        billet20CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet10CHF = reste/10
                                    reste -= billet10CHF*10

                                    if(!(billet10CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet10CHF + " billet(s) de 10 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet10CHF)){
                                          validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o"){
                                        reste = montantretrait 
                                        billet10CHF = reste/10
                                        montantretrait -= (10*billet10CHF) 
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        reste = montantretrait 
                                        montantretrait -= (x*10)
                                        billet10CHF = x
                                      }
                                    }

                                    if(montantretrait == 0){
                                      println("  ")
                                      println("Veuillez retirer la somme demandée :")
                                    }

                                      if(billet500CHF > 0){
                                        println(billet500CHF + " billet(s) de 500 CHF")
                                      }
                                        if(billet200CHF > 0){
                                          println(billet200CHF + " billet(s) de 200 CHF")
                                        }
                                        if(billet100CHF > 0){
                                           println(billet100CHF + " billet(s) de 100 CHF ")
                                          }
                                        if(billet50CHF > 0){
                                           println(billet50CHF + " billet(s) de 50 CHF ")
                                            }
                                        if(billet20CHF > 0){
                                           println(billet20CHF + " billet(s) de 20 CHF ")
                                            }
                                        if(billet10CHF > 0){
                                          println(billet10CHF + " billet(s) de 10 CHF ")
                                        }


                                  comptes(identifiant) -= montantretrait
                                  }

                                  if(choixcoupures == 2){
                                    grossecoupure == false

                                    reste = montantretrait 

                                    billet100CHF = reste/100
                                    reste -= billet100CHF*100

                                    if(!(billet100CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet100CHF + " billet(s) de 100 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet100CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o" ){
                                        reste = montantretrait 
                                        billet100CHF = reste/100
                                        montantretrait -= (100*billet100CHF)
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*100)
                                        reste = montantretrait 
                                        billet100CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet50CHF = reste/50
                                    reste -= billet50CHF*50

                                    if(!(billet50CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet50CHF + " billet(s) de 50 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet50CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o"){
                                        reste = montantretrait 
                                        billet50CHF = reste/50
                                        montantretrait -= (50*billet50CHF) 
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*50)
                                        reste = montantretrait 
                                        billet50CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet20CHF = reste/20
                                    reste -= billet20CHF*20

                                    if(!(billet20CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet20CHF + " billet(s) de 20 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet20CHF)){
                                           validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o"){
                                        reste = montantretrait 
                                        billet20CHF = reste/20
                                        montantretrait -= (20*billet20CHF)
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        montantretrait -= (x*20)
                                        reste = montantretrait 
                                        billet20CHF = x
                                      }

                                    }
                                    reste = montantretrait 

                                    billet10CHF = reste/10
                                    reste -= billet10CHF*10

                                    if(!(billet10CHF == 0)){
                                      println("  ")
                                      println("Il reste " + montantretrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billet10CHF + " billet(s) de 10 CHF")
                                      println("  ")
                                      validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                      while(!(validationclient == "o") && (x >= billet10CHF)){
                                          validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                      }

                                      if(validationclient == "o"){
                                        reste = montantretrait 
                                        billet10CHF = reste/10
                                        montantretrait -= (10*billet10CHF) 
                                      }else{
                                        var x = String.valueOf(validationclient).toInt
                                        reste = montantretrait 
                                        montantretrait -= (x*10)
                                        billet10CHF = x
                                      }
                                    }

                                    if(montantretrait == 0){
                                      println("  ")
                                      println("Veuillez retirer la somme demandée :")
                                    }
                                        if(billet100CHF > 0){
                                           println(billet100CHF + " billet(s) de 100 CHF ")
                                          }
                                        if(billet50CHF > 0){
                                           println(billet50CHF + " billet(s) de 50 CHF ")
                                            }
                                        if(billet20CHF > 0){
                                           println(billet20CHF + " billet(s) de 20 CHF ")
                                            }
                                        if(billet10CHF > 0){
                                          println(billet10CHF + " billet(s) de 10 CHF ")
                                        }

                                    println("  ")
                                    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(identifiant))
                                    comptes(identifiant) -= montantretrait
                                  }

                               } //fin retrait CHF

                                    //retrait en EUR ____________
                                    if(devise == 2){

                                      println("  ")
                                        montantretrait = readLine("Indiquez le montant du retrait > ").toInt

                                        plafond = comptes(identifiant)*0.1/0.95

                                        while(montantretrait < 10 || !(montantretrait % 10 == 0) || (montantretrait > plafond)) { 
                                          if(montantretrait < 10 || !(montantretrait % 10 == 0)){
                                            println("  ")
                                            montantretrait = readLine("Le montant doit être un multiple de 10. Veuillez indiquer un nouveau montant > ").toInt
                                          } 
                                          else { 
                                            println("  ")
                                            if(!(montantretrait <= plafond)){
                                            printf("Votre plafond de retrait autorisé est de : %.2f \n ", plafond )
                                            montantretrait = readLine("Indiquez une nouveau montant > ").toInt
                                          } 
                                          }
                                        }
                                      comptes(identifiant) -= montantretrait*0.95

                                      reste = montantretrait 

                                        billet100EUR = reste/100
                                        reste -= billet100EUR*100

                                        if(!(billet100EUR == 0)){
                                          println("  ")
                                          println("Il reste " + montantretrait + " EUR à distribuer \n Vous pouvez obtenir au maximum " + billet100EUR + " billet(s) de 100 EUR")
                                          println("  ")
                                          validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                          while(!(validationclient == "o") && (x >= billet100EUR)){
                                               validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                          }

                                          if(validationclient == "o" ){
                                            reste = montantretrait 
                                            billet100EUR = reste/100
                                            montantretrait -= (100*billet100EUR)
                                          }else{
                                            var x = String.valueOf(validationclient).toInt
                                            montantretrait -= (x*100)
                                            reste = montantretrait 
                                            billet100EUR = x
                                          }

                                        }
                                        reste = montantretrait 

                                        billet50EUR = reste/50
                                        reste -= billet50EUR*50

                                        if(!(billet50EUR == 0)){
                                          println("  ")
                                          println("Il reste " + montantretrait + " EUR à distribuer \n Vous pouvez obtenir au maximum " + billet50EUR + " billet(s) de 50 EUR")
                                          println("  ")
                                          validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                          while(!(validationclient == "o") && (x >= billet50EUR)){
                                               validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                          }

                                          if(validationclient == "o"){
                                            reste = montantretrait 
                                            billet50EUR = reste/50
                                            montantretrait -= (50*billet50EUR) 
                                          }else{
                                            var x = String.valueOf(validationclient).toInt
                                            montantretrait -= (x*50)
                                            reste = montantretrait 
                                            billet50EUR = x
                                          }

                                        }
                                        reste = montantretrait 

                                        billet20EUR = reste/20
                                        reste -= billet20EUR*20

                                        if(!(billet20EUR == 0)){
                                          println("  ")
                                          println("Il reste " + montantretrait + " EUR à distribuer \n Vous pouvez obtenir au maximum " + billet20EUR + " billet(s) de 20 EUR")
                                          println("  ")
                                          validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                          while(!(validationclient == "o") && (x >= billet20EUR)){
                                               validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                          }

                                          if(validationclient == "o"){
                                            reste = montantretrait 
                                            billet20EUR = reste/20
                                            montantretrait -= (20*billet20EUR)
                                          }else{
                                            var x = String.valueOf(validationclient).toInt
                                            montantretrait -= (x*20)
                                            reste = montantretrait 
                                            billet20EUR = x
                                          }

                                        }
                                        reste = montantretrait 

                                        billet10EUR = reste/10
                                        reste -= billet10CHF*10

                                        if(!(billet10EUR == 0)){
                                          println("  ")
                                          println("Il reste " + montantretrait + " EUR à distribuer \n Vous pouvez obtenir au maximum " + billet10EUR + " billet(s) de 10 EUR")
                                          println("  ")
                                          validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

                                          while(!(validationclient == "o") && (x >= billet10EUR)){
                                              validationclient = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
                                          }

                                          if(validationclient == "o"){
                                            reste = montantretrait 
                                            billet10EUR = reste/10
                                            montantretrait -= (10*billet10EUR)
                                          }else{
                                            var x = String.valueOf(validationclient).toInt
                                            reste = montantretrait 
                                            montantretrait -= (x*10)
                                            billet10EUR = x
                                          }
                                        }

                                        if(montantretrait == 0){
                                          println("  ")
                                          println("Veuillez retirer la somme demandée :")
                                        }
                                            if(billet100EUR > 0){
                                               println(billet100EUR + " billet(s) de 100 EUR ")
                                                }
                                            if(billet50EUR > 0){
                                               println(billet50EUR + " billet(s) de 50 EUR ")
                                                }
                                            if(billet20EUR > 0){
                                               println(billet20EUR + " billet(s) de 20 EUR ")
                                                }
                                            if(billet10EUR > 0){
                                              println(billet10EUR + " billet(s) de 10 EUR ")
                                            }

                                      println("  ")
                                      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(identifiant))
                                      comptes(identifiant) -= montantretrait
                                      }
      // ______________________________________
   }
   def changepin(identifiant : Int, codespin : Array[String]) : Unit = {
     var newcode = "Bonjour1234"
     
     println("  ")
        newcode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
     while(newcode.length < 8){
       println("  ")
       println("Votre code pin ne contient pas au moins 8 caractères")
       newcode = readLine("Veuillez saisir un nouveau code pin (il doit contenir au moins 8 caractères) > ")
     }
        println("  ")
        println("Votre changement de code a été succesivement pris en compte.")
        codespin(identifiant) = newcode
     
   }
   def main(args: Array[String]): Unit = {
     var operation = 1
     var nbessai = 3
     var montantcompte = 1200.0
     var montantretrait = 0
     var montantdepot = 0
     var x = 0
     var nbclients = 100
     var comptes = Array.fill(100)(1200.0)
     var codespin = Array.fill(100)("INTRO1234")
     var identifiant = 0
     var a = true
     var idetcodeok = false
     var tentativeid = 0
     var programme = false
     var newcode = "Bonjour1234"
     var pin = "Hallo"
     
// ______________________________________
//identifiant
    while(programme == false ){
      
     while(idetcodeok == false){
       tentativeid += 1
       nbessai = tentativeid*3
       println("  ")
     identifiant = readLine("Saissisez votre identifiant > ").toInt
       nbclients -= 1
       
       if(identifiant <= nbclients){
         println("  ")
         println("Cet identifiant est correct.")
       }
     
         if(!(identifiant <= nbclients)){
          println("  ")
          println("Cet identifiant est incorrect.")
           System.exit(0)
         }
       
     // ______________________________________
      //Code PIN a 
     println("  ")
     pin = readLine("Saissisez votre code pin > ")
      nbessai = nbessai/tentativeid
       
              while(nbessai > 1 || a == false){
                nbessai = nbessai - 1
                if(pin == codespin(identifiant)) { 
                 println("Le code est bon.")
                  a = true
                  idetcodeok = true
                 } else {
                if(pin != codespin(identifiant)){
                println("  ")
                  println("Code pin erroné, il vous reste " + nbessai + " tentavives.")
                  pin = readLine("Saissisez votre code pin > ")

                  if (nbessai == 1) {
                    if(pin == codespin(identifiant)) { 
                       println("Le code est bon.")
                        a = true
                        idetcodeok = true
                    } else {
                      if(pin != codespin(identifiant)){
                     println("  ")
                     println("Trop d’erreurs, abandon de l’identification")
                    a = true
                      }
                    }
                   }
                  }
                }
              }
     } 
      operation = 1
      //a
// ______________________________________
//Menu 
    while (!(operation == 5)) {
      println("  ")
      operation = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : >").toInt

      while(!(operation == 1) && !(operation == 2) && !(operation == 3) && !(operation == 4) && !(operation == 5)){
        println("  ")
        operation = readLine("Choix Incorrect. \n Veuillez réessayer. >").toInt
      }
    

// ______________________________________
         if (operation == 1) { 
        depot(identifiant, comptes)
         }
     
           if (operation == 2) {
// ______________________________________
//Début retrait
             retrait(identifiant, comptes)
 // ______________________________________
//Fin retrait
                       } else {
                         if (operation == 3) {
// ______________________________________
//Début consultation
                           println("  ")
                           printf("Le montant disponible sur votre compte est de : %.2f \n ", comptes(identifiant))
                          }
                          
               }
// ______________________________________
//Début changement de code
                           if (operation == 4){
                             changepin(identifiant, codespin)
                        } 
                             
                        
                    
//Fin changement de code 
                       
                      if(operation == 5) {
      println("  ")
       println("Fin des opérations, n’oubliez pas de récupérer votre carte.")}

                   }
             //fin while(!(operation == 5))
      idetcodeok = false
      a = false
      tentativeid = 0
     }
  //fin boucle while(programme)
   }
  }
 