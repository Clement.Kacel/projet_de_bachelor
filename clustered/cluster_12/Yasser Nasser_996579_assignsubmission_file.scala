//Assignment: Yasser Nasser_996579_assignsubmission_file

import io.StdIn._
import scala.math._
object Main {
  def main(args: Array[String]): Unit = {

    val nbClients = 100 // nombres de clients
    var comptes: Array[Double] = Array.fill(nbClients)(1200.00) // tableau des comptes
    var codePin: Array[String] = Array.fill(nbClients)("INTRO1234")
    
    var continuer = true
    var toujoursContinuer = true
    var terminer = false

    //Billets coupures
    var billets500 = 0.0
    var billets200 = 0.0
    var billets100 = 0.0
    var billets50 = 0.0
    var billets20 = 0.0
    var billets10 = 0.0

      while (continuer) {
        // Demande d'identification et véérification du code Pin
        var codePinSaisi = ""
        var codePinCorrect = false
        var tentatives = 0
        var tentativesRestantes = 3

        print("\nSaisissez votre identifiant client : ")
        var idClient = readInt()
        if (idClient >= nbClients || idClient < 0){
          println("Cet identifiant n'est pas valable")
          sys.exit()
        }

        while (tentatives < tentativesRestantes && codePinSaisi != codePin(idClient)) {

          print("\nSaisissez votre code PIN : ")
          codePinSaisi = readLine()
          if (codePinSaisi == codePin(idClient)) {
            codePinCorrect = true
            println("\nCode PIN correct")
            toujoursContinuer = true
          } else {
            tentatives += 1
            println("\nCode PIN erroné, il vous reste " + 
            (tentativesRestantes - tentatives) + " tentatives")
          }
          if (tentatives == tentativesRestantes) {
            println("Trop d'erreurs abandon de l'identification. ")
            tentatives = 0
            println("\nSaisissez votre identifiant client : ")
            var idClient = readInt()
            if (idClient > nbClients || idClient < 0){
              println("\nCet identifiant n'est pas valable")
              sys.exit()
            }

          }
        }

        var montantDispo = comptes(idClient)

        //Page d'acceuil
        while(toujoursContinuer){
          
        println("\nChoisissez l'opération que vous souhaitez réaliser: ")
        println("\n1 : Dépôt")
        println("2 : Retrait")
        println("3 : Consultation du compte")
        println("4 : Changement du code PIN")
        println("5 : Terminer")
        print("\nVotre choix : ")

        var choix = readInt()
        while(choix != 1 && choix != 2 && choix != 3 && choix != 4 && choix != 5){ //Permet de vérifier que le choix est possible
          println("\nOpération impossible, veuillez choisir parmi les opérations possibles.")
          choix = readInt()
        }

        //Opération de Dépôt
        if (choix == 1) {
          // Création de la méthode Dépôt
          def depot(id : Int, comptes : Array[Double]) : Unit = {
          println("\nIndiquez la devise du dépot : ")
          println("\n1 : CHF")
          println("2 : EUR")

          var deviseDepot = readInt()
          while(deviseDepot != 1 && deviseDepot != 2){
            println("\nOpération impossible, veuillez choisir parmi les devises possibles.")
            deviseDepot = readInt()
          }
          print("\nIndiquez le montant du dépot : ")

          var montantDepot = readDouble()

          while(montantDepot < 0){
            println("\nOpération impossible, veuillez choisir un montant positif")
            montantDepot = readDouble()
          }

          while ((montantDepot % 10) != 0) {
            println("\nLe montant de dépôt doit être un multiple de 10 pour être valide.")
            println("Indiquez le montant de dépôt : ")
            montantDepot = readDouble()
          }

          if (deviseDepot == 2) {
            montantDepot = (montantDepot * 0.95)
          }

          montantDispo += montantDepot
          printf("\nVous avez déposé : %.2f CHF.\n", montantDepot)
          printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", montantDispo)
          }
          // Utilisation de la méthode dépot
          depot(idClient, comptes)

          //Opération de retrait
        } else if (choix == 2) {
          // Création de la méthode Retrait
          def retrait(id : Int, comptes : Array[Double]) : Unit = {
          println("\nIndiquez la devise de retrait : ")
          println("\n1 : CHF ")
          println("2 : EUR ")
          var deviseRetrait = readInt()
          var affichage = "CHF"
          var affichageEUR = "EUR"
          while(deviseRetrait != 1 && deviseRetrait != 2){
            println("\nOpération impossible, veuillez choisir parmi les devises possibles.")
            deviseRetrait = readInt()
          }
          print("\nIndiquez le montant que vous souhaitez retirer : ")
          var montantRetrait = readDouble()

            do {
              if ((montantRetrait % 10) != 0) {
                println("\nLe montant doit être un multiple de 10 pour être valide.")
                print("Indiquez à nouveau le montant que vous souhaitez retirer : ")
                montantRetrait = readDouble()
              } else if (montantRetrait > montantDispo * 0.1) {
                printf("\nVotre plafond de retrait autorisé est de : %.2f CHF\n", montantDispo * 0.1)
                print("Indiquez à nouveau le montant que vous souhaitez retirer : ")
                montantRetrait = readDouble()
              }
            } while ((montantRetrait % 10) != 0 || montantRetrait > montantDispo * 0.1)

          var reste = montantRetrait // valeur qu'on utilise pour les coupures

          if (reste >= 200 && deviseRetrait == 1) {
            println ("\nVeuillez choisir les coupures que vous souhaitez : ")
            println("1 : Grosse coupures ")
            println("2 : Petites coupures ")
            var coupure = readInt()

            while(coupure != 1 && coupure !=2){
              print("\nOpération impossible veuillez choisir parmi les types de coupures possibles : ")
              coupure = readInt()
            }

            if (coupure == 1){

              //Traitement billets de 500
              if (reste >= 500 ){
                var reste1 = reste
                billets500 = reste / 500
                billets500 = billets500.toInt
                reste = reste % 500
                printf("\nIl reste " + reste1 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets500 + " billet(s) de 500 CHF")
                print("\nTapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste1 - (billets500 * 500))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets500){
                    print("\nSaisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets500 = choixUtilisateur
                  reste = (reste1 - (billets500 * 500))
                }

                print("\nVous avez retiré " + billets500 + " billet(s) de 500 CHF")
              }

              //Traitement billets de 200
              if (reste >= 200 ){
                var reste2 = reste
                billets200 = reste / 200
                billets200 = billets200.toInt
                reste = reste % 200
                println("\nIl reste " + reste2 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets200 + " billet(s) de 200 CHF")
                print("\nTapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste2 - (billets200 * 200))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets200){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets200 = choixUtilisateur
                  reste = (reste2 - (billets200 * 200))
                }

                println("Vous avez retiré " + billets200 + " billet(s) de 200 CHF")
              }

              //Traitement billets de 100 
              if (reste >= 100 ){
                var reste3 = reste
                billets100 = reste / 100
                billets100 = billets100.toInt
                reste = reste % 100
                println("Il reste " + reste3 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste3 - (billets100 * 100))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets100){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets100 = choixUtilisateur
                  reste = (reste3 - (billets100 * 100))
                }

                println("Vous avez retiré " + billets100 + " billet(s) de 100 CHF")
              }
              //Traitement billets de 50
              if (reste >= 50 ){
                var reste4 = reste
                billets50 = reste / 50
                billets50 = billets50.toInt
                reste = reste % 50
                println("Il reste " + reste4 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste4 - (billets50 * 50))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets50){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets50 = choixUtilisateur
                  reste = (reste4 - (billets50 * 50))
                }

                println("Vous avez retiré " + billets50 + " billet(s) de 50 CHF")
              }
              //Traitement billets de 20
              if (reste >= 20 ){
                var reste6 = reste
                billets20 = reste / 20
                billets20 = billets20.toInt
                reste = reste % 20
                println("Il reste " + reste6 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste6 - (billets20 * 20))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets20){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets20 = choixUtilisateur
                  reste = (reste6 - (billets20 * 20))
                }

                println("Vous avez retiré " + billets20 + " billet(s) de 20 CHF")
              }
              //Traitement billets de 10
              if (reste >= 10 ){
                var reste5 = reste
                billets10 = reste / 10
                billets10 = billets10.toInt
                reste = reste % 10
                println("Il reste " + reste5 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets10 + " billet(s) de 10 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste5 - (billets10 * 10))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets10){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  reste = (reste5 - (billets10 * 10))
                }

                println("Vous avez retiré " + billets10 + " billet(s) de 10 CHF")
              }

            } else { //Petites coupures choisi dans un retrait de plus de 200 CHF
              //Traitement billets de 100 
              if (reste >= 100 ){
                var reste3 = reste
                billets100 = reste / 100
                billets100 = billets100.toInt
                reste = reste % 100
                println("Il reste " + reste3 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste3 - (billets100 * 100))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets100){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets100 = choixUtilisateur
                  reste = (reste3 - (billets100 * 100))
                }

                println("Vous avez retiré " + billets100 + " billet(s) de 100 CHF")
              }
              //Traitement billets de 50
              if (reste >= 50 ){
                var reste4 = reste
                billets50 = reste / 50
                billets50 = billets50.toInt
                reste = reste % 50
                println("Il reste " + reste4 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste4 - (billets50 * 50))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets50){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets50 = choixUtilisateur
                  reste = (reste4 - (billets50 * 50))
                }

                println("Vous avez retiré " + billets50 + " billet(s) de 50 CHF")
              }
              //Traitement billets de 20
              if (reste >= 20 ){
                var reste6 = reste
                billets20 = reste / 20
                billets20 = billets20.toInt
                reste = reste % 20
                println("Il reste " + reste6 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste6 - (billets20 * 20))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets20){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  billets20 = choixUtilisateur
                  reste = (reste6 - (billets20 * 20))
                }

                println("Vous avez retiré " + billets20 + " billet(s) de 20 CHF")
              }
              //Traitement billets de 10
              if (reste >= 10 ){
                var reste5 = reste
                billets10 = reste / 10
                billets10 = billets10.toInt
                reste = reste % 10
                println("Il reste " + reste5 + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + billets10 + " billet(s) de 10 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
                var choixCoupure = readLine()

                //Vérification du choix de coupure
                if (choixCoupure.toLowerCase() == "o") {
                  reste = (reste5 - (billets10 * 10))
                } else {
                  var choixUtilisateur = choixCoupure.toInt
                  while (choixUtilisateur >= billets10){
                    println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                    choixUtilisateur = readInt()
                  }
                  reste = (reste5 - (billets10 * 10))
                }

                println("Vous avez retiré " + billets10 + " billet(s) de 10 CHF")
              }




            }

          } else if (reste < 200 && deviseRetrait == 1) { // Retrait en CHF de moin de 200 CHF
            //Traitement billets de 100 
            if (reste >= 100 ){
              var reste3 = reste
              billets100 = reste / 100
              billets100 = billets100.toInt
              reste = reste % 100
              println("Il reste " + reste3 + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
              var choixCoupure = readLine()

              //Vérification du choix de coupure
              if (choixCoupure.toLowerCase() == "o") {
                reste = (reste3 - (billets100 * 100))
              } else {
                var choixUtilisateur = choixCoupure.toInt
                while (choixUtilisateur >= billets100){
                  println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                  choixUtilisateur = readInt()
                }
                billets100 = choixUtilisateur
                reste = (reste3 - (billets100 * 100))
              }

              println("Vous avez retiré " + billets100 + " billet(s) de 100 CHF")
            }
            //Traitement billets de 50
            if (reste >= 50 ){
              var reste4 = reste
              billets50 = reste / 50
              billets50 = billets50.toInt
              reste = reste % 50
              println("Il reste " + reste4 + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
              var choixCoupure = readLine()

              //Vérification du choix de coupure
              if (choixCoupure.toLowerCase() == "o") {
                reste = (reste4 - (billets50 * 50))
              } else {
                var choixUtilisateur = choixCoupure.toInt
                while (choixUtilisateur >= billets50){
                  println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                  choixUtilisateur = readInt()
                }
                billets50 = choixUtilisateur
                reste = (reste4 - (billets50 * 50))
              }

              println("Vous avez retiré " + billets50 + " billet(s) de 50 CHF")
            }
            //Traitement billets de 20
            if (reste >= 20 ){
              var reste6 = reste
              billets20 = reste / 20
              billets20 = billets20.toInt
              reste = reste % 20
              println("Il reste " + reste6 + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
              var choixCoupure = readLine()

              //Vérification du choix de coupure
              if (choixCoupure.toLowerCase() == "o") {
                reste = (reste6 - (billets20 * 20))
              } else {
                var choixUtilisateur = choixCoupure.toInt
                while (choixUtilisateur >= billets20){
                  println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                  choixUtilisateur = readInt()
                }
                billets20 = choixUtilisateur
                reste = (reste6 - (billets20 * 20))
              }

              println("Vous avez retiré " + billets20 + " billet(s) de 20 CHF")
            }
            //Traitement billets de 10
            if (reste >= 10 ){
              var reste5 = reste
              billets10 = reste / 10
              billets10 = billets10.toInt
              reste = reste % 10
              println("Il reste " + reste5 + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + billets10 + " billet(s) de 10 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
              var choixCoupure = readLine()

              //Vérification du choix de coupure
              if (choixCoupure.toLowerCase() == "o") {
                reste = (reste5 - (billets10 * 10))
              } else {
                var choixUtilisateur = choixCoupure.toInt
                while (choixUtilisateur >= billets10){
                  println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                  choixUtilisateur = readInt()
                }
                reste = (reste5 - (billets10 * 10))
              }

              println("Vous avez retiré " + billets10 + " billet(s) de 10 CHF")
            }
          } else if (deviseRetrait == 2) {
          //Traitement billets de 100 
          if (reste >= 100 ){
            var reste3 = reste
            billets100 = reste / 100
            billets100 = billets100.toInt
            reste = reste % 100
            println("Il reste " + reste3 + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
            var choixCoupure = readLine()

            //Vérification du choix de coupure
            if (choixCoupure.toLowerCase() == "o") {
              reste = (reste3 - (billets100 * 100))
            } else {
              var choixUtilisateur = choixCoupure.toInt
              while (choixUtilisateur >= billets100){
                println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                choixUtilisateur = readInt()
              }
              billets100 = choixUtilisateur
              reste = (reste3 - (billets100 * 100))
            }

            println("Vous avez retiré " + billets100 + " billet(s) de 100 EUR")
          }
          //Traitement billets de 50
          if (reste >= 50 ){
            var reste4 = reste
            billets50 = reste / 50
            billets50 = billets50.toInt
            reste = reste % 50
            println("Il reste " + reste4 + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
            var choixCoupure = readLine()

            //Vérification du choix de coupure
            if (choixCoupure.toLowerCase() == "o") {
              reste = (reste4 - (billets50 * 50))
            } else {
              var choixUtilisateur = choixCoupure.toInt
              while (choixUtilisateur >= billets50){
                println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                choixUtilisateur = readInt()
              }
              billets50 = choixUtilisateur
              reste = (reste4 - (billets50 * 50))
            }

            println("Vous avez retiré " + billets50 + " billet(s) de 50 EUR")
          }
          //Traitement billets de 20
          if (reste >= 20 ){
            var reste6 = reste
            billets20 = reste / 20
            billets20 = billets20.toInt
            reste = reste % 20
            println("Il reste " + reste6 + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
            var choixCoupure = readLine()

            //Vérification du choix de coupure
            if (choixCoupure.toLowerCase() == "o") {
              reste = (reste6 - (billets20 * 20))
            } else {
              var choixUtilisateur = choixCoupure.toInt
              while (choixUtilisateur >= billets20){
                println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                choixUtilisateur = readInt()
              }
              billets20 = choixUtilisateur
              reste = (reste6 - (billets20 * 20))
            }

            println("Vous avez retiré " + billets20 + " billet(s) de 20 EUR")
          }
          //Traitement billets de 10
          if (reste >= 10 ){
            var reste5 = reste
            billets10 = reste / 10
            billets10 = billets10.toInt
            reste = reste % 10
            println("Il reste " + reste5 + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + billets10 + " billet(s) de 10 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
            var choixCoupure = readLine()

            //Vérification du choix de coupure
            if (choixCoupure.toLowerCase() == "o") {
              reste = (reste5 - (billets10 * 10))
            } else {
              var choixUtilisateur = choixCoupure.toInt
              while (choixUtilisateur >= billets10){
                println("Saisie incorrecte veuillez saisir un nombre inférieure au nombre maximum de billets autorisés")
                choixUtilisateur = readInt()
              }
              reste = (reste5 - (billets10 * 10))
            }

            println("Vous avez retiré " + billets10 + " billet(s) de 10 EUR")
          }
        }

          if(deviseRetrait == 2){
            montantRetrait = (montantRetrait * 0.95)
            affichage = affichageEUR
          }
          //Fin du retrait on indique combien a été retiré et combien il reste
          montantDispo = (montantDispo - montantRetrait)
          printf("\nVous avez retiré : %.2f CHF au total sur cette opération\n", montantRetrait)
          if (billets500 > 0){
          println(+ billets500 + " billet(s) de 500 CHF")
            billets500 = 0
          }
          if (billets200 > 0){
          println(+ billets200 + " billet(s) de 200 CHF")
            billets200 = 0
          }
          if (billets100 > 0){
          println(+ billets100 + " billet(s) de 100 " + affichage)
            billets100 = 0
          }
          if (billets50 > 0){
          println(+ billets50 + " billet(s) de 50 " + affichage)
            billets50 = 0
          }
          if (billets20 > 0){
          println(+ billets20 + " billet(s) de 20 " + affichage)
            billets20 = 0
          }
          if (billets10 > 0){
          println(+ billets10 + " billet(s) de 10 " + affichage)
            billets10 = 0
          }

          printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", montantDispo)
          }
          // Utilisation de la méthode retrait
          retrait(idClient, comptes)

          // Opération Consultation
        } else if (choix == 3) {
          printf("\nLe montant disponible sur votre compte est de : %.2f CHF\n", montantDispo)

          // Opération Modification de code PIN
        } else if (choix ==4) {
          // Méthode pour modifier le code PIN
          def modifierCodePIN(idClient: Int, codePin: Array[String]): Unit = {
            print("\nSaisissez votre nouveau code PIN il doit contenir au moins 8 caractères : ")
            var nouveauCodePin = scala.io.StdIn.readLine()
            while (nouveauCodePin.length < 8){
              println("\nVotre code PIN ne contient pas au moins 8 caractères.")
              nouveauCodePin = scala.io.StdIn.readLine()
            }
            // Affectation du nouveau Code PIN
            codePin(idClient) = nouveauCodePin
            println("\nVotre nouveau code PIN a été enregistré avec succès.")
          }
          // Utilisation de la méthode
          modifierCodePIN(idClient, codePin)
        } else if (choix == 5) {

          println("\nFin des opérations, n’oubliez pas de récupérer votre carte. ")
          comptes(idClient) = montantDispo
          toujoursContinuer = false

        } else {
          // Si la personne ne choisit une autre opération que les 4 qui sont proposés
          println("\nChoix invalide, veuillez réessayer en choisissant parmi les opérations disponible. ")
        }
        }

      }

  }
  
}
