//Assignment: Laura Nana Ekua Wetzel_996532_assignsubmission_file

import scala.io.StdIn._

object Main {
  var nbessaicode = 1
  var nbclient = 100
  var codespin = Array.fill(nbclient)("INTRO1234")
  var pinclient = "0"
  var comptes = Array.fill(nbclient)(1200.0)
  var choixclient = 0
  var tauxchange = 0.95
  var retrait = 0
  var choixcoupure = 0
  var b500 = 0
  var b200 = 0
  var b100 = 0
  var b50 = 0
  var b20 = 0
  var b10 = 0
  var choixb500 = ""
  var choixb200 = ""
  var choixb100 =""
  var choixb50 = ""
  var choixb20 = ""
  var choixb10 = ""
  var montant = 0
  var montantrestant = 0
  var montantdistribue = 0.00
  var identi = false
  var id = 0






  def depot(id: Int, comptes : Array[Double]): Unit =
  {
    var choixdevise = readLine("Indiquez votre devise du dépôt: 1) CHF ; 2) EUR >").toInt
    var montantdepot = readLine("Indiquez le montant du dépôt > ").toInt

    while (montantdepot % 10 != 0){
      montantdepot = readLine("le montant doit être un multiple de 10. ").toInt
    }
    if ((montantdepot % 10 == 0) && (choixdevise == 1)){

      comptes(id) += montantdepot

    }else if ((montantdepot % 10 == 0) && (choixdevise == 2)){

      comptes(id) += (montantdepot * 0.95).toInt}

    println("Votre dépôt à été pris en compte, le nouveau montant disponible sur votre compte est de: " + comptes(id) + "CHF")

    choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt
  }





  def retrait(id: Int, comptes : Array[Double]): Unit =
  {
    var choixdevise = readLine("Indiquez la devise: 1 : Chf, 2 : Eur > ").toInt

    var plafond = comptes(id)*0.1
    if ((choixdevise < 1) || (choixdevise > 2)) {

      choixdevise = readLine("Indiquez la devise: 1 : Chf, 2 : Eur > ").toInt}

    // montant retrait chf

    var retrait = readLine("Indiquez le montant du retrait > ").toInt
    if (choixdevise == 1){

      while ((retrait % 10 != 0) || (retrait > plafond)){

        if (retrait % 10 != 0) {

          retrait = readLine("le montant doit etre un multiple de 10 \n ").toInt
        }

        if (retrait > plafond){

          retrait = readLine("votre plafond autorisé est de : " + plafond + "\n").toInt
        }
      }
    }
    if (choixdevise == 2){

      while ((retrait % 10 != 0) || (retrait > plafond)){

        if (retrait % 10 != 0) {

          retrait = readLine("le montant doit etre un multiple de 10 \n ").toInt
        }

        if (retrait > plafond){

          retrait = readLine("votre plafond autorisé est de : " + plafond + "\n").toInt
        }
      }
    }


    // Retrait en CHF

    if (choixdevise == 1){

      montantdistribue += retrait

      // Grosse coupure ou petite

      if(retrait >= 200){

        choixcoupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
        //   var grossecoup = 1
        // var petitecoup = 2

        while ((choixcoupure < 1) || (choixcoupure > 2)) {

          choixcoupure = readLine("En 1) grosses coupures, 2) petites coupures").toInt

        }
      }

      // Si grosse coupure
      if (choixcoupure == 1){

        println("Il reste " + retrait + " CHF à distribuer")
        // 500 CHF

        if (retrait >= 500){
          b500 = retrait / 500
        }

        if (b500 > 0){
          println("Vous pouvez obtenir au maximum  " + b500 + " billets de 500 CHF")
          choixb500 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb500 == "o" || choixb500.toInt < b500)){
            choixb500 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb500 == "o"){
            retrait = retrait - (b500 * 500)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }

          else {
            b500 = choixb500.toInt
            retrait = retrait - (b500 * 500)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }

        }
        else {
          b500 = 0
        }

        // 200 CHF
        if (retrait >= 200){
          b200 = retrait / 200
        }

        if (b200 > 0){

          println("Vous pouvez obtenir au maximum  " + b200 + " billets de 200 CHF")
          choixb200 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ").toString

          while (!(choixb200 == "o" || choixb200.toInt < b200)){
            choixb200 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if ( choixb200 == "o"){
            retrait = retrait - (b200 * 200)
            if(retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")
            }
          }

          else {
            b200 = choixb200.toInt
            retrait = retrait - (b200 * 200)
            if(retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")
            }
          }

        }
        else {
          b200 = 0
        }

        // 100 CHF

        if (retrait >= 100){
          b100 = retrait / 100
        }

        if (b100 > 0){

          println("Vous pouvez obtenir au maximum  " + b100 + " billets de 100 CHF")
          choixb100 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb100 == "o" || choixb100.toInt < b100)){
            choixb100 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb100 == "o"){
            retrait = retrait - (b100 * 100)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }else {

            b100 = choixb100.toInt
            retrait = retrait - (b100 * 100)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }
        }else {

          b100 = 0
        }

        // 50 CHF

        if (retrait >= 50){
          b50 = retrait / 50
        }

        if (b50 > 0){
          println("Vous pouvez obtenir au maximum  " + b50 + " billets de 50 CHF")
          choixb50 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb50 == "o" || choixb50.toInt < b50)){
            choixb50 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb50 == "o"){
            retrait = retrait - (b50 * 50)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }

          else {
            b50 = choixb50.toInt
            retrait = retrait - (b50 * 50)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }

        }
        else {
          b50 = 0
        }
        // 20 CHF

        if (retrait >= 20){
          b20 = retrait / 20
        }

        if (b20 > 0){
          println("Vous pouvez obtenir au maximum  " + b20 + " billets de 20 CHF")
          choixb20 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb20 == "o" || choixb20.toInt < b20)){
            choixb20 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb20 == "o"){
            retrait = retrait - (b20 * 20)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }

          else {
            b20 = choixb20.toInt
            retrait = retrait - (b20 * 20)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }

        }
        else {
          b20 = 0
        }
        //10 CHF

        if (retrait >= 10){
          b10 = retrait / 10
        }

        if (b10 > 0){

          println("Vous pouvez obtenir au maximum  " + b10 + " billets de 10 CHF")


        } else {
          b10 = 0
        }

      }// coupure 1


      // petites coupures

      if (choixcoupure == 2)

      { if (retrait >= 100){
        b100 = retrait / 100
      }

        if (b100 > 0){
          println("Vous pouvez obtenir au maximum  " + b100 + " billets de 100 CHF")
          choixb100 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb100 == "o" || choixb100.toInt < b100)){
            choixb100 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb100 == "o"){
            retrait = retrait - (b100 * 100)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }

          else {
            b100 = choixb100.toInt
            retrait = retrait - (b100 * 100)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }

        }
        else {
          b100 = 0
        }

        // billet de 50

        if (retrait >= 50){
          b50 = retrait / 50
        }

        if (b50 > 0){
          println("Vous pouvez obtenir au maximum  " + b50 + " billets de 50 CHF")
          choixb50 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb50 == "o" || choixb50.toInt < b50)){
            choixb50 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb50 == "o"){
            retrait = retrait - (b50 * 50)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }

          else {
            b50 = choixb50.toInt
            retrait = retrait - (b50 * 50)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }

        }
        else {
          b50 = 0
        }

        // 20CHF

        if (retrait >= 20){
          b20 = retrait / 20
          b20 = retrait / 20
        }

        if (b20 > 0){
          println("Vous pouvez obtenir au maximum  " + b20 + " billets de 20 CHF")
          choixb20 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

          while (!(choixb20 == "o" || choixb20.toInt < b20)){
            choixb20 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
          }

          if (choixb20 == "o"){
            retrait = retrait - (b20 * 20)
            if (retrait > 0){
              println("Il reste " + retrait + " CHF à distribuer")}
          }

          else {
            b20 = choixb20.toInt
            retrait = retrait - (b20 * 20)
            if (retrait  > 0) {
              println("Il reste " + retrait + " CHF à distribuer")}
          }

        }
        else {
          b20 = 0
        }
        // 10CHF

        if (retrait >= 10){
          b10 = retrait / 10
        }

        if (b10 > 0){
          println("Vous pouvez obtenir au maximum  " + b10 + " billets de 10 CHF")
        }
        else {
          b10 = 0
        }
      }// 2coup

      if (b500 != 0) { print(s"$b500 billet(s) de 500 CHF \n")}
      if (b200 != 0) { print(s"$b200 billet(s) de 200 CHF \n")}
      if (b100 != 0) { print(s"$b100 billet(s) de 100 CHF \n")}
      if (b50 != 0) { print(s"$b50 billet(s) de 50 CHF \n")}
      if (b20 != 0) { print(s"$b20 billet(s) de 20 CHF \n")}
      if (b10 != 0) { print(s"$b10 billet(s) de 10 CHF \n")}


      comptes(id) -= montantdistribue
      printf("le montant de votre compte s'élève à : %.2f \n ", comptes(id))
      
      choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt

    } //choix devise 1

    //DEVISE EUR
    if (choixdevise == 2){

      montantdistribue += (retrait*tauxchange)

      if (retrait >= 100){
        b100 = retrait / 100
      }

      if (b100 > 0){
        println("Vous pouvez obtenir au maximum  " + b100 + " billets de 100 EUR")
        choixb100 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

        while (!(choixb100 == "o" || choixb100.toInt < b100)){
          choixb100 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
        }

        if (choixb100 == "o"){
          retrait = retrait - (b100 * 100)
          if (retrait > 0){
            println("Il reste " + retrait + " EUR à distribuer")}
        }

        else {
          b100 = choixb100.toInt
          retrait = retrait - (b100 * 100)
          if (retrait  > 0) {
            println("Il reste " + retrait + " EUR à distribuer")}
        }

      }
      else {
        b100 = 0
      }
      // 50 EUR
      if (retrait >= 50){
        b50 = retrait / 50
      }

      if (b50 > 0){
        println("Vous pouvez obtenir au maximum  " + b50 + " billets de 50 EUR")
        choixb50 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

        while (!(choixb50 == "o" || choixb50.toInt < b50)){
          choixb50 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
        }

        if (choixb50 == "o"){
          retrait = retrait - (b50 * 50)
          if (retrait > 0){
            println("Il reste " + retrait + " EUR à distribuer")}
        }

        else {
          b50 = choixb50.toInt
          retrait = retrait - (b50 * 50)
          if (retrait  > 0) {
            println("Il reste " + retrait + " EUR à distribuer")}
        }

      }
      else {
        b50 = 0
      }
      // 20 EUR

      if (retrait >= 20){

        b20 = retrait / 20
      }

      if (b20 > 0){
        println("Vous pouvez obtenir au maximum  " + b20 + " billets de 20 EUR")
        choixb20 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

        while (!(choixb20 == "o" || choixb20.toInt < b20)){
          choixb20 = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
        }

        if (choixb20 == "o"){
          retrait = retrait - (b20 *20)
          if (retrait > 0){
            println("Il reste " + retrait + " EUR à distribuer")}
        }

        else {
          b20 = choixb20.toInt
          retrait = retrait - (b20 * 20)
          if (retrait  > 0) {
            println("Il reste " + retrait + " EUR à distribuer")}
        }

      }
      else {
        b20 = 0
      }

      // 10 EUR

      if (retrait >= 10){
        b10 = retrait / 10
      }

      if (b10 > 0){
        println("Vous pouvez obtenir au maximum  " + b10 + " billets de 10 EUR")
      }
      else {
        b10 = 0
      }
      if (b100 != 0) { print(s"$b100 billet(s) de 100 EUR \n")}
      if (b50 != 0) { print(s"$b50 billet(s) de 50 EUR \n")}
      if (b20 != 0) { print(s"$b20 billet(s) de 20 EUR \n")}
      if (b10 != 0) { print(s"$b10 billet(s) de 10 EUR \n")}

      comptes(id) -= montantdistribue
      printf("le montant de votre compte s'élève à : %.2f \n ", comptes(id))
      choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt

      // devise eur
    }//bhijk

  }//def2
  def consultationcompte(id: Int, comptes : Array[Double]) : Unit =
  {
    println("Le montant disponible sur votre compte est de : " + comptes(id) + "CHF" )

    choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt

  } // def3
  def changepin(id: Int, codespin : Array[String]) : Unit =

  {

    codespin(id) = readLine("Saississez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString

    while (codespin(id).length < 8){

      codespin(id) = readLine("votre code pin ne contient pas au moins 8 caractères").toString
    }
    if (codespin(id).length >= 8)
    {
      choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt
    }

  } //def4










  // VRAI CODE






  def main(args: Array[String]): Unit = {

    while (identi == false){
      var identifiant = readLine("Saississez votre code identifiant >").toInt

      if ((identifiant < 0 )|| (identifiant > nbclient)){
        println("votre identifiant n'est pas valable")
        System.exit(0)
      }
      pinclient = readLine("Saississez votre code pin >").toString

      while ((pinclient != codespin(identifiant)) && (nbessaicode <3)){

        nbessaicode +=1

        println("votre code pin erroné, il vous reste " + (4-nbessaicode) + " essaie(s)")
        pinclient = readLine("Saississez votre code pin >").toString

        if (nbessaicode >= 3){

          println("trop d'erreurs, abandon de l'identification")

          identifiant = readLine("Saississez votre code identifiant >").toInt
          nbessaicode = 0
          pinclient = readLine("Saississez votre code pin >").toString

        }
      }
      if (pinclient == codespin(identifiant)){
        id = identifiant
        choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt
        nbessaicode = 1
        identi = true
      }
    } // identi
    while (choixclient != 5) {
      while (choixclient == 1) {
        depot(id, comptes)
      }
      while (choixclient == 2) {
        retrait(id, comptes)
      }
      while (choixclient == 3) {
        consultationcompte(id, comptes)
      }
      while (choixclient == 4) {
        changepin(id, codespin)
        print("AAAAAAAAA")
      } //while
    }
    while (choixclient == 5)
    {
      identi = false
      while (identi == false){
        var identifiant = readLine("Saississez votre code identifiant >").toInt

        if ((identifiant < 0 )|| (identifiant > nbclient)){
          println("votre identifiant n'est pas valide")
          System.exit(0)
        }
        pinclient = readLine("Saississez votre code pin >").toString

        while ((pinclient != codespin(identifiant)) && (nbessaicode <3)){

          nbessaicode +=1

          println("votre code pin erroné, il vous reste " + (4-nbessaicode) + " essaie(s)")
          pinclient = readLine("Saississez votre code pin >").toString

          if (nbessaicode >= 3){

            println("trop d'erreurs, abandon de l'identification")

            identifiant = readLine("Saississez votre code identifiant >").toInt
            nbessaicode = 0
            pinclient = readLine("Saississez votre code pin >").toString

          }
        }
        if (pinclient == codespin(identifiant)){
          id = identifiant
          choixclient = readLine("Choississez votre opération: \n \t 1) Dépot \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement de code pin \n \t 5) Terminer \n Votre choix :").toInt
          nbessaicode = 1
          identi = true
        }
      } // identi

    }



  } // args


} //object