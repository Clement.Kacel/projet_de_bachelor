//Assignment: Rozana Livia Buscaneanu_996597_assignsubmission_file

 import io.StdIn._
    import math._

object Main {
  var ChoixPin = ("")

  var MontantDisponible = 1200.0
  var Choix = 0
  var ChoixDevise = 0


  var RetraitInitial = 0
  var RetraitEUR = 0
  var Reste = 0

  
  val nbClients = 100
  val codesPin: Array[String] = Array.fill(nbClients)("INTRO1234")
  val comptes: Array[Double] = Array.fill(nbClients)(MontantDisponible)
 
    
 
def depot(id: Int, comptes: Array[Double]): Unit = {
  var Depot = 0.0
  var ChoixDevise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
          Depot = readLine("Indiquez le montant du dépôt > ").toInt
            //tant que le dépôt n'est pas divisible par 10, recommencer
          while (Depot % 10 != 0){
              println("Le montant doit être un multiple de 10. ")
             Depot = readLine().toInt
            }

          // conversion de la monnaie
           if (ChoixDevise == 1){
             comptes(id) = comptes(id) + Depot
              printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n" , comptes(id)) 
           } else if (ChoixDevise == 2){
            Depot = (Depot * 0.95).toDouble
             comptes(id) = comptes(id) + Depot
             printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n" , comptes(id)) 
           }
//MontantDisponible = comptes(id)
  } 
  
def retrait(id : Int, comptes : Array[Double]) : Unit = {
  var Retrait = 0
  var RetraitAutorise = 0.0
  var nbBillets  = 0
    var nbBillets1 = 0
    var nbBillets2 = 0
    var nbBillets3 = 0
    var nbBillets4 = 0
    var nbBillets5 = 0
    var nbBillets6 = 0
  var choix1 :Int = 0 
  var choix2 :Int = 0 
  var choix3 :Int = 0 
  var choix4 :Int = 0 
  var choix5 :Int = 0 
  var choix6 :Int = 0 
  var ChoixCoupure = 0
  var coupure = 0
  val ok = ("o")
  
  var BilletsRetire500 = ("")
  var BilletsRetire200 = ("")
  var BilletsRetire100 = ("")
  var BilletsRetire50 = ("")
  var BilletsRetire20 = ("")
  var BilletsRetire10 = ("")
  var BilletsRetire100EUR = ("")
  var BilletsRetire50EUR = ("")
  var BilletsRetire20EUR = ("")
  var BilletsRetire10EUR = ("")
    // on redemande le choix de la devise tant que le client ne saisi pas 1 ou 2
      ChoixDevise = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt

    while ((ChoixDevise!= 1) && (ChoixDevise !=2)){
       ChoixDevise = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt
    }
     // établir le montant de retrait maximal
     RetraitAutorise = (comptes(id)*10)/100
      RetraitInitial = readLine("Indiquez le montant du retrait > ").toInt
      // tant que le montant de retrait demandé n'est pas un multiple de 10 ou inférieur au montant maximal, on redemande   
      while ((RetraitInitial > RetraitAutorise) || (RetraitInitial % 10 != 0) ) {
      if  (RetraitInitial > RetraitAutorise){
      println("Votre plafond de retrait autorisé est de : " + RetraitAutorise) 
      RetraitInitial = readLine("").toInt
                  }
       else if  (RetraitInitial % 10 != 0){
       println("Le montant doit être un multiple de 10. ")
       RetraitInitial= readLine("").toInt
                    }
                  }

  // on nomme une variable retrait étant égale au rentrait initial (demandé) qu'on va pouvoir modifier 
    Retrait = RetraitInitial

     if (ChoixDevise == 1){
  // le client doit choisir le type de coupures en tapant 1 ou 2
       var TailleCoupures = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt

         while ((TailleCoupures != 1) && (TailleCoupures != 2)) {
                            TailleCoupures = readLine(" En 1) grosses coupures, 2) petites coupures > ").toInt
                          }

  // Répartition du montant en grosses coupures. Le montant doit être supérieur ou égal à 200.
         if ((TailleCoupures == 1) && (Retrait >= 200)){
      // calcul du nombre de billets de 500 CHF
                  coupure = 500
      //le montant doit être supérieur ou égal à 500 pour obtenir des billets de 500 CHF

                 if (Retrait >= 500){
                   // Si le reste de la division du montant du retrait par la coupure n'est pas égal au montant du retrait, alors le nombre de billets de 500 est supérieur à 0
                  if (Retrait % coupure != Retrait ){
                   Reste = (Retrait % coupure).toInt
                    //calcul du nombre de billets de 500
                   nbBillets1 = ((Retrait - Reste) / coupure)
                  nbBillets = nbBillets1

                  println("Il reste "+ Retrait + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum " + nbBillets1 + " billet(s) de " + coupure +  " CHF")

                  do {  
                   var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

  // le client a choisi "ok" (il veut retirer le nombre de billets maximal pour cette coupure) 
                     if (ChoixCoupure == ok) {
                       // calcul de la nouvelle valeur du retrait (montant restant à retirer)
                       Retrait = Retrait - (nbBillets1 * coupure)
                       BilletsRetire500 = (nbBillets1 + " billets de " + coupure + " CHF ")

                                  }
  // le client choisi de retirer un nombre de billets inférieur au nombre maximal
                       else {
                      nbBillets1 = ChoixCoupure.toInt
                         if (nbBillets1 < nbBillets) {
                           Retrait = Retrait - (nbBillets1 * coupure)
                          BilletsRetire500 = (nbBillets1 + " billets de " + coupure +" CHF ")
                                   } 

                                    }
    // l'opération est repétée jusqu'à ce que le client tape "o" ou un nombre de billets inférieur au nombre de billets maximal
                   } while ((Retrait > coupure)&& (nbBillets1 >= nbBillets))

                         }
                       }

  // calcul du nombre de billets de 200 CHF
                      coupure = 200

        // on répète le même procédé pour les autres coupures
                       if (Retrait >= 200){
                        if (Retrait % coupure != Retrait ){
                          Reste = (Retrait % coupure).toInt
                          nbBillets2 = ((Retrait - Reste) / coupure)
                          nbBillets = nbBillets2
                          println("Il reste "+ Retrait + " CHF à distribuer")
                          println("Vous pouvez obtenir au maximum " + nbBillets2 + " billet(s) de " + coupure +  " CHF")

                          do {  
                          var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                           if (ChoixCoupure == ok) {
                             Retrait = Retrait - (nbBillets2 * coupure)
                              BilletsRetire200 = (nbBillets2 + " billets de " + coupure + " CHF ")
                                }
                          else {
                           nbBillets2 = ChoixCoupure.toInt
                            if (nbBillets2 < nbBillets) {
                            Retrait = Retrait - (nbBillets2 * coupure)
                            BilletsRetire200 = (nbBillets2 + " billets de " + coupure +" CHF ")
                              } 
                       }
                        } while ((Retrait > coupure)&& (nbBillets2 >= nbBillets))
                                 }
                               }
                            }
  // les petites coupures commencent à partir de 100 CHF et sont imposées si le montant à retirer est plus petit que 200
                    if  (((TailleCoupures ==2) || (TailleCoupures ==1 )) || ( Retrait<200 )){
  // calcul du nombre de billets de 100 CHF
                      coupure = 100

                      if (Retrait >= 100){
                        if (Retrait % coupure != Retrait ){
                          Reste = (Retrait % coupure).toInt
                          nbBillets3 = ((Retrait - Reste) / coupure)
                          nbBillets= nbBillets3

                       println("Il reste "+ Retrait + " CHF à distribuer")
                                  println("Vous pouvez obtenir au maximum " + nbBillets3 + " billet(s) de " + coupure +  "CHF")

                          do {  
                          var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                          if (ChoixCoupure == ok) {
                          Retrait = Retrait - (nbBillets3 * coupure)
                          BilletsRetire100 = (nbBillets3 + " billets de " + coupure + " CHF ")
                          }
                          else {
                          nbBillets3 = ChoixCoupure.toInt
                              if (nbBillets3 < nbBillets) {
                               Retrait = Retrait - (nbBillets3 * coupure)
                               BilletsRetire100 = (nbBillets3 + " billets de " + coupure +" CHF ")
                              } 

                                }
                                  } while ((Retrait > coupure)&& (nbBillets3 >= nbBillets))
                         }
                       }
   // calcul du nombre de billets de 50 CHF
                    coupure = 50

                        if (Retrait >= 50){
                          if (Retrait % coupure != Retrait ){
                          Reste = (Retrait % coupure).toInt
                          nbBillets4 = ((Retrait - Reste) / coupure)
                          nbBillets= nbBillets4

                            println("Il reste "+ Retrait + " CHF à distribuer")
                            println("Vous pouvez obtenir au maximum " + nbBillets4 + " billet(s) de " + coupure +  " CHF")


                        do {  
                          var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString


                          if (ChoixCoupure == ok) {
                            Retrait = Retrait - (nbBillets4 * coupure)
                            BilletsRetire50 = (nbBillets4 + " billets de " + coupure + " CHF ")
                             }

                          else {
                          nbBillets4 = ChoixCoupure.toInt
                            if (nbBillets4 < nbBillets) {
                              Retrait = Retrait - (nbBillets4 * coupure)
                              BilletsRetire50 = (nbBillets4 + " billets de " + coupure +" CHF ")
                            } 

                          }
                            } while ((Retrait > coupure)&& (nbBillets4 >= nbBillets))

                       }
                     }
  // calcul du nombre de billets de 20 CHF
                    coupure = 20

                      if (Retrait >= 20){
                        if (Retrait % coupure != Retrait ){
                          Reste = (Retrait % coupure).toInt
                          nbBillets5 = ((Retrait - Reste) / coupure)
                          nbBillets = nbBillets5

                          println("Il reste "+ Retrait + " CHF à distribuer")
                          println("Vous pouvez obtenir au maximum " + nbBillets5 + " billet(s) de " + coupure +  " CHF")

                       do {  
                        var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                         if (ChoixCoupure == ok) {
                           Retrait = Retrait - (nbBillets5 * coupure)
                           BilletsRetire20 = (nbBillets5 + " billets de " + coupure + " CHF ")
                          }
                          else {
                            nbBillets5 = ChoixCoupure.toInt
                             if (nbBillets5 < nbBillets) {
                               Retrait = Retrait - (nbBillets5 * coupure)
                               BilletsRetire20 = (nbBillets5 + " billets de " + coupure +" CHF ")
                            } 

                               }
                                } while ((Retrait > coupure)&& (nbBillets5 >= nbBillets))
                       }
                     }
  // Le client n'a pris aucune coupure avant d'arriver à la distribution de celle de 10 CHF. La valeur du montant du retrait n'a pas diminué, aucune opération n'a été faite. Le montant du retrait est alors égal au montant du retrait initial (saisi par le client)
              if (Retrait == RetraitInitial) {
  // le client est obligé de prendre la proposition faite par le programme: le programme prend le nombre de billet maximale pour chaque coupure
                coupure = 500

                if (Retrait >= 500){
                  if (Retrait % coupure != Retrait ){
                  Reste = (Retrait % coupure).toInt
                  nbBillets1 = ((Retrait - Reste) / coupure)

                  Retrait = Retrait - (nbBillets1 * coupure)
                   BilletsRetire500 = (nbBillets1 + " billets de " + coupure + " CHF ")
                  }
                }  

                 coupure = 200
                    if (Retrait >= 200){
                    if (Retrait % coupure != Retrait ){
                    Reste = (Retrait % coupure).toInt
                    nbBillets2 = ((Retrait - Reste) / coupure)

                    Retrait = Retrait - (nbBillets2 * coupure)
                     BilletsRetire200 = (nbBillets2 + " billets de " + coupure + " CHF ")
                    }
                  }  

                coupure = 100
                  if (Retrait >= 100){
                  if (Retrait % coupure != Retrait ){
                  Reste = (Retrait % coupure).toInt
                  nbBillets3 = ((Retrait - Reste) / coupure)

                  Retrait = Retrait - (nbBillets3 * coupure)
                   BilletsRetire100 = (nbBillets3 + " billets de " + coupure + " CHF ")
                  }
                }  
                coupure = 50
                  if (Retrait >= 50){
                  if (Retrait % coupure != Retrait ){
                  Reste = (Retrait % coupure).toInt
                  nbBillets4 = ((Retrait - Reste) / coupure)

                  Retrait = Retrait - (nbBillets4 * coupure)
                   BilletsRetire50 = (nbBillets4 + " billets de " + coupure + " CHF ")
                  }
                }  
                coupure = 20
                  if (Retrait >= 20){
                  if (Retrait % coupure != Retrait ){
                  Reste = (Retrait % coupure).toInt
                  nbBillets5 = ((Retrait - Reste) / coupure)

                  Retrait = Retrait - (nbBillets5 * coupure)
                   BilletsRetire20= (nbBillets5 + " billets de " + coupure + " CHF ")
                  }
                } 

                coupure = 10
                  if (Retrait >= 10){
                  if (Retrait % coupure != Retrait ){
                  Reste = (Retrait % coupure).toInt
                  nbBillets6 = ((Retrait - Reste) / coupure)

                  Retrait = Retrait - (nbBillets6 * coupure)
                   BilletsRetire10 = (nbBillets6 + " billets de " + coupure + " CHF ")
                  }
                } 
                // affichage du nombre de billets à retirer
                  println("Veuillez retirer la somme demandée : ")

                if (nbBillets1 > 0 ) println(BilletsRetire500)
                if (nbBillets2 > 0 ) println(BilletsRetire200)
                if (nbBillets3 > 0 ) println(BilletsRetire100)
                if (nbBillets4 > 0 ) println(BilletsRetire50)
                if (nbBillets5 > 0 ) println(BilletsRetire20)
                if (nbBillets6 > 0 ) println(BilletsRetire10)
  // calcul du nouveau montant disponible : on soustrait le retrait du client au montant disponible du compte               
  comptes(id) = comptes(id) - RetraitInitial
                printf(" Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n ", comptes(id))
              }
                    else {
  // sinon, la personne a pris des coupures avant d'arriver à la distribution de celle de 10 CHF
  //calcul du nombre de billets de 10 CHF
                    coupure = 10

                      if (Retrait >= 10){
                        if (Retrait % coupure != Retrait ){
                          Reste = (Retrait % coupure).toInt
                          nbBillets6 = ((Retrait - Reste) / coupure)
                          nbBillets = nbBillets6

                          println("Il reste "+ Retrait + " CHF à distribuer")
                          println("Vous pouvez obtenir au maximum " + nbBillets6 + " billet(s) de " + coupure +  " CHF")

                       do {  
                        var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                        if (ChoixCoupure == ok) {
                            Retrait = Retrait - (nbBillets6 * coupure)
                            BilletsRetire10 = (nbBillets6 + " billets de " + coupure + " CHF ")
                            }
                        else {
                          nbBillets6 = ChoixCoupure.toInt
                            if (nbBillets6 < nbBillets) {
                              Retrait = Retrait - (nbBillets6 * coupure)
                               BilletsRetire10 = (nbBillets6 + " billets de " + coupure +" CHF ")
                            } 
                                  }
                                } while ((Retrait > coupure)&& (nbBillets6 >= nbBillets))
                   }
                     }
          //affichage du nombre de billets à retirer  
               println("Veuillez retirer la somme demandée : ")
                  if (nbBillets1> 0) println(BilletsRetire500)
                  if (nbBillets2> 0) println(BilletsRetire200)
                  if (nbBillets3> 0) println(BilletsRetire100)
                  if (nbBillets4> 0) println(BilletsRetire50)
                  if (nbBillets5> 0) println(BilletsRetire20)
                  if (nbBillets6> 0) println(BilletsRetire10)

                      comptes(id) = comptes(id) - RetraitInitial
       printf(" Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n ", comptes(id))
       }
     }
     }


  // le client veut retirer des EUR
                if (ChoixDevise ==2){
  // on effectue les mêmes opérations que pour les CHF    
        // // calcul du nombre de billets de 100 EUR
                  coupure = 100

                    if (Retrait >= 100){
                      if (Retrait % coupure != Retrait ){
                        Reste = (Retrait % coupure).toInt
                        nbBillets3 = ((Retrait - Reste) / coupure).toInt
                        nbBillets= nbBillets3

                     println("Il reste "+ Retrait + " EUR à distribuer")
                                println("Vous pouvez obtenir au maximum " + nbBillets3 + " billet(s) de " + coupure +  " EUR")

                        do {  
                        var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                        if (ChoixCoupure == ok) {
                        Retrait = Retrait - (nbBillets3 * coupure)
                        BilletsRetire100EUR = (nbBillets3 + " billets de " + coupure + " EUR ")
                       }
                        else {
                          nbBillets3 = ChoixCoupure.toInt
                            if (nbBillets3 < nbBillets) {
                             Retrait = Retrait - (nbBillets3 * coupure)
                             BilletsRetire100EUR = (nbBillets3 + " billets de " + coupure +" EUR ")
                             } 
                              }
                                } while ((Retrait > coupure)&& (nbBillets3 >= nbBillets))
                       }
                     }

                  // calcul du nombre de billets de 50 EUR
                  coupure = 50

                      if (Retrait >= 50){
                        if (Retrait % coupure != Retrait ){
                          Reste = (Retrait % coupure).toInt
                          nbBillets4 = ((Retrait - Reste) / coupure).toInt
                          nbBillets = nbBillets4

                          println("Il reste "+ Retrait + " EUR à distribuer")
                          println("Vous pouvez obtenir au maximum " + nbBillets4 + " billet(s) de " + coupure +  " EUR")


                      do {  
                        var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                        if (ChoixCoupure == ok) {
                          Retrait = Retrait - (nbBillets4 * coupure)
                          BilletsRetire50EUR = (nbBillets4 + " billets de " + coupure + " EUR ")
                          }
                        else {
                        nbBillets4 = ChoixCoupure.toInt
                          if (nbBillets4 < nbBillets) {
                            Retrait = Retrait - (nbBillets4 * coupure)
                            BilletsRetire50EUR = (nbBillets4 + " billets de " + coupure +" EUR ")
                           } 
                        }
                          } while ((Retrait > coupure)&& (nbBillets4 >= nbBillets))
                     }
                   }

             // calcul du nombre de billets de 20 EUR
                  coupure = 20

                    if (Retrait >= 20){
                      if (Retrait % coupure != Retrait ){
                        Reste = (Retrait % coupure).toInt
                        nbBillets5 = ((Retrait - Reste) / coupure).toInt
                        nbBillets = nbBillets5

                        println("Il reste "+ Retrait + " EUR à distribuer")
                        println("Vous pouvez obtenir au maximum " + nbBillets5 + " billet(s) de " + coupure +  " EUR")

                     do {  
                      var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString

                       if (ChoixCoupure == ok) {
                         Retrait = Retrait - (nbBillets5 * coupure)
                         BilletsRetire20EUR = (nbBillets5 + " billets de " + coupure + " EUR ")
                        }
                         else {
                           nbBillets5 = ChoixCoupure.toInt
                           if (nbBillets5 < nbBillets) {
                             Retrait = Retrait - (nbBillets5 * coupure)
                             BilletsRetire20EUR = (nbBillets5 + " billets de " + coupure +" EUR ")
                           } 
                             }
                              } while ((Retrait > coupure)&& (nbBillets5 >= nbBillets))
                     }
                   }
  // le client n'a pris aucune coupure, le montant du retrait est le même 
                  if (Retrait == RetraitInitial) {
                    // calcul du nombre de billets maximal de 100 EUR
                    coupure = 100
                      if (Retrait >= 100){
                      if (Retrait % coupure != Retrait ){
                      Reste = (Retrait % coupure).toInt
                      nbBillets3 = ((Retrait - Reste) / coupure)

                      Retrait = Retrait - (nbBillets3 * coupure)
                         BilletsRetire100EUR = (nbBillets3 + " billets de " + coupure + " EUR ")
                      }
                    } 
                    // calcul du nombre de billets maximal de 50 EUR
                    coupure = 50
                      if (Retrait >= 50){
                      if (Retrait % coupure != Retrait ){
                      Reste = (Retrait % coupure).toInt
                      nbBillets4 = ((Retrait - Reste) / coupure)

                      Retrait = Retrait - (nbBillets4 * coupure)
                       BilletsRetire50EUR = (nbBillets4 + " billets de " + coupure + " EUR ")
                      }
                    }  
                    // calcul du nombre de billets maximal de 20 EUR
                    coupure = 20
                      if (Retrait >= 20){
                      if (Retrait % coupure != Retrait ){
                      Reste = (Retrait % coupure).toInt
                      nbBillets5 = ((Retrait - Reste) / coupure)

                      Retrait = Retrait - (nbBillets5 * coupure)
                       BilletsRetire20EUR= (nbBillets5 + " billets de " + coupure + " EUR ")
                      }
                    } 
                    // calcul du nombre de billets maximal de 10 EUR
                    coupure = 10
                      if (Retrait >= 10){
                      if (Retrait % coupure != Retrait ){
                      Reste = (Retrait % coupure).toInt
                      nbBillets6 = ((Retrait - Reste) / coupure)

                      Retrait = Retrait - (nbBillets6 * coupure)
                       BilletsRetire10EUR = (nbBillets6 + " billets de " + coupure + " EUR ")
                      }
                    } 
                   // affichage des billets à retirer
                    println("Veuillez retirer la somme demandée : ")
                    if (nbBillets3 > 0) println(BilletsRetire100EUR)
                    if (nbBillets4 > 0) println(BilletsRetire50EUR)
                    if (nbBillets5 > 0) println(BilletsRetire20EUR)
                    if (nbBillets6 > 0) println(BilletsRetire10EUR)
             // calcul du nouveau montant disponible en conversant le retrait d'EUR en CHF
                    comptes(id) = comptes(id) - (RetraitInitial * 0.95)
                    printf(" Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n ", comptes(id))
                  } else {

  //le client a pris des coupures avant d'arriver à la distribution de celle de 10
  // calcul du nombre de billets de 10 EUR
                  coupure = 10


                    if (Retrait >= 10){
                      if (Retrait % coupure != Retrait ){
                        Reste = (Retrait % coupure).toInt
                        nbBillets6 = ((Retrait - Reste) / coupure).toInt
                        nbBillets = nbBillets6

                        println("Il reste "+ Retrait + " EUR à distribuer")
                        println("Vous pouvez obtenir au maximum " + nbBillets6 + " billet(s) de " + coupure +  " EUR")


                     do {  
                      var ChoixCoupure = readLine(" Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString


                      if (ChoixCoupure == ok) {
                          Retrait = Retrait - (nbBillets6 * coupure)
                          BilletsRetire10EUR = (nbBillets6 + " billets de " + coupure + " EUR ")
                        }

                      else {
                       nbBillets6 = ChoixCoupure.toInt
                          if (nbBillets6 < nbBillets) {
                            Retrait = Retrait - (nbBillets6 * coupure)
                             BilletsRetire10EUR = (nbBillets6 + " billets de " + coupure +" EUR ")
                             } 

                                }
                              } while ((Retrait > coupure)&& (nbBillets6 >= nbBillets))
                 }
                   }
                  // affichage du nombre de billets à retirer
                 println("Veuillez retirer la somme demandée : ")
                  if (nbBillets3 > 0) println(BilletsRetire100EUR)
                  if (nbBillets4 > 0) println(BilletsRetire50EUR)
                  if (nbBillets5 > 0) println(BilletsRetire20EUR)
                  if (nbBillets6 > 0) println(BilletsRetire10EUR)
                //// calcul du nouveau montant disponible en conversant le retrait d'EUR en CHF
                    comptes(id) = comptes(id) - (RetraitInitial * 0.95)
                  printf(" Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n ", comptes(id))

                }
          }
// MontantDisponible = comptes(id)
}
  
  def changepin(id : Int, codesPin : Array[String]) : Unit= {
    var nouveauCode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toString
    while (nouveauCode.length < 8) {
      nouveauCode= readLine("Votre code pin ne contient pas au moins 8 caractères.").toString
    }
    codesPin(id) = nouveauCode
  }

  def Menu() : Unit = {
    println("Choisissez votre opération:")
    println("1) Dépôt")
    println("2) Retrait")
    println("3) Consultation du compte")
    println("4) Changement du code pin")
    println("5) Terminer")
    println("Votre choix:") 
  }
  
 def main(args: Array[String]): Unit = {
  var continue = true
   do{
var nbTentatives = 3
var id = readLine("Saisissez votre code identifiant > ").toInt
   if (id >= nbClients){
         println("Cet identifiant n’est pas valable.")
     return;
   }
   else {
   ChoixPin = readLine("Saisissez votre code pin > ").toString
     while ((ChoixPin != codesPin(id)) && (nbTentatives > 1)) {
     nbTentatives = nbTentatives - 1
     println("Code pin erroné, il vous reste " + nbTentatives + " tentatives > ")
     ChoixPin = readLine("Saisissez votre code pin > ").toString }
     if ((ChoixPin != codesPin(id)) && (nbTentatives == 1)) {
     println ("Trop d'erreurs, abandon de l'identification")
     }
   }
if ((ChoixPin == codesPin(id)) || (nbTentatives != 1)) {
   Menu()
   var Choix = readLine().toInt
  
   do {
  if (Choix == 1) {
  depot(id, comptes)
  }  
  if (Choix == 2) {
  retrait(id, comptes)
  }
  if (Choix == 3) {
    //MontantDisponible = comptes(id)
      printf("Le montant disponible sur votre compte est de : %.2f CHF\n" ,comptes(id))
  }
  if (Choix == 4) {
  changepin(id,codesPin)}
Menu()
Choix = readLine().toInt
   } while (Choix != 5)
  if (Choix == 5) {
    println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
  }
  }
 } while (continue == true)
 } 
 
  
}

