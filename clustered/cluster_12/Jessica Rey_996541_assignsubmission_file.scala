//Assignment: Jessica Rey_996541_assignsubmission_file

import scala.io.StdIn._

object Main {
  
  val nbrclients: Int = 100
  var comptesmontant = Array.fill(100)(1200.0)
  var codepin = Array.fill(100)("INTRO1234")
  
// def main arg
  def main(args: Array[String]): Unit = {

    while(true){ //boucle code entier

      println("\n Bienvenue ! veuillez suivre les indications.")

    var id: Int = readLine("\n Saisissez votre code identifiant > ").toInt

    // verification du numéro client
    if (id > nbrclients) {
      println("\n Cet identifiant n'est pas valable.")
      System.exit(0)
    } 
    
      var tentative: Array[Int] = Array.fill(nbrclients)(3)
      var saisipin: String = "\n Saisissez votre code pin > "
      
    // verification du code pin  
    while (codepin(id) != saisipin) {
      saisipin = readLine("\n Saisissez votre code pin > ").toString
     tentative(id) -= 1

      if ((codepin(id) != saisipin) && (tentative(id) != 0)) {
        println("\n Code pin erroné, il vous reste " + tentative(id) + "  tentative(s).")  
      }

       if ((tentative(id) == 0) && (codepin(id) != saisipin)) {
        println("\n Trop d'erreurs, abandon de l'identification.")
        var id = readLine("\n Saissisez votre code identifiant > ").toInt
        tentative = Array.fill(nbrclients)(3)
      }
    } 
      
    var choix: Int = 0

    // debut opération (choix)
    while (choix != 5) {
      choix = readLine("\n Choisissez votre opération: \n 1) Dépot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix : ").toInt 

      // attribution des choix d'opération au definition
      choix match {
        case 0 => main(args)
        case 1 => depot(id, comptesmontant)
        case 2 => retrait(id, comptesmontant)
        case 3 => consultation(id)
        case 4 => changepin(id, codepin)
        case 5 => terminer()
      }
    } 

// def opération dépot
  def depot(id : Int, comptesmontant : Array[Double]) : Unit = {
    val tauxEURtoCHF = 0.95
        val tauxCHFtoEUR = 1.05

        var devisedepot = readLine("\n Indiquez la devise du dépôt: 1) CHF ; 2) EUR > ").toString

    while((devisedepot != "1" && devisedepot != "CHF") && (devisedepot != "2" && devisedepot != "EUR")) {
      
      devisedepot = readLine("\n Indiquez la devise du dépôt: 1) CHF ; 2) EUR > ").toString
    }
          
        var montantdepot = readLine("\n Indiquez le montant du dépôt > ").toInt 
        
  // condition multiple de 10
        while (montantdepot % 10 != 0) {
          println("\n le montant doit être un multiple de 10")
        montantdepot = readLine("\n Indiquez le montant du dépôt > ").toInt
        } 

        // convertion  Eur à Chf
        if (devisedepot == "2" || devisedepot == "EUR") {

          var montantconvdepot = montantdepot * 0.95
          comptesmontant(id) += montantconvdepot
          
        } else 
          comptesmontant(id) += montantdepot
          
        println("\n Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + comptesmontant(id))
  } // fin def depot

// def opération retrait
   def retrait(id : Int, comptesmontant : Array[Double]) : Unit = {

    var deviseretrait = readLine("\n Indiquez la devise: 1 CHF, 2 : EUR > ").toString
        
        while ((deviseretrait != "1" && deviseretrait != "CHF")&&(deviseretrait != "2" && deviseretrait != "EUR") ) {
          
          deviseretrait = readLine("\n Indiquez la devise: 1 CHF, 2 : EUR > ").toString
        }

          var montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt

  // Devise en CHF

    //montant supérieur à 200
        if ((deviseretrait == "1" || deviseretrait == "CHF")&&(montantretrait >= 200)){

          var plafond = comptesmontant(id)/10

          while(montantretrait % 10 != 0) {
            println("\n le montant doit être un multiple de 10")

            montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt
          }
          
        //plafond retrait
          while (montantretrait > plafond){
            println("\n Votre plafond de retrait autorisé est de : " + plafond)
            montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt
            
          }
          
        // choix grosse petite coupure 
          var coupure = 0
          while ((coupure != 1) && (coupure != 2)) {
           coupure = readLine ("\n En 1) grosses coupures, 2) petites coupures > ").toInt
        }
          
    // Répartition grosse coupure CHF     
          var montantrestant: Int = montantretrait 
          var coupureChf500 = 0
          var coupureChf200 = 0
          var coupureChf100 = 0
          var coupureChf50 = 0
          var coupureChf20 = 0
          var coupureChf10 = 0
           
          if (coupure == 1){
            while (montantrestant != 0){

              // coupure de 500
              if (montantrestant >= 500){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 500

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 500 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf500 += billetmax
                montantrestant -= (500 * billetmax)
                
              } else {
                
                coupureChf500 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 500)
              } 
              }

              //coupure de 200
              if (montantrestant >= 200){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 200

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 200 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf200 += billetmax
                montantrestant -= (200 * billetmax)
                
              } else {
                
                coupureChf200 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 200)
              } 
              }

              //coupure de 100
              if (montantrestant >= 100){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 100

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 100 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf100 += billetmax
                montantrestant -= (100 * billetmax)
                
              } else {
                
                coupureChf100 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 100)
              } 
              }

              //coupure de 50
              if (montantrestant >= 50){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 50

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 50 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf50 += billetmax
                montantrestant -= (50 * billetmax)
                
              } else {
                coupureChf50 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 50)
              } 
              }

              //coupure de 20
              if (montantrestant >= 20){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 20

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 20 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf20 += billetmax
                montantrestant -= (20 * billetmax)
                
              } else {
                
                coupureChf20 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 20)
              } 
              }

            // si autre coupure = 0
            if(coupureChf500 == 0 && coupureChf200 == 0 && coupureChf100 == 0 && coupureChf50 == 0 && coupureChf20 == 0){
              println("\n Il reste " + montantrestant + " CHF à distribuer. ")
              var billetmax = montantrestant / 10
              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10") 
              coupureChf10 += billetmax
              montantrestant -= (10 * billetmax)
              
            // coupure de 10
            } else if (montantrestant >= 10){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 10

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf10 += billetmax
                montantrestant -= (10 * billetmax)
                
              } else {
                
                coupureChf10 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 10)
              } 
              } 
            }

          // vu d'ensemble des Grosses coupures CHF (>200)
          println("\n Veuillez retirer la somme demandée: ")
          if (coupureChf500 != 0){
            println(coupureChf500 + " billet(s) de 500 CHF")
          }
          if (coupureChf200 != 0){
            println(coupureChf200 + " billet(s) de 200 CHF ")
          }
          if (coupureChf100 != 0){
            println(coupureChf100 + " billet(s) de 100 CHF ")
          } 
          if (coupureChf50 != 0){
             println(coupureChf50 + " billet(s) de 50 CHF ")
          }
          if (coupureChf20 != 0){
            println(coupureChf20 + " billet(s) de 20 CHF ")
          }
          if (coupureChf10 != 0){
            println(coupureChf10 + " billet(s) de 10 CHF ")
          }     
        }
          
    // Répartition petite coupure CHF
          if (coupure == 2){
            while(montantrestant != 0){

            // coupure de 100
            if (montantrestant >= 100){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 100

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 100 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf100 += billetmax
                montantrestant -= (100 * billetmax)
                
              } else {
              
                coupureChf100 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 100)
              } 
              }

              // coupure de 50
              if (montantrestant >= 50){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 50

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 50 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf50 += billetmax
                montantrestant -= (50 * billetmax)
                
              } else {
                
                coupureChf50 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 50)
              }  
              }

              // coupure de 20
              if (montantrestant >= 20){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 20

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 20 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf20 += billetmax
                montantrestant -= (20 * billetmax)
                
              } else {
                coupureChf20 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 20)
              }  
              }

              // si autre coupure = 0
            if(coupureChf100 == 0 && coupureChf50 == 0 && coupureChf20 == 0){
              println("\n Il reste " + montantrestant + " CHF à distribuer. ")
              var billetmax = montantrestant / 10
              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10") 
              coupureChf10 += billetmax
              montantrestant -= (10 * billetmax)
              
            // coupure de 10
            } else if (montantrestant >= 10){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 10

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf10 += billetmax
                montantrestant -= (10 * billetmax)
                
              } else {
                
                coupureChf10 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 10)
              } 
              } 
            }

          // vu d'ensemble des petites coupures CHF (>200)
          println("\n Veuillez retirer la somme demandée: ")
          if (coupureChf100 != 0){
            println(coupureChf100 + " billet(s) de 100 CHF ")
          } 
          if (coupureChf50 != 0){
             println(coupureChf50 + " billet(s) de 50 CHF ")
          }
          if (coupureChf20 != 0){
            println(coupureChf20 + " billet(s) de 20 CHF ")
          }
          if (coupureChf10 != 0){
            println(coupureChf10 + " billet(s) de 10 CHF ")
          }  
        }
          
  //montant moins de 200
        } else if((deviseretrait == "1" || deviseretrait =="CHF")&&(montantretrait < 200)){

          var plafond = comptesmontant(id)/10

          //multiple de 10
          while(montantretrait % 10 != 0) {
            println("\n le montant doit être un multiple de 10")

            montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt
          }

          // montant retrait autorisé
          while (montantretrait > plafond){
            println("\n Votre plafond de retrait autorisé est de : " + plafond)
            montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt  
          }

          var montantrestant: Int = montantretrait 
          var coupureChf100 = 0
          var coupureChf50 = 0
          var coupureChf20 = 0
          var coupureChf10 = 0
          
        // repartition coupure 
        while(montantrestant != 0){

            // coupure de 100
            if (montantrestant >= 100){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 100

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 100 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf100 += billetmax
                montantrestant -= (100 * billetmax)
                
            } else {
              
              coupureChf100 += nbrbillet.toInt
              montantrestant -= (nbrbillet.toInt * 100)
            } 
            }

            // coupure de 50
            if (montantrestant >= 50){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 50

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 50 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf50 += billetmax
                montantrestant -= (50 * billetmax)
                
            } else {
                
                coupureChf50 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 50)
              }  
              }

            // coupure de 20
            if (montantrestant >= 20){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 20

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 20 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf20 += billetmax
                montantrestant -= (20 * billetmax)
                
            } else {
                coupureChf20 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 20)
              }  
              }
          
            // si autre coupure = 0
            if(coupureChf100 == 0 && coupureChf50 == 0 && coupureChf20 == 0){
              println("\n Il reste " + montantrestant + " CHF à distribuer. ")
              var billetmax = montantrestant / 10
              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10") 
              coupureChf10 += billetmax
              montantrestant -= (10 * billetmax)
              
            // coupure de 10
            } else if (montantrestant >= 10){
              println("\n Il reste " + montantrestant + " CHF à distribuer.") 
              var billetmax = montantrestant / 10

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10 CHF.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureChf10 += billetmax
                montantrestant -= (10 * billetmax)
                
            } else {
                
                coupureChf10 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 10)
              } 
              } 
            }
          
          // vu d'ensemble des coupures CHF (<200)
          println("\n Veuillez retirer la somme demandée: ")
          if (coupureChf100 != 0){
            println(coupureChf100 + " billet(s) de 100 CHF ")
          } 
          if (coupureChf50 != 0){
             println(coupureChf50 + " billet(s) de 50 CHF ")
          }
          if (coupureChf20 != 0){
            println(coupureChf20 + " billet(s) de 20 CHF ")
          }
          if (coupureChf10 != 0){
            println(coupureChf10 + " billet(s) de 10 CHF ")
          }  
        }

          // Devise en EUR
       else if (deviseretrait == "2" || deviseretrait =="EUR") {
          
          var plafond = (comptesmontant(id)/10)*1.05

          while(montantretrait % 10 != 0) {
            println("\n le montant doit être un multiple de 10")

            montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt
          }

          while (montantretrait > plafond){
            println("\n Votre plafond de retrait autorisé est de : " + plafond)
            montantretrait = readLine("\n Indiquez le montant du retrait > ").toInt 
          }
          
          var montantrestant: Int = montantretrait 
          var coupureEur100 = 0
          var coupureEur50 = 0
          var coupureEur20 = 0
          var coupureEur10 = 0
          
        //distribution de coupure
          while (montantrestant != 0){

            // coupure de 100
            if (montantrestant >= 100){
              println("\n Il reste " + montantrestant + " EUR à distribuer.") 
              var billetmax = montantrestant / 100

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 100 EUR.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureEur100 += billetmax
                montantrestant -=  (100 * billetmax)
                
              } else {
              
                coupureEur100 += nbrbillet.toInt
                montantrestant -= (nbrbillet.toInt * 100)
                
               }
              }

            // coupure de 50
            if (montantrestant >= 50){
              println("\n Il reste " + montantrestant + " EUR à distribuer.") 
              var billetmax = montantrestant / 50

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 50 EUR.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureEur50 += billetmax
                montantrestant -=  (50 * billetmax)
                
              } else {
              
                coupureEur50 += (nbrbillet).toInt
                montantrestant -= (nbrbillet.toInt * 50)
              } 
              }

            // coupure de 20
            if (montantrestant >= 20){
              println("\n Il reste " + montantrestant + " EUR à distribuer.") 
              var billetmax = montantrestant / 20

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 20 EUR.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureEur20 += billetmax
                montantrestant -= (20 * billetmax)
                
              } else {
              
                coupureEur20 += (nbrbillet).toInt
                montantrestant -= (nbrbillet.toInt * 20)
              } 
              }

            // si autre coupure = 0
            if(coupureEur100 == 0 && coupureEur50 == 0 && coupureEur20 == 0){
              println("\n Il reste " + montantrestant + " EUR à distribuer. ")
              var billetmax = montantrestant / 10
              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10") 
              coupureEur10 += billetmax
              montantrestant -= (10 * billetmax)
              
            // coupure de 10
            } else if (montantrestant >= 10){
              println("\n Il reste " + montantrestant + " EUR à distribuer.") 
              var billetmax = montantrestant / 10

              println("\n Vous pouvez obtenir au maximum " + billetmax + " billet(s) de 10 EUR.")

              val nbrbillet : String = readLine ("\n Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")

              if(nbrbillet == "o"){
                coupureEur10 += billetmax
                montantrestant -= (10 * billetmax)
                
              } else {
              
                coupureEur10 += (nbrbillet).toInt
                montantrestant -= (nbrbillet.toInt * 10)
              }
              }
          }

        // vu d'ensemble des coupure EUR
         println("\n Veuillez retirer la somme demandée: ")
          if (coupureEur100 != 0){
            println(coupureEur100 + " billet(s) de 100 EUR ")
          } 
          if (coupureEur50 != 0){
             println(coupureEur50 + " billet(s) de 50 EUR ")
          }
          if (coupureEur20 != 0){
            println(coupureEur20 + " billet(s) de 20 EUR ")
          }
          if (coupureEur10 != 0){
            println(coupureEur10 + " billet(s) de 10 EUR ")
          }   
       }

            //convertion du retrait Eur à Chf 
        if (deviseretrait == "2" || deviseretrait == "EUR") {

          var montantconvretrait = montantretrait * 0.95
          comptesmontant(id) -= montantconvretrait
          
        } else if (deviseretrait == "1" || deviseretrait =="CHF") {
          comptesmontant(id) -= montantretrait
        }
        printf("\n Le nouveau montant disponible sur votre compte est de : %.2f \n", comptesmontant(id))
     
  } // fin def retrait

// def opération consultation
   def consultation(id: Int): Unit = {
    printf("\n Le montant disponible sur votre compte est de : %.2f \n", comptesmontant(id))
  } // fin def consultation

// def opération chagement code pin
   def changepin(id : Int, codepin : Array[String]) : Unit = {

    var nouvpin: String = "\n Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >  "
     
    do {
    nouvpin = readLine("\n Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >  ")
      
    codepin(id) = nouvpin
    if (nouvpin.length < 8){
      println("\n Votre code pin ne contient pas au moins 8 caractères")
    } else println("\n Votre nouveau code pin a été enregistré. ")
          
    } while (nouvpin.length < 8)
  } // fin def changement pin

// def opération terminer
  def terminer(): Unit = {
    println("\n Fin des opérations, n'oubliez pas de récupérer votre carte. ")  
    } // fin def terminer
      
   } // while (true) boucle code entier
  } // fin def Main
 } // object Main