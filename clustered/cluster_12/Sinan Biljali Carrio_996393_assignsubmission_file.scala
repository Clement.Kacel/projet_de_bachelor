//Assignment: Sinan Biljali Carrio_996393_assignsubmission_file

import scala.io.StdIn._

object Main {

  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")

  def depot(id: Int, comptes: Array[Double]): Unit = {

    var deviseDepot = 0
    var montantDepot = 0
    
          deviseDepot = readLine("\n Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt 

    if (deviseDepot == 1) {
      montantDepot = readLine("\n Indiquez le montant du dépôt >").toInt

      while ((montantDepot < 10) || !(montantDepot % 10 == 0)) {
        montantDepot = readLine("\n Le montant doit être un multiple de 10 >").toInt
      }

      comptes(id) += montantDepot
      printf("\n Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

    } else if (deviseDepot == 2) {
      montantDepot = readLine("\n Indiquez le montant du dépôt >").toInt

      while ((montantDepot < 10) || !(montantDepot % 10 == 0)) {
        montantDepot = readLine("\n Le montant doit être un multiple de 10 >").toInt
      }

      comptes(id) += montantDepot * 0.95
      printf("\n Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

    }

  }
  
  def retrait(id: Int, comptes: Array[Double]): Unit = {


    var nbBillets500 = 0
    var nbBillets200 = 0
    var nbBillets100 = 0
    var nbBillets50 = 0
    var nbBillets20 = 0
    var nbBillets10 = 0

    var deviseRetrait = readLine("\n Indiquez la devise du retrait : 1) CHF ; 2) EUR >").toInt

    while ((deviseRetrait < 1) || (deviseRetrait > 2)) {

      deviseRetrait = readLine("\n Indiquez la devise du retrait : 1) CHF ; 2) EUR >").toInt

    }


    var montantRetrait1 = readLine("\n Indiquez le montant du retrait >").toInt

    while ((montantRetrait1 < 10) || !(montantRetrait1 % 10 == 0)) {

      montantRetrait1 = readLine("\n Le montant doit être un multiple de 10 >").toInt

      }

    while (montantRetrait1 > comptes(id)*0.1) {

        println("\n Votre plafond autorisé est de : " + comptes(id)*0.1)
        montantRetrait1 = readLine("\n Indiquez le montant du retrait >").toInt

        while ((montantRetrait1 < 10) || !(montantRetrait1 % 10 == 0)) {

          montantRetrait1 = readLine("\n Le montant doit être un multiple de 10 >").toInt

      }

      }

    var montantRetrait2 = montantRetrait1

    if (deviseRetrait == 1) {

      var coupures = 0

        if (montantRetrait2 < 200) {
          coupures = 2
        }

        else while ((coupures < 1) || (coupures > 2)) {

        coupures = readLine("\n En 1) grosses coupures, 2) petites coupures >").toInt

        }

        if (coupures == 1) {

          var montantTemporaire1 = montantRetrait2

          if (montantTemporaire1 < 500) {

            montantTemporaire1 = montantTemporaire1*1

          } else {

          while (montantTemporaire1 >= 500) {

            nbBillets500 += 1
            montantTemporaire1 -= 500
          }

          var choix1 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets500 + " billet(s) de 500 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix1 == "o") {

            montantRetrait2 -= nbBillets500*500

          } else {

          var transformation1 = String.valueOf(choix1).toInt

            while (transformation1 >= nbBillets500) {

              choix1 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets500 + " billet(s) de 500 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

              transformation1 = String.valueOf(choix1).toInt

            }

            nbBillets500 = transformation1
            montantRetrait2 -= nbBillets500*500
          }

          }

          var montantTemporaire2 = montantRetrait2

          if (montantTemporaire2 < 200) {

            montantTemporaire2 = montantTemporaire2*1

          } else {

          while (montantTemporaire2 >= 200) {

            nbBillets200 += 1
            montantTemporaire2 -= 200
          }

          var choix2 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets200 + " billet(s) de 200 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix2 == "o") {

            montantRetrait2 -= nbBillets200*200

          } else {

            var transformation2 = String.valueOf(choix2).toInt

            while (transformation2 >= nbBillets200) {

            choix2 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets200 + " billet(s) de 200 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

              transformation2 = String.valueOf(choix2).toInt

          }

            nbBillets200 = transformation2
            montantRetrait2 -= nbBillets200*200
          }

          }

          var montantTemporaire3 = montantRetrait2

          if (montantTemporaire3 < 100) {

            montantTemporaire3 = montantTemporaire3*1

          } else {

          while (montantTemporaire3 >= 100) {

            nbBillets100 += 1
            montantTemporaire3 -= 100
          }

           var choix3 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets100 + " billet(s) de 100 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix3 == "o") {

            montantRetrait2 -= nbBillets100*100

          } else {

            var transformation3 = String.valueOf(choix3).toInt

            while (transformation3 >= nbBillets100) {

            choix3 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets100 + " billet(s) de 100 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

              transformation3 = String.valueOf(choix3).toInt

          }

            nbBillets100 = transformation3
            montantRetrait2 -= nbBillets100*100
          }

          }

          var montantTemporaire4 = montantRetrait2

          if (montantTemporaire4 < 50) {

            montantTemporaire4 = montantTemporaire4*1

          } else {

          while (montantTemporaire4 >= 50) {

            nbBillets50 += 1
            montantTemporaire4 -= 50
          }

          var choix4 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets50 + " billet(s) de 50 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix4 == "o") {

            montantRetrait2 -= nbBillets50*50

          } else {

            var transformation4 = String.valueOf(choix4).toInt

            while (transformation4 >= nbBillets50) {

            choix4 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets50 + " billet(s) de 50 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation4 = String.valueOf(choix4).toInt

          }

            nbBillets50 = transformation4
            montantRetrait2 -= nbBillets50*50
          }

          }

          var montantTemporaire5 = montantRetrait2

          if (montantTemporaire5 < 20) {

            montantTemporaire5 = montantTemporaire5*1

          } else {

          while (montantTemporaire5 >= 20) {

            nbBillets20 += 1
            montantTemporaire5 -= 20
          }

          var choix5 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets20 + " billet(s) de 20 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix5 == "o") {

            montantRetrait2 -= nbBillets20*20

          } else {

            var transformation5 = String.valueOf(choix5).toInt

            while (transformation5 >= nbBillets20) {

            choix5 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets20 + " billet(s) de 20 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation5 = String.valueOf(choix5).toInt

          }

            nbBillets20 = transformation5
            montantRetrait2 -= nbBillets20*20
          }

          }

          var montantTemporaire6 = montantRetrait2

          if (montantTemporaire6 < 10) {

            montantTemporaire6 = montantTemporaire6*1

          } else {

          while (montantTemporaire6 >= 10) {

            nbBillets10 += 1
            montantTemporaire6 -= 10
          }

            montantRetrait2 -= nbBillets10*10

          }


        } else if (coupures == 2) {

          var montantTemporaire3 = montantRetrait2

          if (montantTemporaire3 < 100) {

            montantTemporaire3 = montantTemporaire3*1

          } else {

          while (montantTemporaire3 >= 100) {

            nbBillets100 += 1
            montantTemporaire3 -= 100
          }

          var choix3 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets100 + " billet(s) de 100 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix3 == "o") {

            montantRetrait2 -= nbBillets100*100

          } else {

            var transformation3 = String.valueOf(choix3).toInt

            while (transformation3 >= nbBillets100) {

            choix3 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets100 + " billet(s) de 100 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation3 = String.valueOf(choix3).toInt

          }

            nbBillets100 = transformation3
            montantRetrait2 -= nbBillets100*100
          }

          }

          var montantTemporaire4 = montantRetrait2

          if (montantTemporaire4 < 50) {

            montantTemporaire4 = montantTemporaire4*1

          } else {

          while (montantTemporaire4 >= 50) {

            nbBillets50 += 1
            montantTemporaire4 -= 50
          }

          var choix4 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets50 + " billet(s) de 50 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix4 == "o") {

            montantRetrait2 -= nbBillets50*50

          } else {

            var transformation4 = String.valueOf(choix4).toInt

            while (transformation4 >= nbBillets50) {

            choix4 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets50 + " billet(s) de 50 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation4 = String.valueOf(choix4).toInt

          }

            nbBillets50 = transformation4
            montantRetrait2 -= nbBillets50*50
          }

          }

          var montantTemporaire5 = montantRetrait2

          if (montantTemporaire5 < 20) {

            montantTemporaire5 = montantTemporaire5*1

          } else {

          while (montantTemporaire5 >= 20) {

            nbBillets20 += 1
            montantTemporaire5 -= 20
          }

          var choix5 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets20 + " billet(s) de 20 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix5 == "o") {

            montantRetrait2 -= nbBillets20*20

          } else {

            var transformation5 = String.valueOf(choix5).toInt

            while (transformation5 >= nbBillets20) {

            choix5 = readLine("\n Il reste " + montantRetrait2 + " CHF à distribuer \n Vous pouvez obtenir au maximum " + nbBillets20 + " billet(s) de 20 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation5 = String.valueOf(choix5).toInt

          }

            nbBillets20 = transformation5
            montantRetrait2 -= nbBillets20*20
          }

          }

          var montantTemporaire6 = montantRetrait2

          if (montantTemporaire6 < 10) {

            montantTemporaire6 = montantTemporaire6*1

          } else {

          while (montantTemporaire6 >= 10) {

            nbBillets10 += 1
            montantTemporaire6 -= 10
          }

            montantRetrait2 -= nbBillets10*10

          }

        }

        println("\n Veuillez retirer la somme demandée :")
        if (nbBillets500 > 0) println(s" $nbBillets500 billet(s) de 500 CHF")
        if (nbBillets200 > 0) println(s" $nbBillets200 billet(s) de 200 CHF")
        if (nbBillets100 > 0) println(s" $nbBillets100 billet(s) de 100 CHF")
        if (nbBillets50 > 0) println(s" $nbBillets50 billet(s) de 50 CHF")
        if (nbBillets20 > 0) println(s" $nbBillets20 billet(s) de 20 CHF")
        if (nbBillets10 > 0) println(s" $nbBillets10 billet(s) de 10 CHF")

        comptes(id) -= montantRetrait1

      printf("\n Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

    } else if (deviseRetrait == 2) {

      var montantRetrait2 = montantRetrait1
      var montantTemporaire3 = montantRetrait2

          if (montantTemporaire3 < 100) {

            montantTemporaire3 = montantTemporaire3*1

          } else {

          while (montantTemporaire3 >= 100) {

            nbBillets100 += 1
            montantTemporaire3 -= 100
          }

           var choix3 = readLine("\n Il reste " + montantRetrait2 + " EUR à distribuer \n Vous pouvez obtenir au maximum " + nbBillets100 + " billet(s) de 100 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix3 == "o") {

            montantRetrait2 -= nbBillets100*100

          } else {

            var transformation3 = String.valueOf(choix3).toInt

            while (transformation3 >= nbBillets100) {

            choix3 = readLine("\n Il reste " + montantRetrait2 + " EUR à distribuer \n Vous pouvez obtenir au maximum " + nbBillets100 + " billet(s) de 100 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation3 = String.valueOf(choix3).toInt

          }

            nbBillets100 = transformation3
            montantRetrait2 -= nbBillets100*100
          }

          }

          var montantTemporaire4 = montantRetrait2

          if (montantTemporaire4 < 50) {

            montantTemporaire4 = montantTemporaire4*1

          } else {

          while (montantTemporaire4 >= 50) {

            nbBillets50 += 1
            montantTemporaire4 -= 50
          }

          var choix4 = readLine("\n Il reste " + montantRetrait2 + " EUR à distribuer \n Vous pouvez obtenir au maximum " + nbBillets50 + " billet(s) de 50 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix4 == "o") {

            montantRetrait2 -= nbBillets50*50

          } else {

            var transformation4 = String.valueOf(choix4).toInt

            while (transformation4 >= nbBillets50) {

            choix4 = readLine("\n Il reste " + montantRetrait2 + " EUR à distribuer \n Vous pouvez obtenir au maximum " + nbBillets50 + " billet(s) de 50 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation4 = String.valueOf(choix4).toInt

          }

            nbBillets50 = transformation4
            montantRetrait2 -= nbBillets50*50
          }

          }

          var montantTemporaire5 = montantRetrait2

          if (montantTemporaire5 < 20) {

            montantTemporaire5 = montantTemporaire5*1

          } else {

          while (montantTemporaire5 >= 20) {

            nbBillets20 += 1
            montantTemporaire5 -= 20
          }

          var choix5 = readLine("\n Il reste " + montantRetrait2 + " EUR à distribuer \n Vous pouvez obtenir au maximum " + nbBillets20 + " billet(s) de 20 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

          if (choix5 == "o") {

            montantRetrait2 -= nbBillets20*20

          } else {

            var transformation5 = String.valueOf(choix5).toInt

            while (transformation5 >= nbBillets20) {

            choix5 = readLine("\n Il reste " + montantRetrait2 + " EUR à distribuer \n Vous pouvez obtenir au maximum " + nbBillets20 + " billet(s) de 20 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString

            transformation5 = String.valueOf(choix5).toInt

          }

            nbBillets20 = transformation5
            montantRetrait2 -= nbBillets20*20
          }

          }

         var montantTemporaire6 = montantRetrait2

          if (montantTemporaire6 < 10) {

            montantTemporaire6 = montantTemporaire6*1

          } else {

          while (montantTemporaire6 >= 10) {

            nbBillets10 += 1
            montantTemporaire6 -= 10
          }

            montantRetrait2 -= nbBillets10*10

        }

          println("\n Veuillez retirer la somme demandée :")
          if (nbBillets500 > 0) println(s" $nbBillets500 billet(s) de 500 EUR")
          if (nbBillets200 > 0) println(s" $nbBillets200 billet(s) de 200 EUR")
          if (nbBillets100 > 0) println(s" $nbBillets100 billet(s) de 100 EUR")
          if (nbBillets50 > 0) println(s" $nbBillets50 billet(s) de 50 EUR")
          if (nbBillets20 > 0) println(s" $nbBillets20 billet(s) de 20 EUR")
          if (nbBillets10 > 0) println(s" $nbBillets10 billet(s) de 10 EUR")

        comptes(id) -= montantRetrait1*0.95

      printf("\n Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

      }
    
  }

  def consultation(id: Int, comptes: Array[Double]): Unit = {

    printf("\n Le montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
    
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {

    codespin(id) = readLine("\n Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString

    while (codespin(id).length < 8) {
      println("\n Votre code pin ne contient pas au moins 8 caractères.")
      codespin(id) = readLine("\n Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString
    }

    if (codespin(id).length > 7) {
      println("\n Votre code pin a bien été mis à jour.")
    }
    
  }


  
  def main(args: Array[String]): Unit = {

    var identifiant = readLine("Saisissez votre code identifiant >").toInt
    var verification = 0
    var choix = 1
    var codePINUtilisateur = "0"

    while ((choix > 0) && (choix < 6)) {
      
    while (verification == 0) {

      if (identifiant >= nbclients) {
        println("Cet identifiant n'est pas valable.")
        verification = 1
      } else {
      
      var tentativesRestantes = 2
      codePINUtilisateur = readLine("\n Saisissez votre code PIN >").toString

        if (codePINUtilisateur == codespin(identifiant)) {
          tentativesRestantes = tentativesRestantes*1
          verification = 2
        }
        
      while (!(codePINUtilisateur == codespin(identifiant)) && !(tentativesRestantes == 0)) {

        codePINUtilisateur = readLine("\n Code PIN erroné, il vous reste " + tentativesRestantes + " tentatives >").toString

        if (codePINUtilisateur == codespin(identifiant)) {
          tentativesRestantes = tentativesRestantes*1
          verification = 2
        } else {
        tentativesRestantes -= 1
        }

      }

        if (tentativesRestantes == 0) {

        println("\n Trop d'erreurs, abandon de l'identification.")
        identifiant = readLine("\n Saisissez votre code identifiant >").toInt
          
      }
    }
  }
    if (verification == 1) {
      System.exit(0)
    } else if (verification == 2) {
    
    var choix = readLine("\n Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt

    while ((choix > 0) && (choix < 5)) {

      if (choix == 1) {
        depot(identifiant, comptes)
        choix = readLine("\n Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
      } else if (choix == 2) {
        retrait(identifiant, comptes)
        choix = readLine("\n Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
      } else if (choix == 3) {
        consultation(identifiant, comptes)
        choix = readLine("\n Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
      } else if (choix == 4) {
        changepin(identifiant, codespin)
        choix = readLine("\n Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
      }
      
    }
      
    if (choix == 5) {
      println("\n Fin des opérations, n'oubliez pas de récupérer votre carte.")
      verification = 0
      identifiant = readLine("\n Saisissez votre code identifiant >").toInt
      }
    
  }

  }

}
  
}