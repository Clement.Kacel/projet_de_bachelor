//Assignment: Lyna Tahriou_996440_assignsubmission_file

//importation des librairies utiles au projet
import scala.io.StdIn._
import scala.math

object Main {

  var depotDevise = 0
  var retraitDevise =0

  var depotMontant = 0
  var retraitMontant = 0
  var entree_programme = 0
  var plafond = 0.0

  var value_inf=0
  var o=""
  var identifiant=0
  var indic_denom=0

  //Declaration des variables
  var nbclients=100
  var comptes = Array.fill(nbclients)(1200.00)
  var codespin = Array.fill(nbclients)("INTRO1234")

  def main(args: Array[String]): Unit = {

var essaiCodePin = 3

var saisieDuPin = ""
var choix_utilisateur = 0
var bon_pin:Boolean = false
var deja_enregistre:Boolean=false  //booléen pour savoir si faut redemander l'id
 while (identifiant<nbclients) {  
          if(!deja_enregistre)
         {  //si on est déconnecté
           do{
             identifiant = readLine("Saisissez votre code identifiant >").toInt
             if(identifiant>nbclients-1)
             {
               println("Cet identifiant n’est pas valable. ")
             }
           }while(identifiant<0)  //nbclients-1 car on commence à 0 et on va jusqu'à 99 et non pas juqu'à nbclients c'est comme ça les array
         }

          if(identifiant<nbclients)  //Si la saisie de l'identifiant est bonne, on accède au programme
          {
            if (entree_programme == 0){
                      //il faut demander le code pin
                      do
                      {
                        saisieDuPin = readLine("Saisissez votre code pin >")
                        if (saisieDuPin != codespin(identifiant))
                        {  //mauvaise saisie
                          essaiCodePin -=  1
                          if (essaiCodePin == 0){
                            //au bout de 3 essais, le programme se termine
                            println ("Trop d’erreurs, abandon de l’identification")
                            saisieDuPin=codespin(identifiant)  //pour sortir du while
                            deja_enregistre=false  //pour redemander l'id
                            bon_pin=false
                          }
                        else
                          {  //si il reste encore des essais, on l'écrit
                            print("Code pin erroné, il vous reste ")
                            print(essaiCodePin)
                            println(" tentatives >")
                          }
                        }
                        else
                        {
                          entree_programme +=  1  //somme de entree programme + 1
                          bon_pin=true
                          deja_enregistre=true
                        }
                      }while (saisieDuPin != codespin(identifiant))
                      essaiCodePin=3
                      } 

                  if(bon_pin){
                    do{  //executer cette boucle au moins 1x et tant que le choix est inférieur à 1 ou supérieur à 5
                       println("Choisissez votre opération :")
                       println("1) Dépôt")
                       println("2) Retrait")
                       println("3) Consultation du compte")
                       println("4) Changement du code pin")
                       println("5) Terminer")

                      choix_utilisateur = readLine("Votre choix : ").toInt  //lecture de la saisie du choix et conversion en int 

                      //si le choix n'est pas bon, on demande a l'utilisateur de le resaisir
                      if (choix_utilisateur < 1 || choix_utilisateur > 5){
                        println("Erreur, veuillez saisir à nouveau votre choix :  \n")
                      }
                      //Si le choix est 5, c'est fini
                      else if (choix_utilisateur == 5){
                        entree_programme=0
                         println("Fin des opérations, n’oubliez pas de récupérer votre carte.")

                        deja_enregistre=false
                      }
                    }while(choix_utilisateur<1 || choix_utilisateur>5)


                  }



              if (choix_utilisateur == 1){
                depot(identifiant,comptes)  //on appelle la fonction depot avec 2 paramètres
              }
              else if (choix_utilisateur == 2){
                retrait(identifiant,comptes)
              }
              else if (choix_utilisateur == 3)
              {  //affichage du montant disponible
                printf("Le montant disponible sur votre compte est de: %.2f", comptes(identifiant))
               println(" CHF")
              }
              else if(choix_utilisateur==4)
             {
               changepin(identifiant,codespin)
             }
            if(choix_utilisateur==5){
              choix_utilisateur=0  //pour remettre à 0 et pas terminer la prochaine fois
            }

          }
  }

}

  def depot(id : Int, comptes : Array[Double]) : Unit = 
  {
    //si la saisie est 1 alors c'est un depot
        //depotDevise est initialise à 0 donc ni 1 ni 2, on rentre dans la boucle et on en ressort pas tant qu'on a ni 1 ni 2
          depotDevise=0
        while(depotDevise!=1 && depotDevise!=2)
        {
          depotDevise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
        }

          //depotMontant est initialisé à 0 donc on rentre dans la boucle et on en ressort pas tant que le depot n'est pas un multiple de 10 positif
          depotMontant=0
        while(depotMontant<=0 || depotMontant%10!=0){
          depotMontant = readLine("Indiquez le montant du dépôt >").toInt
          if(depotMontant<=0)
          {
            println("Le montant doit être positif")
          }
          else if (depotMontant % 10 != 0)
          {
            println("Le montant doit être un multiple de 10")
          }
        }

        //Si on a choisit en EURO, il faut faire la conversion avec le taux de conversion = 0.95
        if (depotDevise == 2){
                comptes(id) = comptes(id) + (depotMontant*0.95)
        }  
    else {  //sinon on fait la somme du depot + du compte actuel
      comptes(id) += depotMontant
    }
        //AFFICHAGE DU comptes ACTUEL
        print("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : ")
        print(comptes(id))
        println(" CHF.")
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = 
  {
    //le choix 2 fait référence au retrait 

    var billet50 = 0
     var billet20 = 0
     var billet10 = 0

      var billet500 = 0
     var billet200 = 0
     var billet100 = 0
    retraitDevise=0
    indic_denom=0
    var plafond = comptes(id) / 10  //variable du plafond = 10% du comptes

    //tant que la devise n'est pas 1 ou 2 on redemande  
    while(retraitDevise != 1 && retraitDevise != 2){
      retraitDevise = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
    }

    do{  //on demande le montant en verifiant que c'est positif, multiple de 10
      retraitMontant = readLine("Indiquez le montant du retrait >").toInt
      if (retraitMontant % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
      }
    }while (retraitMontant<=0 || retraitMontant % 10 != 0 )


    while (retraitMontant > plafond)  //si le montant est > à 10% on redemande et on revérifie les conditions
      {
        printf("Votre plafond de retrait autorisé est de: %.2f \n", plafond)
        do
        {
          retraitMontant = readLine("Indiquez le montant du retrait >").toInt
        }while(retraitMontant<0 || retraitMontant%10!=0 || retraitMontant>plafond )
      }

    if(retraitDevise == 1){  //si on retire en CHF
      comptes(id) = (comptes(id) - retraitMontant)

      if(retraitMontant>=200)
      {
        //si c'est >=200 alors on a le choix entre grosses coupures et petite coupure
        var coupure=0

        while(coupure != 1 && coupure != 2)
        {
          coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
        } 


        if (coupure == 1) { // le cas de grosse coupure

           while (retraitMontant != 0) {
             println("Il reste " + retraitMontant + " CHF à distribuer")
             if (retraitMontant >= 500){

               billet500 = retraitMontant/500  //on divise par 500 et pas 500.0 comme ça si on a 900/500 ça donne 1
               print("Vous pouvez obtenir au maximum ")
              print(billet500)
              println(" billet(s) de 500 CHF")
               do
               {
                  o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 if(o!="o")
                 {
                   //si c'est pas egal à 'o' c'est que c'est une valeur
                    value_inf = o.toInt


                    while(value_inf<0 || value_inf>billet500)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                    {
                      value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                    }
                    billet500 = value_inf

                    retraitMontant = retraitMontant - (billet500 * 500)  //on soustrait au retrait le nombre de billet de 500*500
                    o="o"  //pour pouvoir sortir du while on met o="o"

                 }
                 else
                 {
                   //sinon, on garde le reste de la division par 500
                    retraitMontant = retraitMontant%500
                 }
               }while(o!="o")

             }

              if (retraitMontant >= 200){
                billet200 = retraitMontant/200
                println("Il reste " + retraitMontant + " CHF à distribuer")
                print("Vous pouvez obtenir au maximum ")
                print(billet200)
                println(" billet(s) de 200 CHF")
                 do
                 {
                    o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 if(o!="o")
                  {
                    //si c'est pas egal à 'o' c'est que c'est une valeur
                     value_inf = o.toInt


                     while(value_inf<0 || value_inf>billet200)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                     {
                       value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                     }
                     billet200 = value_inf

                     retraitMontant = retraitMontant - (billet200 * 200)
                     o="o"

                  }
                  else
                  {
                   //pareil
                     retraitMontant = retraitMontant%200
                  }
                 }while(o!="o")

             }
              if (retraitMontant >= 100){
                println("Il reste " + retraitMontant + " CHF à distribuer")
                billet100 = retraitMontant/100
                print("Vous pouvez obtenir au maximum ")
                print(billet100)
                println(" billet(s) de 100 CHF")
                 do
                 {
                    o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 if(o!="o")
                  {
                    //si c'est pas egal à 'o' c'est que c'est une valeur
                     value_inf = o.toInt


                     while(value_inf<0 || value_inf>billet100)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                     {
                       value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                     }
                     billet100 = value_inf

                     retraitMontant = retraitMontant - (billet100 * 100)  //on soustrait au retrait le nombre de billet de 100*100
                     o="o"  //pour pouvoir sortir du while on met o="o"

                  }
                  else
                  {
                    //sinon, on garde le reste de la division par 100
                     retraitMontant = retraitMontant%100
                  }
                 }while(o!="o")

             }
              if (retraitMontant >= 50){
                println("Il reste " + retraitMontant + " CHF à distribuer")
                billet50 = retraitMontant/50
                print("Vous pouvez obtenir au maximum ")
                print(billet50)
                println(" billet(s) de 50 CHF")
                 do
                 {
                    o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 if(o!="o")
                  {
                    //si c'est pas egal à 'o' c'est que c'est une valeur
                     value_inf = o.toInt


                     while(value_inf<0 || value_inf>billet50)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                     {
                       value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                     }
                     billet50 = value_inf

                     retraitMontant = retraitMontant - (billet50 * 50)  //on soustrait au retrait le nombre de billet de 50*50
                     o="o"  //pour pouvoir sortir du while on met o="o"

                  }
                  else
                  {
                    //sinon, on garde le reste de la division par 50
                     retraitMontant = retraitMontant%50
                  }
                 }while(o!="o")

             }
              if (retraitMontant >= 20){
                println("Il reste " + retraitMontant + " CHF à distribuer")
                billet20 = retraitMontant/20
                print("Vous pouvez obtenir au maximum ")
                print(billet20)
                println(" billet(s) de 20 CHF")
                 do
                 {
                    o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 if(o!="o")
                  {
                    //si c'est pas egal à 'o' c'est que c'est une valeur
                     value_inf = o.toInt


                     while(value_inf<0 || value_inf>billet20)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                     {
                       value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                     }
                     billet20 = value_inf

                     retraitMontant = retraitMontant - (billet20 * 20)  //on soustrait au retrait le nombre de billet de 20*20
                     o="o"  //pour pouvoir sortir du while on met o="o"

                  }
                  else
                  {
                    //sinon, on garde le reste de la division par 20
                     retraitMontant = retraitMontant%20
                  }
                 }while(o!="o")

             }

              if (retraitMontant >= 10){  //on ne laisse pas me choix à l'utilisateur du nombre de billets pour 10 car c'est le minimum
                println("Il reste " + retraitMontant + " CHF à distribuer")
                billet10 = retraitMontant/10
                print("Vous pouvez obtenir au maximum ")
                print(billet10)
                println(" billet(s) de 10 CHF")

                  retraitMontant = retraitMontant - (billet10 * 10)


             }
             //affichage du nombre de billets
             if(billet500>0){
               print(billet500)
               println(" billet(s) de 500 CHF ")
             }
             if(billet200>0){
               print(billet200)
               println(" billet(s) de 200 CHF ")
             }
             if(billet100>0){
               print(billet100)
               println(" billet(s) de 100 CHF ")
             }
             if(billet50>0){
               print(billet50)
               println(" billet(s) de 50 CHF ")
             }
             if(billet20>0){
               print(billet20)
               println(" billet(s) de 20 CHF ")
             }
             if(billet10>0){
               print(billet10)
               println(" billet(s) de 10 CHF ")
             }
           }
          }


          if (coupure==2){  //pour le cas des petites coupures

            indic_denom=1;  //on met à 1 cet indicateur qui va nous permettre ensuite de réaliser le retrait dans le cas d'un montant <=200 pour ne pas ré-écrire tout le code
              }
          }       
          //si le montant est inférieur à 200CHF ou qu'il est supérieur mais qu'on veut des petites coupures
          if(retraitMontant<200 || indic_denom==1){

           while (retraitMontant!= 0){
           if (retraitMontant >= 100){
               println("Il reste " + retraitMontant + " CHF à distribuer")
               billet100 = retraitMontant/100
               print("Vous pouvez obtenir au maximum ")
             print(billet100)
             println(" billet(s) de 100 CHF")
                do
                {
                   o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                if(o!="o")
                 {
                   //si c'est pas egal à 'o' c'est que c'est une valeur
                    value_inf = o.toInt


                    while(value_inf<0 || value_inf>billet100)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                    {
                      value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                    }
                    billet100 = value_inf

                    retraitMontant = retraitMontant - (billet100 * 100)  //on soustrait au retrait le nombre de billet de 100*100
                    o="o"  //pour pouvoir sortir du while on met o="o"

                 }
                 else
                 {
                   //sinon, on garde le reste de la division par 100
                    retraitMontant = retraitMontant%100
                 }
                }while(o!="o")

            }
             if (retraitMontant >= 50){
               println("Il reste " + retraitMontant + " CHF à distribuer")
               billet50 = retraitMontant/50
               print("Vous pouvez obtenir au maximum ")
               print(billet50)
               println(" billet(s) de 50 CHF")
                do
                {
                   o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  if(o!="o")
                   {
                     //si c'est pas egal à 'o' c'est que c'est une valeur
                      value_inf = o.toInt


                      while(value_inf<0 || value_inf>billet50)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                      {
                        value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                      }
                      billet50 = value_inf

                      retraitMontant = retraitMontant - (billet50 * 50)  //on soustrait au retrait le nombre de billet de 50*50
                      o="o"  //pour pouvoir sortir du while on met o="o"

                   }
                   else
                   {
                     //sinon, on garde le reste de la division par 500
                      retraitMontant = retraitMontant%50
                   }                
                }while(o!="o")

            }
             if (retraitMontant >= 20){  //si il reste plus que 20 -
               println("Il reste " + retraitMontant + " CHF à distribuer")
               billet20 = retraitMontant/20
               print("Vous pouvez obtenir au maximum ")
               print(billet20)
               println(" billet(s) de 20 CHF")
                do
                {
                   o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                if(o!="o")
                 {
                   //si c'est pas egal à 'o' c'est que c'est une valeur
                    value_inf = o.toInt


                    while(value_inf<0 || value_inf>billet20)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                    {
                      value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                    }
                    billet20 = value_inf

                    retraitMontant = retraitMontant - (billet20 * 20)  //on soustrait au retrait le nombre de billet de 20*20
                    o="o"  //pour pouvoir sortir du while on met o="o"

                 }
                 else
                 {
                   //sinon, on garde le reste de la division par 500
                    retraitMontant = retraitMontant%20
                 }
                }while(o!="o")

            }
             if (retraitMontant >= 10){
               println("Il reste " + retraitMontant + " CHF à distribuer")
               billet10 = retraitMontant/10
               print("Vous pouvez obtenir au maximum ")
               print(billet10)
               println(" billet(s) de 10 CHF")

                 retraitMontant = retraitMontant - (billet10 * 10)


            }
              //on affiche les billets a distribuer
                if (billet100>0){
                  print(billet100)
                  println(" billet(s) de 100 CHF ")
                }
                if (billet50>0){
                  print(billet50)
                  println(" billet(s) de 50 CHF ")
                }
                if (billet20>0){
                  print(billet20)
                  println(" billet(s) de 20 CHF ")
                }
                if (billet10>0){
                  print(billet10)
                  println(" billet(s) de 10 CHF ")
                }
            }   
          }  
      }  
    //devise en euro
      else{

        var retraitEur = 0.95*retraitMontant  //il faut d'abord convertir en euro en appliquant *0.95
         comptes(id) = (comptes(id) - retraitEur)
        while (retraitMontant!= 0){
          //en commencant par les billets de 100 qui sont la plus haute coupure pour des euros
        if (retraitMontant >= 100){
            println("Il reste " + retraitMontant + " EUR à distribuer")
            billet100 = retraitMontant/100
            print("Vous pouvez obtenir au maximum ")
          print(billet100)
          println(" billet(s) de 100 EUR")
             do
             {
                o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
             if(o!="o")
              {
                //si c'est pas egal à 'o' c'est que c'est une valeur
                 value_inf = o.toInt


                 while(value_inf<0 || value_inf>billet100)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                 {
                   value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                 }
                 billet100 = value_inf

                 retraitMontant = retraitMontant - (billet100 * 100)  //on soustrait au retrait le nombre de billet de 100*100
                 o="o"  //pour pouvoir sortir du while on met o="o"

              }
              else
              {
                //sinon, on garde le reste de la division par 500
                 retraitMontant = retraitMontant%100
              }
             }while(o!="o")

         }
          if (retraitMontant >= 50){
            println("Il reste " + retraitMontant + " EUR à distribuer")
            billet50 = retraitMontant/50
            println("Vous pouvez obtenir au maximum ")
            print(billet50)
            println(" billet(s) de 50 EUR")
             do
             {  //saisie de o pour ok ou valeu inférieure
                o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
             if(o!="o")
              {
                //si c'est pas egal à 'o' c'est que c'est une valeur
                 value_inf = o.toInt


                 while(value_inf<0 || value_inf>billet50)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                 {
                   value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                 }
                 billet50 = value_inf

                 retraitMontant = retraitMontant - (billet50 * 50)  //on soustrait au retrait le nombre de billet de 50*50
                 o="o"  //pour pouvoir sortir du while on met o="o"

              }
              else
              {
                //sinon, on garde le reste de la division par 50
                 retraitMontant = retraitMontant%50
              }
             }while(o!="o")

         }
          if (retraitMontant >= 20){
            println("Il reste " + retraitMontant + " EUR à distribuer")
            billet20 = retraitMontant/20
            print("Vous pouvez obtenir au maximum ")
            print(billet20)
            println(" billet(s) de 20 EUR")
             do
             {
                o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
             if(o!="o")
              {
                //si c'est pas egal à 'o' c'est que c'est une valeur
                 value_inf = o.toInt


                 while(value_inf<0 || value_inf>billet20)  //il faut verifier que la saisie ne soit pas negative et ne soit pas > au maximum
                 {
                   value_inf = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toInt
                 }
                 billet20 = value_inf

                 retraitMontant = retraitMontant - (billet20 * 20)  //on soustrait au retrait le nombre de billet de 20*20
                 o="o"  //pour pouvoir sortir du while on met o="o"

              }
              else
              {
                //sinon, on garde le reste de la division par 20
                 retraitMontant = retraitMontant%20
              }
             }while(o!="o")

         }
          if (retraitMontant >= 10){
            println("Il reste " + retraitMontant + " EUR à distribuer")
            billet10 = retraitMontant/10  //division euclidienne
            print("Vous pouvez obtenir au maximum ")
            print(billet10)
            println(" billet(s) de 10 EUR")

              retraitMontant = retraitMontant - (billet10 * 10)

         }

          println("Veuillez retirer la somme demandée :")
          if (billet100>0){
            print(billet100)
            println(" billet(s) de 100 EUR ")
          }
          if (billet50>0){
            print(billet50)
            println(" billet(s) de 50 EUR ")
          }
          if (billet20>0){
            print(billet20)
            println(" billet(s) de 20 EUR ")
          }
          if (billet10>0){
            print(billet10)
            println(" billet(s) de 10 EUR ")
          }

        }

        comptes(id) = math.floor(comptes(id) * 10)/10  //pas forcement utile
      }

    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de:  %.2f", comptes(id))
    println(" CHF")

  }

  def changepin(id : Int, codespin : Array[String]) : Unit = 
  {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var nvpin = readLine()  //lecture 
    while(nvpin.length<8){  //Tant que c'est <8, il faut demander encore
    println("Votre code pin ne contient pas au moins 8 caractères.\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      nvpin = readLine()  //et lire à nouveau
    }
    codespin(id) = nvpin  //on change l'ancien pin par le nouveau
  }
}










