//Assignment: Mélissa Gonçalves Pereira_996610_assignsubmission_file

  import scala.io.StdIn._
  object Main {

    //def depot 
    def depot(id : Int, comptes : Array[Double]) : Unit = {
      println("1) CHF")
      println("2) EUR")
      println("Indiquez la devise du dépôt: ")
      var deviseD = readInt()
      var montantD = 0.0
      println("Indiquez le montant du dépôt: ")
      montantD = readInt()
      //conditions du montant dépôt
      while ((montantD%10 != 0)||(montantD==0)) { 
        println("Le montant doit être un multiple de 10.")
        println("Indiquez le montant du dépôt: ")
        montantD = readInt() 
      }
      if (deviseD==1) {
       // montantBANQUE = montantBANQUE+montantD
        comptes(id) += montantD
      }
      if (deviseD==2) {
        montantD = montantD * 0.95 //conversion
      //montantBANQUE = montantD + montantBANQUE
        comptes(id) += montantD
      }
      printf("Votre dépôt à été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f \n ", comptes(id) ," CHF") 
    } // fin def depot

    //def retrait
    def retrait(id : Int, comptes : Array[Double]) : Unit = {
      var decompte = 0
      var montantR = 0.0 
      var typecoupures = 0
      var ok = "o"
      var nbbillets500 = 0
      var nbbillets200 = 0 
      var nbbillets100 = 0
      var nbbillets50 = 0
      var nbbillets20 = 0
      var nbbillets10 = 0
      println("1) CHF")
      println("2) EUR")
      println("Indiquez la devise: ")
      var deviseR = readInt()
      //boucle devise
      while ((deviseR!=1)&&(deviseR!=2)){
        println("Indiquez la devise: ")
        deviseR = readInt()
      }
      println("Indiquez le montant du retrait: ")
      montantR = readInt()
      //conditions du montant retrait
      if (deviseR==1){
        while ((montantR>(comptes(id)/10))||(montantR%10 !=0)||(montantR==0)){
          println("Votre plafond de retrait autorisé est de: "+ (comptes(id)/10))
          println("Le montant doit être un multiple de 10.")
          println("Indiquez le montant du retrait: ")
          montantR = readInt() 
      }
      //CHF grand montant 
      if ((deviseR==1)&&(montantR>=200)) {
        println("1) grosses coupures")
        println("2) petites coupures")
        println("Indiquez le type de coupures: ")
        typecoupures = readInt()
        while ((typecoupures!=1) && (typecoupures!=2)){
          println("Indiquez le type de coupures: ")
          typecoupures = readInt()
        }
      }
      //grosses coupures 
      if (typecoupures==1){
        println("Il reste "+montantR+" CHF à distribuer")
      //billets500
        if (montantR>=500){
          while (montantR>=500){
            montantR = montantR - 500
            decompte = decompte + 1
          }
          println("Vous pouvez obtenir au maximum "+decompte+" billets de 500 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
          ok = readLine()
          if (ok=="o"){ 
            nbbillets500=decompte
          }
          if (ok!="o"){
            nbbillets500=(ok.toInt) 
          } //forcer la valeur (String à Int)
          while (nbbillets500>decompte){
            println("Vous pouvez obtenir au maximum "+decompte+" billets de 500 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
            ok = readLine()
            if (ok=="o"){ 
              nbbillets500=decompte
            }
            if (ok!="o"){
              nbbillets500=(ok.toInt)
            }
          }
          println("Vous avez choisi "+nbbillets500+" billets de 500 CHF")//pas nécessaire
          montantR= ((montantR+ decompte*500)-(nbbillets500*500)) 
          decompte=0 
          println("Il reste "+montantR+" CHF à distribuer")
        }
        //billets200
        if (montantR>=200){
          while (montantR>=200){
            montantR = montantR - 200
            decompte = decompte + 1
          }
          println("Vous pouvez obtenir au maximum "+decompte+" billets de 200 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
          ok = readLine()
          if (ok=="o"){
            nbbillets200=decompte
          }
          if (ok!="o"){
            nbbillets200=(ok.toInt)
          }
          while (nbbillets200>decompte){
            println("Vous pouvez obtenir au maximum "+decompte+" billets de 500 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
            ok = readLine()
            if (ok=="o"){
              nbbillets200=decompte
            }
            if (ok!="o"){
              nbbillets200=(ok.toInt)
            }
          }
          println("Vous avez choisi "+nbbillets200+" billets de 200 CHF")//pas nécessaire
          montantR= ((montantR+ decompte*200)-(nbbillets200*200))
          decompte=0 
          println("Il reste "+montantR+" CHF à distribuer") 
        }
        //billets100
        if (montantR>=100){
          while (montantR>=100){
            montantR = montantR - 100
            decompte = decompte + 1
          }
          println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
          ok = readLine()
          if (ok=="o"){
            nbbillets100=decompte
          }
          if (ok!="o"){
            nbbillets100=(ok.toInt)
          }
          while (nbbillets100>decompte){
            println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
            ok = readLine()
            if (ok=="o"){
              nbbillets100=decompte
            }
            if (ok!="o"){
              nbbillets100=(ok.toInt)
            }
          }
          println("Vous avez choisi "+nbbillets100+" billets de 100 CHF")//pas nécessaire 
                    montantR= ((montantR+ decompte*100)-(nbbillets100*100))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets50
                  if (montantR>=50){
                    while (montantR>=50){
                      montantR = montantR - 50
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets50=decompte
                    }
                    if (ok!="o"){
                      nbbillets50=(ok.toInt)
                    }
                    while (nbbillets50>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine() 
                      if (ok=="o"){
                        nbbillets50=decompte
                      }
                      if (ok!="o"){
                        nbbillets50=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets50+" billets de 50 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*50)-(nbbillets50*50))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets20
                  if (montantR>=20){
                    while (montantR>=20){
                      montantR = montantR - 20
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine() 
                    if (ok=="o"){
                      nbbillets20=decompte
                    }
                    if (ok!="o"){
                      nbbillets20=(ok.toInt)
                    }
                    while (nbbillets20>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine() 
                      if (ok=="o"){
                        nbbillets20=decompte
                      }
                      if (ok!="o"){
                        nbbillets20=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets20+" billets de 20 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*20)-(nbbillets20*20))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  } //reste pour billets de 10
      //billets10
                  if (montantR>=10){ 
                    while (montantR>=10){
                      montantR = montantR - 10
                      decompte = decompte + 1
                    }
                    nbbillets10= decompte
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  } // car distribué en billets de 10
      //total des billets
                  println("Veuillez retirer la somme demandée:")
                  if (nbbillets500>0){
                    println(nbbillets500+" billet(s) de 500 CHF")
                  }
                  if (nbbillets200>0){
                    println(nbbillets200+" billet(s) de 200 CHF")
                  }
                  if (nbbillets100>0){
                    println(nbbillets100+" billet(s) de 100 CHF")
                  }
                  if (nbbillets50>0){
                    println(nbbillets50+" billet(s) de 50 CHF")
                  }
                  if (nbbillets20>0){
                    println(nbbillets20+" billet(s) de 20 CHF")
                  }
                  if (nbbillets10>0){
                    println(nbbillets10+" billet(s) de 10 CHF")
                  }
                  comptes(id) -= (nbbillets500*500+nbbillets200*200+nbbillets100*100+nbbillets50*50+nbbillets20*20+nbbillets10*10)
                  printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f \n ", comptes(id) ," CHF")
                }//fin de coupures 1 (grandes) pour grosse somme
      //petites coupures pour grosse somme
                if (typecoupures==2){
                  println("Il reste "+montantR+" CHF à distribuer")
      //billets100
                  if (montantR>=100){
                    while (montantR>=100){
                      montantR = montantR - 100
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets100=decompte
                    }
                    if (ok!="o"){
                      nbbillets100=(ok.toInt)
                    }
                    while (nbbillets100>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine()
                      if (ok=="o"){
                        nbbillets100=decompte
                      }
                      if (ok!="o"){
                        nbbillets100=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets100+" billets de 100 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*100)-(nbbillets100*100))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets50
                  if (montantR>=50){
                    while (montantR>=50){
                      montantR = montantR - 50
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets50=decompte
                    }
                    if (ok!="o"){
                      nbbillets50=(ok.toInt)
                    }
                    while (nbbillets50>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine()
                      if (ok=="o"){
                        nbbillets50=decompte
                      }
                      if (ok!="o"){
                        nbbillets50=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets50+" billets de 50 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*50)-(nbbillets50*50))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets20
                  if (montantR>=20){
                    while (montantR>=20){
                      montantR = montantR - 20
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets20=decompte
                    }
                    if (ok!="o"){
                    nbbillets20=(ok.toInt)
                    }
                    while (nbbillets20>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine() 
                      if (ok=="o"){
                        nbbillets20=decompte
                      }
                      if (ok!="o"){
                        nbbillets20=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets20+" billets de 20 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*20)-(nbbillets20*20))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets10
                  if (montantR>=10){
                    while (montantR>=10){
                      montantR = montantR - 10
                      decompte = decompte + 1
                    }
                    nbbillets10= decompte
                    decompte=0
                    println("Il reste "+montantR+" CHF à distribuer")
                  } // 0 car ditribué en billets de 10
                  println("Veuillez retirer la somme demandée:")
                  if (nbbillets100>0){
                    println(nbbillets100+" billet(s) de 100 CHF")
                  }
                  if (nbbillets50>0){
                    println(nbbillets50+" billet(s) de 50 CHF")
                  }
                  if (nbbillets20>0){
                    println(nbbillets20+" billet(s) de 20 CHF")
                  }
                  if (nbbillets10>0){
                    println(nbbillets10+" billet(s) de 10 CHF")
                  } 
                  comptes(id) -= (nbbillets100*100+nbbillets50*50+nbbillets20*20+nbbillets10*10)
                  printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f \n ", comptes(id) ," CHF")
                }//fin de coupures 2 (petites) pour grosse somme
      // retrait petite somme SEULEMENT en petites coupures
                if ((deviseR==1)&&(montantR<200)&&(montantR>0)){
              // println("seulement en petites coupures car retrait infèrieur à 200")
                  println("Il reste "+montantR+" CHF à distribuer")
      //billets100
                  if (montantR>=100){
                    while (montantR>=100){
                      montantR = montantR - 100
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets100=decompte
                    }
                    if (ok!="o"){
                      nbbillets100=(ok.toInt)
                    }
                    while (nbbillets100>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine()
                      if (ok=="o"){
                        nbbillets100=decompte
                      }
                      if (ok!="o"){
                        nbbillets100=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets100+" billets de 100 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*100)-(nbbillets100*100))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets50
                  if (montantR>=50){
                    while (montantR>=50){
                      montantR = montantR - 50
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets50=decompte
                    }
                    if (ok!="o"){
                      nbbillets50=(ok.toInt)
                    }   
                    while (nbbillets50>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine()
                      if (ok=="o"){
                        nbbillets50=decompte
                      }
                      if (ok!="o"){
                        nbbillets50=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets50+" billets de 50 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*50)-(nbbillets50*50))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
       //billets20
                  if (montantR>=20){
                    while (montantR>=20){
                      montantR = montantR - 20
                      decompte = decompte + 1
                    }
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets20=decompte
                    }
                    if (ok!="o"){
                      nbbillets20=(ok.toInt)
                    }
                    while (nbbillets20>decompte){
                      println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 CHF")
                      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                      ok = readLine()
                      if (ok=="o"){
                        nbbillets20=decompte
                      }
                      if (ok!="o"){
                        nbbillets20=(ok.toInt)
                      }
                    }
                    println("Vous avez choisi "+nbbillets20+" billets de 20 CHF")//pas nécessaire
                    montantR= ((montantR+ decompte*20)-(nbbillets20*20))
                    decompte=0 
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
      //billets10
                  if (montantR>=10){
                    while (montantR>=10){
                      montantR = montantR - 10
                      decompte = decompte + 1
                    }
                    nbbillets10= decompte
                    decompte=0
                    println("Il reste "+montantR+" CHF à distribuer")
                  }
                  println("Veuillez retirer la somme demandée:")
                  if (nbbillets100>0){
                    println(nbbillets100+" billet(s) de 100 CHF")
                  }
                  if (nbbillets50>0){
                    println(nbbillets50+" billet(s) de 50 CHF")
                  }
                  if (nbbillets20>0){
                    println(nbbillets20+" billet(s) de 20 CHF")
                  }
                  if (nbbillets10>0){
                    println(nbbillets10+" billet(s) de 10 CHF")
                  } 
                  comptes(id) -= (nbbillets100*100+nbbillets50*50+nbbillets20*20+nbbillets10*10)
                  printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f \n ", comptes(id) ," CHF")
                }//fin somme petite seulement petites coupures
              }//fin devise 1 RETRAIT
       //DEVISE EUR
              if (deviseR==2){
      //conditions du montant de retrait
                while (((montantR*0.95)>(comptes(id)/10))||(montantR%10 !=0)||(montantR==0)) {
                  println("Votre plafond de retrait autorisé en EUR est de: "+ ((comptes(id)*1.05)/10))
                  println("Le montant doit être un multiple de 10.")
                  println("Indiquez le montant du retrait: ") 
                  montantR = readInt() 
                }
                println("Il reste "+montantR+" EUR à distribuer") 
      //billets100
                if (montantR>=100){
                  while (montantR>=100){
                    montantR = montantR - 100
                    decompte = decompte + 1
                  }
                  println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 EUR")
                  println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                  ok = readLine() 
                  if (ok=="o"){
                    nbbillets100=decompte
                  }
                  if (ok!="o"){
                    nbbillets100=(ok.toInt)
                  }
                  while (nbbillets100>decompte){
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 100 EUR")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine() 
                    if (ok=="o"){
                      nbbillets100=decompte
                    }
                    if (ok!="o"){
                      nbbillets100=(ok.toInt)
                    }
                  }
                  println("Vous avez choisi "+nbbillets100+" billets de 100 EUR")//pas nécessaire
                  montantR= ((montantR+ decompte*100)-(nbbillets100*100))
                  decompte=0 
                  println("Il reste "+montantR+" EUR à distribuer")
                }
      //billets50
                if (montantR>=50){
                  while (montantR>=50){
                    montantR = montantR - 50
                    decompte = decompte + 1
                  }
                  println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 EUR")
                  println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                  ok = readLine() 
                  if (ok=="o"){
                    nbbillets50=decompte
                  }
                  if (ok!="o"){
                    nbbillets50=(ok.toInt)
                  }   
                  while (nbbillets50>decompte){
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 50 EUR")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()  
                    if (ok=="o"){
                      nbbillets50=decompte
                    }
                    if (ok!="o"){
                      nbbillets50=(ok.toInt)
                    }
                  }
                  println("Vous avez choisi "+nbbillets50+" billets de 50 EUR")//pas nécessaire
                  montantR= ((montantR+ decompte*50)-(nbbillets50*50))
                  decompte=0 
                  println("Il reste "+montantR+" EUR à distribuer")
                }
      //billets20
                if (montantR>=20){
                  while (montantR>=20){
                    montantR = montantR - 20
                    decompte = decompte + 1
                  }
                  println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 EUR")
                  println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                  ok = readLine()  
                  if (ok=="o"){
                    nbbillets20=decompte
                  }
                  if (ok!="o"){
                    nbbillets20=(ok.toInt)
                  }
                  while (nbbillets20>decompte){
                    println("Vous pouvez obtenir au maximum "+decompte+" billets de 20 EUR")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")
                    ok = readLine()
                    if (ok=="o"){
                      nbbillets20=decompte 
                    }
                    if (ok!="o"){
                      nbbillets20=(ok.toInt)
                    }
                  }
                  println("Vous avez choisi "+nbbillets20+" billets de 20 EUR")//pas nécessaire
                  montantR= ((montantR+ decompte*20)-(nbbillets20*20))
                  decompte=0 
                  println("Il reste "+montantR+" EUR à distribuer")
                }
      //billets10
                if (montantR>=10){
                  while (montantR>=10){
                    montantR = montantR - 10
                    decompte = decompte + 1
                  }
                  nbbillets10= decompte
                  decompte=0
                  println("Il reste "+montantR+" EUR à distribuer")
                }
                println("Veuillez retirer la somme demandée:")
                if (nbbillets100>0){
                  println(nbbillets100+" billet(s) de 100 EUR")
                }
                if (nbbillets50>0){
                  println(nbbillets50+" billet(s) de 50 EUR")
                }
                if (nbbillets20>0){
                  println(nbbillets20+" billet(s) de 20 EUR")
                }
                if (nbbillets10>0){
                  println(nbbillets10+" billet(s) de 10 EUR")
                } 
                comptes(id) -= (0.95*(nbbillets100*100+nbbillets50*50+nbbillets20*20+nbbillets10*10))
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f ", comptes(id)) 
                println("CHF")
      }//fin devide EUR
    } //fin de def retrait 

    def changepin(id : Int, codespin : Array[String]) : Unit = {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères): ")
      var nouveauPin = readLine() 
      while (nouveauPin.length<8){
        println("Votre code pin ne contient pas au moins 8 caractères")
        println("Saissisez votre nouveau code pin: ")
        nouveauPin = readLine()
      }
      codespin(id)= nouveauPin
      println("Votre nouveau code pin est: " + codespin(id))
    } //fin def changepin
    
    def main(args: Array[String]): Unit = {
      //tableaux 
      var comptes = Array.fill(100)(1200.0)
      var codespin = Array.fill(100)("INTRO1234")
      //id
      var id = readLine("Saisissez votre code d'identifiant: ").toInt 
      var arret = false
      if (id >= 100) {
        println("Cet identifiant n'est pas valable.")
        arret = true
      }
      //while pour arreter le programme 
      while (arret == false) {
        //saisie du code pin avec 3 tentatives 
        var codejuste= false
        var pin= codespin(id)
        var tentativespin=3 
        while ((codejuste == false) && (arret == false)){
        var pinentre= readLine("Saisissez votre code pin: ").toString
        while ((pin!=pinentre)&&(tentativespin>1)){
          tentativespin=tentativespin-1
          println("Code pin erroné, il vous reste "+tentativespin+" tentatives.")
          pinentre = readLine("Saisissez votre code pin: ").toString 
        }
        if (pin==pinentre) {
          codejuste = true
          println("Le code pin est valide.") //pas nécessaire
        }
        if ((pin!=pinentre)&&(tentativespin==1)){
          println("Trop d'erreurs, abandon de l'identification.")
          codejuste = false 
          id = readLine("Saisissez votre code d'identifiant: ").toInt
          if (id >= 100) {
            println("Cet identifiant n'est pas valable.")
            arret = true
          }
          tentativespin = 3
        }} 
        var selection = 0
        while ((codejuste == true) && (arret == false) && (selection!=5)){
          //menu que si codepin ok et id valide
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      selection = readLine("Votre choix: ").toInt 
      var montantBANQUE = comptes(id) //ca nous donne 1200.0 
      //var montantD = 0.0
      //var montantR = 0.0
      //var typecoupures = 0
  //boucle menu 1,2,3,4
      while ((codejuste == true)&&(selection!=5)) {
  //selection 1 DEPOT
        if (selection==1) {
          depot(id,comptes)
  //relance menu
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          selection= readLine("Votre choix: ").toInt
        }
  //slection 3 AFFICHAGE DU MONTANT TOTAL
        if (selection==3){
          printf("Le montant disponible sur votre compte est de: %.2f ", comptes(id))
          println("CHF")
  //relance menu
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          selection= readLine("Votre choix: ").toInt
        }
  //selection 2 RETRAIT
        if (selection==2){
          retrait(id,comptes)
  //relance menu
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          selection= readLine("Votre choix: ").toInt
        }//ferme la selection 2
        //debut selection 4 (code pin)
        if (selection==4){ 
          changepin(id,codespin) 
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          selection= readLine("Votre choix: ").toInt
        }//fin selection 4
      }//ferme le GRAND while
  //selection 5 TERMINER 
      if (selection==5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.") 
        id = readLine("Saisissez votre code identifiant: ").toInt
        if (id >= 100) {
          println("Cet identifiant n'est pas valable.")
          arret = true
        }
      }
        }//deuxieme while arret avec code juste
        }//premier while arret
    }
  }