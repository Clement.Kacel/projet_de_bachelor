//Assignment: Dâmaris Ribeiro Vieira_996591_assignsubmission_file

  import scala.io.StdIn.readInt
  import scala.io.StdIn.readChar
  import scala.math.floor
  import scala.util.control.Breaks.break
  import scala.util.control._

  object Main {
    def main(args: Array[String]):
    Unit = {
      //println("Hello wor4ld!")
    }

    var dansLaBoucle = true
    //tableau clients
    val nbclients = 100
    var tableauClients = Array()
    var comptes : Array[Double] = new Array[Double](100)

    for (client <- 1 to nbclients) {
      tableauClients:+ client
    }

    var codespin : Array[String] = new Array[String](100)

    for (i <- 0 until comptes.length) {
      codespin(i) = "INTRO1234"
    }

    for (i <- 0 until comptes.length) {
      comptes(i) = 1200.0
    }

    var identifiant = 0

    var danslaboucleprincipale = true
    while(danslaboucleprincipale){
      //identifiants

      var danslaboucleidentifiant = true
      while (danslaboucleidentifiant) {
        println("Saisissez votre code identifiant >")
        identifiant = readInt()
        if (identifiant > nbclients) {
          println("Cet identifiant n'est pas valable.")
        }
        else {
          danslaboucleidentifiant = false
        }
      }

      //pin
      var nTentatives = 2
      var pin = ""
      var danslabouclepin = true
      while (danslabouclepin) {
        pin = scala.io.StdIn.readLine("Saisissez votre code pin >")
        if (pin != "INTRO1234") {
          if(nTentatives>0){
            println("Code pin erroné il vous reste " + nTentatives + " tentatives")
          }

          nTentatives = nTentatives - 1
          if (nTentatives < 0) {
            println("Trop d'erreurs, abandon de l'identification")
            danslabouclepin = false
          }
        }
        else {
          danslaboucleidentifiant = false
          danslaboucleprincipale = false
          danslabouclepin = false
        }
      }
    }

    val codePIN = "INTRO1234"
    var nbTentativesTot = 3
    var nbTentatives = 1
    val tauxConversionEURtoCHF = 0.95
    val tauxConversionCHFtoEUR = 1.05
    var dansLaBoucleDevise = true
    var dansLaBoucleRetrait = true

    var choixDevise = 55
    var choixDeviseRetrait = 55
    var dansLaBoucleCodePin = true
    var dansLaBoucleCoupure = true
    var pinInput = ""

    var montant = comptes(identifiant-1)


    var choixOperation = 0
    //var montantComptes: Array[Double] = null

    var choixCoupure = 10

    var dansLaBoucleCoupure500 = true
    var dansLaBoucleCoupure200 = true
    var dansLaBoucleCoupure100 = true
    var dansLaBoucleCoupure50 = true
    var dansLaBoucleCoupure20 = true
    var dansLaBoucleCoupure10 = true

    //FONCTION CODE PIN
    def changepin(id : Int, codespin : Array[String]): Unit = {
      var danslabouclenvcodepin = true
      var nvpin = ""
      while (danslabouclenvcodepin) {
        nvpin = scala.io.StdIn.readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères)>")
        if (nvpin.length() < 8) {
          println("Votre code pin ne contient pas au moins 8 car")
        }
        else {
          danslabouclenvcodepin = false
        }
      }
      codespin(id) = nvpin
    }

    //FONCTION DEPOT
    def depot(id : Int, comptes : Array[Double]): Array[Double] = {
      var montant = comptes(id)
      dansLaBoucleDevise = true
      while (dansLaBoucleDevise) {
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        choixDevise = readInt()
        if (choixDevise == 1 || choixDevise == 2) {
          dansLaBoucleDevise = false
        }
      }
      //println(choixDevise)
      println("Indiquez le montant du dépôt >")
      var montantDepot = readInt()
      if (choixDevise == 2) {
        montant = montant + (montantDepot * tauxConversionEURtoCHF)
        comptes(id) = montant
      }
      if (choixDevise == 1) {
        montant = montant + montantDepot
        comptes(id) = montant
      }
      while (montantDepot % 10 != 0) {
        println("Le montant doit être un multiple de 10")
        montantDepot = readInt()
      }
      //nbTentatives = 4
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + montant)
      return comptes
    }

    //FONCTION RETRAIT
    def retrait(id: Int, comptes: Array[Double]): Array[Double] = {
      var montant = comptes(id)

      while (dansLaBoucleRetrait) {
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        choixDeviseRetrait = readInt()
        if (choixDeviseRetrait == 1 || choixDeviseRetrait == 2) {
          dansLaBoucleRetrait = false
        }
      }
      println("Indiquez le montant du retrait >")
      var montantRetrait = readInt()
      //if (choixDeviseRetrait == 2) {
      //montant = montant + (montantRetrait * tauxConversionEURtoCHF)
      //}

      while (0.1 * montant < montantRetrait) {
        println("Votre plafond de retrait autorisé est de : " + (0.1 * montant))
        montantRetrait = readInt()
      }
      while (montantRetrait % 10 != 0) {
        println("Le montant doit être un multiple de 10")
        montantRetrait = readInt()
        while (0.1 * montant < montantRetrait) {
          println("Votre plafond de retrait autorisé est de : " + (0.1 * montant))
          montantRetrait = readInt()
        }
      }

      //SI CHF
      if (choixDeviseRetrait == 1) {
        if (montantRetrait >= 200) {
          while (dansLaBoucleCoupure) {
            println("En 1) grosses coupures, 2) petites coupures >")
            choixCoupure = readInt()
            if (choixCoupure == 1 || choixCoupure == 2) {
              dansLaBoucleCoupure = false
            }
          }
        }
        else {
          choixCoupure = 2
        }

        val cinqcent = 500
        var reste = montantRetrait
        var nbCoupures500 = 0
        var input500 = "0"

        val deuxcent = 200
        var nbCoupures200 = 0
        var input200 = "0"

        val cent = 100
        var nbCoupures100 = 0
        var input100 = "0"

        val cinquante = 50
        var nbCoupures50 = 0
        var input50 = "0"

        val vingt = 20
        var nbCoupures20 = 0
        var input20 = "0"

        val dix = 10
        var nbCoupures10 = 0
        var input10 = "0"

        //Si Grosse coupure
        if (choixCoupure == 1) {
          //550 | 250 | 1050
          //maxCoupure = 500
          while (dansLaBoucleCoupure500) {
            nbCoupures500 = floor(montantRetrait.toDouble / cinqcent).toInt
            if (nbCoupures500 > 0) {

              input500 = scala.io.StdIn.readLine("Il reste " + reste + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures500 + " billet(s) de " + cinqcent + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              //reste = montantRetrait - (cinqcent * (nbCoupures500-input500.toInt))
            }
            if (input500 == "o") {
              input500 = nbCoupures500.toString
              reste = montantRetrait - (cinqcent * (input500.toInt))
              dansLaBoucleCoupure500 = false
            }
            else {
              if (input500.toInt <= nbCoupures500) {
                //println(montantRetrait + "-(" + cinqcent + "*(" + nbCoupures500 + "-" + input500.toInt + "))")
                reste = montantRetrait - (cinqcent * (input500.toInt))
                dansLaBoucleCoupure500 = false
              }
            }

          }

          var reste200 = reste
          while (dansLaBoucleCoupure200) {
            nbCoupures200 = floor(reste200.toDouble / deuxcent).toInt
            if (nbCoupures200 > 0) {

              input200 = scala.io.StdIn.readLine("Il reste " + reste200 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures200 + " billet(s) de " + deuxcent + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              //reste200 = montantRetrait - (deuxcent * (nbCoupures200-input200.toInt))
            }
            if (input200 == "o") {
              input200 = nbCoupures200.toString
              reste200 = reste - (deuxcent * (input200.toInt))
              dansLaBoucleCoupure200 = false
            }
            else {
              if (input200.toInt <= nbCoupures200) {
                reste200 = reste - (deuxcent * (input200.toInt))
                dansLaBoucleCoupure200 = false
              }
            }
          }

          var reste100 = reste200
          while (dansLaBoucleCoupure100) {
            nbCoupures100 = floor(reste100.toDouble / cent).toInt
            if (nbCoupures100 > 0) {
              //  reste100 = montantRetrait - (cent * nbCoupures100)
              input100 = scala.io.StdIn.readLine("Il reste " + reste100 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures100 + " billet(s) de " + cent + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            if (input100 == "o") {
              input100 = nbCoupures100.toString
              reste100 = reste200 - (cent * (input100.toInt))
              dansLaBoucleCoupure100 = false
            }
            else {
              if (input100.toInt <= nbCoupures100) {
                reste100 = reste200 - (cent * (input100.toInt))
                dansLaBoucleCoupure100 = false
              }
            }
          }

          var reste50 = reste100
          while (dansLaBoucleCoupure50) {
            nbCoupures50 = floor(reste50.toDouble / cinquante).toInt
            if (nbCoupures50 > 0) {
              //reste50 = montantRetrait - (cinquante * nbCoupures100)
              input50 = scala.io.StdIn.readLine("Il reste " + reste50 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures50 + " billet(s) de " + cinquante + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            if (input50 == "o") {
              input50 = nbCoupures50.toString
              reste50 = reste100 - (cinquante * (input50.toInt))
              dansLaBoucleCoupure50 = false
            }
            else {
              if (input50.toInt <= nbCoupures50) {
                reste50 = reste100 - (cinquante * (input50.toInt))
                dansLaBoucleCoupure50 = false
              }
            }
          }

          var reste20 = reste50
          while (dansLaBoucleCoupure20) {
            nbCoupures20 = floor(reste20.toDouble / vingt).toInt
            if (nbCoupures20 > 0) {
              //reste20 = montantRetrait - (vingt * nbCoupures20)
              input20 = scala.io.StdIn.readLine("Il reste " + reste20 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures20 + " billet(s) de " + vingt + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            if (input20 == "o") {
              input20 = nbCoupures20.toString
              reste20 = reste50 - (vingt * (input20.toInt))
              dansLaBoucleCoupure20 = false
            }
            else {
              if (input20.toInt <= nbCoupures20) {
                reste20 = reste50 - (vingt * (input20.toInt))
                dansLaBoucleCoupure20 = false
              }
            }
          }

          var reste10 = reste20
          var tot10 = 0
          while (dansLaBoucleCoupure10) {
            nbCoupures10 = floor(reste10.toDouble / dix).toInt
            if (nbCoupures10 > 0) {
              //reste10 = montantRetrait - (dix * nbCoupures10)
              input10 = scala.io.StdIn.readLine("Il reste " + reste10 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures10 + " billet(s) de " + dix + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              tot10 = tot10 + nbCoupures10
            }
            if (input10.toString == "o") {
              input10 = nbCoupures10.toString
              reste10 = reste10 - (dix * (nbCoupures10))
              dansLaBoucleCoupure10 = false
            }
            else {
              if (input10.toInt <= nbCoupures10) {
                reste10 = reste10 - (dix * (input10.toInt))
                if (reste10 <= 0) {
                  dansLaBoucleCoupure10 = false
                }
              }
            }
          }

          if (reste10 == 0) {
            println("Veuillez retirer la somme demandée : ")
            if (input500.toInt != 0) {
              println(input500 + " billet(s) de 500 CHF")
            }
            if (input200.toInt != 0) {
              println(input200 + " billet(s) de 200 CHF")
            }
            if (input100.toInt != 0) {
              println(input100 + " billet(s) de 100 CHF")
            }
            if (input50.toInt != 0) {
              println(input50 + " billet(s) de 50 CHF")
            }
            if (input20.toInt != 0) {
              println(input20 + " billet(s) de 20 CHF")
            }
            if (tot10 != 0) {
              println(tot10 + " billet(s) de 10 CHF")
            }

            montant = montant - montantRetrait
            comptes(id) = montant
            println("Votre retrait a été pris en compte, le nouveau montant\ndisponible sur votre compte est de : " + montant)
          }
        }
        dansLaBoucleRetrait = true
        dansLaBoucleCoupure = true
        dansLaBoucleCoupure500 = true
        dansLaBoucleCoupure200 = true
        dansLaBoucleCoupure100 = true
        dansLaBoucleCoupure50 = true
        dansLaBoucleCoupure20 = true
        dansLaBoucleCoupure10 = true
        if (choixCoupure == 2) {
          val cent = 100
          var nbCoupures100 = 0
          var input100 = "0"

          val cinquante = 50
          var nbCoupures50 = 0
          var input50 = "0"

          val vingt = 20
          var nbCoupures20 = 0
          var input20 = "0"

          val dix = 10
          var nbCoupures10 = 0
          var input10 = "0"

          //Si Grosse coupure
          //550 | 250 | 1050
          //maxCoupure = 500

          var reste100 = montantRetrait
          while (dansLaBoucleCoupure100) {
            nbCoupures100 = floor(montantRetrait.toDouble / cent).toInt
            if (nbCoupures100 > 0) {
              //  reste100 = montantRetrait - (cent * nbCoupures100)
              input100 = scala.io.StdIn.readLine("Il reste " + reste100 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures100 + " billet(s) de " + cent + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            if (input100 == "o") {
              input100 = nbCoupures100.toString
              reste100 = montantRetrait - (cent * (input100.toInt))
              dansLaBoucleCoupure100 = false
            }
            else {
              if (input100.toInt <= nbCoupures100) {
                reste100 = montantRetrait - (cent * (input100.toInt))
                dansLaBoucleCoupure100 = false
              }
            }
          }

          var reste50 = reste100
          while (dansLaBoucleCoupure50) {
            nbCoupures50 = floor(reste50.toDouble / cinquante).toInt
            if (nbCoupures50 > 0) {
              //reste50 = montantRetrait - (cinquante * nbCoupures100)
              input50 = scala.io.StdIn.readLine("Il reste " + reste50 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures50 + " billet(s) de " + cinquante + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            if (input50 == "o") {
              input50 = nbCoupures50.toString
              reste50 = reste100 - (cinquante * (input50.toInt))
              dansLaBoucleCoupure50 = false
            }
            else {
              if (input50.toInt <= nbCoupures50) {
                reste50 = reste100 - (cinquante * (input50.toInt))
                dansLaBoucleCoupure50 = false
              }
            }
          }

          var reste20 = reste50
          while (dansLaBoucleCoupure20) {
            nbCoupures20 = floor(reste20.toDouble / vingt).toInt
            if (nbCoupures20 > 0) {
              //reste20 = montantRetrait - (vingt * nbCoupures20)
              input20 = scala.io.StdIn.readLine("Il reste " + reste20 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures20 + " billet(s) de " + vingt + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            if (input20 == "o") {
              input20 = nbCoupures20.toString
              reste20 = reste50 - (vingt * (input20.toInt))
              dansLaBoucleCoupure20 = false
            }
            else {
              if (input20.toInt <= nbCoupures20) {
                reste20 = reste50 - (vingt * (input20.toInt))
                dansLaBoucleCoupure20 = false
              }
            }
          }

          var reste10 = reste20
          var tot10 = 0
          while (dansLaBoucleCoupure10) {
            nbCoupures10 = floor(reste10.toDouble / dix).toInt
            if (nbCoupures10 > 0) {
              //reste10 = montantRetrait - (dix * nbCoupures10)
              input10 = scala.io.StdIn.readLine("Il reste " + reste10 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures10 + " billet(s) de " + dix + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              tot10 = tot10 + nbCoupures10
            }
            if (input10.toString == "o") {
              input10 = nbCoupures10.toString
              reste10 = reste10 - (dix * (nbCoupures10))
              dansLaBoucleCoupure10 = false
            }
            else {
              if (input10.toInt <= nbCoupures10) {
                reste10 = reste10 - (dix * (input10.toInt))
                if (reste10 <= 0) {
                  dansLaBoucleCoupure10 = false
                }
              }
            }
          }

          if (reste10 == 0) {
            println("Veuillez retirer la somme demandée : ")
            if (input100.toInt != 0) {
              println(input100 + " billet(s) de 100 CHF")
            }
            if (input50.toInt != 0) {
              println(input50 + " billet(s) de 50 CHF")
            }
            if (input20.toInt != 0) {
              println(input20 + " billet(s) de 20 CHF")
            }
            if (tot10 != 0) {
              println(tot10 + " billet(s) de 10 CHF")
            }
            montant = montant - montantRetrait
            comptes(id) = montant
            println("Votre retrait a été pris en compte, le nouveau montant\ndisponible sur votre compte est de : " + montant)
          }

          dansLaBoucleRetrait = true
          dansLaBoucleCoupure = true
          dansLaBoucleCoupure500 = true
          dansLaBoucleCoupure200 = true
          dansLaBoucleCoupure100 = true
          dansLaBoucleCoupure50 = true
          dansLaBoucleCoupure20 = true
          dansLaBoucleCoupure10 = true
        }
      }

      //SI EUR
      if (choixDeviseRetrait == 2) {
        val cent = 100
        var nbCoupures100 = 0
        var input100 = "0"

        val cinquante = 50
        var nbCoupures50 = 0
        var input50 = "0"

        val vingt = 20
        var nbCoupures20 = 0
        var input20 = "0"

        val dix = 10
        var nbCoupures10 = 0
        var input10 = "0"

        //Si Grosse coupure
        //550 | 250 | 1050
        //maxCoupure = 500

        var reste100 = montantRetrait
        while (dansLaBoucleCoupure100) {
          nbCoupures100 = floor(montantRetrait.toDouble / cent).toInt
          if (nbCoupures100 > 0) {
            //  reste100 = montantRetrait - (cent * nbCoupures100)
            input100 = scala.io.StdIn.readLine("Il reste " + reste100 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures100 + " billet(s) de " + cent + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
          }
          if (input100 == "o") {
            input100 = nbCoupures100.toString
            reste100 = montantRetrait - (cent * (input100.toInt))
            dansLaBoucleCoupure100 = false
          }
          else {
            if (input100.toInt <= nbCoupures100) {
              reste100 = montantRetrait - (cent * (input100.toInt))
              dansLaBoucleCoupure100 = false
            }
          }
        }

        var reste50 = reste100
        while (dansLaBoucleCoupure50) {
          nbCoupures50 = floor(reste50.toDouble / cinquante).toInt
          if (nbCoupures50 > 0) {
            //reste50 = montantRetrait - (cinquante * nbCoupures100)
            input50 = scala.io.StdIn.readLine("Il reste " + reste50 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures50 + " billet(s) de " + cinquante + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
          }
          if (input50 == "o") {
            input50 = nbCoupures50.toString
            reste50 = reste100 - (cinquante * (input50.toInt))
            dansLaBoucleCoupure50 = false
          }
          else {
            if (input50.toInt <= nbCoupures50) {
              reste50 = reste100 - (cinquante * (input50.toInt))
              dansLaBoucleCoupure50 = false
            }
          }
        }

        var reste20 = reste50
        while (dansLaBoucleCoupure20) {
          nbCoupures20 = floor(reste20.toDouble / vingt).toInt
          if (nbCoupures20 > 0) {
            //reste20 = montantRetrait - (vingt * nbCoupures20)
            input20 = scala.io.StdIn.readLine("Il reste " + reste20 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures20 + " billet(s) de " + vingt + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
          }
          if (input20 == "o") {
            input20 = nbCoupures20.toString
            reste20 = reste50 - (vingt * (input20.toInt))
            dansLaBoucleCoupure20 = false
          }
          else {
            if (input20.toInt <= nbCoupures20) {
              reste20 = reste50 - (vingt * (input20.toInt))
              dansLaBoucleCoupure20 = false
            }
          }
        }

        var reste10 = reste20
        var tot10 = 0
        while (dansLaBoucleCoupure10) {
          nbCoupures10 = floor(reste10.toDouble / dix).toInt
          if (nbCoupures10 > 0) {
            //reste10 = montantRetrait - (dix * nbCoupures10)
            input10 = scala.io.StdIn.readLine("Il reste " + reste10 + " CHF à distribuer\nVous pouvez obtenir au maximum " + nbCoupures10 + " billet(s) de " + dix + " CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
            tot10 = tot10 + nbCoupures10
          }
          if (input10.toString == "o") {
            input10 = nbCoupures10.toString
            reste10 = reste10 - (dix * (nbCoupures10))
            dansLaBoucleCoupure10 = false
          }
          else {
            if (input10.toInt <= nbCoupures10) {
              reste10 = reste10 - (dix * (input10.toInt))
              if (reste10 <= 0) {
                dansLaBoucleCoupure10 = false
              }
            }
          }
        }

        if (reste10 == 0) {
          println("Veuillez retirer la somme demandée : ")
          if (input100.toInt != 0) {
            println(input100 + " billet(s) de 100 CHF")
          }
          if (input50.toInt != 0) {
            println(input50 + " billet(s) de 50 CHF")
          }
          if (input20.toInt != 0) {
            println(input20 + " billet(s) de 20 CHF")
          }
          if (tot10 != 0) {
            println(tot10 + " billet(s) de 10 CHF")
          }
          montant = montant - (montantRetrait * tauxConversionEURtoCHF)
          comptes(id) = montant
          println("Votre retrait a été pris en compte, le nouveau montant\ndisponible sur votre compte est de : " + montant)
        }

        dansLaBoucleRetrait = true
        dansLaBoucleCoupure = true
        dansLaBoucleCoupure500 = true
        dansLaBoucleCoupure200 = true
        dansLaBoucleCoupure100 = true
        dansLaBoucleCoupure50 = true
        dansLaBoucleCoupure20 = true
        dansLaBoucleCoupure10 = true
      }
      return comptes
    }

    //Code
    while(dansLaBoucle){
      println("Choisissez votre opération :")
      println("1)  Dépot")
      println("2)  Retrait")
      println("3)  Consultation du compte")
      println("4)  Changement du code pin")
      println("5)  Terminer")

      choixOperation = readInt()

      if (choixOperation == 5){
        println("Opérations terminées. Merci et à bientôt!")
        dansLaBoucle = false
      }

      else{
        if (choixOperation == 4) {
          changepin(identifiant-1, codespin)
          //println(codespin(identifiant-1))
        }

        if(choixOperation == 1){
          //montantComptes = depot(identifiant-1, comptes)
          comptes(identifiant-1) = depot(identifiant-1, comptes)(identifiant-1)
        }
      }
      if (choixOperation == 2) {
        //montantComptes = retrait(identifiant-1, comptes)
        comptes(identifiant-1) = retrait(identifiant-1, comptes)(identifiant-1)
      }
      else if(choixOperation == 3){
        println("Le montant disponible sur votre compte est de : " + comptes(identifiant-1))
        //nbTentatives = 4
      }
    }
  }

