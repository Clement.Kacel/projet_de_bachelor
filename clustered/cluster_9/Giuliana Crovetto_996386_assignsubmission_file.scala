//Assignment: Giuliana Crovetto_996386_assignsubmission_file

import scala.io.StdIn._

object Main {
  def main(args: Array[String]): Unit = {

    val nbClients = 100
    var comptes = Array.fill(nbClients)(1200.0)

    val id = readLine("Saisissez votre code identifiant > ").toInt

    if (id >= nbClients) {
      println("Cet identifiant n’est pas valable.")
      sys.exit(1)

    }

    if (!verifierCodePin(id)) {
      println("Trop d’erreurs, abandon de l’identification")
      sys.exit(1)
    }

    var continuer = true

    while (continuer) {
      println("Choisissez votre opération :")
      println("1) Dépôt")
      print("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")

      val choix = readInt()

      choix match {
        case 1 => depot(id)
        case 2 => retrait(id)
        case 3 => consultation(id)
        case 4 => changepin(id)
        case 5 => 
               terminer = false
        pritnln("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        case _ => println("Opération invalide. Veuilllez réessayer.")
      }
    }
  }

  def verifierCodePin(id: Int): Boolean = {
  var tentatives = 0
  var essai = ""

  while (tentatives < 3 && essai != codePin(id)) {
    println("Saisissez votre code pin > ")
    essai = readLine().toString

    if (essai != codePin(id)) {
      tentatives += 1
      println("Code pin erroné, il vous reste " + (3 - tentatives) + " tentatives")
    }
  }

  essai == codePin(id)
}

def depot(id: Int): Unit = {
var devise = 0 
var depot = 0

while (devise != 1 && devise != 2) {
  println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
  devise = readInt()
}

do {
  depot = reandLine ("Indiquez le montant du dépôt > ").toInt
  if(depot % 10 != 0) {
    println("Le montant doit être un multiple de 10. Réessayez.>")

    while (depot % 10 != 0) {
      readLine("Le montant doit être un multiple de 10. Réessayez.>")
    }
  }
} while (depot % 10 != 0)  

if (devise == 2) {
  depot = (depot * 1.05).toInt
}

comptes(id) += depot
println ("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id) + " CHF")
}

def retrait(id: Int): Unit = {
var devise = 0
var retrait = 0
var tauxCHf_EUR = 0.95
val plafond = (comptes(id)*0.1).toInt

while (devise != 1 && devise != 2) {
  println("Indiquez la devise :1 CHF, 2 : EUR > ")
  devise = readInt()
}

while (retrait % 10 != 0 || retrait > plafond) {
  println ("Indiquez le montant du retrait > ")
  retrait = readInt()

  if (retrait % 10 != 0) {
    println("Le montant doit être un multiple de 10. Réessayez >")
  }else if (retrait > plafond) {
    println("Votre plafond de retrait autorisé est de : " + plafond + " CHF")

  }
}

if (devise == 2) {
  retrait = (retrait * 1.05).toInt
}

comptes(id) -= retrait
println ("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
}

def consultation(id: Int): Unit = {
println ("Le montant disponible sur votre compte est de : " + comptes(id) + " CHF")
}

def changepin(id: Int): Unit = {
var nouveauCodePin = ""
do {
  nouveauCodePin = readLine ("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").

  if (nouveauCodePin.length < 8) {
    println("Votre code pin ne contient pas au moins 8 caractères.Réessayez >")
}
}while (nouveauCodePin.length < 8)

codePin(id) = nouveauCodePin
println ("Le code pin a été modifié avec succès.")   
  }
}
