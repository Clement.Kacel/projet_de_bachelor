//Assignment: Mikaël Issa Rizzato_996542_assignsubmission_file

object Main extends App {
  val nbclients: Int = 100
  val comptes: Array[Double] = Array.tabulate(nbclients)(i => (i * 1200.0) / 100.0)
  val codespin: Array[String] = Array.fill(nbclients)("INTRO1234")

  case class Client(identifiant: Int) {
    val codePin: String = codespin(identifiant)
    var montantDisponible: Double = comptes(identifiant)
  }

  def identifierUtilisateur(): Client = {
    println("Saisissez votre code identifiant:")
    val identifiant = scala.io.StdIn.readInt()

    if (identifiant >= nbclients) {
      println("Cet identifiant n'est pas valable.")
      System.exit(1) // Arrêter le programme
    }

    val client = Client(identifiant)

    // Gestion des tentatives de saisie du code PIN
    var tentativesRestantes = 3
    var codePinSaisi = ""

    do {
      println("Saisissez votre code pin:")
      codePinSaisi = scala.io.StdIn.readLine()

      if (codePinSaisi != client.codePin) {
        tentativesRestantes -= 1
        println(s"Code pin erroné, il vous reste $tentativesRestantes tentatives.")
      }
    } while (tentativesRestantes > 0 && codePinSaisi != client.codePin)

    if (tentativesRestantes == 0) {
      println("Trop d'erreurs, abandon de l'identification.")
      System.exit(1) // Arrêter le programme
    }

    println("Identification réussie.")
    client
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    println("Saisissez la devise (CHF ou EUR):")
    val devise = scala.io.StdIn.readLine().toUpperCase()

    println("Saisissez le montant à déposer:")
    val montantDepot = scala.io.StdIn.readDouble()

    if (montantDepot % 10 != 0) {
      println("Le montant doit être divisible par 10.")
      return
    }

    val montantEnCHF = if (devise == "EUR") montantDepot * 1.1 else montantDepot

    comptes(id) += montantEnCHF
    println(s"Opération de dépôt réussie. Nouveau solde : ${comptes(id)}")
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {
    println("Saisissez la devise (CHF ou EUR):")
    val devise = scala.io.StdIn.readLine().toUpperCase()

    println("Saisissez le montant à retirer:")
    val montantRetrait = scala.io.StdIn.readDouble()

    if (montantRetrait % 10 != 0 || montantRetrait > comptes(id)) {
      println("Montant invalide ou dépasse le solde disponible.")
      return
    }

    val montantEnCHF = if (devise == "EUR") montantRetrait * 1.1 else montantRetrait

    comptes(id) -= montantEnCHF
    println(s"Opération de retrait réussie. Nouveau solde : ${comptes(id)}")
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères):")
    var nouveauCodePin = scala.io.StdIn.readLine()

    while (nouveauCodePin.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères. Réessayez :")
      nouveauCodePin = scala.io.StdIn.readLine()
    }

    codespin(id) = nouveauCodePin
    println("Changement de code pin réussi.")
  }

  // Exemple d'utilisation
  var continuer = true
  while (continuer) {
    val clientIdentifie = identifierUtilisateur()

    var choix = 0
    while (choix != 5) {
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")

      print("Votre choix : ")
      choix = scala.io.StdIn.readInt()

      choix match {
        case 1 => depot(clientIdentifie.identifiant, comptes)
        case 2 => retrait(clientIdentifie.identifiant, comptes)
        case 3 => println(s"Solde actuel : ${comptes(clientIdentifie.identifiant)}")
        case 4 => changepin(clientIdentifie.identifiant, codespin)
        case 5 => println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        case _ => println("Choix invalide.")
      }
    }

    print("Voulez-vous effectuer des opérations pour un autre client ? (oui/non) : ")
    val reponse = scala.io.StdIn.readLine().toLowerCase()
    continuer = reponse == "oui"
  }
}





