//Assignment: Eric Ingabire_996395_assignsubmission_file

import scala.io.StdIn._
object Bancomat {
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin= Array.fill(nbclients)("INTRO1234")
    // Boucle principale
    while (true){
      println("Saisissez votre code identifiant >")
      val id = readInt()
      if (id < 0 ||id >= nbclients) {
        println("Cet identifiant n'est pas valable.")
        sys.exit(1)
      }
      // Tentatives pour saisir le code pin
      var tentatives = 3
      var pinCorrect = false
      while (tentatives > 0 && ! pinCorrect){
        println("Saisissez votre code pin >")
        val enteredPin = readLine()
        if (enteredPin == codespin (id)) {
          pinCorrect = true
        } else {
          tentatives -= 1
          println(s"Code pin erroné , il vous reste $tentatives tentatives" )
    }
      }
      if (!pinCorrect) {
        println ("Trop d'erreurs, abandon de l'identification.")
        sys.exit(1)
        }
      // Accès au menu
      menu (id, comptes , codespin)
    }
  }
  
  def menu(id:Int , comptes: Array[Double], codespin: Array[String]): Unit = {
    var choix = 0
    while (choix != 5){
      println("Choisissez votre opération:")
      println("1) Dépot")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) changement du code pin")
      println("5) Terminer")
      println("Votre choix:")
      choix = readInt()

      choix match { 
        case 1 => depot (id, comptes)
       case 2 => retrait (id , comptes)
        case 3 => consultation (id , comptes)
        case 4=> changepin(id, codespin)
       case 5 => println(" Fin des opérations , n'oubliez pas de récuperer votre carte.")
        case _=> println("Choix invalide . Veuillez choisir une option valide. ") 
      }  
   }
}
  def depot (id:Int, comptes : Array[Double]): Unit= {
      println("Indiquer la devise du dépôt : 1.CHF ; 2. EUR>")
      val deviseDep = readInt()

      println("Indiquer le montant du dépôt:")
      val montantDep = readDouble()

      // Implement deposit logic here
      comptes(id) += montantDep

      println("Opération de dépôt effectuée.")
    }

    def retrait(id: Int, comptes: Array[Double]): Unit = {
      println("Indiquer la devise du retrait : 1.CHF ; 2. EUR>")
      val deviseRet = readInt()

      println("Indiquer le montant du retrait:")
      var montantRet = readDouble()

      while (montantRet % 10 != 0 || montantRet > comptes(id) * 0.1) {
        println(s"Montant invalide. Le montant doit être un multiple de 10 et ne peut dépasser 10% du solde. Réessayez.")
        montantRet = readDouble()
      }

      comptes(id) -= montantRet
      println("Opération de retrait effectuée.")
    }

    def consultation(id: Int, comptes: Array[Double]): Unit = {
      println(s"Le solde de votre compte est : ${comptes(id)} CHF")
    }

    def changepin(id: Int, codespin: Array[String]): Unit = {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      var newPin = readLine()

      while (newPin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères. Veuillez réessayer.")
        newPin = readLine()
      }

      codespin(id) = newPin
      println("Changement du code pin effectué.")
  }
}
