//Assignment: Matteo Zignani_996461_assignsubmission_file

import scala.io.StdIn.readInt
import io.StdIn.readLine

object Main {
  def main(args: Array[String]): Unit = {


    var identifiant = 0
    val comptes = Array.fill(100)(1200.0)
    var continuer = true


// Si le code identifiant est introuvable, alors le code s'arrete  
      
      while(continuer == true) { 
      println("Saisissez votre code identifiant >")
      identifiant = readInt()
      if (identifiant < 0 || identifiant > 99) {
      continuer = false
      println("Cet identifiant n'est pas valable")
        }


          if (continuer == true) {
// le code s'arrête lorsque l'identifiant est introuvable

// le code pin
    var codepin = "0"
    val codespins = Array.fill(100) ("INTRO1234")
    var tentatives = 3
    println("Saisissez votre code pin >")
    codepin = readLine ()
    if (codepin != codespins){
    do {
      println("Code pin erroné, il vous reste " + tentatives + " tentatives")
      tentatives = tentatives - 1
      println("Saisissez votre code pin >")
      codepin = readLine()
       }
      while (tentatives > 0)
      if (tentatives == 0) { 
        println("Trop d'erreurs, abandon de l'identification")
        continuer = false
         }
       }


//menu
    var correct = true
    while (correct) {
      println("Choisissez votre opération :")
      println("  1) Dépôt")
      println("  2) Retrait")
      println("  3) Consultation du compte")
      println("  4) Changement de code pin")
      println("  5) Terminer")
      println("Votre choix :")
      var instruction = readLine()
      val choix = instruction == 1 || instruction == 2 || instruction == 3 || instruction == 4 || instruction == 5
      if (!choix) println("opération invalide")

      var nouveaucodepin = "0"
      if (instruction == 4) {
      do { 
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      nouveaucodepin = readLine()
         }
      while(nouveaucodepin.length < 8)
      if (nouveaucodepin.length < 8){
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
      }

    
      if (instruction == "5") {
        println("Fin des opérations, n'oubliez pas de récupérer votre carte")
        correct = false
         }
       }
     }
         }
         

  }
}



