//Assignment: Dossè Kodjo Fulbert Agbodza_996564_assignsubmission_file

import scala.io.StdIn._

object Main {
  def changepin(id: Int, codespin: Array[String]): Unit = {
      println("Changement du code PIN")
      var entrerNouveauPin = true
      while (entrerNouveauPin) {
        var nouveauPin = readLine("Saisissez votre nouveau code PIN (au moins 8 caractères) > ")

        if (nouveauPin.length >= 8) {
          codespin(id) = nouveauPin
          entrerNouveauPin = false
          println("Le code PIN a été changé avec succès.")
        } else {
          println("La longueur du nouveau code PIN doit être d'au moins 8 caractères.")
        }
      }
    }






  def main(args: Array[String]): Unit = {
    var lancerBancomat = true
    var motDePasseCorrect = ""
    var mdpDejaEntrer = false // mdp = mot de passe

    var nbClients = 100
    var comptes = Array.fill[Double](nbClients)(1200.0)
    var codesPin = Array.fill[String](nbClients)("INTRO1234")
    var identifiants = Array.fill[Int](nbClients)(0)

    for (i <- 0 until nbClients) {
      identifiants(i) = i
    }

    def changepin(id: Int, codepin: String): Unit = {
      codesPin(id) = codepin
    }

    while (lancerBancomat) {
      var entrerIdentifiant = true
      var identifiant = -1
      while (entrerIdentifiant) {
        println("Saisissez votre code identifiant >")
        identifiant = readInt()

        if (identifiant >=0 && identifiant <=99) {
          entrerIdentifiant = false
          motDePasseCorrect = codesPin(identifiant)
        } 
        else {
          println("Cet identifiant n'est pas valable")
        }


        if(mdpDejaEntrer == false){
          var nbreDeTentative = 3
          var mdpEntrer = ""
          while (nbreDeTentative > 0 ) {
            mdpEntrer = readLine("Saisissez votre pin > ")
            if (nbreDeTentative == 0){
              println("Trop d'erreur, abandon de l'identification")
              lancerBancomat = false
            }
            else if (mdpEntrer != motDePasseCorrect) {
              nbreDeTentative = nbreDeTentative - 1
              println(s"Code pin erroné, il vous reste $nbreDeTentative tentdeatives >")
            }
            else if (mdpEntrer == motDePasseCorrect) {
              mdpDejaEntrer = true 
              nbreDeTentative=0
            }
          }
          var operation = true
          while (operation){
              println("Choisissez votre opération :\n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code pin\n5) Terminer")
              print("Votre choix : ")
              var choix = readInt()

              if(choix == 5){
                println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
                operation = false 
                mdpDejaEntrer = false
              }
              else if (choix == 3){
                var dispo = comptes(identifiant)
                println(s"Le montant disponible sur votre compte est de : $dispo")               
              }
              else if (choix == 4){
                println("Changement du code PIN")

                var entrerNouveauPin = true
                while (entrerNouveauPin) {
                  print("Saisissez le nouveau mot de passe > ")
                  var nouveauPin = readLine()

                  if (nouveauPin.length >= 8) {
                    codesPin(identifiant) = nouveauPin
                    entrerNouveauPin = false
                    println("Le code PIN a été changé avec succès.")
                  } else {
                    println("La longueur du nouveau code PIN doit être d'au moins 8 caractères.")
                  }
                }
              }
          }


        }
      }


    }
  }
}



