//Assignment: Alejandro Rangel Salazar_996485_assignsubmission_file

import scala.io.StdIn._
object Main {

  val Nb_Clients: Int = 100
  var Code_PIN: Array[String] = Array.fill(Nb_Clients)("INTRO1234")
  var Comptes:  Array[Double] = Array.fill(Nb_Clients)(1200)
  val Taux_EUR = 0.95 


  def Dépôt(id: Int, Comptes: Array[Double]) : Unit = {
//Opération de Dépôt
    var Devise = 0
    while (Devise != 1 && Devise != 2) {
      Devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR : ").toInt
      if (Devise != 1 && Devise != 2) {
        println("Veuillez choisir une option valide (1 ou 2).")
      }
    }

  var Boucle = true
  while(Boucle) {
    var Montant_Choisi_Utilisateur = readLine("Indiquez le montant du dépôt :").toInt 

    if (Montant_Choisi_Utilisateur % 10 == 0){
      Boucle = false
      if (Devise == 2){
        Comptes(id) += + Montant_Choisi_Utilisateur * Taux_EUR
      } 
      else if (Devise == 1){
        Comptes(id) += + Montant_Choisi_Utilisateur
      }
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f\n", Comptes(id))
    } 
    else {
      println("Le montant doit être un multiple de 10.")
    }
  } 
}

def Retrait(id : Int, Comptes : Array[Double]) : Unit = {
  //Retrait
  var Boucle = true
  var Limite_Retrait = Comptes(id) * 0.1
  val Billets_CHF = Array(500,200,100,50,20,10)
  val Billet_EUR = Array(100,50,20,10)
  val Monnaie = Array("","CHF","EUR")
  var Devise = 0
  var Coupures = 0
  var start = 0
  var Display_Retrait : String = "Veuillez retirer la somme demandée :\n"
  while(Boucle) {
    
    while (Devise != 1 && Devise != 2) {
      Devise = readLine("Indiquez la devise : 1) CHF, 2) EUR :").toInt
      if (Devise != 1 && Devise != 2) {
        println("Veuillez choisir une option valable (1 ou 2).")
        }
      }
    if (Devise == 2){
      start = 2
    }
    var Montant_Choisi_Utilisateur = readLine("Indiquez le montant du retrait :").toInt
    if (Montant_Choisi_Utilisateur % 10 != 0){
      println("Le montant doit être un multiple de 10.")
    }
    else if (Montant_Choisi_Utilisateur > Limite_Retrait){
      println ("Votre plafond de retrait autorisé est de :"+ Limite_Retrait)
    } 
    else {
      Boucle = false
      if (Montant_Choisi_Utilisateur <200 || Devise == 2){
        Coupures = 2
      }
      while (Coupures !=1 && Coupures!=2){
        Coupures = readLine("In 1) Grosse Coupures, 2) Petite Coupure>").toInt 
      }
      var Montant_Restant = Montant_Choisi_Utilisateur

      for (i <- start.to(5)) {

        var x = Billets_CHF(i)
        var Nb_Billets_Restant: Int = Montant_Restant / x
        var Ordre: Int = Nb_Billets_Restant
        while (Nb_Billets_Restant!=0 && (Nb_Billets_Restant <= Ordre || Ordre < 0)){
          println ("\nIl reste " + Montant_Restant + " " + Monnaie(Devise) + " à distribuer.\nVous pouvez obtenir au maximum de " + Nb_Billets_Restant + " billets de  " + x + " " + Monnaie(Devise) + ".")
          var inpstr = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >").toString
          if (inpstr == "o"){
            Ordre = Nb_Billets_Restant
            Nb_Billets_Restant = 0
          }
          else {
            Ordre = inpstr.toInt 
          }
        }
        if (Ordre > 0){
          var Deduc = Ordre * x
          Montant_Restant -= Deduc
          Comptes(id) -= Deduc
          Display_Retrait += Ordre + " billets de " + x + " " + Monnaie(Devise) + "\n"
        }
      }
      println (Display_Retrait)
    }
  }
}
def Changement_PIN(id : Int, Code_PIN : Array[String]) : Unit = {
  // Changer le code PIN
  var Nombre_Charact_Min = 0
  while(Nombre_Charact_Min < 8){ 
    var New_PIN = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) :").toString
    Nombre_Charact_Min = New_PIN.length()
    if (Nombre_Charact_Min >= 8){ 
      Code_PIN(id) = New_PIN 
      println("Changement effectué correctement")
    }
    else {
      println("Votre code pin ne contient pas au moins 8 caractères.")
    }
  }
}
  def main(args: Array[String]): Unit = {
    var is_user = true

    while ( is_user ) {
      var Action_Ulilisat = 1
      var ID_1 = true
      var ID_Utilisat = true
      var Nb_Essaie = 3
      var ID = -1
      ID = readLine("Saisissez votre code identifiant :").toInt
      if ( ID >= 0 && ID < Nb_Clients ) {
        while (Nb_Essaie > 0) {
          var PIN_Ecrit_Utilisat = readLine("Saisissez votre code pin :").toString
          if(PIN_Ecrit_Utilisat != Code_PIN(ID)){
            Nb_Essaie -= 1
            println("Code pin erroné, il vous reste  " + Nb_Essaie + " tentatives :")
            if (Nb_Essaie == 0){

              println("Trop d’erreurs, abandon de l’identification")
              Action_Ulilisat = -1
            }
          }
          else {

            Nb_Essaie = 0
            Action_Ulilisat = 1
          }
        }
      }
      else {

        println("Cet identifiant n’est pas valable.")
        ID_Utilisat = false
        Action_Ulilisat = -2
      }

      if(Action_Ulilisat > 0){
        while (Action_Ulilisat != 5 && Action_Ulilisat > 0){
          println("\nChoisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consulter le solde\n4) Changer le PIN\n5) Terminer\n")
          Action_Ulilisat = readLine("Votre choix : ").toInt
          if (ID_1 && Action_Ulilisat != 5) {
            ID_1 = false
            var prompt = "Saisissez votre code pin "
            while (Nb_Essaie > 0){
              var PIN_Ecrit_Utilisat = readLine(prompt).toString 
              if (PIN_Ecrit_Utilisat != Code_PIN){
                Nb_Essaie -= 1
                prompt = "Code pin erroné, il vous reste "+ Nb_Essaie + " tentatives >"
                if (Nb_Essaie == 0){
                  Action_Ulilisat = -1
                  println("Trop d’erreurs, abandon de l’identification")
                }
              } 
              else {
                Nb_Essaie = 0
              }
            }
          }

          if (Action_Ulilisat == 1) {

            Dépôt(ID, Comptes)
          }
          else if (Action_Ulilisat == 2) {

            Retrait(ID,Comptes)
          }
          else if(Action_Ulilisat == 3) {

            printf("Le montant disponible sur votre compte est de: %.2f\nK", Comptes(ID))
          }
          else if (Action_Ulilisat == 4) {

             Changement_PIN(ID, Code_PIN)
          }
          else if (Action_Ulilisat == 5) {

            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          }
          else {
          }
        }
      }
    }  
  }  
}