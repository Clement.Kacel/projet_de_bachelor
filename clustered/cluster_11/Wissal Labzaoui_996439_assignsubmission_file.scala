//Assignment: Wissal Labzaoui_996439_assignsubmission_file

import scala.io.StdIn._

object Main {

def main(args: Array[String]): Unit = {




val nbclients = 100
var comptes = new Array[Double](nbclients)
var codespin = new Array[String](nbclients)
for (i <- 0 until nbclients) {
comptes(i) = 1200.0
codespin(i) = "INTRO1234"
}

var continuer = true

while (continuer) {
println("Saisissez votre code identifiant >")
val id = readInt()

if (id < 0 || id >= nbclients) {
println("Cet identifiant n'est pas valable.")
System.exit(0)
} else {
var pinCorrect = false
var tentatives = 3

while (!pinCorrect && tentatives > 0) {
println("Saisissez votre code pin >")
val codepin = readLine()

if (codespin(id) == codepin) {
pinCorrect = true
} else {
tentatives -= 1
println(s"Code pin erroné, il vous reste $tentatives tentatives >")
}

if (tentatives == 0) {
println("Trop d’erreurs, abandon de l’identification")
}
}

if (pinCorrect) {
var choix = 0
do {
println("Choisissez votre opération :")
println("1) Dépôt")
println("2) Retrait")
println("3) Consultation du compte")
println("4) Changement du code pin")
println("5) Terminer")

choix = readInt()

choix match {
case 1 => depot(id, comptes)
case 2 => retrait(id, comptes)
case 3 => println(s"Le montant disponible sur votre compte est ${comptes(id)} CHF")
case 4 => changepin(id, codespin)
case 5 => {
println("Fin des opérations, n’oubliez pas de récupérer votre carte.")

}
case _ => println("Choix invalide. Veuillez réessayer.")
}
} while (choix != 5)
}
}
}
}

def depot(id: Int, comptes: Array[Double]): Unit = {
println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
val devise = lireDevise()
println("Indiquez le montant du dépôt >")

var montant = lireMontant()

if (devise == "2") {
montant *= 0.95
}

comptes(id) += montant
println(s"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de ${comptes(id)} CHF")
}

def retrait(id: Int, comptes: Array[Double]): Unit = {
println("Indiquez la devise pour le retrait : 1) CHF ; 2) EUR >")
val devise = lireDevise()
println("Indiquez le montant à retirer >")
var montant = lireMontantRetrait(comptes(id))

if (devise == "1") {
distributionBillets(montant)
 comptes(id) -= montant
}
 else {

distributionBilletsEur(montant)
   comptes(id) -= (montant*0.95)
}


println(s"Retrait effectué. Nouveau solde est de ${comptes(id)} CHF")
}




 def distributionBillets(montant: Double): Unit = {
   val billets = List(500, 200, 100, 50, 20, 10)
   var reste = montant.toInt

   for (billet <- billets) {
     val nbBillets = reste / billet
     if (nbBillets > 0) {
       println(s"Vous pouvez obtenir au maximum $nbBillets billet(s) de $billet CHF")
       println("Appuyez sur 'o' pour OK ou entrez le nombre de billets souhaités")
       val input = readLine()
       val nombre = if (input == "o" || billet == 10) nbBillets else input.toInt.min(nbBillets)
       println(s"Vous obtenez $nombre billet(s) de $billet CHF")
       reste -= nombre * billet
     }
   }
 }

 def distributionBilletsEur(montant: Double): Unit = {
   val billetsEur = List(100, 50, 20, 10)
   var reste = montant.toInt

   for (billet <- billetsEur) {
     val nbBillets = reste / billet
     if (nbBillets > 0) {
       println(s"Vous pouvez obtenir au maximum $nbBillets billet(s) de $billet EUR")
       println("Appuyez sur 'o' pour OK ou entrez le nombre de billets souhaités")
       val input = readLine()

       val nombre = (billet, input) match {
         case (10, _) => nbBillets // Si billet de 10, on donne tous les billets restants
         case _ if input == "o" => nbBillets
         case _ => input.toInt.min(nbBillets)
       }

       println(s"Vous obtenez $nombre billet(s) de $billet EUR")
       reste -= nombre * billet
     }
   }
 }

def lireMontantRetrait(solde: Double): Double = {
var montant = readDouble()
while (montant % 10 != 0 || montant > solde) {
if (montant % 10 != 0) {
println("Le montant doit être un multiple de 10")
}
if (montant > solde) {
println(s"Montant supérieur au solde disponible de $solde CHF")
}
montant = readDouble()
}
return montant
}

def changepin(id: Int, codespin: Array[String]): Unit = {
println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
var newCodePin = readLine()

while (newCodePin.length < 8) {
println("Votre code pin ne contient pas au moins 8 caractères")
newCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
}

codespin(id) = newCodePin
println("Changement de code pin effectué.")
}

def lireDevise(): String = {

var devise = readLine().toUpperCase

while (devise != "1" && devise != "2") {
println("Devise inconnue. Veuillez saisir CHF ou EUR.")
devise = readLine().toUpperCase
}

return devise
}

def lireMontant(): Double = {
var montant = readLine().toDouble

while (montant % 10 != 0) {
println("Le montant doit être un multiple de 10")
montant = readLine().toDouble
}

return montant
}

}