//Assignment: Hugues Miki Ishimwe_996530_assignsubmission_file

import io.StdIn._

object Main {
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    val codespint = "INTRO1234"
    val comptes = Array.fill(nbclients)(1200.0)
    val codespin = Array.fill(nbclients)("INTRO1234")

    val tauxchangeCHFtoEUR = 1.05
    val tauxchangeEURtoCHF = 0.95

    // Identification de l'utilisateur
    val id = readLine("Saisissez votre code identifiant ").toInt 
    if (id >= nbclients) {
      println("Cet identifiant n'est pas valable.")
      System.exit(1) // Arrêt du programme si l'identifiant n'est pas valable
    }

    // Vérification du code PIN
    var tentatives = 3
    while (tentatives > 0) {
      val saisircodespin = readLine("Saisissez votre code pin ")
      if (saisircodespin == codespin(id)) {
        tentatives = 0
        println("Votre code pin est correct.")
      } else { 
        println("Code pin erroné, il vous reste " + tentatives + " tentatives.")
        tentatives -= 1
        if (tentatives == 0) {
          println("Trop d'erreurs, abandon de l'identification.")
          System.exit(1) // Arrêt du programme si trop d'erreurs de code PIN
        }
      }
    }

    // Menu principal
    var choix = 0
    while (choix != 5) {
      println("Choisissez votre opération :")
      println("1) Dépot.")
      println("2) Rétrait.")
      println("3) Consultation du compte.")
      println("4) Changement du code pin.")
      println("5) Terminer.")

      choix = readLine("Votre choix :").toInt

      // Opération du dépôt
      
      if (choix == 1) {
        val devise = readLine("Indiquez la devise du dépot : 1) CHF; 2) EUR >").toInt
        var montantDepot = readLine("Indiquez le montant du dépot >").toInt

        while (montantDepot % 10 != 0) {
          println("Le montant doit être multiple de 10.")
          montantDepot = readLine("Indiquez le montant du dépot >").toInt
        }

        if (devise == 1) {
          comptes(id) += montantDepot
          println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id))
        } else if (devise == 2) {
          comptes(id) += (montantDepot * tauxchangeEURtoCHF)
          println("Votre dépot a été pris en compte, le nouveau montant disponible sur votre compte est >" + comptes(id))
        }
      }
       
      // opération du retrait
    
       if (choix == 2) {
        val devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR >").toInt
        var montantRetrait = readLine("Indiquez le montant du retrait > ").toInt
        while (montantRetrait%10 != 0){
          println("le montant doit être multiple de 10")
          montantRetrait = readLine("Indiquez le montant du retrait >").toInt
        }

         val plafondRetrait = comptes(id) 
         if (montantRetrait > plafondRetrait){
           println("Votre plafond de retrait autorisé est de : " + plafondRetrait)
          montantRetrait = readLine("Indiquez le montant du retrait > ").toInt
         }
         

       if (devise == 1) {
         var choixcoupures = 0
         while (choixcoupures != 1 && choixcoupures != 2) {
           choixcoupures = readLine("1) En grosses coupures ; 2) en petites coupures > ").toInt
         }
         if (choixcoupures != 1 && comptes(id) >= 200) {
         val coupuresDisponible = Array(500, 200)
          

         } else {
         if (choixcoupures == 2){
           val coupuresDisponible = Array(100, 50, 20, 10)
            
         }
           comptes(id) -= montantRetrait
         println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id))
      
         }
         
         if (devise == 2){
          comptes(id) -= (montantRetrait * tauxchangeEURtoCHF)
           val  coupuresDisponible = Array(100, 50, 20, 10)
          println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id))
    }
       }
         
       
        
           
         
  
      // opération de consultation du compte
      if (choix == 3) {
        println ("le montant disponible sur votre compte est de : " + comptes(id))
      }
      // opération de changement du code pin
     
        
      if (choix == 4) {
        var nouveaucodepin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractéres) : ").toString
        while (nouveaucodepin.length < 8) {
          println("votre code pin ne contient pas au moins 8 caractéres.")
          nouveaucodepin = readLine("saisissez votre nouveau code pin (il doit contenir au moins 8 caractéres) : ").toString
          

        }
      }

      // opération de Terminer
      if (choix == 5) {
        println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
      }

       }
    }
  }
}


        






  


