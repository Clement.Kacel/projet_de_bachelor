//Assignment: Amélie Sophie Filip_996590_assignsubmission_file

object Main {

  def main(args: Array[String]): Unit = {
    val nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")

    def depositOperation(clientId: Int): Unit = {
      var currencyChoice = ""
      do {
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        currencyChoice = scala.io.StdIn.readLine()
        if (currencyChoice != "1" && currencyChoice != "2")
          println("Choix invalide, veuillez réessayer.")
      } while (currencyChoice != "1" && currencyChoice != "2")

      println("Indiquez le montant du dépôt >")
      var depositAmount = scala.io.StdIn.readDouble()

      while (depositAmount % 10 != 0) {
        println("Le montant doit être un multiple de 10")
        depositAmount = scala.io.StdIn.readDouble()
      }

      if (currencyChoice == "2") {
        //  EUR en CHF
        depositAmount = (depositAmount * 0.95)
      }

      comptes(clientId) += depositAmount
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(clientId))
    }

    def consultationOperation(clientId: Int): Unit = {
      printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(clientId))
    }

    def withdrawalOperation(clientId: Int): Unit = {
      var withdrawalCurrency = ""
      do {
        println("Indiquez la devise : 1) CHF ; 2) EUR >")
        withdrawalCurrency = scala.io.StdIn.readLine()
        if (withdrawalCurrency != "1" && withdrawalCurrency != "2")
          println("Choix invalide, veuillez réessayer.")
      } while (withdrawalCurrency != "1" && withdrawalCurrency != "2")

      println("Indiquez le montant du retrait >")
      var withdrawalAmount = scala.io.StdIn.readDouble()

      while (withdrawalAmount % 10 != 0 || withdrawalAmount > comptes(clientId) * 0.1) {
        if (withdrawalAmount % 10 != 0) {
          println("Le montant doit être un multiple de 10")
        } else {
          printf("Votre plafond de retrait autorisé est de : %.2f CHF\n", comptes(clientId) * 0.1)
        }
        withdrawalAmount = scala.io.StdIn.readDouble()
      }

      // Grosses et petites coupures en CHF
      var useLargeBills = false
      if (withdrawalCurrency == "1" && withdrawalAmount >= 200) {
        var billChoice = ""
        do {
          println("En 1) grosses coupures, 2) petites coupures >")
          billChoice = scala.io.StdIn.readLine()
          if (billChoice != "1" && withdrawalCurrency != "2")
            println("Choix invalide, veuillez réessayer.")
        } while (billChoice != "1" && billChoice != "2")

        if (billChoice == "1") {
          useLargeBills = true
        }
      }

      // Retrait
      var remainingAmount = withdrawalAmount
      val availableBills = if (useLargeBills) List(500, 200, 100, 50, 20, 10) else List(100, 50, 20, 10)

      for (bill <- availableBills) {
        if (remainingAmount >= bill) {
          val maxBills = remainingAmount / bill
          var billCount = 0

          do {
            printf("Il reste %.2f %s à distribuer\n", remainingAmount, if (withdrawalCurrency == "1") "CHF" else "EUR")
            printf("Vous pouvez obtenir au maximum %d billet(s) de %d %s\n", maxBills.toInt, bill, if (withdrawalCurrency == "1") "CHF" else "EUR")
            print("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            val userChoice = scala.io.StdIn.readLine()

            if (userChoice == "o") {
              billCount = maxBills.toInt
            } else {
              print("Entrez le nombre de billets que vous souhaitez > ")
              billCount = scala.io.StdIn.readInt()
            }
          } while (billCount > maxBills || billCount < 0)

          remainingAmount -= billCount * bill
          //          printf("Il reste %.2f %s à distribuer\n", remainingAmount, if (withdrawalCurrency == 1) "CHF" else "EUR")
          //          printf("Vous pouvez obtenir au maximum %d billet(s) de %d %s\n", maxBills.toInt, bill, if (withdrawalCurrency == 1) "CHF" else "EUR")

          if (billCount > 0) {
            printf("Veuillez retirer %d billet(s) de %d %s\n", billCount, bill, if (withdrawalCurrency == "1") "CHF" else "EUR")
          }
        }
      }

      // Actualisation
      if (withdrawalCurrency == "2") {
        // CHF en EUR
        withdrawalAmount = (withdrawalAmount * 0.95)
      }
      comptes(clientId) -= withdrawalAmount
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(clientId))
    }

    def changePinOperation(clientId: Int): Unit = {
      println("Saisissez votre nouveau code PIN (8 caractères minimum) > ")
      var newPin = scala.io.StdIn.readLine()
      while (newPin.length < 8) {
        println("Le code PIN doit comporter au moins 8 caractères, veuillez réessayer.")
        newPin = scala.io.StdIn.readLine()
      }
      codespin(clientId) = newPin
      println("Votre code PIN a été changé avec succès.")
    }

    // Main loop for client operations
    while (true) {
      println("Saisissez votre identifiant client > ")
      val clientId = scala.io.StdIn.readInt()

      if (clientId >= nbclients) {
        println("Identifiant client invalide. Le programme va se terminer.")
        return
      }

      var pinAttempts = 0
      var isAuthenticated = false
      while (pinAttempts < 3 && !isAuthenticated) {
        println("Saisissez votre code PIN > ")
        val enteredPin = scala.io.StdIn.readLine()

        if (enteredPin != codespin(clientId)) {
          pinAttempts += 1
          println(s"Code PIN erroné, il vous reste ${3 - pinAttempts} tentatives.")
        } else {
          isAuthenticated = true
        }
      }

      if (!isAuthenticated) {
        println("Nombre de tentatives de PIN dépassé. Retour à l'écran d'identification.")
      }

      var userChoice = ""
      while (userChoice != "5" && isAuthenticated) {
        println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changer de code PIN\n5) Terminer\nVotre choix : ")
        userChoice = scala.io.StdIn.readLine()

        userChoice match {
          case "1" => depositOperation(clientId)
          case "2" => withdrawalOperation(clientId)
          case "3" => consultationOperation(clientId)
          case "4" => changePinOperation(clientId)
          case "5" => println("Fin de vos opérations, n’oubliez pas de récupérer votre carte.")
          case _ => println("Choix invalide, veuillez réessayer.")
        }
      }
    }
  }
}