//Assignment: Maicen Dardour_996472_assignsubmission_file

import scala.io.StdIn._
import scala.util.control.Breaks._

object Main {
  def retrait(id:Int, comptes: Array[Double]) :Unit = {
    var valide:Boolean =  false
    var devise:String = ""
    var erreur:String = ""
    var choixDevise:String = ""
    var montantderetrait: Int = 1
    var denominationdechoix:Int = 0
    var Billets =  Map((500, 0), (200, 0), (100, 0), (50, 0), (20, 0), (10, 0))
    var Grossescoupurres:List[Int] = List(500,200,100,50,20,10)
    var Petitescoupurres:List[Int] = List(100,50,20,10)
    while (choixDevise != "1" && choixDevise != "2") {
            choixDevise = readLine("Indiquer la devise : 1) CHF ; 2)EUR >") 
                                   
         }
    while (!valide){
      montantderetrait = readLine("Indiquer le montant de retrait : > ").toInt
          var montantMax:Double = 0.1 * comptes(id)
          if (montantderetrait % 10 != 0 ){
            println("Le montant doit être un multiple de 10. ")
          }
          else if (montantderetrait > montantMax){
            println("Votre limite de retrait autorisée  : " + montantMax )
          }
          else {
            valide = true
          }
        }
        if (choixDevise == "1"){
          if (montantderetrait >= 200){
            while ((denominationdechoix != 1 && denominationdechoix != 2) ){
              print("En 1) Grossescoupurres, 2) Petitescoupurres > ")
              denominationdechoix = readInt()
            }
          }
        }
        if (choixDevise == "1"){
          comptes(id) -= montantderetrait
        }
        if (choixDevise=="2"){
          var montantEuros:Double = montantderetrait * 0.95
          comptes(id) -= montantEuros
        }
        comptes(id) = math.floor(comptes(id) * 100)/100
        var nbBillets:Int = 0
        var reste:Int = montantderetrait

        if (denominationdechoix == 1) {
          breakable {
            for (i <- Grossescoupurres) {
              nbBillets = reste / i
              reste = reste % i

              Billets = Billets.updated(i, nbBillets )
              if (reste == 0) {
                break
              }
            } 
          }
        }
        else if (denominationdechoix == 2 || denominationdechoix == 0 ){
          breakable {
            for (i <- Petitescoupurres) {
              nbBillets = reste / i
              reste = reste % i

              Billets = Billets.updated(i, nbBillets )
              if (reste == 0) {
                break
              }
            } 
          } 
        }

        for ( i <- Grossescoupurres){
          if (Billets(i) != 0) {
            if (choixDevise == "1"){
              println(Billets(i) + " Billets(s) de " + i +" CHF")
            }
            else {
              println(Billets(i) + " Billets(s) de " + i + " EUR")
            }
          }
        }  
        println("Votre retrait a été traité, le nouveau montant disponible sur votre compte est > "+ comptes(id))  


      }

   def depot(id: Int, comptes: Array[Double]) : Unit = {
     var choix:Int = 0
     var montantDuDepot:Int = 9
     print("Indiquer la devise du dépôt: 1) CHF; 2) EUR > ")
     choix = readInt()
     while(montantDuDepot < 10 || montantDuDepot % 10 !=0){
     print("Indiquez le montant de la caution: ")
     montantDuDepot = readInt()
     }
     var convertirMontant:Double = 0
     if (choix == 1){
       convertirMontant = montantDuDepot
     }
     else if (choix == 2){
       convertirMontant = montantDuDepot * 0.95
     }
     comptes(id) += convertirMontant
     comptes(id) = math.floor(comptes(id) * 100)/100
     println("Votre dépôt a été traité, le nouveau montant disponible sur votre compte  > " + comptes(id))
     } 

  def changepin(id:Int, codespin: Array[String]): Unit = {
    var nouveauMotDePasse:String = ""
    while (nouveauMotDePasse.length() < 8){
      nouveauMotDePasse = readLine("Entrez votre nouveau code PIN (il doit contenir au moins 8 caractères) > ")
      if (nouveauMotDePasse.length < 8){
       println("Votre code PIN ne contient pas au moins 8 caractères")
      }
    }
    codespin(id) = nouveauMotDePasse
  }
 
  def main(args: Array[String]): Unit = {
    var nbclientes:Int = 100
    var comptes = Array.fill(nbclientes)(1200.0)
    var codepin = Array.fill(nbclientes)("INTRO1234")
    var montantderetrait: Int = 0
    var utilisateurAuth :Boolean = false

    while(true) {

       print("Entrez votre identifiant > ")
       var codedeconnexion : Int = readInt()



       if (codedeconnexion > nbclientes - 1 ) {
         println("Cet identifiant n'est pas valide.")
         System.exit(0)
       }

       var nombreTentativesRestantes:Int = 3
       breakable { 
         while(nombreTentativesRestantes > 0){
           var pinUtilisateur:String = readLine("Entrez votre code PIN > " )
           if (pinUtilisateur == codepin(codedeconnexion)){
              utilisateurAuth = true
              nombreTentativesRestantes= -1
            }

            else if (nombreTentativesRestantes >1) {

                nombreTentativesRestantes -= 1
                println("Mauvais code PIN, vous l'avez toujours " + nombreTentativesRestantes + " tentatives")
            }
            else {
              println("Trop d'erreurs, abandon de l'identification")
              break
            } 
          }
        }
          breakable {
            while ( utilisateurAuth ) {
              var choixAction : Int = 0 
              println("\nChoisissez votre opération: \n1) Depot \n2) retrait \n3) Consultation de compte \n4) Changer le code PIN \n5) Finition")
              print("Votre choix : ")
              choixAction = readInt()  

              if (choixAction == 1){
                depot(codedeconnexion, comptes)
              }
              else if (choixAction == 2 ){
                retrait(codedeconnexion, comptes)
              }

              else if (choixAction == 3){
                println("Le montant disponible sur votre compte : " + comptes(codedeconnexion))
              }

              else if (choixAction == 4) {
                changepin(codedeconnexion,codepin)
              }

              else if (choixAction == 5) {
                println("Fin des opérations, pensez à récupérer votre carte.")
                utilisateurAuth = false
                break
              }
            }
          }
        }
      }
    }
