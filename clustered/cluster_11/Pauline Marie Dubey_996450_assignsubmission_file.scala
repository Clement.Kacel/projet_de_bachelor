//Assignment: Pauline Marie Dubey_996450_assignsubmission_file

import scala.io.StdIn._
object Main {
  def main(args: Array[String]): Unit = {

    //1.Les deux tableaux, comptes et codespin ont la même taille et cette taille sera pour cet exercice définie à partir d’une variable nbclients qui sera initialisée à 100 dans le programme.
    val nbclient = 100
    val comptes = Array.fill (100)(1200.0)
    val codespin = Array.fill(100)("INTRO1234")



   //2.Au lancement du programme, l’utilisateur-trice devra s’identifier avec son identifiant et son code PIN. Saisissez votre code identifiant >
    val id = readLine("Saisissez votre code identifiant >").toInt
//3.si l’utilisateur-trice fournit un numéro d’identifiant strictement supérieur à nbclients, le programme doit s’arrêter après avoir indiqué le message :Cet identifiant n’est pas valable.
  if (id >= 100) {
    println ("Cet identifiant n’est pas valable.")
  } 
// 4.declaration de variables et boucle while : demande du code pin, 3 tentatives, abandon
  var tentatives = 3
  var codegagnant = false

  while ((codegagnant != true ) && (tentatives == 1 || tentatives == 2 || tentatives == 3)){

  val pingagnant = readLine("Saisissez votre code pin >").toString

  if(pingagnant == codespin(id)){
    codegagnant = true
  }else if (pingagnant != codespin(id)){

    tentatives = tentatives - 1
    println("Code pin erroné, il vous reste " + tentatives + "tentatives >")
  }
  }
  if(tentatives == 0){
     println ("Trop d’erreurs, abandon de l’identification")
      val id = readLine("Saisissez votre code identifiant >").toInt
     if (id >= 100) {
       println ("Cet identifiant n’est pas valable.")
     }    
  }
    //5.affichage du menu si id et code pin ok
  println("Choisissez votre opération :")
  println("1) Dépôt")
  println("2) Retrait")
  println("3) Consultation du compte")
  println("4) Changement du code pin")
  println("5) Terminer")
  val choix = readInt()
  println("Votre choix :" + choix)

if(choix == 1) depot(id, comptes)
if(choix == 2) retrait (id, comptes)
if(choix == 4) changepin(id, codespin)
if (choix == 5){
     println ("Fin des opérations, n’oubliez pas de récupérer votre carte.")
    val id = readLine("Saisissez votre code identifiant >").toInt
     if (id >= 100) {
       println ("Cet identifiant n’est pas valable.")

     }    
    }
   //3) Opération du changement de code pin
 
 def changepin(id : Int, codespin : Array[String]) : Unit = {
 var nouveaupin = readLine ("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
   while (nouveaupin.length < 8){
     nouveaupin = readLine ("Votre code pin ne contient pas au moins 8 caractères  ")
   }
       codespin(id) = nouveaupin 
   println ("changement réussi")
}
     //1) Opération de depot

    def depot(id: Int, comptes: Array[Double]): Unit = {
      println("Choisissez la devise :")
      println("1) CHF")
      println("2) EUR")

      print("Votre choix : ")
      var choixdevise = readLine().toString
      
      println("Veuillez saisir le montant à déposer (doit être divisible par 10) en devise : ")
      var montant = readDouble()
        //devise CHF
      if (choixdevise == "CHF") {
        if (montant % 10 == 0) {
          comptes(id) = comptes(id) + montant
          println("Opération de dépôt réussie. Nouveau solde :" + comptes(id) + "CHF")
        } else {
          println("Le montant doit être divisible par 10.")
        }
      } else if (choixdevise == "EUR") {
        // devise EUR
        montant = montant * 1.1 // pour EUR
        
        if (montant % 10 == 0) {
          comptes(id) = comptes(id) + montant
          println("Opération de dépôt réussie. Nouveau solde : " + comptes(id) + "CHF")
        } else {
          println("Le montant doit être divisible par 10.")
        }
      } 
    }

    //operation de retrait 
 def retrait(id : Int, comptes : Array[Double]) : Unit = {
   println("Choisissez la devise :")
     println("1) CHF")
     println("2) EUR")

     print("Votre choix : ")
     var choixdevise = readLine().toString

     println("Veuillez saisir le montant de retrait (doit être divisible par 10) en devise : ")
     var montantretrait = readDouble()
       //devise CHF
     if (choixdevise == "CHF") {
       if (montantretrait % 10 == 0) {
         comptes(id) = comptes(id) - montantretrait
         println("Opération de retrait réussie. Nouveau solde :" + comptes(id) + "CHF")
       } else {
         println("Le montant doit être divisible par 10.")
       }
     } else if (choixdevise == "EUR") {
       // devise EUR
       montantretrait = montantretrait * 1.1 // pour EUR

       if (montantretrait % 10 == 0) {
         comptes(id) = comptes(id) - montantretrait
         println("Opération de retrait réussie. Nouveau solde : " + comptes(id) + "CHF")
       } else {
         println("Le montant doit être divisible par 10.")
       }
     } 
   }


   
   
 }


    
 



}
  


