//Assignment: Ian Jerzy Grobis_996385_assignsubmission_file

import scala.io.StdIn._
import scala.util.Try

object Main {

  val menu : String = """Choisissez votre opération :
  1) Dépôt
  2) Retrait
  3) Consultation du compte
  4) Modification du code pin
  5) Terminer
  Votre choix : """

  val choixpin : String = "Saisissez votre code pin > "
  val msgerreurpin = "Code pin erroné, il vous reste %d tentatives"
  val msgerreurfin : String = "Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte."
  val tentativesmax : Int = 3
  val codepin : String = "INTRO1234"
  var montantcompte : Double = 1200.0
  val eurofranc : Double = 0.95
  val franceuro : Double = 1.05

  def verifpin(tentativesmax: Int): Boolean = {
    var nbtentatives = tentativesmax
    var tentativepin = ""
    while (nbtentatives>0){
      tentativepin = readLine(choixpin)
      if(tentativepin == codepin){
        return true
      }else{
        nbtentatives -= 1
        if(nbtentatives>0){
          println(msgerreurpin.format(nbtentatives))
        }
      }
    }
    println(msgerreurfin)
    return false
  }
  val msgdevisedepot : String = "Indiquez la devise du dépôt : 1) CHF ; 2) EUR > "
  val msgmontantdepot : String = "Indiquez le montant du dépôt > "
  val msgerreurdepot : String = "Le montant doit être un multiple de 10."
  val msgreussitedepot : String = "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF."
  def depot(amount: Double): Double = {
    var montantcompte : Double = amount
    var devisedepot : String = ""
    while (devisedepot!="1" && devisedepot!="2"){
      devisedepot = readLine(msgdevisedepot)
    }
    var montantdepot : String = readLine(msgmontantdepot)
    while (!Try(montantdepot.toInt).isSuccess || montantdepot.toInt%10!=0 || montantdepot.toInt<10){
      println(msgerreurdepot)
      montantdepot = readLine(msgmontantdepot)
    }
    val montantdepotint : Int = montantdepot.toInt
    if(devisedepot=="1"){
      montantcompte += montantdepotint
    }else{
      montantcompte += montantdepotint*eurofranc
    }
    println(msgreussitedepot.format(montantcompte))
    return montantcompte
  }
  val msgdeviseretrait : String = "Indiquez la devise du retrait : 1) CHF ; 2) EUR > "
  val msgmontantretrait : String = "Indiquez le montant du retrait > "
  val msgerreurretrait : String = "Le montant doit être un multiple de 10."
  val msgerreurplafond : String = "Votre plafond de retrait autorisé est de : %d CHF"
  val msgchoixcoupures : String = "En 1) grosses coupures, 2) petites coupures > "
  val msgreste : String = "Il reste %d %s à distribuer"
  val msgmaxcoupures : String = "Vous pouvez obtenir au maximum %d billets de %d %s"
  val msgchoixo : String = " Tapez o pour ok ou une autre valeur inférieure à celle proposée > "
  val msgretraitargent : String = """Veuillez retirer la somme demandée :
  """
  val msgretraitcoupures : String = "%d billet(s) de %d %s"
  val msgreussiteretrait : String = "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF."

  val msgfin : String = "Fin des opérations, n’oubliez pas de récupérer votre carte."

  def retrait(amount: Double): Double = {
    var montantcompte: Double = amount
    var deviseretrait: String = ""
    while(deviseretrait!="1" && deviseretrait!="2"){
      deviseretrait = readLine(msgdeviseretrait)
    }
    var plafond: Int = 0
    if(deviseretrait=="1"){ 
      plafond = (montantcompte*0.01).toInt*10 
    } else { 
      plafond = (montantcompte*franceuro*0.01).toInt*10 
    }

    var montantretrait: String = readLine(msgmontantretrait)
    while(!Try(montantretrait.toInt).isSuccess || montantretrait.toInt<10 || montantretrait.toInt%10!=0 || montantretrait.toInt>plafond){
      println(msgerreurretrait)
      println(msgerreurplafond.format(plafond))
      montantretrait = readLine(msgmontantretrait)
    }
    var montantretraitint: Int = montantretrait.toInt

    montantcompte = montantcompte - (if(deviseretrait=="1") montantretraitint else montantretraitint*eurofranc) 

    var coupures: String = "" 
    if(montantretraitint>=200 && deviseretrait=="1"){ 
      while(coupures!="1" && coupures!="2"){ 
        coupures = readLine(msgchoixcoupures)
      }
    } else{
      coupures = "2"
    }
    var coupuresdispo: List[Int] = List()
    if(coupures == "1"){ 
      coupuresdispo = List(500, 200, 100, 50, 20, 10)
    } else { 
      coupuresdispo = List(100, 50, 20, 10)
    }
    var coupuresactuelles: Int = 0
    val devisestring: String = if(deviseretrait=="1") "CHF" else "EUR"

    var listeretrait: List[(Int, Int)] = List()
    var retraitchoixo: String = ""
    while(montantretraitint>0 && coupuresactuelles<coupuresdispo.length){
      if(montantretraitint/coupuresdispo(coupuresactuelles)>0){ 
        if(coupuresactuelles!=coupuresdispo.length-1){
          println(msgreste.format(montantretraitint, devisestring))
          println(msgmaxcoupures.format(montantretraitint/coupuresdispo(coupuresactuelles), coupuresdispo(coupuresactuelles), devisestring))

          retraitchoixo = readLine(msgchoixo)
          while (!(retraitchoixo=="o" || retraitchoixo == "ok" || (Try(retraitchoixo.toInt).isSuccess && retraitchoixo.toInt>=0 && retraitchoixo.toInt<=montantretraitint/coupuresdispo(coupuresactuelles)))){ 
            retraitchoixo = readLine(msgchoixo)
          }
          if(retraitchoixo == "o" || retraitchoixo == "ok"){
            listeretrait = listeretrait :+ (montantretraitint/coupuresdispo(coupuresactuelles), coupuresdispo(coupuresactuelles))
            montantretraitint -= (montantretraitint/coupuresdispo(coupuresactuelles))*coupuresdispo(coupuresactuelles)
          } else if(retraitchoixo.toInt>0){
            listeretrait = listeretrait :+ (retraitchoixo.toInt, coupuresdispo(coupuresactuelles))
            montantretraitint -= retraitchoixo.toInt*coupuresdispo(coupuresactuelles)
          }
        } else { 
          listeretrait = listeretrait :+ (montantretraitint/coupuresdispo(coupuresactuelles), coupuresdispo(coupuresactuelles))
          montantretraitint -= (montantretraitint/coupuresdispo(coupuresactuelles))*coupuresdispo(coupuresactuelles)
        }
      }
      coupuresactuelles+=1 
    }
    println(msgretraitargent)
    for (e <- listeretrait){
      println(msgretraitcoupures.format(e._1, e._2, devisestring))
    }
    println(msgreussiteretrait.format(montantcompte))
    return montantcompte
  }
  val msgconsultationcompte : String = "Le montant disponible sur votre compte est de %.2f CHF."

  def consultationcompte(): Unit = {
    println(msgconsultationcompte.format(montantcompte))
  }
  def modificationcodepin(): Unit = {
    println("""
Je n'ai pas réussi à terminer, ni à faire fonctionner le code. Je préfère donc rendre cette version à peine modifiée, qui peut à minima compiler et fonctionner pour l'exercice 1.  
    """)
  }

  def main(args: Array[String]): Unit = {
    var choixmenu: String = readLine(menu)
    while (choixmenu!="1" && choixmenu!=2 && choixmenu!="3" && choixmenu!="4" && choixmenu!="5"){
      choixmenu = readLine(menu)
    }
    if (choixmenu=="1" || choixmenu=="2" || choixmenu=="3" || choixmenu=="4"){
      if(verifpin(tentativesmax)){
        while(choixmenu!="5"){
          if(choixmenu=="1"){
            montantcompte = depot(montantcompte)
          }else if(choixmenu=="2"){
            montantcompte = retrait(montantcompte)
          }else if(choixmenu=="3"){
            consultationcompte()
          }else if(choixmenu=="4"){
            modificationcodepin()
          }
          choixmenu = readLine(menu)
        }
        println(msgfin)
      }
    }else{
      println(msgfin)
    }
  } 
}