//Assignment: Alp Aliev Huryilmaz_996561_assignsubmission_file

import io.StdIn._

object Main {

  // Définition de la méthode dépôt
  def depot(id:Int, comptes: Array[Double]): Unit = {
    var operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
    for(id <- 0 to comptes.length){
      var pin = readLine("Saisissez votre code pin: \n > ")
      while(!(codespin(i) == "INTRO1234") || (nbtentativesfausses == 3)){
        codespin(i) = readLine("Code pin eroné, il vous reste " + (3 - nbtentativesfausses) + " tentatives >").toString
        if(codespin(i) == "INTRO1234"){
          nbtentativesfausses = 0
        } else {
          nbtentativesfausses += 1
        }
      }
      if(nbtentativesfausses == 3){
        println("Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte.")
        println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + montantdisponible)
      var devise = readLine("Entrez la devise : 1) CHF 2) EUR").toInt
      var montant = readLine("Indiquez le montant du dépôt: > ").toDouble
      var nbtentatives = 0
      var montantdisponible = 0.0
      var montantretrait = 0.0
      var tauxEuro2CHF = 0.95
      if(devise == '2'){
        montant *= tauxEuro2CHF
      }
      while(!(montant % 10 == 0)){
        println("Le montant doit être un multiple de 10")
    }
    montantdisponible += montant
    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + montantdisponible)
  }

  // Définition de la méthode de retrait 
  def retrait(id:Int, comptes: Array[Double]): Unit = {
    var operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
    var pin = readLine("Saisissez votre code pin: \n > ")
    var devise = readLine("Entrez la devise : 1) CHF 2) EUR").toInt
    var montantretrait = 0.0
    var tauxEuro2CHF = 0.95
    var id = 0
    var coupure = readLine("Choisissez votre coupure : \n 1) 500CHF \n 2) 200CHF \n 3) 100CHF \n 4) 50CHF \n 5) 20CHF \n 6) 10 CHF").toInt   
    for(id <- 0 to comptes.length){
      var retraitautorise = comptes(id) * 0.10
      if(devise == '2'){
        montantretrait *= tauxEuro2CHF
      }
      while(retraitautorise > comptes(id) * 0.10){
        println("Votre plafond de retrait autorisé est de " + retraitautorise)
      }
      if(coupure == '1'){
        comptes(i) > 200
      }
      else{
        comptes(i) < 200
      }
      var nbbillets = 0
      var montant1 = 0.0
      while(montant1 > 0){
      var coupuredisponibleCHF = readLine("Choisissez votre coupure : \n 1) 500CHF \n 2) 200CHF \n 3) 100CHF \n 4) 50CHF \n 5) 20CHF \n 6) 10 CHF").toInt   
      var coupuredisponibleEUR = readLine("Choisissez votre coupure : \n 1) 100CHF \n 2) 50EUR \n 3) 20EUR \n 4) 10EUR").toInt 
      while(comptes(id) > 500){
      if(comptes(id) / 500 == 1){
        nbbillets = readLine("Combien de billets de 500 CHF voulez-vous retirer ? >").toInt  
        comptes(i) = comptes(i) - (nbbillets * 500)
      }
      else if(montant / 200 == 1){
        nbbillets = readLine("Combien de billets de 200 CHF voulez-vous retirer ? >").toInt  
        comptes(i) = comptes(i) - (nbbillets * 200)
      }
      else if(montant / 100 == 1){
        nbbillets = readLine("Combien de billets de 100 CHF voulez-vous retirer ? >").toInt  
        montantdisponible = montantdisponible - (nbbillets * 100)
      }
      else if(montant / 50 == 1){
        nbbillets = readLine("Combien de billets de 50 CHF voulez-vous retirer ? >").toInt  
        montantdisponible = montantdisponible - (nbbillets * 50)
      }
      else if(montant / 20 == 1){
        nbbillets = readLine("Combien de billets de 20 CHF voulez-vous retirer ? >").toInt  
        montantdisponible = montantdisponible - (nbbillets * 20)
      }
      else {
        nbbillets = readLine("Combien de billets de 100 CHF voulez-vous retirer ? >").toInt  
        montantdisponible = montantdisponible - (nbbillets * 10)
      }

      println("Il reste " + retraitautorise + " à distribuer.")
      println("Vous pouvez obtenir au minimum " + nbbillets + " billets " + " de " + coupuredisponibleCHF + " CHF")
      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      if(readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >") == 'o'){
        println("Veuillez retirer la somme demandée: \n " + nbbillets + " billet(s) de " + montant + " CHF")
      }
      printf("La valeur du montant sur le compte est: %.2f \n", montantdisponible)
  }
  }
  }
    
  // Définition de la méthode qui nous permet de changer le code pin d'un utilisateur
  def changepin(id:Int, codespin: Array[String]): Unit = {
    var operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
    for(id <- 0 to codespin.length){
      var nouveauCodePin = readLine("Saisissez votre nouveau code pin: \n > ")
    while(nouveauCodePin.length < 8){
      println("Votre code pin ne comporte pas au moins 8 caractères")
    }
    }
  }

  // Les opérations de consultation du compte et de la fin de l'opération d'un client sont définies ci-dessous
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
    var id = Array.fill(nbclients)(0)
    var nbtentativesfausses = 0
    var codeid = readLine("Saisisez votre code identifiant > ").toInt
    if(codeid > nbclients){
      println("Cet identifiant n'est pas valable.")
    }
    for(id <- 0 to nbclients - 1){
      do{
        println("Saisissez votre code pin > ")
      }while(codespin(i) != "INTRO1234")  
      if(codespin(i) == "INTRO1234" && id == true){
        operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
      }
    } 
    if(operation == 5){
      println("Fin des opérations, n'oubliez pas de recuperer votre carte")
      var nouveauCodePin = readLine("Saisissez votre nouveau code pin: > ")
    } else {
      for(id <- 0 to codespin.length - 1){
        while(!(codespin(i) == "INTRO1234") || (nbtentativesfausses == 3)){
          codespin(i) = readLine("Code pin eroné, il vous reste " + (3 - nbtentativesfausses) + " tentatives >").toString
          if(codespin(i) == "INTRO1234"){
            nbtentativesfausses = 0
          } else {
            nbtentativesfausses += 1
          }
        }
        if(nbtentativesfausses == 3){
          println("Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte.")
          println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + montantdisponible)
        }
    while(operation != 5){
      if(operation == 4){
        for(i <- 0 to nbclients - 1){
          if(codespin(i).length == 8){
            // appel à la méthode changepin
            var nouveauPin = changepin(id, codespin)
            println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
        } else {
          println("Votre code pin ne contient pas 8 caractères")
        }
        operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
        }
      }
        if(operation == 5){
          println("Fin des opérations, n'oubliez pas de recupérer votre carte")
          var nouveauCodePin = readLine("Saisissez votre nouveau code pin: > ")
        }
      else if(operation == 3){
        printf("Le montant disponible sur le compte actuel est de: %2f CHF ", montantDisponible)
        operation = readLine("Saisissez l'operation souhaitée: \n 1) Dépot \n 2) Retrait \n 3)Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n > ").toInt
        if(operation == 5){
          println("Fin des opérations, n'oubliez pas de recuperer votre carte")
        }
      }
      else if(operation == 2){                  
        for(i <- 0 to nbclients - 1){
          // appel à la méthode retrait
          var montantretrait = retrait(id, codespin)
        }
        if(operation == 5){
          println("Fin des opérations, n'oubliez pas de recupérer votre carte")
          var nouveauCodePin = readLine("Saisissez votre nouveau code pin: > ")
        }
      else if(operation == 1){
        for(i <- 0 to nbclients - 1){
          // appel à la méthode depot
          var montantdepose = depot(id, codespin)
        if(operation == 5){
          println("Fin des opérations, n'oubliez pas de recupérer votre carte")
          var nouveauCodePin = readLine("Saisissez votre nouveau code pin: > ")
        }
      }
    }
    }
  }
}  
}
}
}
}
}