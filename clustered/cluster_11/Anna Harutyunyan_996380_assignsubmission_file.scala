//Assignment: Anna Harutyunyan_996380_assignsubmission_file

import scala.io.StdIn._

object Main {
  def depot(id: Int, comptes: Array[Double]): Unit = {
    var deviseChoisie = ""
    var montantDepot: Double = 0.0

    do {
      deviseChoisie = readLine("Indiquez la devise du dépôt :\n1) CHF\n2) EUR ")
    } while (deviseChoisie != "1" && deviseChoisie != "2")

    do {
      montantDepot = readLine("Indiquez le montant du dépôt : ").toDouble
      if (montantDepot % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
      }
    } while (montantDepot % 10 != 0)

    val montantCHF = if (deviseChoisie == "2") (montantDepot * 0.95) else montantDepot
    comptes(id) += montantCHF

    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {

    var deviseChoisie = ""
    var montantRetrait: Double = 0.0

    do {
      deviseChoisie = readLine("Indiquez la devise du retrait :\n1) CHF\n2) EUR ")
    } while (deviseChoisie != "1" && deviseChoisie != "2")

    do {
      montantRetrait = readLine("Indiquez le montant du retrait : ").toDouble
      if (montantRetrait % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
      } else if (montantRetrait > comptes(id) * 0.1) {
        printf("Votre plafond de retrait autorisé est de : %.2f CHF\n", comptes(id) * 0.1)
      }
    } while (montantRetrait % 10 != 0 || montantRetrait > comptes(id) * 0.1)

    if (deviseChoisie == "1") {
      var choixCoupures = ""
      if (montantRetrait >= 200) {
        do {
          choixCoupures = readLine("En 1) grosses coupures, 2) petites coupures > ")
        } while (choixCoupures != "1" && choixCoupures != "2")
      }

      val coupuresCHF = Array(500, 200, 100, 50, 20, 10)
      val coupuresEUR = Array(100, 50, 20, 10)

      var coupuresDisponibles = if (deviseChoisie == "1") coupuresCHF else coupuresEUR
      val montant = if (deviseChoisie == "2") (montantRetrait * 0.95) else montantRetrait

      var resteMontant = montant.toInt

      if (montantRetrait >= 200 && choixCoupures == "1") {
        coupuresDisponibles = coupuresDisponibles.filter(_ >= 200)
      } else {
        coupuresDisponibles = coupuresDisponibles.filter(_ >= 10)
      }

      for (coupure <- coupuresDisponibles) {
        if (resteMontant >= coupure) {
          val nbCoupures = resteMontant / coupure
          resteMontant %= coupure
          println("Vous pouvez obtenir au maximum " + nbCoupures + " billet(s) de " + coupure + " " + (if (deviseChoisie == "1") "CHF" else "EUR"))
          var saisi = ""
          do {
            saisi = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          } while (saisi != "o" && (saisi.toInt > nbCoupures || saisi.toInt < 0))
          if (saisi == "o") {
            if (resteMontant == 0) {
              comptes(id) -= montant
              printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
              return
            }
          } else {
            resteMontant += saisi.toInt * coupure
          }
        }
      }
    } else {
      val coupuresEUR = Array(100, 50, 20, 10)
      val montant = montantRetrait
      var resteMontant = montant.toInt
      var coupuresDisponibles = coupuresEUR.filter(_ <= resteMontant)
      for (coupure <- coupuresDisponibles) {
        if (resteMontant >= coupure) {
          val nbCoupures = resteMontant / coupure
          resteMontant %= coupure
          println("Vous pouvez obtenir au maximum "+nbCoupures+" billet(s) de "+coupure+" EUR")
          var input = ""
          do {
            input = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          } while (input != "o" && (input.toInt > nbCoupures || input.toInt < 0))
          if (input == "o") {
            if (resteMontant == 0) {
              val montantCHF = montant / 0.95
              comptes(id) -= montantCHF
              printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
              return
            }
          } else {
            resteMontant += input.toInt * coupure
          }
        }
      }
    }
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {
    var nouveauCodePin = ""
    do {
      nouveauCodePin = readLine("Saisissez votre nouveau code PIN (au moins 8 caractères) > ")
      if (nouveauCodePin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      }
    } while (nouveauCodePin.length < 8)

    codespin(id) = nouveauCodePin
    println("Votre code PIN a été mis à jour.")
  }

  def main(args: Array[String]): Unit = {
    val nbClients = 101
    var comptes = Array.fill(nbClients)(1200.0)
    var codesPin = Array.fill(nbClients)("INTRO1234")
    println("Saisissez votre code identifiant >")
    var identifiant = readInt()

    if (identifiant < 0 || identifiant > 100) {
      println("Cet identifiant n’est pas valable.")
      sys.exit(0)
    }

    var codePinValide = false

    println("Saisissez votre code pin >")
    val saisiCodePIN = readLine()

    if (saisiCodePIN != codesPin(identifiant)) {
      println("Code pin erroné.")
      sys.exit(0)
    } else {
      println("Code PIN bon.")
      var choix = ""
      while (choix != "5") {
        println("\nChoisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du solde\n4) Changement du code pin\n5) Terminer")

        choix = readLine("Votre choix : ")

        if (choix == "1") {
          depot(identifiant, comptes)
        } else if (choix == "2") {
          retrait(identifiant, comptes)
        } else if (choix == "3") {
          println("Le montant disponible sur votre compte est de : " + comptes(identifiant) + " CHF")
        } else if (choix == "4") {
          changepin(identifiant, codesPin)
        } else if (choix != "5") {
          println("Choix invalide.")
        }
      }
      println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
    }
  }
}