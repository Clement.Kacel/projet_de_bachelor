//Assignment: Tea Kashahu_996427_assignsubmission_file

import io.StdIn._
import math._

object Main {
def main(args: Array[String]): Unit = {
  val nbClients = 100
  val comptes = Array.fill(nbClients)(1200.0)
  val codespin = Array.fill(nbClients)("INTRO1234")
  val hello = 0
  var world = 1
  while (true) {
    println("Saisissez votre code identifiant >")
    val id = readInt()

    if (id > nbClients) {
      println("Cet identifiant n’est pas valable.")
    } else {
      if (authenticateClient(id, codespin)) {
        var userChoice = 0
        while (userChoice != 5) {
          println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer")
          userChoice = readInt()
          userChoice match {
            case 1 => depot(id, comptes)
            case 2 => retrait(id, comptes)
            case 3 => println("Le montant disponible sur votre compte est de : " + comptes(id) + " CHF")
            case 4 => changepin(id, codespin)
            case 5 => println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            case _ => println("Choix invalide")
          }
        }
      }
    }
  }
}

  def authenticateClient(id: Int, codespin: Array[String]): Boolean = {
    var pinTries = 0
    while (pinTries < 3) {
      println("Saisissez votre code pin >")
      val enteredPin = readLine()
      if (enteredPin == codespin(id)) {
        return true
      } else {
        pinTries += 1
        println("Code pin erroné, il vous reste "+ (3 - pinTries) +" tentative(s)")
      }
    }
    println("Trop d’erreurs, abandon de l’identification")
    false
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    val currencyChoice = readInt()
    println("Indiquez le montant du dépôt >")
    var depositAmount = readDouble()
    while (depositAmount % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      depositAmount = readDouble()
    }
    if (currencyChoice == 2) {
      depositAmount *= 0.95
    }
    comptes(id) += depositAmount
    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
    val currencyChoice = readInt()
    println("Indiquez le montant du retrait >")
    var withdrawalAmount = readDouble()
    while (withdrawalAmount % 10 != 0 || withdrawalAmount > (comptes(id) * 0.1)) {
      if (withdrawalAmount % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      } else {
        println("Votre plafond de retrait autorisé est de : " + comptes(id) * 0.1 + " CHF")
      }
      withdrawalAmount = readDouble()
    }

    if (currencyChoice == 1) {
      distributeCHF(withdrawalAmount)
    } else if (currencyChoice == 2) {
      distributeEUR(withdrawalAmount)
      withdrawalAmount *= 0.95
    }

    comptes(id) -= withdrawalAmount
    println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
  }

  def distributeCHF(amount: Double): Unit = {
    var remainingAmount = amount
    val bigNotes = List(500, 200)
    val smallNotes = List(100, 50, 20, 10)
    val notes = if (remainingAmount >= 200) {
      var choice = 0
      while (choice != 1 && choice != 2) {
        println("Choisissez le type de coupures : 1) Grosses coupures, 2) Petites coupures >")
        choice = readInt()
      }
      if (choice == 1) bigNotes else smallNotes
    } else smallNotes

    distributeAmount(remainingAmount, notes, "CHF")
  }

  def distributeEUR(amount: Double): Unit = {
    val notes = List(100, 50, 20, 10)
    distributeAmount(amount, notes, "EUR")
  }

  def distributeAmount(amount: Double, notes: List[Int], currency: String): Unit = {
    var remainingAmount = amount
    for (note <- notes if note != 10 && remainingAmount >= note) {
      var maxCount = (remainingAmount / note).toInt
      var countSelected = false
      while (!countSelected && maxCount > 0) {
        println("Il reste " + remainingAmount + " " + currency + " à distribuer")
        println("Vous pouvez obtenir au maximum " + maxCount + " billet(s) de " + note + " " + currency)
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        val input = readLine()

        input match {
          case "o" =>
            remainingAmount -= maxCount * note
            countSelected = true
          case _ =>
            val newCount = input.toInt
            if (newCount >= 0 && newCount <= maxCount) {
              remainingAmount -= newCount * note
              countSelected = true
            } else {
              println("Entrée invalide. Veuillez réessayer.")
            }
        }
      }
    }


    if (remainingAmount > 0) {
      val count10 = (remainingAmount / 10).toInt
      remainingAmount -= count10 * 10
      println("Vous recevez " + count10 + " billet(s) de 10 " + currency)
    }

    println("Veuillez retirer la somme demandée.")
  }


  def changepin(id: Int, codespin: Array[String]): Unit = {
    var newPin = ""
    do {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      newPin = readLine()
      if (newPin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
    } while (newPin.length < 8)
    codespin(id) = newPin
    println("Votre code pin a été modifié avec succès.")
  }
}