//Assignment: Loris Thomas_996466_assignsubmission_file

import org.w3c.dom.css.Counter

import scala.io.StdIn._
object Main {
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    val comptes = Array.fill(nbclients)(1200.0)
    var actionChoice = 0
    val codespin = Array.fill(nbclients)("INTRO1234")
    var infinite = true
    var id = 0

    while (infinite) {
      var identification = false
        while (!identification) {
          id = readLine("Saississez votre code identifiant >").toInt
          if(id>=nbclients) {println("Cet identifiant n'est pas valable.") ; infinite=false}
          else {
            var pinInput = ""
            var tryNb = 2
            var pin = true
            while (pin) {
              pinInput = readLine("Saisissez votre code PIN > ")
              if ((pinInput != codespin(id)) && (tryNb > 0)) {
                tryNb -= 1
                println("Code pin erroné, il vous reste " + (tryNb + 1) + " tentatives > ")
              } else if (pinInput != codespin(id) && tryNb == 0) {
                println("Trop d'erreurs, abandon de l'identification")
                pin = false
              } else if (pinInput == codespin(id)) {
                identification = true
                pin = false
              }
            }
          }
        }

      //choix d'action
      while (actionChoice == 0 && identification) {
        println("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer")
        actionChoice = readLine("Votre choix : ").toInt

        //Dépot
        if (actionChoice == 1) {
          depot(id, comptes)
          actionChoice = 0

          //retrait
        } else if (actionChoice == 2) {
          retrait(id, comptes)
          actionChoice = 0

          //Consultation du compte
        } else if (actionChoice == 3) {
          printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
          actionChoice = 0

          //changement de code pin
        } else if (actionChoice == 4) {
          changepin(id, codespin)
          actionChoice = 0
        }
        //terminer
        else if (actionChoice == 5) {
          println("Fin des opérations, n'oubliez pas de récupérer votre carte")
          identification=false
          actionChoice = 0
        }
      }
    }
  }

  def depot(id : Int, comptes : Array[Double]): Unit = {
    var depositCurrency = 0
    var depositAmmount = 1

    //devise dépot
    while (depositCurrency != 1 && depositCurrency != 2) {
      depositCurrency = readLine("Indiquez la devise du dépot : 1) CHF ; 2) EUR > ").toInt
    }

    //choix montant dépot
    while (depositAmmount % 10 != 0 || depositAmmount < 0) {
      depositAmmount = readLine("Indiquez le montant du dépôt > ").toInt
      if (depositAmmount % 10 != 0 || depositAmmount < 0) print("Le montant doit être un multiple de 10. ")
    }

    //dépot en CHF
    if (depositCurrency == 1) {
      comptes(id) += depositAmmount
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }

    //dépot en EUR
    else if (depositCurrency == 2) {
      comptes(id) += (depositAmmount * 0.95)
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }
  }

  def retrait(id : Int, comptes : Array[Double]): Unit = {
    var withdrawalCurrency = 0
    var currentCurrency = ""
    var withdrawalAmmount = 1
    val withdrawalLimit = comptes(id) / 10
    var withdrawalSize = 0
    var withdrawalRest = 0
    var done = false
    val coupures = Array(500, 200, 100, 50, 20, 10)
    val nbCoupures = Array(0, 0, 0, 0, 0, 0)

    //choix de la devise
    while (withdrawalCurrency != 1 && withdrawalCurrency != 2) {
      withdrawalCurrency = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
    }
    if (withdrawalCurrency == 1) currentCurrency=" CHF" else if (withdrawalCurrency==2)currentCurrency=" EUR"

    //choix du montant du retrait
    while ((withdrawalAmmount % 10 != 0 || withdrawalAmmount > withdrawalLimit) || withdrawalAmmount < 0) {
      withdrawalAmmount = readLine("Indiquez le montant du retrait > ").toInt
      if (withdrawalAmmount % 10 != 0 || withdrawalAmmount < 0) print("Le montant doit être un multiple de 10. ")
      if (withdrawalAmmount > withdrawalLimit) println("Votre plafond de retrait est de : " + withdrawalLimit)
    }
    withdrawalRest = withdrawalAmmount

    //retrait en CHF
    if (withdrawalCurrency == 1) {
      if (withdrawalAmmount >= 200) {
        while (withdrawalSize != 1 && withdrawalSize != 2) {
          withdrawalSize = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
        }
      } else withdrawalSize = 2
    } else if(withdrawalCurrency==2)withdrawalSize=1

    for (c <- 0 to 5) {
      done=false
      if (withdrawalSize==2 && coupures(c)>100) done=true
      while (!done && withdrawalRest / coupures(c) != 0 && withdrawalRest != 0) {
        if (c!=5) {
          println("il reste " + withdrawalRest + currentCurrency + " à distribuer")
          println("Vous pouvez obtenir au maximum " + (withdrawalRest / coupures(c)) + " billet(s) de " + coupures(c) + currentCurrency)
          println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
        } else if (c==5) {
          println("il reste " + withdrawalRest + currentCurrency + " à distribuer")
          println("Vous pouvez obtenir " + (withdrawalRest / 10) + " billet(s) de 10" + currentCurrency)
          println("Tapez o pour ok > ")
        }
        val input = readLine()
        if (input == "o") {
          nbCoupures(c) = withdrawalRest / coupures(c)
          done = true
        } else if (input.toInt < (withdrawalRest / coupures(c))) {
          nbCoupures(c) = input.toInt
          done = true
        }
        withdrawalRest = withdrawalRest - nbCoupures(c) * coupures(c)
      }
    }
    println("Veuillez retirer la sommme demandée :")
    for (c <- 0 to 5) {
      if (!(nbCoupures(c) == 0)) println(nbCoupures(c) + " billet(s) de "+ coupures(c) + currentCurrency)
    }

    if (withdrawalCurrency==1) {
      comptes(id) -= withdrawalAmmount
    } else if (withdrawalCurrency==2) comptes(id) -= (withdrawalAmmount * 0.95)
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
  }

  def changepin(id : Int, codespin : Array[String]): Unit = {
    var changeDone = false
    while (!changeDone) {
      var newPin = readLine("Saississez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      if (newPin.length < 8) println("Votre code pin ne contient pas au moins 8 caractères")
      else {
        codespin(id) = newPin
        changeDone = true
      }
    }
  }
}