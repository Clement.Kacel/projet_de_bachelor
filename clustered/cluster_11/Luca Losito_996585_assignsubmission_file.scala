//Assignment: Luca Losito_996585_assignsubmission_file

import scala.io.StdIn // pour les readLine
import scala.util.Try // pour la methode tu try 
import scala.collection.mutable.ArrayBuffer // pour les calculs de vecteurs  
object Main {
def main(args: Array[String]): Unit = { // réinitialiser la page ou scroller dans la console si le programme freeze 
val nbclients=100  
val comptes=Array.fill(nbclients){1200.0} //L'argent du compte
val codespin=Array.fill(nbclients){"INTRO1234"}       
var choix= ""   
var choixBillets=""//pour le nmbres de billets à donner    
var nomDevise="" // pour les Euros ou Francs   
var tentatives=3 // du code
var repet= false 
var id=0

  def depot(id : Int, comptes : Array[Double]) : Unit= {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")  
    var devise=StdIn.readLine()  
      while (devise!="2" && devise!="1") {   // verifie que la devise soit en CHF ou en EUR
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        devise=StdIn.readLine()
      }
    println("Indiquez le montant du dépôt >")
    var montant=StdIn.readInt()
      while (montant % 10 != 0) { // Verifie que le montant est un multiple de 10
        println("Le montant doit être un multiple de 10")
        montant=StdIn.readInt()   
      }  
    if (devise=="1") { // calcul du compte en chf
      comptes(id)=comptes(id)+montant
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id))   
    } else {  //calcul du compte en eur
      comptes(id)=comptes(id)+montant*0.95
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id)) 
    } 
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit= {
    var recuBillets=ArrayBuffer(0) //vecteurs pour le recu
    var recuNbr=ArrayBuffer(0) //vecteurs pour le recu
    var coup500=0 // pour la coupure de 500   
    var coup200=0 // pour la coupure de 200   
    println("Indiquez la devise :1 CHF, 2 : EUR >")  
    var deviseRetrait=StdIn.readLine()  
      while (deviseRetrait!="2" && deviseRetrait!="1") { // verifie que la devise soit en CHF ou en EUR
        println("Indiquez la devise :1 CHF, 2 : EUR >")
        deviseRetrait=StdIn.readLine()
      }
    println("Indiquez le montant du retrait >")
    var montant=StdIn.readInt()
      while ((montant % 10 != 0)||(deviseRetrait=="1" && comptes(id)/10<montant)||(deviseRetrait=="2" && comptes(id)/9.5<montant)) { // Verifie que le montant est un multiple de 10 ou plus grand que 10% du montant disponible sur le compte
        if (montant % 10 != 0) {
          println("Le montant doit être un multiple de 10")
          montant=StdIn.readInt() 
        }
        if (deviseRetrait=="1" && comptes(id)/10<montant) {
          printf("Votre plafond de retrait autorisé est de : %.2f \n",comptes(id)/10)
          montant=StdIn.readInt()
        } else if (deviseRetrait=="2" && comptes(id)/9.5<montant) {
          printf("Votre plafond de retrait autorisé est de : %.2f \n",comptes(id)/9.5)
          montant=StdIn.readInt()
        }  
      }
      if (deviseRetrait=="1") { //pour les CHF + calcul du compte
        nomDevise=" CHF "
        comptes(id)=comptes(id)-montant
          if (montant>=200) { // pour les grandes coupures
            println("En 1) grosses coupures, 2) petites coupures >")
            var coupure=StdIn.readLine()
              while (coupure!="1" && coupure!="2") {
                println("En 1) grosses coupures, 2) petites coupures >")
                coupure=StdIn.readLine() 
              } 
              if (coupure=="1") { // pour les grandes coupures
                coup500=500
                coup200=200
              }
          } 
      } else { // pour les EUR + calcul du compte
        nomDevise=" EUR " 
        comptes(id)=comptes(id)-montant*0.95
      }
      if (montant>=20) { // pour eviter le cas ou on retire 10 CHF
        println("Il reste "+ montant + nomDevise+"à distribuer")
      }
    val nbrFrancs=Array(coup500,coup200,100,50,20,10) //vecteur des coupures
      for (r <- 0 to 5) { // Pour chaque coupure
        if (nbrFrancs(r)>0) { //evite les grandes coupure s'il faut
          var billets=(montant/nbrFrancs(r)).toInt //calcul du nombres de billets disponibles
            if (billets>0) { // evite de distribuer 0 billets
              if (r!=5){ 
                println("Vous pouvez obtenir au maximum "+billets+" billet(s) de "+nbrFrancs(r)+ nomDevise)
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                choixBillets=StdIn.readLine()
                var test= Try(choixBillets.toInt).toOption // methode du Try qui verifie si la conversion en Int exste et l'applique dans ce cas
                  while (!(choixBillets=="o" || test.exists(_ < billets))) { // S'assure que l'utilisateur tape bien o ou un nombre inférieur au nombre de billets disponibles
                    println("Vous pouvez obtenir au maximum "+billets+" billet(s) de "+nbrFrancs(r)+nomDevise)
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    choixBillets=StdIn.readLine()
                    test= Try(choixBillets.toInt).toOption
                  }
              } else { // Quand il ne reste que les coupures de 10
                choixBillets="o"
              }  
            if (choixBillets=="o"){
              montant=montant-billets*nbrFrancs(r) //modifie le montant restant
              recuBillets+=billets //concatenation du vecteur des nmbres de billets
              recuNbr+=nbrFrancs(r) //concatenation du vecteur des coupures
            } else {  
              montant=montant-nbrFrancs(r)*choixBillets.toInt // calcul du montant restant 
                if (choixBillets.toInt>0) {
                  recuBillets+=choixBillets.toInt //concatenation du vecteur des nmbres de billets
                  recuNbr+=nbrFrancs(r) //concatenation du vecteur des coupures
                }  
            }
            }  
        }
      }
    println("Veuillez retirer la somme demandée :")
      for (i<- 1 to recuNbr.length-1) { //boucle pour le recu des billets
        println(recuBillets(i)+" billet(s) de "+recuNbr(i)+nomDevise)
      }
    println("Votre retrait a été pris en compte, le nouveau montant")
    printf("disponible sur votre compte est de : %.2f \n",comptes(id))
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    codespin(id)=StdIn.readLine()
      while (codespin(id).length<8) { // boucle pour eviter de saisir un code pin trop court)
        println("Votre code pin ne contient pas au moins 8 caractères")
        codespin(id)=StdIn.readLine()
      }    
  }




  
//debut du code    
while (true){ // pour que le code continue toujours
  tentatives=3
    while (!repet) { // pour pouvoir repeter l'identifiant si on s'est trompe de code
      println("Saisissez votre code identifiant >") //debut du code
      id= StdIn.readInt()
        if ((id>=100)){
          println("Cet identifiant n’est pas valable.")
          sys.exit(0)
        }  
      println("Saisissez votre code pin >") 
      var codetest = StdIn.readLine()
        while (codespin(id) != codetest && !repet ) {
          tentatives += -1
            if (tentatives != 0) {
              println("Code pin erroné, il vous reste "+ tentatives +" tentatives >")
              codetest = StdIn.readLine()
            } else {
              println("Trop d’erreurs, abandon de l’identification")
              repet= true
            } 
        }   
    
      if (codespin(id) == codetest) { // pour sortir de la boucle de repetition de code
        repet= true
      } else { // pour rester dans la boucl de repetition de code
        repet= false
      }
    }
      while (choix != "5") {
        println("Choisissez votre opération :")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
        println("Votre choix : ")
        choix= StdIn.readLine() 
          if (choix == "1") { // Le dépôt
            depot(id, comptes)
          } else if (choix == "2") { // Le retrait
            retrait(id, comptes)
          } else if (choix=="3") { // La consultation
            printf("Le montant disponible sur votre compte est de : %.2f \n",comptes(id))
          } else if (choix=="4"){ // le changement du code pin
            changepin(id,codespin)  
          }
      }        
println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
repet= false //pour rentrer dans la boucle de test de code
choix="" // pour rentrer dans la boucle choix 
}        
}
}

