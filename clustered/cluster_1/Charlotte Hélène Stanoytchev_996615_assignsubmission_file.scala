//Assignment: Charlotte Hélène Stanoytchev_996615_assignsubmission_file

//import des libs pour le programme
import scala.io.StdIn.readInt
import scala.io.StdIn.readLine
import scala.math

object Main {
  //variables de retrait
  var billetde500:Int = 0
  var billetde200:Int = 0
  var billetde100:Int = 0
  var billetde50:Int = 0
  var billetde20:Int = 0
  var billetde10:Int = 0

  var devise:Int = 0

  def main(args: Array[String]): Unit = {
    var codeIdentifiant = 5
    var choix_utilisateur:Int = 5
    var compteur_codePin:Int = 0
    var nbclients=100
    var codespin= Array.fill(nbclients)("INTRO1234")
    var comptes = Array.fill(nbclients)(1200.00)
    var essais:Int = 3

    //DEBUT DU PROGRAMME  
    while (codeIdentifiant>=0 && codeIdentifiant<nbclients)
    {
      if(choix_utilisateur==5){
        codeIdentifiant = readLine("Saisissez votre code identifiant >").toInt
      }
      if(codeIdentifiant>=0 && codeIdentifiant<nbclients)
      {
        var entrer_pin:String = ""
         essais=3
        if(compteur_codePin==0)  //si c'est la première fois qu'on accède au programme, alors on doit demander le code PIN
        {
          entrer_pin = readLine("Saisissez votre code PIN > ")  //on demande le code PIN
          while(entrer_pin!=codespin(codeIdentifiant))
          {  //tant qu'il n'est pas bon, on affiche le nombre de tentatives restantes en enlevant chaque fois 1 au nombre de tentatives restantes + si on est a 0, on sort du programme
            if(essais>1)
            {
              println("Code pin erroné, il vous reste " + (essais-1) + " tentatives ")
              essais = essais - 1   //mauvais pin, on perd 1 essais
              entrer_pin = readLine("Saisissez votre code PIN > ")
            }
            else
            {
              //si le client s'est tromp3 3x, on passe à un autre client
              println("Trop d’erreurs, abandon de l’identification")
              entrer_pin=codespin(codeIdentifiant)
              choix_utilisateur=5
             essais= -100
            }
          }
          //Ici, le code pin est forcément le bon
          if(essais>=0) compteur_codePin=10  //on le met à 10 pour ne plus rentrer dans le if(=0)
        }

        if(compteur_codePin!=0)  //on met pas de else mais if(compteur!=0) pour que si on vient de réussir le code on puisse quand même rentrer ici
        {
          //OPTIONS
          println("Choisissez votre opération :")
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          println("Votre choix : ")
          choix_utilisateur = readInt()  //lecture de l'entrée utilisateur


            //Choix autre que 1, 2 ou 3 ou 4
            while(choix_utilisateur!=1 && choix_utilisateur!=2 && choix_utilisateur!=3 && choix_utilisateur!=4 && choix_utilisateur!=5){
              // si on a fait un bon choix 
              println("Choisissez votre opération :")
              println("1) Dépôt")
              println("2) Retrait")
              println("3) Consultation du compte")
              println("4) Changement du code pin")
              println("5) Terminer")
              println("Votre choix : ")
              choix_utilisateur = readInt()  //lecture de l'entrée
            }
          if(choix_utilisateur==1)
          {
            //choix de dépôt
            depot(codeIdentifiant,comptes)
          }
          else if(choix_utilisateur==2)
          {
            //OPERATION DE RETRAIT
            retrait(codeIdentifiant,comptes)
          }
          else if(choix_utilisateur==3)
          {
            //OPERATION DE CONSULTATION DU COMPTE
            printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(codeIdentifiant))
          }
          else if(choix_utilisateur==4)
          {
            changepin(codeIdentifiant,codespin)
          }
          else  //cela fait référence à l'option 5 
          {
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            compteur_codePin=0
          }
        }
      }
      else
      {
        println("Cet identifiant n’est pas valable. ")
      }
    }
  }

  //DECLARATION ET IMPLEMENTATION DES METHODES RETRAIT CHANGER DE PIN ET DEPOSER

  //En-TETE DE LA METHODE POUR RETIER DE L'ARGENT
  def retrait(id : Int, comptes : Array[Double]) : Unit = 
  {
    //variables de retrait
    var devise_chf_ou_eur:String = ""
    var valeur_int:Int = 0
    devise=0
    //tant que on a pas la bonne devise, on répète cette boucle
    while(devise!=1 && devise!=2){
      print("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
      devise = readInt()
    }

    //plus simple pour l'affichage après, on aura pas à le faire deux fois (une fois pour CHF et une fois pour euro)
    if(devise==1){
      devise_chf_ou_eur = "CHF"
    }
    else{
      devise_chf_ou_eur = "EUR"
    }

    var retraitM:Int = 0
    var modulo10:Boolean = (retraitM%10==0) //condition que le retrait soit un multiple de 10
    var plafond_10pourcent:Boolean = (retraitM<=(comptes(id)/10.0))
    while(!modulo10 || retraitM<=0 || !plafond_10pourcent){//si la coniditon n'est pas respectée
      print("Indiquez le montant du retrait >")
      retraitM = readInt()
      //Recalcul des conditions
      plafond_10pourcent = (retraitM<=(comptes(id)/10.0))  
      modulo10 = (retraitM%10==0)
      if(!modulo10){
        //Si c'est pas un multiple de 10
        println("Le montant doit être un multiple de 10.")
      }
      if(!plafond_10pourcent){
        //Si c'est au dessus du plafond
        printf("Votre plafond de retrait autorisé est de : %.2f CHF \n", comptes(id)/10.0)
      }

    }//fin du while

    //Ici, le montant est forcément le bon

    if(devise==1){
      comptes(id) = comptes(id) - retraitM  //solde après retrait
    }
    else{
      comptes(id) = comptes(id) - (0.95*retraitM)//solde après retrait avec conversion
    }

    var coupure:Int = 0

    var maximum:Int = 0

    if(devise==1 && retraitM>=200)
    {  //si on est en CHF et qu'on a retirer plus de 200 CHF
      while(coupure!=1 && coupure!=2)
      {  //tant que le resultat n'est pas 1 ou 2  
        print("En 1) grosses coupures, 2) petites coupures >")
        coupure = readInt()
      }
      //ici la coupure est forcément 1 ou 2
    }
    else
    {
      coupure=2  //car pour euro la coupure max est 100 et pour petite coupure aussi
    }

    if(coupure==2)
    {
      //EURO ou petites coupures
      if(retraitM>=100)
      {
        //Affichage du nombre d'argent restant a tirer
        println("Il reste " + retraitM + " CHF à distribuer")
        //Affichage du maximum a avoir
        println("Vous pouvez obtenir au maximum " + retraitM/100 + " billet(s) de 100 " + devise_chf_ou_eur)
        maximum = retraitM/100  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (100*maximum)
          billetde100 = maximum
        }
        else
        {  //si on a une autre valeur que o ça signifie que l'utilisateur a tapé une autre valeur mais numérique
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            //si elle est sup au max ou negative, on redemande
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {  //on convertit en numerique
              valeur_int = tappez_o.toInt
            }
          }
          //ici, un nombre juste a été tappé, soit "o" soit un nombre positif et inférieur au maximum, alors on peut retirer et garder en mémoire le nombre de billets à retirer
          billetde100 = valeur_int
          retraitM = retraitM - (100*billetde100)

        }
      }
      //PAREIL POUR 50 ET AINSI DE SUITE
      if(retraitM>=50)
      {
        println("Il reste " + retraitM + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/50 + " billet(s) de 50 " + devise_chf_ou_eur)
        maximum = retraitM/50  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (50*maximum)
          billetde50 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde50 = valeur_int
          retraitM = retraitM - (50*billetde50)

        }
      }
      if(retraitM>=20)
      {
        println("Il reste " + retraitM + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/20 + " billet(s) de 20 " + devise_chf_ou_eur)
        maximum = retraitM/20  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (20*maximum)
          billetde20 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde20 = valeur_int
          retraitM = retraitM - (20*billetde20)

        }
      }
      if(retraitM>=10)
      {
        println("Il reste " + retraitM + " CHF à distribuer")
        maximum = retraitM/10
        retraitM = retraitM - (10*maximum)
        billetde10 = maximum
      }

    }
    if(coupure==1)
    {
      if(retraitM>=500)
      {
        println("Il reste " + retraitM + " " + devise_chf_ou_eur + " à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/500 + " billet(s) de 500 " + devise_chf_ou_eur)
        maximum = retraitM/500  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (500*maximum)
          billetde500 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum
              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde500 = valeur_int
          retraitM = retraitM - (500*billetde500)

        }
      }
      if(retraitM>=200)
      {
        println("Il reste " + retraitM + " " + devise_chf_ou_eur + " à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/200 + " billet(s) de 200 " + devise_chf_ou_eur)
        maximum = retraitM/200  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (200*maximum)
          billetde200 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde200 = valeur_int
          retraitM = retraitM - (200*billetde200)

        }
      }
      if(retraitM>=100)
      {
        println("Il reste " + retraitM + " " + devise_chf_ou_eur + " à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/100 + " billet(s) de 100 " + devise_chf_ou_eur)
        maximum = retraitM/100  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (100*maximum)
          billetde100 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde100 = valeur_int
          retraitM = retraitM - (100*billetde100)

        }
      }
      if(retraitM>=50)
      {
        println("Il reste " + retraitM + " " + devise_chf_ou_eur + " à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/50 + " billet(s) de 50 " + devise_chf_ou_eur)
        maximum = retraitM/50  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (50*maximum)
          billetde50 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde50 = valeur_int
          retraitM = retraitM - (50*billetde50)

        }
      }
      if(retraitM>=20)
      {
        println("Il reste " + retraitM + " " + devise_chf_ou_eur + " à distribuer")
        println("Vous pouvez obtenir au maximum " + retraitM/20 + " billet(s) de 20 " + devise_chf_ou_eur)
        maximum = retraitM/20  //division euclidienne

        var tappez_o:String = ""
        tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if(tappez_o=="o")
        {
          //on doit retirer le montant maximum
          retraitM = retraitM - (20*maximum)
          billetde20 = maximum
        }
        else
        {
          valeur_int = tappez_o.toInt
          while(valeur_int>maximum || valeur_int<0){
            tappez_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(tappez_o=="o")
            {
              //on doit retirer le montant maximum

              valeur_int=maximum
            }
            else
            {
              valeur_int = tappez_o.toInt
            }
          }
          billetde20 = valeur_int
          retraitM = retraitM - (20*billetde20)

        }
      }
      if(retraitM>=10)
      {
        println("Il reste " + retraitM + " " + devise_chf_ou_eur + " à distribuer")
        maximum = retraitM/10
        retraitM = retraitM - (10*maximum)
         billetde10 = maximum
      }
    }

    println("Veuillez retirer la somme demandée :")
    if(billetde500>0){
      println(billetde500 + " billet(s) de 500 " + devise_chf_ou_eur )
    }
    if(billetde200>0){
      println(billetde200 + " billet(s) de 200 " + devise_chf_ou_eur )
    }
    if(billetde100>0){
      println(billetde100 + " billet(s) de 100 " + devise_chf_ou_eur )
    }
    if(billetde50>0){
      println(billetde50 + " billet(s) de 50 " + devise_chf_ou_eur )
    }
    if(billetde20>0){
      println(billetde20 + " billet(s) de 20 " + devise_chf_ou_eur )
    }
    if(billetde10>0){
      println(billetde10 + " billet(s) de 10 " + devise_chf_ou_eur )
    }

    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n ", comptes(id))

    billetde500 = 0
    billetde200 = 0
    billetde100 = 0
    billetde50 = 0
    billetde20 = 0
    billetde10 = 0
  }

  //EN-TETE DE LA METHODE POUR CHANGER DE PIN
  def changepin(id : Int, codespin : Array[String]) : Unit = 
  {
    var nouveaucode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var tropcourt = nouveaucode.length<8
    while(tropcourt){
      nouveaucode = readLine("Votre code pin ne contient pas au moins 8 caractères, veuillez le saisir à nouveau >")
      tropcourt = nouveaucode.length<8
    }
    codespin(id) = nouveaucode  //bon code pin
  }

  //E-TETE DE LA METHODE POUR DEPSER DE L'ARGENT
  def depot(id : Int, comptes : Array[Double]):Unit = 
  {
    //MODE DE DEPOT
    devise=0
    //tant que c'est pas la bonne devise, on doit le ré-écrire
    while(devise!=1 && devise!=2){
      devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
    }

    var depotM:Int = 0
    var modulo10:Boolean = depotM%10==0  //condition que le depot soit un multiple de 10

    while(!modulo10 || depotM<=0)  //si la coniditon n'est pas respectée ou que le depot est négatif ou nul, on redemande
    {
      depotM = readLine("Indiquez le montant du dépôt >").toInt
      modulo10 = (depotM%10==0)  //on recalcule la condition
      if(!modulo10){
        println("Le montant doit être un multiple de 10")
      }
    }
    if(devise==1){
      comptes(id)+=depotM  //alors le nouveau solde sera juste le solde actuel + le montant du depot
    }
    else{
      comptes(id)+=(depotM*0.95)  //on doit convertir les euros en franc suisse donc on utilise le taux de conversion = 0.95
    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF\n", comptes(id))  //Affichage du nouveau solde banciare
  }
}