//Assignment: Alix Michalet_996489_assignsubmission_file

import math._
import io.StdIn._
object Main {



  //DECLARATION FONCTION DEPOT
  def depot(id : Int, comptes : Array[Double]) : Unit =
  {
    var devisedepot : String = " "
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    var choixmoney : Int = readInt()
    while(choixmoney!=1 && choixmoney!=2)
    {
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      choixmoney = readInt()
    }
    if (choixmoney == 1)
    {
      devisedepot = "CHF"
    }
    if (choixmoney == 2)
    {
      devisedepot = "EUR"
    }
    //Choix de la devise par le client
    println("Indiquez le montant du dépôt >")
    var valdépot : Double = readDouble()
    var ajout : Double = 0
    while (valdépot%10 != 0)
    {
      println("Le montant doit être un multiple de 10")
      valdépot = readDouble()
    }
    if (devisedepot == "EUR")
    {
      ajout = valdépot*0.95
    }
    if (devisedepot == "CHF")
    {
      ajout = valdépot
    }
    //Conversion du dépot en CHF si ce sont des euros

    comptes(id) += ajout
    //calcul du nouveau montant disponible sur le compte

    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ",comptes(id))
    println("CHF")
  }



  //DECLARATION FONCTION RETRAIT
  def retrait(id : Int, comptes : Array[Double]) : Unit =
  {
    println("Indiquez la devise :1) CHF, 2) : EUR >")
    var choixmoney2 : Int = readInt()
    var deviseretrait : String = " "
    
    while (choixmoney2 != 1 && choixmoney2 != 2)
    {
      println("Indiquez la devise :1) CHF, 2) : EUR >");
      choixmoney2 = readInt();
    }
    if (choixmoney2 == 1)
    {
      deviseretrait = "CHF"
    }
    if (choixmoney2 == 2)
    {
      deviseretrait = "EUR"
    }
    //Choix de la devise par le client pour le retrait

    println("Indiquez le montant du retrait >")
    var valretrait : Double = readDouble()
    var valretrait_tot : Double = valretrait
    while (valretrait%10 != 0 || valretrait > comptes(id)*0.1)
    {
      if (valretrait%10 != 0)
      {
        println("Le montant doit être un multiple de 10.")
      }
      if (valretrait > comptes(id)/10)
      {
        println("Votre plafond de retrait autorisé est de : "+ comptes(id)/10)
      }
      valretrait = readDouble()
      valretrait_tot = valretrait
    }
    if (choixmoney2 == 1)
    {
      var choixcoupure : Int = 3
      if (valretrait >= 200)
      {
        while (choixcoupure != 1 && choixcoupure != 2)
        {
          println("En 1) grosses coupures, 2) petites coupures >")
          choixcoupure = readInt()
        }
      }

      //Choix de la coupure en fonction du montant du retrait
      var i : Int = 0
      var nb500 : Int = 0
      var nb200 : Int = 0
      var nb100 : Int = 0
      var nb50 : Int = 0
      var nb20 : Int = 0
      var nb10 : Int = 0
      var choixnombre : String = ""

      if (valretrait >= 500)
      {
        i=valretrait.toInt/500
        choixnombre = i.toString +1
        while (choixnombre != "o" && choixnombre.toInt > i)
        {
          println("Il reste "+ valretrait+" CHF à distribuer"
                 +"\n" + "Vous pouvez obtenir au maximum "+ i +"billet(s) de 500 CHF"
                  +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixnombre = readLine()
        }
        if (choixnombre == "o")
        {
          nb500 = i
        }
        else
        {
           nb500 = choixnombre.toInt
        }
        choixnombre =""
        valretrait -= nb500*500
      }
      if (valretrait >= 200)
      {
         i=valretrait.toInt/200
         choixnombre = i.toString + 1
         while (choixnombre != "o" && choixnombre.toInt > i)
         {
           println("Il reste "+ valretrait+" CHF à distribuer"
            +"\n" + "Vous pouvez obtenir au maximum "+ i +"billet(s) de 200 CHF"
            +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           choixnombre = readLine()
         }
         if (choixnombre == "o")
         {
           nb200 = i
         }
         else
         {
           nb200 = choixnombre.toInt
         }
         choixnombre =""
         valretrait -= nb200*200
      }
      if (valretrait >= 100)
      {
         i=valretrait.toInt/100
         choixnombre = i.toString + 1
         while (choixnombre != "o" && choixnombre.toInt > i)
         {
           println("Il reste "+ valretrait+" CHF à distribuer"
            +"\n" + "Vous pouvez obtenir au maximum "+ i +"billet(s) de 100 CHF"
            +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           choixnombre = readLine()
         }
         if (choixnombre == "o")
         {
           nb100 = i
         }
         else
         {
           nb100 = choixnombre.toInt
         }
         choixnombre =""
         valretrait -= nb100*100
      }
      if (valretrait >= 50)
      {
         i=valretrait.toInt/50
         choixnombre = i.toString + 1
         while (choixnombre != "o" && choixnombre.toInt > i)
         {
           println("Il reste "+ valretrait+" CHF à distribuer"
            +"\n" + "Vous pouvez obtenir au maximum "+ i +"billet(s) de 50 CHF"
            +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           choixnombre = readLine()
         }
         if (choixnombre == "o")
         {
           nb50 = i
         }
         else
         {
           nb50 = choixnombre.toInt
         }
         choixnombre =""
         valretrait -= nb50*50
      }
      if (valretrait >= 20)
      {
         i=valretrait.toInt/20
         choixnombre = i.toString + 1
         while (choixnombre != "o" && choixnombre.toInt > i)
         {
           println("Il reste "+ valretrait+" CHF à distribuer"
            +"\n" + "Vous pouvez obtenir au maximum "+ i +"billet(s) de 20 CHF"
            +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           choixnombre = readLine()
         }
         if (choixnombre == "o")
         {
           nb20 = i
         }
         else
         {
           nb20 = choixnombre.toInt
         }
         choixnombre =""
         valretrait -= nb20*20
      }
      if (valretrait >= 10)
      {
         i=valretrait.toInt/10
        choixnombre = i.toString + 1
         while (choixnombre != "o")
         {
           println("Il reste "+ valretrait+" CHF à distribuer"
            +"\n" + "Vous pouvez obtenir au maximum "+ i +"billet(s) de 10 CHF"
            +"\n" + "Tapez o pour ok >")
           choixnombre = readLine()
         }
         nb10 = i
         choixnombre =""
         valretrait -= nb10*10
      }
      println("Veuillez retirer la somme demandée :")
      if (nb500 != 0)
      {
        println(nb500 + "billet(s) de 500 CHF")
      }
      if (nb200 != 0)
      {
        println(nb200 + "billet(s) de 200 CHF")
      }
      if (nb100 != 0)
      {
        println(nb100 + "billet(s) de 100 CHF")
      }
      if (nb50 != 0)
      {
        println(nb50 + "billet(s) de 50 CHF")
      }
      if (nb20 != 0)
      {
        println(nb20 + "billet(s) de 20 CHF")
      }
      if (nb10 != 0)
      {
        println(nb10 + "billet(s) de 10 CHF")
      }
      comptes(id)-=valretrait_tot
    }

    if (deviseretrait == "EUR")
    {
      var j : Int = 0
      var nbe100 : Int = 0
      var nbe50 : Int = 0
      var nbe20 : Int = 0
      var nbe10 : Int = 0
      var choixnombre2 : String = ""
      var montantdispoeuro : Double = comptes(id)*0.95

      if (valretrait >= 100)
      {
        j=valretrait.toInt/100
        choixnombre2 = j.toString + 1
        while (choixnombre2 != "o" && choixnombre2.toInt > j)
        {
          println("Il reste "+ valretrait+" EUR à distribuer"
                 +"\n" + "Vous pouvez obtenir au maximum "+ j +"billet(s) de 100 EUR"
                  +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixnombre2 = readLine()
        }
        if (choixnombre2 == "o")
        {
          nbe100 = j
        }
        else
        {
          nbe100 = choixnombre2.toInt
        }
        choixnombre2 =""
        valretrait -= nbe100*100
      }
      if (valretrait >= 50)
      {
        j=valretrait.toInt/50
        choixnombre2 = j.toString + 1
        while (choixnombre2 != "o" && choixnombre2.toInt > j)
        {
          println("Il reste "+ valretrait+" EUR à distribuer"
                 +"\n" + "Vous pouvez obtenir au maximum "+ j +"billet(s) de 50 EUR"
                  +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixnombre2 = readLine()
        }
        if (choixnombre2 == "o")
        {
          nbe50 = j
        }
        else
        {
          nbe50 = choixnombre2.toInt
        }
        choixnombre2 =""
        valretrait -= nbe50*50
      }
      if (valretrait >= 20)
      {
        j=valretrait.toInt/20
        choixnombre2 = j.toString + 1
        while (choixnombre2 != "o" && choixnombre2.toInt > j)
        {
          println("Il reste "+ valretrait+" EUR à distribuer"
                 +"\n" + "Vous pouvez obtenir au maximum "+ j +"billet(s) de 20 EUR"
                  +"\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixnombre2 = readLine()
        }
        if (choixnombre2 == "o")
        {
          nbe20 = j
        }
        else
        {
          nbe20 = choixnombre2.toInt
        }
        choixnombre2 =""
        valretrait -= nbe20*20
      }
      if (valretrait >= 10)
      {
         j=valretrait.toInt/10
        choixnombre2 = j.toString + 1
         while (choixnombre2 != "o")
         {
           println("Il reste "+ valretrait+" EUR à distribuer"
            +"\n" + "Vous pouvez obtenir au maximum "+ j +"billet(s) de 10 EUR"
            +"\n" + "Tapez o pour ok >")
           choixnombre2 = readLine()
         }
         nbe10 = j
         choixnombre2 =""
         valretrait -= nbe10*10
      }
      println("Veuillez retirer la somme demandée :")
      if (nbe100 != 0)
      {
        println(nbe100 + "billet(s) de 100 EUR")
      }
      if (nbe50 != 0)
      {
        println(nbe50 + "billet(s) de 50 EUR")
      }
      if (nbe20 != 0)
      {
        println(nbe20 + "billet(s) de 20 EUR")
      }
      if (nbe10 != 0)
      {
        println(nbe10 + "billet(s) de 10 EUR")
      }
      montantdispoeuro -= valretrait_tot
      comptes(id) = montantdispoeuro*1.05
    }
    //j'ai plus la force de commenter à ce stade
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ",comptes(id))
    println("CHF")
  }



  //DECLARATION FONCTION CHANGEMENT DE PIN
  def changepin(id : Int, codespin : Array[String]) : Unit =
  {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var nouveauPin = readLine()
    while (nouveauPin.length < 8)
    {
      println("Votre code pin ne contient pas au moins 8 caractères")
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      nouveauPin = readLine()
    }
    codespin(id) = nouveauPin
  }


  
  //FONCTION MAIN
  def main(args: Array[String]): Unit = 
  {
    var choix = 10  
    var pinIN : String = " "
    var devise : String = " "
    var pinOUT : String = "INTRO1234"
    //La valeur du code pin du client
    
    var nbclients : Int = 100
    //variable pour le nombre de clients

    var findesopérations : Boolean = true
    //Bool pour savoir quand c'est un nouveau client ou pas
    
    val codespin = new Array[String](nbclients)
    for (i <- 0 to (nbclients - 1))
    {
      codespin(i) = "INTRO1234"
    }
    //déclaration des Pin de tous les comptes clients et initialisation à "INTRO1234"
    val comptes = new Array[Double](nbclients)
    for (i<-0 to (nbclients -1))
    {
      comptes(i) = 1200.0
    }
    //déclaration de tous les comptes et initialisation de ces derniers à 1200.0 CHF
    
    while(choix != 0)
    {
      //DEBUT DU PROGRAMME
      choix = 10

      
      println("Saisissez votre code identifiant >")
      var id = readInt()

      if (id > nbclients -1)
      {
        println("Cet identifiant n’est pas valable.")
        choix = 0
      }
      //arret du programme si mauvais identifiant

      if (choix != 0)
      {
        
        if(findesopérations)
        {
          println ("Saisissez votre code pin >")
          pinIN = readLine()
          var i : Int = 1
          //nb de tentatives

          while (i<3 && pinIN!=codespin(id))
          {
            println("Code pin erroné, il vous reste " + (3-i) + " tentatives >");
            i+=1;
            println ("Saisissez votre code pin >");
            pinIN = readLine();
          }

          if (pinIN!=codespin(id) && i==3)
          {
            println("Trop d’erreurs, abandon de l’identification")
            choix = 10
            findesopérations = true
          }  
          if (pinIN==codespin(id))
          {
            choix = 9
            findesopérations = false
          }
          //code pour demander le pin à un nouveau Client
        }
        
      }

      while(choix!=0 && choix!=5 && choix!=10)
      {
        println("Choisissez votre opération :"
          + "\n" + "\t" + "1) Dépôt"
          + "\n" + "\t" + "2) Retrait"
          + "\n" + "\t" + "3) Consultation du compte"
          + "\n" + "\t" + "4) Changement du code pin"
          + "\n" + "\t" + "5) Terminer"
          + "\n" + "Votre choix :" )
        //affichage des opérations disponibles
        choix = readInt()
        //Choix d'opération      


        
        if (choix==1)
        {
          depot(id,comptes)
        }


        
        if (choix == 2)
        {
          retrait(id,comptes)
        }
        

        
        if (choix == 3)
        {
          printf("Le montant disponible sur votre compte est de : %.2f ",comptes(id))
          println("CHF")
        }
        //Le code pour la consultation du montant disponible sur le compte


        if(choix == 4)
        {
          changepin(id,codespin)
        }


        
        if (choix == 5)
        {
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
          findesopérations = true
        }
      }
    }
  }
}
