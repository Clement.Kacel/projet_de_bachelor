//Assignment: Ali Nasser Eddine Raad_996588_assignsubmission_file

import scala.io.StdIn._
object Main {
  var nbclients=100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin =Array.fill(nbclients)("INTRO1234")
  
  def idpi(): Int={
    println("Quel est votre id ?")
    var nbclient=0
    var tentatives = 3
    var saisiepin="o"
     nbclient = readInt()
    if (nbclient>=100){
      println("Cet identifiant n’est pas valable.")
    }else{
      println("Quel est votre code pin ?")
    saisiepin=readLine()
      while (saisiepin != codespin(nbclient) && tentatives > 1) {
          tentatives -= 1
          println("Code pin erroné, il vous reste " + tentatives + " tentatives > ")
          saisiepin = readLine("Saisissez votre code pin >")
        }
      if (saisiepin != codespin(nbclient)) {
        println("Trop d’erreurs, abandon de l’identification")
        nbclient=idpi()
      }
      }
  return nbclient
  }
  def depot(id : Int, comptes : Array[Double]) : Unit ={

    
    var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt

              if (devise == 1) {
    var depotfranc = readLine("Indiquez le montant du dépôt >").toInt
                while (((depotfranc % 10) != 0)||(depotfranc<=0)) {
                  println("Le montant doit être un multiple de 10")
                  depotfranc = readLine("Indiquez le montant du dépôt>").toInt
                }
                comptes(id) = comptes(id ) + depotfranc
                println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de " + comptes(id) +" chf")

            }else{
              var depoteuro =readLine("Indiquez le montant du dépôt >").toInt

              while ((depoteuro % 10) != 0) {
              println("Le montant doit être un multiple de 10")
              depoteuro = readLine("Indiquez le montant du dépôt>").toInt
            }

                comptes(id) = comptes(id ) + (depoteuro * 0.95)
                println("votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de " +comptes(id) +" chf")
              }
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit ={
     
    var minretrait = 0.0
var deviser=0

                do {
                  deviser = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR >").toInt
                } while (deviser != 1 && deviser != 2)

                var retraitfranc = readLine("Indiquez le montant du retrait >").toInt
                if(deviser==1){
                  minretrait= (0.1 * comptes(id))-((comptes(id)*0.1)%10)
                }else{
                  minretrait= ((comptes(id)*1.05)*0.1)-(((comptes(id)*1.05*0.1))%10)
                }
                while (retraitfranc % 10 != 0) {
                  println("Le montant doit être un multiple de 10 ")
                   retraitfranc = readLine("").toInt
               }
                while((minretrait < retraitfranc )|| (retraitfranc%10!=0)){
                  if(deviser==1){
                  println("Votre plafond de retrait autorisé est de "+ minretrait +"chf")
                    if(retraitfranc%10!=0){
                       println("Le montant doit être un multiple de 10 ")
                    }
                  }
                  else{
                    println("Votre plafond de retrait autorisé est de "+ minretrait +" euros")
                    if(retraitfranc%10!=0){
                       println("Le montant doit être un multiple de 10 ")
                    }

                  }
                retraitfranc=readLine().toInt
                }
                 if(deviser==1){
               comptes(id)= comptes(id)-retraitfranc}
                else{
                  comptes(id)= comptes(id)-(retraitfranc*0.95)
                }
                 var cinqcent = 500
                var deuxcent = 200
                var cent = 100
                var cinquante = 50
                var vingt = 20
                var dix=10
                var coupuresf = 0
                var reste=retraitfranc
                 var dispo500 = (retraitfranc/cinqcent).toInt
                 var dispo200= (reste/deuxcent).toInt
                 var dispo100= (reste/cent).toInt
                 var dispo50= (reste/cinquante).toInt
                 var dispo20= (reste/vingt).toInt
                 var dispo10= (reste/dix).toInt
                 var coupures500=0
                 var coupures200=0
                 var coupures100=0
                 var coupures50=0
                 var coupures20=0
                 var coupures10=0
                 var avis="0"
                if (deviser == 1){



                  if(retraitfranc>=200){
                  do {
                   coupuresf = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
                }while((coupuresf!=1)&& (coupuresf!=2))

                 if(coupuresf==1){
                  if (dispo500>0){
                    println("il reste "+ reste+" chf à distribuer " )
                   println ("Vous pouvez obtenir au maximum "+dispo500 +  " billet(s) de 500 CHF")


                       do{ 
                         println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                        avis = readLine()
                        if (avis == "o"){
                          coupures500 = dispo500


                        }
                      else if(avis!="o" ){

                        coupures500 = avis.toInt
                        }
                       }while((dispo500<=coupures500) && (avis!="o"))


                      }
                   reste = retraitfranc - (coupures500*500)
                   dispo200 = (reste/deuxcent).toInt
    if (dispo200>0){ 
    println("il reste "+ reste+"chf à distribuer " )
    println ("Vous pouvez obtenir au maximum "+ dispo200 +  " billet(s) de 200 CHF")

                            do{
                              println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                              avis = readLine()
                            if (avis == "o"){
                              coupures200 = dispo200


                            }
                          else if (avis!="o" ){

                            coupures200 = avis.toInt
                          }

                              }while((dispo200<=coupures200)&&(avis!="o"))
                            }
                            reste = reste - (coupures200*200)
                   dispo100= (reste/cent).toInt



                  if(dispo100>0){
                    println("il reste "+ reste+"chf à distribuer " )
                    println ("Vous pouvez obtenir au maximum "+ dispo100 +  " billet(s) de 100 CHF")

                                            do{
                                              println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                              avis = readLine()
                                            if (avis == "o"){
                                              coupures100 = dispo100


                                            }
                                          else if (avis!="o" ){

                                            coupures100 = avis.toInt
                                          }

                                              }while((dispo100<=coupures100)&&(avis!="o"))
                                            }
                                            reste = reste - (coupures100*100)
                                   dispo50= (reste/cinquante).toInt
                    if(dispo50>0){
                      println("il reste "+ reste+"chf à distribuer " )
                      println ("Vous pouvez obtenir au maximum "+ dispo50 +  " billet(s) de 50 CHF")

                                              do{
                                                println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                avis = readLine()
                                              if (avis == "o"){
                                                coupures50 = dispo50


                                              }
                                            else if (avis!="o" ){

                                              coupures50 = avis.toInt
                                            }

                                                }while((dispo50<=coupures50)&&(avis!="o"))
                                              }
                                              reste = reste - (coupures50*50)
                                     dispo20= (reste/vingt).toInt
                    if(dispo20>0){
                     println("il reste "+ reste+"chf à distribuer " )
                     println ("Vous pouvez obtenir au maximum "+ dispo20 +  " billet(s) de 20 CHF")

                                             do{
                                               println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                               avis = readLine()
                                             if (avis == "o"){
                                               coupures20 = dispo20


                                             }
                                           else if (avis!="o" ){

                                             coupures20 = avis.toInt
                                           }

                                               }while((dispo20<=coupures20)&&(avis!="o"))
                                             }
                                             reste = reste - (coupures20*20)
                                    dispo10= (reste/dix).toInt

                    println("Veuillez retirer la somme demandée :")
                   if (coupures500>0)println(coupures500+ " billet(s) de 500 CHF")
                   if (coupures200>0)println(coupures200+ " billet(s) de 200 CHF")
                   if (coupures100>0)println(coupures100 + " billet(s) de 100 CHF")
                   if (coupures50>0)println(coupures50 + " billet(s) de 50 CHF")
                   if (coupures20>0)println(coupures20+  " billet(s) de 20 CHF")
                   if (dispo10>0)println(dispo10+ " billet(s) de 10 CHF")
                   printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f\n",comptes(id))
                  }else{
                    //si petite coupures est choisi
                    if(dispo100>0){
                    println("il reste "+ reste+"chf à distribuer " )
                      println ("Vous pouvez obtenir au maximum "+ dispo100 +  " billet(s) de 100 CHF")

                                              do{
                                                println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                avis = readLine()
                                              if (avis == "o"){
                                                coupures100 = dispo100


                                              }
                                            else if (avis!="o" ){

                                              coupures100 = avis.toInt
                                            }

                                                }while((dispo100<=coupures100)&&(avis!="o"))
                    }
                                              reste = reste - (coupures100*100)
                                     dispo50= (reste/cinquante).toInt
                      if(dispo50>0){
                        println("il reste "+ reste+"chf à distribuer " )
                        println ("Vous pouvez obtenir au maximum "+ dispo50 +  " billet(s) de 50 CHF")

                                                do{
                                                  println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                  avis = readLine()
                                                if (avis == "o"){
                                                  coupures50 = dispo50


                                                }
                                              else if (avis!="o" ){

                                                coupures50 = avis.toInt
                                              }

                                                  }while((dispo50<=coupures50)&&(avis!="o"))
                                                }
                                                reste = reste - (coupures50*50)
                                       dispo20= (reste/vingt).toInt
                      if(dispo20>0){
                       println("il reste "+ reste+"chf à distribuer " )
                       println ("Vous pouvez obtenir au maximum "+ dispo20 +  " billet(s) de 20 CHF")

                                               do{
                                                 println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                 avis = readLine()
                                               if (avis == "o"){
                                                 coupures20 = dispo20


                                               }
                                             else if (avis!="o" ){

                                               coupures20 = avis.toInt
                                             }

                                                 }while((dispo20<=coupures20)&&(avis!="o"))
                                               }
                                               reste = reste - (coupures20*20)
                                      dispo10= (reste/dix).toInt

                      println("Veuillez retirer la somme demandée :")

                     if (coupures100>0)println(coupures100+" billet(s) de 100 CHF")
                     if (coupures50>0)println(coupures50+" billet(s) de 50 CHF")
                     if (coupures20>0)println(coupures20+" billet(s) de 20 CHF")
                     if (dispo10>0)println(dispo10+" billet(s) de 10 CHF")
                     printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f\n",comptes(id))
                  }

                  }else if (retraitfranc<200) {

                    if(dispo100>0){
                      println("il reste "+ reste+" chf à distribuer " )
                      println ("Vous pouvez obtenir au maximum "+ dispo100 +  " billet(s) de 100 CHF")

                                              do{
                                                println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                avis = readLine()
                                              if (avis == "o"){
                                                coupures100 = dispo100


                                              }
                                            else if (avis!="o" ){

                                              coupures100 = avis.toInt
                                            }

                                                }while((dispo100<=coupures100)&&(avis!="o"))
                                              }
                                              reste = reste - (coupures100*100)
                                     dispo50= (reste/cinquante).toInt
                      if(dispo50>0){
                        println("il reste "+ reste+"chf à distribuer " )
                        println ("Vous pouvez obtenir au maximum "+ dispo50 +  " billet(s) de 50 CHF")

                                                do{
                                                  println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                  avis = readLine()
                                                if (avis == "o"){
                                                  coupures50 = dispo50


                                                }
                                              else if (avis!="o" ){

                                                coupures50 = avis.toInt
                                              }

                                                  }while((dispo50<=coupures50)&&(avis!="o"))
                                                }
                                                reste = reste - (coupures50*50)
                                       dispo20= (reste/vingt).toInt
                      if(dispo20>0){
                       println("il reste "+ reste+"chf à distribuer " )
                       println ("Vous pouvez obtenir au maximum "+ dispo20 +  " billet(s) de 20 CHF")

                                               do{
                                                 println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                 avis = readLine()
                                               if (avis == "o"){
                                                 coupures20 = dispo20


                                               }
                                             else if (avis!="o" ){

                                               coupures20 = avis.toInt
                                             }

                                                 }while((dispo20<=coupures20)&&(avis!="o"))
                                               }
                                               reste = reste - (coupures20*20)
                                      dispo10= (reste/dix).toInt

                      println("Veuillez retirer la somme demandée :")

                     if (coupures100>0)println(coupures100+" billet(s) de 100 CHF")
                     if (coupures50>0)println(coupures50+" billet(s) de 50 CHF")
                     if (coupures20>0)println(coupures20+" billet(s) de 20 CHF")
                     if (dispo10>0)println(dispo10+" billet(s) de 10 CHF")
                     printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f\n",comptes(id))

                  }



                }else if(deviser==2){
                  if(dispo100>0){
                    println("il reste "+ reste+" euros à distribuer " )
                      println ("Vous pouvez obtenir au maximum "+ dispo100 +  " billet(s) de 100 euros")

                                              do{
                                                println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                avis = readLine()
                                              if (avis == "o"){
                                                coupures100 = dispo100


                                              }
                                            else if (avis!="o" ){

                                              coupures100 = avis.toInt
                                            }

                                                }while((dispo100<=coupures100)&&(avis!="o"))
                    }
                                              reste = reste - (coupures100*100)
                                     dispo50= (reste/cinquante).toInt
                      if(dispo50>0){
                        println("il reste "+ reste+" euros à distribuer " )
                        println ("Vous pouvez obtenir au maximum "+ dispo50 +  " billet(s) de 50 euros")

                                                do{
                                                  println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                  avis = readLine()
                                                if (avis == "o"){
                                                  coupures50 = dispo50


                                                }
                                              else if (avis!="o" ){

                                                coupures50 = avis.toInt
                                              }

                                                  }while((dispo50<=coupures50)&&(avis!="o"))
                                                }
                                                reste = reste - (coupures50*50)
                                       dispo20= (reste/vingt).toInt
                      if(dispo20>0){
                       println("il reste "+ reste+" euros à distribuer " )
                       println ("Vous pouvez obtenir au maximum "+ dispo20 +  " billet(s) de 20 euros ")

                                               do{
                                                 println("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée.")
                                                 avis = readLine()
                                               if (avis == "o"){
                                                 coupures20 = dispo20


                                               }
                                             else if (avis!="o" ){

                                               coupures20 = avis.toInt
                                             }

                                                 }while((dispo20<=coupures20)&&(avis!="o"))
                                               }
                                               reste = reste - (coupures20*20)
                                      dispo10= (reste/dix).toInt

                      println("Veuillez retirer la somme demandée :")

                     if (coupures100>0)println(coupures100 +" billet(s) de 100 euros")
                     if (coupures50>0)println(coupures50 +" billet(s) de 50 euros")
                     if (coupures20>0)println(coupures20 + " billet(s) de 20 euros")
                     if (dispo10>0)println(dispo10 + " billet(s) de 10 euros")
                     printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f\n",comptes(id))
                }
  }
  def consultation (id : Int, comptes : Array[Double]) : Unit ={
    println("Votre montant disponible sur votre compte est de : " + comptes(id))
  }
   def changepin(id : Int, codespin : Array[String]) : Unit={
     
     codespin(id) = readLine("Veuillez saisir votre nouveau code PIN :")
     if (codespin(id).length<8){
       
       do{
         println("Le code PIN doit comporter au moins 8 caractères")
         codespin(id) = readLine("Veuillez saisir votre nouveau code PIN :")
       }while(codespin(id).length<8)
     }
     println("Votre nouveau code PIN est : " + codespin(id))
   }
  def traitement(id : Int, comptes : Array[Double]) : Unit ={
    println("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code pin \n5) Terminer")
        var choix = readInt()
          if (choix==5){
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          }else {
          while(choix!=5){
          if (choix==1){
     depot(id,comptes)     
          }else if (choix==2){
            retrait(id,comptes)
          }else if (choix==3){
          consultation(id: Int, comptes : Array[Double])
          }else if(choix==4){
            changepin(id: Int, codespin : Array[String])
          }
          
            println("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code pin \n5) Terminer")
             choix = readInt()
          }
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          }
             
        }

  
  
  def main(args: Array[String]): Unit = {
  var id = idpi()
    
    if (id < 100){
      traitement(id,comptes)
      id = idpi()
      while (id<100){
        traitement(id,comptes)
        id = idpi()
      }
      }
      
    }
    
}
  

