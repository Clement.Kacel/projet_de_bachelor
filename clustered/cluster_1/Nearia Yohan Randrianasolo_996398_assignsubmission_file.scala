//Assignment: Nearia Yohan Randrianasolo_996398_assignsubmission_file

import scala.io.StdIn._

object Main{
  val nbClients: Int = 100
  val Comptes: Array[Double] = Array.fill(nbClients)(1200.0)
  val CodePin: Array[String] = Array.fill(nbClients)("INTRO1234")
  // Méthode pour l'opération de dépôt
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    // Taux de conversion
    var EURenCHF = 0.95
    var CHFenEUR = 1.05
    println("Indiquez la devise du dépôt:  \n1) CHF \n2) EUR ")
    val devise = readLine("Votre choix: ").toInt
    //Saisie du montant de dépôt 
    var montantDepot = 0
    var montantCorrect = false
    // Le montant doit être un muliple de 10
    while(!montantCorrect){
      montantDepot = readLine("Indiquez le montant du dépôt > ").toInt
      if(montantDepot % 10 == 0){
        montantCorrect = true 
      } else {
        println("Le montant doit être un multiple de 10")
      }
    }
    if(devise == 2){
      // Conversion EUR en CHF 
      val montantEnCHF = montantDepot * EURenCHF
      // Mise à jour du solde 
      comptes(id) += montantEnCHF
    } else {
      // Si la devise en CHF en ajuste
      comptes(id) += montantDepot
    }
    // Afficher le nouveau solde
    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
  } 
 // Méthode pour la consultation de solde
  def consultationSolde(id: Int, comptes: Array[Double]): Unit = {
    println("Le montant disponible sur votre compte est de : "+ comptes(id)+ " CHF")  
  }
  // Méthode pour changer le code PIN
  def changepin(id: Int, codespin: Array[String]): Unit = {
    var newCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    // Vérifier que le nouveau code PIN contient 8 caractères 
    while (newCodePin.length < 8){
      println("Votre code PIN ne contient pas au moins 8 caractères")
      newCodePin = readLine("Saisissez à noouveau votre nouveau code PIN > ")
    }
    CodePin(id) = newCodePin
    println("Le code PIN a été mise à jour avec succès")
  }
  def retrait(id: Int, comptes: Array[Double]): Unit ={
    // Taux de conversion
    var EURenCHF = 0.95
    var CHFenEUR = 1.05
    // Opération de retrait 
    println("Indiquez la devise : \n1 : CHF  \n2 : EUR ")

    // Le message doit se répéter tant que l’utilisateur fait un choix différent de 1 ou 2.
    var devise_Retrait = 0
    var devise_Correct = false

    while(!devise_Correct){
      devise_Retrait = readLine("Votre choix de devise : ").toInt
      if (devise_Retrait == 1 || devise_Retrait == 2){
        devise_Correct = true
      } else {
        println("Veuilez choisir entre 1 pour CHF ou 2 pour EUR. ")
      } 
    } // while !devise_Correct 
    // Plafond de retrait = 10% du solde total 
    val plafond_Retrait_Autorise = comptes(id) * 0.10

    // Saisie du montant de retrait 
    var montant_Retrait = 0
    var montant_Correct = false
    while (!montant_Correct){
        montant_Retrait =readLine("Indiquez le montant du retrait : ").toInt

        // Vérifier si le montant est un multiple de 10
      if (montant_Retrait % 10 == 0){

        // Vérifier si le montant est inférieur ou égale au plafond
        if (montant_Retrait <= plafond_Retrait_Autorise){
         montant_Correct = true 

        } else {
          println("Votre plafond de retrait autorisé est de : " + plafond_Retrait_Autorise + " CHF " )
        }
      } else {
        println("Le montant doit être un multiple de 10.")
      }
    } //while !montant_Correct 
    var choix_de_Coupures = 0
    var Bonne_Coupures = false 
    // Si la devise est en CHF
    if (devise_Retrait == 1) {
      if (montant_Retrait >= 200){
        //Le message doit se répéter tant que l’utilisateur fait un choix différent de 1 ou 2
        while (!Bonne_Coupures){
          choix_de_Coupures = readLine("En \n1) grosses coupures \n2) petites coupures : ").toInt
          if (choix_de_Coupures == 1 || choix_de_Coupures == 2){
            Bonne_Coupures = true
          } else {
             println("Vous devez chosir entre 1 pour grosses coupures et 2 pour petites coupures")
          }
        }
      } else {
        choix_de_Coupures = 2
      }
    } // if devise_Retrait == 1 

    // Chaque coupure de billets
    var coupure500 = 0
    var coupure200 = 0
    var coupure100 = 0
    var coupure50 = 0 
    var coupure20 = 0
    var coupure10 = 0


    // Le nombre de billets 
    var billets500 = 0
    var billets200 = 0
    var billets100 = 0
    var billets50 = 0
    var billets20 = 0
    var billets10 = 0

    var reste = montant_Retrait
    // Traitement pour les grosses coupures 
    if (choix_de_Coupures == 1){
      // Pour les billets de 500 
      if (reste >= 500){
        coupure500 = reste / 500
        coupure500 = coupure500.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure500 + " billet(s) de 500 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets500 = coupure500
           reste = reste - billets500 * 500
         } else if (choixCoupure == "0") {
           billets500 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure500){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets500 = choixClient
              reste = reste - billets500 * 500
          }
      }  // Grosses coupures 500
    if (reste >= 200){
      coupure500 = reste / 200
      coupure200 = coupure200.toInt

       println("Il reste " + reste + " CHF à distribuer")
       println("Vous pouvez obtenir au maximum " + coupure200 + " billet(s) de 200 CHF")
       print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

       var choixCoupure = readLine()
       if (choixCoupure == "o"){
         billets200 = coupure200
         reste = reste - billets200 * 200
       } else if (choixCoupure == "0") {
         billets200 = 0 
        } else {
          var choixClient = choixCoupure.toInt
          while (choixCoupure != "o" && choixCoupure.toInt >= coupure200){
            print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
            choixCoupure = readLine ()
            choixClient = choixCoupure.toInt        
          }
            billets200 = choixClient
            reste = reste - billets200 * 200
        }
    }  // Grosses coupures 200

      if (reste >= 100){
        coupure100 = reste / 100
        coupure100 = coupure100.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets100 = coupure100
           reste = reste - billets100 * 100
         } else if (choixCoupure == "0") {
           billets100 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure100){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets100 = choixClient
              reste = reste - billets100 * 100
          }
      }  // Grosses coupures 100
      if (reste >= 50){
        coupure50 = reste / 50
        coupure50 = coupure50.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets50 = coupure50
           reste = reste - billets50 * 50
         } else if (choixCoupure == "0") {
           billets50 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure50){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets50 = choixClient
              reste = reste - billets50 * 50
          }
      }  // Grosses coupures 50
      if (reste >= 20){
        coupure20 = reste / 20
        coupure20 = coupure20.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets20 = coupure20
           reste = reste - billets20 * 20
         } else if (choixCoupure == "0") {
           billets20 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure20){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets20 = choixClient
              reste = reste - billets20 * 20
          }
      }  // Grosses coupures 20
      if (reste >= 10){
        coupure10 = reste / 10
        coupure10 = coupure10.toInt
        billets10 = coupure10
        println("Vous avez choisi aucune coupure alors vous aurez " + coupure10 + " billet(s) de 10 EUR")
        reste = reste - billets10 * 10

      }  // Coupure 10 EUR 

           println("Veuillez retirer la somme demandée :")
            if (billets500 > 0) println(s"$billets500 billet(s) de 500 CHF")
            if (billets200 > 0) println(s"$billets200 billet(s) de 200 CHF")
            if (billets100 > 0) println(s"$billets100 billet(s) de 100 CHF")
            if (billets50 > 0) println(s"$billets50 billet(s) de 50 CHF")
            if (billets20 > 0) println(s"$billets20 billet(s) de 20 CHF")
            if (billets10 > 0) println(s"$billets10 billet(s) de 10 CHF")
    } else {
    if (choix_de_Coupures == 2) {
      if (reste >= 100){
        coupure100 = reste / 100
        coupure100 = coupure100.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets100 = coupure100
           reste = reste - billets100 * 100
         } else if (choixCoupure == "0") {
           billets100 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure100){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets100 = choixClient
              reste = reste - billets100 * 100
          }
      }  // Petites coupures 100
      if (reste >= 50){
        coupure50 = reste / 50
        coupure50 = coupure50.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets50 = coupure50
           reste = reste - billets50 * 50
         } else if (choixCoupure == "0") {
           billets50 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure50){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets50 = choixClient
              reste = reste - billets50 * 50
          }
      }  // Petites coupures 50
      if (reste >= 20){
        coupure20 = reste / 20
        coupure20 = coupure20.toInt

         println("Il reste " + reste + " CHF à distribuer")
         println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 CHF")
         print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

         var choixCoupure = readLine()
         if (choixCoupure == "o"){
           billets20 = coupure20
           reste = reste - billets20 * 20
         } else if (choixCoupure == "0") {
           billets20 = 0 
          } else {
            var choixClient = choixCoupure.toInt
            while (choixCoupure != "o" && choixCoupure.toInt >= coupure20){
              print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
              choixCoupure = readLine ()
              choixClient = choixCoupure.toInt        
            }
              billets20 = choixClient
              reste = reste - billets20 * 20
          }
      }  // Petites coupures 20
      if (reste >= 10){
        coupure10 = reste / 10
        coupure10 = coupure10.toInt
        billets10 = coupure10
        println("Vous avez choisi aucune coupure alors vous aurez " + coupure10 + " billet(s) de 10 EUR")
        reste = reste - billets10 * 10

      }  // Coupure 10 EUR 

        println("Veuillez retirer la somme demandée :")
        if (billets100 > 0) println(s"$billets100 billet(s) de 100 CHF")
        if (billets50 > 0) println(s"$billets50 billet(s) de 50 CHF")
        if (billets20 > 0) println(s"$billets20 billet(s) de 20 CHF")
        if (billets10 > 0) println(s"$billets10 billet(s) de 10 CHF")
      } else if (devise_Retrait == 2) {
        // Si la devise est en EUR 
        if (reste >= 100){
          coupure100 = reste / 100
          coupure100 = coupure100.toInt

           println("Il reste " + reste + " EUR à distribuer")
           println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 EUR")
           print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

           var choixCoupure = readLine()
           if (choixCoupure == "o"){
             billets100 = coupure100
             reste = reste - billets100 * 100
           } else if (choixCoupure == "0") {
             billets100 = 0 
            } else {
              var choixClient = choixCoupure.toInt
              while (choixCoupure != "o" && choixCoupure.toInt >= coupure100){
                print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
                choixCoupure = readLine ()
                choixClient = choixCoupure.toInt        
              }
                billets100 = choixClient
                reste = reste - billets100 * 100
            }
        }  // Coupure 100 EUR
        if (reste >= 50){
          coupure50 = reste / 50
          coupure50 = coupure50.toInt

           println("Il reste " + reste + " EUR à distribuer")
           println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 EUR")
           print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

           var choixCoupure = readLine()
           if (choixCoupure == "o"){
             billets50 = coupure50
             reste = reste - billets50 * 50
           } else if (choixCoupure == "0") {
             billets50 = 0 
            } else {
              var choixClient = choixCoupure.toInt
              while (choixCoupure != "o" && choixCoupure.toInt >= coupure50){
                print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
                choixCoupure = readLine ()
                choixClient = choixCoupure.toInt        
              }
                billets50 = choixClient
                reste = reste - billets50 * 50
            }
        }  //  Coupure 50 EUR 
        if (reste >= 20){
          coupure20 = reste / 20
          coupure20 = coupure20.toInt

           println("Il reste " + reste + " EUR à distribuer")
           println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 EUR")
           print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")

           var choixCoupure = readLine()
           if (choixCoupure == "o"){
             billets20 = coupure20
             reste = reste - billets20 * 20
           } else if (choixCoupure == "0") {
             billets20 = 0 
            } else {
              var choixClient = choixCoupure.toInt
              while (choixCoupure != "o" && choixCoupure.toInt >= coupure20){
                print("Veuilez saisir une valeur inférieure à celle qui est proposé :")
                choixCoupure = readLine ()
                choixClient = choixCoupure.toInt        
              }
                billets20 = choixClient
                reste = reste - billets20 * 20
            }
        }  //  Coupure 20 EUR
        if (reste >= 10){
          coupure10 = reste / 10
          coupure10 = coupure10.toInt
          billets10 = coupure10
          println("Vous avez choisi aucune coupure alors vous aurez " + coupure10 + " billet(s) de 10 EUR")
          reste = reste - billets10 * 10

        }  // Coupure 10 EUR 

         println("Veuillez retirer la somme demandée :")

         if (billets100 > 0) println(s"$billets100 billet(s) de 100 EUR")
         if (billets50 > 0) println(s"$billets50 billet(s) de 50 EUR")
         if (billets20 > 0) println(s"$billets20 billet(s) de 20 EUR")
        if (billets10 > 0) println(s"$billets10 billet(s) de 10 EUR")
      } // if choix_de_Coupures = 2

    } 
    // Déduction du montant retiré
    if (devise_Retrait == 2){
      // Conversion EUR en CHF
      val montant_RetireCHF = montant_Retrait * EURenCHF
      //Vérification montant en CHF 
      if (montant_RetireCHF <= plafond_Retrait_Autorise) {
      //Mise à jour du solde 
      comptes(id) -= montant_RetireCHF
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte esst de: %2f CHF \n", comptes(id))
      }else {
      println("Votre plafond de retrait autorisé est de: "+ plafond_Retrait_Autorise + " CHF")
      }
  } else {
      // conversion CHF en EUR 
      val montant_RetireEUR = montant_Retrait / CHFenEUR
      // Vérifier si montant en EUR est <= au plafond
      if(montant_RetireEUR <= plafond_Retrait_Autorise){
        // Mise à jour solde
        comptes(id)-= montant_Retrait
        printf ("Votre retraita été pris en compte, le nouveau montant disponible sur votre compte est de: %2f CHF \n", comptes(id))
      } else {
        printf("Votre plafond de retrait autorisé est de: "+ plafond_Retrait_Autorise + " CHF")
      }
    }
  }
  def main(args: Array[String]): Unit ={
    //Menu
    val Depot = 1
    val Retrait = 2
    val Compte = 3
    val Changement = 4
    val Terminer = 5
    var ContinuerProgramme = true
    while(ContinuerProgramme){
      // Boucle pour saisie identifiant et code PIN
      var identification = true
      while (identification){
        // Saisie Code identifiant 
        val IdClient = readLine("Saisissez votre code identifiant >").toInt
        // Validité de l'identifiant 
        if (IdClient >= nbClients || IdClient < 0 ){
          println("cet identifiant n'est pas valable.")
          identification = false
          ContinuerProgramme = false
        } else {
          var codePinValid = false 
          var  tentatives = 3
          while (!codePinValid && tentatives > 0){
            print("Saisissez votre code PIN > ")
            val codeSaisie = readLine()
            // Vérifier si le code PIN est bon
            if (codeSaisie == CodePin(IdClient)){
              codePinValid = true 
            } else {
              tentatives -= 1 
              println("  Code pin erroné, il vous reste "+ tentatives+" tentatives >")
              if (tentatives == 0){
                println("Trop d’erreurs, abandon de l’identification")
                identification = false
              }
            }
          } // Boucle code PIN 
          if (codePinValid){
            var continuerOperation = true 
            while (continuerOperation){
              println("Choisissez votre opération :")
              println("1) Dépôt d'argent")
              println("2) Retrait d'argent")
              println("3) Consultation du solde")
              println("4) Changement du code PIN")
              println("5) Terminer")
              // Choix utilisateur
              val choix = readLine("Votre choix >").toInt 
              if (choix == 5){
                println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
                continuerOperation = false
              } else if (choix == 4){
                // Appel méthode pour changement de code PIN
                changepin(IdClient, CodePin)
              } else if ( choix == 1) {
                // Appel de la méthode depot pour l'opération de dépôt
                depot(IdClient, Comptes)
              } else if (choix == 3){
                // Opération de consultation du solde
                consultationSolde(IdClient, Comptes)
              } else if (choix == 2){
                retrait(IdClient, Comptes)
              } else{
                println("Opération non-valide. Veuillez choisir une opération valide")
              }
            }// Boucle continuer Opération
          }
        }
      }// Boucle Identification
    }// Boucle continuerProgramme
  }// def main
}// Main