//Assignment: Ilinca Pletosu_996612_assignsubmission_file

//import des librairies de scala pour lire des entiers et des strings
import scala.io.StdIn.readInt
import scala.io.StdIn.readLine


object Main {
  var taux_de_change:Double = 1.00

  def main(args: Array[String]): Unit = {
    //DEBUT DU MAIN
    var choix_client:Int = 0
    var nbclients:Int = 100
    var bon_choix:Boolean = false
    var continuer_programme:Boolean = true
    val sortie_programme = 5
    var PIN_premiere:Boolean = true
    var nombreTentatives = 3
    var entreePin:String = ""
    var codespin = Array.fill(nbclients)("INTRO1234")
    val depotvar = 1
    val retraitvar = 2
    val consulter = 3
    var changer_pin = 4
    var comptes = Array.fill(nbclients)(1200.00)
    var login:Int = 0
    while(continuer_programme)
    {
      if(PIN_premiere==true)
      {
        //si c'est la première fois que on entre dans le programme alors on demande le code PIN

        println("Saisissez votre code identifiant >")  //on commence par demander le login du compte
        login = readInt()  //on le lit sous forme d'entier
        if(login>=nbclients || login<0){
          //Si c'est une valeur hors du tableau
          println("Cet identifiant n’est pas valable. ")
          System.exit(0)
        }
        while((nombreTentatives==1 || nombreTentatives==2 || nombreTentatives==3) && PIN_premiere==true)
        {
          println("Saisissez votre code PIN >")
          entreePin = readLine()
          if(entreePin==codespin(login))
          {
            //le client a mis le bon PIN
            PIN_premiere=false
          }
          else
          {
            //il reste le nombre de tentatives - 1
            nombreTentatives = nombreTentatives - 1
            if(nombreTentatives==0)
            {//Plus de tentatives, le client a fait le mauvais code 3 fois donc on sort de son login
              println("Trop d’erreurs, abandon de l’identification")
            }
            else{
              //on affiche combien il reste de tentatives
              print("Code pin erroné, il vous reste ")
              print(nombreTentatives)
              println(" tentatives >")
            }
          }
        }
        nombreTentatives = 3
      }
      choix_client=0
      while(choix_client!=sortie_programme && PIN_premiere==false)
      {
        //tant que le client n'a pas choisi l'option 5 : sortir, alors on écrit le menu :
        do
        {
          //on demande au client l'opération qu'il souhaite faire
          println("Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\n")
          print("Votre choix : ")
          choix_client = readInt()  //récupération de l'opération du client

        }while(choix_client>5 || choix_client<1)

        if(choix_client==sortie_programme){
          //si le choix est le choix n°4, il faut quitter le programme
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          PIN_premiere=true
        }
        else
        {
          //le code est bon :
          if(choix_client==depotvar)
          {
            depot(login,comptes)
          }
          else if(choix_client==retraitvar)
          {
            retrait(login,comptes)
          }
          else if(choix_client==consulter)
          {
            printf("Le montant disponible sur votre compte est de : %.2f CHF \n", comptes(login))
          }
          else if(choix_client==changer_pin)
          {
            changepin(login,codespin)
          }
        }
      }
    }
  }
   def depot(id : Int, comptes : Array[Double]) : Unit = {
     var devise=0  //variable de la devise du depot 
     taux_de_change=1.00
     do
     {
       //on execute cette sequence au moins 1x et tant que le resultat est mauvais 
       println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
       devise = readInt()
     }while ((devise!=1) && (devise!=2))
     if(devise==2){
       taux_de_change=0.95
     }
     var MontantDuDepot:Int = 0

     do
     {              //on execute cette sequence au moins 1x et tant que le resultat est mauvais 

       MontantDuDepot = readLine("Indiquez le montant du dépôt >").toInt
       if(MontantDuDepot<=0){
         println("Veuillez entrer un montant positif")
       }
       if(MontantDuDepot%10!=0)
       {  //Si c'est pas multiple de 10, le rappeler au client
         println("Le montant doit être un multiple de 10")
       }
       else{}  //sinon, rien faire
     }while((MontantDuDepot<0) || (MontantDuDepot%10!=0))  //on ne sort pas tant que les conditions sont mauvaises 


     comptes(id) = comptes(id) +(MontantDuDepot*taux_de_change)

     printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f" , comptes(id))
     println(" CHF")

   }

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    var devise=0  //variable de la devise du retrait 
    var choix_o = ""  //variable du choix d'accepter ou non la choix_de_coupure
    var passage=0
     taux_de_change=1.00
    var b500=0
    var b200=0
    var b100=0
    var b50=0
    var b20=0
    var b10=0
    do
    {
      //on execute cette sequence au moins 1x et tant que le resultat est mauvais 
      println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
      devise = readInt()
    }while ((devise!=1) && (devise!=2))
    if(devise==2){
      taux_de_change=0.95
    }
    var MontantDuRetrait:Int = 0

    do
    {              //on execute cette sequence au moins 1x et tant que le resultat est mauvais 

      MontantDuRetrait = readLine("Indiquez le montant du retrait >").toInt
      if(MontantDuRetrait<=0){
        println("Veuillez entrer un montant positif")
      }
      if(MontantDuRetrait%10!=0)
      {  //Si c'est pas multiple de 10, le rappeler au client
        println("Le montant doit être un multiple de 10")
      }
      if(MontantDuRetrait>(comptes(id)/10))
      {
        printf("Votre plafond de retrait autorisé est de : %.2f CHF\n", comptes(id)/10)
      }
    }while((MontantDuRetrait<0) || (MontantDuRetrait%10!=0) || (MontantDuRetrait>(comptes(id)/10)))  //on ne sort pas tant que les conditions sont mauvaises 

    comptes(id) = comptes(id) - (MontantDuRetrait*taux_de_change)

    if(devise==1)
    {
      var choix_de_coupure:Int = 0
      //retrait en CHF
      if(MontantDuRetrait>=200)
      {  //>=200 donc on laisse le choix au client de la choix_de_coupure
        do
        {
          choix_de_coupure = readLine("En 1) grosses choix_de_coupures, 2) petites choix_de_coupures >").toInt
        }while((choix_de_coupure!=1)&&(choix_de_coupure!=2))  //tant que la choix_de_coupure est une mauvaise séléction, on répète la séquence
      }
      else{
        choix_de_coupure=2
      }
        if(choix_de_coupure==1)
        {  //si on a choisit la grosse choix_de_coupure alors on a accès aux billets de 500

            //On détermine quel est le billet qu'on va proposer au client tant que le retrait n'est pas effectué et qu'il reste des sous à donner au client
            println("Il reste " + MontantDuRetrait + " CHF à distribuer")  
            if(MontantDuRetrait>=500){
              println("Vous pouvez obtenir au maximum " + MontantDuRetrait/500 + " billet(s) de 500 CHF")

              choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if(choix_o=="o")
              {
                b500 = MontantDuRetrait/500
                MontantDuRetrait = MontantDuRetrait - (b500*500)
              }
              else
              {
                if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/500){
                  //Si on a choisit un nombre qui est bon alors
                  b500 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b500*500)
                }
                else
                {
                  while((choix_o.toInt<0) || (choix_o.toInt>MontantDuRetrait/500)){
                    choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                  }
                  b500 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b500*500)
                }
              }

              passage+=1
            }//fin de 500
             if(MontantDuRetrait>=200){
              println("Vous pouvez obtenir au maximum " + MontantDuRetrait/200 + " billet(s) de 200 CHF")

              choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if(choix_o=="o")
              {
                b200 = MontantDuRetrait/200
                MontantDuRetrait = MontantDuRetrait - (b200*200)
              }
              else
              {
                if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/200){
                  //Si on a choisit un nombre qui est bon alors
                  b200 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b200*200)
                }
                else
                {
                  while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/200)){
                    choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                  }
                  b200 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b200*200)
                }
              }

              passage+=1
            }

              choix_de_coupure=2




        }
        if(choix_de_coupure==2)
        {


             println("Il reste " + MontantDuRetrait + " CHF à distribuer")  
            if(MontantDuRetrait>=100){
              println("Vous pouvez obtenir au maximum " + MontantDuRetrait/100 + " billet(s) de 100 CHF")

              choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if(choix_o=="o")
              {
                b100 = MontantDuRetrait/100
                MontantDuRetrait = MontantDuRetrait - (b100*100)
              }
              else
              {
                if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/100){
                  //Si on a choisit un nombre qui est bon alors
                  b100 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b100*100)
                }
                else
                {
                  while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/100)){
                    choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
//le cas ou l'utilisateur ne remplit toujjours pas les bonnes conditions

                  }
                  b100 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b100*100)
                }
              }

              passage+=1  //on ajoute 1 au nombre de passage
            }
            if(MontantDuRetrait>=50){
              println("Vous pouvez obtenir au maximum " + MontantDuRetrait/50 + " billet(s) de 50 CHF")

              choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if(choix_o=="o")
              {
                b50 = MontantDuRetrait/50
                MontantDuRetrait = MontantDuRetrait - (b50*50)
              }
              else
              {
                if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/50){
                  //Si on a choisit un nombre qui est bon alors
                  b50 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b50*50)
                }
                else
                {
                  while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/50)){
                    choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                  }
                  b50 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b50*50)
                }
              }

              passage+=1
            }
            if(MontantDuRetrait>=20){
              println("Vous pouvez obtenir au maximum " + MontantDuRetrait/20 + " billet(s) de 20 CHF")

              choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if(choix_o=="o")
              {
                b20 = MontantDuRetrait/20
                MontantDuRetrait = MontantDuRetrait - (b20*20)
              }
              else
              {
                if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/20){
                  //Si on a choisit un nombre qui est bon alors
                  b20 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b20*20)
                }
                else
                {
                  while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/20)){
                    choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                  }
                  b20 = choix_o.toInt
                  MontantDuRetrait = MontantDuRetrait - (b20*20)
                }
              }

              passage+=1
            }
            if(MontantDuRetrait>=10)
            {
              println("Vous pouvez obtenir au maximum " + MontantDuRetrait/10 + " billet(s) de 10 CHF")
              b10 = MontantDuRetrait/10
              MontantDuRetrait-=(b10*10)
            }

          println("Veuillez retirer la somme demandée : ")
          if(b500>0)
          {
            println(""+b500 + " billet(s) de 500 CHF")
          }
          if(b200>0)
          {
            println(""+b200 + " billet(s) de 200 CHF")
          }
          if(b100>0)
          {
            println(""+b100 + " billet(s) de 100 CHF")
          }
          if(b50>0)
          {
            println(""+b50 + " billet(s) de 50 CHF")
          }
          if(b20>0)
          {
            println(""+b20 + " billet(s) de 20 CHF")
          }
          if(b10>0)
          {
            println(""+b10 + " billet(s) de 10 CHF")
          }

        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

        }
    }
    else
    {
      //DEVISE EN EURO

           println("Il reste " + MontantDuRetrait + " CHF à distribuer")  
          if(MontantDuRetrait>=100 ){
            println("Vous pouvez obtenir au maximum " + MontantDuRetrait/100 + " billet(s) de 100 EUR")

            choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(choix_o=="o")
            {
              b100 = MontantDuRetrait/100
              MontantDuRetrait = MontantDuRetrait - (b100*100)
            }
            else
            {
              if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/100){
                //Si on a choisit un nombre qui est bon alors
                b100 = choix_o.toInt
                MontantDuRetrait = MontantDuRetrait - (b100*100)
              }
              else
              {
                while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/100)){
                  choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                }
                b100 = choix_o.toInt
                MontantDuRetrait = MontantDuRetrait - (b100*100)
              }
            }

            passage+=1
          }
           if(MontantDuRetrait>=50 ){
            println("Vous pouvez obtenir au maximum " + MontantDuRetrait/50 + " billet(s) de 50 EUR")

            choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(choix_o=="o")
            {
              b50 = MontantDuRetrait/50
              MontantDuRetrait = MontantDuRetrait - (b50*50)
            }
            else
            {
              if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/50){
                //Si on a choisit un nombre qui est bon alors
                b50 = choix_o.toInt
                MontantDuRetrait = MontantDuRetrait - (b50*50)
              }
              else
              {
                while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/50)){
                  choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                }
                b50 = choix_o.toInt
                MontantDuRetrait = MontantDuRetrait - (b50*50)
              }
            }

            passage+=1
          }
           if(MontantDuRetrait>=20 ){
            println("Vous pouvez obtenir au maximum " + MontantDuRetrait/20 + " billet(s) de 20 EUR")

            choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if(choix_o=="o")
            {
              b20 = MontantDuRetrait/20
              MontantDuRetrait = MontantDuRetrait - (b20*20)
            }
            else
            {
              if(choix_o.toInt>=0 && choix_o.toInt<=MontantDuRetrait/20){
                //Si on a choisit un nombre qui est bon alors
                b20 = choix_o.toInt
                MontantDuRetrait = MontantDuRetrait - (b20*20)
              }
              else
              {
                while((choix_o.toInt<0 || choix_o.toInt>MontantDuRetrait/20)){
                  choix_o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                }
                b20 = choix_o.toInt
                MontantDuRetrait = MontantDuRetrait - (b20*20)
              }
            }

            passage+=1
          }
          if(MontantDuRetrait>=10 )
          {
            println("Vous pouvez obtenir au maximum " + MontantDuRetrait/10 + " billet(s) de 10 EUR")
            b10 = MontantDuRetrait/10
            MontantDuRetrait-=(b10*10)
          }

        println("Veuillez retirer la somme demandée : ")

        if(b100>0)
        {
          println(""+b100 + " billet(s) de 100 EUR")
        }
        if(b50>0)
        {
          println(""+b50 + " billet(s) de 50 EUR")
        }
        if(b20>0)
        {
          println(""+b20 + " billet(s) de 20 EUR")
        }
        if(b10>0)
        {
          println(""+b10 + " billet(s) de 10 EUR")
        }

      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
    }
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var proposition_pin:String = ""
    var taille:Int=0
    do{
      proposition_pin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      taille = proposition_pin.length  //taille du nouveau mot de passe
      if(taille<8){
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
    }while(taille<8)  //tant que la taille est pas >=8, on saisit à nouveau un nouveau mot de passe
    codespin(id) = proposition_pin

  }
}
