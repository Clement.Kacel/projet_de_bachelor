//Assignment: Eya Saoudi_996405_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine
import scala.math

object Main {
  var devise = 0
  var montant=0
  val CHF = 1
  val EUR = 2
  var sortie:Boolean=false
  var convert_inINT=0 // Variable pour stocker la valeur saisie, initialisée à 0
  def main(args: Array[String]): Unit = {
    var nbclients=100
    var premiereFois = true
    var tentativesPin = 3 
    var comptes = Array.fill(nbclients)(1200.00) //somme de départ du compte bancaire
    var option_choisi = 0
    var pin_entree = "pin"
    var codespin  = Array.fill(nbclients)("INTRO1234")
    var PIN_valide = false
    var identifiant=0

    while (identifiant>=0 && identifiant<=nbclients) {
      if(premiereFois){  //Si c'est notre première fois, on demande le code PIN
        println("Saisissez votre code identifiant >")
        identifiant=readInt()
      }
      if(identifiant>=0 && identifiant<=nbclients)
      {
        while (tentativesPin>0 && tentativesPin<=3 && premiereFois)
        { //tant que il reste des tentatives
          pin_entree = readLine("Saisissez votre code PIN >")  //lecture de l'entree du code pin
          if (pin_entree!=codespin(identifiant)) {
            tentativesPin -= 1 
            if(tentativesPin<=0)
            {  //si on a raté 3x le code, alors on affiche ce mesage
              println("Trop d’erreurs, abandon de l’identification")
              tentativesPin=90
              premiereFois=false
            }else
            {
              println("Code pin erroné, il vous reste " +tentativesPin+" tentatives >" )
            }
          }else
          {
            tentativesPin=100  //Comme ça la variable sera >3 et on ne reviendra plus ici
            premiereFois=false
          }
        }
        if(tentativesPin==100){  // <=> code pin réussi
          do{
            println("Choisissez votre opération :")
            println("1) Dépôt")
            println("2) Retrait")
            println("3) Consultation du compte")
            println("4) Changement du code pin")
            println("5) Terminer")

            option_choisi = readLine("Votre choix >").toInt  //on demande le choix et on le recupere dans la variable option_choisi
          }while(option_choisi<1 || option_choisi>5)

          if((option_choisi==1)&&(tentativesPin>3))
          {  //si on a choisi l'option 1 et que on a réussi notre code PIN
            depot(identifiant,comptes)
          }
          else if((option_choisi==2)&&(tentativesPin>3))
          {
            retrait(identifiant,comptes)
          }
          else if((option_choisi==3)&&(tentativesPin>3))
          { 
            printf("Le montant disponible sur votre compte est : %.2f CHF    \n", comptes(identifiant))
          }
          else if((option_choisi==4)&&(tentativesPin>3))
          {
            changepin(identifiant,codespin)
          }
          else
          {
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            tentativesPin=3
            premiereFois=true
          }
        }
        else{
          //si on est ici, ca veut dire qu'on a raté 3x le code PIN
          tentativesPin=3
          premiereFois=true
        }
        
      }
      else{
        println("Cet identifiant n’est pas valable.")
      }
    } 
  }


  def depot(id : Int, comptes : Array[Double]) : Unit = 
  {
    devise=0

    /*VERIFICATION DE LA DEVISE*/
    while ((devise!=1)&&(devise!=2))
    {  //tant que la devise du depot rentré est !=1 ou 2
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
      devise = readInt()
    }


    montant=0
    /*VERIFICATION DU MONTANT*/
    do  //Do while utile pour rentrer au moins 1x ici
    {
      println("Indiquez le montant du dépôt > ")  //demander le montant du depot
      montant = readLine().toInt  //lire la valeur

      if(montant%10==0){  //tester la valeur, pour voir si elle est multiple de 10. % indique le modulo, donc si le reste de la divison par 10 est 0, ça veut dire que c'est multiple de 10, donc on ne fait rien
      }
      else{
        println("Le montant doit être un multiple de 10")
      }
    }
    while((montant<10)||(montant%10!=0))

    /*CONVERSION DU DEPOT*/
    if(devise==CHF) {
      comptes(id) +=  montant
    }
    if(devise==EUR)
    {
      comptes(id) += (montant.toDouble)*0.95
    }

    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > %.2f CHF   \n", comptes(id))

  }
  def retrait(id : Int, comptes : Array[Double]) : Unit = 
  {
    var distribution500 = 0
     var distribution200 = 0
     var distribution100 = 0
     var distribution50 = 0
     var distribution20 = 0
     var distribution10 = 0
    var tappez="0"
    var convert_inINT=0

    var montant_avant_retrait = 0
    var coupure_petite_indic=false
    var plafond = comptes(id)/10  //variable du plafond de retrait à ne pas dépasser
    var coup=0

    do{  //demander la devise en int
      println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
      devise=readInt()
    }while((devise!=1)&&(devise!=2))  //il faut qu'elle soit 1 ou 2


    do{  //Demander le montant du retrait en int
      println("Indiquez le montant du retrait: ")
      montant = readInt()
      if(montant%10!=0){  //si il y a un reste à la division par 10, alors pas multiple de 10
        println("Le montant doit être un multiple de 10 >")  //Affichage de la mauvaise valeur
      }
      else if(montant>plafond){  //cas traité ou le plafond est dépassé
        println("Votre plafond de retrait autorisé est de : "+plafond + " CHF.")
      }
      montant_avant_retrait = montant  //on stocke le montant avant le retrait
    }while((montant%10!=0)||(plafond<montant)||(montant<=9))  //il ne faut pas sortir de cette boucle tant qu'on a pas un nombre >=10, multiple de 10 et inférieur au plafond de retrait

    if(devise==CHF){  //si la devise de retrait est en chf, on regarde si le retrait est supérieur à 200 CHF pour peut-être proposé le choix de coupure
      if (montant>=200){
        do{  //on propose le choix de coupure tant que la saisie n'est pas bonne
           coup = readLine("En 1) grosses coupures; 2) petites coupures >").toInt
        }while((coup!=1)&&(coup!=2))
      }
    }
    if(devise==CHF){  //devise en franc suisse
    if (coup==1) {   //si la coupure est GROSSE COUPURE
       if (montant >= 500){  //si le montant restant est supérieur à 500, c'est alors que on peut donner un billet de 500
         print("Il reste "+ montant)  //affichage du montant restant à distribuer
         println(" CHF à distribuer")
         distribution500 = montant/500  //calul du nombre maximum de billets de 500CHF que on va pouvoir donner. Comme demandé dans le sujet de l'exercice, on effectue la division euclidienne 
         print("Vous pouvez obtenir au maximum " + distribution500 + " billet(s) de 500 CHF \n")  //Affichage de ce maximum
       while (sortie==false) {  //tant que la bonne condition n' pas lieu alors:
         tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")  //lire la valeur de o
         if (tappez!="o") {
            convert_inINT = tappez.toInt
           while ((convert_inINT>distribution500)||(convert_inINT<0))
           {
             convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
           }
           distribution500 = convert_inINT
         }
         //ici c'est si la valeur est inferieure ou que l'utilisateur a mis "o"
         montant = montant - (distribution500*500)  //calcul du nouveau montant
         sortie=true  //on peut sortir 
       }
         sortie=false  //pour re rentrer dans la prochaine

       }

        if (montant >= 200){
          print("Il reste "+ montant)
          println(" CHF à distribuer")
         distribution200 = montant/200
          println("Vous pouvez obtenir au maximum " + distribution200 + " billet(s) de 200 CHF")

          while (sortie==false) {
             tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
             if (tappez!="o") {
                convert_inINT = tappez.toInt
               while ((convert_inINT>distribution200)||(convert_inINT<0))
               {
                 convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
               }
               distribution200 = convert_inINT
             }
             montant = montant - (distribution200*200)
             sortie=true
           }
          sortie=false

       }
         if (montant >= 100){
           print("Il reste "+ montant)
           println(" CHF à distribuer")
         distribution100 = montant/100
          println("Vous pouvez obtenir au maximum " + distribution100 + " billet(s) de 100 CHF")


           while (sortie==false) {
              tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
              if (tappez!="o") {
                 convert_inINT = tappez.toInt
                while ((convert_inINT>distribution100)||(convert_inINT<0))
                {
                  convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
                }
                distribution100 = convert_inINT
              }
              montant = montant - (distribution100*100)
              sortie=true
            }
           sortie=false

       }
         if (montant >= 50){
           print("Il reste "+ montant)
           println(" CHF à distribuer")
         distribution50 = montant/50
          println("Vous pouvez obtenir au maximum " + distribution50 + " billet(s) de 50 CHF")


           while (sortie==false) {
              tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
              if (tappez!="o") {
                 convert_inINT = tappez.toInt
                while ((convert_inINT>distribution50)||(convert_inINT<0))
                {
                  convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
                }
                distribution50 = convert_inINT
              }
              montant = montant - (distribution50*50)
              sortie=true
            }
           sortie=false

       }
         if (montant >= 20){
           print("Il reste "+ montant)
           println(" CHF à distribuer")
         distribution20 = montant/20
          println("Vous pouvez obtenir au maximum " + distribution20 + " billet(s) de 20 CHF")

           while (sortie==false) {
              tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
              if (tappez!="o") {
                 convert_inINT = tappez.toInt
                while ((convert_inINT>distribution20)||(convert_inINT<0))
                {
                  convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
                }
                distribution20 = convert_inINT
              }
              montant = montant - (distribution20*20)
              sortie=true
            }
           sortie=false

       }
         if (montant >= 10){
           print("Il reste "+ montant)
           println(" CHF à distribuer")
          distribution10 = montant/10
          montant -= (distribution10 * 10)
       }


       if(distribution500>=1)
        {
         println(s"$distribution500 billet(s) de 500 CHF")
       }
       if(distribution200>=1)
        {
         println(s"$distribution200 billet(s) de 200 CHF")
       }
       if(distribution100>=1)
        {
         println(s"$distribution100 billet(s) de 100 CHF")
       }
       if(distribution50>=1)
        {
         println(s"$distribution50 billet(s) de 50 CHF")
       }
       if(distribution20>=1)
        {
         println(s"$distribution20 billet(s) de 20 CHF")
       }
       if(distribution10>=1)
        {
         println(s"$distribution10 billet(s) de 10 CHF")
       }

      comptes(id) = comptes(id) - montant_avant_retrait
       printf("Votre retrait a été pris en compte,Le nouveau montant disponible sur votre compte est > %.2f CHF\n", comptes(id))

    }

    else if((montant<200)||(coup==2))
    {
    if (montant >= 100){
      print("Il reste "+ montant)
      println(" CHF à distribuer")
       distribution100 = montant/100
        println("Vous pouvez obtenir au maximum " + distribution100 + " billet(s) de 100 CHF")


         while (sortie==false) {
            tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
            if (tappez!="o") {
               convert_inINT = tappez.toInt
              while ((convert_inINT>distribution100)||(convert_inINT<0))
              {
                convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
              }
              distribution100 = convert_inINT
            }
            montant = montant - (distribution100*100)
            sortie=true
          }
      sortie=false

     }
       if (montant >= 50){
         print("Il reste "+ montant)
         println(" CHF à distribuer")
       distribution50 = montant/50
        println("Vous pouvez obtenir au maximum " + distribution50 + " billet(s) de 50 CHF")


         while (sortie==false) {
            tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
            if (tappez!="o") {
               convert_inINT = tappez.toInt
              while ((convert_inINT>distribution50)||(convert_inINT<0))
              {
                convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
              }
              distribution50 = convert_inINT
            }
            montant = montant - (distribution50*50)
            sortie=true
          }
         sortie=false

     }
       if (montant >= 20){
         print("Il reste "+ montant)
         println(" CHF à distribuer")
       distribution20 = montant/20
        println("Vous pouvez obtenir au maximum " + distribution20 + " billet(s) de 20 CHF")

         while (sortie==false) {
            tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
            if (tappez!="o") {
               convert_inINT = tappez.toInt
              while ((convert_inINT>distribution20)||(convert_inINT<0))
              {
                convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
              }
              distribution20 = convert_inINT
            }
            montant = montant - (distribution20*20)
            sortie=true
          }
         sortie=false

        }
       if (montant>=10)
        {
          print("Il reste "+ montant)
          println(" CHF à distribuer")
          distribution10 = montant/10
          montant -= (distribution10 * 10)
        }
          if (distribution100>=1){
            println(s"$distribution100 billet(s) de 100 CHF ")
          }
          if (distribution50>=1){
            println(s"$distribution50 billet(s) de 50 CHF ")
          }
          if (distribution20>=1){
            println(s"$distribution20 billet(s) de 20 CHF " )
          }
          if (distribution10>=1){
            println(s"$distribution10 billet(s) de 10 CHF ")
          }

      comptes(id) = comptes(id) - montant_avant_retrait
       printf("Votre retrait a été pris en compte,Le nouveau montant disponible sur votre compte est > %.2f CHF   \n", comptes(id))

    }
    }
    if (devise==2){
      if (montant >= 100){
         println("Il reste " + montant + " EUR à distribuer")
       distribution100 = montant/100
        println("Vous pouvez obtenir au maximum " + distribution100 + " billet(s) de 100 EUR")

         while (sortie==false) {
            tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
            if (tappez!="o") {
               convert_inINT = tappez.toInt
              while ((convert_inINT>distribution100)||(convert_inINT<0))
              {
                convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
              }
              distribution100 = convert_inINT
            }
            montant = montant - (distribution100*100)
            sortie=true
          }
        sortie=false

      }
       if (montant >= 50){
         println("Il reste " + montant + " EUR à distribuer")
       distribution50 = montant/50
        println("Vous pouvez obtenir au maximum " + distribution50 + " billet(s) de 50 EUR")


         while (sortie==false) {
            tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
            if (tappez!="o") {
               convert_inINT = tappez.toInt
              while ((convert_inINT>distribution50)||(convert_inINT<0))
              {
                convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
              }
              distribution50 = convert_inINT
            }
            montant = montant - (distribution50*50)
            sortie=true
          }
         sortie=false

      }
       if (montant >= 20){
         println("Il reste " + montant + " EUR à distribuer")
       distribution20 = montant/20
        println("Vous pouvez obtenir au maximum " + distribution20 + " billet(s) de 20 EUR")

         while (sortie==false) {
            tappez = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ")
            if (tappez!="o") {
               convert_inINT = tappez.toInt
              while ((convert_inINT>distribution20)||(convert_inINT<0))
              {
                convert_inINT = readLine("Tapez 'o' pour OK ou une autre valeur inférieure à celle proposée > ").toInt
              }
              distribution20 = convert_inINT
            }
            montant = montant - (distribution20*20)
            sortie=true
          }
         sortie=false

        }
       if (montant>=10)
        {
          distribution10 = montant/10
          montant -= (distribution10 * 10)
        }
          if (distribution100>=1){
            println(s"$distribution100 billet(s) de EUR EUR ")
          }
          if (distribution50>=1){
            println(s"$distribution50 billet(s) de 50 EUR ")
          }
          if (distribution20>=1){
            println(s"$distribution20 billet(s) de 20 EUR " )
          }
          if (distribution10>=1){
            println(s"$distribution10 billet(s) de 10 EUR ")
          }

      comptes(id) = comptes(id) - 0.95*montant_avant_retrait

         printf("Votre retrait a été pris en compte,Le nouveau montant disponible sur votre compte est > %.2f EUR    \n", comptes(id))
    }

  }
  def changepin(id : Int, codespin : Array[String]) : Unit = 
  {
    var codepin: String = ""
    do{
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      codepin = readLine()  //read de l'entrée de l'utilisateur
      if(codepin.length>=8){}  //si la taille du code pin est supérieure ou égale à 8 alors on sort de la boucle
      else println("Votre code pin ne contient pas au moins 8 caractères")  //sinon on affiche un message d'erreur  
    }while(codepin.length<8)
    println("Code pin a été modifié avec succès")
    codespin(id) = codepin  //changement du code pin dans le tableau
  }
}
