//Assignment: Mohamed Abdellah Tabti_996379_assignsubmission_file

import scala.io.StdIn.{readInt, readLine}


object Main {

  //variable qui stocke le taux de change
  val EUR_TO_CHF = 0.95
  var CHF_TO_EUR = 1.05

  def main(args: Array[String]): Unit = {


    //tant que le le client est connecté on continue la boucle principale c'est à dire tant que l'utilisateur n'a pas
    //appuyer sur terminer
    var enMarche = true
    var nombreClient = 100

    //***********VARIABLES DE MOT DE PASSE***********
    // on initialise le tableau des codes pins avec des valeurs par défaut
    var codepins = Array.fill(nombreClient)("INTRO1234")

    //***********VARIABLES DE COMPTE***********
    //on initialise le tableau des comptes avec des valeurs par défaut
    var comptes = Array.fill(nombreClient)(1200.0)


    while (enMarche) {



      var idClient = 0
      var isConnected = false

      // cette boucle permet de gérer la connexion
      // tant que le client n'est pas connecté et que le programme est en marche on continue la boucle
      // pour être connecté il faut que le client ait rentré un id correct et un pin correct
      while(!isConnected && enMarche){ // IDENTIFICATION (ID + PIN) ----------------------------------------------------------------

        var idClientValid = false

        while (!idClientValid && enMarche) { // ID ------------------------------------
          println("Saisissez votre code identifiant >")
          idClient = readInt()
          // on vérifie si l'id est correct en le comparant avec l'id stocké dans le tableau des comptes
          if (idClient >= 0 && idClient < nombreClient) {
            idClientValid = true
          } else {
            println("Cet identifiant n’est pas valable")
            enMarche = false
          }
        }

        var tentatives = 3
        var pinCorrect = false
        if(enMarche){

          // tant que le pin n'est pas correct et que le nombre de tentative est supérieur à 0 on continue la boucle
          while (tentatives > 0 && !pinCorrect) { // PIN ------------------------------------
            println("Saisissez votre code pin >")
            val pin = scala.io.StdIn.readLine()
            // on vérifie si le pin est correct en le comparant avec le pin stocké dans le tableau des codes pins
            if (pin == codepins(idClient)) {
              pinCorrect = true // pour sortir de la boucle identification
              isConnected = true // pour passer à la suite du programme
            } else {
              tentatives -= 1
              if (tentatives == 0) {
                println("Trop d’erreurs, abandon de l’identification")
              } else {
                println("Code pin erroné, il vous reste " + tentatives + " tentatives")
              }
            }
          }
        }
      }

      var choixDuUser = 0

      // boucle principale du programme
      // tant que le choix de l'utilisateur est différent de 5 et que le programme est en marche on continue la boucle
      while(choixDuUser != 5 && enMarche){
        println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix :")
        //variable qui stocke le choix de l'utilisateur
        choixDuUser = scala.io.StdIn.readInt()

        if (choixDuUser == 1) { // DEPOT --------------------------------------------------------------------------------

          depot(idClient,comptes)

        } else if (choixDuUser == 2) { // RETRAIT --------------------------------------------------------------------------------

          retrait(idClient,comptes)

        } else if (isConnected && choixDuUser == 3) { // CONSULTATION DU COMPTE ----------------------------------------------------------
          println("Vous avez choisi la consultation du compte")


          // utiliser printf plutot que println
          printf("Le montant disponible sur votre compte est de %.2f CHF \n", comptes(idClient))

        }else if(isConnected && choixDuUser == 4){


          changepin(idClient,codepins)


        }

      }

      //à ce moment la si le pin n'est pas réussi alors le programme devrait s'arrêter
      if ( choixDuUser==5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")        
        isConnected = false
      }

    }

  }



  def depot(id : Int, comptes : Array[Double]) : Unit = {
    println("Vous avez choisi le dépôt")

    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    var currency = scala.io.StdIn.readInt()

    while (currency != 1 && currency != 2) {
      println("Choix invalide")
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      currency = scala.io.StdIn.readInt()
    }

    println("Indiquez le montant du dépôt >")
    var amount = scala.io.StdIn.readInt()
    // le modulo permet de vérifier si le nombre est un multiple de 10
    //tant que le montant n'est pas un multiple de 10 on redemande à l'utilisateur de rentrer un montant
    while (amount % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      amount = scala.io.StdIn.readInt()
    }
    if (currency == 2) comptes(id) += (amount * EUR_TO_CHF)
    if (currency == 1) comptes(id) += amount
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    println("Vous avez choisi le retrait")

    println("Indiquez la devise :1) CHF, 2) EUR >")
    var currency = scala.io.StdIn.readInt()

    while (currency != 1 && currency != 2) {
      println("Choix invalide")
      println("Indiquez la devise : 1) CHF ; 2) EUR >")
      currency = scala.io.StdIn.readInt()
    }

    println("Indiquez le montant du retrait >")
    var amount = scala.io.StdIn.readInt()

    // tant que le montant n'est pas un multiple de 10 ou que le montant est supérieur au plafond de retrait on
    // redemande à l'utilisateur de rentrer un montant
    var plafond = 0.0
    if (currency == 1) plafond = comptes(id) * 0.1
    if (currency == 2) plafond = comptes(id) * 0.1 * CHF_TO_EUR
    while (amount % 10 != 0 || amount > plafond) {
      if (amount % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      } else {
        println(f"Votre plafond de retrait autorisé est de : ${plafond}%.2f")
      }
      amount = scala.io.StdIn.readInt()
    }
    // maintenant on a la devise ainsi que le montant du retrait
    //il  y a 3 scenario possible
    // CHF grosses coupures
    // CHF petites coupures
    // EUR petites coupures

    if (currency == 1) { // RETRAIT EN CHF -------------------------------------------------------------------------

      var coupures = 2 // on initialise la variable coupures à 2 (CHF) car si le montant est inférieur à 200 on ne
      // rentre pas dans la boucle et on ne change pas la valeur de la variable coupures

      //tant que l'utilisateur n'a pas choisi 1 ou 2 on redemande à l'utilisateur de rentrer un montant
      if (amount >= 200) {

        println("En 1) grosses coupures, 2) petites coupures >")
        coupures = scala.io.StdIn.readInt()

        while (coupures != 1 && coupures != 2) {
          println("Choix invalide")
          println("En 1) grosses coupures, 2) petites coupures >")
          coupures = scala.io.StdIn.readInt()
        }
      }

      //le reste est le montant restant à distribuer
      var reste = amount
      var nb500 = 0
      var nb200 = 0
      var nb100 = 0
      var nb50 = 0
      var nb20 = 0
      var nb10 = 0

      //tant que ce reste est supérieur à 0 on continue la boucle car il reste de l'argent à distribuer
      while (reste > 0) {

        println("il reste " + reste + " à distribuer")

        var choisi = false

        if (reste >= 500 && coupures == 1) {

          println(s"Vous pouvez obtenir au maximum ${reste / 500} billet(s) de 500 CHF")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 500)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb500 += reste / 500
            reste -= nb500 * 500
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 500
            nb500 += input.toInt
          }


        }

        if (reste >= 200 && !choisi && coupures == 1) {

          println(s"Vous pouvez obtenir au maximum ${reste / 200} billet(s) de 200 CHF")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 200)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb200 += reste / 200
            reste -= nb200 * 200
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 200
            nb200 += input.toInt
          }

        }

        if (reste >= 100 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 100} billet(s) de 100 CHF")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 100)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb100 += reste / 100
            reste -= nb100 * 100
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 100
            nb100 += input.toInt
          }

        }

        if (reste >= 50 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 50} billet(s) de 50 CHF")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 50)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb50 += reste / 50
            reste -= nb50 * 50
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 50
            nb50 += input.toInt
          }

        }

        if (reste >= 20 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 20} billet(s) de 20 CHF")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 20)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb20 += reste / 20
            reste -= nb20 * 20
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 20
            nb20 += input.toInt
          }

        }

        if (reste >= 10 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 10} billet(s) de 10 CHF")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 10)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb10 += reste / 10
            reste -= nb10 * 10
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 10
            nb10 += input.toInt
          }

        }

      }

      //affichage des billets
      println("Veuillez retirer la somme demandée :")
      if (nb500 != 0) println(nb500 + " billet(s) de 500 CHF")
      if (nb200 != 0) println(nb200 + " billet(s) de 200 CHF")
      if (nb100 != 0) println(nb100 + " billet(s) de 100 CHF")
      if (nb50 != 0) println(nb50 + " billet(s) de 50 CHF")
      if (nb20 != 0) println(nb20 + " billet(s) de 20 CHF")
      if (nb10 != 0) println(nb10 + " billet(s) de 10 CHF")


      // on met à jour le solde du compte
      comptes(id) -= amount
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
    }

    if (currency == 2) { // RETRAIT EN EUR ---------------------------------------------------------------------------

      //le reste est le montant restant à distribuer
      var reste = amount
      var nb100 = 0
      var nb50 = 0
      var nb20 = 0
      var nb10 = 0


      //tant que ce reste est supérieur à 0 on continue la boucle car il reste de l'argent à distribuer
      while (reste > 0) {

        println("il reste " + reste + " à distribuer")

        var choisi = false

        if (reste >= 100) {

          println(s"Vous pouvez obtenir au maximum ${reste / 100} billet(s) de 100 EUR")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 100)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb100 += reste / 100
            reste -= nb100 * 100
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 100
            nb100 += input.toInt
          }

        }

        if (reste >= 50 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 50} billet(s) de 50 EUR")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 50)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb50 += reste / 50
            reste -= nb50 * 50
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 50
            nb50 += input.toInt
          }

        }

        if (reste >= 20 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 20} billet(s) de 20 EUR")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 20)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb20 += reste / 20
            reste -= nb20 * 20
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 20
            nb20 += input.toInt
          }

        }

        if (reste >= 10 && !choisi) {

          println(s"Vous pouvez obtenir au maximum ${reste / 10} billet(s) de 10 EUR ")
          var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

          //tant que le input n'est pas o ou une valeur numérique inférieure à celle proposée on redemande à l'utilisateur
          //de rentrer un input
          while (input.toLowerCase != "o" && input.toInt > (reste / 10)) {
            println("Choix invalide")
            input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          if (input.toLowerCase == "o") { // on choisi le nombre maximum
            nb10 += reste / 10
            reste -= nb10 * 10
            choisi = true
          } else if (input.toInt != 0) { // on choisi un nombre inférieur au maximum
            choisi = true
            reste -= input.toInt * 10
            nb10 += input.toInt
          }

        }

      }

      //affichage des billets
      println("Veuillez retirer la somme demandée :")
      if (nb100 != 0) println(nb100 + " billet(s) de 100 EUR")
      if (nb50 != 0) println(nb50 + " billet(s) de 50 EUR")
      if (nb20 != 0) println(nb20 + " billet(s) de 20 EUR")
      if (nb10 != 0) println(nb10 + " billet(s) de 10 EUR")

      // on met à jour le solde du compte
      comptes(id) -= amount * EUR_TO_CHF
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
    }
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var nouveauPin = scala.io.StdIn.readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")

    while (nouveauPin.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères")
      nouveauPin = scala.io.StdIn.readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    }

    codespin(id) = nouveauPin
  }


}
