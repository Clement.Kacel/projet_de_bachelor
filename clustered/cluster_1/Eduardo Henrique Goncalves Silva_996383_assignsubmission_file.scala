//Assignment: Eduardo Henrique Goncalves Silva_996383_assignsubmission_file


import io.StdIn._

object Main {

  // PHASE IDENTIFICATION + CHOIX DANS LE MENU //

  val nbclients = 100
  var codepin : Array[String] = Array.fill(nbclients)("INTRO1234")
  var index = 0
  var id = 0
  var soldes: Array[Double] = Array.fill(nbclients)(1200.0) // Solde de base du compte
  var identifiant : Array[Int] = Array.fill(nbclients){index += 1; index}
  var tentativeratés = 0
  var tentativesrest = 0
  var vérif = ""
  var rénitialisation = 0
  var validationf = 0 // Garde en mémoire l'entrée du code pin
  var opération = 0 // Choix de l'opération que l'on souhaite réaliser
  var menu = 0 // Pour revenir au menu à travers les méthodes
  var solde : Double = 0
  var pinok = 0
  var NVPIN = ""


  // CODE PIN //

  def verification(): Unit = {
    while (((validationf != 1) && (tentativeratés < 3))) {
      vérif = readLine("Saisissez votre code pin > ")
      if (vérif != codepin(id)) {
        tentativeratés += 1
        if (tentativeratés == 3) {
          opération = 5
          rénitialisation = 1
          println("Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte.")
        }
        else {
          tentativesrest = 3 - tentativeratés
          println(" Code pin erroné, il vous reste " + tentativesrest + " tentative(s)")

        }
      }
      else if (vérif == codepin(id)) {
        validationf = 1
      }
    }
  }

      //TERMINER LES OPERATIONS//

      def terminer(): Unit = {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte")
      }


      // DEPÔT //

      def dêpot(id : Int, Soldes : Array[Double]) : Unit = {

        println("\n Veuillez saisir un montant étant un multiple de 10. \n")
        val devise = readLine("Indiquez la devise du dépôt : \n1) CHF \n2) EUR \n \n >> ").toInt

        if (devise == 1){

          var dépôt = readLine("Indiquez le montant du dépôt en CHF >> ").toDouble
          if (dépôt >= 0) {
            if (dépôt % 10 == 0) {
              soldes(id) += dépôt
              printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF. \n", soldes(id))
            }
            else println("ERREUR 1 : Veuillez saisir un montant étant un multiple de 10.")
          }
          else println("Veuillez entrer une valeur positive.")
        }
        else if (devise == 2) {
          val dépôt = readLine("Indiquez le montant du dépôt en EUR >> ").toDouble
          if (dépôt >= 0) {

            if (dépôt % 10 == 0) {
              soldes(id) += dépôt * 0.95
              printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF. \n", soldes(id))
            }
            else println("ERREUR 1 : Veuillez saisir un montant étant un multiple de 10.")
          }
          else println("Veuillez entrer une valeur positive.")
        }

      }
  def changedepin(id : Int, codepin : Array[String]) : Unit = {
    while (pinok != 1){
      NVPIN = readLine("Veuillez indiquer le nouveau code pin souhaité >> ")
      if (NVPIN.length <= 7){
        println("Erreur 4 : Attention le code pin soit contenir au minimum 8 caractères.")
      }
      else {
        pinok = 1
        codepin(id) = NVPIN
        println("Le nouveau code pin à bien été effectué pour le compte " + id + ".")
      }
    }


  }

      // RETRAIT //

  def retrait(id : Int, Soldes : Array[Double]) : Unit = {

        println("\nLe montant du retrait doit être une valeur étant un multiple de 10. \n \n")

        var devise = "" // pour faire le choix même si on se trompe //
        var BILLET500 = 0
        var BILLET200 = 0
        var BILLET100 = 0 // Comptage des billets à distribuer.
        var BILLET50 = 0
        var BILLET20 = 0
        var BILLET10 = 0
        var quantité_billets = ""
        var reste_retrait = 0
        var coupure = ""


        while ((devise != "1") && (devise != "2")) {
          devise = readLine("Indiquez la devise du retrait : \n1) CHF \n2) EUR \n \n >> ")
        }


        if (devise == "1") { // partie en CHF //

          val plafond = soldes(id) * 0.10
          println("Votre plafond de retrait autorisé est de : " + plafond + " CHF.")
          var montant_retrait = readLine("Indiquez le montant du retrait en CHF >> ").toInt


          if (montant_retrait % 10 == 0) {
            if (montant_retrait <= plafond) {

              while (montant_retrait >= 10) {

                if (montant_retrait >= 200){coupure = readLine("En 1) grosses coupures, 2) petites coupures > ")}
                else{coupure = "2"}

                if (coupure == "1") {

                  if (montant_retrait >= 500) { // BILLET 500
                    BILLET500 += (montant_retrait / 500)
                    reste_retrait -= BILLET500 * 500


                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET500 + " billets(s) de 500 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (500 * BILLET500)
                      soldes(id) -= BILLET500 * 500

                    }
                    else {
                      while (quantité_billets.toInt > BILLET500) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 500

                        BILLET500 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 500
                      }
                    }
                  }


                  if (montant_retrait >= 200) { // BILLET 500
                    BILLET200 += (montant_retrait / 200)
                    reste_retrait -= BILLET200 * 200


                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET200 + " billets(s) de 200 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (200 * BILLET200)
                      soldes(id) -= BILLET200 * 200

                    }
                    else {
                      while (quantité_billets.toInt > BILLET200) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 200
                        BILLET200 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 200
                      }
                    }
                  }
                  if (montant_retrait >= 100) { // BILLET 100
                    BILLET100 += (montant_retrait / 100)
                    reste_retrait -= BILLET100 * 100

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET100 + " billet(s) de 100 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (100 * BILLET100)
                      soldes(id) -= BILLET100 * 100

                    }
                    else {
                      while (quantité_billets.toInt > BILLET100) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 100
                        BILLET100 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 100
                      }
                    }
                  }


                  if (montant_retrait >= 50) { // BILLET 50
                    BILLET50 += montant_retrait / 50
                    reste_retrait -= BILLET50 * 50

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET50 + " billet(s) de 50 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (50 * BILLET50)
                      soldes(id) -= BILLET50 * 50

                    }
                    else {
                      while (quantité_billets.toInt > BILLET50) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 50
                        BILLET50 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 50
                      }
                    }
                  }

                  if (montant_retrait >= 20) { // BILLET 20
                    BILLET20 += montant_retrait / 20
                    reste_retrait -= BILLET20 * 20

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET20 + " billet(s) de 20 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (20 * BILLET20)
                      soldes(id) -= BILLET20 * 20

                    }
                    else {
                      while (quantité_billets.toInt > BILLET20) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 20
                        BILLET20 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 20
                      }
                    }
                  }

                  if (montant_retrait >= 10) { // BILLET 10
                    BILLET10 += montant_retrait / 10
                    montant_retrait -= BILLET10 * 10
                    reste_retrait -= BILLET10 * 10
                    soldes(id) -= BILLET10 * 10

                    println("Il reste " + montant_retrait + " EUR à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET10 + " billet(s) de 10 EUR.")
                  }
                }

                if (coupure == "2") {

                  if (montant_retrait >= 100) { // BILLET 100
                    BILLET100 += (montant_retrait / 100)
                    reste_retrait -= BILLET100 * 100

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET100 + " billet(s) de 100 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (100 * BILLET100)
                      soldes(id) -= BILLET100 * 100

                    }
                    else {
                      while (quantité_billets.toInt > BILLET100) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 100
                        BILLET100 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 100
                      }
                    }
                  }


                  if (montant_retrait >= 50) { // BILLET 50
                    BILLET50 += montant_retrait / 50
                    reste_retrait -= BILLET50 * 50

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET50 + " billet(s) de 50 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (50 * BILLET50)
                      soldes(id) -= BILLET50 * 50

                    }
                    else {
                      while (quantité_billets.toInt > BILLET50) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 50
                        BILLET50 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 50
                      }
                    }
                  }

                  if (montant_retrait >= 20) { // BILLET 20
                    BILLET20 += montant_retrait / 20
                    reste_retrait -= BILLET20 * 20

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET20 + " billet(s) de 20 CHF.")
                    quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                    if (quantité_billets == "o") {
                      montant_retrait -= (20 * BILLET20)
                      soldes(id) -= BILLET20 * 20

                    }
                    else {
                      while (quantité_billets.toInt > BILLET20) {
                        quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                      }
                      {
                        montant_retrait -= quantité_billets.toInt * 20
                        BILLET20 = quantité_billets.toInt
                        soldes(id) -= quantité_billets.toInt * 20
                      }
                    }
                  }

                  if (montant_retrait >= 10) { // BILLET 10
                    BILLET10 += montant_retrait / 10
                    montant_retrait -= BILLET10 * 10
                    soldes(id) -= BILLET10 * 10

                    println("Il reste " + montant_retrait + " CHF à distribuer.")
                    println("Vous pouvez obtenir au maximum " + BILLET10 + " billet(s) de 10 CHF.")
                  }
                }

                println("\n Veuillez retirer la somme demandée :\n ")

                if (BILLET500 >= 1) {println(BILLET500 + " billet(s) de 500 CHF.")}
                if (BILLET200 >= 1) {println(BILLET200 + " billet(s) de 200 CHF.")}
                if (BILLET100 >= 1) {println(BILLET100 + " billet(s) de 100 CHF.")}
                if (BILLET50 >= 1) {println(BILLET50 + " billet(s) de 50 CHF.")}
                if (BILLET20 >= 1) {println(BILLET20 + " billet(s) de 20 CHF.")}
                if (BILLET10 >= 1) {println(BILLET10 + " billet(s) de 10 CHF.")}
              }
            }
            else println("ERREUR 2 : Veuillez saisir une valeur de retrait respectant le plafond actuel de votre compte.")
          }
          else println("ERREUR 1 : Veuillez saisir un montant étant un multiple de 10.")
        }


        if (devise == "2") { // partie en EUR //

          val plafond = soldes(id) * 0.10
          println("Votre plafond de retrait autorisé est de : " + plafond + " EUR.")
          var montant_retrait = readLine("Indiquez le montant du retrait en EUR >> ").toInt


          if (montant_retrait % 10 == 0) {
            if (montant_retrait <= plafond) {

              while (montant_retrait >= 10) {

                if (montant_retrait >= 100) { // BILLET 100
                  BILLET100 += (montant_retrait / 100)
                  reste_retrait -= BILLET100 * 100

                  println("Il reste " + montant_retrait + " EUR à distribuer.")
                  println("Vous pouvez obtenir au maximum " + BILLET100 + " billet(s) de 100 EUR.")
                  quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                  if (quantité_billets == "o") {
                    montant_retrait -= (100 * BILLET100)
                    soldes(id) -= (BILLET100 * 100)*0.95
                  }
                  else {
                    while (quantité_billets.toInt > BILLET100) {
                      quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                    }
                    {
                      montant_retrait -= quantité_billets.toInt * 100
                      BILLET100 = quantité_billets.toInt
                      soldes(id) -= (quantité_billets.toInt * 100)*0.95
                    }
                  }
                }

                if (montant_retrait >= 50) { // BILLET 50
                  BILLET50 += montant_retrait / 50
                  reste_retrait -= BILLET50 * 50

                  println("Il reste " + montant_retrait + " EUR à distribuer.")
                  println("Vous pouvez obtenir au maximum " + BILLET50 + " billet(s) de 50 EUR.")
                  quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                  if (quantité_billets == "o") {
                    montant_retrait -= (50 * BILLET50)
                    soldes(id) -= (BILLET50 * 50)*0.95
                  }
                  else {
                    while (quantité_billets.toInt > BILLET50) {
                      quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                    }
                    {
                      montant_retrait -= quantité_billets.toInt * 50
                      BILLET50 = quantité_billets.toInt
                      soldes(id) -= (quantité_billets.toInt * 50)*0.95
                    }
                  }
                }

                if (montant_retrait >= 20) { // BILLET 20
                  BILLET20 += montant_retrait / 20
                  reste_retrait -= BILLET20 * 20

                  println("Il reste " + montant_retrait + " EUR à distribuer.")
                  println("Vous pouvez obtenir au maximum " + BILLET20 + " billet(s) de 20 EUR.")
                  quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")

                  if (quantité_billets == "o") {
                    montant_retrait -= (20 * BILLET20)
                    soldes(id) -= (BILLET20 * 20)*0.95

                  }
                  else {
                    while (quantité_billets.toInt > BILLET20) {
                      quantité_billets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée. >> ")
                    }
                    {
                      montant_retrait -= quantité_billets.toInt * 20
                      BILLET20 = quantité_billets.toInt
                      soldes(id) -= (quantité_billets.toInt * 20)*0.95
                    }
                  }
                }

                if (montant_retrait >= 10) { // BILLET 10
                  BILLET10 += montant_retrait / 10
                  montant_retrait -= BILLET10*10
                  reste_retrait -= BILLET10 * 10
                  soldes(id) -= (BILLET10 * 10)*0.95

                  println("Il reste " + montant_retrait + " EUR à distribuer.")
                  println("Vous pouvez obtenir au maximum " + BILLET10 + " billet(s) de 10 EUR.")
                }

                println("\n Veuillez retirer la somme demandée :\n ")

                if (BILLET500 >= 1) {println(BILLET500 + " billet(s) de 500 EUR.")}
                if (BILLET200 >= 1) {println(BILLET200 + " billet(s) de 200 EUR.")}
                if (BILLET100 >= 1) {println(BILLET100 + " billet(s) de 100 EUR.")}
                if (BILLET50 >= 1) {println(BILLET50 + " billet(s) de 50 EUR.")}
                if (BILLET20 >= 1) {println(BILLET20 + " billet(s) de 20 EUR.")}
                if (BILLET10 >= 1) {println(BILLET10 + " billet(s) de 10 EUR.")}



              }
            }
            else println("ERREUR 2 : Veuillez saisir une valeur de retrait respectant le plafond actuel de votre compte.")
          }
          else println("ERREUR 1 : Veuillez saisir un montant étant un multiple de 10.")
        }
  }

  def main(args: Array[String]): Unit = {
    while (opération != 5) {
      while (menu == 0) {
        id = readLine("Indiquez votre identifiant >> ").toInt
        if (identifiant.indexOf(id) == -1) {
          println("Cet identifiant n'est pas valable.")
        }
        else {
          verification()
          if (validationf == 1) {
            menu = 1
          }
          else {
            validationf = 0
            tentativeratés = 0
          }
        }
      }


      // AFFICHAGE DU MENU//

      while ((opération != 1) && (opération != 2) && (opération != 3) && (opération != 4) && (opération != 5)) {
        opération = readLine("\n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changer le code pin \n5) Terminer \nQuel opération voulez vous effectuer ? >> ").toInt
      }

      // Dêpot //
      if((opération == 1) && (validationf == 1)){
        println("\n--------------Phase de dépôt-------------- \n")
        dêpot(id, soldes)
        opération = 0
      }

      // Retrait //
      if((opération == 2) && (validationf == 1)){
        println("\n--------------Phase de Retrait-------------- \n")
        retrait(id, soldes)
        opération = 0
      }

      // CONSULTATION DU COMPTE //
      if ((opération == 3) && (validationf == 1)){
        println("\n--------------Consultation du compte " + id + " --------------\n")
        println("Le montant disponible sur votre compte est de : " + soldes(id))
        opération = 0
      }

      if ((opération == 4) && (validationf == 1)){
        println("\n -------- Changement du code pin --------\n")
        changedepin(id, codepin)
        opération = 0
      }

      if ((opération == 5) && (validationf == 1)){
        terminer()
        id = 0
        validationf = 0
        opération = 0
        menu = 0
        tentativeratés = 0
      }
    }
  }
}



