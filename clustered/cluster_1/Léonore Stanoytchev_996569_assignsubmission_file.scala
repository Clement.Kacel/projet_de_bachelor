//Assignment: Léonore Stanoytchev_996569_assignsubmission_file

import io.StdIn._
object Main {
  val tauxdechangedeCHFaEUR = 0.95
  val tauxdechangedeEURaCHF = 1.05
  var devise = 0
  def main(args: Array[String]): Unit = {
    var choix = 0
    var nbclients=100
    var codespin = Array.fill(nbclients)("INTRO1234")
    var test = "0"
    var testpin = false
    var erreurPin = false
    var tentatives = 3
    var identifiant_change=true
    var identifiant=0
    var comptes:Array[Double] = Array.fill(nbclients)(1200.00)

     while(identifiant<nbclients && identifiant>=0)
    {
      tentatives = 3
      if(identifiant_change==true)
      {
        testpin=false
        println("Saisissez votre code identifiant > ")
        identifiant = readInt()
        if(identifiant<nbclients && identifiant>=0)
        {
          if (testpin == false) {
             do {
              test = readLine ("Saisissez votre code pin : ").toString
             if (test == codespin(identifiant)) {
               testpin = true
               identifiant_change = false
             }else{ // pin est faux
               tentatives = tentatives -1
               if(tentatives == 2 || tentatives == 1) {
                 println ("Code pin erroné, il vous reste " + tentatives +" tentatives")
               }else{
                 println("Trop d’erreurs, abandon de l’identification")
                 erreurPin = true
               }
             }
             }while (testpin == false && (tentatives == 1 || tentatives == 2  || tentatives == 3))
           }
        }
        else{
          println("Cet identifiant n’est pas valable.")  //puis il faut sortir du programme
        }
      }
      if(identifiant_change==false){
        println("Choisissez votre opération :" + '\n'+ "  1) Dépôt"+ '\n'+ "  2) Retrait" + '\n'+"  3) Consultation du compte"+ '\n'+ "  4) Changement du code pin\n5) Terminer")
        do {
         choix = readLine("Votre choix : ").toInt
        }while(!(choix == 1 || choix == 2 || choix == 3 || choix == 4 || choix==5))  // On vérifie que le choix est bien un nombre entre 1 et 5
           // DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT DEPOT 
         if (choix == 1) {
           depot(identifiant,comptes)
         } // RETRAIT RETRAIT RETRAIT RETRAIT RETRAIT RETRAIT RETRAIT RETRAIT RETRAIT 
         else if(choix == 2) {
           retrait(identifiant,comptes)
         }
         //  CONSULTATION CONSULTATION CONSULTATION CONSULTATION CONSULTATION
         else if (choix ==3) {
           consult(identifiant,comptes)
         } else if(choix==4){ // choix = 4
           changepin(identifiant,codespin)
         }
        else if(choix==5){
          //terminer
          identifiant_change=true
        }
      }
     }

  }

  def consult(id:Int,comptes:Array[Double]) : Unit = 
  {
    //AFFICHAGE DU SOLDE BANCAIRE DU COMPTE NUMERO ID
     printf ("Le montant disponible sur votre compte est de : %.2f", comptes(id)) 
     println (" CHF")
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = 
  {
    var retraitCHF = 0
    var retraitEUR = 0
    var coupure = 0
    var choixdecoupures = false
    var nb500 = 0
    var choix500 = "false"
    var choix500int = 0
    var nb200 = 0
    var choix200 = "false"
    var choix200int = 0
    var nb100 = 0
    var choix100 = "false"
    var choix100int = 0
    var nb50 = 0
    var choix50 = "false"
    var choix50int = 0
    var nb20 = 0
    var choix20 = "false"
    var choix20int = 0
    var nb10 = 0
    var choix10 = "false"


       choixdecoupures = false
       do {
          devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR : ").toInt
        }while(!( (devise ==1) || (devise == 2) )) 
       if(devise == 1) { // On veut retirer en CHF
         do { 
           retraitCHF = readLine("Indiquez le montant du retrait : ").toInt
         } while(!((retraitCHF % 10 == 0) && (retraitCHF <= 0.1*comptes(id)))) 
         comptes(id) = comptes(id) - retraitCHF
         if (retraitCHF>= 200){
           do{
             coupure = readLine ("En 1) grosses coupures, 2) petites coupures : ").toInt
           }while (!(coupure ==1 || coupure == 2))
           if (coupure == 1){
             if (retraitCHF / 500 >= 1){
               println("Il reste "+retraitCHF+" CHF à distribuer")
               nb500 = retraitCHF / 500
               println("Vous pouvez obtenir au maximum "+ nb500 +" billet(s) de 500 CHF")
               choix500 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
               if (choix500 == "o"){
                 retraitCHF = retraitCHF - nb500 * 500
                 choixdecoupures = true
               }else if (choix500 != "o") {
                 choix500int = choix500.toInt
                 while(choix500int < 0 || choix500int > nb500-1){
                   choix500 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                   choix500int = choix500.toInt
                 }
                 retraitCHF = retraitCHF - choix500int * 500
                 nb500 = choix500int
                 if(choix500int != 0){choixdecoupures = true}

               }
             }
             if (retraitCHF / 200 >= 1){
                println("Il reste "+retraitCHF+" CHF à distribuer")
                nb200 = retraitCHF / 200
                println("Vous pouvez obtenir au maximum "+ nb200 +" billet(s) de 200 CHF")
                choix200 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                if (choix200 == "o"){
                  retraitCHF = retraitCHF - nb200 * 200
                  choixdecoupures = true
                }else if (choix200 != "o") {
                  choix200int = choix200.toInt
                   while(choix200int < 0 || choix200int > nb200-1){
                     choix200 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                     choix200int = choix200.toInt
                   }
                  retraitCHF = retraitCHF - choix200int * 200
                  nb200 = choix200int
                  if(choix200int != 0){choixdecoupures = true}
                }
              }
             if (retraitCHF / 100 >= 1){
                println("Il reste "+retraitCHF+" CHF à distribuer")
                nb100 = retraitCHF / 100
                println("Vous pouvez obtenir au maximum "+ nb100 +" billet(s) de 100 CHF")
                choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                if (choix100 == "o"){
                  retraitCHF = retraitCHF - nb100 * 100
                  choixdecoupures = true
                }else if (choix100 != "o") {
                  choix100int = choix100.toInt
                   while(choix100int < 0 || choix100int > nb100-1){
                     choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                     choix100int = choix100.toInt
                   }
                  retraitCHF = retraitCHF - choix100int * 100
                  nb100 = choix100int
                  if(choix100int != 0){choixdecoupures = true}
                }
              }
             if (retraitCHF / 50 >= 1){
                println("Il reste "+retraitCHF+" CHF à distribuer")
                nb50 = retraitCHF / 50
                println("Vous pouvez obtenir au maximum "+ nb50 +" billet(s) de 50 CHF")
                choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                if (choix50 == "o"){
                  retraitCHF = retraitCHF - nb50 * 50
                  choixdecoupures = true
                }else if (choix50 != "o") {
                choix50int = choix50.toInt
                while(choix50int < 0 || choix50int > nb50-1){
                  choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                  choix50int = choix50.toInt
                }
                retraitCHF = retraitCHF - choix50int * 50
                nb50 = choix50int
                  if(choix50int != 0){choixdecoupures = true}
                }
              }
             if (retraitCHF / 20 >= 1){
                println("Il reste "+retraitCHF+" CHF à distribuer")
                nb20 = retraitCHF / 20
                println("Vous pouvez obtenir au maximum "+ nb20 +" billet(s) de 20 CHF")
                choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                if (choix20 == "o"){
                  retraitCHF = retraitCHF - nb20 * 20
                  choixdecoupures = true
                }else if (choix20 != "o") {
                choix20int = choix20.toInt
                while(choix20int < 0 || choix20int > nb20-1){
                  choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                  choix20int = choix20.toInt
                }
                retraitCHF = retraitCHF - choix20int * 20
                nb20 = choix20int
                if(choix20int != 0) {choixdecoupures = true}
                }
              }
             if (retraitCHF / 10 >= 1){
                  println("Il reste "+retraitCHF+" CHF à distribuer")
                  nb10 = retraitCHF / 10
                  retraitCHF = retraitCHF - nb10 * 10

             }

            }else{ // petite coupures chf

             if (retraitCHF / 100 >= 1){
               println("Il reste "+retraitCHF+" CHF à distribuer")
               nb100 = retraitCHF / 100
               println("Vous pouvez obtenir au maximum "+ nb100 +" billet(s) de 100 CHF")
               choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
               if (choix100 == "o"){
                 retraitCHF = retraitCHF - nb100 * 100
                 choixdecoupures = true
               }else if (choix100 != "o") {
                 choix100int = choix100.toInt
                  while(choix100int < 0 || choix100int > nb100-1){
                    choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                    choix100int = choix100.toInt
                  }
                 retraitCHF = retraitCHF - choix100int * 100
                 nb100 = choix100int
                 if(choix100int != 0){choixdecoupures = true}
               }
             }
             if (retraitCHF / 50 >= 1){
               println("Il reste "+retraitCHF+" CHF à distribuer")
               nb50 = retraitCHF / 50
               println("Vous pouvez obtenir au maximum "+ nb50 +" billet(s) de 50 CHF")
               choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
               if (choix50 == "o"){
                 retraitCHF = retraitCHF - nb50 * 50
                 choixdecoupures = true
               }else if (choix50 != "o") {
               choix50int = choix50.toInt
               while(choix50int < 0 || choix50int > nb50-1){
                 choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                 choix50int = choix50.toInt
               }
               retraitCHF = retraitCHF - choix50int * 50
               nb50 = choix50int
                 if(choix50int != 0){choixdecoupures = true}
               }
             }
             if (retraitCHF / 20 >= 1){
               println("Il reste "+retraitCHF+" CHF à distribuer")
               nb20 = retraitCHF / 20
               println("Vous pouvez obtenir au maximum "+ nb20 +" billet(s) de 20 CHF")
               choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
               if (choix20 == "o"){
                 retraitCHF = retraitCHF - nb20 * 20
                 choixdecoupures = true
               }else if (choix20 != "o") {
               choix20int = choix20.toInt
               while(choix20int < 0 || choix20int > nb20-1){
                 choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                 choix20int = choix20.toInt
               }
               retraitCHF = retraitCHF - choix20int * 20
               nb20 = choix20int
               if(choix20int != 0) {choixdecoupures = true}
               }
             }
             if (retraitCHF / 10 >= 1){
                   println("Il reste "+retraitCHF+" CHF à distribuer")
                   nb10 = retraitCHF / 10
                   retraitCHF = retraitCHF - nb10 * 10

              }
          }
           }else{ // le cas ou retraitCHF < 200 par defaut petites coupures
           if (retraitCHF / 100 >= 1){
             println("Il reste "+retraitCHF+" CHF à distribuer")
             nb100 = retraitCHF / 100
             println("Vous pouvez obtenir au maximum "+ nb100 +" billet(s) de 100 CHF")
             choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
             if (choix100 == "o"){
               retraitCHF = retraitCHF - nb100 * 100
               choixdecoupures = true
             }else if (choix100 != "o") {
               choix100int = choix100.toInt
                while(choix100int < 0 || choix100int > nb100-1){
                  choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                  choix100int = choix100.toInt
                }
               retraitCHF = retraitCHF - choix100int * 100
               nb100 = choix100int
               if(choix100int != 0){choixdecoupures = true}
             }
           }
           if (retraitCHF / 50 >= 1){
             println("Il reste "+retraitCHF+" CHF à distribuer")
             nb50 = retraitCHF / 50
             println("Vous pouvez obtenir au maximum "+ nb50 +" billet(s) de 50 CHF")
             choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
             if (choix50 == "o"){
               retraitCHF = retraitCHF - nb50 * 50
               choixdecoupures = true
             }else if (choix50 != "o") {
             choix50int = choix50.toInt
             while(choix50int < 0 || choix50int > nb50-1){
               choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
               choix50int = choix50.toInt
             }
             retraitCHF = retraitCHF - choix50int * 50
             nb50 = choix50int
               if(choix50int != 0){choixdecoupures = true}
             }
           }
           if (retraitCHF / 20 >= 1){
             println("Il reste "+retraitCHF+" CHF à distribuer")
             nb20 = retraitCHF / 20
             println("Vous pouvez obtenir au maximum "+ nb20 +" billet(s) de 20 CHF")
             choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
             if (choix20 == "o"){
               retraitCHF = retraitCHF - nb20 * 20
               choixdecoupures = true
             }else if (choix20 != "o") {
             choix20int = choix20.toInt
             while(choix20int < 0 || choix20int > nb20-1){
               choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
               choix20int = choix20.toInt
             }
             retraitCHF = retraitCHF - choix20int * 20
             nb20 = choix20int
             if(choix20int != 0) {choixdecoupures = true}
             }
           }
           if (retraitCHF / 10 >= 1){
               println("Il reste "+retraitCHF+" CHF à distribuer")
               nb10 = retraitCHF / 10
               retraitCHF = retraitCHF - nb10 * 10
            }
         }
         println("Veuillez retirer la somme demandée :")
         if(nb500 >= 1){
           println(nb500 +" billet(s) de 500 CHF")
         }
         if(nb200 >= 1){
           println(nb200 +" billet(s) de 200 CHF")
         }
         if(nb100 >= 1){
            println(nb100 +" billet(s) de 100 CHF")
          }
         if(nb50 >= 1){
            println(nb50 +" billet(s) de 50 CHF")
          }
         if(nb20 >= 1){
            println(nb20 +" billet(s) de 20 CHF")
          }
         if(nb10 >= 1){
            println(nb10 +" billet(s) de 10 CHF")
          }
         printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
       }
       else{ // On veut retirer en EUR
         do { 
            retraitEUR = readLine("Indiquez le montant du retrait : ").toInt
          } while(!((retraitEUR % 10 == 0) && (retraitEUR <= 0.1*comptes(id)*tauxdechangedeCHFaEUR))) 
         comptes(id) = comptes(id) - retraitEUR * tauxdechangedeCHFaEUR
       if (retraitEUR / 100 >= 1){
         println("Il reste "+retraitEUR+" EUR à distribuer")
         nb100 = retraitEUR / 100
         println("Vous pouvez obtenir au maximum "+ nb100 +" billet(s) de 100 EUR")
         choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
         if (choix100 == "o"){
           retraitEUR = retraitEUR - nb100 * 100
           choixdecoupures = true
         }else if (choix100 != "o") {
           choix100int = choix100.toInt
            while(choix100int < 0 || choix100int > nb100-1){
              choix100 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
              choix100int = choix100.toInt
            }
           retraitEUR = retraitEUR - choix100int * 100
           nb100 = choix100int
           if(choix100int != 0){choixdecoupures = true}
         }
       }
       if (retraitEUR / 50 >= 1){
         println("Il reste "+retraitEUR+" EUR à distribuer")
         nb50 = retraitEUR / 50
         println("Vous pouvez obtenir au maximum "+ nb50 +" billet(s) de 50 EUR")
         choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
         if (choix50 == "o"){
           retraitEUR = retraitEUR - nb50 * 50
           choixdecoupures = true
         }else if (choix50 != "o") {
         choix50int = choix50.toInt
         while(choix50int < 0 || choix50int > nb50-1){
           choix50 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
           choix50int = choix50.toInt
         }
         retraitEUR = retraitEUR - choix50int * 50
         nb50 = choix50int
           if(choix50int != 0){choixdecoupures = true}
         }
       }
       if (retraitEUR / 20 >= 1){
         println("Il reste "+retraitEUR+" EUR à distribuer")
         nb20 = retraitEUR / 20
         println("Vous pouvez obtenir au maximum "+ nb20 +" billet(s) de 20 EUR")
         choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
         if (choix20 == "o"){
           retraitEUR = retraitEUR - nb20 * 20
           choixdecoupures = true
         }else if (choix20 != "o") {
         choix20int = choix20.toInt
         while(choix20int < 0 || choix20int > nb20-1){
           choix20 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
           choix20int = choix20.toInt
         }
         retraitEUR = retraitEUR - choix20int * 20
         nb20 = choix20int
         if(choix20int != 0) {choixdecoupures = true}
         }
       }
         if (retraitEUR / 10 >= 1){
               println("Il reste "+retraitEUR+" CHF à distribuer")
               nb10 = retraitEUR / 10
               retraitEUR = retraitEUR - nb10 * 10

          }
         println("Veuillez retirer la somme demandée :")
         if(nb100 >= 1){
            println(nb100 +" billet(s) de 100 EUR")
          }
         if(nb50 >= 1){
            println(nb50 +" billet(s) de 50 EUR")
          }
         if(nb20 >= 1){
            println(nb20 +" billet(s) de 20 EUR")
          }
         if(nb10 >= 1){
            println(nb10 +" billet(s) de 10 EUR")
          }
         printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
          }

  }
  def depot(id : Int, comptes : Array[Double]) : Unit = 
  {
    var depotCHF = 0
    var depotEUR = 0

    do {
       devise = readLine ("Indiquez la devise du dépôt : 1) CHF ; 2) EUR : ").toInt
     }while (!(devise ==1 || devise == 2))
      if (devise == 1) { // On veut déposer en CHF
        do{
          depotCHF = readLine ("Indiquez le montant du dépôt : ").toInt
        }while(!((depotCHF % 10 == 0) && depotCHF >= 10))
        comptes(id) = depotCHF + comptes(id)
        printf ("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
      }else { // On veut déposer en EUR
        do {
          depotEUR = readLine ("Indiquez le montant du dépôt : ").toInt
        }while (!((depotEUR % 10 == 0) && depotEUR >= 10))
        comptes(id) = depotEUR * tauxdechangedeCHFaEUR + comptes(id)
        printf ("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
      }
  }
  def changepin(id: Int, codespin:Array[String]) : Unit =
  {
    var taillepin:Int = 0  //variable pour la taille (nombre de caractères) du mot de passe
    var pin_a_saisir:String = "A SAISIR"
    do{
      pin_a_saisir = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      taillepin = pin_a_saisir.length
      if(taillepin >= 8){  //bonne longueur de mot de passe, affichage d'un message de confirmation
        println("Code pin modifié avec succès.")
      }
      else{
        println("Votre code pin ne contient pas au moins 8 caractères")  //mauvaise longueur de mot de passe, affichage d'un message d'erreur
      }
    }while(taillepin<8)  //condition de sortie de la boucle
    codespin(id) = pin_a_saisir  //mise à jour du tableau des codes pins
  }
}
