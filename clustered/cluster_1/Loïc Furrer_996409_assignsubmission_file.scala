//Assignment: Loïc Furrer_996409_assignsubmission_file

import scala.io.StdIn._

object Main {
  var nbClients = 100
  var comptes = Array.fill(nbClients)(1200.0) // 100 comptes initialisés à 1200.0 CHF
  var codesPin = Array.fill(nbClients)("INTRO1234") // Codes PIN pour les 100 comptes
  var montantDepot = 0
  var choixDevise = 0 //pour l'opération 1) dépôt
  var choixDevise2 = 0 //pour l'opération 2) retrait
  var choixMontant = 0
  val montantCHF = 0  
  var GrossesCoupures = false
  var nb500 = 0
  var nb200 = 0
  var nb100 = 0       
  var nb50 = 0
  var nb20 = 0
  var nb10 = 0

  def estIdValide(idClient: Int): Boolean = {
    idClient >= 0 && idClient < nbClients
  }

  def traiterIdentification(idClient: Int): Unit = {
    var pinCorrect = false
      var tentative = 3
      var choix = 0

      while (!pinCorrect && tentative > 0) {
        println("Saisissez votre code PIN : ")
        val entreePin = readLine()

        if (estPinValide(idClient, entreePin)) {
          pinCorrect = true
          println("Code PIN correct")
        } else {
          tentative -= 1
          println(s"Code PIN erroné, il vous reste " + tentative + " tentative tentatives.")
          if (tentative == 0) {
            println("Trop d'erreurs, abandon de l'identification.")
            return
          }
        }
      }



      if (pinCorrect) {

        def afficherMenu(): Unit = {
          println("\nChoisissez votre opération :")
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code PIN")
          println("5) Terminer")
          print("Votre choix : ")
        }

        do {
          afficherMenu()
          choix = readInt()

          choix match {
            case 1 => depot(idClient, comptes)
            case 2 => retrait(idClient, comptes)
            case 3 => consultation(idClient)
            case 4 => changepin(idClient, codesPin)
            case 5 => println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
            case _ => println("Choix invalide. Veuillez choisir un chiffre entre 1 et 5.")
          }          

        } while (choix != 5)
      }
    }


  def estPinValide(idClient: Int, entreePin: String): Boolean = {
    entreePin == codesPin(idClient)
  }


  def depot(idClient: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la devise du dépôt : 1) CHF; 2) EUR")
    choixDevise = readInt()

    if (choixDevise == 1){
    println("Indiquez le montant du dépôt en CHF ")
    montantDepot = readInt()
    }else{
    println("Indiquez le montant du dépôt en EUR ")
    montantDepot = readInt()
    }

    while (!(montantDepot % 10 == 0)) {
      println("Le montant doit être un multiple de 10")
      if (choixDevise == 1){
      println("Indiquez le montant du dépôt en CHF") 
      }else{ 
      println( "Indiquez le montant du dépôt en EUR") 
      montantDepot = readInt()
      }
    }

    if (choixDevise == 2) {
      montantDepot = (montantDepot * 0.95).toInt // Convertir EUR en CHF
    }

    comptes(idClient) += montantDepot
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible est de: CHF %.2f\n", comptes(idClient))
  }

  def retrait(idClient: Int, comptes: Array[Double]): Unit = {

    var choixCoupure = 0 // Réinitialisation de choixCoupure
    println("Indiquez la devise: 1) CHF, 2) EUR")
    choixDevise2 = readInt()

    while(choixDevise2 != 1 && choixDevise2 != 2){
    println("Indiquez la devise: 1) CHF, 2) EUR")
    choixDevise2 = readInt()
    }//fermeture boucle while pour la Devise2

    println("Indiquez le montant du retrait: ")
    choixMontant = readInt()

      while(choixMontant > (comptes(idClient) * 0.1)){//CHF120
      println("Votre plafond de retrait autorisé est de: " + (comptes(idClient) * 0.1))
      println("Indiquez le montant du retrait")
      choixMontant = readInt()
      }

     while(!(choixMontant % 10 == 0)) {
       //Vérifier que le montant est un multiple de 10
       println("Le retrait doit être un multilpe de 10")
       println("Indiquez le montant du retrait")
       choixMontant = readInt()
       }

    //demandez choix de coupure
      if(choixDevise2 == 1){
        GrossesCoupures = choixMontant >= 200

        if(!GrossesCoupures){
          choixCoupure == 2
        }else{

        while(choixCoupure != 1 && choixCoupure != 2){
        println("En 1) grosses coupures, 2) petites coupures")
        choixCoupure = readInt()
        }
        }

        //Si le montant est >= 500, alors grosse coupure
        if(choixCoupure == 1) {
          var restant = choixMontant
          var confirmation = ""

          if (restant >= 500) {
             nb500 = restant / 500
            println("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb500 + "  billet(s) de 500 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb500 = confirmation.toInt
              while(nb500 > restant / 500){
                println("Impossible d'avoir ce nombre de billets, saisissez une valeur plus petite")
                confirmation = readLine()
                nb500 = confirmation.toInt
              }
                restant -= nb500 * 500 //restant-(nb500*500)
            }
            else {
              restant -= nb500 * 500
            }
          }

          if (restant >= 200) {
             nb200 = restant / 200
            println ("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb200 + " billet(s) de 200")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb200 = confirmation.toInt
              while(nb200 > restant / 200){
                println("Impossible d'avoir ce nombre de billets, saisissez une valeur plus petite")
                confirmation = readLine()
                nb200 = confirmation.toInt
              }
                restant -= nb200 * 200
            }
            else {
              restant -= nb200 * 200
            }
          }

          if (restant >= 100) {
             nb100 = restant / 100
            println ("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb100 + " billet(s) de 100")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb100 = confirmation.toInt
              while(nb100 > restant / 100){
                println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
                confirmation = readLine()
                nb100 = confirmation.toInt
              }
                restant -= nb100 * 100
            }
            else {
              restant -= nb100 * 100
            }
          }

          if (restant >= 50) {
             nb50 = restant / 50
            println ("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb50 + " billet(s) de 50")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb50 = confirmation.toInt
              while(nb50 > restant / 50){
                println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
                confirmation = readLine()
                nb50 = confirmation.toInt
              }
                restant -= nb50 * 50
            }
            else {
              restant -= nb50 * 50
            }
          }

          if (restant >= 20) {
             nb20 = restant / 20
            println ("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb20 + " billet(s) de 20")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb20 = confirmation.toInt
              while(nb20 > restant / 20){
                println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
                confirmation = readLine()
                nb20 = confirmation.toInt
              }
                restant -= nb20 * 20
            }
            else {
              restant -= nb20 * 20
            }
          }

          if (restant >= 10) {
             nb10 = restant / 10
            println ("Il reste " + restant + " CHF à distribuer")
            println("Veuillez retirer la somme demandée:\n")
            if (nb500 > 0) println(nb500 + " billet(s) de 500 CHF")
            if (nb200 > 0) println(nb200 + " billet(s) de 200 CHF")
            if (nb100 > 0) println(nb100 + " billet(s) de 100 CHF")
            if (nb50 > 0)  println(nb50 + " billet(s) de 50 CHF")
            if (nb20 > 0)  println(nb20 + " billet(s) de 20 CHF")
            println(nb10 + " billet(s) de 10 CHF")
            // Mise à jour de la solde du compte après le retrait
            comptes(idClient)-= choixMontant
            printf("Votre retrait à été pris en compte, le nouveau montant disponible sur votre compte est de CHF %.2f\n ", comptes(idClient))
          }

          if (restant == 0) {
            println("Veuillez retirer la somme demandée:\n")
            if (nb500 > 0) println(nb500 + " billet(s) de 500 CHF")
            if (nb200 > 0) println(nb200 + " billet(s) de 200 CHF")
            if (nb100 > 0) println(nb100 + " billet(s) de 100 CHF")
            if (nb50 > 0)  println(nb50 + " billet(s) de 50 CHF")
            if (nb20 > 0)  println(nb20 + " billet(s) de 20 CHF")
            if (nb10 > 0)  println(nb10 + " billet(s) de 10 CHF")
            // Mise à jour de la solde du compte après le retrait
            comptes(idClient)-= choixMontant
            printf("Votre retrait à été pris en compte, le nouveau montant disponible sur votre compte est de CHF %.2f\n ", comptes(idClient))
          }

        }else{
          var restant = choixMontant
          var confirmation = ""

          if (restant >= 100) {
             nb100 = restant / 100
            println ("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb100 + " billet(s) de 100")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb100 = confirmation.toInt
              while(nb100 > restant / 100){
                println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
                confirmation = readLine()
                nb100 = confirmation.toInt
              }
                restant -= nb100 * 100
            }else {
              restant -= nb100 * 100
            }
          }

          if (restant >= 50) {
             nb50 = restant / 50
            println ("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb50 + " billet(s) de 50")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb50 = confirmation.toInt
              while(nb50 > restant / 50){
                println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
                confirmation = readLine()
                nb50 = confirmation.toInt
              }
                restant -= nb50 * 50
            }else {
              restant -= nb50 * 50
            }
          }

          if (restant >= 20) {
             nb20 = restant / 20
            println("Il reste " + restant + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nb20 + " billet(s) de 20")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            confirmation = readLine()

            if (confirmation != "o") {
              nb20 = confirmation.toInt
              while(nb20 > restant / 20){
                println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
                confirmation = readLine()
                nb20 = confirmation.toInt
              }
                restant -= nb20 * 20
            }else {
              restant -= nb20 * 20
            }
          }

          if (restant >= 10) {
             nb10 = restant / 10
            println ("Il reste " + restant + " CHF à distribuer")
            println("Veuillez retirer la somme demandée:\n")
            if (nb100 > 0) println(nb100 + " billet(s) de 100 CHF")
            if (nb50 > 0)  println(nb50 + " billet(s) de 50 CHF")
            if (nb20 > 0)  println(nb20 + " billet(s) de 20 CHF")
            println(nb10 + " billet(s) de 10 CHF")
            // Mise à jour de la solde du compte après le retrait
            comptes(idClient)-= choixMontant
            printf("Votre retrait à été pris en compte, le nouveau montant disponible sur votre compte est de CHF %.2f\n ", comptes(idClient))
          }

          if (restant == 0) {
            println("Veuillez retirer la somme demandée:\n")
            if (nb100 > 0) println(nb100 + " billet(s) de 100 CHF")
            if (nb50 > 0)  println(nb50 + " billet(s) de 50 CHF")
            if (nb20 > 0)  println(nb20 + " billet(s) de 20 CHF")
            if (nb10 > 0)  println(nb10 + " billet(s) de 10 CHF")
            // Mise à jour de la solde du compte après le retrait
            comptes(idClient)-= choixMontant
            printf("Votre retrait à été pris en compte, le nouveau montant disponible sur votre compte est de CHF %.2f\n ", comptes(idClient))
          }
        }

      }else if(choixDevise2 == 2) {
        var restant = choixMontant
        var confirmation = ""

        if (restant >= 100) {
           nb100 = restant / 100
          println ("Il reste " + restant + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nb100 + " billet(s) de 100")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          confirmation = readLine()

          if (confirmation != "o") {
            nb100 = confirmation.toInt
            while(nb100 > restant / 100){
              println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
              confirmation = readLine()
              nb100 = confirmation.toInt
            }
              restant -= nb100 * 100
          }else {
            restant -= nb100 * 100
          }
        }

        if (restant >= 50) {
           nb50 = restant / 50
          println ("Il reste " + restant + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nb50 + " billet(s) de 50")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          confirmation = readLine()

          if (confirmation != "o") {
            nb50 = confirmation.toInt
            while(nb50 > restant / 50){
              println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
              confirmation = readLine()
              nb50 = confirmation.toInt
            }
              restant -= nb50 * 50
          }else {
            restant -= nb50 * 50
          }
        }

        if (restant >= 20) {
           nb20 = restant / 20
          println ("Il reste " + restant + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nb20 + " billet(s) de 20")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          confirmation = readLine()

          if (confirmation != "o") {
            nb20 = confirmation.toInt
            while(nb20 > restant / 20){
              println("Impossible d'avoir ce nombre de billet, saisissez une valeur plus petite")
              confirmation = readLine()
              nb20 = confirmation.toInt
            }
              restant -= nb20 * 20
          }else {
            restant -= nb20 * 20
          }
        }

        if (restant >= 10) {
           nb10 = restant / 10
          println ("Il reste " + restant + " EUR à distribuer")
          println("Veuillez retirer la somme demandée:\n")
          if (nb100 > 0) println(nb100 + " billet(s) de 100 EUR")
          if (nb50 > 0)  println(nb50 + " billet(s) de 50 EUR")
          if (nb20 > 0)  println(nb20 + " billet(s) de 20 EUR")
          println(nb10 + " billet(s) de 10 EUR")
          // Convertir EUR en CHF
          val montantCHF = choixMontant * 0.95
          // Mise à jour de la solde du compte après le retrait
          comptes(idClient)-= montantCHF
          printf("Votre retrait à été pris en compte, le nouveau montant disponible sur votre compte est de CHF %.2f\n ", comptes(idClient))
        }

        if (restant == 0) {
          println("Veuillez retirer la somme demandée:\n")
          if (nb100 > 0) println(nb100 + " billet(s) de 100 EUR")
          if (nb50 > 0)  println(nb50 + " billet(s) de 50 EUR")
          if (nb20 > 0)  println(nb20 + " billet(s) de 20 EUR")
          if (nb10 > 0)  println(nb10 + " billet(s) de 10 EUR") 
          // Convertir EUR en CHF
          val montantCHF = choixMontant * 0.95
          // Mise à jour de la solde du compte après le retrait
          comptes(idClient)-= montantCHF
          printf("Votre retrait à été pris en compte, le nouveau montant disponible sur votre compte est de CHF %.2f\n ", comptes(idClient))
        }
      }
  }

  def consultation(idClient: Int): Unit = {
    printf("Le montant disponible sur votre compte est de : CHF %.2f\n", comptes(idClient))
  }

  def changepin(idClient: Int, codesPin: Array[String]): Unit = {
    println("Saisissez votre nouveau code PIN (il doit contenir au moins 8 caractères) : ")
    var nouveauPin = readLine()

    while (nouveauPin.length < 8) {
      println("Votre code PIN ne contient pas au moins 8 caractères.")
      println("Saisissez à nouveau votre nouveau code PIN : ")
      nouveauPin = readLine()
    }
    // Mettez à jour le tableau codespin avec le nouveau PIN
    codesPin(idClient) = nouveauPin

    println("Votre code PIN a été modifié avec succès.")
  }

  def main(args: Array[String]): Unit = {
    var choix = 0

    do {
      println("\nSaisissez votre code identifiant : ")
      val idClient = readInt()

      // Validation de l'identifiant
      if (estIdValide(idClient)) {
        traiterIdentification(idClient)
      } else {
        println("Cet identifiant n'est pas valable.")
        return
      }

      // ... (le reste du code de votre méthode main)

    } while (choix != 5)
  }
}
