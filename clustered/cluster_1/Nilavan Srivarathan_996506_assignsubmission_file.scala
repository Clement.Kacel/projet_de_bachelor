//Assignment: Nilavan Srivarathan_996506_assignsubmission_file

import scala.io.StdIn._

object Main {

  def main(args: Array[String]): Unit = {
  
    val lenombredeclients = 100
    val lescomptes: Array[Double] = Array.fill[Double](lenombredeclients)(1200.0)
    val lescodespins: Array[String] = Array.fill[String](lenombredeclients)("INTRO1234")

    def afficherMenu(): Unit = {
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
    }
  
    while (true) {
      var lechoix = 0
      val identifier = readLine("Entrer votre numéro d'identifiant > ").toInt

      if (identifier >= lenombredeclients) {
        println("Cet identifiant n'est pas valide.")
        sys.exit()
      }
      var nombredetentatives = 3
      var lecodepinestbon = false
      while (nombredetentatives > 0 && !lecodepinestbon) {
        val enteredPin = readLine("Entrer votre code pin : ")
        if (enteredPin == lescodespins(identifier)) {
          lecodepinestbon = true
        } else {
          nombredetentatives = nombredetentatives - 1
          if (nombredetentatives > 0) {
            println("Le code pin entré est mauvais, vous disposez encore de " + nombredetentatives + " tentatives.")
          } else {
            println("Il y a trop d'erreur. L'identification est abandonné")
          }
        }
      }


      if (lecodepinestbon) {
        while(lechoix != 5) {
          afficherMenu()
          lechoix = readLine("Quel est votre choix ? > ").toInt


          if (lechoix == 1) { // les dépôts
            depot(identifier, lescomptes)
          }

          if (lechoix == 2) {  // le retrait
            retrait(identifier, lescomptes)
          }

          if (lechoix == 3) {  // consultation du compte
            printf("Le montant disponible sur votre compte est de : %.2f CHF\n", lescomptes(identifier))
          }

          if (lechoix == 4) {  // changement du code pin
            changepin(identifier, lescodespins)
          }
        }
        if (lechoix == 5) {  // terminer
          println("N'oubliez pas de récupérer votre carte !")
        }
      }
    }
  }

  def depot(id: Int, lescomptes: Array[Double]): Unit = {
    var montantajouteenCHF = 0.0
    var montantajouteenEUR = 0.0

    val ladevisedudépôt = readLine("Quel est la devise de votre dépôt 1) CHF 2) EUR ").toInt

    if (ladevisedudépôt == 1) {
      montantajouteenCHF = readLine("Quel est le montant de votre dépôt > ").toInt

      if (montantajouteenCHF != 0 && montantajouteenCHF % 10 == 0) {
        lescomptes(id) = lescomptes(id) + montantajouteenCHF
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible est de : %.2f CHF\n ", lescomptes(id))
      } else {
        println("Le montant doit être un multiple de 10 CHF")

      }
    } else if (ladevisedudépôt == 2) {
      montantajouteenEUR = readLine("Quel est le montant de votre dépôt > ").toInt

      if (montantajouteenEUR % 10 == 0) {
        lescomptes(id) = lescomptes(id) + montantajouteenEUR * 0.95
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible est de : %.2f CHF\n ", lescomptes(id))
      } else {
        println("Le montant doit être un multiple de 10")

      }

    }

  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {
    var ladeviseduretrait = 0

    while (ladeviseduretrait != 1 && ladeviseduretrait != 2) {
      ladeviseduretrait = readLine("Quel est de la devise de votre retrait 1) CHF 2 ) EUR ").toInt
    }

    if (ladeviseduretrait == 1) {

      var lemontantderetraitenCHF = readLine("Quel est le montant de votre retrait > ").toInt

      while (lemontantderetraitenCHF % 10 != 0) {


        if (lemontantderetraitenCHF % 10 != 0) {
          println("Le montant doit être un multiple de 10 CHF")
          lemontantderetraitenCHF = readLine("Quel est le montant de votre retrait > ").toInt
        }
      }
      while (lemontantderetraitenCHF > comptes(id) * 0.1) {

        if (lemontantderetraitenCHF > comptes(id) * 0.1) {
          println("Votre plafond de retrait est de :" + comptes(id) * 0.1)
          lemontantderetraitenCHF = readLine("Quel est le montant de votre retrait > ").toInt
        }

      }

      if (lemontantderetraitenCHF >= 200) { // si c est plus grand que 200 ça demande en quelle coupure
        var tailledubilletdebanque = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt

        if (tailledubilletdebanque == 1) {

          var lemontantrestant = lemontantderetraitenCHF
          var billetsde500 = 0
          var billetsde200 = 0
          var billetsde100 = 0
          var billetsde50 = 0
          var billetsde20 = 0
          var billetsde10 = 0


          // Pour les billets de 500
          if (lemontantrestant >= 500) {
            val choixbillets500 = readLine("Il reste " + lemontantrestant + " à distribuer \nVous pouvez recevoir au maximum " + lemontantrestant / 500 + " billets de 500 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets500 == "o") {
              billetsde500 = lemontantrestant / 500
              lemontantrestant = lemontantrestant - billetsde500 * 500
            } else if (choixbillets500 == "0") {

            } else {
              // convertir le nombre entrée en Int
              val choixInt = choixbillets500.toInt
              billetsde500 = choixInt
              lemontantrestant = lemontantrestant - billetsde500 * 500
            }
          }



          // Pour les billets de 200
          if (lemontantrestant >= 200) {
            val choixbillets200 = readLine("Il reste " + lemontantrestant + " à distribuer \nVous pouvez recevoir au maximum " + lemontantrestant / 200 + " billets de 200 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets200 == "o") {
              billetsde200 = lemontantrestant / 200
              lemontantrestant = lemontantrestant - billetsde200 * 200
            } else if (choixbillets200 == "0") {

            } else {

              val choixInt = choixbillets200.toInt
              billetsde200 = choixInt
              lemontantrestant = lemontantrestant - billetsde200 * 200
            }
          }

          // Pour les billets de 100
          if (lemontantrestant >= 100) {
            val choixbillets100 = readLine("Il reste " + lemontantrestant + " à distribuer \nVous pouvez recevoir au maximum " + lemontantrestant / 100 + " billets de 100 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets100 == "o") {
              billetsde100 = lemontantrestant / 100
              lemontantrestant = lemontantrestant - billetsde100 * 100
            } else if (choixbillets100 == "0") {

            } else {

              val choixInt = choixbillets100.toInt
              billetsde100 = choixInt
              lemontantrestant = lemontantrestant - billetsde100 * 100
            }
          }

          // Pour les billets de 50
          if (lemontantrestant >= 50) {
            val choixbillets50 = readLine("Il reste " + lemontantrestant + " à distribuer \nVous pouvez recevoir au maximum " + lemontantrestant / 50 + " billets de 50 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets50 == "o") {
              billetsde50 = lemontantrestant / 50
              lemontantrestant = lemontantrestant - billetsde50 * 50
            } else if (choixbillets50 == "0") {

            } else {

              val choixInt = choixbillets50.toInt
              billetsde50 = choixInt
              lemontantrestant = lemontantrestant - billetsde50 * 50
            }
          }

          // Pour les billets de 20
          if (lemontantrestant >= 20) {
            val choixbillets20 = readLine("Il reste " + lemontantrestant + " à distribuer ,Vous pouvez recevoir au maximum " + lemontantrestant / 20 + " billets de 20 CHF Taper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets20 == "o") {
              billetsde20 = lemontantrestant / 20
              lemontantrestant = lemontantrestant - billetsde20 * 20
            } else if (choixbillets20 == "0") {

            } else {

              val choixInt = choixbillets20.toInt
              billetsde20 = choixInt
              lemontantrestant = lemontantrestant - billetsde20 * 20
            }
          }
          if (lemontantrestant == lemontantrestant) {
            billetsde10 = lemontantrestant / 10
            lemontantrestant = lemontantrestant - lemontantrestant
          }

          // Pour les billets de 10
          if (lemontantrestant >= 10) {
            val choixbillets10 = readLine("Il reste " + lemontantrestant + " à distribuer ,Vous pouvez recevoir au maximum " + lemontantrestant / 10 + " billets de 10 CHF Taper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets10 == "o") {
              billetsde10 = lemontantrestant / 10
              lemontantrestant = lemontantrestant - billetsde10 * 10
            } else if (choixbillets10 == "0") {

            } else {

              val choixInt = choixbillets10.toInt
              billetsde10 = choixInt
              lemontantrestant = lemontantrestant - billetsde10 * 10
            }
          }


          if (billetsde500 > 0) println(billetsde500 + " billets de 500 CHF")
          if (billetsde200 > 0) println(billetsde200 + " billets de 200 CHF")
          if (billetsde100 > 0) println(billetsde100 + " billets de 100 CHF")
          if (billetsde50 > 0) println(billetsde50 + " billets de 50 CHF")
          if (billetsde20 > 0) println(billetsde20 + " billets de 20 CHF")
          if (billetsde10 > 0) println(billetsde10 + " billets de 10 CHF")


          comptes(id) = comptes(id) - (billetsde500 * 500) - (billetsde200 * 200) - (billetsde100 * 100) - (billetsde50 * 50) - (billetsde20 * 20) - (billetsde10 * 10)
          printf("Le montant disponible sur votre compte est d : %.2f CHF\n", comptes(id))


        } else if (tailledubilletdebanque == 2) {

          var montantrestant = lemontantderetraitenCHF
          var billetsde100 = 0
          var billetsde50 = 0
          var billetsde20 = 0
          var billetsde10 = 0



          // Pour les billets de 100
          if (montantrestant >= 100) {
            val choixbillets100 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 100 + " billets de 100 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets100 == "o") {
              billetsde100 = montantrestant / 100
              montantrestant = montantrestant - billetsde100 * 100
            } else if (choixbillets100 == "0") {

            } else {
              // convertir le nombre entrée en Int
              val choixInt = choixbillets100.toInt
              billetsde100 = choixInt
              montantrestant = montantrestant - billetsde100 * 100
            }
          }

          // Pour les billets de 50
          if (montantrestant >= 50) {
            val choixbillets50 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 50 + " billets de 50 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets50 == "o") {
              billetsde50 = montantrestant / 50
              montantrestant = montantrestant - billetsde50 * 50
            } else if (choixbillets50 == "0") {

            } else {
              // convertir le nombre entrée en Int
              val choixInt = choixbillets50.toInt
              billetsde50 = choixInt
              montantrestant = montantrestant - billetsde50 * 50
            }
          }

          // Pour les billets de 20
          if (montantrestant >= 20) {
            val choixbillets20 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 20 + " billets de 20 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets20 == "o") {
              billetsde20 = montantrestant / 20
              montantrestant = montantrestant - billetsde20 * 20
            } else if (choixbillets20 == "0") {

            } else {
              // convertir le nombre entrée en Int
              val choixInt = choixbillets20.toInt
              billetsde20 = choixInt
              montantrestant = montantrestant - billetsde20 * 20
            }
          }

          // si l utilisateur met que des 0 avant, il va forcement tout recupérer a la fin en billets de 10
          if (montantrestant == montantrestant) {
            billetsde10 = montantrestant / 10
            montantrestant = montantrestant - montantrestant
          }

          // Pour les billets de 10
          if (montantrestant >= 10) {
            val choixbillets10 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 10 + " billets de 10 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

            if (choixbillets10 == "o") {
              billetsde10 = montantrestant / 10
              montantrestant = montantrestant - billetsde10 * 10
            } else if (choixbillets10 == "0") {

            } else {
              // conertir le nombre entrée en Int
              val choixInt = choixbillets10.toInt
              billetsde10 = choixInt
              montantrestant = montantrestant - billetsde10 * 10
            }
          }


          if (billetsde100 > 0) println(billetsde100 + " billets de 100 CHF")
          if (billetsde50 > 0) println(billetsde50 + " billets de 50 CHF")
          if (billetsde20 > 0) println(billetsde20 + " billets de 20 CHF")
          if (billetsde10 > 0) println(billetsde10 + " billets de 10 CHF")

          comptes(id) = comptes(id) - (billetsde100 * 100) - (billetsde50 * 50) - (billetsde20 * 20) - (billetsde10 * 10)
          printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        }

      } else {

        var montantrestant = lemontantderetraitenCHF
        var billetsde100 = 0
        var billetsde50 = 0
        var billetsde20 = 0
        var billetsde10 = 0

        // Pour les billets de 100
        if (montantrestant >= 100) {
          val choixbillets100 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 100 + " billets de 100 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

          if (choixbillets100 == "o") {
            billetsde100 = montantrestant / 100
            montantrestant = montantrestant - billetsde100 * 100
          } else if (choixbillets100 == "0") {

          } else {
            // convertir le nombre entrée en Int
            val choixInt = choixbillets100.toInt
            billetsde100 = choixInt
            montantrestant = montantrestant - billetsde100 * 100
          }
        }

        // Pour les billets de 50
        if (montantrestant >= 50) {
          val choixbillets50 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 50 + " billets de 50 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

          if (choixbillets50 == "o") {
            billetsde50 = montantrestant / 50
            montantrestant = montantrestant - billetsde50 * 50
          } else if (choixbillets50 == "0") {

          } else {

            val choixInt = choixbillets50.toInt
            billetsde50 = choixInt
            montantrestant = montantrestant - billetsde50 * 50
          }
        }

        // Pour les billets de 20
        if (montantrestant >= 20) {
          val choixbillets20 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 20 + " billets de 20 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

          if (choixbillets20 == "o") {
            billetsde20 = montantrestant / 20
            montantrestant = montantrestant - billetsde20 * 20
          } else if (choixbillets20 == "0") {

          } else {

            val choixInt = choixbillets20.toInt
            billetsde20 = choixInt
            montantrestant = montantrestant - billetsde20 * 20
          }
        }

        if (montantrestant == montantrestant) {
          billetsde10 = montantrestant / 10
          montantrestant = montantrestant - montantrestant
        }

        // Pour les billets de 10
        if (montantrestant >= 10) {
          val choixbillets10 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 10 + " billets de 10 CHF \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

          if (choixbillets10 == "o") {
            billetsde10 = montantrestant / 10
            montantrestant = montantrestant - billetsde10 * 10
          } else if (choixbillets10 == "0") {

          } else {

            val choixInt = choixbillets10.toInt
            billetsde10 = choixInt
            montantrestant = montantrestant - billetsde10 * 10
          }
        }


        if (billetsde100 > 0) println(billetsde100 + " billets de 100 CHF")
        if (billetsde50 > 0) println(billetsde50 + " billets de 50 CHF")
        if (billetsde20 > 0) println(billetsde20 + " billets de 20 CHF")
        if (billetsde10 > 0) println(billetsde10 + " billets de 10 CHF")

        comptes(id) = comptes(id) - (billetsde100 * 100) - (billetsde50 * 50) - (billetsde20 * 20) - (billetsde10 * 10)
        printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      }


    } // les retraits en euros
    if (ladeviseduretrait == 2) {
      var montantretiréenEUR = readLine("Quel est le montant de votre retrait > ").toInt

      while (montantretiréenEUR % 10 != 0 || montantretiréenEUR > comptes(id) * 0.1) {


        montantretiréenEUR = readLine("Quel est le montant de votre retrait > ").toInt

        if (montantretiréenEUR % 10 != 0) {
          println("Le montant doit être un multiple de 10")
        }

        if (montantretiréenEUR > comptes(id) * 0.1) {
          println("Votre plafond de retrait est de  :" + comptes(id) * 0.1)
        }

      }

      var montantrestant = montantretiréenEUR
      var billetsde100 = 0
      var billetsde50 = 0
      var billetsde20 = 0
      var billetsde10 = 0

      // les billets de 100 euros
      if (montantrestant >= 100) {
        val choixbillets100 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 100 + " billets de 100 EUR \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

        if (choixbillets100 == "o") {
          billetsde100 = montantrestant / 100
          montantrestant = montantrestant - billetsde100 * 100
        } else if (choixbillets100 == "0") {

        } else {

          val choixInt = choixbillets100.toInt
          billetsde100 = choixInt
          montantrestant = montantrestant - billetsde100 * 100
        }
      }

      // Les billets de 50 euros
      if (montantrestant >= 50) {
        val choixbillets50 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 50 + " billets de 50 EUR \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

        if (choixbillets50 == "o") {
          billetsde50 = montantrestant / 50
          montantrestant = montantrestant - billetsde50 * 50
        } else if (choixbillets50 == "0") {

        } else {

          val choixInt = choixbillets50.toInt
          billetsde50 = choixInt
          montantrestant = montantrestant - billetsde50 * 50
        }
      }

      // Les billets de 20 euros
      if (montantrestant >= 20) {
        val choixbillets20 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 20 + " billets de 20 EUR \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

        if (choixbillets20 == "o") {
          billetsde20 = montantrestant / 20
          montantrestant = montantrestant - billetsde20 * 20
        } else if (choixbillets20 == "0") {

        } else {

          val choixInt = choixbillets20.toInt
          billetsde20 = choixInt
          montantrestant = montantrestant - billetsde20 * 20
        }
      }

      if (montantrestant == montantrestant) {
        billetsde10 = montantrestant / 10
        montantrestant = montantrestant - montantrestant
      }

      // Pour les billets de 10 euros
      if (montantrestant >= 10) {
        val choixbillets10 = readLine("Il reste " + montantrestant + " à distribuer \nVous pouvez recevoir au maximum " + montantrestant / 10 + " billets de 10 EUR \nTaper o pour ok ou une autre valeur inférieur à celle proposée")

        if (choixbillets10 == "o") {
          billetsde10 = montantrestant / 10
          montantrestant = montantrestant - billetsde10 * 10
        } else if (choixbillets10 == "0") {

        } else {

          val choixInt = choixbillets10.toInt
          billetsde10 = choixInt
          montantrestant = montantrestant - billetsde10 * 10
        }
      }
      if (billetsde100 > 0) println(billetsde100 + " billets de 100 EUR")
      if (billetsde50 > 0) println(billetsde50 + " billets de 50 EUR")
      if (billetsde20 > 0) println(billetsde20 + " billets de 20 EUR")
      if (billetsde10 > 0) println(billetsde10 + " billets de 10 EUR")

      comptes(id) = comptes(id) - (billetsde100 * (100 * 0.95)) - (billetsde50 * (50 * 0.95)) - (billetsde20 * (20 * 0.95)) - (billetsde10 * (10 * 0.95))
      printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
    }
  }

    def changepin(id: Int, codespin: Array[String]): Unit = { // le changement du code pin
    var nouveaupin = readLine("Entrer votre code pin (il doit contenir au moins 8 caractères) > ")

    while (nouveaupin.length < 8) {
      println("Le code pin saisie ne contient pas au moins 8 caractères.")
      nouveaupin = readLine("Entrer votre code pin (il doit contenir au moins 8 caractères) > ")
    }
    
    codespin(id) = nouveaupin
    println("Le code pin a été changé avec succès")
  }

}