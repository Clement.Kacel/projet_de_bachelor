//Assignment: Mauro Nicolas Villaman George_996529_assignsubmission_file

import scala.io.StdIn._

object Main {
  var essaisRestants = 3
  def main(args: Array[String]): Unit = {
    val nbclients=100
    var codespin = Array.fill(nbclients)("INTRO1234")
    var comptes =  Array.fill(nbclients)(1200.00)
    var login=0
    var choix = 0
    var codePinVerifie = false
    var ident = 0
    var stay:Boolean=true

    while(true){
      if(stay){
        codePinVerifie = false
        essaisRestants=3
        ident = readLine("Saisissez votre code identifiant >").toInt
          if(ident>=nbclients){
            println("Cet identifiant n'est pas valable.")
            System.exit(0)  //sortie du  programme car identifiant hors du tableau de clients, cela créer un bug sinon
          }
          if(ident<0){
            println("Cet identifiant n'est pas valable.")
            System.exit(0)
          }
          else{
            //code pin a verifier 
            if (!codePinVerifie) {
              if (verifierCodePin(ident,codespin)) {
                codePinVerifie = true
                println("Code PIN correct. Vous pouvez effectuer des opérations.")
                stay=false
              } else {
                println("Trop d’erreurs, abandon de l’identification")
                stay=true
              }
            }
          }
        
      }
          if(!stay){
            choix=0
            while (choix != 5) {

              if (codePinVerifie) {
                choix = readLine("Choisissez votre opération :  \n1) Dépôt \n2) Retrait\n3) Consultation du compte \n4) Changement du code Pin \n5) Terminer \n -> Votre choix : ").toInt

                if (choix == 1) {
                  depot(ident,comptes)
                } else if (choix == 2) {
                  retrait(ident,comptes)
                } else if (choix == 3) {
                  consultation(ident,comptes)
                } else if (choix == 4) {
                  changepin(ident,codespin)
                }
                else if(choix==5){
                  stay=true
                }
                else {
                  println("Choix invalide. Veuillez réessayer.")
                }
              }
              else {
                stay=true
              }
            }
          }
    }
  }
  def verifierCodePin(id : Int ,codespin : Array[String]): Boolean = {
    var tentative = readLine("Entrez le code PIN : ")
    while (tentative != codespin(id) && essaisRestants > 1) {
      essaisRestants -= 1
      println(s"Code PIN incorrect. Il vous reste "+essaisRestants+" essai(s).")
      tentative = readLine("Entrez le code PIN : ")
    }
    essaisRestants > 1
  }
  def changepin(id:Int,codespin:Array[String]):Unit={
    var nouveauCodePin:String = ""
    do{
      nouveauCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      if(nouveauCodePin.length<8){
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
    }while(nouveauCodePin.length < 8)
    codespin(id) = nouveauCodePin
  }
    
  def depot(id : Int , comptes : Array[Double]): Unit = {

      var devise = 0
      var montant_depot = 0
      var depot_euro = 0.0
      var euro = 0.0
      do{
        devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR ").toInt
      }while(devise!=1 && devise!=2)
      montant_depot = readLine("Indiquez le montant du dépôt : ").toInt

      while (!(montant_depot % 10 == 0)) {
        println("Le montant doit être un multiple de 10.")
        montant_depot = readLine("Indiquez le montant du dépôt : ").toInt
      }

      if (devise == 2) {
        euro = (montant_depot * 0.95)
        comptes(id) = (euro + comptes(id))
        euro = math.floor(euro * 100) / 100
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est %.2f CHF\n" , comptes(id))

      } else {
        comptes(id) = (comptes(id) + montant_depot)
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est %.2f CHF\n" , comptes(id))
      }

  }
  def retrait(id : Int, comptes : Array[Double]): Unit = {
    
    var retrait = 0.0
    var devise = 0
    var montant_retrait = 0
    var euro = 0.0
    var retrait_euro = 0.0
    var montant_autorise = 0.0
    var coupures = 0
    var pcoupures = 0
    var gcoupures_500 = 0
    var coupure500 = 0
    var gcoupures_200 = 0
    var coupure200 = 0
    var gcoupures_100 = 0
    var coupure100 = 0
    var gcoupures_50 = 0
    var coupure50 = 0
    var gcoupures_20 = 0
    var coupure20 = 0
    var gcoupures_10 = 0
    var coupure10 = 0
    var pcoupures_100 = 0
    var pcoupure100 = 0
    var pcoupures_50 = 0
    var pcoupure50 = 0
    var pcoupures_20 = 0
    var pcoupure20 = 0
    var pcoupures_10 = 0
    var pcoupure10 = 0
    var accepter_billet=""
    
    do{
      devise = readLine("Indiquez la devise :1 CHF, 2 : EUR ").toInt
    }while(devise!=1 && devise!=2)

    do{
      montant_retrait = readLine("Indiquez le montant du retrait : ").toInt
      if(montant_retrait%10!=0){
        println("Le montant doit être un multiple de 10.")
      }
      if(montant_retrait>0.1*comptes(id)){
        println("Votre plafond de retrait autorisé est de : " + (0.1*comptes(id)))
      }
    }while(montant_retrait%10!=0 || montant_retrait<10 || montant_retrait>0.1*comptes(id))
      
       if(devise==1)
          {  //la devise 1 représente un rerait en franc suisse : CHF
            comptes(id) -= montant_retrait
            var coupure_choice:Int = 0
            if(montant_retrait>=200)
            { //si c'est >=200, on propose de choisir la coupure, sinon non
              do
              {  //do = au moins 1x
                coupure_choice = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
              }while(coupure_choice!=1 && coupure_choice!=2)//repreter le while tant que c'est pas bo,
            }
            else
            {
              coupure_choice=2  //sinon, le choix de coupure est 2 car il n'a pas le choix
            }
              if(coupure_choice==1)
              {  //proposition des billets de 500,200,100,50,20,10

                  //print du montant qu'il reste
                  println("Il reste " + montant_retrait + " CHF à distribuer")  
                  if(montant_retrait>=500){
                    println("Vous pouvez obtenir au maximum " + montant_retrait/500 + " billet(s) de 500 CHF")  //Division entière pour avoir le max de billets de 500

                    accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    if(accepter_billet=="o")
                    {
                      coupure500 = montant_retrait/500
                      montant_retrait -= (500*coupure500)
                    }
                    else
                    {
                      if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/500)
                      {
                        //si c'est un nombre valide, on le prend
                        coupure500 = accepter_billet.toInt
                        montant_retrait -= (500*coupure500)
                      }
                      else
                      {  
                        //sinon, on recommence
                        while((accepter_billet.toInt<0) || (accepter_billet.toInt>montant_retrait/500)){
                          accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

                        }
                        //apres le while, c'est forcement un nombre valide
                        coupure500 = accepter_billet.toInt
                        montant_retrait -= (500*coupure500)
                      }
                    }

                  }
                   if(montant_retrait>=200){
                    println("Vous pouvez obtenir au maximum " + montant_retrait/200 + " billet(s) de 200 CHF")

                    accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    if(accepter_billet=="o")
                    {
                      //si dès le début, on a accepter_billet, on peut faire le calcul 
                      coupure200 = montant_retrait/200
                      montant_retrait -= (200*coupure200)
                    }
                    else
                    {
                      if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/200)
                      {
                        //même procédé que pour le billet de 500
                        coupure200 = accepter_billet.toInt
                        montant_retrait -= (200*coupure200)
                      }
                      else
                      {
                        while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/200)){
                          accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

                        }
                        coupure200 = accepter_billet.toInt
                        montant_retrait -= (200*coupure200)
                      }
                    }
                  }
                    //ici on vient de faire les billets de 500 et 200 donc après on peut directement mettre la variable coupure_choice = 2 car dans tous les cas on va passer aux billets de 100, cela economisera et optimisera nos lignes de code
                    coupure_choice=2
              }
              if(coupure_choice==2)
              {
                //on s'occupre ici des billets de 100,50,20,10 en suivant le même procédé que pour les billets de 500 et 200
                   println("Il reste " + montant_retrait + " CHF à distribuer")  
                  if(montant_retrait>=100){
                    println("Vous pouvez obtenir au maximum " + montant_retrait/100 + " billet(s) de 100 CHF")

                    accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    if(accepter_billet=="o")
                    {
                      //accépter_billet = o donc on retire le maximum proposé
                      coupure100 = montant_retrait/100
                      montant_retrait -= (coupure100*100)
                    }
                    else
                    {
                      if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/100){
                        //Si on a choisit un nombre qui est bon alors
                        coupure100 = accepter_billet.toInt
                        montant_retrait -= (coupure100*100)
                      }
                      else
                      {
                        while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/100)){
                          accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        //mauvais respect des conditions = on reste dans le while

                        }
                        coupure100 = accepter_billet.toInt
                        montant_retrait -= (coupure100*100)
                      }
                    }
                  }
                  if(montant_retrait>=50){
                    println("Vous pouvez obtenir au maximum " + montant_retrait/50 + " billet(s) de 50 CHF")

                    accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    if(accepter_billet=="o")
                    {
                      coupure50 = montant_retrait/50
                      montant_retrait -= (coupure50*50)
                    }
                    else
                    {
                      if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/50){
                        //choix valide, c a dire entre 0 et le max de billets autorisé
                        coupure50 = accepter_billet.toInt
                        montant_retrait -= (coupure50*50)
                      }
                      else
                      {
                        while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/50)){
                          accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                        }
                        //Apres le while, c'est forcement un nombre valide
                        coupure50 = accepter_billet.toInt
                        montant_retrait -= (coupure50*50)
                      }
                    }

                  }
                  if(montant_retrait>=20){
                    println("Vous pouvez obtenir au maximum " + montant_retrait/20 + " billet(s) de 20 CHF")

                    accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    if(accepter_billet=="o")
                    {
                      coupure20 = montant_retrait/20
                      montant_retrait -= (coupure20*20)
                    }
                    else
                    {
                      if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/20){
                        //le nombre est valide
                        coupure20 = accepter_billet.toInt
                        montant_retrait -= (coupure20*20)
                      }
                      else
                      {
                        //le nombre est pas valide, alors on demande une saisie à nouveau et tant qu'elle est n'est pas bonne, on demande
                        while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/20)){
                          accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

                        }
                        //la coupure est valide
                        coupure20 = accepter_billet.toInt
                        montant_retrait -= (coupure20*20)
                      }
                    }

                    
                  }
                  if(montant_retrait>=10)
                  {
                    println("Vous pouvez obtenir au maximum " + montant_retrait/10 + " billet(s) de 10 CHF")
                    coupure10 = montant_retrait/10
                    montant_retrait-=(coupure10*10)
                  }

                println("Veuillez retirer la somme demandée : ")
                if(coupure500>0)
                {
                  println(s"$coupure500  billet(s) de 500 CHF")
                }
                if(coupure200>0)
                {
                  println(s"$coupure200  billet(s) de 200 CHF")
                }
                if(coupure100>0)
                {
                  println(s"$coupure100  billet(s) de 100 CHF")
                }
                if(coupure50>0)
                {
                  println(s"$coupure50  billet(s) de 50 CHF")
                }
                if(coupure20>0)
                {
                  println(s"$coupure20  billet(s) de 20 CHF")
                }
                if(coupure10>0)
                {
                  println(s"$coupure10  billet(s) de 10 CHF")
                }

              printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))

              }
          }
          else
          {
            //la devise ici comme c'est un else est forcément la devise 2, or la devise 2 représente la devise en euro : EUR
                //alors on utilise els mêmes procédés que pour les billets de 500,200,100,50,20,10, mais on utilise cette fois que 100 etc... car c'est la règle pour la devise n°2
            comptes(id) -= (montant_retrait*0.95)
                 println("Il reste " + montant_retrait + " CHF à distribuer")  
                if(montant_retrait>=100 ){
                  println("Vous pouvez obtenir au maximum " + (montant_retrait/100) + " billet(s) de 100 EUR")

                  accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  if(accepter_billet=="o")
                  {
                    coupure100 = montant_retrait/100
                    montant_retrait -= (coupure100*100)
                  }
                  else
                  {
                    if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/100){
                      //nombre valide
                      coupure100 = accepter_billet.toInt
                      montant_retrait -= (coupure100*100)
                    }
                    else
                    {
                      while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/100)){
                        accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


                      }
                      //Nombre valide après le while
                      coupure100 = accepter_billet.toInt
                      montant_retrait -= (coupure100*100)
                    }
                  }

                  
                }
                 if(montant_retrait>=50 ){
                  println("Vous pouvez obtenir au maximum " + (montant_retrait/50) + " billet(s) de 50 EUR")

                  accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  if(accepter_billet=="o")
                  {
                    coupure50 = montant_retrait/50
                    montant_retrait -= (coupure50*50)
                  }
                  else
                  {
                    if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/50){
                      coupure50 = accepter_billet.toInt
                      montant_retrait -= (coupure50*50)
                    }
                    else
                    {
                      while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/50)){
                        //Boucle qui permet de vérifier que la valeur est uniquement inférieure au maximum ou = "ok"
                        accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

                      }
                      coupure50 = accepter_billet.toInt
                      montant_retrait -= (coupure50*50)
                    }
                  }

                  
                }
                 if(montant_retrait>=20 ){
                  println("Vous pouvez obtenir au maximum " + (montant_retrait/20) + " billet(s) de 20 EUR")

                  accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  if(accepter_billet=="o")
                  {
                    coupure20 = montant_retrait/20
                    montant_retrait -=  (coupure20*20)
                  }
                  else
                  {
                    if(accepter_billet.toInt>=0 && accepter_billet.toInt<=montant_retrait/20){
                      coupure20 = accepter_billet.toInt  //si la valeur est inférieure mais bonne, alors directement on retire
                      montant_retrait -=  (coupure20*20)
                    }
                    else
                    {
                      while((accepter_billet.toInt<0 || accepter_billet.toInt>montant_retrait/20)){  //Boucle pour vérifier bonne valeur
                        accepter_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                      }
                      coupure20 = accepter_billet.toInt
                      montant_retrait -=  (coupure20*20)
                    }
                  }

                  
                }
                if(montant_retrait>=10 )
                {
                  //plus de choix pour ce montant car c'est la denrière coupure possible
                  println("Vous pouvez obtenir au maximum " + (montant_retrait/10) + " billet(s) de 10 EUR")
                  coupure10 = montant_retrait/10
                  montant_retrait-=(coupure10*10)
                }

              println("Veuillez retirer la somme demandée : ")

          if(coupure100>0)
          {
            println(s"$coupure100  billet(s) de 100 CHF")
          }
          if(coupure50>0)
          {
            println(s"$coupure50  billet(s) de 50 CHF")
          }
          if(coupure20>0)
          {
            println(s"$coupure20  billet(s) de 20 CHF")
          }
          if(coupure10>0)
          {
            println(s"$coupure10  billet(s) de 10 CHF")
          }

            printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
          }
  }


  def consultation(id : Int, comptes : Array[Double]): Unit = {
    printf("Votre solde est de : %.2f CHF\n", comptes(id))
  }

}