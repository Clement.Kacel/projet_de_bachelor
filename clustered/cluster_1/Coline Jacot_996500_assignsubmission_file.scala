//Assignment: Coline Jacot_996500_assignsubmission_file

import scala.io.StdIn._

object Main {

// Méthode opération 1 de dépôt
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    var devise = readLine("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR : \n").toInt
    var montant = readLine("Indiquez le montant du dépôt : \n").toInt
    var montantconvertit = (montant * 0.97).toInt
    while (montant % 10 != 0) {
      println("Le montant doit être un multiple de 10.")
      montant = readLine("Indiquez le montant du dépôt : \n").toInt
    }
    if (devise == 2) {
      println("Le montant convertit est : " + montantconvertit + " CHF")
      montant = montantconvertit
    }
    return comptes(id) = comptes(id) + montant
  }
// Méthode opération 2 de retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
      var Coupures500 = 0
      var Coupures200 = 0
      var Coupures100 = 0 
      var Coupures50 = 0
      var Coupures20 = 0
      var Coupures10 = 0
      var QuantiteBillets = ""
      var ResteRetrait = 0
      var Coupures = ""
      val Plafond = comptes(id) * 0.10
    
      var devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR : \n").toInt
      
    
     while (devise == 1) { // partie en CHF //
       var montant = readLine("Indiquez le montant du retrait en CHF: \n").toInt
        if(montant % 10 != 0) {
          println("Veuillez saisir un montant étant un multiple de 10.\n")
          montant = readLine("Indiquez le montant du retrait en CHF: \n").toInt
        }
        if(montant % 10 == 0) {
           while (montant >= Plafond) {
             println("Veuillez saisir une valeur de retrait respectant le Plafond actuel de votre compte.\n")
             println("Votre Plafond de retrait autorisé est de : " + Plafond + " CHF.")
             montant = readLine("Indiquez le montant du retrait en CHF:\n").toInt
           }
          if (montant <= Plafond) {
           while (montant >= 10) {
              if (montant >= 200){
                Coupures = readLine("En 1) grosses Coupuress, 2) petites Coupuress: \n")
              }
              else{Coupures = "2"}
              if (Coupures == "1") {
                if (montant >= 500) { // BILLET 500
                   Coupures500 += (montant / 500)
                   ResteRetrait -= Coupures500 * 500


                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures500 + " billets(s) de 500 CHF.")
                   QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (500 * Coupures500)
                     comptes(id) -= Coupures500 * 500

                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures500) {
                       QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                     }
                     {
                       montant -= QuantiteBillets.toInt * 500
                       Coupures500 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 500
                     }
                   }
                 }
                
                if (montant >= 200) { // BILLET 200
                   Coupures200 += (montant / 200)
                   ResteRetrait -= Coupures200 * 200


                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures200
                       + " billets(s) de 200 CHF.")
                   QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (200 * Coupures200)
                     comptes(id) -= Coupures200 * 200
                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures200 ) {
                        QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                     }
                     {
                       montant -= QuantiteBillets.toInt * 200
                       Coupures200 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 200
                     }
                   }
                 }
                
                 if (montant >= 100) { // BILLET 100
                   Coupures100 += (montant / 100)
                   ResteRetrait -= Coupures100 * 100

                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures100 + " billet(s) de 100 CHF.")
                   QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (100 * Coupures100)
                     comptes(id) -= Coupures100 * 100
                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures100) {
                        QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                      
                     }
                     {
                       montant -= QuantiteBillets.toInt * 100
                       Coupures100 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 100
                     }
                   }
                 }

                if (montant >= 50) { // BILLET 50
                   Coupures50 += montant / 50
                   ResteRetrait -= Coupures50 * 50

                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures50 + " billet(s) de 50 CHF.")
                   QuantiteBillets
                = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (50 * Coupures50)
                     comptes(id) -= Coupures50 * 50

                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures50) {
                       QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                  
                     }
                     {
                       montant -= QuantiteBillets.toInt * 50
                       Coupures50 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 50
                   
                     }
                   }
                 }

                 if (montant >= 20) { // BILLET 20
                   Coupures20 += montant / 20
                   ResteRetrait -= Coupures20 * 20

                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures20 + " billet(s) de 20 CHF.")
                   QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (20 * Coupures20)
                     comptes(id) -= Coupures20 * 20

                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures20) {
                       QuantiteBillets= readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                     }
                     {
                       montant -= QuantiteBillets.toInt * 20
                       Coupures20 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 20
                     }
                   }
                 }

                if (montant >= 10) { // BILLET 10
                     Coupures10 += montant / 10
                     montant -= Coupures10 * 10
                     ResteRetrait -= Coupures10 * 10
                     comptes(id) -= Coupures10 * 10

                     println("Il reste " + montant + " CHF à distribuer.")
                     println("Vous pouvez obtenir au maximum " + Coupures10 + " billet(s) de 10 CHF.")
                   }
                 } // end du if Coupures == 1
              if (Coupures == "2") {
                if (montant >= 100) { // BILLET 100
                   Coupures100 += (montant / 100)
                   ResteRetrait -= Coupures100 * 100

                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures100 + " billet(s) de 100 CHF.")
                   QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (100 * Coupures100)
                     comptes(id) -= Coupures100 * 100

                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures100) {
                       QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                     }
                     {
                       montant -= QuantiteBillets.toInt * 100
                       Coupures100 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 100
                     }
                   }
                 }

                if (montant >= 50) { // BILLET 50
                   Coupures50 += montant / 50
                   ResteRetrait -= Coupures50 * 50

                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures50 + " billet(s) de 50 CHF.")
                   QuantiteBillets
                = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (50 * Coupures50)
                     comptes(id) -= Coupures50 * 50

                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures50) {
                       QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                   
                     }
                     {
                       montant -= QuantiteBillets.toInt * 50
                       Coupures50 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 50
                     }
                   }
                 }

                 if (montant >= 20) { // BILLET 20
                   Coupures20 += montant / 20
                   ResteRetrait -= Coupures20 * 20

                   println("Il reste " + montant + " CHF à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures20 + " billet(s) de 20 CHF.")
                   QuantiteBillets
                = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                   if (QuantiteBillets == "o") {
                     montant -= (20 * Coupures20)
                     comptes(id) -= Coupures20 * 20

                   }
                   else {
                     while (QuantiteBillets.toInt > Coupures20) {
                       QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                     }
                     {
                       montant -= QuantiteBillets.toInt * 20
                       Coupures20 = QuantiteBillets.toInt
                       comptes(id) -= QuantiteBillets.toInt * 20
                     }
                   }
                 }

                   if (montant >= 10) { // BILLET 10
                     Coupures10 += montant / 10
                     montant -= Coupures10 * 10
                     comptes(id) -= Coupures10 * 10

                     println("Il reste " + montant + " CHF à distribuer.")
                     println("Vous pouvez obtenir au maximum " + Coupures10 + " billet(s) de 10 CHF.")
                   }
                 } // end du if Coupures == 2
              // AFFICHAGE RESULTAT
              println("\n Veuillez retirer la somme demandée :\n ")

              if (Coupures500 >= 1) {println(Coupures500 + " billet(s) de 500 CHF.")}
              if (Coupures200 >= 1) {println(Coupures200 + " billet(s) de 200 CHF.")}
              if (Coupures100 >= 1) {println(Coupures100 + " billet(s) de 100 CHF.")}
              if (Coupures50 >= 1) {println(Coupures50 + " billet(s) de 50 CHF.")}
              if (Coupures20 >= 1) {println(Coupures20 + " billet(s) de 20 CHF.")}
              if (Coupures10 >= 1) {println(Coupures10 + " billet(s) de 10 CHF.")}

              println("\n Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est: " + comptes(id) + " CHF.\n")
             
           }// montant >= 10
         } // if montant <= Plafond 
       }// if montant multiple de 10
     } // if devise == 1 end
    
     if (devise == 2) { // partie en EUR //
       var montant = readLine("Indiquez le montant du retrait en EURO: \n").toInt
       if(montant % 10 != 0) {
         println("Veuillez saisir un montant étant un multiple de 10.\n")
         montant = readLine("Indiquez le montant du retrait en EURO: \n").toInt
       }
       if(montant % 10 == 0) {
          while (montant >= Plafond) {
            println("Veuillez saisir une valeur de retrait respectant le Plafond actuel de votre compte.\n")
            println("Votre Plafond de retrait autorisé est de : " + Plafond + " EURO.")
            montant = readLine("Indiquez le montant du retrait en EURO:\n").toInt
          }
          if (montant <= Plafond) {
            while (montant >= 10) {
              if (montant >= 100) { // BILLET 100
                 Coupures100 += (montant / 100)
                 ResteRetrait -= Coupures100 * 100

                 println("Il reste " + montant + " EURO à distribuer.")
                 println("Vous pouvez obtenir au maximum " + Coupures100 + " billet(s) de 100 EURO.")
                 QuantiteBillets
              = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                 if (QuantiteBillets == "o") {
                   montant -= (100 * Coupures100)
                   comptes(id) -= Coupures100 * 100

                 }
                 else {
                   while (QuantiteBillets.toInt > Coupures100) {
                     QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                 
                   }
                   {
                     montant -= QuantiteBillets.toInt * 100
                     Coupures100 = QuantiteBillets.toInt
                     comptes(id) -= QuantiteBillets.toInt * 100
                   }
                 }
               }

              if (montant >= 50) { // BILLET 50
                 Coupures50 += montant / 50
                 ResteRetrait -= Coupures50 * 50

                 println("Il reste " + montant + " EURO à distribuer.")
                 println("Vous pouvez obtenir au maximum " + Coupures50 + " billet(s) de 50 EURO.")
                 QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                 if (QuantiteBillets== "o") {
                   montant -= (50 * Coupures50)
                   comptes(id) -= Coupures50 * 50

                 }
                 else {
                   while (QuantiteBillets.toInt > Coupures50) {
                     QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                   }
                   {
                     montant -= QuantiteBillets.toInt * 50
                     Coupures50 = QuantiteBillets.toInt
                     comptes(id) -= QuantiteBillets.toInt * 50
                   }
                 }
               }

               if (montant >= 20) { // BILLET 20
                 Coupures20 += montant / 20
                 ResteRetrait -= Coupures20 * 20

                 println("Il reste " + montant + " EURO à distribuer.")
                 println("Vous pouvez obtenir au maximum " + Coupures20 + " billet(s) de 20 EURO.")
                 QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")

                 if (QuantiteBillets == "o") {
                   montant -= (20 * Coupures20)
                   comptes(id) -= Coupures20 * 20

                 }
                 else {
                   while (QuantiteBillets.toInt > Coupures20) {
                     QuantiteBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée.\n ")
                   }
                   {
                     montant -= QuantiteBillets.toInt * 20
                     Coupures20 = QuantiteBillets.toInt
                     comptes(id) -= QuantiteBillets.toInt * 20
                   }
                 }
               }

                 if (montant >= 10) { // BILLET 10
                   Coupures10 += montant / 10
                   montant -= Coupures10 * 10
                   comptes(id) -= Coupures10 * 10

                   println("Il reste " + montant + " EURO à distribuer.")
                   println("Vous pouvez obtenir au maximum " + Coupures10 + " billet(s) de 10 EURO.")
                 }
              // AFFICHAGE RESULTAT
                println("\n Veuillez retirer la somme demandée :\n ")
                if (Coupures100 >= 1) {println(Coupures100 + " billet(s) de 100 EUR.")}
                if (Coupures50 >= 1) {println(Coupures50 + " billet(s) de 50 EUR.")}
                if (Coupures20 >= 1) {println(Coupures20 + " billet(s) de 20 EUR.")}
                if (Coupures10 >= 1) {println(Coupures10 + " billet(s) de 10 EUR.")}
                  
                   
                   println("\n Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est: " + comptes(id) + " CHF.\n")

              
            } //while montant >= 10 end
          }// if montant <= Plafond end
        } // if montant multiple de 10 end
     }// if devise == 2 end
  } // def end
  
// Méthode opération 4 de changement de code pin
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var nouveaucode = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : \n")
    while (nouveaucode.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères.")
      nouveaucode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : \n")
    }
    return codespin(id) = nouveaucode
  }

  
  def main(args: Array[String]): Unit = {

//Variables communes
    var comptes = Array.fill(100)(1200.0) //CHF
    var codespin = Array.fill(100)("INTRO1234")
    var nbrclients = 100
    
    
// LANCEMENT DU PROGRAMME
    var id = readLine("Saisissez votre code identifiant ? ").toInt     
    var essai = 3 //Nombre d'essais
    
//Code identifiant
    if (id > nbrclients) {
          println("Cet identifiant n’est pas valable.")
          return
    } else println ("Identifiant valable.")
    
//Code PIN
    var pin = readLine("Saisissez votre code pin ? ").toString 
    while (pin != codespin(id)){
            essai -= 1
            println("Code pin erroné, il vous reste " + essai + " tentatives.")
            readLine("Saisissez votre code pin ? ").toString  
           
        if (essai == 1) {
              println("Trop d’erreurs, abandon de l’identification.\n")
              id = readLine("Saisissez votre code identifiant ? ").toInt
                  if (id > nbrclients) {
                        println("Cet identifiant n’est pas valable.")
                        return
                  } else println ("Identifiant valable.")
              pin = readLine("Saisissez votre code pin ? ").toString
                var essai = 3 
                  while (pin != codespin(id)){
                    essai -= 1
                    println("Code pin erroné, il vous reste " + essai + " tentatives.")
                    readLine("Saisissez votre code pin ? ").toString  
                  if (essai == 1) {
                    println("Trop d’erreurs, abandon de l’identification.\n")
                    }
            } 
        }
    }
    
  var operation = readLine ("\nChoisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre Choix : ").toInt //Choix de l'opération") 

// DEPOT
  while (operation == 1) {
    depot(id, comptes)
    println("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id))
    //Choix de l'opération") 
    operation = readLine ("\nChoisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre Choix : ").toInt 
  }
    
// RETRAIT
    while (operation == 2) {
      retrait(id, comptes)
      //Choix de l'opération") 
      operation = readLine ("\nChoisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre Choix : ").toInt 
    }
    
// CONSULTATION DU COMPTE
    while (operation == 3) {
      println("\nLe montant disponible sur votre compte est de: " + comptes(id) + " CHF. \n")
      //Choix de l'opération") 
      operation = readLine ("\nChoisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre Choix : ").toInt 
    }

// CHANGEMENT DE CODE PIN
  while (operation == 4) {
    changepin(id, codespin)
    println("Votre code pin a été modifié.\n")
    //Choix de l'opération") 
    operation = readLine ("\nChoisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre Choix : ").toInt 
  }
    
// TERMINER
  while (operation == 5) {
        println("\nFin des opérations, n’oubliez pas de récupérer votre carte.")
        var id = readLine("\nSaisissez votre code identifiant ? ").toInt     
            var essai = 3 //Nombre d'essais

        //Code identifiant
            if (id > nbrclients) {
                  println("Cet identifiant n’est pas valable.")
                  return
            } else println ("Identifiant valable.")

        //Code PIN
            var pin = readLine("Saisissez votre code pin ? ").toString 
            while (pin != codespin(id)){
                    essai -= 1
                    println("Code pin erroné, il vous reste " + essai + " tentatives.")
                    readLine("Saisissez votre code pin ? ").toString  

                if (essai == 1) {
                      println("Trop d’erreurs, abandon de l’identification.\n")
                      id = readLine("Saisissez votre code identifiant ? ").toInt
                          if (id > nbrclients) {
                                println("Cet identifiant n’est pas valable.")
                                return
                          } else println ("Identifiant valable.")
                      pin = readLine("Saisissez votre code pin ? ").toString
                        var essai = 3 
                          while (pin != codespin(id)){
                            essai -= 1
                            println("Code pin erroné, il vous reste " + essai + " tentatives.")
                            readLine("Saisissez votre code pin ? ").toString  
                          if (essai == 1) {
                            println("Trop d’erreurs, abandon de l’identification.\n")
                            }
                    } 
                }
          }
    operation = readLine ("\nChoisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre Choix : ").toInt //Choix de l'opération") 
      }
  }
}
    
  
