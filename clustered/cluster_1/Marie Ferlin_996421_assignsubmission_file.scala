//Assignment: Marie Ferlin_996421_assignsubmission_file

import io.StdIn._

object Main {

  //Methode Depot
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    //Il faut simplement prendre le code de la partie Depot de l'exercice 1, et remplacer la variable montant par comptes(id)
    
    var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt//Déclarer et remplire la variable nous indiquant quelle monaie (euros ou francs) on utilisera lors de cette transaction
     println("Indiquez le montant du dépôt >")
     var depot = readInt()//Déclarer et remplir la variable indiquant la somme que nous voulons déposer



     //Vérifier que le montant que le client veut déposer est bien un multiple de 10. Ce calcul ne changera pas que le montant soit en euros ou en francs, car il me sera donné directement dans la devise qui interesse le client.
     while (depot%10 != 0){//Soit tant que le montant à déposer n'est pas un multiple de 10 (j'utilise Mod)
       println("Le montant doit être un multiple de 10")
       println("Indiquez le montant du dépôt >")
       depot = readInt()
     }

     //Différence entre les opérations à faire, selon si on a choisit de faire les opérations en francs ou en euros
     if (devise == 1){//Si as pris des CHF
       comptes(id) = comptes(id) + depot
       //Controler que la somme que veut déposer est bien un multiple de 10
     } else if(devise == 2){//Si as pris des EUR
       comptes(id) = comptes(id)/0.95//Je convertit mon montant de francs en euros
       comptes(id) = comptes(id) + depot//J'ajoute mon dépot en euros à mon montant en euros
       comptes(id) = comptes(id)*0.95//Je reconvertit mon montant d'euros en francs
     }

     println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de " +comptes(id))
  }


  //Methode Changement de Pin
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var pin = readLine()
    while(pin.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      pin = readLine()
    }
    codespin(id) = pin
  }



  //Methode de Retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    //Encore une fois, il suffit de copier la méthode créée dans l'exercice 1 et de remplacer la variable montant par comptes(id)
     var devise = 0//Indique si choisit de faire un déport en francs ou en euros.
     var retrait = 0D//Indique le montant que l'utilisateur veut retirer
     var plafond = comptes(id)*0.1//Indique le plafond de retrait autorisé (10% du montant total du compte), pour des francs
     var coupures = 0//Indique en quel type de coupures on voudra recevoir notre argent

     //Demande à l'utilisateur de lui donner une valeur pour la devise. Se répète tands que la valeur n'est pas 1 ou 2
     do {
       devise = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
     } while (!(devise == 1 || devise == 2))



     //Si on veut retirer des Euros, le plafond doit etre calculé à partir du montant en Euros
     if (devise == 2){
       plafond = (comptes(id)/0.95)*0.1
     }


     //Demande le montant du retrait à l'utilisateur. Se répète tands que ce n'est pas un multiple de 10 ET qu'il ne dépasse pas le plafond de retrait autorisé
     print("Indiquez le montant du retrait >")
     retrait = readInt()



     while(retrait%10 != 0 || retrait > plafond){
       if (retrait%10 != 0){//Si le montant à retirer n'est pas un multiple de 10
         println("Le montant doit être un multiple de 10.")
         print("\nIndiquez le montant du retrait >")
         retrait = readInt()
       }
       if (retrait > plafond){//Si le montant à retirer est supérieur au montant autorisé
         println("Votre plafond de retrait autorisé est de : "+plafond)
         print("\nIndiquez le montant du retrait >")
         retrait = readInt()
       }

     }




     //Détermine quelles coupures on va utiliser
     if (devise == 1){//Si je veut retirer des CHF

       //Je vais écrire quel sera le montant total dans le compte client après le retrait (je le fais maintenant car plus tard, retrait sera utilisé pour calculer autre chose, et sa valeur sera modifiée)
       comptes(id) = comptes(id) - retrait


       //Je commence par déterminer si le retrait se fera en grosses coupures ou petites coupures
       if (retrait < 200){//Si le montant à retirer est < 200, alors il se fera forcement en petites coupures
         coupures = 2
         println("Votre retrait se fera en petites coupures.")
       }


       while(!(coupures == 1 || coupures == 2)){
         coupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
       }


       //Indique comment le découpage va se faire
       if(coupures == 2){//Petites coupures
         //J'ai fait les Grandes coupures avant de faire les petites coupures. Par conséquent, il y a plus de commentaires expliquant mon raisonnement dans le code des grandes coupures puisque le principe reste le meme.


         //Pour un billet de 100 CHF
         var billet = 100
         var nbBillets1 = (retrait/billet).toInt
         if(nbBillets1 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets1+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
             if (nbBillets1 <= suite.toInt){
               while (nbBillets1 <= suite.toInt && suite != "o"){
                 suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
               }
             }
             if(suite != "o"){
               nbBillets1 = suite.toInt
             }
           }
         }
         retrait = retrait - billet*nbBillets1


         //Pour un Billet de 50 CHF
         billet = 50
         var nbBillets2 = (retrait/billet).toInt
         if(nbBillets2 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets2+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
             if (nbBillets2 <= suite.toInt){
                while (nbBillets2 <= suite.toInt && suite != "o"){
                  suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                }
              }
              if(suite != "o"){
                nbBillets2 = suite.toInt
             }
           }
         }
         retrait = retrait - billet*nbBillets2


         //Pour un billet de 20 CHF
         billet = 20
         var nbBillets3 = (retrait/billet).toInt
         if(nbBillets3 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets3+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
           if (nbBillets3 <= suite.toInt){
              while (nbBillets3 <= suite.toInt && suite != "o"){
                suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
            }
            if(suite != "o"){
              nbBillets3 = suite.toInt
            }
           }
         }
         retrait = retrait - billet*nbBillets3


         //Pour un billet de 10 CHF
         billet = 10
         var nbBillets4 = (retrait/billet).toInt


         //Affichage des quantités de billets à retirer
         println("Veuillez retirer la somme demandée :")
         if(nbBillets1 != 0){
           printf("%d billet(s) de 100 CHF\n",nbBillets1)
         }
         if(nbBillets2 != 0){
           printf("%d billet(s) de 50 CHF\n",nbBillets2)
         }
         if(nbBillets3 != 0){
           printf("%d billet(s) de 20 CHF\n",nbBillets3)
         }
         if(nbBillets4 != 0){
           printf("%d billet(s) de 10 CHF\n",nbBillets4)
         }


       } else if (coupures == 1){//Grandes coupures
         //Ce schéma va se répéter pour tous types de coupures (y compris pour les EUR), avec différentes valeurs initiales

         //Pour un billet de 500 CHF
         var billet = 500
         var nbBillets1 = (retrait/billet).toInt//Nombre de billets de 500 CHF que l'on peut trouver dans le montant à retirer

         if(nbBillets1 != 0){//Si le nombre de billets que l'on peut enlever est supérieur à 0 ... S'il n'est pas supérieur à 0, nous passons directement au billet suivant.
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets1+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){//Si je tape un nouveau nombre de billets

             if (nbBillets1 <= suite.toInt){//Si mon nouveau nombre de billets est supérieur ou égal à mon ancien nombre de billets (il ne doit pas l'etre)
               while (nbBillets1 <= suite.toInt && suite != "o"){//Soit tand que je n'ai pas fournis une valeur appropriée à suite, je continue
                 suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
               }
             }
             if (suite != "o"){//Maintenant que je sui certaine que mon nombre de billet est possible, ET que suite n'est pas égale à o, je peut donner à billet1 le nouveau nombre de billets
               nbBillets1 = suite.toInt
             }
           }
         }
         retrait = retrait - billet*nbBillets1//Soit le montant à retirer qu'il me reste à définir en coupures diminue de ce que j'ai ici défini en coupures.


         //Pour un billet de 200 CHF. Les seules choses qui changent sont la valeur de la variable billet (qui prends une valeur de 200 CHF), et la variable nbBillets1, qui sera remplacée par la variable nbBillets2.
         //Je doit conserver les variables nbBillets individuelles, car elles me seront necessaires lors de l'affichage des quantités de billets que l'utilisateur recevra
         billet = 200
         var nbBillets2 = (retrait/billet).toInt
         if(nbBillets2 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets2+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
             if (nbBillets2 <= suite.toInt){
                while (nbBillets2 <= suite.toInt && suite != "o"){
                  suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                }
              }
              if(suite != "o"){
                nbBillets2 = suite.toInt
              }
           }
         }
         retrait = retrait - billet*nbBillets2


         //Pour un billet de 100 CHF
         billet = 100
         var nbBillets3 = (retrait/billet).toInt
         if(nbBillets3 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets3+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
             if (nbBillets3 <= suite.toInt){
                while (nbBillets3 <= suite.toInt && suite != "o"){
                  suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                }
              }
              if(suite != "o"){
                nbBillets3 = suite.toInt
              }
           }
         }
         retrait = retrait - billet*nbBillets3


         //Pour un billet de 50 CHF
         billet = 50
         var nbBillets4 = (retrait/billet).toInt
         if(nbBillets4 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets4+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
             if (nbBillets4 <= suite.toInt){
                while (nbBillets4 <= suite.toInt && suite != "o"){
                  suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                }
              }
              if(suite != "o"){
                nbBillets4 = suite.toInt
              }
           }
         }
         retrait = retrait - billet*nbBillets4


         //Pour un billet de 20 CHF
         billet = 20
         var nbBillets5 = (retrait/billet).toInt
         if(nbBillets5 != 0){
           println("Il reste "+retrait+" CHF à distribuer")
           println("Vous pouvez obtenir au maximum "+nbBillets5+" billet(s) de "+billet+" CHF")
           var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
           if(suite != "o"){
             if (nbBillets5 <= suite.toInt){
                while (nbBillets5 <= suite.toInt && suite != "o"){
                  suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                }
              }
              if(suite != "o"){
                nbBillets5 = suite.toInt
              }
           }
         }
         retrait = retrait - billet*nbBillets5


         //Pour un billet de 10 CHF. Contrairement aux billets précédents, le code n'est pas simplement une copie du code pour un billet de 500 CHF. En effet, ici nous ne pouvons pas passer à un billet plus petit. Tout ce qui reste doit etre donné en billet(s) de 10 CHF.
         billet = 10
         var nbBillets6 = (retrait/billet).toInt



         //Affichage des quantités de billets à retirer
         println("Veuillez retirer la somme demandée :")
         if(nbBillets1 != 0){
           printf("%d billet(s) de 500 CHF\n",nbBillets1)
         }
         if(nbBillets2 != 0){
           printf("%d billet(s) de 200 CHF\n",nbBillets2)
         }
         if(nbBillets3 != 0){
           printf("%d billet(s) de 100 CHF\n",nbBillets3)
         }
         if(nbBillets4 != 0){
           printf("%d billet(s) de 50 CHF\n",nbBillets4)
         }
         if(nbBillets5 != 0){
           printf("%d billet(s) de 20 CHF\n",nbBillets5)
         }
         if(nbBillets6 != 0){
           printf("%d billet(s) de 10 CHF\n",nbBillets6)
         }
       }

     } else if (devise == 2){//Si je veux retirer des EUR
       //Je commence par convertir mon montant total en euros (le montant à retirer est déjà en euros)
       comptes(id) = comptes(id)/0.95//Je convertit mon montant de francs en euros
       comptes(id) = comptes(id) - retrait//Enlever le montant retiré par l'utilisateur du montant total du compte.
       comptes(id) = comptes(id)*0.95//Reconvertir mon montant total en francs

       //Ici, uniquement l'otion des petites coupures. Je vais simplement copier le code utilisé pour les petites coupures en CHF, et remplacer tous les CHF par des EUR dans les print.

       //Pour un billet de 100 EUR
       var billet = 100
       var nbBillets1 = (retrait/billet).toInt
       if(nbBillets1 != 0){
         println("Il reste "+retrait+" EUR à distribuer")
         println("Vous pouvez obtenir au maximum "+nbBillets1+" billet(s) de "+billet+" EUR")
         var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
         if(suite != "o"){
           if (nbBillets1 <= suite.toInt){
              while (nbBillets1 <= suite.toInt && suite != "o"){
                suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
            }
            if(suite != "o"){
              nbBillets1 = suite.toInt
            }
         }
       }
       retrait = retrait - billet*nbBillets1


       //Pour un Billet de 50 EUR
       billet = 50
       var nbBillets2 = (retrait/billet).toInt
       if(nbBillets2 != 0){
         println("Il reste "+retrait+" EUR à distribuer")
         println("Vous pouvez obtenir au maximum "+nbBillets2+" billet(s) de "+billet+" EUR")
         var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
         if(suite != "o"){
           if (nbBillets2 <= suite.toInt){
              while (nbBillets2 <= suite.toInt && suite != "o"){
                suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
            }
            if(suite != "o"){
              nbBillets2 = suite.toInt
            }
         }
       }
       retrait = retrait - billet*nbBillets2


       //Pour un billet de 20 EUR
       billet = 20
       var nbBillets3 = (retrait/billet).toInt
       if(nbBillets3 != 0){
         println("Il reste "+retrait+" EUR à distribuer")
         println("Vous pouvez obtenir au maximum "+nbBillets3+" billet(s) de "+billet+" EUR")
         var suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
         if(suite != "o"){
           if (nbBillets3 <= suite.toInt){
              while (nbBillets3 <= suite.toInt && suite != "o"){
                suite = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
            }
            if(suite != "o"){
              nbBillets3 = suite.toInt
            }
         }
       }
       retrait = retrait - billet*nbBillets3


       //Pour un billet de 10 EUR
       billet = 10
       var nbBillets4 = (retrait/billet).toInt


       //Affichage des quantités de billets à retirer
       println("Veuillez retirer la somme demandée :")
       if(nbBillets1 != 0){
         printf("%d billet(s) de 100 EUR\n",nbBillets1)
       }
       if(nbBillets2 != 0){
         printf("%d billet(s) de 50 EUR\n",nbBillets2)
       }
       if(nbBillets3 != 0){
         printf("%d billet(s) de 20 EUR\n",nbBillets3)
       }
       if(nbBillets4 != 0){
         printf("%d billet(s) de 10 EUR\n",nbBillets4)
       }
     }

     //Afficher le montant restant dans le compte après ce retrait
     printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :%.1f",comptes(id))

  }


  
  def main(args: Array[String]): Unit = {
    var fin = false
    var retour = false

    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)//Tableau donnant l'argent en compte de chaque client (identifié par leur id)
    var codespin = Array.fill(nbclients)("INTRO1234")//Tableau donnant le code pin du compte de chaque client (identifié par leur id)
    
    var id = 0//Je lui donne une valeur afin de pouvoir le déclarer ici
    var pin = "_"//Je lui donne une valeur afin de pouvoir le déclarer ici

    var choix = 0//Choix de l'opération. 0 ne corresponds à aucune opération
    
    
    while (fin != true){//Boucle qui deviendra vraie uniquement si l'id fournit est faux. Sinon, on continue éternellement meme en changeant d'utilisateur
      println("Saisissez votre code identifiant >")
      id = readInt()
      
      if (id >= nbclients && fin == false){//Active si l'id n'est pas correct et la variable activant la fin n'est pas vraie
        println("Cet identifiant n’est pas valable.")
        fin = true
      } 
      
      else if (id < nbclients && fin == false){//Active si l'id est correct et la variable activant la fin n'est pas vraie
        println("Saisissez votre code pin >")
        pin = readLine()

        if (pin != codespin(id)){ //Active si le pin n'est pas correct

          var nbTentaRestantes = 2

          while(nbTentaRestantes > 0 && pin != codespin(id)){//Continue tands qu'il reste des tentatives et que le pin est faux
            println("Code pin erroné, il vous reste "+nbTentaRestantes+" tentatives >")
            pin = readLine()
            nbTentaRestantes -= 1
          }
          if (nbTentaRestantes == 0 && pin != codespin(id)) {//Active tandis si le pin est faux et qu'il ne reste plus de tentatives
            println("Trop d’erreurs, abandon de l’identification")
            retour = true
          }
        } 
        
        if (pin == codespin(id) && retour == false){//Active si le pin est faux et la variable nous retournant au choix de l'id est fausse

          var finOpClient = false
          while(finOpClient == false){//tandis qu'un client spécifique n'a pas terminé ses opérations

            
            //Choix de l'Opération
            println("\nChoisissez votre opération :\n  1) Dépôt\n  2) Retrait\n  3) Consultation du compte\n  4) Changement du code pin\n  5) Terminer")
            choix = readLine("Votre choix : ").toInt



            //Opération de Consultation
            if (choix == 1){
              depot(id, comptes)
            }

            //Opération de Retrait
            if (choix == 2){
              retrait(id,comptes)
            }

            //Opération de Consultation du Compte
            if (choix == 3){
              println("Le montant disponible sur votre compte est de : "+comptes(id))
            }

            //Opération de Changement de Code PIN
            if (choix == 4){
              changepin(id, codespin)
            }

            //Terminer
            if (choix == 5){
              println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
              finOpClient = true
            }
          }
        }
      }
    }
  }
}
