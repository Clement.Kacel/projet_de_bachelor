//Assignment: Santiago Franz Aguirre Bendezu_996549_assignsubmission_file

import scala.io.StdIn._

object Main {
  def depot(id : Int, montantdisponible : Array[Double]) : Unit = {
    var tauxdechange1 = 0.95
    var montantdepot1 = 0.0
    var devisedepot = readLine("Indiquez la devise du dépôt : \nCHF ; \nEUR > ").toString
    var montantdepot = readLine("Indiquez le montant du dépôt > ").toInt
    while (montantdepot % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      montantdepot = readLine("Indiquez le montant du dépôt > ").toInt
    }
    if (devisedepot == "EUR") {
    montantdepot1 = montantdepot.toDouble * tauxdechange1
    montantdisponible(id) += montantdepot1
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantdisponible(id))
      print("CHF\n")
      }
    else {
      montantdisponible(id) += montantdepot
      printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantdisponible(id))
      print("CHF\n")
    }
  }
  def retrait(id : Int, montantdisponible : Array[Double]) : Unit = {
    var deviseretrait = readLine("Indiquez la devise \nCHF, \nEUR >").toString
      while (deviseretrait != "CHF" && deviseretrait != "EUR") {
      deviseretrait = readLine("Indiquez la devise \nCHF, \nEUR >").toString
      }
       var montantretrait = readLine("Indiquez le montant du retrait > ").toInt
      while (montantretrait % 10 != 0 || montantretrait > montantdisponible(id) * 0.1) {
        if (montantretrait % 10 != 0) {
          println("Le montant doit être un multiple de 10")
        }
        if (montantretrait > montantdisponible(id) * 0.1) {
          println("Votre plafond de retrait autorisé est de :" + montantdisponible(id) * 0.1)
        }
        montantretrait = readLine("Indiquez le montant du retrait > ").toInt
      }
      var b500retires = 0
      var b200retires = 0
      var b100retires = 0
      var b50retires = 0
      var b20retires = 0
      var b10retires = 0
      var montantretrait1 = 0.0
      var tauxdechange = 0.95
      if (deviseretrait == "CHF") {
        if (montantretrait >= 200) {
          var coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          while (coupure != 1 && coupure != 2) {
            coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          }
          if (coupure == 1) {
            var b500disponibles = (montantretrait / 500).toInt
            if (b500disponibles > 0) {
              var b500retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b500disponibles + " billet(s) de 500 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString                  
              if (b500retiresInput == "o") {
                b500retires = b500disponibles.toInt
              }
              else {
                b500retires = (b500retiresInput).toInt
              }
              montantretrait = montantretrait - 500 * b500retires.toInt
            }
            var b200disponibles = (montantretrait / 200).toInt
            if (b200disponibles > 0) {
              var b200retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b200disponibles + " billet(s) de 200 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b200retiresInput == "o") {
                b200retires = b200disponibles
              }
              else {
                b200retires = (b200retiresInput).toInt
              }
              montantretrait = montantretrait - 200 * b200retires.toInt
            }
            var b100disponibles = (montantretrait / 100).toInt
            if (b100disponibles > 0) {
              var b100retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b100disponibles + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b100retiresInput == "o") {
                b100retires = b100disponibles
              }
              else {
                b100retires = (b100retiresInput).toInt
              }
              montantretrait = montantretrait - 100 * b100retires.toInt
            }
            var b50disponibles = (montantretrait / 50).toInt
            if (b50disponibles > 0) {
              var b50retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b50disponibles + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b50retiresInput == "o") {
                b50retires = b50disponibles
              }
              else {
                b50retires = (b50retiresInput).toInt
              }
              montantretrait = montantretrait - 50 * b50retires.toInt
            }
            var b20disponibles = (montantretrait / 20).toInt
            if (b20disponibles > 0) {
              var b20retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b20disponibles + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b20retiresInput == "o") {
                b20retires = b20disponibles
              }
              else {
                b20retires = (b20retiresInput).toInt
              }
              montantretrait = montantretrait - 20 * b20retires.toInt
            }
            var b10disponibles = (montantretrait / 10).toInt
            if (b10disponibles > 0) {
              var b10retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b10disponibles + " billet(s) de 10 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b10retiresInput == "o") {
                b10retires = b10disponibles
              }
              else {
                b10retires = (b10retiresInput).toInt
              }
              montantretrait = montantretrait - 10 * b10retires.toInt
            }
            if (montantretrait == 0) {
              println("Veuillez retirer la somme demandée : \n")
              if (b500retires.toInt > 0) {
                println(b500retires.toInt + " billet(s) de 500 CHF")
              }
              if (b200retires.toInt > 0) {
                println(b200retires.toInt + " billet(s) de 200 CHF")
              }
              if (b100retires.toInt > 0) {
                println(b100retires.toInt + " billet(s) de 100 CHF")
              }
              if (b50retires.toInt > 0) {
                println(b50retires.toInt + " billet(s) de 50 CHF")
              }
              if (b20retires.toInt > 0) {
                println(b20retires.toInt + " billet(s) de 20 CHF")
              }
              if (b10retires.toInt > 0) {
                println(b10retires.toInt + " billet(s) de 10 CHF")
              }
            }
            else {
              println("Veuillez retirer la somme demandée : \n")
              if (b500retires.toInt > 0) {
                println(b500retires.toInt + " billet(s) de 500 CHF")
              }
              if (b200retires.toInt > 0) {
                println(b200retires.toInt + " billet(s) de 200 CHF")
              }
              if (b100retires.toInt > 0) {
                println(b100retires.toInt + " billet(s) de 100 CHF")
              }
              if (b50retires.toInt > 0) {
                println(b50retires.toInt + " billet(s) de 50 CHF")
              }
              if (b20retires.toInt > 0) {
                println(b20retires.toInt + " billet(s) de 20 CHF")
              }
                println(b10disponibles + " billet(s) de 10 CHF")
            }
            montantretrait = b500retires * 500 + b200retires * 200 + b100retires * 100 + b50retires * 50 + b20retires * 20 + b10disponibles * 10
            montantdisponible(id) -= montantretrait
            printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantdisponible(id))
            print("CHF\n")
          }
          if (coupure == 2) {
            var b100disponibles = (montantretrait / 100).toInt
            if (b100disponibles > 0) {
              var b100retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b100disponibles + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b100retiresInput == "o") {
                b100retires = b100disponibles
              }
              else {
                b100retires = (b100retiresInput).toInt
              }
              montantretrait = montantretrait - 100 * b100retires.toInt
            }
            var b50disponibles = (montantretrait / 50).toInt
            if (b50disponibles > 0) {
              var b50retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b50disponibles + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b50retiresInput == "o") {
                b50retires = b50disponibles
              }
              else {
                b50retires = (b50retiresInput).toInt
              }
              montantretrait = montantretrait - 50 * b50retires.toInt
            }
            var b20disponibles = (montantretrait / 20).toInt
            if (b20disponibles > 0) {
              var b20retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b20disponibles + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b20retiresInput == "o") {
                b20retires = b20disponibles
              }
              else {
                b20retires = (b20retiresInput).toInt
              }
              montantretrait = montantretrait - 20 * b20retires.toInt
            }
            var b10disponibles = (montantretrait / 10).toInt
            if (b10disponibles > 0) {
              var b10retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b10disponibles + " billet(s) de 10 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b10retiresInput == "o") {
                b10retires = b10disponibles
              }
              else {
                b10retires = (b10retiresInput).toInt
              }
              montantretrait = montantretrait - 10 * b10retires.toInt
            }
            if (montantretrait == 0) {
              println("Veuillez retirer la somme demandée : \n")
              if (b500retires.toInt > 0) {
                println(b500retires.toInt + " billet(s) de 500 CHF")
              }
              if (b200retires.toInt > 0) {
                println(b200retires.toInt + " billet(s) de 200 CHF")
              }
              if (b100retires.toInt > 0) {
                println(b100retires.toInt + " billet(s) de 100 CHF")
              }
              if (b50retires.toInt > 0) {
                println(b50retires.toInt + " billet(s) de 50 CHF")
              }
              if (b20retires.toInt > 0) {
                println(b20retires.toInt + " billet(s) de 20 CHF")
              }
              if (b10retires.toInt > 0) {
                println(b10retires.toInt + " billet(s) de 10 CHF")
              }
            }
            else {
              println("Veuillez retirer la somme demandée : \n")
              if (b500retires.toInt > 0) {
                println(b500retires.toInt + " billet(s) de 500 CHF")
              }
              if (b200retires.toInt > 0) {
                println(b200retires.toInt + " billet(s) de 200 CHF")
              }
              if (b100retires.toInt > 0) {
                println(b100retires.toInt + " billet(s) de 100 CHF")
              }
              if (b50retires.toInt > 0) {
                println(b50retires.toInt + " billet(s) de 50 CHF")
              }
              if (b20retires.toInt > 0) {
                println(b20retires.toInt + " billet(s) de 20 CHF")
              }
                println(b10disponibles + " billet(s) de 10 CHF")
            }
            montantretrait = b500retires * 500 + b200retires * 200 + b100retires * 100 + b50retires * 50 + b20retires * 20 + b10disponibles * 10
            montantdisponible(id) -= montantretrait
            printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantdisponible(id))
            print("CHF\n")
          }
        }
        else {
          var b100disponibles = (montantretrait / 100).toInt
              if (b100disponibles > 0) {
                var b100retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b100disponibles + " billet(s) de 100 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
                if (b100retiresInput == "o") {
                  b100retires = b100disponibles
                }
                else {
                  b100retires = (b100retiresInput).toInt
                }
                montantretrait = montantretrait - 100 * b100retires.toInt
              }
              var b50disponibles = (montantretrait / 50).toInt
              if (b50disponibles > 0) {
                var b50retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b50disponibles + " billet(s) de 50 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
                if (b50retiresInput == "o") {
                  b50retires = b50disponibles
                }
                else {
                  b50retires = (b50retiresInput).toInt
                }
                montantretrait = montantretrait - 50 * b50retires.toInt
              }
              var b20disponibles = (montantretrait / 20).toInt
              if (b20disponibles > 0) {
                var b20retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b20disponibles + " billet(s) de 20 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
                if (b20retiresInput == "o") {
                  b20retires = b20disponibles
                }
                else {
                  b20retires = (b20retiresInput).toInt
                }
                montantretrait = montantretrait - 20 * b20retires.toInt
              }
              var b10disponibles = (montantretrait / 10).toInt
              if (b10disponibles > 0) {
                var b10retiresInput = readLine("\nIl reste " + montantretrait + " CHF à distribuer \nVous pouvez obtenir au maximum " + b10disponibles + " billet(s) de 10 CHF \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
                if (b10retiresInput == "o") {
                  b10retires = b10disponibles
                }
                else {
                  b10retires = (b10retiresInput).toInt
                }
                montantretrait = montantretrait - 10 * b10retires.toInt
              }
              if (montantretrait == 0) {
                println("Veuillez retirer la somme demandée : \n")
                if (b500retires.toInt > 0) {
                  println(b500retires.toInt + " billet(s) de 500 CHF")
                }
                if (b200retires.toInt > 0) {
                  println(b200retires.toInt + " billet(s) de 200 CHF")
                }
                if (b100retires.toInt > 0) {
                  println(b100retires.toInt + " billet(s) de 100 CHF")
                }
                if (b50retires.toInt > 0) {
                  println(b50retires.toInt + " billet(s) de 50 CHF")
                }
                if (b20retires.toInt > 0) {
                  println(b20retires.toInt + " billet(s) de 20 CHF")
                }
                if (b10retires.toInt > 0) {
                  println(b10retires.toInt + " billet(s) de 10 CHF")
                }
              }
              else {
                println("Veuillez retirer la somme demandée : \n")
                if (b500retires.toInt > 0) {
                  println(b500retires.toInt + " billet(s) de 500 CHF")
                }
                if (b200retires.toInt > 0) {
                  println(b200retires.toInt + " billet(s) de 200 CHF")
                }
                if (b100retires.toInt > 0) {
                  println(b100retires.toInt + " billet(s) de 100 CHF")
                }
                if (b50retires.toInt > 0) {
                  println(b50retires.toInt + " billet(s) de 50 CHF")
                }
                if (b20retires.toInt > 0) {
                  println(b20retires.toInt + " billet(s) de 20 CHF")
                }
                  println(b10disponibles + " billet(s) de 10 CHF")
              }
          montantretrait = b500retires * 500 + b200retires * 200 + b100retires * 100 + b50retires * 50 + b20retires * 20 + b10disponibles * 10
          montantdisponible(id) -= montantretrait
          printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantdisponible(id))
          print("CHF\n")
        }
      }
      else if (deviseretrait == "EUR") {
        var b100disponibles = (montantretrait / 100).toInt
            if (b100disponibles > 0) {
              var b100retiresInput = readLine("\nIl reste " + montantretrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + b100disponibles + " billet(s) de 100 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b100retiresInput == "o") {
                b100retires = b100disponibles
              }
              else {
                b100retires = (b100retiresInput).toInt
              }
              montantretrait = montantretrait - 100 * b100retires.toInt
            }
            var b50disponibles = (montantretrait / 50).toInt
            if (b50disponibles > 0) {
              var b50retiresInput = readLine("\nIl reste " + montantretrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + b50disponibles + " billet(s) de 50 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b50retiresInput == "o") {
                b50retires = b50disponibles
              }
              else {
                b50retires = (b50retiresInput).toInt
              }
              montantretrait = montantretrait - 50 * b50retires.toInt
            }
            var b20disponibles = (montantretrait / 20).toInt
            if (b20disponibles > 0) {
              var b20retiresInput = readLine("\nIl reste " + montantretrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + b20disponibles + " billet(s) de 20 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b20retiresInput == "o") {
                b20retires = b20disponibles
              }
              else {
                b20retires = (b20retiresInput).toInt
              }
              montantretrait = montantretrait - 20 * b20retires.toInt
            }
            var b10disponibles = (montantretrait / 10).toInt
            if (b10disponibles > 0) {
              var b10retiresInput = readLine("\nIl reste " + montantretrait + " EUR à distribuer \nVous pouvez obtenir au maximum " + b10disponibles + " billet(s) de 10 EUR \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
              if (b10retiresInput == "o") {
                b10retires = b10disponibles
              }
              else {
                b10retires = (b10retiresInput).toInt
              }
              montantretrait = montantretrait - 10 * b10retires.toInt
            }
            if (montantretrait == 0) {
              println("Veuillez retirer la somme demandée : \n")
              if (b500retires.toInt > 0) {
                println(b500retires.toInt + " billet(s) de 500 EUR")
              }
              if (b200retires.toInt > 0) {
                println(b200retires.toInt + " billet(s) de 200 EUR")
              }
              if (b100retires.toInt > 0) {
                println(b100retires.toInt + " billet(s) de 100 EUR")
              }
              if (b50retires.toInt > 0) {
                println(b50retires.toInt + " billet(s) de 50 EUR")
              }
              if (b20retires.toInt > 0) {
                println(b20retires.toInt + " billet(s) de 20 EUR")
              }
              if (b10retires.toInt > 0) {
                println(b10retires.toInt + " billet(s) de 10 EUR")
              }
            }
            else {
              println("Veuillez retirer la somme demandée : \n")
              if (b500retires.toInt > 0) {
                println(b500retires.toInt + " billet(s) de 500 EUR")
              }
              if (b200retires.toInt > 0) {
                println(b200retires.toInt + " billet(s) de 200 EUR")
              }
              if (b100retires.toInt > 0) {
                println(b100retires.toInt + " billet(s) de 100 EUR")
              }
              if (b50retires.toInt > 0) {
                println(b50retires.toInt + " billet(s) de 50 EUR")
              }
              if (b20retires.toInt > 0) {
                println(b20retires.toInt + " billet(s) de 20 EUR")
              }
                println(b10disponibles + " billet(s) de 10 EUR")
            }
        montantretrait = b100retires * 100 + b50retires * 50 + b20retires * 20 + b10disponibles * 10
        montantretrait1 = tauxdechange * montantretrait.toDouble
        montantdisponible(id) -= montantretrait1
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantdisponible(id))
        print("CHF\n")
    }
  }
  def changepin(id : Int, codepin : Array[String]) : Unit = {
    codepin(id) = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toString
    while (codepin(id).length < 8) {
      println("\nVotre code pin ne contient pas au moins 8 caractères")
      codepin(id) = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toString
    }
  }
  def main(args: Array[String]): Unit = {
    var Depot = 1
    var Retrait = 2
    var ConsultationDuCompte = 3
    var Changementcodepin = 4
    var Terminer = 5
    var nbclients = 100
    var id = 0
    var codepin = Array.fill(nbclients)("INTRO1234")
    var choixcodepin = "0"
    var montantdisponible = Array.fill(nbclients)(1200.0)
    var tentatives = 3

    while (choixcodepin != codepin(id)) {
      id = readLine("\nSaisissez votre code identifiant > ").toInt
      if (id > nbclients) {
        println("\nCet identifiant n'est pas valable.")
        sys.exit
      }
      choixcodepin = readLine("\nSaisissez votre code pin > ").toString
      var tentatives = 3
      while (tentatives > 1 && choixcodepin != codepin(id)) {
          tentatives -= 1
          println ("\nCode pin erroné, il vous reste " + tentatives + " tentatives")
          choixcodepin = readLine("\nSaisissez votre code pin > ").toString
          if (tentatives == 1 && choixcodepin != codepin(id)) {
          println ("\nTrop d'erreurs, abandon de l'identification")
        }
      }
    }
    var choix = readLine("\n\nChoisissez votre opération : \n 1) Depot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix :").toInt
    while (choix == 1 || choix == 2 || choix == 3 || choix == 4 || choix == 5) {
        if (choix == 1) {
          depot(id, montantdisponible)
          choix = readLine("\n\nChoisissez votre opération : \n 1) Depot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix :").toInt
        }
        if (choix == 2) {
          retrait(id, montantdisponible)
          choix = readLine("\n\nChoisissez votre opération : \n 1) Depot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix :").toInt
        }
        if (choix == 3) {
          println ("Le montant disponible sur votre compte est de : " + montantdisponible(id) + " CHF")
          choix = readLine("\n\nChoisissez votre opération : \n 1) Depot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix :").toInt
        }
        if (choix == 4) {
          changepin(id, codepin)
          choix = readLine("\n\nChoisissez votre opération : \n 1) Depot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix :").toInt
        }
        if (choix == 5) {
          println ("\nFin des opérations, n’oubliez pas de récupérer votre carte.")
        var choixcodepin = "0"
        while (choixcodepin != codepin(id)) {
          id = readLine("\nSaisissez votre code identifiant > ").toInt
          if (id > nbclients) {
            println("\nCet identifiant n'est pas valable.")
            sys.exit
          }
          choixcodepin = readLine("\nSaisissez votre code pin > ").toString
          var tentatives = 3
          while (tentatives > 1 && choixcodepin != codepin(id)) {
              tentatives -= 1
              println ("\nCode pin erroné, il vous reste " + tentatives + " tentatives")
              choixcodepin = readLine("\nSaisissez votre code pin > ").toString
              if (tentatives == 1 && choixcodepin != codepin(id)) {
              println ("\nTrop d'erreurs, abandon de l'identification")
            }
          }
        }
          choix = readLine("\n\nChoisissez votre opération : \n 1) Depot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement de code pin \n 5) Terminer \n Votre choix :").toInt
      }
    }
  }
}