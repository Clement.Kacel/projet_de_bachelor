//Assignment: Rania Hemissi_996484_assignsubmission_file

import scala.collection.mutable.ListBuffer
import scala.io.StdIn.readLine

object Main {

  def main(args: Array[String]): Unit = {
    var codespin: Array[String] = new Array[String](100)
    var comptes: Array[Double] = new Array[Double](100)

    val petites_coupures: List[Int] = List(100, 50, 20, 10)
    val grosses_coupures: List[Int] = List(500, 200, 100, 50, 20, 10)
    val nbrclients: Int = 100
    val nbrCaractereAttendu: Int = 8

    // var solde_compte: Double = 1200.0
    var tentatives: Int = 0
    var code_pin_saisie: String = ""
    var tentatives_restantes: Int = 3
    var choix: Int = 0
    var devise: Int = 0
    var continue: Boolean = true
    var montant_retrait: Int = 0
    val chaineRepetee: String = "INTRO1234"
    var nbr_de_choix: Int = 0

    var coupure: Int = 0
    var montant_restant: Int = 0
    var nbr_billet: Int = 0

    codespin = Array.fill(100)("INTRO1234")
    comptes = Array.fill(100)(1200.0)

    // 1ere methode : DEPOT

    def depot(id: Int, comptes: Array[Double]): Unit = {
      while (devise < 1 || devise > 2) {
        println("Indiquez la devise du depot : 1 CHF, 2:EUR >")
        devise = readLine("->").toInt
      }

      println("Indiquez le montant du depot :")
      var montant_depot = readLine("->").toDouble

      while (montant_depot % 10 != 0) {
        println("Le montant doit etre un multiple de 10.")
        println("Indiquez le montant du depot :")
        montant_depot = readLine("->").toDouble
      }

      if (devise == 1) {
        montant_depot = montant_depot
        comptes(id) += montant_depot
        printf("Votre depot a ete pris en compte, le nouveau montant disponible sur votre compte est de: %.1f CHF.\n", comptes(id))
        // continue = true
        devise = 0
      }
      else {
        montant_depot = 0.95 * montant_depot
        comptes(id) += montant_depot
        printf("Votre depot a ete pris en compte, le nouveau montant disponible sur votre compte est de: %.2f CHF.\n", comptes(id))
        // continue = true
        devise = 0

      }
    }

    // 2eme methode : RETRAIT

    def retrait(id: Int, comptes: Array[Double]): Unit = {

      // continue = false
      devise = 0
      while (devise < 1 || devise > 2) {
        println("Indiquez la devise du retrait : 1:CHF,  2:EUR >")
        devise = readLine("->").toInt
      }

      println("Indiquez le montant du retrait :")
      montant_retrait = readLine("->").toInt

      while (montant_retrait % 10 != 0) {
        println("Le montant doit etre un multiple de 10.")
        println("Indiquez le montant de retrait :")
        montant_retrait = readLine("->").toInt
      }

      var plafond_autorise = comptes(id) * 0.1

      while (montant_retrait > plafond_autorise) {
        println(s"Votre plafond de retrait autorise est de: $plafond_autorise")
        println("Indiquez le montant de retrait :")
        montant_retrait = readLine("->").toInt
      }

      if (devise == 1 && montant_retrait >= 200) {
        coupure = 0
        while (coupure < 1 || coupure > 2) {
          println("En 1) grosses coupures, 2) petites coupures >")
          coupure = readLine("->").toInt
          var montant_retrait_2 = montant_retrait

          if (coupure == 1) {
            while (montant_retrait != 0) {
              var valeursEtNbBillets = ListBuffer[(Int, Int)]()
              for ((valeur, i) <- grosses_coupures.zipWithIndex) {
                if (montant_retrait / valeur != 0) {
                  nbr_billet = montant_retrait / valeur
                  println(s"Vous pouvez obtenir au maximum $nbr_billet billet(s) de $valeur CHF")
                  println("Tapez o pour ok ou une autre valeur inferieur a celle proposee >")
                  var input = readLine()
                  if (input == "o") {

                    montant_retrait = montant_retrait - valeur * nbr_billet
                    valeursEtNbBillets += ((valeur, nbr_billet))

                    if (montant_retrait != 0) {
                      println(s"Il reste $montant_retrait CHF a distribuer")
                    } else if (montant_retrait == 0) {
                      println("Veuillez retirer la somme demandee:")
                      for ((valeur, nbr_billet) <- valeursEtNbBillets) {
                        if (nbr_billet != 0) {
                          println(s"$nbr_billet billet(s) de $valeur CHF")
                        }
                      }
                      comptes(id) -= montant_retrait_2
                      printf("Votre retrait a ete pris en compte, le nouveau montant disponible sur votre compte" +
                        s" de : %.1f CHF.\n", comptes(id))
                      // continue = true
                    }

                    else {

                    }

                  }
                  else {
                    var input_bis = input.toInt
                    nbr_billet = input_bis
                    montant_retrait = montant_retrait - nbr_billet * valeur
                    valeursEtNbBillets += ((valeur, nbr_billet))
                    println(s"Il reste $montant_retrait CHF a distribuer")
                  }

                }
              }
            }

          }
          else {
            while (montant_retrait != 0) {
              var valeursEtNbBillets = ListBuffer[(Int, Int)]()

              for ((valeur, i) <- petites_coupures.zipWithIndex) {
                if (montant_retrait / valeur != 0) {
                  nbr_billet = montant_retrait / valeur
                  println(s"Vous pouvez obtenir au maximum $nbr_billet billet(s) de $valeur CHF")
                  println("Tapez o pour ok ou une autre valeur inferieur a celle proposee >")
                  var input = readLine()
                  if (input == "o") {

                    montant_retrait = montant_retrait - valeur * nbr_billet
                    valeursEtNbBillets += ((valeur, nbr_billet))

                    if (montant_retrait != 0) {
                      println(s"Il reste $montant_retrait CHF a distribuer")
                    } else if (montant_retrait == 0) {
                      println("Veuillez retirer la somme demandee:")
                      for ((valeur, nbr_billet) <- valeursEtNbBillets) {
                        if (nbr_billet != 0) {
                          println(s"$nbr_billet billet(s) de $valeur CHF")
                        }
                      }
                      comptes(id) -= montant_retrait_2
                      printf("Votre retrait a ete pris en compte, le nouveau montant disponible sur votre compte" +
                        s" de : %.1f CHF.\n", comptes(id))
                      // continue = true
                    }

                    else {

                    }

                  }
                  else {
                    var input_bis = input.toInt
                    nbr_billet = input_bis
                    montant_retrait = montant_retrait - nbr_billet * valeur
                    valeursEtNbBillets += ((valeur, nbr_billet))
                    println(s"Il reste $montant_retrait CHF a distribuer")
                  }

                }
              }

            }
          }


        }

      } else if (devise == 1 && montant_retrait <= 200) {
        while (montant_retrait != 0) {

          var montant_retrait_2 = montant_retrait
          var valeursEtNbBillets = ListBuffer[(Int, Int)]()
          for ((valeur, i) <- petites_coupures.zipWithIndex) {
            if (montant_retrait / valeur != 0) {
              nbr_billet = montant_retrait / valeur
              println(s"Vous pouvez obtenir au maximum $nbr_billet billet(s) de $valeur CHF")
              println("Tapez o pour ok ou une autre valeur inferieur a celle proposee >")
              var input = readLine()
              if (input == "o") {

                montant_retrait = montant_retrait - valeur * nbr_billet
                valeursEtNbBillets += ((valeur, nbr_billet))

                if (montant_retrait != 0) {
                  println(s"Il reste $montant_retrait CHF a distribuer")
                } else if (montant_retrait == 0) {
                  println("Veuillez retirer la somme demandee:")
                  for ((valeur, nbr_billet) <- valeursEtNbBillets) {
                    if (nbr_billet != 0) {
                      println(s"$nbr_billet billet(s) de $valeur CHF")
                    }
                  }
                  comptes(id) -= montant_retrait_2
                  printf("Votre retrait a ete pris en compte, le nouveau montant disponible sur votre compte" +
                    s" de : %.1f CHF.\n", comptes(id))
                  // continue = true
                }

                else {

                }

              }
              else {
                var input_bis = input.toInt
                nbr_billet = input_bis
                montant_retrait = montant_retrait - nbr_billet * valeur
                valeursEtNbBillets += ((valeur, nbr_billet))
                println(s"Il reste $montant_retrait CHF a distribuer")
              }

            }
          }

        }

      }

      // devise euro
      else {
        while (montant_retrait != 0) {

          var montant_retrait_2 = montant_retrait
          var valeursEtNbBillets = ListBuffer[(Int, Int)]()
          for ((valeur, i) <- petites_coupures.zipWithIndex) {
            if (montant_retrait / valeur != 0) {
              nbr_billet = montant_retrait / valeur
              println(s"Vous pouvez obtenir au maximum $nbr_billet billet(s) de $valeur EUR")
              println("Tapez o pour ok ou une autre valeur inferieur a celle proposee >")
              var input = readLine()
              if (input == "o") {

                montant_retrait = montant_retrait - valeur * nbr_billet
                valeursEtNbBillets += ((valeur, nbr_billet))

                if (montant_retrait != 0) {
                  println(s"Il reste $montant_retrait EUR a distribuer")
                } else if (montant_retrait == 0) {
                  println("Veuillez retirer la somme demandee:")
                  for ((valeur, nbr_billet) <- valeursEtNbBillets) {
                    if (nbr_billet != 0) {
                      println(s"$nbr_billet billet(s) de $valeur EUR")
                    }
                  }
                  comptes(id) -= montant_retrait_2 * 0.95
                  printf("Votre retrait a ete pris en compte, le nouveau montant disponible sur votre compte" +
                    s" de : %.2f CHF.\n", comptes(id))
                  // continue = true
                }

                else {

                }

              }
              else {
                var input_bis = input.toInt
                nbr_billet = input_bis
                montant_retrait = montant_retrait - nbr_billet * valeur
                valeursEtNbBillets += ((valeur, nbr_billet))
                println(s"Il reste $montant_retrait EUR a distribuer")
              }

            }
          }

        }

      }
    }


    def changepin(id: Int, codespin: Array[String]): Unit =
    {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caracteres >")
      var nouveau_code_pin  = readLine("->").toString

      if(nouveau_code_pin.length != nbrCaractereAttendu)
      {
        println("Votre code pin doit contenir au moins 8 caracteres")
        nouveau_code_pin  = readLine("->").toString
      }
      else
      {
        codespin(id) = nouveau_code_pin
      }

    }


    while (continue) {
      println("1) Depot")

      println("2) Retrait")

      println("3) Consultation du compte")

      println("4) Changement du code pin")

      println("5) Terminer")

      choix = readLine("->").toInt

      if (choix == 5) {
        println("Fin des operations, n'oubliez pas de recuperer votre carte.")
        nbr_de_choix += 1
      }


      if (choix < 1 || choix > 5) {
        println("loperarion doit etre entre 1 et 5")
      } else if (choix >= 1 && choix < 5)
      {
        nbr_de_choix += 1
        println("Saisissez votre identifiant")
        var id = readLine("->").toInt
        if (id > nbrclients) {
          println("Cet identifiant n'est pas valable")
          sys.exit()

        }
        else {
          println("Saisissez votre code pin")
          code_pin_saisie = scala.io.StdIn.readLine("-›").toString
          while (code_pin_saisie != codespin(id) && tentatives_restantes > 0)
          {
            tentatives_restantes  -= 1

            if (tentatives_restantes == 0)
            {
              println("Trop d'erreurs, abandon de l'identification")
              println("Saisissez votre identifiant")
              id = readLine("->").toInt
              if (id > nbrclients) {
                println("Cet identifiant n'est pas valable")
                sys.exit()
              }
              println("Saisissez votre code pin")
              code_pin_saisie = scala.io.StdIn.readLine("-›").toString
              if (code_pin_saisie == codespin(id))
              {
                println("1) Depot")

                println("2) Retrait")

                println("3) Consultation du compte")

                println("4) Changement du code pin")

                println("5) Terminer")

                choix = readLine("->").toInt
              }

            }
            else
            {
              println(s"Code pin errone, il vous reste : $tentatives_restantes tentatives")
              code_pin_saisie = readLine("-›").toString
            }


          }

        }


        if (choix == 1 && code_pin_saisie == codespin(id)) {
          depot(id = id, comptes = comptes)
        }

        if (choix == 2 && code_pin_saisie == codespin(id)) {
          retrait(id = id, comptes = comptes)
        }

        if (choix == 3 && code_pin_saisie == codespin(id)) {
          printf("Le montant disponible sur votre compte est de: %.2f CHF.\n", comptes(id))
        }

        if (choix == 4 && code_pin_saisie == codespin(id)) {
          changepin(id =id , codespin = codespin)
        }

        if (choix == 5) {
          println("Fin des operations, n'oubliez pas de recuperer votre carte.")
          nbr_de_choix += 1
        }

        while(nbr_de_choix > 0 && choix != 5)
        {
          println("1) Depot")

          println("2) Retrait")

          println("3) Consultation du compte")

          println("4) Changement du code pin")

          println("5) Terminer")

          choix = readLine("->").toInt

          if (choix == 1)
          {
            depot(id = id, comptes = comptes)
          }

          if (choix == 2) {
            retrait(id = id, comptes = comptes)
          }

          if (choix == 3) {
            printf("Le montant disponible sur votre compte est de: %.2f CHF.\n", comptes(id))
          }


          if (choix == 4)
          {
            changepin(id = id, codespin = codespin)
          }

          if (choix == 5)
          {
            println("Fin des operations, n'oubliez pas de recuperer votre carte.")
            nbr_de_choix += 1
          }


        }
        while(nbr_de_choix > 0 && choix == 5) {
          println("Saisissez votre identifiant")
          var id = readLine("->").toInt
          if (id > nbrclients) {
            println("Cet identifiant n'est pas valable")
            sys.exit()
          }
          else {
            println("Saisissez votre code pin")
            code_pin_saisie = scala.io.StdIn.readLine("-›").toString
            while (code_pin_saisie != codespin(id) && tentatives_restantes > 0) {
              tentatives_restantes -= 1

              if (tentatives_restantes == 0) {
                println("Trop d'erreurs, abandon de l'identification")
                println("Saisissez votre identifiant")
                id = readLine("->").toInt
                if (id > nbrclients) {
                  println("Cet identifiant n'est pas valable")
                  sys.exit()
                }
                println("Saisissez votre code pin")
                code_pin_saisie = scala.io.StdIn.readLine("-›").toString
                if (code_pin_saisie == codespin(id)) {
                  println("1) Depot")

                  println("2) Retrait")

                  println("3) Consultation du compte")

                  println("4) Changement du code pin")

                  println("5) Terminer")

                  choix = readLine("->").toInt
                }

              }
              else {
                println(s"Code pin errone, il vous reste : $tentatives_restantes tentatives")
                code_pin_saisie = readLine("-›").toString
              }
            }

            if (code_pin_saisie == codespin(id))
            {
              println("1) Depot")

              println("2) Retrait")

              println("3) Consultation du compte")

              println("4) Changement du code pin")

              println("5) Terminer")

              choix = readLine("->").toInt
            }

            if (choix == 1)
            {
              depot(id = id, comptes = comptes)
            }

            if (choix == 2) {
              retrait(id = id, comptes = comptes)
            }

            if (choix == 3) {
              printf("Le montant disponible sur votre compte est de: %.2f CHF.\n", comptes(id))
            }


            if (choix == 4) {
              changepin(id = id, codespin = codespin)
            }

            if (choix == 5) {
              println("Fin des operations, n'oubliez pas de recuperer votre carte.")
              nbr_de_choix += 1

            }

          }

        }
      }
    }


  }



  
}
