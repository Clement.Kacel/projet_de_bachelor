//Assignment: Lina Baraka_996559_assignsubmission_file

import scala.io.StdIn._

object Main {

  val nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")

  var monchoix = 0 
  var choixdupin = false 
  var nb_essaie_pin = 3
  var justepin = false

  def depot(id : Int, comptes : Array[Double]) : Unit = {

    var ddevise = 3 
    while (ddevise != 1 && ddevise != 2) {
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      ddevise = readLine().toInt
      if(ddevise != 1 && ddevise != 2 ){
      }
    }

    //ON INITIALISE NOS VARIABLES A NUL
    var c = false
    var montant_d = 0;
    while (!c){
      println("Indiquez le montant du dépôt >")
      montant_d = readLine().toInt
      if(montant_d % 10 == 0 && montant_d>0){
        c = true
      }else{
        println("Le montant doit être un multiple de 10")
      }
    }         
    if(ddevise == 1) { comptes(id) += montant_d
    }else{ comptes(id) += 0.95 * montant_d //IL FAUT CONVERTIR
    }
    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id))

  }


  def retrait(id : Int, comptes : Array[Double]) : Unit = {


    //INITIALISATION DE NOS VARIABLES 
    var retrait = 3 
    var billet500 = 0
    var billet200 = 0
    var billet100 = 0
    var billet50 = 0
    var billet20 = 0
    var billet10 = 0

    while (retrait != 1 && retrait != 2) {
      println("Indiquez la devise : 1 CHF, 2: EUR")
      retrait = readLine().toInt
      if(retrait != 1 && retrait != 2 ){
      }
    }
    //var billet500 = 0
    //var billet200 = 0
    //var billet100 = 0
    //var billet50 = 0
    //var billet20 = 0
    //var billet10 = 0
    //var retrait = 3

    var c = false
    var montant = 0;
    val ten = comptes(id) * 0.1
    while (!c){ println("Indiquez le montant du retrait >")
      montant = readInt()
      if(montant % 10 == 0 ){
        if(montant <= ten && montant>0){
          c = true
        }else{ //INDICATION DU PLAFOND DISPONIBLE
          println("Votre plafond de retrait autorisé est de :" + ten)
        }
      }else{
        println("Le montant doit être un multiple de 10")
      }
    }  //NOMBRE DE COUPURE A DISPOSITION
    var coupure = 3 
    if(retrait == 1){ 
      if(montant >= 200){ 
        while (coupure != 1 && coupure != 2) { 
          println("En, 1) Grosses coupures 2) Petites coupures >")
          coupure = readInt()
          if(coupure != 1 && coupure != 2 ){
            println("choix invalide")
          }
        }
      }
    }
    println("Veuillez retirer la somme demandée :")
    var reste = montant

    if(retrait == 1) {
      if(coupure == 1){ 

        while (reste > 0){  
          if(reste / 500 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/500 + "billet(s) de 500 CHF")  //METHODE PRINTLN

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine() 
              if (selectionbillet != "o") {
                billet500 = selectionbillet.toInt 
                if (billet500 < 0 || billet500 > (reste / 500)) {  
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet500 * 500) 
                  selectionbillet="o"  
                }
              }
              else
              { 
                billet500 = reste/500
                reste -= ((reste/500) * 500)
              }

            } while (selectionbillet != "o")

          }
          if(reste / 200 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/200 + "billet(s) de 200 CHF") //METHODE PRINTLN

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()

              if (selectionbillet != "o") { //o
                billet200 = selectionbillet.toInt
                if (billet200 < 0 || billet200 > (reste / 200)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet200 * 200)
                  selectionbillet="o"
                }
              }
              else
              { billet200 = reste/200
                reste -= ((reste/200) * 200)
              }
            } while (selectionbillet != "o")

          }
          if(reste / 100 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/100 + "billet(s) de 100 CHF") //METHODE PRINTLN 

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()

              if (selectionbillet != "o") {
                 billet100 = selectionbillet.toInt
                if (billet100 < 0 || billet100 > (reste / 100)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet100 * 100)
                  selectionbillet="o"
                }
              }
              else
              { billet100 = reste/100
                reste -= ((reste/100) * 100)
              }

            } while (selectionbillet != "o")

          }
          if(reste / 50 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/50 + "billet(s) de 50 CHF") //PRINTLN METHODE

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()
              if (selectionbillet != "o") {
                billet50 = selectionbillet.toInt
                if (billet50 < 0 || billet50 > (reste / 50)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet50 * 50)
                  selectionbillet="o"
                }
              }
              else
              {
                billet50 = reste/50
                reste -= ((reste/50) * 50)
              }

            } while (selectionbillet != "o")

          }
          if(reste / 20 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/20 + "billet(s) de 20 CHF") 

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()

              if (selectionbillet != "o") {
                billet20 = selectionbillet.toInt
                if (billet20 < 0 || billet20 > (reste / 20)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet20 * 20)
                  selectionbillet="o"
                }
              }
              else
              {
                billet20 = reste/20
                reste -= ((reste/20) * 20)
              }

            } while (selectionbillet != "o")

          }
          if(reste / 10 >= 1) {         
            println(s"Il reste $reste CHF à distribuer")
            print("Vous pouvez obtenir au maximum ")
            print(reste/10)
            billet10 = (reste/10)
            println(" billet(s) de 10 CHF")
            reste -= ((reste/10) * 10)
          }
        }
      }
      else{ 
        while (reste > 0){
          if(reste / 100 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/100 + "billet(s) de 100 CHF") 
            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()

              if (selectionbillet != "o") {
                 billet100 = selectionbillet.toInt
                if (billet100 < 0 || billet100 > (reste / 100)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet100 * 100)
                  selectionbillet="o"
                }
              }
              else
              {
                billet100 = reste/100
                reste -= ((reste/100) * 100)
              }

            } while (selectionbillet != "o")
          }
          if(reste / 50 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/50 + "billet(s) de 50 CHF") 

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()

              if (selectionbillet != "o") {
                billet50 = selectionbillet.toInt
                if (billet50 < 0 || billet50 > (reste / 50)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet50 * 50)
                  selectionbillet="o"
                }
              }
              else
              {
                billet50 = reste/50
                reste -= ((reste/50) * 50)
              }
            } while (selectionbillet != "o")
          }
          if(reste / 20 >= 1){
            println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/20 + "billet(s) de 20 CHF") 

            var selectionbillet = ""
            do {
              println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              selectionbillet = readLine()

              if (selectionbillet != "o") {
                billet20 = selectionbillet.toInt
                if (billet20 < 0 || billet20 > (reste / 20)) {
                  println("Veuillez saisir une valeur valide.")
                } else {
                  reste -= (billet20 * 20)
                  selectionbillet="o"
                }
              }
              else
              {
                billet20 = reste/20
                reste -= ((reste/20) * 20)
              }

            } while (selectionbillet != "o")

          }
          if(reste / 10 >= 1) {
            println(s"Il reste $reste CHF à distribuer")
            print("Vous pouvez obtenir au maximum ")
            print(reste/10) //CALCULS
            billet10 = (reste/10)
            println(" billet(s) de 10 CHF")
            reste -= ((reste/10) * 10)
          }
        }

      }          
      println("Veuillez retirer la somme demandée : ")
      if(billet500>0)
      {
        print(billet500)
        println(" billet(s) de 500 CHF")
      }
      if(billet200>0)
      {
        print(billet200)
        println(" billet(s) de 200 CHF")
      }
      if(billet100>0)
      {
        print(billet100)
        println(" billet(s) de 100 CHF")
      }
      if(billet50>0)
      {
        print(billet50)
        println(" billet(s) de 50 CHF")
      }
      if(billet20>0)
      {
        print(billet20)
        println(" billet(s) de 20 CHF")
      }
      if(billet10>0)
      {
        print(billet10)
        println(" billet(s) de 10 CHF")
      }

    }else{ // EUR
      while (reste > 0){
        if(reste / 100 >= 1){
          println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/100 + "billet(s) de 100 EUR") //METHODE PRINTLN

          var selectionbillet = ""
          do {
            println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            selectionbillet = readLine()

            if (selectionbillet != "o") {
               billet100 = selectionbillet.toInt
              if (billet100 < 0 || billet100 > (reste / 100)) {
                println("Veuillez saisir une valeur valide.")
              } else {
                reste -= (billet100 * 100)
                selectionbillet="o"
              }
            }
            else
            {
              billet100 = reste/100
              reste -= ((reste/100) * 100)
            }

          } while (selectionbillet != "o")

        }
        if(reste / 50 >= 1){
          println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/50 + "billet(s) de 50 EUR") 
          var selectionbillet = ""
          do {
            println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            selectionbillet = readLine()

            if (selectionbillet != "o") {
              billet50 = selectionbillet.toInt
              if (billet50 < 0 || billet50 > (reste / 50)) {
                println("Veuillez saisir une valeur valide.")
              } else {
                reste -= (billet50 * 50)
                selectionbillet="o"
              }
            }
            else
            {
              billet50 = reste/50
              reste -= ((reste/50) * 50)
            }

          } while (selectionbillet != "o")


        }
        if(reste / 20 >= 1){
          println("Il reste" + reste + "CHF à distribuer" + "\nVous pouvez obtenir au maximum" + reste/20 + "billet(s) de 20 EUR") 
          var selectionbillet = ""
          do {
            println(s"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            selectionbillet = readLine()

            if (selectionbillet != "o") {
              billet20 = selectionbillet.toInt
              if (billet20 < 0 ||billet20 > (reste / 20)) {
                println("Veuillez saisir une valeur valide.")
              } else {
                reste -= (billet20 * 20)
                selectionbillet="o"
              }
            }
            else
            {
              billet20 = reste/20
              reste -= ((reste/20) * 20)
            }

          } while (selectionbillet != "o")

        }
        if(reste / 10 >= 1) {
          println(s"Il reste $reste CHF à distribuer")
          print("Vous pouvez obtenir au maximum ")
          print(reste/10)
          billet10 = (reste/10)

          println(" billet(s) de 10 EUR")
          reste -= ((reste/10) * 10)
        }
    }
    } //ONT EFFECTUE LES CALCULS
    if(retrait == 1) {
      comptes(id) -= montant
    }else{
      println("Veuillez retirer la somme demandée : ")

      if(billet100>0)
      { print(billet100)
        println(" billet(s) de 100 EUR")
      } 
      if(billet50>0)
      { print(billet50)
        println(" billet(s) de 50 EUR")
      }
      if(billet20>0)
      { print(billet20)
        println(" billet(s) de 20 EUR")
      }
      if(billet10>0)
      { print(billet10)
        println(" billet(s) de 10 EUR")
      }
      comptes(id) -= montant * 0.95
    }  //METHODE A UTILISER COMME INDIQUÉ DANS L'ENNONCÉ
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est : %.2f CHF\n", comptes(id))

    
  }


  def consultation(id : Int, comptes : Array[Double]) : Unit = {
    // ON AFFICHE LE SOLDE DU COMPTE
    
    printf("Le montant disponible sur votre compte est : %.2f", comptes(id))
    println(" CHF")
    
  }


  def changepin(id : Int, codespin : Array[String]) : Unit = {
    // ON CHOISIT UN NOUVEAU CODE PIN
    
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    codespin(id) = readLine().toString

    while (codespin(id).length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères.")
      
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      codespin(id) = readLine().toString
    }

    if (codespin(id).length >= 8) {
      
      println("Votre code pin a été mis à jour correctement.")
    }
    
  }
  
  
  def main(args: Array[String]): Unit = {

    println("Saisissez votre code identifiant >")
    var identifiant = readLine().toInt

    if (identifiant >= nbclients) {
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
    }
    
    var pin = "FAUX"

    while (monchoix < 6) {
      
    while (pin != codespin(identifiant)) {

    if (identifiant >= nbclients) {
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
    } else {

      while (nb_essaie_pin > 0 && !justepin){  //ONT UTLISE LA LOGIQUE BOOLEENE
        println("Saisissez votre code PIN >")
        pin = readLine()
        if (pin == codespin(identifiant)) {
          justepin = !false 
          choixdupin = true
        } else { nb_essaie_pin -= 1 
          if(nb_essaie_pin == 0){ 
            println("Trop d’erreurs, abandon de l’identification")
            println("Saisissez votre code identifiant >")
            identifiant = readLine().toInt
            nb_essaie_pin = 3
          }else{
            println(s"Code pin erroné, il vous reste $nb_essaie_pin tentatives >")
          }
        }
      }
      
    }

    }

    println("Choisissez votre opération :")
    println("1) Dépôt")
    println("2) Retrait")
    println("3) Consultation du compte")
    println("4) Changement du code pin")
    println("5) Terminer")

    //ICI NOUS AFFICHONS LE MENU PRINCIPALE DU BANCOMAT.

    monchoix = readLine().toInt

    while (monchoix != 5) {
    
    if (monchoix == 1) {
      depot(identifiant, comptes)
    } else if (monchoix == 2) {
      retrait(identifiant, comptes)
    } else if (monchoix == 3) {
      consultation(identifiant, comptes)
    } else if (monchoix == 4) {
      changepin(identifiant, codespin)
    }

      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")

      //ICI NOUS AFFICHONS LE MENU PRINCIPALE DU BANCOMAT.

      monchoix = readLine().toInt
      
    }

    if (monchoix == 5) {
      println("Fin des opérations, n'oubliez pas de récupérer votre carte")
      
      println("Saisissez votre code identifiant >")
      identifiant = readLine().toInt

      if (identifiant >= nbclients) {
        println("Cet identifiant n’est pas valable.")
        System.exit(0)
      }

      pin = "FAUX"
      choixdupin = false
      justepin = false
      nb_essaie_pin = 3
    }
      
    }

  }

  
}
