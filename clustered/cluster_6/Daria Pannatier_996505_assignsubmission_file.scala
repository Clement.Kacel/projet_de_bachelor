//Assignment: Daria Pannatier_996505_assignsubmission_file

import scala.io.StdIn.readLine
import scala.io.StdIn.readInt
object Main {
  def main(args: Array[String]): Unit = {
    var operation = 0
    var testPin = false
    var soldeCpt = 1200.0 //valeur initiale
    var nbclients = 100
    val cptes = Array.fill[Double](nbclients)(1200.0)
    val codespin = Array.fill[Int](nbclients)("INTRO1234") 
    val id = 0
    val soldeCpte = cptes(id)
    val codespin = codespin(id)
    val id = 1
    val soldeCpte = cptes(id)
    val codespin = codespin(id)
    var depot = 0
    var devise = 1 // CHF par defaut 
     var retrait = 0
    var coupure = 1
    var nbCoupures500 = 0
    var nbCoupures200 = 0
    var nbCoupures100 = 0
    var nbCoupures50 = 0
    var nbCoupures20 = 0
    var nbCoupures10 = 0
    var ok = false

    println("Saisissez votre code identifiant>")
    var identifiant = readLine().toInt
    if (identifiant > nbclients) {
      println ("Cet identification n'est pas valable.")
      //else { 


  
 
      
    do {
      println("Menu :")
      println("1. Dépôt")
      println("2. Retrait")
      println("3. Consultation du compte")
      println("4. Changement du code pin")
      println("5. Terminer")
      println("Votre choix : ")
      operation = readLine().toInt
    
      var tentative = 3
      operation match {
        //si le choix n'est pas 5 alors on doit demander le PIN avant de faire les opérations autres
        case 1 =>
          if (testPin == false)
        { 
          while((testPin == false) && (tentative<=3) && (tentative>0)) 
            {
            println("Saisissez votre code PIN >")
            var pin = readLine().toString
            if (pin == "INTRO1234") 
              {
              //code pour le dépôt println("Entrer le montant à déposer: ")
              testPin = true  
                //var depot = 0
                 // var devise = 1 
            
                    do {
                      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
                  devise = scala.io.StdIn.readLine().toInt
                    } while (devise != 1 && devise != 2)
                    do {  
                      println("Indiquez le montant du dépôt >")
                      depot = scala.io.StdIn.readLine().toInt
                      if (depot % 10 != 0 || depot <= 0) {
                        println("Le montant du depot doit être un multiple de 10.")
                    }
                  }
                while (depot % 10 != 0 || depot <= 0)
                    if (devise == 1) {
                      soldeCpt = soldeCpt + depot
                      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + (soldeCpt) + " CHF") 
                    } 
                    else { 
                      soldeCpt = soldeCpt + (depot * 0.95)
                      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + soldeCpt + " CHF")
                    }
                        
              } 
            else 
              { 
                tentative = tentative - 1
                println("Code PIN erroné, il vous reste " + tentative  + " tentative(s) >")  
                if (tentative == 0)
                   { 
                  testPin = false
                  println("Trop d'erreurs, abandon de l’identification.")
                     }
                //operation = 4
              }
            }
        }
        else
        {
          //var depot = 0
           // var devise = 1 
              do {
                println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
            devise = scala.io.StdIn.readLine().toInt
              } while (devise != 1 && devise != 2)
              do {  
                println("Indiquez le montant du dépôt >")
                depot = scala.io.StdIn.readLine().toInt
                if (depot % 10 != 0 || depot <= 0) {
                  println("Le montant du depot doit être un multiple de 10.")
              }
            }
          while (depot % 10 != 0 || depot <= 0)
              if (devise == 1) {
                soldeCpt = soldeCpt + depot
                println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + (soldeCpt) + " CHF") 
              } 
              else { 
                soldeCpt = soldeCpt + (depot * 0.95)
                println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + soldeCpt + " CHF")
              }
                  
  
        }
        
       case 2 =>
          if (testPin == false)
           {
            
             while((testPin == false) && (tentative<=3) && (tentative>0)) 
             {
             println("Saisissez votre code PIN:")
             var pin = readLine().toString
             if (pin == "INTRO1234") 
               {
               //code pour le retrait println("Entrer le montant à retirer: ")
                  do 
      {
      println("Indiquez la devise: 1) CHF, 2) EUR >")
      devise = scala.io.StdIn.readLine().toInt
      } 
    while (devise !=1 && devise !=2)
    do 
      { 
       println("Indiquez le montant du retrait >")
      retrait = scala.io.StdIn.readLine().toInt
      if (retrait % 10 != 0 || retrait <= 0) 
        {
        println("Le montant doit etre un multiple de 10.")
        }

      if (retrait > soldeCpt * 0.1) 
        {
        println("Votre plafond de retrait autorisé est de " + (soldeCpt * 0.1) + "CHF.")
        }
      }  
    while (retrait % 10 != 0 || retrait <= 0 || retrait > soldeCpt * 0.1)

   if (devise == 1 && retrait >= 200) 
    { 
    do 
      {
      println("En 1) grosses coupures, 2) petites coupures >")
      coupure = scala.io.StdIn.readLine().toInt
      } 
    while (coupure !=1 && coupure !=2)

    do 
      {
      if (devise == 1 && coupure == 1) 
        {
        soldeCpt = soldeCpt - retrait
          if (retrait / 500 >= 1)
           {
        do 
          {
          println ("Il reste " + retrait + " CHF à distribuer.")
          println ("Vous pouvez obtenir au maximum " + (retrait / 500).toInt + " coupures de 500 CHF.")
          println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

          nbCoupures500 = scala.io.StdIn.readLine().toInt
          if (nbCoupures500 == 0) 
             {
            ok = true  
            nbCoupures500 =  (retrait / 500).toInt
            retrait = retrait - (retrait / 500).toInt*500
            
             }  
          else 
            if (ok == false && nbCoupures500 <= (retrait / 500).toInt) 
               {
                ok = true 
                retrait = retrait - (nbCoupures500 * 500)
                }
          } 
        while (ok == false)
        } 

          if (retrait / 200 >= 1)
           {
        do 
          {
          println ("Il reste " + retrait + " CHF à distribuer.")
          println ("Vous pouvez obtenir au maximum " + (retrait / 200).toInt + " coupures de 200 CHF.")
          println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

          nbCoupures200 = scala.io.StdIn.readLine().toInt
             if (nbCoupures200 == 0) 
                  {
                 ok = true   
                    nbCoupures200 =  (retrait / 200).toInt
                    retrait = retrait - (retrait / 200).toInt*200
                 
                  }  
               else 
                 if (ok == false && nbCoupures200 <= (retrait / 200).toInt) 
                    {
                     ok = true 
                     retrait = retrait - (nbCoupures200 * 200)
                     }
               } 
             while (ok == false)
        }

          if (retrait / 100 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 100).toInt + " coupures de 100 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures100 = scala.io.StdIn.readLine().toInt
               if (nbCoupures100 == 0) 
                    {
                   ok = true 
                      nbCoupures100 =  (retrait / 100).toInt
                   retrait = retrait - (retrait / 100).toInt*100
                   
                    }  
                 else 
                   if (ok == false && nbCoupures100 <= (retrait / 100).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures100 * 100)
                       }
                 } 
               while (ok == false)
          }
          if (retrait / 50 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 50).toInt + " coupures de 50 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures50 = scala.io.StdIn.readLine().toInt
               if (nbCoupures50 == 0) 
                    {
                   ok = true   
                      nbCoupures50 =  (retrait / 50).toInt
                   retrait = retrait - (retrait / 50).toInt*50
                   
                    }  
                 else 
                   if (ok == false && nbCoupures50 <= (retrait / 50).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures50 * 50)
                       }
                 } 
               while (ok == false)
          }
          if (retrait / 20 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 20).toInt + " coupures de 20 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures20 = scala.io.StdIn.readLine().toInt
               if (nbCoupures20 == 0) 
                    {
                   ok = true 
                      nbCoupures20 =  (retrait / 20).toInt
                   retrait = retrait - (retrait / 20).toInt*20
                   
                    }  
                 else 
                   if (ok == false && nbCoupures20 <= (retrait / 20).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures20 * 20)
                       }
                 } 
               while (ok == false)
          }
          if (retrait / 10 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 10).toInt + " coupures de 10 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures10 = scala.io.StdIn.readLine().toInt
               if (nbCoupures10 == 0) 
                    {
                   ok = true   
                    nbCoupures10 =  (retrait / 10).toInt
                   retrait = retrait - (retrait / 10).toInt*10
                   
                    }  
                 else 
                   if (ok == false && nbCoupures10 <= (retrait / 10).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures10 * 10)
                       }
                 } 
               while (ok == false)
          }
        }
      }
    while (retrait != 0 )
    }
    println ("Veuillez retirer la somme demandée: ")
    if (nbCoupures500 != 0 ) 
      println (nbCoupures500 + " billet(s) de 500 CHF")
    if (nbCoupures200 != 0 )
      println (nbCoupures200 + " billet(s) de 200 CHF")
    if (nbCoupures100 != 0 ) 
      println (nbCoupures100 + " billet(s) de 100 CHF")
    if (nbCoupures50 != 0 )
      println (nbCoupures50 + " billet(s) de 50 CHF")
    if (nbCoupures20 != 0 ) 
      println (nbCoupures20 + " billet(s) de 20 CHF")
    if (nbCoupures10 != 0 )
      println (nbCoupures10 + " billet(s) de 10 CHF")
               testPin = true  
                 println("PIN correct")
               } 
             else 
               { 
                 tentative = tentative - 1
                 println("Code PIN erroné, il vous reste " + tentative  + " tentative(s).")  
                 if (tentative == 0)
                  { 
                   testPin = false
                   println("Pour votre protection, les opérations bancaires vont s'interrompre, récupérez votre carte.")
                    }
                 //operation = 4
               }
             }
             }
           else
             {
              // code du retrait println("Entrer le montant à retirer: ")
                do 
      {
      println("Indiquez la devise: 1) CHF, 2) EUR >")
      devise = scala.io.StdIn.readLine().toInt
      } 
    while (devise !=1 && devise !=2)
    do 
      { 
       println("Indiquez le montant du retrait >")
      retrait = scala.io.StdIn.readLine().toInt
      if (retrait % 10 != 0 || retrait <= 0) 
        {
        println("Le montant doit etre un multiple de 10.")
        }

      if (retrait > soldeCpt * 0.1) 
        {
        println("Votre plafond de retrait autorisé est de " + (soldeCpt * 0.1) + "CHF.")
        }
      }  
    while (retrait % 10 != 0 || retrait <= 0 || retrait > soldeCpt * 0.1)

   if (devise == 1 && retrait >= 200) 
    { 
    do 
      {
      println("En 1) grosses coupures, 2) petites coupures >")
      coupure = scala.io.StdIn.readLine().toInt
      } 
    while (coupure !=1 && coupure !=2)

    do 
      {
      if (devise == 1 && coupure == 1) 
        {
        soldeCpt = soldeCpt - retrait
          if (retrait / 500 >= 1)
           {
        do 
          {
          println ("Il reste " + retrait + " CHF à distribuer.")
          println ("Vous pouvez obtenir au maximum " + (retrait / 500).toInt + " coupures de 500 CHF.")
          println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

          nbCoupures500 = scala.io.StdIn.readLine().toInt
          if (nbCoupures500 == 0) 
             {
            ok = true  
            nbCoupures500 =  (retrait / 500).toInt
            retrait = retrait - (retrait / 500).toInt*500
            
             }  
          else 
            if (ok == false && nbCoupures500 <= (retrait / 500).toInt) 
               {
                ok = true 
                retrait = retrait - (nbCoupures500 * 500)
                }
          } 
        while (ok == false)
        } 

          if (retrait / 200 >= 1)
           {
        do 
          {
          println ("Il reste " + retrait + " CHF à distribuer.")
          println ("Vous pouvez obtenir au maximum " + (retrait / 200).toInt + " coupures de 200 CHF.")
          println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

          nbCoupures200 = scala.io.StdIn.readLine().toInt
             if (nbCoupures200 == 0) 
                  {
                 ok = true   
                    nbCoupures200 =  (retrait / 200).toInt
                    retrait = retrait - (retrait / 200).toInt*200
                 
                  }  
               else 
                 if (ok == false && nbCoupures200 <= (retrait / 200).toInt) 
                    {
                     ok = true 
                     retrait = retrait - (nbCoupures200 * 200)
                     }
               } 
             while (ok == false)
        }

          if (retrait / 100 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 100).toInt + " coupures de 100 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures100 = scala.io.StdIn.readLine().toInt
               if (nbCoupures100 == 0) 
                    {
                   ok = true 
                      nbCoupures100 =  (retrait / 100).toInt
                   retrait = retrait - (retrait / 100).toInt*100
                   
                    }  
                 else 
                   if (ok == false && nbCoupures100 <= (retrait / 100).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures100 * 100)
                       }
                 } 
               while (ok == false)
          }
          if (retrait / 50 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 50).toInt + " coupures de 50 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures50 = scala.io.StdIn.readLine().toInt
               if (nbCoupures50 == 0) 
                    {
                   ok = true   
                      nbCoupures50 =  (retrait / 50).toInt
                   retrait = retrait - (retrait / 50).toInt*50
                   
                    }  
                 else 
                   if (ok == false && nbCoupures50 <= (retrait / 50).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures50 * 50)
                       }
                 } 
               while (ok == false)
          }
          if (retrait / 20 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 20).toInt + " coupures de 20 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures20 = scala.io.StdIn.readLine().toInt
               if (nbCoupures20 == 0) 
                    {
                   ok = true 
                      nbCoupures20 =  (retrait / 20).toInt
                   retrait = retrait - (retrait / 20).toInt*20
                   
                    }  
                 else 
                   if (ok == false && nbCoupures20 <= (retrait / 20).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures20 * 20)
                       }
                 } 
               while (ok == false)
          }
          if (retrait / 10 >= 1)
             {
          do 
            {
            println ("Il reste " + retrait + " CHF à distribuer.")
            println ("Vous pouvez obtenir au maximum " + (retrait / 10).toInt + " coupures de 10 CHF.")
            println ("Tapez 0 pour ok ou une autre valeur inférieure à celle proposée >")

            nbCoupures10 = scala.io.StdIn.readLine().toInt
               if (nbCoupures10 == 0) 
                    {
                   ok = true   
                    nbCoupures10 =  (retrait / 10).toInt
                   retrait = retrait - (retrait / 10).toInt*10
                   
                    }  
                 else 
                   if (ok == false && nbCoupures10 <= (retrait / 10).toInt) 
                      {
                       ok = true 
                       retrait = retrait - (nbCoupures10 * 10)
                       }
                 } 
               while (ok == false)
          }
        }
      }
    while (retrait != 0 )
    }
    println ("Veuillez retirer la somme demandée: ")
    if (nbCoupures500 != 0 ) 
      println (nbCoupures500 + " billet(s) de 500 CHF")
    if (nbCoupures200 != 0 )
      println (nbCoupures200 + " billet(s) de 200 CHF")
    if (nbCoupures100 != 0 ) 
      println (nbCoupures100 + " billet(s) de 100 CHF")
    if (nbCoupures50 != 0 )
      println (nbCoupures50 + " billet(s) de 50 CHF")
    if (nbCoupures20 != 0 ) 
      println (nbCoupures20 + " billet(s) de 20 CHF")
    if (nbCoupures10 != 0 )
      println (nbCoupures10 + " billet(s) de 10 CHF")
            }
        case 3 =>
        if (testPin == false)
         {
           while((testPin == false) && (tentative<=3) && (tentative>0)) 
           {
           println("Saisissez votre code PIN >")
           var pin = readLine().toString
           if (pin == "INTRO1234") 
             {
             testPin = true  
               println("PIN correct")
               println("Le montant disponible sur votre compte est de : " + soldeCpt)
             } 
           else 
             { 
               tentative = tentative - 1
               println("Code PIN erroné, il vous reste " + tentative  + " tentative(s) >")  
               if (tentative == 0)
               {
                 testPin = false
                 println("Pour votre protection, les opérations bancaires vont s'interrompre, récupérez votre carte.")
               }
             }
           }
         }
        else 
          {
            println("Le montant disponible sur votre compte est de :" + soldeCpt)
          }

        case 4 =>
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.") 
        }
    } while (operation != 4)
  
  }
}
