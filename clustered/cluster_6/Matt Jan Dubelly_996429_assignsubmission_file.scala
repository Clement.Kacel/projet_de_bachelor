//Assignment: Matt Jan Dubelly_996429_assignsubmission_file

import scala.io.StdIn._
object Main {

  def depot(id : Int, comptes : Array[Double]) : Unit ={
  //début méthode depot
  var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
  //choix de la devise en Euros/CHF
  var montant_depot = readLine("Indiquez le montant de votre dépôt > ").toDouble
  // choix montant à déposer dans le compte

    while ((!(montant_depot % 10 == 0))||(!(montant_depot>=10))){ 
    //boucle qui redemande le montant du depot tant que le montant n'est pas un multiple de 10 ou inférieur à 10
    println ("Le montant doit être un multiple de 10")
    montant_depot = readLine("Veuillez de nouveau indiquer le montant de votre dépôt > ").toInt
    } 
    if (devise==1){ 

    comptes(id) += montant_depot
    //lorsque la devise choisie est en CHF, on ajoute le montant du depot au montant du compte correspondant à l'identifiant donné (l'identifiant correspond à l'index du tableau compte)
    }  
    if (devise==2){

    montant_depot *= 0.95
    //le compte est en CHF, losque la devise choisie est en Euro, on multiplie le montant du depot par 0.95 pour obtenir le montant de depot en CHF
    comptes(id) += montant_depot
    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n",  comptes(id))
  }
  //fin de la méthode depot

  def retrait(id : Int, comptes : Array[Double]) : Unit ={
  //début méthode retrait
  var plafond = 0.0
  //variable limitant le montant du retrait en fonction du montant disponible sur le compte.
  var clavier = "x"
  //variable pour choisir le nombre de billet(s) qu'on souhaite retirer (par prix) en String.

  var clavier_500 = 0
  var clavier_200 = 0
  var clavier_100 = 0
  var clavier_50 = 0
  var clavier_20 = 0
  var clavier_10 = 0
  //variables du choix du nombre de billet(s) qu'on souhaite retirer (- que le nb de billet(s) proposé(s) par prix en Int).
  var coupure = 0
  // variable pour choisir si l'on souhaite retirer en grosses ou petites coupures.

  var billets_500 =0
  var billets_200 =0 
  var billets_100 =0
  var billets_50 =0
  var billets_20 =0
  var billets_10 =0
  //les variables billets correspondent aux billets disponibles à chaque prix lors du retrait.
  var devise= 0
  // variable choissisant si le montant est en EUR ou CHF.
  var montant_retrait= 0
  // variable pour faire un retrait d'argent sur le compte.
  var montant=0
  //variable équivalente à montant_retrait initial sans faire d'opérations.
  var montant_compte_EUR =0.0
  // montant du compte en Euros
  devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt 

    while (!(devise==1)&&(!(devise==2))){
    //tant que devise pas égal à 1 ou 2, l'utilisateur doit saisir de nouveau la devise.
    devise = readLine("Vous n'avez pas choisi entre l'option 1 ou 2, Veuillez Indiquer de nouveau la devise du retrait : 1) CHF ; 2) EUR > ").toInt 
     }
     if (devise==1){ 

     montant_retrait = readLine("Indiquez le montant de votre retrait > ").toInt
     plafond = comptes(id)*0.1 

        while (!(montant_retrait % 10 == 0)||(!(montant_retrait >= 10))||(!(montant_retrait <= plafond ))){
        //comme pour le dépôt, le montant du retrait doit être un multiple de 10 (et doit être plus grand ou égal à 10) mais il doit aussi être plus petit ou égal au plafond autorisé (10% du montant du compte), tant qu'une de ces conditions n'est pas respecté le montant du retrait devra être saisi. 
          if ((!(montant_retrait % 10 == 0))||(!(montant_retrait >= 0))) 

          println ("Le montant doit être un multiple de 10")

          if (!(montant_retrait <= plafond)) { 
          printf("Le plafond de retrait autorisé est de : %.2f CHF\n", plafond)
          } 
          montant_retrait = readLine("Veuillez de nouveau indiquer le montant de votre retrait > ").toInt 
       }
       montant = montant_retrait
       // cette équivalence permet de conserver la valeur initiale du montant du retrait, en tant que montant.
       if (montant<200)coupure=2
       // lorsque le montant du retrait est inférieur à 200 CHF, il s'agit des petites coupures donc coupure = 2 sans demander à l'utilisateur. À l'inverse l'utilisateur à le choix comme ci-dessous:
       if (montant_retrait>=200){

       coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt

          while (!(coupure==1)&&(!(coupure==2))){ 

          coupure = readLine("Vous n'avez pas tapez 1 ou 2, veuillez choisir entre : 1) grosses coupures, 2) petites coupures > ").toInt
          }
       }
     }
     //fin de la condition devise en CHF
     if (devise ==2)coupure=0
     // lorsque le montant du retrait est en EUR, il ne doit pas y avoir de coupure, sans cette ligne de code, si nous commençons par un retrait en CHF et que la prochaine opération effectuée est un retrait en EUR, la valeur de la coupure serait soit 1 soit 2, ce qui poserait problème.
     if (coupure == 1){
     //début grosses coupures
       if (montant_retrait>=500){
       // cette condition sera utilisé pour les différents prix de coupures. Cela permet de ne pas afficher le choix d'une coupure si le montant du retrait est plus petit que le prix de cette même  coupure.
       billets_500 = montant_retrait/500
       // cela permet de calculer le nb de billets au différents prix. En tant qu'Int la variable billets_(prix) prendra une valeur entière. Donc, en divisant le montant par le prix de la coupure, on peut savoir combien de billets de ce prix on peut prendre au maximum pour ne pas dépasser le montant du retrait.
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_500 + " billet(s) de 500 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")

       clavier = readLine()

         if(!(clavier == "o"))clavier_500 =clavier.toInt
         // cette condition permet d'effectuer des calculs lorsque l'utilisateur tape un nombre/chiffre, car le clavier est en String . Il faut donc forcer sa conversion en Int pour pouvoir effectuer des calculs. clavier_(prix) est égal à la valeur Int du clavier. (le ".toInt" force cette conversion).          

         while (!(clavier == "o")&&(!(clavier_500 < billets_500))) {
         //cette boucle permet de répeter la saisie du nombre de billet(s), tant que la valeur du clavier n'est pas égal à "o" (= prendre tous les billet(s) proposé(s) pour un prix) ou que la valeur du clavier en Int n'est pas inférieure au nombre de billet(s) proposé(s).

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier = readLine()

          if(!(clavier == "o"))clavier_500 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-500*clavier_500
         billets_500= 0
         //l'utilisateur saisi un nombre et non "o". Pour savoir combien d'argent il reste après le retrait d'une coupure il faut soustraite le montant_retrait par le prix multiplier par le nombre saisi(clavier_(prix)). Le billets_500= 0 permet au programme de comprendre que le nombre de billets a été choisi par l'utilisateur. il n'est donc pas égal aux nombres de billets proposés par le programme. Le programme n'affichera donc pas le nombre de billets proposés par le programme lorsque c'est l'utilisateur qui choisit le nombre de billets.
         }
         if (clavier == "o"){

         montant_retrait=montant_retrait-500*billets_500
         clavier_500 = 0
         //Il s'agit du même calcul que le précédent, sauf que cette fois-ci l'utilisateur saisi "o". Par conséquent, le prix sera multiplier cette fois-ci par billets_(prix). clavier_500 = 0 permet au code de comprendre, que lors de l'affichage du nb de billet(s) il doit afficher la valeur de billets_prix et non du clavier_(prix).
         }
       }

       if (montant_retrait>=200){

       billets_200 = montant_retrait/200
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_200 + " billet(s) de 200 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_200 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_200 < billets_200))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

          if(!(clavier == "o"))clavier_200 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-200*clavier_200
         billets_200= 0

         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-200*billets_200
         clavier_200 = 0
         }
       }

       if (montant_retrait>=100){

       billets_100 = montant_retrait/100
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_100 + " billet(s) de 100 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_100 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_100 < billets_100))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_100 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-100*clavier_100
         billets_100= 0
         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-100*billets_100
         clavier_100 = 0
         }
       }

       if (montant_retrait>=50){

       billets_50 = montant_retrait/50                
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_50 + " billet(s) de 50 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_50 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_50 < billets_50))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_50 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-50*clavier_50
         billets_50= 0
         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-50*billets_50
         clavier_50 = 0
         }
       }
       if (montant_retrait>=20){

       billets_20 = montant_retrait/20              
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_20 + " billet(s) de 20 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_20 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_20 < billets_20))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_20 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-20*clavier_20
         billets_20= 0
         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-20*billets_20
         clavier_20 = 0
         }
       }

       if (montant_retrait>=10){

       billets_10 = montant_retrait/10
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous devez prendre " + billets_10 + " billet(s) de 10 CHF")
       montant_retrait=montant_retrait-10*billets_10
       }


     }
     //fin de grosses coupures
     if (coupure == 2){ 
     //début petites coupures

       if (montant_retrait>=100){

       billets_100 = montant_retrait/100
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_100 + " billet(s) de 100 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_100 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_100 < billets_100))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_100 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-100*clavier_100
         billets_100= 0
         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-100*billets_100
         clavier_100 = 0
         }
       }

       if (montant_retrait>=50){

       billets_50 = montant_retrait/50
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_50 + " billet(s) de 50 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_50 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_50 < billets_50))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_50 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-50*clavier_50
         billets_50= 0
         }
         if (clavier == "o"){
         montant_retrait = montant_retrait - 50 * billets_50
         clavier_50 = 0
         }
       }

       if (montant_retrait >= 20){

       billets_20 = montant_retrait/20
       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous pouvez obtenir au maximum " + billets_20 + " billet(s) de 20 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_20 = clavier.toInt

         while (!(clavier == "o")&&(!(clavier_20 < billets_20))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_20 =clavier.toInt
         }
         if (!(clavier == "o")){

         montant_retrait = montant_retrait - 20 * clavier_20
         billets_20= 0
         }
         if (clavier == "o"){

         montant_retrait = montant_retrait - 20 * billets_20
         clavier_20 = 0
         }
       }

       if (montant_retrait >= 10){billets_10 = montant_retrait/10

       println("Il reste "+ montant_retrait +" CHF à distribuer.\nVous devez prendre " + billets_10 + " billet(s) de 10 CHF")
       montant_retrait = montant_retrait - 10 * billets_10
       }
     }
     //fin petites coupures
     if ((coupure==1)||(coupure==2)){
     println("Veuillez retirer la somme demandée :")

       if(clavier_500>0){

       println(clavier_500 + " billet(s) de 500 CHF")
       }
       if(billets_500>0){

       println(billets_500 + " billet(s) de 500 CHF")
       }
       if(clavier_200>0){

       println(clavier_200 + " billet(s) de 200 CHF")
       } 
       if(billets_200>0){

       println(billets_200 + " billet(s) de 200 CHF")
       } 
       if(clavier_100>0){

       println(clavier_100 + " billet(s) de 100 CHF")
       }
       if(billets_100>0){

       println(billets_100 + " billet(s) de 100 CHF")
       }

       if(clavier_50>0){

       println(clavier_50 + " billet(s) de 50 CHF")
       }
       if(billets_50>0){

       println(billets_50 + " billet(s) de 50 CHF")
       }
       if(clavier_20>0){

       println(clavier_20 + " billet(s) de 20 CHF")
       } 
       if(billets_20>0){

       println(billets_20 + " billet(s) de 20 CHF")
       }
       if(billets_10>0){

       println(billets_10 + " billet(s) de 10 CHF")
       }
     }
     // ces conditions permettent d'afficher les nb de billets choisies, tant que les coupures sont grosses ou petites. Le programme n'affiche pas le nb de billets lorsque ce nombre=0

     if (devise == 2){
     // debut coupures Euros
     montant_retrait = readLine("Indiquez le montant de votre retrait > ").toInt 
     montant_compte_EUR=comptes(id)*1.05
     // pour savoir quelle est le plafond de retrait pour des retraits en EUR il faut tout convertir dans la même devise.C'est pourquoi, on multiplie le compte par 1.05 (taux de change EUR). 
     plafond = montant_compte_EUR*0.1 
       while (!(montant_retrait % 10 == 0)||(!(montant_retrait >= 0))||(!(montant_retrait <= plafond))){ 



         if ((!(montant_retrait % 10 == 0))||(!(montant_retrait >= 0))) println ("Le montant doit être un multiple de 10")

         if (!(montant_retrait <= plafond)){

         printf("Le plafond de retrait autorisé est de : %.2f EUR\n", plafond)

         }

         montant_retrait = readLine("Veuillez de nouveau indiquer le montant de votre retrait > ").toInt 
       }
       montant = montant_retrait

       if (montant_retrait>=100){

       billets_100 = montant_retrait/100
       println("Il reste "+ montant_retrait +" EUR à distribuer.\nVous pouvez obtenir au maximum " + billets_100 + " billet(s) de 100 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_100 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_100 < billets_100))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_100 =clavier.toInt
         }   
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-100*clavier_100
         billets_100= 0
         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-100*billets_100        
         clavier_100 = 0
         }
       }
       if (montant_retrait>=50){

       billets_50 = montant_retrait/50
       println("Il reste "+ montant_retrait +" EUR à distribuer.\nVous pouvez obtenir au maximum " + billets_50 + " billet(s) de 50 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_50 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_50 < billets_50))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_50 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-50*clavier_50
         billets_50= 0
         }
         if (clavier== "o"){
         montant_retrait=montant_retrait-50*billets_50
         clavier_50 = 0
         }
       }
       if (montant_retrait>=20){

       billets_20 = montant_retrait/20
       println("Il reste "+ montant_retrait +" EUR à distribuer.\nVous pouvez obtenir au maximum " + billets_20 + " billet(s) de 20 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
       clavier= readLine()

         if(!(clavier == "o"))clavier_20 =clavier.toInt

         while (!(clavier == "o")&&(!(clavier_20 < billets_20))) {

         println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
         clavier= readLine()

           if(!(clavier == "o"))clavier_20 =clavier.toInt
         }
         if (!(clavier=="o")){

         montant_retrait=montant_retrait-20*clavier_20
         billets_20= 0
         }
         if (clavier== "o"){

         montant_retrait=montant_retrait-20*billets_20
         clavier_20 = 0
         }
       }

       if (montant_retrait>=10){

       billets_10 = montant_retrait/10
       println("Il reste "+ montant_retrait +" EUR à distribuer.\nVous devez prendre " + billets_10 + " billet(s) de 10 EUR")
       montant_retrait=montant_retrait-10*billets_10    
       }
       println("Veuillez retirer la somme demandée :")

         if(clavier_100>0){

         println(clavier_100 + " billet(s) de 100 EUR")
         }
         if(billets_100>0){

         println(billets_100 + " billet(s) de 100 EUR")
         }
         if(clavier_50>0){

         println(clavier_50 + " billet(s) de 50 EUR")
         }        
         if(billets_50>0){

         println(billets_50 + " billet(s) de 50 EUR")
         }
         if(clavier_20>0){

         println(clavier_20 + " billet(s) de 20 EUR")
         }        
         if(billets_20>0){

         println(billets_20 + " billet(s) de 20 EUR")
         } 
         if(billets_10>0){

         println(billets_10 + " billet(s) de 10 EUR")
         }
     }
     //fin devise=2 (Euros)

     if (devise==1) comptes(id)-=montant
     //il faut soustraitre le montant de retrait au montant du  compte, on utilise donc le montant pour avoir la valeur initiale du montant_retrait.

     if (devise==2) comptes(id)-= montant*0.95
     //pour la devise EUR il faut faire de même sauf qu'il faut multiplier le montant par 0.95 pour qu'il soit en CHF (taux de change d'EUR à CHF = 0.95)   

     printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))

  }
  //fin de la métode retrait

  def changepin(id : Int, codespin : Array[String]) : Unit = {
  //début méthode changepin 
      codespin(id) = readLine ("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      // on demande le nouveau code pin, celui-ci doit être rattacher à l'identifiant de l'utilisateur. Par conséquent, nous devons changer la cellule (code PIN) sur l'index correspondant à l'identifiant dans le tableau codepin.
      while (codespin(id).length<8){codespin(id) = readLine ("Votre code pin ne contient pas au moins 8 caractères > ")
      // on vérifie que le code pin contient au moins 8 caractères. Tant que la longeur de la cellule codespin est inférieure à 8, on redemande le code pin.
      }

 }
 //fin méthode changepin




def main(args: Array[String]): Unit = {

var nbClients=100
// nbClients est le nombre de comptes que l'on peut créer.(Comptes et codespin ont la même taille et cette taille est la valeur de nbClients). 
var nb_tentatives = 3
// nb_tentatives est le nombre de tentatives pour le code pin.
var PIN = "x"
// PIN est le code pin de l'utilisateur.
var comptes =  Array.fill(nbClients)(1200.0)
// comptes est le tableau des comptes des utilisateurs dont le montant est initialisé à 1200.0.
var codespin = Array.fill(nbClients)("INTRO1234")
// codespin est le tableau des code pins des utilisateurs dont le code est initialisé à "INTRO1234"
var choix= 0
// choix est le choix des différentes options de l'utilisateur (ex : dépôt).
var id= 0 
//il s'agit du numéro d'identifiant choisi (index des comptes et des codespin).
 while (id<nbClients)  {
 // cette boucle permet de recommencer la demande de l'identifiant et celle du code pin, lorsque l'utilisateur a fait plus de 3 tentatives ou qu'il a choisit l'opération "Terminer"

id= readLine("Saisissez l'identifiant du client > ").toInt

  if (id>=100){

  println("Cet identifiant n’est pas valable.")
  sys.exit(0)
  // le programme doit s'arrêter si l'identifiant est supérieur ou égal à 100.
  }
  nb_tentatives = 3
  // nb_tentatives est remis à 3 à chaque fois que l'opération "Terminer" est choisit.
  PIN= "x"
  // PIN est remis à "x" à chaque fois que l'opération "Terminer". Si on reprennais 2 fois le même identifiant, le code Pin resterait le même, le programme passerait donc directement au choix des opérations.
  choix= 0   
  // rénitialise le choix des opérations à chaque fois que l'opération "Terminer" est choisit.

  while ((!(nb_tentatives==0))&&(codespin(id)!= PIN)){
  // cette boucle permet de commencer le code pin si le nombre de tentatives n'est pas égal à 0 et que le codepin n'est pas le bon.
  PIN = readLine("\nSaisissez votre code pin > ")

    while ((codespin(id) != PIN)&&(!(nb_tentatives==0))){
    // lorsque le nombre de tentatives n'est pas égal à 0 et que le codepin n'est pas le bon, on doit diminuer le nombre de tentatives de 1, comme écrit ci-dessous : 
    nb_tentatives-= 1
    println("Code pin erroné, il vous reste " + nb_tentatives + " tentatives > ")

      if (!(nb_tentatives==0)){

      PIN = readLine("Saisissez votre code pin > ")
      }
      // si on fait 3 fois faux le code pin, le programme redemanderait le codepin une dernière fois si il n'y avait pas cette condition.
    }
  }
  //fin code pin
  if (nb_tentatives==0){

  println ("Trop d’erreurs, abandon de l’identification")
  }
  // si le nombre de tentatives est égal à 0, le programme doit afficher l'abandon de l'identification.

  while ((codespin(id) == PIN)&&(choix!=5)){
  // cette boucle permet de faire tourner le programme tant que l'utilisateur ne choisit pas l'opération "Terminer" et qu'il a réussit son code pin.

  choix = readLine("Voici le menu des opérations disponibles au bancomat :\nVous devez choisir l'une des opérations suivantes :\n 1) Dépôt\n 2) Retrait\n 3) Consultation du compte\n 4) Changement du code pin\n 5) Terminer\nVotre choix : ").toInt

    if (choix==1){
    depot(id, comptes)
    }
    // On insère la méthode dépot avec comme paramètre l'identifiant de l'utilisateur et le tableau des comptes. Pour que l'opération de dépot soit effectuée.
    if (choix==2){
    retrait(id, comptes)
    }
    // On insère la méthode retrait avec comme paramètre l'identifiant de l'utilisateur et le tableau des comptes. Pour que l'opération de retrait soit effectuée.
    if (choix==3){ 
    printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id)) 
    }
    //L'opération de consultation du compte.
    if (choix ==4){
    changepin(id, codespin)
    }
    //On insère la méthode changepin avec comme paramètre l'identifiant de l'utilisateur et le tableau codespin. Pour que l'opération de changement de code pin soit effectuée.
  }
  //fin boucle choix des opérations
  if (choix ==5){
  println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
  }
 }
 //fin de la boucle choix identifiant
 }
}

