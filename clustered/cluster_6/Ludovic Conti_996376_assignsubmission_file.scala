//Assignment: Ludovic Conti_996376_assignsubmission_file

import scala.io.StdIn._

object Main {

  // Méthode de dépôt
  def depot(id: Int, comptes: Array[Double]): Unit = {
      var montantDepot = 0
      var deviseDepot = ""
      do {
        while (!(deviseDepot == "CHF" || deviseDepot == "EUR")) {
          deviseDepot = readLine("\nIndiquez la devise du dépôt : CHF ; EUR > ")
          if (!(deviseDepot == "CHF" || deviseDepot == "EUR")) {
            println("\nLe choix doit être CHF ou EUR")
          }
        }
        montantDepot = readLine("\nIndiquez le montant du dépôt : > ").toInt
        if (montantDepot % 10 != 0) {
          println("\nLe montant doit être un multiple de 10.")
        } else {
          if (deviseDepot == "CHF") {
            comptes(id) += montantDepot
          } else if (deviseDepot == "EUR") {
            comptes(id) += montantDepot * 0.95
          }
          println(
            "\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de ",comptes(id))
        }
      } while (montantDepot % 10 != 0)
  }

  // méthode de retrait 
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    var deviseRetrait = ""
    var montantRetrait = 0
    var plafond = 0.1 * comptes(id)
    var coupures = 0
    var tailleCoupure = ""
    var billet500 = 0
    var billet200 = 0
    var billet100 = 0
    var billet50 = 0
    var billet20 = 0
    var billet10 = 0

    do {
      while (!(deviseRetrait == "CHF" || deviseRetrait == "EUR")) {
        deviseRetrait = readLine("\nIndiquez la devise du retrait : CHF ; EUR > ")
        if (!(deviseRetrait == "CHF" || deviseRetrait == "EUR")) {
          println("\nLe choix doit être CHF ou EUR")
        }
      }

      montantRetrait = readLine("\nIndiquez le montant du retrait : > ").toInt

      if (montantRetrait % 10 != 0) {
        println("\nLe montant doit être un multiple de 10.")
      } else if (montantRetrait > plafond) {
        println("\nVotre plafond de retrait autorisé est de : " + plafond + " CHF.")
      } else if (montantRetrait < 0) {
        println("\nLe nombre ne peut pas être négatif.")
      } else {

          // Pour les CHF
          if (deviseRetrait == "CHF") {
            comptes(id) -= montantRetrait

            if (montantRetrait >= 200) {
              do {
              coupures = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt

              if (!(coupures == 1 || coupures == 2)) {
                println("\nLe chiffre doit être 1 ou 2")
              }

              // Grandes coupures
              if (coupures == 1) {
                tailleCoupure = ""

              if ((montantRetrait >= billet500) && (montantRetrait/500 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 500 + " billet(s) de 500 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 500) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 500))
                    }
                    billet500 = nombre
                    montantRetrait = montantRetrait - nombre * 500
                  } 
                   
                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet500 = montantRetrait / 500
                    montantRetrait = montantRetrait - (montantRetrait / 500) * 500
                  }
                } while (nombre < 0 || nombre > montantRetrait / 500)
              }
                
                
              if ((montantRetrait >= billet200) && (montantRetrait/200 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 200 + " billet(s) de 200 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 200) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 200))
                    }
                    billet200 = nombre
                    montantRetrait = montantRetrait - nombre * 200
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet200 = montantRetrait / 200
                    montantRetrait = montantRetrait - (montantRetrait / 200) * 200
                  }
                } while (nombre < 0 || nombre > montantRetrait / 200)
              }

              if ((montantRetrait >= billet100) && (montantRetrait/100 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 100 + " billet(s) de 100 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 100) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 100))
                    }
                    billet100 = nombre
                    montantRetrait = montantRetrait - nombre * 100
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet100 = montantRetrait / 100
                    montantRetrait = montantRetrait - (montantRetrait / 100) * 100
                  }
                } while (nombre < 0 || nombre > montantRetrait / 100)
              }

              if ((montantRetrait >= billet50) && (montantRetrait/50 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 50 + " billet(s) de 50 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 50) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 50))
                    }
                    billet50 = nombre
                    montantRetrait = montantRetrait - nombre * 50
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet50 = montantRetrait / 50
                    montantRetrait = montantRetrait - (montantRetrait / 50) * 50
                  }
                } while (nombre < 0 || nombre > montantRetrait / 50)
              }

              if ((montantRetrait >= billet20) && (montantRetrait/20 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 20 + " billet(s) de 20 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 20) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 20))
                    }
                    billet20 = nombre
                    montantRetrait = montantRetrait - nombre * 20
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet20 = montantRetrait / 20
                    montantRetrait = montantRetrait - (montantRetrait / 20) * 20
                  }
                } while (nombre < 0 || nombre > montantRetrait / 20)
              }
                
                if ((montantRetrait >= 10) && (montantRetrait/10 >= 1)) {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer en " + montantRetrait / 10 + " billet(s) de 10 CHF")
                  billet10 = montantRetrait / 10
                  montantRetrait = montantRetrait - (montantRetrait / 10) * 10
                }
                // petite coupure
            } else {
              tailleCoupure = ""
              if ((montantRetrait >= billet100) && (montantRetrait/100 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 100 + " billet(s) de 100 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 100) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 100))
                    }
                    billet100 = nombre
                    montantRetrait = montantRetrait - nombre * 100
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet100 = montantRetrait / 100
                    montantRetrait = montantRetrait - (montantRetrait / 100) * 100
                  }
                } while (nombre < 0 || nombre > montantRetrait / 100)
              }

              if ((montantRetrait >= billet50) && (montantRetrait/50 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 50 + " billet(s) de 50 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 50) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 50))
                    }
                    billet50 = nombre
                    montantRetrait = montantRetrait - nombre * 50
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet50 = montantRetrait / 50
                    montantRetrait = montantRetrait - (montantRetrait / 50) * 50
                  }
                } while (nombre < 0 || nombre > montantRetrait / 50)
              }

              if ((montantRetrait >= billet20) && (montantRetrait/20 >= 1)) {
                var nombre = 0
                do {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 20 + " billet(s) de 20 CHF")
                  tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (tailleCoupure != "o") {
                    var nombre = tailleCoupure.toInt
                    if (nombre < 0 || nombre > montantRetrait / 20) {
                      println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 20))
                    }
                    billet20 = nombre
                    montantRetrait = montantRetrait - nombre * 20
                  } 

                      if (tailleCoupure == "o") {
                         tailleCoupure = "x"
                    billet20 = montantRetrait / 20
                    montantRetrait = montantRetrait - (montantRetrait / 20) * 20
                  }
                } while (nombre < 0 || nombre > montantRetrait / 20)
              }

                if ((montantRetrait >= 10) && (montantRetrait/10 >= 1)) {
                  println("\nIl reste " + montantRetrait + " CHF à distribuer en " + montantRetrait / 10 + " billet(s) de 10 CHF")
                  billet10 = montantRetrait / 10
                  montantRetrait = montantRetrait - (montantRetrait / 10) * 10
                }
            } // fin petites coupures  

                println("\nVeuillez retirer la somme demandée : ")
                if (billet500 > 0) {
                  println(billet500 + " billet(s) de 500 CHF ")
                }
                if (billet200 > 0) {
                  println(billet200 + " billet(s) de 200 CHF ")
                }
                if (billet100 > 0) {
                  println(billet100 + " billet(s) de 100 CHF ")
                }
                if (billet50 > 0) {
                  println(billet50 + " billet(s) de 50 CHF ")
                }
                if (billet20 > 0) {
                  println(billet20 + " billet(s) de 20 CHF ")
                }
                if (billet10 > 0) {
                  println(billet10 + " billet(s) de 10 CHF ")
                }
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
                println("")

            } // fin des >= 200

              while (!(coupures == 1 || coupures == 2))

        } else if (montantRetrait < 200) {
            // Ici c'est si moin de 200

            coupures = 2
            tailleCoupure = ""
            if ((montantRetrait >= billet100) && (montantRetrait/100 >= 1)) {
              var nombre = 0
              do {
                println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 100 + " billet(s) de 100 CHF")
                tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (tailleCoupure != "o") {
                  var nombre = tailleCoupure.toInt
                  if (nombre < 0 || nombre > montantRetrait / 100) {
                    println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 100))
                  }
                  billet100 = nombre
                  montantRetrait = montantRetrait - nombre * 100
                } 

                    if (tailleCoupure == "o") {
                       tailleCoupure = "x"
                  billet100 = montantRetrait / 100
                  montantRetrait = montantRetrait - (montantRetrait / 100) * 100
                }
              } while (nombre < 0 || nombre > montantRetrait / 100)
            }

            if ((montantRetrait >= billet50) && (montantRetrait/50 >= 1)) {
              var nombre = 0
              do {
                println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 50 + " billet(s) de 50 CHF")
                tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (tailleCoupure != "o") {
                  var nombre = tailleCoupure.toInt
                  if (nombre < 0 || nombre > montantRetrait / 50) {
                    println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 50))
                  }
                  billet50 = nombre
                  montantRetrait = montantRetrait - nombre * 50
                } 

                    if (tailleCoupure == "o") {
                       tailleCoupure = "x"
                  billet50 = montantRetrait / 50
                  montantRetrait = montantRetrait - (montantRetrait / 50) * 50
                }
              } while (nombre < 0 || nombre > montantRetrait / 50)
            }

            if ((montantRetrait >= billet20) && (montantRetrait/20 >= 1)) {
              var nombre = 0
              do {
                println("\nIl reste " + montantRetrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 20 + " billet(s) de 20 CHF")
                tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (tailleCoupure != "o") {
                  var nombre = tailleCoupure.toInt
                  if (nombre < 0 || nombre > montantRetrait / 20) {
                    println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 20))
                  }
                  billet20 = nombre
                  montantRetrait = montantRetrait - nombre * 20
                } 

                    if (tailleCoupure == "o") {
                       tailleCoupure = "x"
                  billet20 = montantRetrait / 20
                  montantRetrait = montantRetrait - (montantRetrait / 20) * 20
                }
              } while (nombre < 0 || nombre > montantRetrait / 20)
            }

              if ((montantRetrait >= 10) && (montantRetrait/10 >= 1)) {
                println("\nIl reste " + montantRetrait + " CHF à distribuer en " + montantRetrait / 10 + " billet(s) de 10 CHF")
                billet10 = montantRetrait / 10
                montantRetrait = montantRetrait - (montantRetrait / 10) * 10
              }
              
              println("\nVeuillez retirer la somme demandée : ")
              if (billet500 > 0) {
                println(billet500 + " billet(s) de 500 CHF ")
              }
              if (billet200 > 0) {
                println(billet200 + " billet(s) de 200 CHF ")
              }
              if (billet100 > 0) {
                println(billet100 + " billet(s) de 100 CHF ")
              }
              if (billet50 > 0) {
                println(billet50 + " billet(s) de 50 CHF ")
              }
              if (billet20 > 0) {
                println(billet20 + " billet(s) de 20 CHF ")
              }
              if (billet10 > 0) {
                println(billet10 + " billet(s) de 10 CHF ")
              }
              printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
              println("")
          } // fin des < 200

          } // fin devise retrait CHF
        // pour les EUR
         else if (deviseRetrait == "EUR") {
           comptes(id) -= montantRetrait * 0.95
           tailleCoupure = ""
         if ((montantRetrait >= billet100) && (montantRetrait/100 >= 1)) {
           var nombre = 0
           do {
             println("\nIl reste " + montantRetrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 100 + " billet(s) de 100 EUR")
             tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
             if (tailleCoupure != "o") {
               var nombre = tailleCoupure.toInt
               if (nombre < 0 || nombre > montantRetrait / 100) {
                 println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 100))
               }
               billet100 = nombre
               montantRetrait = montantRetrait - nombre * 100
             } 

                 if (tailleCoupure == "o") {
                    tailleCoupure = "x"
               billet100 = montantRetrait / 100
               montantRetrait = montantRetrait - (montantRetrait / 100) * 100
             }
           } while (nombre < 0 || nombre > montantRetrait / 100)
         }

         if ((montantRetrait >= billet50) && (montantRetrait/50 >= 1)) {
           var nombre = 0
           do {
             println("\nIl reste " + montantRetrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 50 + " billet(s) de 50 EUR")
             tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
             if (tailleCoupure != "o") {
               var nombre = tailleCoupure.toInt
               if (nombre < 0 || nombre > montantRetrait / 50) {
                 println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 50))
               }
               billet50 = nombre
               montantRetrait = montantRetrait - nombre * 50
             } 

                 if (tailleCoupure == "o") {
                    tailleCoupure = "x"
               billet50 = montantRetrait / 50
               montantRetrait = montantRetrait - (montantRetrait / 50) * 50
             }
           } while (nombre < 0 || nombre > montantRetrait / 50)
         }

         if ((montantRetrait >= billet20) && (montantRetrait/20 >= 1)) {
           var nombre = 0
           do {
             println("\nIl reste " + montantRetrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + montantRetrait / 20 + " billet(s) de 20 EUR")
             tailleCoupure = readLine("\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
             if (tailleCoupure != "o") {
               var nombre = tailleCoupure.toInt
               if (nombre < 0 || nombre > montantRetrait / 20) {
                 println("\nLe montant est trop haut ou invalide. Veuillez saisir un montant inférieur ou égal à " + (montantRetrait / 20))
               }
               billet20 = nombre
               montantRetrait = montantRetrait - nombre * 20
             } 

                 if (tailleCoupure == "o") {
                    tailleCoupure = "x"
               billet20 = montantRetrait / 20
               montantRetrait = montantRetrait - (montantRetrait / 20) * 20
             }
           } while (nombre < 0 || nombre > montantRetrait / 20)
         }

           if ((montantRetrait >= 10) && (montantRetrait/10 >= 1)) {
             println("\nIl reste " + montantRetrait + " EUR à distribuer en " + montantRetrait / 10 + " billet(s) de 10 EUR")
             billet10 = montantRetrait / 10
             montantRetrait = montantRetrait - (montantRetrait / 10) * 10
           }
           println("\nVeuillez retirer la somme demandée : ")
           if (billet500 > 0) {
             println(billet500 + " billet(s) de 500 EUR ")
           }
           if (billet200 > 0) {
             println(billet200 + " billet(s) de 200 EUR ")
           }
           if (billet100 > 0) {
             println(billet100 + " billet(s) de 100 EUR ")
           }
           if (billet50 > 0) {
             println(billet50 + " billet(s) de 50 EUR ")
           }
           if (billet20 > 0) {
             println(billet20 + " billet(s) de 20 EUR ")
           }
           if (billet10 > 0) {
             println(billet10 + " billet(s) de 10 EUR ")
           }
           printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
           println("")
           } // fin euro
      } 

    } while (!(montantRetrait % 10 == 0 && montantRetrait <= plafond && montantRetrait >= 0)) 
  }

  // méthode de changement du code pin
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var nouveauCodePin = ""
    var saisieValide = false

    while (!saisieValide) {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      nouveauCodePin = readLine()

      if (nouveauCodePin.length >= 8) {
        saisieValide = true
      } else {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      }
    }

    codespin(id) = nouveauCodePin
    println("Code pin mis à jour avec succès.")
  }

  // consultation 
  def consultation(id: Int, comptes: Array[Double]): Unit = {
     printf("\nLe montant disponible sur votre compte est de : %.2f \n", comptes(id))
  }

  def main(args: Array[String]): Unit = {


    var nbclients = 100
    var comptes = Array.fill[Double](nbclients)(1200.0)
    var codespin = Array.fill[String](nbclients)("INTRO1234")
    var essais = 3
    var idClient = -1
    var operation = 0
    var id = -1

    // Boucle pour la saisie de l'identifiant
    while (essais > 0 && idClient == -1) {
      println("Saisissez votre code identifiant >")
      val idSaisi = readInt()

      // Vérification de l'identifiant
      if (idSaisi >= 0 && idSaisi < nbclients) {
        idClient = idSaisi
        var codeSaisiReussi = false

        // Boucle pour la saisie du code PIN
        while (essais > 0 && !codeSaisiReussi) {
          println("Saisissez votre code pin >")
          val codeSaisi = readLine()

          // Vérification du code PIN
          if (codeSaisi == codespin(idClient)) {
            codeSaisiReussi = true
            println("Identification réussie!")

            // Ajoutez ici la logique pour le menu et les opérations
            while (operation != 5) {
              // Vérification du chiffre pour l'opération
              while (!(operation >= 1 && operation <= 5)) {
                operation = readLine(
                  "Choisissez votre opération : \n \t 1) Dépôt \n \t 2) Retrait \n \t 3) Consultation du compte \n \t 4) Changement du code pin \n \t 5) Terminer \n Votre choix : "
                ).toInt
                if (!(operation >= 1 && operation <= 5)) {
                  println("\nLe chiffre doit être 1, 2, 3, 4 ou 5")
                }
              }

              // Opération de dépôt
              if (operation == 1) {
                depot(idClient, comptes)
                operation = 0
              }

              // Opération de retrait
              if (operation == 2) {
                retrait(idClient, comptes)
                operation = 0
              }

              // Opération de consultation du compte
              if (operation == 3) {
            consultation(idClient, comptes)
                 operation = 0
                       }

              // Opération de changement de code pin
              if (operation == 4) {
                changepin(idClient, codespin)
                  operation = 0
                }

            } // Fin de la boucle des opérations
          } else {
            essais -= 1
            println(s"Code pin erroné, il vous reste $essais tentatives.")
          }
        }

        // Opération de fin
        if (operation == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          idClient = -1
          essais = 3
          operation = 0
        }

        if (!codeSaisiReussi) {
          println("Trop d’erreurs, abandon de l’identification.")
          idClient = -1
          essais = 3
          println("")
        }

      } else {
        println("Cet identifiant n’est pas valable.")
        return
      }
    }
  }
}
