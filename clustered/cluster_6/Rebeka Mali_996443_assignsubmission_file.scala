//Assignment: Rebeka Mali_996443_assignsubmission_file

import scala.io.StdIn._

object Main {
  // Définition de l'opération de dépôt
  def depot(id: Int, comptes: Array[Double]): Unit = {
    val devise_chf = 1
    val devise_eur = 2
    var montant_du_depot = 1
    var devise = 0

    while ((devise != devise_chf) && (devise != devise_eur)) {
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
      devise = readInt()
    }

    while (montant_du_depot % 10 != 0) {
      println("Indiquez le montant du dépôt > ")
      montant_du_depot = readInt()
      if (montant_du_depot % 10 != 0) println("Le montant doit être un multiple de 10")
    }

    if (montant_du_depot % 10 == 0) {
      if (devise == devise_chf) {
        comptes(id) = comptes(id) + montant_du_depot
      } else if (devise == devise_eur) {
        comptes(id) = comptes(id) + (montant_du_depot * 0.95)
      }
      printf(
        "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f \n",
        comptes(id)
      )
    }
  }
  //Fin de la définition de l'opération de dépôt


//Def de l'opération de retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    val devise_chf = 1
    val devise_eur = 2
    var devise = 0
    val retrait_autorise = (comptes(id)/ 100) * 10
    var coupures = 0
    var retrait = 1
    var reste_retrait = 0
    var coupure_500 = 0
    var coupure_200 = 0
    var coupure_100 = 0
    var coupure_50 = 0
    var coupure_20 = 0
    var coupure_10 = 0
    var operation_termine = false
    val grosses_coupures = 1
    val petites_coupures = 2
    val message_fin: String = "Veuillez retirer la sommme demandée : "

    //si la devise est fausse
    while ((devise != devise_chf) && (devise != devise_eur)) {
      println("Indiquez la devise : 1 CHF; 2 : EUR")
      devise = readInt()
    }

    while ((retrait % 10 != 0) || (retrait > retrait_autorise)) {
      println("Indiquez le montant du retrait")
      retrait = readInt()
      if (retrait > retrait_autorise) {
        println("Votre plafond de retrait autorisé est de : " + retrait_autorise)
      }
      else if (retrait % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
    }

    reste_retrait = retrait

    //Retrait en CHF
    if (devise == devise_chf) {
      if (retrait >= 200) {

        while ((coupures != grosses_coupures) && (coupures != petites_coupures)) {
          println("En 1) grosses coupures, 2) petites coupures >")
          coupures = readInt()
        }
      } else coupures = petites_coupures


      //retrait en grosses coupures

      if (coupures == grosses_coupures) {

        //coupure 500

        while ((retrait / 500 != 0) && (!operation_termine)) {
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (retrait / 500) + " billet(s) de 500 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          //si le client tape "o"
          if (autre_valeur == "o") {
            coupure_500 = retrait / 500
            operation_termine = true
          }

          //si le client tape une autre valeur
          else if (autre_valeur.toInt < (retrait / 500)) {
            coupure_500 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //on calcule le reste
        reste_retrait = retrait - (coupure_500 * 500)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        //coupure 200

        while ((reste_retrait / 200 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 200) + " billet(s) de 200 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_200 = reste_retrait / 200
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 200)) {
            coupure_200 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste
        reste_retrait = reste_retrait - (coupure_200 * 200)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        //coupure 100

        while ((reste_retrait / 100 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 100) + " billet(s) de 100 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_100 = reste_retrait / 100
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 100)) {
            coupure_100 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste

        reste_retrait = reste_retrait - (coupure_100 * 100)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        //coupure 50

        while ((reste_retrait / 50 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 50) + " billet(s) de 50 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_50 = reste_retrait / 50
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 50)) {
            coupure_50 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste
        reste_retrait = reste_retrait - (coupure_50 * 50)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        //coupure 20

        while ((reste_retrait / 20 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 20) + " billet(s) de 20 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_20 = reste_retrait / 20
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 20)) {
            coupure_20 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste

        reste_retrait = reste_retrait - (coupure_20 * 20)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        // coupure 10

        while ((reste_retrait / 10 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 10) + " billet(s) de 10 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_10 = reste_retrait / 10
            operation_termine = true
          }

        }

      }


      else if (coupures == petites_coupures) {

        //que 100, 50, 20, 10

        //coupure 100

        while ((reste_retrait / 100 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 100) + " billet(s) de 100 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_100 = reste_retrait / 100
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 100)) {
            coupure_100 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste
        reste_retrait = reste_retrait - (coupure_100 * 100)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        //coupure 50

        while ((reste_retrait / 50 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 50) + " billet(s) de 50 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_50 = reste_retrait / 50
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 50)) {
            coupure_50 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste

        reste_retrait = reste_retrait - (coupure_50 * 50)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        //coupure 20

        while ((reste_retrait / 20 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 20) + " billet(s) de 20 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          val autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_20 = reste_retrait / 20
            operation_termine = true
          }

          else if (autre_valeur.toInt < (reste_retrait / 20)) {
            coupure_20 = autre_valeur.toInt
            operation_termine = true
          }
        }

        //reste
        reste_retrait = reste_retrait - (coupure_20 * 20)

        if (reste_retrait != 0) {
          operation_termine = false
        }

        // coupure 10

        while ((reste_retrait / 10 != 0) && (!operation_termine)) {
          println("Il reste " + reste_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (reste_retrait / 10) + " billet(s) de 10 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          var autre_valeur = readLine()

          if (autre_valeur == "o") {
            coupure_10 = reste_retrait / 10
            operation_termine = true
          }
        }
      }

      // fin de coupures CHF, sans "else if" car sinon ça marche pas
      println(message_fin)
      if (coupure_500 > 0) {
        println(coupure_500.toString + " billet(s) de 500 CHF")
      }
      if (coupure_200 > 0) {
        println(coupure_200.toString + " billet(s) de 200 CHF")
      }
      if (coupure_100 > 0) {
        println(coupure_100.toString + " billet(s) de 100 CHF")
      }
      if (coupure_50 > 0) {
        println(coupure_50.toString + " billet(s) de 50 CHF")
      }
      if (coupure_20 > 0) {
        println(coupure_20.toString + " billet(s) de 20 CHF")
      }
      if (coupure_10 > 0) {
        println(coupure_10.toString + " billet(s) de 10 CHF")
      }

      comptes(id) = comptes(id) - retrait
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id)) //compted(id) pour mettre à jour le montant sur le compte
    }


    //devise en EUR

    else if (devise == devise_eur) {

      //100, 50, 20, 10

      //coupure 100

      while ((reste_retrait / 100 != 0) && (!operation_termine)) {
        println("Il reste " + reste_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + (reste_retrait / 100) + " billet(s) de 100 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        val autre_valeur = readLine()

        if (autre_valeur == "o") {
          coupure_100 = reste_retrait / 100
          operation_termine = true
        }

        else if (autre_valeur.toInt < (reste_retrait / 100)) {
          coupure_100 = autre_valeur.toInt
          operation_termine = true
        }
      }

      //reste
      reste_retrait = reste_retrait - (coupure_100 * 100)

      if (reste_retrait != 0) {
        operation_termine = false
      }

      //coupure 50

      while ((reste_retrait / 50 != 0) && (!operation_termine)) {
        println("Il reste " + reste_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + (reste_retrait / 50) + " billet(s) de 50 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        val autre_valeur = readLine()

        if (autre_valeur == "o") {
          coupure_50 = reste_retrait / 50
          operation_termine = true
        }

        else if (autre_valeur.toInt < (reste_retrait / 50)) {
          coupure_50 = autre_valeur.toInt
          operation_termine = true
        }
      }

      //reste

      reste_retrait = reste_retrait - (coupure_50 * 50)

      if (reste_retrait != 0) {
        operation_termine = false
      }

      //coupure 20

      while ((reste_retrait / 20 != 0) && (!operation_termine)) {
        println("Il reste " + reste_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + (reste_retrait / 20) + " billet(s) de 20 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        var autre_valeur = readLine()

        if (autre_valeur == "o") {
          coupure_20 = reste_retrait / 20
          operation_termine = true
        }

        else if (autre_valeur.toInt < (reste_retrait / 20)) {
          coupure_20 = autre_valeur.toInt
          operation_termine = true
        }
      }

      //reste

      reste_retrait = reste_retrait - (coupure_20 * 20)

      if (reste_retrait != 0) {
        operation_termine = false
      }

      // coupure 10

      while ((reste_retrait / 10 != 0) && (!operation_termine)) {
        println("Il reste " + reste_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + (reste_retrait / 10) + " billet(s) de 10 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        var autre_valeur = readLine()

        if (autre_valeur == "o") {
          coupure_10 = reste_retrait / 10
          operation_termine = true
        }
      }

      //messages de fin pour EUR, sans "else if" car sinon ça marche pas
      println(message_fin)
      if (coupure_100 > 0) {
        println(coupure_100.toString + " billet(s) de 100 EUR")
      }
      if (coupure_50 > 0) {
        println(coupure_50.toString + " billet(s) de 50 EUR")
      }
      if (coupure_20 > 0) {
        println(coupure_20.toString + " billet(s) de 20 EUR")
      }
      if (coupure_10 > 0) {
        println(coupure_10.toString + " billet(s) de 10 EUR")
      }
      comptes(id) = comptes(id) - (retrait * 0.95)
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }

  }
  //fin def de retrait

  //def pour changer le pin
  def changepin(id : Int, codespin : Array[String]) : Unit = {

    //Booleans
    var retour_menu_operation = true
    var premiere_operation = false

    var nouveau_mdp = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")

    while (nouveau_mdp.length < 8) {
      nouveau_mdp = readLine("Votre code pin ne contient pas au moins 8 caractères\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    }

    if (nouveau_mdp.length >= 8) {
      codespin(id) = nouveau_mdp //on met à jour le code pin
      retour_menu_operation = true
      premiere_operation = true
    }

  }


//Ici commence le main code
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn._

    // On définit la taille du tableau et on initialise la val nbClients
    val nbClients: Int = 100

    // On initialise les tableaux comptes et codespin
    val comptes: Array[Double] = Array.fill(nbClients)(1200.0)
    var codespin: Array[String] = Array.fill(nbClients)("INTRO1234")

    // On initialise l'identifiant
    var identifiant = 0

    // Booleans
    var retour_menu_operation = true
    var premiere_operation = false
    var retour_identifiant_pin = true

    // On définit le montant du compte client
    var montant_compte_client: Double = {
      if ((identifiant >= 0) && (identifiant < nbClients)) {
        comptes(identifiant) // Ainsi nous pouvons acceder à la valeur à l'indice identifiant
      } else {
        0.0 // Sinon donne erreur de type
      }
    }

    // On commence avec l'identification et Pin
    while (retour_identifiant_pin){
      identifiant = readLine("Saisissez votre code identifiant > ").toInt
      if ((identifiant >= 0) && (identifiant < nbClients)) {
        var tentatives = 1
        var pin_client = ""

        while ((codespin(identifiant) != pin_client) && (tentatives <= 3)) {
          println("Saisissez votre code PIN > ")
          pin_client = readLine()

          if (codespin(identifiant) != pin_client) {
            tentatives += 1
            println("Code pin erroné, il vous reste " + (4 - tentatives) + " tentatives >")
          }
        }

        if (codespin(identifiant) == pin_client) {
          premiere_operation = true
          retour_menu_operation = true
          retour_identifiant_pin = false
        } else {
          println("Trop d’erreurs, abandon de l’identification")
          retour_menu_operation = false
          retour_identifiant_pin = true
        }

      } else {
        println("Cet identifiant n'est pas valable.")
        premiere_operation = false
        retour_menu_operation = false
        retour_identifiant_pin = false
      }

      while (retour_menu_operation == true) {
        val menu_operation: String = "Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Chengement du code pin \n 5) Terminer \n Votre choix : "
        println(menu_operation)
        var choix = readLine().toInt

        //Si le client choisit 1,2,3
        if ((choix == 1) || (choix == 2) || (choix == 3) || (choix == 4)) {

          // Opération de dépot

          if (choix == 1) {

            depot(identifiant, comptes) // Ici on fournit l'id du client et le tableau des comptes
            retour_menu_operation = true
          }

          //operation de dépot terminé


          //Operation de consultation compte
          else if (choix == 3) {
            montant_compte_client = comptes(identifiant) // Nous mettons à jour le montant du compte 
            printf("Le montant disponible sur votre compte est de %.2f \n", montant_compte_client)
            retour_menu_operation = true
          }

          //fin operation de consultation


          //Operation de retrait
          else if (choix == 2) {
            retrait(identifiant, comptes)
            retour_menu_operation = true
          }
          //fin operation de retrait

          //Operation de changement de pin
          else if (choix == 4){
            changepin(identifiant, codespin)
            retour_menu_operation = true
          }
          //Fin opération de changement de pin

        //Fin des opérations
        }
        else if (choix == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          retour_identifiant_pin = true
          retour_menu_operation = false
        }
        //Fin de la fin des opérations
      }
    }
  }
}








