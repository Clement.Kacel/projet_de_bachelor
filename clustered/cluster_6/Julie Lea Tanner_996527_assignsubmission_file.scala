//Assignment: Julie Lea Tanner_996527_assignsubmission_file

import scala.io.StdIn._

object Main {


  def depot(clients : Int, comptes : Array[Double]): Unit = {
    var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
    var depotmontant = readLine("Indiquez le montant du dépôt >").toInt
    while (depotmontant%10 != 0){
      println("Le montant doit être un multiple de 10.")
      depotmontant = readLine("Indiquez le montant du dépôt >").toInt
    }
    if (devise == 2){
      comptes(clients) += depotmontant * 0.95
    }
    else {
      comptes(clients) += depotmontant
    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n" , comptes(clients))
  }



  def consultation(clients : Int, comptes : Array[Double]):Unit={
    printf("Le montant disponible sur votre compte est de : %.2f\n" , comptes(clients))
  }
  
  def retrait(clients : Int, comptes : Array[Double]):Unit={
    
    var deviseretrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt

    while (deviseretrait != 1 && deviseretrait != 2){
     deviseretrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
    }
    var retrait = readLine("Indiquez le montant du retrait >").toInt

    while (retrait%10 != 0){
      println("Le montant doit être un multiple de 10.")
      retrait = readLine("Indiquez le montant du retrait >").toInt
    }

    var plafond = 0.0
    if (deviseretrait == 1){
      plafond = comptes(clients)/10
    }
    else if (deviseretrait==2){
      plafond = (comptes(clients) * 1.05)/10
    }
    while (retrait > plafond){
      println("Votre plafond de retrait autorisé est de : "+ plafond)
      retrait = readLine("Indiquez le montant du retrait >").toInt
      while (retrait%10 != 0){
        println("Le montant doit être un multiple de 10.")
        retrait = readLine("Indiquez le montant du retrait >").toInt
      }  
    }
    if (deviseretrait == 1){
      if (retrait >= 200){
      var taillecoupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
      while (taillecoupure != 1 && taillecoupure != 2){
         taillecoupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
      }
        comptes(clients) -= retrait
        if (taillecoupure == 1){
          var billet500f1 = 0
          var billet200f1 = 0
          var billet100f1 = 0
          var billet50f1 = 0
          var billet20f1 = 0
          var billet10f1 = 0
          var saisie = ""
          if ((retrait/500)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " +(retrait/500)+ " billet(s) de 500 CHF")
            saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        while (saisie !="o" && saisie.toInt >= (retrait/500)){
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " +(retrait/500)+ " billet(s) de 500 CHF")
          saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        } 
          if (saisie == "o"){
            billet500f1 = (retrait/500)
            retrait -= billet500f1*500
          }
          else {  
            billet500f1 = saisie.toInt
            retrait -= billet500f1*500
          }
        }
          if ((retrait/200)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/200)+" billet(s) de 200 CHF")
            saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie !="o" && saisie.toInt >= (retrait/200)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/200)+ " billet(s) de 200 CHF")
              saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie == "o"){
              billet200f1 = (retrait/200)
              retrait -= billet200f1*200
            }
            else {  
              billet200f1 = saisie.toInt
              retrait -= billet200f1*200
            }
          }
          if ((retrait/100)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/100)+" billet(s) de 100 CHF")
            saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie !="o" && saisie.toInt >= (retrait/100)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/100)+ " billet(s) de 100 CHF")
              saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie == "o"){
              billet100f1 = (retrait/100)
              retrait -= billet100f1*100
            }
            else {  
              billet100f1 = saisie.toInt
              retrait -= billet100f1*100
            }
          }
          if ((retrait/50)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/50)+" billet(s) de 50 CHF")
            saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie !="o" && saisie.toInt >= (retrait/50)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/50)+ " billet(s) de 50 CHF")
              saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie == "o"){
              billet50f1 = (retrait/50)
              retrait -= billet50f1*50
            }
            else {  
              billet50f1 = saisie.toInt
              retrait -= billet50f1*50
            }
          }
          if ((retrait/20)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/20)+" billet(s) de 20 CHF")
            saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie !="o" && saisie.toInt >= (retrait/20)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/20)+ " billet(s) de 20 CHF")
              saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie == "o"){
              billet20f1 = (retrait/20)
              retrait -= billet20f1*20
            }
            else {  
              billet20f1 = saisie.toInt
              retrait -= billet20f1*20
            }
          }
          if ((retrait/10)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/10)+" billet(s) de 10 CHF")
            saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie !="o" && saisie.toInt >= (retrait/10)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/10)+ " billet(s) de 10 CHF")
              saisie = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie == "o"){
              billet10f1 = (retrait/10)
              retrait -= billet10f1*10
            }
            else {  
              billet10f1 = (retrait/10)
              retrait -= billet10f1*10

            }
          }

          if (billet500f1> 0){
            println(+ billet500f1 + " billet(s) de 500 CHF")
          }
          if (billet200f1> 0){
            println(+ billet200f1 + " billet(s) de 200 CHF")
          }
          if (billet100f1> 0){
            println(+ billet100f1 + " billet(s) de 100 CHF")
          }
          if (billet50f1> 0){
            println(+ billet50f1 + " billet(s) de 50 CHF")
          }
          if (billet20f1> 0){
            println(+ billet20f1 + " billet(s) de 20 CHF")
          }
          if (billet10f1> 0){
            println(+ billet10f1 + " billet(s) de 10 CHF")
          }
        }
        else if (taillecoupure == 2){
          var billet100f2 = 0
          var billet50f2 = 0
          var billet20f2 = 0
          var billet10f2 = 0
          var saisie2 = ""

          if ((retrait/100)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/100)+" billet(s) de 100 CHF")
            saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie2 !="o" && saisie2.toInt >= (retrait/100)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/100)+ " billet(s) de 100 CHF")
              saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie2 == "o"){
              billet100f2 = (retrait/100)
              retrait -= billet100f2*100
            }
            else {  
              billet100f2 = saisie2.toInt
              retrait -= billet100f2*100
            }
          }
          if ((retrait/50)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/50)+" billet(s) de 50 CHF")
            saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie2 !="o" && saisie2.toInt >= (retrait/50)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/50)+ " billet(s) de 50 CHF")
              saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie2 == "o"){
              billet50f2 = (retrait/50)
              retrait -= billet50f2*50
            }
            else {  
              billet50f2 = saisie2.toInt
              retrait -= billet50f2*50
            }
          }
          if ((retrait/20)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/20)+" billet(s) de 20 CHF")
            saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie2 !="o" && saisie2.toInt >= (retrait/20)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/20)+ " billet(s) de 20 CHF")
              saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie2 == "o"){
              billet20f2 = (retrait/20)
              retrait -= billet20f2*20
            }
            else {  
              billet20f2 = saisie2.toInt
              retrait -= billet20f2*20
            }
          }
          if ((retrait/10)>0){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+(retrait/10)+" billet(s) de 10 CHF")
            saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            while (saisie2 !="o" && saisie2.toInt >= (retrait/10)){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " +(retrait/10)+ " billet(s) de 10 CHF")
              saisie2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } 
            if (saisie2 == "o"){
              billet10f2 = (retrait/10)
              retrait -= billet10f2*10
            }
            else {  
              billet10f2 = (retrait/10)
              retrait -= billet10f2*10
            }
          }
          println("Veuillez retirer la somme demandée :")
          if (billet100f2> 0){
            println(+ billet100f2 + " billet(s) de 100 CHF")
          }
          if (billet50f2> 0){
            println(+ billet50f2 + " billet(s) de 50 CHF")
          }
          if (billet20f2> 0){
            println(+ billet20f2 + " billet(s) de 20 CHF")
          }
          if (billet10f2> 0){
            println(+ billet10f2 + " billet(s) de 10 CHF")
          }
      }
      }
      else{
        comptes(clients) -= retrait
        var billet100f = 0
        var billet50f = 0
        var billet20f = 0
        var billet10f = 0
        var saisie3 = ""
        if ((retrait/100)>0){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum "+(retrait/100)+" billet(s) de 100 CHF")
              saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while (saisie3 !="o" && saisie3.toInt >= (retrait/100)){
                println("Il reste " + retrait + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " +(retrait/100)+ " billet(s) de 100 CHF")
                saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              } 
              if (saisie3 == "o"){
                billet100f = (retrait/100)
                retrait -= billet100f*100
              }
              else {  
                billet100f = saisie3.toInt
                retrait -= billet100f*100
              }
            }
            if ((retrait/50)>0){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum "+(retrait/50)+" billet(s) de 50 CHF")
              saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while (saisie3 !="o" && saisie3.toInt >= (retrait/50)){
                println("Il reste " + retrait + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " +(retrait/50)+ " billet(s) de 50 CHF")
                saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              } 
              if (saisie3 == "o"){
                billet50f = (retrait/50)
                retrait -= billet50f*50
              }
              else {  
                billet50f = saisie3.toInt
                retrait -= billet50f*50
              }
            }
            if ((retrait/20)>0){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum "+(retrait/20)+" billet(s) de 20 CHF")
              saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while (saisie3 !="o" && saisie3.toInt >= (retrait/20)){
                println("Il reste " + retrait + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " +(retrait/20)+ " billet(s) de 20 CHF")
                saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              } 
              if (saisie3 == "o"){
                billet20f = (retrait/20)
                retrait -= billet20f*20
              }
              else {  
                billet20f = saisie3.toInt
                retrait -= billet20f*20
              }
            }
            if ((retrait/10)>0){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum "+(retrait/10)+" billet(s) de 10 CHF")
              saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while (saisie3 !="o" && saisie3.toInt >= (retrait/10)){
                println("Il reste " + retrait + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " +(retrait/10)+ " billet(s) de 10 CHF")
                saisie3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              } 
              if (saisie3 == "o"){
                billet10f = (retrait/10)
                retrait -= billet10f*10
              }
              else {  
                billet10f = (retrait/10)
                retrait -= billet10f*10
              }
            }
            println("Veuillez retirer la somme demandée :")
            if (billet100f> 0){
              println(+ billet100f + " billet(s) de 100 CHF")
            }
            if (billet50f> 0){
              println(+ billet50f + " billet(s) de 50 CHF")
            }
            if (billet20f> 0){
              println(+ billet20f + " billet(s) de 20 CHF")
            }
            if (billet10f> 0){
              println(+ billet10f + " billet(s) de 10 CHF")
            }
        }


    }

    else if (deviseretrait == 2){
    comptes(clients) -= retrait * 0.95
      var billet100 = 0
      var billet50 = 0
      var billet20 = 0
      var billet10 = 0
      var saisie4 = ""
      if ((retrait/100)>0){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum "+(retrait/100)+" billet(s) de 100 EUR")
        saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        while (saisie4 !="o" && saisie4.toInt >= (retrait/100)){
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " +(retrait/100)+ " billet(s) de 100 EUR")
          saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        } 
        if (saisie4 == "o"){
          billet100 = (retrait/100)
          retrait -= billet100*100
        }
        else {  
          billet100 = saisie4.toInt
          retrait -= billet100*100
        }
      }
      if ((retrait/50)>0){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum "+(retrait/50)+" billet(s) de 50 EUR")
        saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        while (saisie4 !="o" && saisie4.toInt >= (retrait/50)){
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " +(retrait/50)+ " billet(s) de 50 EUR")
          saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        } 
        if (saisie4 == "o"){
          billet50 = (retrait/50)
          retrait -= billet50*50
        }
        else {  
          billet50 = saisie4.toInt
          retrait -= billet50*50
        }
      }
      if ((retrait/20)>0){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum "+(retrait/20)+" billet(s) de 20 EUR")
        saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        while (saisie4 !="o" && saisie4.toInt >= (retrait/20)){
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " +(retrait/20)+ " billet(s) de 20 EUR")
          saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        } 
        if (saisie4 == "o"){
          billet20 = (retrait/20)
          retrait -= billet20*20
        }
        else {  
          billet20 = saisie4.toInt
          retrait -= billet20*20
        }
      }
      if ((retrait/10)>0){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum "+(retrait/10)+" billet(s) de 10 EUR")
        saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        while (saisie4 !="o" && saisie4.toInt >= (retrait/10)){
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " +(retrait/10)+ " billet(s) de 10 EUR")
          saisie4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        } 
        if (saisie4 == "o"){
          billet10 = (retrait/10)
          retrait -= billet10*10
        }
        else {  
          billet10 = (retrait/10)
          retrait -= billet10*10
        }
      }
      println("Veuillez retirer la somme demandée :")
      if (billet100> 0){
        println(+ billet100 + " billet(s) de 100 EUR")
      }
      if (billet50> 0){
        println(+ billet50 + " billet(s) de 50 EUR")
      }
      if (billet20> 0){
        println(+ billet20 + " billet(s) de 20 EUR")
      }
      if (billet10> 0){
        println(+ billet10 + " billet(s) de 10 EUR")
      }
    }

  printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n" , comptes(clients))
} 
  
  def changementpin(clients : Int, codespin : Array[String]):Unit={
    var pin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    while (pin.size < 8 ){
      println("Votre code pin ne contient pas au moins 8 caractères")
      pin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    }
    codespin(clients) = pin 

  }
  
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var identification = 0
    var solde = 1200.0
    val comptes = Array.fill(nbclients)(solde)
    var PIN = "INTRO1234"
    var PINTEST = ""
    val codespin = Array.fill(nbclients)(PIN)
    var client = 0
    client = readLine("Saisissez votre code identifiant >").toInt
    if (client>(nbclients-1)){
      println("Cet identifiant n’est pas valable.")
      sys.exit()
    }
    PINTEST = readLine("Saisissez votre code pin >")

            var tentatives = 3
            for (i <- 1 to 3){
              if (PINTEST != codespin(client)){
                 if (tentatives-i > 0){
                PINTEST = readLine("Code pin erroné, il vous reste "+ (tentatives-i) +" tentatives >")
                   identification = 0
                }
                 else {
      println("Trop d’erreurs, abandon de l’identification")
      identification = 1
    }
              }}
    while (client<=(nbclients-1)){

    while (identification == 1){
      client = readLine("Saisissez votre code identifiant >").toInt
      if (client>(nbclients-1)){
        println("Cet identifiant n’est pas valable.")
        sys.exit()
      }
      else {
            PINTEST = readLine("Saisissez votre code pin >")
            identification = 0
                  var tentatives = 3
                  for (i <- 1 to 3){
                    if (PINTEST != codespin(client)){
                       if (tentatives-i > 0){
                      PINTEST = readLine("Code pin erroné, il vous reste "+ (tentatives-i) +" tentatives >")
                         identification = 0
                      }
                       else {
            println("Trop d’erreurs, abandon de l’identification")
            identification = 1
          }
                    }}
    }
    }
    println("Choisissez votre opération :")
    println("  1) Dépôt")
    println("  2) Retrait")
    println("  3) Consultation du compte")
    println("  4) Changement du code pin")
    println("  5) Terminer")

    var choix = readLine("Votre choix : ").toInt
    

     if (choix == 3){
       identification = 0
       consultation(client, comptes)

          }

         else if (choix == 1){
            identification = 0
            depot(client, comptes)
          }
          else if (choix == 2){
            identification = 0
            retrait(client, comptes)
          }
          else if (choix == 4){
            identification = 0
            changementpin(client, codespin)
          }

      if (choix == 5){
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        identification = 1
      }
    }
    
  }
  }

