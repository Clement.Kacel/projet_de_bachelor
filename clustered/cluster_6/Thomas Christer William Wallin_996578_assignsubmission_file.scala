//Assignment: Thomas Christer William Wallin_996578_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine

object Main{
def changepin(id : Int, codespin : Array[String]) : Unit = {

    var nouveaupin = ""
    while (8 > nouveaupin.length) { println("Entrez votre nouveau code PIN (il doit contenir au moins 8 caractères")
      nouveaupin = readLine()
      if (nouveaupin.length < 8) { println("Votre code PIN ne contient pas au moins 8 caractères")
      }

                                    }

    codespin(id) = nouveaupin
  }
def retrait(id : Int, accounts : Array[Double]) : Unit = {
      var selectionTicket_INT500 = 0
      var selectionTicket_INT200 = 0
      var selectionTicket_INT100 = 0
      var selectionTicket_INT50 = 0
      var selectionTicket_INT20 = 0
      var selectionTicket_INT10 = 0
      var retrait_devise = 3

      while (retrait_devise != 1 && retrait_devise != 2) {
        println("Indiquez la devise : 1) CHF; 2) EUR >")

        retrait_devise = readLine().toInt

        if(retrait_devise != 1 && retrait_devise != 2 ){

          println("invalide")

        }
      }

      var logique = false
      var v = 0;
      val limitaccounts = accounts(id) * 0.1

      while (!logique){
        println("Indiquez le montant du retrait. >")
        v = readInt()

        if(v % 10 == 0 ){


          if(v <= limitaccounts && v>0){
            logique = true
          }else{
            println("Votre limite de retrait autorisée est :" + limitaccounts)
          }

        }else{
          println("Le montant doit être un multiple de 10.")
        }

      }

      var coupure = 3

      if(retrait_devise == 1){

        if(v >= 200){

          while (coupure != 1 && coupure != 2) {
            println("En 1) grosses coupures, 2) petites coupures >")
            coupure = readInt()
          }
        }
      }

      println("Veuillez retirer le montant demandé:")

      var modulo = v


      if(retrait_devise == 1) {


        if(coupure == 1){

          while (modulo > 0){  
            if(modulo / 500 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/500)
              println(" billet(s) de 500 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                   selectionTicket_INT500 = selectionTicket.toInt
                  if (selectionTicket_INT500 < 0 || selectionTicket_INT500 > (modulo / 500)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT500 * 500)  
                    selectionTicket="o"  
                  }
                }
                else
                {
                  selectionTicket_INT500 = modulo/500
                  modulo -= ((modulo/500) * 500)
                }

              } while (selectionTicket != "o")


            }
            if(modulo / 200 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/200)
              println(" billet(s) de 200 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                   selectionTicket_INT200 = selectionTicket.toInt
                  if (selectionTicket_INT200 < 0 || selectionTicket_INT200 > (modulo / 200)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT200 * 200)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT200 = modulo/200
                  modulo -= ((modulo/200) * 200)
                }

              } while (selectionTicket != "o")

            }
            if(modulo / 100 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/100)
              println(" billet(s) de 100 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                   selectionTicket_INT100 = selectionTicket.toInt
                  if (selectionTicket_INT100 < 0 || selectionTicket_INT100 > (modulo / 100)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT100 * 100)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT100 = modulo/100
                  modulo -= ((modulo/100) * 100)
                }

              } while (selectionTicket != "o")

            }
            if(modulo / 50 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/50)
              println(" billet(s) de 50 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                  selectionTicket_INT50 = selectionTicket.toInt
                  if (selectionTicket_INT50 < 0 || selectionTicket_INT50 > (modulo / 50)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT50 * 50)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT50 = modulo/50
                  modulo -= ((modulo/50) * 50)
                }

              } while (selectionTicket != "o")


            }
            if(modulo / 20 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/20)
              println(" billet(s) de 20 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                  selectionTicket_INT20 = selectionTicket.toInt
                  if (selectionTicket_INT20 < 0 || selectionTicket_INT20 > (modulo / 20)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT20 * 20)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT20 = modulo/20
                  modulo -= ((modulo/20) * 20)
                }

              } while (selectionTicket != "o")

            }
            if(modulo / 10 >= 1) {  
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/10)
              selectionTicket_INT10 = (modulo/10)

              println(" billet(s) de 10 CHF")

              modulo -= ((modulo/10) * 10)


            }


          }



        }
        else{
          while (modulo > 0){

            if(modulo / 100 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/100)
              println(" billet(s) de 100 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                   selectionTicket_INT100 = selectionTicket.toInt
                  if (selectionTicket_INT100 < 0 || selectionTicket_INT100 > (modulo / 100)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT100 * 100)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT100 = modulo/100
                  modulo -= ((modulo/100) * 100)
                }

              } while (selectionTicket != "o")

            }
            if(modulo / 50 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/50)
              println(" billet(s) de 50 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                  selectionTicket_INT50 = selectionTicket.toInt
                  if (selectionTicket_INT50 < 0 || selectionTicket_INT50 > (modulo / 50)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT50 * 50)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT50 = modulo/50
                  modulo -= ((modulo/50) * 50)
                }

              } while (selectionTicket != "o")


            }
            if(modulo / 20 >= 1){
              println(s"Il reste $modulo CHF à distribuer.")
              print("Vous pouvez obtenir au maxomum ")
              print(modulo/20)
              println(" billet(s) de 20 CHF")

              var selectionTicket = ""
              do {
                println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                selectionTicket = readLine()

                if (selectionTicket != "o") {
                  selectionTicket_INT20 = selectionTicket.toInt
                  if (selectionTicket_INT20 < 0 || selectionTicket_INT20 > (modulo / 20)) {
                    println("Veuillez entrer une valeur valide.")
                  } else {
                    modulo -= (selectionTicket_INT20 * 20)
                    selectionTicket="o"
                  }
                }
                else
                {
                  selectionTicket_INT20 = modulo/20
                  modulo -= ((modulo/20) * 20)
                }

              } while (selectionTicket != "o")

            }
            if(modulo / 10 >= 1) {
println(s"Il reste $modulo CHF à distribuer.")
print("Vous pouvez obtenir au maxomum ")
print(modulo/10)
              selectionTicket_INT10 = (modulo/10)

              println(" billet(s) de 10 CHF")

              modulo -= ((modulo/10) * 10)

            }

          }

        }
        println("Veuillez retirer le montant demandé : ")
        if(selectionTicket_INT500>0)
        {
          print(selectionTicket_INT500)
          println(" billet(s) de 500 CHF")
        }
        if(selectionTicket_INT200>0)
        {
          print(selectionTicket_INT200)
          println(" billet(s) de 200 CHF")
        }
        if(selectionTicket_INT100>0)
        {
          print(selectionTicket_INT100)
          println(" billet(s) de 100 CHF")
        }
        if(selectionTicket_INT50>0)
        {
          print(selectionTicket_INT50)
          println(" billet(s) de 50 CHF")
        }
        if(selectionTicket_INT20>0)
        {
          print(selectionTicket_INT20)
          println(" billet(s) de 20 CHF")
        }
        if(selectionTicket_INT10>0)
        {
          print(selectionTicket_INT10)
          println(" billet(s) de 10 CHF")
        }

      }else{

        while (modulo > 0){

          if(modulo / 100 >= 1){
            println(s"Il reste $modulo CHF à distribuer.")
            print("Vous pouvez obtenir au maxomum ")
print(modulo/100)
println(" billet(s) de 100 EUR")

            var selectionTicket = ""
            do {
              println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
              selectionTicket = readLine()

              if (selectionTicket != "o") {
                 selectionTicket_INT100 = selectionTicket.toInt
                if (selectionTicket_INT100 < 0 || selectionTicket_INT100 > (modulo / 100)) {
                  println("Veuillez entrer une valeur valide.")
                } else {
                  modulo -= (selectionTicket_INT100 * 100)
                  selectionTicket="o"
                }
              }
              else
              {
                selectionTicket_INT100 = modulo/100
                modulo -= ((modulo/100) * 100)
              }

            } while (selectionTicket != "o")

          }
          if(modulo / 50 >= 1){
            println(s"Il reste $modulo CHF à distribuer.")
            print("Vous pouvez obtenir au maxomum ")
            print(modulo/50)
            println(" billet(s) de 50 EUR")

            var selectionTicket = ""
            do {
              println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
              selectionTicket = readLine()

              if (selectionTicket != "o") {
                selectionTicket_INT50 = selectionTicket.toInt
                if (selectionTicket_INT50 < 0 || selectionTicket_INT50 > (modulo / 50)) {
                  println("Veuillez entrer une valeur valide.")
                } else {
                  modulo -= (selectionTicket_INT50 * 50)
                  selectionTicket="o"
                }
              }
              else
              {
                selectionTicket_INT50 = modulo/50
                modulo -= ((modulo/50) * 50)
              }

            } while (selectionTicket != "o")


          }
          if(modulo / 20 >= 1){
            println(s"Il reste $modulo CHF à distribuer.")
            print("Vous pouvez obtenir au maxomum ")
            print(modulo/20)
            println(" billet(s) de 20 EUR")

            var selectionTicket = ""
            do {
              println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
              selectionTicket = readLine()

              if (selectionTicket != "o") {
                selectionTicket_INT20 = selectionTicket.toInt
                if (selectionTicket_INT20 < 0 || selectionTicket_INT20 > (modulo / 20)) {
                  println("Veuillez entrer une valeur valide.")
                } else {
                  modulo -= (selectionTicket_INT20 * 20)
                  selectionTicket="o"
                }
              }
              else
              {
                selectionTicket_INT20 = modulo/20
                modulo -= ((modulo/20) * 20)
              }

            } while (selectionTicket != "o")

          }
          if(modulo / 10 >= 1) {
            println(s"Il reste $modulo CHF à distribuer.")
            print("Vous pouvez obtenir au maxomum ")
            print(modulo/10)
            selectionTicket_INT10 = (modulo/10)

            println(" billet(s) de 10 EUR")

            modulo -= ((modulo/10) * 10)

          }

      }
      }


      if(retrait_devise == 1) {

        accounts(id) -= v
      }else{
        println("Veuillez retirer le montant demandé : ")

        if(selectionTicket_INT100>0)
        {
          print(selectionTicket_INT100)
          println(" billet(s) de 100 EUR")
        }
        if(selectionTicket_INT50>0)
        {
          print(selectionTicket_INT50)
          println(" billet(s) de 50 EUR")
        }
        if(selectionTicket_INT20>0)
        {
          print(selectionTicket_INT20)
          println(" billet(s) de 20 EUR")
        }
        if(selectionTicket_INT10>0)
        {
          print(selectionTicket_INT10)
          println(" billet(s) de 10 EUR")
        }

        accounts(id) -= v * 0.95

      }

      printf("Votre retrait a été traité, le nouveau montant disponible sur votre compte est : %.2f CHF\n", accounts(id))
  }


  def depot(id : Int, accounts : Array[Double]) : Unit = {
    var devwith = 3
    while (devwith != 1 && devwith != 2) {
      println("Indiquez la devise du dépôt : 1) CHF; 2) EUR >")
      devwith = readInt()
    }

    var valeur_depot = 0;
    var logique = !true
    while (!logique){ println("Indiquez le montant du dépot >")
      valeur_depot = readInt()
      if(valeur_depot % 10 == 0){ logique = true
      }
    }

    if(devwith == 1) { accounts(id) += valeur_depot
    }else{ accounts(id) += 0.95 * valeur_depot }


    accounts(id) = math.floor(accounts(id) * 100) / 100
    println("Votre dépôt a été traité, le nouveau montant disponible sur votre compte est > " + accounts(id))
  }

   def main(args: Array[String]): Unit = {



    var monchoix = 0
    var tentatiives = 3
    var logique = false
    var bleu = 5
    var id_valide = true
    var accounts = Array.fill(100) (1200.0)
    var codespin = Array.fill(100)("INTRO1234")
    var nbclients = 100
    var id_users = 0

    while (id_valide){

      var logique = false

      while(!logique && id_valide){
        println("Entrez votre login code >")
        id_users = readInt()
        if(id_users >= nbclients){ println("Cette identifiant n'est pas valide.")
          id_valide = false
        }

        if(id_valide){

          tentatiives = 3
          while (tentatiives > 0 && !logique) {  
            println("Entrez votre code pin >")
            var motdepasse = readLine()
            if (motdepasse == codespin(id_users)) {            
             logique = true
            }else { tentatiives -= 1
              if(tentatiives == 0){ println("Trop d'erreurs, abandon de l'identification")
              }else{ println("Code pin erroné, il vous reste " + tentatiives + " tentatives >")
              }


            }


          }

        }


      }


      monchoix = 0



      while (monchoix != bleu && logique) {
        println("Choisissez votre opération : : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Modification du code PIN \n5) Terminer")
        monchoix = readInt()



        if(monchoix == 1 && logique){            
          depot(id_users,accounts)

        }else if(monchoix == 2 && logique){
          retrait(id_users,accounts)

        }else if(monchoix == 5){
println("Fin des opérations, n'oubliez pas de récupérer votre carte.") }else if(monchoix == 3 && logique){
println("Le montant disponible sur votre compte est : " + accounts(id_users))
        }else if(monchoix == 4 && logique){
          changepin(id_users,codespin)
        }


      }


    }

  }


}

