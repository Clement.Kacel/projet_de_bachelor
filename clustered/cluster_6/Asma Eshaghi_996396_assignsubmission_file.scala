//Assignment: Asma Eshaghi_996396_assignsubmission_file

import scala.io.StdIn._
object Main {

  var NB_COMPTE: Int = 100
  var SOLD_INITIAL: Double = 1200.0
  var MDP_INITIAL: String = "INTRO1234"
  var CHANGE_TAUX: Double = 0.95
  var TANTATIVE: Int = 3
  var LOGGED_IN : Boolean = false
  var LOGGED_COMPTE_ID: Int = -1


  var comptes: Array[Double] = Array.fill(NB_COMPTE)(SOLD_INITIAL)
  var codespin: Array[String] = Array.fill(NB_COMPTE)(MDP_INITIAL)

  def displayMenu(){

    println("Choisissez votre opération:")
    println("1. Dépôt")
    println("2. Retrait")
    println("3. Consultation")
    println("4. Changement du code pin")
    println("5. Terminer")
    println("Entrez le numéro de l'opération: ")

  }

  def get_multiple_de_10(message_init:String): Double = {
    //println("quelle est le montant a deposer? ")
    println(message_init)
    var montantdeposer = readDouble()
    while (montantdeposer % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      //println("quelle est le montant a deposer? ")
      println(message_init)
      montantdeposer = readDouble()
    }
    return montantdeposer
  }

  // change EUR TO CHF
  def eur_to_chf(valeur: Double): Double = {
    return valeur * CHANGE_TAUX
  }

  def login() {
    if(!LOGGED_IN) {
      // code identifiant :
      println("Saisissez votre code identifiant >")
      var compte_number = readInt()

      if (compte_number > comptes.length - 1 || compte_number < 0) {
        println("Cet identifiant n’est pas valable.")
      }
      var user_tantative = TANTATIVE

      while (readLine("Saisissez votre code pin > ") != codespin(compte_number) && user_tantative > 1) {
        user_tantative -= 1
        println(s"Code pin erroné, il vous reste " + user_tantative + " tantative>")

      }
      // si le tantative = 0
      if (user_tantative == 1) {
        println("Pour votre protection, les opérations bancaires vont s'interrompre, récupérez votre carte")
        LOGGED_IN = false
      } else {
        LOGGED_COMPTE_ID = compte_number
        LOGGED_IN = true
      }
    }
  }

  def depot(id: Int, comptes_utilisateurs: Array[Double]): Unit = {
    // pin correct
    println("quelle est la devise: 1 pour CHF et 2 pour EUR? ")
    var devise = readInt()
    // CHF
    if (devise == 1) {
      var montantdeposer = get_multiple_de_10("quelle est le montant a deposer? ")
      comptes_utilisateurs(id) += montantdeposer

    }
    // EUR
    if (devise == 2) {
      var montantdeposer = get_multiple_de_10("quelle est le montant a deposer?")
      comptes_utilisateurs(id) += montantdeposer

      // change EUR TO CHF
      montantdeposer = eur_to_chf(montantdeposer)

      comptes_utilisateurs(id) += montantdeposer

      // if fault considérer les valeures négatives : à faire

    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes_utilisateurs(id))

  }

  def consultation(id:Int, comptes: Array[Double]): Unit = {

    printf("la valeur de montantdisponible  : %.2f \n", comptes(id))

  }



  def retrait(id:Int, comptes: Array[Double]): Unit =  {

    var coupure500 = 0;
    var coupure200 = 0;
    var coupure100 = 0;
    var coupure50 = 0;
    var coupure20 = 0;
    var coupure10 = 0;

    println("quelle est la devise: 1 pour CHF et 2 pour EUR? ")
    var devise = readInt()

    while (devise < 1 || devise > 2) {
      println("quelle est la devise: 1 pour CHF et 2 pour EUR? ")
      devise = readInt()
    }
    // CHF
    if (devise == 1) {
      // check if div par 10
      var retirer = true;
      while (retirer) {


        var montantRetirer = get_multiple_de_10("quelle est le montant a  retirer ? ")

        var dixPourcent = 0.1 * comptes(id)

        if (montantRetirer > dixPourcent) {
          printf("Votre plafond de retrait autorisé est de : %.2f CHF\n", dixPourcent)
        } else {
          // check réussi
          if (dixPourcent >= 200) {

            println("choisissez la coupure: 1 grosses coupures, 2 petites coupures")
            var coupures = readInt()

            while (coupures < 1 || coupures > 2) {
              println("choisissez la coupure: 1 grosses coupures, 2 petites coupures")
              coupures = readInt()
            }
            // grosses coupures
            if (coupures == 1) {

              if (montantRetirer >= 500) {

                coupure500 = (montantRetirer / 500).toInt;

                println("Il reste " + montantRetirer + " CHF à distribuer")

                println("Vous pouvez obtenir au maximum " + coupure500 + " billet(s) de 500 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure500 * 500)
                  montantRetirer = montantRetirer - (coupure500 * 500)
                } else {
                  if (ok.toInt < coupure500 && ok.toInt > 0) {
                    coupure500 = ok.toInt
                    comptes(id) = comptes(id) - (coupure500 * 500)
                    montantRetirer = montantRetirer - (coupure500 * 500)

                  } else {
                    coupure500 = 0
                  }

                }
              }
              if (montantRetirer >= 200) {
                coupure200 = (montantRetirer / 200).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure200 + " billet(s) de 200 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id)  - (coupure200 * 200)
                  montantRetirer = montantRetirer - (coupure200 * 200)
                } else {

                  if (ok.toInt < coupure200 && ok.toInt > 0) {
                    coupure200 = ok.toInt
                    comptes(id) = comptes(id) - (coupure200 * 200)
                    montantRetirer = montantRetirer - (coupure200 * 200)
                  } else {
                    coupure200 = 0
                  }
                }
              }

              if (montantRetirer >= 100) {
                coupure100 = (montantRetirer / 100).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure100 * 100)
                  montantRetirer = montantRetirer - (coupure100 * 100)
                } else {
                  if (ok.toInt < coupure100 && ok.toInt > 0) {
                    coupure100 = ok.toInt
                    comptes(id) = comptes(id) - (coupure100 * 100)
                    montantRetirer = montantRetirer - (coupure100 * 100)
                  } else {
                    coupure100 = 0
                  }
                }
              }

              if (montantRetirer >= 50) {
                coupure50 = (montantRetirer / 50).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure50 * 50)
                  montantRetirer = montantRetirer - (coupure50 * 50)
                } else {
                  if (ok.toInt < coupure50 && ok.toInt > 0) {
                    coupure50 = ok.toInt
                    comptes(id) = comptes(id) - (coupure50 * 50)
                    montantRetirer = montantRetirer - (coupure50 * 50)
                  } else {
                    coupure50 = 0
                  }
                }
              }

              if (montantRetirer >= 20) {
                coupure20 = (montantRetirer / 20).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure20 * 20)
                  montantRetirer = montantRetirer - (coupure20 * 20)
                } else {
                  if (ok.toInt < coupure20 && ok.toInt > 0) {
                    coupure20 = ok.toInt
                    comptes(id) = comptes(id) - (coupure20 * 20)
                    montantRetirer = montantRetirer - (coupure20 * 20)
                  } else {
                    coupure20 = 0
                  }
                }
              }

              if (montantRetirer >= 10) {
                coupure10 = (montantRetirer / 10).toInt;
                comptes(id) = comptes(id)- (coupure10 * 10)
                montantRetirer = montantRetirer - (coupure10 * 10)
              }
            }
            // petites coupures
            if (coupures == 2) {
              if (montantRetirer >= 100) {
                coupure100 = (montantRetirer / 100).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure100 * 100)
                  montantRetirer = montantRetirer - (coupure100 * 100)
                } else {
                  if (ok.toInt < coupure100 && ok.toInt > 0) {
                    coupure100 = ok.toInt
                    comptes(id) = comptes(id) - (coupure100 * 100)
                    montantRetirer = montantRetirer - (coupure100 * 100)
                  } else {
                    coupure100 = 0
                  }
                }
              }

              if (montantRetirer >= 50) {
                coupure50 = (montantRetirer / 50).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure50 * 50)
                  montantRetirer = montantRetirer - (coupure50 * 50)
                } else {
                  if (ok.toInt < coupure50 && ok.toInt > 0) {
                    coupure50 = ok.toInt
                    comptes(id) = comptes(id) - (coupure50 * 50)
                    montantRetirer = montantRetirer - (coupure50 * 50)
                  } else {
                    coupure50 = 0
                  }
                }
              }

              if (montantRetirer >= 20) {
                coupure20 = (montantRetirer / 20).toInt;
                println("Il reste " + montantRetirer + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var ok = readLine()

                if (ok == "o") {
                  comptes(id) = comptes(id) - (coupure20 * 20)
                  montantRetirer = montantRetirer - (coupure20 * 20)
                } else {
                  if (ok.toInt < coupure20 && ok.toInt > 0) {
                    coupure20 = ok.toInt
                    comptes(id) = comptes(id) - (coupure20 * 20)
                    montantRetirer = montantRetirer - (coupure20 * 20)
                  } else {
                    coupure20 = 0
                  }
                }
              }

              if (montantRetirer >= 10) {
                coupure10 = (montantRetirer / 10).toInt;
                comptes(id) = comptes(id)- (coupure10 * 10)
                montantRetirer = montantRetirer - (coupure10 * 10)
              }

            }


          }
          // petit coupure par défault
          else {
            if (montantRetirer >= 100) {
              coupure100 = (montantRetirer / 100).toInt;
              println("Il reste " + montantRetirer + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var ok = readLine()

              if (ok == "o") {
                comptes(id) = comptes(id) - (coupure100 * 100)
                montantRetirer = montantRetirer - (coupure100 * 100)
              } else {
                if (ok.toInt < coupure100 && ok.toInt > 0) {
                  coupure100 = ok.toInt
                  comptes(id) = comptes(id) - (coupure100 * 100)
                  montantRetirer = montantRetirer - (coupure100 * 100)
                } else {
                  coupure100 = 0
                }
              }
            }

            if (montantRetirer >= 50) {
              coupure50 = (montantRetirer / 50).toInt;
              println("Il reste " + montantRetirer + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var ok = readLine()

              if (ok == "o") {
                comptes(id) = comptes(id) - (coupure50 * 50)
                montantRetirer = montantRetirer - (coupure50 * 50)
              } else {
                if (ok.toInt < coupure50 && ok.toInt > 0) {
                  coupure50 = ok.toInt
                  comptes(id) = comptes(id) - (coupure50 * 50)
                  montantRetirer = montantRetirer - (coupure50 * 50)
                } else {
                  coupure50 = 0
                }
              }
            }

            if (montantRetirer >= 20) {
              coupure20 = (montantRetirer / 20).toInt;
              println("Il reste " + montantRetirer + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var ok = readLine()

              if (ok == "o") {
                comptes(id) = comptes(id) - (coupure20 * 20)
                montantRetirer = montantRetirer - (coupure20 * 20)
              } else {
                if (ok.toInt < coupure20 && ok.toInt > 0) {
                  coupure20 = ok.toInt
                  comptes(id) = comptes(id) - (coupure20 * 20)
                  montantRetirer = montantRetirer - (coupure20 * 20)
                } else {
                  coupure20 = 0
                }
              }
            }

            if (montantRetirer >= 10) {
              coupure10 = (montantRetirer / 10).toInt;
              comptes(id) = comptes(id) - (coupure10 * 10)
              montantRetirer = montantRetirer - (coupure10 * 10)
            }
          }

          println("Veuillez retirer la somme demandée :")
          if (coupure500 > 0) {
            println(coupure500 + " billet (s) de 500 CHF")
          }

          if (coupure200 > 0) {
            println(coupure200 + " billet (s) de 200 CHF")
          }

          if (coupure100 > 0) {
            println(coupure100 + " billet (s) de 100 CHF")
          }

          if (coupure50 > 0) {
            println(coupure50 + " billet (s) de 50 CHF")
          }

          if (coupure20 > 0) {
            println(coupure20 + " billet (s) de 20 CHF")
          }
          if (coupure10 > 0) {
            println(coupure10 + " billet (s) de 10 CHF")
          }

          retirer = false
        }
      }
    }
    // EUR : pas de choix de coupure
    else if (devise == 2) {

      var retirer = true;
      while (retirer) {

        println("quelle est le montant a  retirer ? ")
        var montantRetirer = readDouble()

        var dixPourcent = (0.1 * comptes(id))

        var dixPourcentEUR = (dixPourcent / 0.95) // 10 compte en EUR

        dixPourcentEUR = ((dixPourcentEUR) - (dixPourcentEUR % 10)).toDouble // remove le reste qui est inférieur à 10

        if (montantRetirer % 10 != 0) {
          println("Le montant doit être un multiple de 10")
        } else if (montantRetirer > dixPourcentEUR) {
          printf("Votre plafond de retrait autorisé est de : %.2f  EUR\n", dixPourcentEUR)
        } else {

          // all checks passed
          if (montantRetirer >= 100) {

            coupure100 = (montantRetirer / 100).toInt;
            println("Il reste " + montantRetirer + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + coupure100 + " billet(s) de 100 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var ok = readLine()

            if (ok == "o") {
              comptes(id) = comptes(id) - (coupure100 * 100) * 0.95
              montantRetirer = montantRetirer - (coupure100 * 100)
            } else {
              if (ok.toInt < coupure100 && ok.toInt > 0) {
                coupure100 = ok.toInt
                comptes(id) = comptes(id) - (coupure100 * 100) * 0.95
                montantRetirer = montantRetirer - (coupure100 * 100)
              } else {
                coupure100 = 0
              }
            }
          }

          if (montantRetirer >= 50) {
            coupure50 = (montantRetirer / 50).toInt;
            println("Il reste " + montantRetirer + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + coupure50 + " billet(s) de 50 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var ok = readLine()

            if (ok == "o") {
              comptes(id) = comptes(id) - (coupure50 * 50)
              montantRetirer = montantRetirer - (coupure50 * 50)
            } else {
              if (ok.toInt < coupure50 && ok.toInt > 0) {
                coupure50 = ok.toInt
                comptes(id) = comptes(id) - (coupure50 * 50)
                montantRetirer = montantRetirer - (coupure50 * 50)
              } else {
                coupure50 = 0
              }
            }
          }

          if (montantRetirer >= 20) {
            coupure20 = (montantRetirer / 20).toInt;
            println("Il reste " + montantRetirer + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + coupure20 + " billet(s) de 20 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var ok = readLine()

            if (ok == "o") {
              comptes(id) = comptes(id) - (coupure20 * 20)
              montantRetirer = montantRetirer - (coupure20 * 20)
            } else {
              if (ok.toInt < coupure20 && ok.toInt > 0) {
                coupure20 = ok.toInt
                comptes(id) = comptes(id) - (coupure20 * 20)
                montantRetirer = montantRetirer - (coupure20 * 20)
              } else {
                coupure20 = 0
              }
            }
          }

          if (montantRetirer >= 10) {
            coupure10 = (montantRetirer / 10).toInt;
            comptes(id) = comptes(id) - (coupure10 * 10)
            montantRetirer = montantRetirer - (coupure10 * 10)
          }


          // affiche les billet

          println("Veuillez retirer la somme demandée :")

          if (coupure100 > 0) {
            println(coupure100 + " billet (s) de 100 EUR")
          }

          if (coupure50 > 0) {
            println(coupure50 + " billet (s) de 50 EUR")
          }

          if (coupure20 > 0) {
            println(coupure20 + " billet (s) de 20 EUR")
          }
          if (coupure10 > 0) {
            println(coupure10 + " billet (s) de 10 EUR")
          }
          retirer = false
        }


      }

    }
  }


  def changepin(id: Int, codespin:Array[String]): Unit = {
    println("saisir le nouveau codepin > ")
    var nouveau_code = readLine()

    // check avec 8 character
    while(nouveau_code.length < 8){
      println("le codepin doit-etre au moins 8 character")
      println("saisir le nouveau codepin > ")
      nouveau_code = readLine()
    }
    // log out
    LOGGED_IN = false
    LOGGED_COMPTE_ID = -1
    codespin(id) = nouveau_code
  }
  def main(args: Array[String]): Unit = {

    var continuer = true;
    while (continuer) {
      displayMenu()
      var choix = readInt()
      if(choix == 1){
        login()
        depot(LOGGED_COMPTE_ID, comptes)
      }
      if (choix == 2){
        login()
        retrait(LOGGED_COMPTE_ID, comptes)
      }
      if(choix == 3){
        login()
        consultation(LOGGED_COMPTE_ID, comptes)

      }
      if(choix == 4){
        login()
        changepin(LOGGED_COMPTE_ID, codespin)
      }
      if(choix == 5){
        println("Fin des opérations.n'oubliez pas de récupérer votre carte.")
        continuer = false
      }

    }
  }
}