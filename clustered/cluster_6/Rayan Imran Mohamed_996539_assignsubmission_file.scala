//Assignment: Rayan Imran Mohamed_996539_assignsubmission_file

import scala.io.StdIn._
object Main {

  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")
   var nbessaipin = 1 
    var dépot = 1
    var retrait = 2
    var consultation = 3
    var terminer = 4
    val pin = "INTRO1234"
    var pincorrect = false
    val chgEurToChf = 0.95
    val chgChfToEur = 1.05
    var choix = 0
    var choix2 = 0 
    var identifiant = 0


  //DEPOT
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise du dépot: 1) CHF 2) EUR > ")
      var choixdevise = readInt()

      if (choixdevise == 1){
        println("Indiquez le montant du dépôt")

        var montant = readInt()

        if (montant%10 != 0){
          println("Le montant de être multiple de 10")
          montant = readInt()
        } 
        else{
          comptes(id) += montant
          printf("Votre dépot a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
        }
      }
      if(choixdevise == 2){
        println("Indiquez le montant du dépôt")

        var montant = readInt()

        if (montant%10 != 0){
          println("Le montant de être multiple de 10")
          montant = readInt()
        } 
        else{
          comptes(id) += montant*chgEurToChf
          printf("Votre dépot a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF", comptes(id))
        }


      }
      
  }

  
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise : 1) CHF, 2) EUR > ")
    var devise = readInt()
    var nb500 = 0
    var nb200 = 0
    var nb100 = 0
    var nb50 = 0
    var nb20 = 0
    var nb10 = 0

    var choixcoupure = 0 

    var montantretraitCHF = readLine("Indiquez le montant du retrait > ").toInt
    var reste = montantretraitCHF

      if (montantretraitCHF <200){ 
        var choix = ""
        if(reste >= 100){
              nb100 = (reste / 100.0).toInt
              printf("Vous pouvez retirer %d billets de 100 CHF \n", nb100)
              print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
              choix = readLine()

          while(!(choix == "o" || choix.toInt < nb100)){
             choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
           }

              if(choix == "o"){
                reste -= (nb100*100)
                nb100 = nb100

              }
              else {
                reste -= choix.toInt*100
              nb100 = choix.toInt
              }
              choix = ""

            }
           if(reste >= 50){
              nb50 = (reste / 50.0).toInt
              printf("Vous pouvez retirer %d billets de 50 CHF \n", nb50)
              print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
              choix = readLine()

             while(!(choix == "o" || choix.toInt < nb50)){
                choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
              }

              if(choix == "o"){
                reste -= (nb50*50)
                nb50 = nb50

              }
              else {
                reste -= choix.toInt*50
                nb50 = choix.toInt

              }
              choix = ""

            }
           if(reste >= 20){
              nb20 = (reste / 20.0).toInt
              printf("Vous pouvez retirer %d billets de 20 CHF \n", nb20)
              print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
              choix = readLine()

             while(!(choix == "o" || choix.toInt < nb20)){
                choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
              }

              if(choix == "o"){
                reste -= (nb20*20)
                nb20 = nb20

              }
              else {
                reste -= choix.toInt*20
                nb20 = choix.toInt

              }
              choix = ""

            }
           if(reste >= 10){
              nb10 = (reste / 10.0).toInt

                reste -= (nb10*10)

              }
              println("Veuillez retirer la somme demandée : ")
              if (nb100 > 0) println(s" $nb100 billets de 100 CHF")
              if (nb50 > 0) println(s" $nb50 billets de 50 CHF")
              if (nb20 > 0) println(s" $nb20 billets de 20 CHF")
              if (nb10 > 0) println(s" $nb10 billets de 10 CHF")

        comptes(id)-= montantretraitCHF
               printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF \n", comptes(id)) 


            
      }
    
        if (devise == 1){
          

          while ((montantretraitCHF%10 != 0) || (montantretraitCHF > 0.1*comptes(id))){
            if(montantretraitCHF%10 != 0){
              println("Le montant doit être multiple de 10")
              montantretraitCHF = readInt()
            }
            else if(montantretraitCHF > 0.1*comptes(id)){
              montantretraitCHF = readLine("Votre plafond de retrait autorisé est de : %.2f CHF \n ", 0.1*comptes(id)).toInt
            }

          }

                 
        
          
                 
          
           if(montantretraitCHF >= 200){
            while ((choixcoupure != 1) && (choixcoupure != 2)){
              println("En 1) grosses coupures, 2) petites coupures: ")
              choixcoupure = readInt()
            }
           
             

          //Distribution des billets



           if(choixcoupure == 1){ 

            var choix = ""

             while(reste > 0){
               printf("Il reste %d CHF à distribuer \n", reste)
               if(reste >= 500){
                 nb500 = (reste / 500.0).toInt
                 printf("Vous pouvez retirer %d billets de 500 CHF \n", nb500)

                 print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                 choix = readLine()

                 while(!(choix == "o" || choix.toInt < nb500)){
                   choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                 }

                 if(choix == "o"){
                   reste -= (nb500*500)
                   nb500 = nb500
                 }
                 else {
                   reste -= choix.toInt*500
                   nb500 = choix.toInt
                 }
                 choix = ""

               }
               if(reste >= 200){
                  nb200 = (reste / 200.0).toInt
                  printf("Vous pouvez retirer %d billets de 200 CHF \n", nb200)
                  print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  choix = readLine()

                 while(!(choix == "o" || choix.toInt < nb200)){
                    choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  }

                  if(choix == "o"){
                    reste -= (nb200*200)
                 nb200 = nb200
                  }

                  else {
                    reste -= choix.toInt*200
                  nb200 = choix.toInt
                  }
                  choix = ""

                }
               if(reste >= 100){
                  nb100 = (reste / 100.0).toInt
                  printf("Vous pouvez retirer %d billets de 100 CHF \n", nb100)
                  print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  choix = readLine()

                 while(!(choix == "o" || choix.toInt < nb100)){
                    choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  }

                  if(choix == "o"){
                    reste -= (nb100*100)
                    nb100 = nb100
                  }
                  else {
                    reste -= choix.toInt*100
                    nb100 = choix.toInt
                  }

                  choix = ""

                }
               if(reste >= 50){
                  nb50 = (reste / 50.0).toInt
                  printf("Vous pouvez retirer %d billets de 50 CHF \n", nb50)
                  print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  choix = readLine()

                 while(!(choix == "o" || choix.toInt < nb50)){
                    choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  }

                  if(choix == "o"){
                    reste -= (nb50*50)
                    nb50 = nb50

                  }
                  else {
                    reste -= choix.toInt*50
                    nb50 = choix.toInt
                  }

                  choix = ""

                }
               if(reste >= 20){
                  nb20 = (reste / 20.0).toInt
                  printf("Vous pouvez retirer %d billets de 20 CHF \n", nb20)
                  print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  choix = readLine()

                 while(!(choix == "o" || choix.toInt < nb20)){
                    choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                  }

                  if(choix == "o"){
                    reste -= (nb20*20)
                    nb20 = nb20

                  }
                  else {
                    reste -= choix.toInt*20
                  nb20 = choix.toInt
                  }
                  choix = ""

                }
               if(reste >= 10){
                  nb10 = (reste / 10.0).toInt


                    reste -= (nb10*10)



                }
               println("Veuillez retirer la somme demandée : ")
                if (nb500 > 0) println(s" $nb500 billets de 500 CHF") 
                if (nb200 > 0) println(s" $nb200 billets de 200 CHF")
                if (nb100 > 0) println(s" $nb100 billets de 100 CHF")
                if (nb50 > 0) println(s" $nb50 billets de 50 CHF")
                if (nb20 > 0) println(s" $nb20 billets de 20 CHF")
                if (nb10 > 0) println(s" $nb10 billets de 10 CHF")

               comptes(id)-= montantretraitCHF
                 printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF \n", comptes(id)) 


               choix2 = readLine("\n Choisissez votre opération:" + "\n" + "1) Dépot" + "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n" + "5) Terminer" + "\n" + "Votre choix : ").toInt
               if (choix2 == 4){
                 println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
               }

             

            } 
            if(choixcoupure == 2){ 

            var choix = ""
              if(reste >= 100){
                    nb100 = (reste / 100.0).toInt
                    printf("Vous pouvez retirer %d billets de 100 CHF \n", nb100)
                    print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                    choix = readLine()

                while(!(choix == "o" || choix.toInt < nb100)){
                   choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                 }

                    if(choix == "o"){
                      reste -= (nb100*100)
                      nb100 = nb100

                    }
                    else {
                      reste -= choix.toInt*100
                    nb100 = choix.toInt
                    }
                    choix = ""

                  }
                 if(reste >= 50){
                    nb50 = (reste / 50.0).toInt
                    printf("Vous pouvez retirer %d billets de 50 CHF \n", nb50)
                    print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                    choix = readLine()

                   while(!(choix == "o" || choix.toInt < nb50)){
                      choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                    }

                    if(choix == "o"){
                      reste -= (nb50*50)
                      nb50 = nb50

                    }
                    else {
                      reste -= choix.toInt*50
                      nb50 = choix.toInt

                    }
                    choix = ""

                  }
                 if(reste >= 20){
                    nb20 = (reste / 20.0).toInt
                    printf("Vous pouvez retirer %d billets de 20 CHF \n", nb20)
                    print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                    choix = readLine()

                   while(!(choix == "o" || choix.toInt < nb20)){
                      choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                    }

                    if(choix == "o"){
                      reste -= (nb20*20)
                      nb20 = nb20

                    }
                    else {
                      reste -= choix.toInt*20
                      nb20 = choix.toInt

                    }
                    choix = ""

                  }
                 if(reste >= 10){
                    nb10 = (reste / 10.0).toInt

                      reste -= (nb10*10)

                    }
                    println("Veuillez retirer la somme demandée : ")
                    if (nb100 > 0) println(s" $nb100 billets de 100 CHF")
                    if (nb50 > 0) println(s" $nb50 billets de 50 CHF")
                    if (nb20 > 0) println(s" $nb20 billets de 20 CHF")
                    if (nb10 > 0) println(s" $nb10 billets de 10 CHF")

              comptes(id)-= montantretraitCHF
                     printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF \n", comptes(id)) 


                  

            }
               }
              }   
                 
                 
                 
                 }

        if (devise == 2){

          var montantretraitEUR = readLine("Indiquez le montant du retrait > ").toInt

          var reste = montantretraitEUR
          var nb100 = 0
          var nb50 = 0
          var nb20 = 0
          var nb10 = 0


          while ((montantretraitEUR%10 != 0) || (montantretraitEUR > 0.1*comptes(id))){
            if(montantretraitEUR%10 != 0){
              println("Le montant doit être multiple de 10")
              montantretraitEUR = readInt()
            }
            else if(montantretraitEUR > 0.1*comptes(id)){
              montantretraitEUR = readLine("Votre plafond de retrait autorisé est de : %.2f EUR \n ", 0.1*comptes(id)).toInt
            }

          }


          var choix = ""
          if(reste >= 100){
                nb100 = (reste / 100.0).toInt
                printf("Vous pouvez retirer %d billets de 100 EUR \n", nb100)
                print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                choix = readLine()

            while(!(choix == "o" || choix.toInt < nb100)){
               choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
             }

                if(choix == "o"){
                  reste -= (nb100*100)
                  nb100 = nb100
                }
                else{
                  reste -= choix.toInt*100
                  nb100 = choix.toInt
                }
                choix = ""

              }
             if(reste >= 50){
                nb50 = (reste / 50.0).toInt
                printf("Vous pouvez retirer %d billets de 50 EUR \n", nb50)
                print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                choix = readLine()

               while(!(choix == "o" || choix.toInt < nb50)){
                  choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                }

                if(choix == "o"){
                  reste -= (nb50*50)
                  nb50 = nb50
                }
                else{
                  reste -= choix.toInt*50
                  nb50 = choix.toInt
                }
                choix = ""

              }
             if(reste >= 20){
                nb20 = (reste / 20.0).toInt
                printf("Vous pouvez retirer %d billets de 20 EUR \n", nb20)
                print("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                choix = readLine()

               while(!(choix == "o" || choix.toInt < nb20)){
                  choix = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée > ")
                }

                if(choix == "o"){
                  reste -= (nb20*20)
                nb20 = nb20
                }
                else {
                  reste -= choix.toInt*20
                  nb20 = choix.toInt
                }
                choix = ""

              }
             if(reste >= 10){
                nb10 = (reste / 10.0).toInt

                  reste -= (nb10*10)
                }
              println("Veuillez retirer la somme demandée : ")        
         if (nb100 > 0) println(s" $nb100 billets de 100 EUR")
         if (nb50 > 0) println(s" $nb50 billets de 50 EUR")
         if (nb20 > 0) println(s" $nb20 billets de 20 EUR")
         if (nb10 > 0) println(s" $nb10 billets de 10 EUR")

          comptes(id)-= montantretraitEUR*0.95
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f CHF \n", comptes(id)) 


          



    }

    
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {

    codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString

    if (codespin(id).size >= 8){
      println("Votre nouveau code pin à bien été actualisé.")
    } else {
    while(codespin(id).size < 8){
  println("Votre code pin ne contient pas au moins 8 caractères")
  codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString

  }  
    }
   }



  
  def main(args: Array[String]): Unit = {
    
    identifiant = readLine("Saisissez votre code identifiant > ").toInt
    
    if (identifiant > 99){
      println("Cet identifiant n'est pas valable.")
      System.exit(0)
    }
    var boucle = 0
    while(boucle == 0){
      if (identifiant > 99){
        println("Cet identifiant n'est pas valable.")
        System.exit(0)
      }
      println("Saisissez votre code pin")

      while ((nbessaipin <= 3) &&(pincorrect == false)){
        var choixpin = readLine()

        if (codespin(identifiant) != choixpin){
          println(" code pin erroné, il vous reste " + (3 - nbessaipin) + " tentatives > ")
        }
        else{
          pincorrect = true
        }
        nbessaipin += 1
      }
      if (choix2 == 4) {
        println ("Fin des opérations, n'oubliez pas de récupérer votre carte.")
      }
      if ((nbessaipin == 4) && (pincorrect == false)){
        println("Pour votre protection, vos opérations bancaires vont s'interromprent, récupérez votre carte.")
        identifiant = readLine("Saisissez votre code identifiant > ").toInt
      }
    
      choix2 = readLine("\n Choisissez votre opération:" + "\n" + "1) Dépot" + "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n" + "5) Terminer" + "\n" + "Votre choix : ").toInt

    while (!(choix2 == 5)) {
      if ((choix2 == 1) && (pincorrect == true)){

        depot(identifiant, comptes)

        choix2 = readLine("\n Choisissez votre opération:" + "\n" + "1) Dépot" + "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n" + "5) Terminer" + "\n" + "Votre choix : ").toInt
      }

      else if ((choix2 == 2) && (pincorrect == true)){
        retrait(identifiant, comptes)

        choix2 = readLine("\n Choisissez votre opération:" + "\n" + "1) Dépot" + "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n" + "5) Terminer" + "\n" + "Votre choix : ").toInt
        
      }
      
      else if((choix2 == 3) && (pincorrect == true)){
      printf("Le montant disponible sur votre compte est de : %.2f CHF \n", comptes(identifiant))

      choix2 = readLine("\n Choisissez votre opération:" + "\n" + "1) Dépot" + "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n" + "5) Terminer" + "\n" + "Votre choix : ").toInt
      }
       else if((choix2 == 4) && (pincorrect == true)){

         changepin(identifiant, codespin)
         choix2 = readLine("\n Choisissez votre opération:" + "\n" + "1) Dépot" + "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n" + "5) Terminer" + "\n" + "Votre choix : ").toInt
       }


        
  }

    if (choix2 == 5){
      println("Fin des opérations, veuillez récupérer votre carte.")
      nbessaipin = 1
      pincorrect = false
      identifiant = readLine("Saisissez votre code identifiant > ").toInt
    } 

    }
    
  }

                 
                 
                 
                 

                 
                 
                 
}
