//Assignment: Timothée Sauvageot_996575_assignsubmission_file

import scala.io.StdIn._

object Main {  

    // début de l'opération 1
    var devise_depot = 0
    var montant_depot = 0
    
    def depot(id : Int, comptes : Array[Double]) : Unit = {  
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        devise_depot = readInt()
   
      // dépôt CHF 
      if (devise_depot == 1) { 
        println("Indiquez le montant du dépôt >") 
        montant_depot = readInt() 
      while (!((montant_depot >= 10) && (montant_depot % 10 == 0)))  {
        println("Le montant doit être un multiple de 10.\nIndiquez le montant du dépôt >") 
        montant_depot = readInt() }
        comptes(id) = comptes(id) + montant_depot
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      }
  
      // dépôt EUR                        
      if (devise_depot == 2) { 
        println("Indiquez le montant du dépôt >")
        montant_depot = readInt() 
      while (!((montant_depot >= 10) && (montant_depot % 10 == 0)))  { 
        println("Le montant doit être un multiple de 10.\nIndiquez le montant du dépôt >") 
        montant_depot = readInt() }
        comptes(id) = comptes(id) + (montant_depot * 0.95)
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      } 
    } // fin de l'opération 1

    // début de l'opération 2
    var devise_retrait = 0
    var montant_retrait = 0
    var montant_plafond = 0.0
    var choix_coupures = 0
    var choix_client = ""
    var nbrCoupures500 = 0
    var nbrCoupures200 = 0
    var nbrCoupures100 = 0
    var nbrCoupures50 = 0
    var nbrCoupures20 = 0
    var nbrCoupures10 = 0
    var nbrCoupures100E = 0
    var nbrCoupures50E = 0
    var nbrCoupures20E = 0
    var nbrCoupures10E = 0
    var nbr_billets_client500 = 0
    var nbr_billets_client200 = 0
    var nbr_billets_client100 = 0
    var nbr_billets_client50 = 0
    var nbr_billets_client20 = 0
    var nbr_billets_client10 = 0
    var nbr_billets_client100E = 0
    var nbr_billets_client50E = 0
    var nbr_billets_client20E = 0
    var nbr_billets_client10E = 0
    var montant_retrait_initial = 0
    
    def retrait(id : Int, comptes : Array[Double]) : Unit = { 
        println("Indiquez la devise :1 CHF, 2 : EUR >") 
        devise_retrait = readInt() 
      while (!(devise_retrait == 1) && !(devise_retrait == 2)) { 
        println("Vous devez choisir 1 ou 2.\nIndiquez la devise :1 CHF, 2 : EUR >")
        devise_retrait = readInt() }                       
        println("Indiquez le montant du retrait >")
        montant_retrait = readInt()
      if (devise_retrait==1) montant_plafond = comptes(id) * 0.1  
      if (devise_retrait==2) montant_plafond = comptes(id) * 0.1 * 1.05
      while (!(montant_retrait >= 0) || (!(montant_retrait % 10 == 0)) || (!(montant_retrait <= montant_plafond)) ) { 
      if (!(montant_retrait >= 0) || (!(montant_retrait % 10 == 0))){println("Le montant doit être un multiple de 10.")}                                                
      if ((montant_retrait > montant_plafond) && (devise_retrait == 1)) { 
        printf("Votre plafond de retrait autorisé est de : %.2f CHF\n", montant_plafond) }
      if ((montant_retrait > montant_plafond) && (devise_retrait == 2)) { 
        printf("Votre plafond de retrait autorisé est de : %.2f EUR\n", montant_plafond) }
        println("Indiquez le montant du retrait >")                         
        montant_retrait = readInt() 
      }
        montant_retrait_initial = montant_retrait

      // retrait CHF
      if (devise_retrait == 1) {
      if (montant_retrait_initial >= 200) { 
      while (!(choix_coupures == 1) && !(choix_coupures == 2)) { 
        println("Vous devez choisir entre 1) grosses coupures, 2) petites coupures >")
        choix_coupures = readInt()
      }
      } else { 
        choix_coupures = 2
      }

      // choix grosses coupures  
      if (choix_coupures == 1) {
      nbrCoupures500 = montant_retrait / 500
      if (montant_retrait >= 500) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures500 + " billet(s) de 500 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }

      if (choix_client != "o") { 
        nbr_billets_client500 = choix_client.toInt 
      }
      while ((nbrCoupures500 <= nbr_billets_client500) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client500 = choix_client.toInt 
      }
      }

      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures500 * 500)
        nbr_billets_client500 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client500 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client500 * 500)
        nbrCoupures500 = 0
      }

        nbrCoupures200 = montant_retrait / 200
      if (montant_retrait >= 200) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures200 + " billet(s) de 200 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }

      if (choix_client != "o") { 
        nbr_billets_client200 = choix_client.toInt 
      }
      while ((nbrCoupures200 <= nbr_billets_client200) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client200 = choix_client.toInt 
      }
      }

      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures200 * 200)
        nbr_billets_client200 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client200 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client200 * 200)
        nbrCoupures200 = 0
      }

        nbrCoupures100 = montant_retrait / 100
      if (montant_retrait >= 100) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures100 + " billet(s) de 100 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }

      if (choix_client != "o") { 
        nbr_billets_client100 = choix_client.toInt 
      }
      while ((nbrCoupures100 <= nbr_billets_client100) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client100 = choix_client.toInt 
      }
      }

      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures100 * 100)
        nbr_billets_client100 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client100 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client100 * 100)
        nbrCoupures100 = 0
      }

        nbrCoupures50 = montant_retrait / 50
      if (montant_retrait >= 50) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures50 + " billet(s) de 50 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }

      if (choix_client != "o") { 
        nbr_billets_client50 = choix_client.toInt 
      }
      while ((nbrCoupures50 <= nbr_billets_client50) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client50 = choix_client.toInt 
      }
      }

      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures50 * 50)
        nbr_billets_client50 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client50 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client50 * 50)
        nbrCoupures50 = 0
      }

        nbrCoupures20 = montant_retrait / 20
      if (montant_retrait >= 20) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures20 + " billet(s) de 20 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }

      if (choix_client != "o") { 
        nbr_billets_client20 = choix_client.toInt 
      }
      while ((nbrCoupures20 <= nbr_billets_client20) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client20 = choix_client.toInt 
      }
      }

      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures20 * 20)
        nbr_billets_client20 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client20 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client20 * 20)
        nbrCoupures20 = 0
      }

        nbrCoupures10 = montant_retrait / 10
      if (montant_retrait >= 10) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures10 + " billet(s) de 10 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }

      if (choix_client != "o") { 
        nbr_billets_client10 = choix_client.toInt 
      }
      while ((nbrCoupures10 <= nbr_billets_client10) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client10 = choix_client.toInt 
      }
      }

      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures10 * 10)
        nbr_billets_client10 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client10 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client10 * 10)
        nbrCoupures10 = 0
      }

        print("Veuillez retirer la somme demandée : ")
      if (nbrCoupures500 > 0) {
        print("\n" + nbrCoupures500 + " billet(s) de 500 CHF ")
      }
      if (nbrCoupures200 > 0) {
        print("\n" + nbrCoupures200 + " billet(s) de 200 CHF ")
      }
      if (nbrCoupures100 > 0) {
        print("\n" + nbrCoupures100 + " billet(s) de 100 CHF ")
      }
      if (nbr_billets_client100 > 0) {
        print("\n" + nbr_billets_client100 + " billet(s) de 100 CHF ")
      }
      if (nbrCoupures50 > 0) {
        print("\n" + nbrCoupures50 + " billet(s) de 50 CHF ")
      }
      if (nbr_billets_client50 > 0) {
        print("\n" + nbr_billets_client50 + " billet(s) de 50 CHF ")
      }
      if (nbrCoupures20 > 0) {
        print("\n" + nbrCoupures20 + " billet(s) de 20 CHF ")
      }
      if (nbr_billets_client20 > 0) {
        print("\n" + nbr_billets_client20 + " billet(s) de 20 CHF ")
      }
      if (nbrCoupures10 > 0) {
        print("\n" + nbrCoupures10 + " billet(s) de 10 CHF ")
      }
      if (nbr_billets_client10 > 0) {
        print("\n" + nbr_billets_client10 + " billet(s) de 10 CHF ")
      }
        comptes(id) = comptes(id) - montant_retrait_initial 
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      } // fin du choix grosses coupures 
  
      // choix petites coupures 
      if (choix_coupures == 2) {
        nbrCoupures100 = montant_retrait / 100
      if (montant_retrait >= 100) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures100 + " billet(s) de 100 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }
  
      if (choix_client != "o") { 
        nbr_billets_client100 = choix_client.toInt 
      }
      while ((nbrCoupures100 <= nbr_billets_client100) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client100 = choix_client.toInt 
      }
      }
  
      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures100 * 100)
        nbr_billets_client100 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client100 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client100 * 100)
        nbrCoupures100 = 0
      }
  
        nbrCoupures50 = montant_retrait / 50
      if (montant_retrait >= 50) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures50 + " billet(s) de 50 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }
  
      if (choix_client != "o") { 
        nbr_billets_client50 = choix_client.toInt 
      }
      while ((nbrCoupures50 <= nbr_billets_client50) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client50 = choix_client.toInt 
      }
      }
  
      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures50 * 50)
        nbr_billets_client50 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client50 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client50 * 50)
        nbrCoupures50 = 0
      }
  
        nbrCoupures20 = montant_retrait / 20
      if (montant_retrait >= 20) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures20 + " billet(s) de 20 CHF.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }
  
      if (choix_client != "o") { 
        nbr_billets_client20 = choix_client.toInt 
      }
      while ((nbrCoupures20 <= nbr_billets_client20) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client20 = choix_client.toInt 
      } 
      }
  
      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures20 * 20)
        nbr_billets_client20 = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client20 = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client20 * 20)
        nbrCoupures20 = 0
      }
  
        nbrCoupures10 = montant_retrait / 10
      if (montant_retrait >= 10) { 
        println("Il reste " + montant_retrait + "CHF à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures10 + " billet(s) de 10 CHF.") 
  
        montant_retrait = montant_retrait - (nbrCoupures10 * 10)
        nbr_billets_client10 = 0 
      }
  
        print("Veuillez retirer la somme demandée : ")
      if (nbrCoupures100 > 0) {
        print("\n" + nbrCoupures100 + " billet(s) de 100 CHF ")
      }
      if (nbr_billets_client100 > 0) {
        print("\n" + nbr_billets_client100 + " billet(s) de 100 CHF ")
      }
      if (nbrCoupures50 > 0) {
        print("\n" + nbrCoupures50 + " billet(s) de 50 CHF ")
      }
      if (nbr_billets_client50 > 0) {
        print("\n" + nbr_billets_client50 + " billet(s) de 50 CHF ")
      }
      if (nbrCoupures20 > 0) {
        print("\n" + nbrCoupures20 + " billet(s) de 20 CHF ")
      }
      if (nbr_billets_client20 > 0) {
        print("\n" + nbr_billets_client20 + " billet(s) de 20 CHF ")
      }
      if (nbrCoupures10 > 0) {
        print("\n" + nbrCoupures10 + " billet(s) de 10 CHF ")
      }
      if (nbr_billets_client10 > 0) {
        print("\n" + nbr_billets_client10 + " billet(s) de 10 CHF ")
      }
        comptes(id) = comptes(id) - montant_retrait_initial 
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      } // fin du choix petites coupures 
      } // fin du retrait en CHF
  
        // retrait EUR
      if (devise_retrait == 2) {
        nbrCoupures100E = montant_retrait / 100
      if (montant_retrait >= 100) { 
        println("Il reste " + montant_retrait + "EUR à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures100E + " billet(s) de 100 EUR.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }
  
      if (choix_client != "o") { 
        nbr_billets_client100E = choix_client.toInt 
      }
      while ((nbrCoupures100E <= nbr_billets_client100E) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client100E = choix_client.toInt 
      } 
      }
  
      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures100E * 100) 
        nbr_billets_client100E = 0 }
      if (choix_client != "o") { 
        nbr_billets_client100E = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client100E * 100)
        nbrCoupures100E = 0
      }
  
        nbrCoupures50E = montant_retrait / 50
      if (montant_retrait >= 50) { 
        println("Il reste " + montant_retrait + "EUR à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures50E + " billet(s) de 50 EUR.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }
  
      if (choix_client != "o") { 
        nbr_billets_client50E = choix_client.toInt 
      }
      while ((nbrCoupures50E <= nbr_billets_client50E) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client50E = choix_client.toInt 
      } 
      }
  
      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures50E * 50)
        nbr_billets_client50E = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client50E = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client50E * 50)
        nbrCoupures50E = 0
      }
  
        nbrCoupures20E = montant_retrait / 20
      if (montant_retrait >= 20) { 
        println("Il reste " + montant_retrait + "EUR à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures20E + " billet(s) de 20 EUR.\nTapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      }
  
      if (choix_client != "o") { 
        nbr_billets_client20E = choix_client.toInt 
      }
      while ((nbrCoupures20E <= nbr_billets_client20E) && (choix_client != "o")) {
        println("Vous devez tapez o pour ok ou une autre valeur inférieure à celle proposée") 
        choix_client = readLine() 
      if (choix_client != "o") { 
        nbr_billets_client20E = choix_client.toInt 
      } 
      }
  
      if (choix_client == "o") { 
        montant_retrait = montant_retrait - (nbrCoupures20E * 20)
        nbr_billets_client20E = 0 
      }
      if (choix_client != "o") { 
        nbr_billets_client20E = choix_client.toInt 
        montant_retrait = montant_retrait - (nbr_billets_client20E * 20)
        nbrCoupures20E = 0
      }
  
        nbrCoupures10E = montant_retrait / 10
      if (montant_retrait >= 10) { 
        println("Il reste " + montant_retrait + "EUR à distribuer.\nVous pouvez obtenir au maximum " + nbrCoupures10E + " billet(s) de 10 EUR.") 
        montant_retrait = montant_retrait - (nbrCoupures10E * 10)
        nbr_billets_client10E = 0 
      }
  
      if (devise_retrait==2) comptes(id) = comptes(id) - (montant_retrait_initial * 0.95)
        print("Veuillez retirer la somme demandée : ")
      if (nbrCoupures100E > 0) {
        print("\n" + nbrCoupures100E + " billet(s) de 100 EUR ")
      }
      if (nbr_billets_client100E > 0) {
        print("\n" + nbr_billets_client100E + " billet(s) de 100 EUR ")
      }
      if (nbrCoupures50E > 0) {
        print("\n" + nbrCoupures50E + " billet(s) de 50 EUR ")
      }
      if (nbr_billets_client50E > 0) {
        print("\n" + nbr_billets_client50E + " billet(s) de 50 EUR ")
      }
      if (nbrCoupures20E > 0) {
        print("\n" + nbrCoupures20E + " billet(s) de 20 EUR ")
      }
      if (nbr_billets_client20E > 0) {
        print("\n" + nbr_billets_client20E + " billet(s) de 20 EUR ")
      }
      if (nbrCoupures10E > 0) {
        print("\n" + nbrCoupures10E + " billet(s) de 10 EUR ")
      }  
      if (nbr_billets_client10E > 0) {
        print("\n" + nbr_billets_client10E + " billet(s) de 10 EUR ")
      }
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      } // fin du retrait en EUR
    } // fin de l'opération 2  

    // début de l'opération 4
    def changepin(id : Int, codespin : Array[String]) : Unit = {
        println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
        codespin(id) = readLine()
      while (codespin(id).length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères ")
        codespin(id) = readLine()
      }
    } // fin de l'opération 4

    def main(args: Array[String]): Unit = { 
      // identifiant clients + code PIN
      var id = 0
      var nbclients = 100
      var tentativesRestantes = 3
      var codespin = Array.fill(nbclients)("INTRO1234")
      var comptes = Array.fill(nbclients)(1200.0)
      var choix_operation = 0
      var pin = "x"
  
      while (id < nbclients) { 
        tentativesRestantes = 3
        choix_operation = 0
        id = readLine("Saisissez votre code identifiant > ").toInt
      if (id > 99) {
        println("Cet identifiant n’est pas valable.")
        sys.exit(0)
      }
        println("Saisissez votre code PIN >")
        pin = readLine()
      while (tentativesRestantes != 0 && codespin(id) != pin) {
        tentativesRestantes -= 1
        println("Code pin erroné, il vous reste " + tentativesRestantes + " tentatives >") 
      if (tentativesRestantes!=0)  {
        println("Saisissez votre code PIN >")
        pin = readLine() 
      }
      }   
      if (tentativesRestantes == 0) {
        println("Trop d’erreurs, abandon de l’identification")
      }
        
      // choix des opérations
      while (codespin(id) == pin && choix_operation != 5) { 
        println("Choisissez votre opération: \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \nVotre choix:")
        choix_operation = readInt()
         
       
      if (choix_operation == 1) {
        depot(id, comptes)
      }
    
      if (choix_operation == 2) {
        retrait(id, comptes)
      }
    
      // début de l'opération 3
      if (choix_operation == 3) { 
        printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id)) 
      } // fin de l'opération 3
    
      if (choix_operation == 4) {
        changepin(id, codespin)
      }
       
      } // fin de la boucle du choix des opérations
        
      if (choix_operation == 5) { 
        println("Fin des opérations, n'oubliez pas de récupérer votre carte.") 
      }
      } // fin de la boucle principal 
    }
}
