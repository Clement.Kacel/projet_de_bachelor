//Assignment: Sami Mechhidan_996493_assignsubmission_file

import scala.io.StdIn._

object Main {
  
  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")
  var fin = 0
  var id = 0
  var pin = ""
  var verifpin = 0
  var montant = 1200.0
  var choix = 0
  var entree = ""
  var compteurP = 0
  var compteurT = 3
  var devise = 0
  var devise2 = 0 
  var depot = 0.0
  var retrait = 0.0
  var plafond = 0.0
  var coupure = 0
  var ok = ""
  var C500 = 0.0
  var C200 = 0.0
  var C100 = 0.0
  var C50 = 0.0
  var C20 = 0.0
  var C10 = 0.0


  def depot(id : Int, comptes : Array[Double]) : Unit = { //DEPOT
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    devise = readInt()

    println("Indiquez le montant du dépôt >")
    depot = readInt()

    while (depot%10 != 0) {
      println("Le montant doit être un multiple de 10. Indiquez le nouveau montant du dépôt >")
      depot = readInt()
    }
    if (devise == 1 ) {
      montant += depot
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :" + montant + "CHF")

      }

    if(devise == 2) {
      depot *= 0.95
      montant += depot
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :" + montant + "CHF")
    }
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = { //RETRAIT
    devise2 = 0 
    while (devise2 !=1 && devise2 != 2){
      println("Indiquez la devise :1 CHF, 2 : EUR >")
      devise2 = readInt()
    }
    println("Indiquez le montant du retrait >")
    retrait = readInt()
    plafond= 0.1*montant
    while (retrait%10 != 0 || retrait > plafond) {
      if (retrait%10 != 0) {
        println("Le montant doit être un multiple de 10.")
      }
      if (retrait > plafond){
        println("Votre plafond de retrait autorisé est de :" + plafond)
      }
      retrait = readInt()
    }

    if (devise2 == 1 ) { // retrait en CHF
      montant -= retrait // MAJ du montant total du compte
      if (retrait >= 200){
        while (coupure !=1 && coupure != 2){
          println("En 1) grosses coupures, 2) petites coupures >")
          coupure = readInt()
          }
          if (coupure == 1){ // grosse coupure
            C500 = retrait/500
            C500 = C500.toInt
            if (C500 >= 1){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + C500.toInt + " billet(s) de 500 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              ok = readLine()
              if (ok == "o"){
                retrait -= 500*C500
              }
              if (ok != "o"){
                while(ok.toInt > C500){
                  println ("Valeur trop grande")
                  println("Tapez une autre valeur inférieure à celle proposée")
                  ok = readLine()
                }
                C500 = ok.toInt
                retrait -= 500*C500
                }

            }
            C200 = retrait/200
            C200 = C200.toInt
            if (C200 >= 1){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + C200.toInt + " billet(s) de 200CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              ok = readLine()
              if (ok == "o"){
                retrait -= 200*C200
              }
              if (ok != "o"){
                while(ok.toInt > C200){
                  println ("Valeur trop grande")
                  println("Tapez une autre valeur inférieure à celle proposée")
                  ok = readLine()
                }
                C200 = ok.toInt
                retrait -= 200*C200
                }

            }
            C100 = retrait/100
            C100 = C100.toInt
            if (C100 >= 1){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + C100.toInt + " billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              ok = readLine()
              if (ok == "o"){
                retrait -= 100*C100
              }
              if (ok != "o"){
                while(ok.toInt > C100){
                  println ("Valeur trop grande")
                  println("Tapez une autre valeur inférieure à celle proposée")
                  ok = readLine()
                }
                C100 = ok.toInt
                retrait -= 100*C100
                }

            }
            C50 = retrait/50
            C50 = C50.toInt
            if (C50 >= 1){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + C50.toInt + " billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              ok = readLine()
              if (ok == "o"){
                retrait -= 50*C50
              }
              if (ok != "o"){
                while(ok.toInt > C50){
                  println ("Valeur trop grande")
                  println("Tapez une autre valeur inférieure à celle proposée")
                  ok = readLine()
                }
                C50 = ok.toInt
                retrait -= 50*C50
                }

            }
            C20 = retrait/20
            C20 = C20.toInt
            if (C20 >= 1){
              println("Il reste " + retrait + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + C20.toInt + " billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              ok = readLine()
              if (ok == "o"){
                retrait -= 20*C20
              }
              if (ok != "o"){
                while(ok.toInt > C20){
                  println ("Valeur trop grande")
                  println("Tapez une autre valeur inférieure à celle proposée")
                  ok = readLine()
                }
                C20 = ok.toInt
                retrait -= 20*C20
                }

            }
            if (C500+C200+C100+C50+C20 == 0){   // si aucun billet precedent
              C10 = retrait/10
              C10 = C10.toInt
            }
            else {
              C10 = retrait/10
              C10 = C10.toInt
              if (C10 >= 1){
                println("Il reste " + retrait + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + C10.toInt + " billet(s) de 10 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                ok = readLine()
                if (ok == "o"){
                  retrait -= 10*C10
                }
                if (ok != "o"){
                  while(ok.toInt > C10){
                    println ("Valeur trop grande")
                    println("Tapez une autre valeur inférieure à celle proposée")
                    ok = readLine()
                  }
                  C10 = ok.toInt
                  retrait -= 10*C10
                  }

              }

            }

            println("Veuillez retirer la somme demandée :")
            if (C500 > 0){
              println(C500 + " billet(s) de 500 CHF")
            }
            if (C200 > 0){
              println(C200 + " billet(s) de 200 CHF")
            }
            if (C100 > 0){
              println(C100 + " billet(s) de 100 CHF")
            }
            if (C50 > 0){
              println(C50 + " billet(s) de 50 CHF")
            }
            if (C20 > 0){
              println(C20 + " billet(s) de 20 CHF")
            }
            if (C10 > 0){
              println(C10 + " billet(s) de 10 CHF")
            }
        }
      }

      
        
      if (coupure == 2 || retrait < 200){ // petite coupure
        C100 = retrait/100
        C100 = C100.toInt
        if (C100 >= 1){
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + C100.toInt + " billet(s) de 100 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          ok = readLine()
          if (ok == "o"){
            retrait -= 100*C100
          }
          if (ok != "o"){
            while(ok.toInt > C100){
              println ("Valeur trop grande")
              println("Tapez une autre valeur inférieure à celle proposée")
              ok = readLine()
            }
            C100 = ok.toInt
            retrait -= 100*C100
            }

        }
        C50 = retrait/50
        C50 = C50.toInt
        if (C50 >= 1){
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + C50.toInt + " billet(s) de 50 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          ok = readLine()
          if (ok == "o"){
            retrait -= 50*C50
          }
          if (ok != "o"){
            while(ok.toInt > C50){
              println ("Valeur trop grande")
              println("Tapez une autre valeur inférieure à celle proposée")
              ok = readLine()
            }
            C50 = ok.toInt
            retrait -= 50*C50
            }

        }
        C20 = retrait/20
        C20 = C20.toInt
        if (C20 >= 1){
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + C20.toInt + " billet(s) de 20 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          ok = readLine()
          if (ok == "o"){
            retrait -= 20*C20
          }
          if (ok != "o"){
            while(ok.toInt > C20){
              println ("Valeur trop grande")
              println("Tapez une autre valeur inférieure à celle proposée")
              ok = readLine()
            }
            C20 = ok.toInt
            retrait -= 20*C20
            }

        }
        if (C100+C50+C20 == 0){   // si aucun billet precedent
          C10 = retrait/10
          C10 = C10.toInt
        }
        else{
          C10 = retrait/10
          C10 = C10.toInt
          if (C10 >= 1){
            println("Il reste " + retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + C10.toInt + " billet(s) de 10 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            ok = readLine()
            if (ok == "o"){
              retrait -= 10*C10
            }
            if (ok != "o"){
              while(ok.toInt > C10){
                println ("Valeur trop grande")
                println("Tapez une autre valeur inférieure à celle proposée")
                ok = readLine()
              }
              C10 = ok.toInt
              retrait -= 10*C10
              }

          }
        }

        println("Veuillez retirer la somme demandée :")
        if (C500 > 0){
          println(C500 + " billet(s) de 500 CHF")
        }
        if (C200 > 0){
          println(C200 + " billet(s) de 200 CHF")
        }
        if (C100 > 0){
          println(C100 + " billet(s) de 100 CHF")
        }
        if (C50 > 0){
          println(C50 + " billet(s) de 50 CHF")
        }
        if (C20 > 0){
          println(C20 + " billet(s) de 20 CHF")
        }
        if (C10 > 0){
          println(C10 + " billet(s) de 10 CHF")
        }

      }
      

    }
    if (devise2 == 2){ // retrait en Euro
      montant = montant - retrait * 0.95 // MAJ du montant
      C100 = retrait/100
      C100 = C100.toInt
      if (C100 >= 1){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + C100.toInt + " billet(s) de 100 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        ok = readLine()
        if (ok == "o"){
          retrait -= 100*C100
        }
        if (ok != "o"){
          while(ok.toInt > C100){
            println ("Valeur trop grande")
            println("Tapez une autre valeur inférieure à celle proposée")
            ok = readLine()
          }
          C100 = ok.toInt
          retrait -= 100*C100
          }

      }
      C50 = retrait/50
      C50 = C50.toInt
      if (C50 >= 1){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + C50.toInt + " billet(s) de 50 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        ok = readLine()
        if (ok == "o"){
          retrait -= 50*C50
        }
        if (ok != "o"){
          while(ok.toInt > C50){
            println ("Valeur trop grande")
            println("Tapez une autre valeur inférieure à celle proposée")
            ok = readLine()
          }
          C50 = ok.toInt
          retrait -= 50*C50
          }

      }
      C20 = retrait/20
      C20 = C20.toInt
      if (C20 >= 1){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + C20.toInt + " billet(s) de 20 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        ok = readLine()
        if (ok == "o"){
          retrait -= 20*C20
        }
        if (ok != "o"){
          while(ok.toInt > C20){
            println ("Valeur trop grande")
            println("Tapez une autre valeur inférieure à celle proposée")
            ok = readLine()
          }
          C20 = ok.toInt
          retrait -= 20*C20
          }

      }
      if (C100+C50+C20 == 0){   // si aucun billet precedent
        C10 = retrait/10
        C10 = C10.toInt
      }
      else {
        C10 = retrait/10
        C10 = C10.toInt
        if (C10 >= 1){
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + C10.toInt + " billet(s) de 10 EUR")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          ok = readLine()
          if (ok == "o"){
            retrait -= 10*C10
          }
          if (ok != "o"){
            while(ok.toInt > C10){
              println ("Valeur trop grande")
              println("Tapez une autre valeur inférieure à celle proposée")
              ok = readLine()
            }
            C10 = ok.toInt
            retrait -= 10*C10
            }
        }
      }
      C10 = retrait/10
      C10 = C10.toInt
      if (C10 >= 1){
        println("Il reste " + retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + C10.toInt + " billet(s) de 10 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        ok = readLine()
        if (ok == "o"){
          retrait -= 10*C10
        }
        if (ok != "o"){
          while(ok.toInt > C10){
            println ("Valeur trop grande")
            println("Tapez une autre valeur inférieure à celle proposée")
            ok = readLine()
          }
          C10 = ok.toInt
          retrait -= 10*C10
          }
      }
      println("Veuillez retirer la somme demandée :")
      if (C100 > 0){
        println(C100 + " billet(s) de 100 CHF")
      }
      if (C50 > 0){
        println(C50 + " billet(s) de 50 CHF")
      }
      if (C20 > 0){
        println(C20 + " billet(s) de 20 CHF")
      }
      if (C10 > 0){
        println(C10 + " billet(s) de 10 CHF")
      }
    
    }
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    
    println("Saisissez votre nouveau code pin d'au moins 8 caractères")
    codespin(id) = readLine()
    while (codespin(id).length < 8){
      println("Saisissez votre nouveau code pin d'au moins 8 caractères")
      codespin(id) = readLine()
    }
    
  }
  





  
  def main(args: Array[String]): Unit = {

   

    
    while (verifpin != 1){
      verifpin = 0
      println("Saisissez votre code identifiant >")
      id = readInt()
      if (id > 100){
        println("Cet identifiant n’est pas valable. ")
        System.exit(0)
      }
      else{
        println("Saisissez votre code pin >")
        pin = readLine()
        if (pin != codespin(id)){
          println("Code pin erroné, il vous reste 2 tentatives >")
          pin = readLine()
          if (pin != codespin(id)){
            println("Code pin erroné, il vous reste 1 tentatives >")
            pin = readLine()
            if (pin != codespin(id)){
              println("Trop d’erreurs, abandon de l’identification")
            }
          }
        }
      }
      if (pin == codespin(id)){
        verifpin = 1
      }
    }

    
    while (fin != 1){ // debut boucle des choix
      
      while (verifpin != 1){
        
        println("Saisissez votre code identifiant >")
        id = readInt()
        if (id > 100){
          println("Cet identifiant n’est pas valable. ")
          System.exit(0)
        }
        else{
          println("Saisissez votre code pin >")
          pin = readLine()
          if (pin != codespin(id)){
            println("Code pin erroné, il vous reste 2 tentatives >")
            pin = readLine()
            if (pin != codespin(id)){
              println("Code pin erroné, il vous reste 1 tentatives >")
              pin = readLine()
              if (pin != codespin(id)){
                println("Trop d’erreurs, abandon de l’identification")
              }
            }
          }
        }
        if (pin == codespin(id)){
          verifpin = 1
        }
      }
      
      
      
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")
      choix = readInt()


      if (choix == 1){
        depot(id, comptes)
      }
      if (choix == 2){
        retrait(id, comptes)
      }
      if (choix == 4){
        changepin(id, codespin)
      }
      if (choix == 3){ // 3) CONSULTATION DU COMPTE
        println(f"Le montant disponible sur votre compte est de : $montant%.2f CHF")
      }
      if (choix == 5){
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        verifpin = 0 // reset pour refaire une identification
      }


    }




    
  }
}
