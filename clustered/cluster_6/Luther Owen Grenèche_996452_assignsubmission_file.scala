//Assignment: Luther Owen Grenèche_996452_assignsubmission_file

import scala.io.StdIn._
object Main {
  def main(args: Array[String]): Unit = {
    var choix = 0
    var NBBillet500 = 0
    var NBBillet200 = 0 
    var NBBillet100 = 0 
    var NBBillet50 = 0 
    var NBBillet20 = 0
    var NBBillet10 = 0
    
    var NBBillet100EUR = 0 
    var NBBillet50EUR = 0 
    var NBBillet20EUR = 0
    var NBBillet10EUR = 0
    
    var conf1 = "o"
    var conf2 = "o"
    var conf3 = "o"
    var conf4 = "o"
    var conf5 = "o"
    var conf6 = "o"

    var Retrait = 0.0
    var RetraitTotal = Retrait 

    val CodePinOrdi = ("INTRO1234")
    var CodePin = ""

    var NBtentatives = 0
    val NBtentativesMax = 3
    var TentativesRestantes = NBtentativesMax - NBtentatives



    // condition que si client met chiffre > 4 alors on refait la question
        do {
          println("Choisissez votre opération : ") 
          println("1 : dépot")
          println("2 : Retrait") 
          println("3 : Consultation du compte") 
          println("4 : Terminer")
          println("Votre choix : ")   
          choix = readLine().toInt
        } while (choix < 1 || choix > 4)




        if (choix == 4){
          println("Merci et à la prochaine")
        } else if(TentativesRestantes != 1) {
          println("Saisissez votre code pin")
          CodePin = readLine()
          while (!(CodePinOrdi == CodePin) && (TentativesRestantes > 1)) {
            NBtentatives += 1
            TentativesRestantes = NBtentativesMax - NBtentatives
            if(TentativesRestantes >= 1) println("Code pin erroné, il vous reste " + TentativesRestantes + " tentatives")
            CodePin = readLine("Veuillez resaissir votre code pin : ")
            if (TentativesRestantes == 1) println("Pour votre protection, les opérations bancaires vont s'interrompre, récuperer votre carte.")

          }
        }
      
        
          
        



          //on écrit prog comme quoi si le client fait son code faux 3 fois, le prog s'arrête
          

          

          var ArgentSurCompte = 1200.0
          var Devise = 0
          var Depot = 0.0

          if (choix == 1 && TentativesRestantes != 1){
             Devise = readLine("Indiquez la devise du dépot : 1) CHF ; 2) EUR ").toInt
             Depot = readLine("Indiquez le montant du dépôt : ").toDouble
            while (!(Depot % 10.0 == 0.0)){
              println("Le dépot doit être un multiple de 10")
              Depot =readLine("Veuillez indiquer le montant du dépot : ").toDouble
            }
            if (Devise == 2){
              Depot = Depot * 0.95
              ArgentSurCompte = ArgentSurCompte + Depot
              printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ArgentSurCompte)
            }
            if (Devise == 1){
              ArgentSurCompte = ArgentSurCompte + Depot
               printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , ArgentSurCompte )
            }


          }





          if(choix == 3 && TentativesRestantes != 1){
            printf("Le montant disponible sur votre compte est de : %.2f CHF \n " , ArgentSurCompte)
          }










          if(choix == 2 && TentativesRestantes != 1){

            Devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR ").toInt
            while (!(Devise == 1 || Devise == 2)){
              Devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR ").toInt
            }

                    Retrait = readLine("Indiquez le montant du retrait : ").toDouble
            RetraitTotal = Retrait
          }
             var MontantAutorise = ArgentSurCompte * 10 / 100
    while(Retrait % 10.0 != 0.0 || MontantAutorise < Retrait){
    while(MontantAutorise < Retrait){
          println("Votre plafond de retrait autorisé est de : " + MontantAutorise )
          Retrait = readLine("Veuillez indiquer le montant du retrait : ").toDouble
    }


            while (!(Retrait % 10.0 == 0.0)){
              println("Le retrait doit être un multiple de 10")
              Retrait =readLine("Veuillez indiquer le montant du retrait : ").toDouble
              RetraitTotal = Retrait
            }
    }

           
            

            var ChoixCoupures = 0 

             if (choix == 2 && Devise == 1){
            if (Retrait >= 200){

                ChoixCoupures = readLine("En 1) grosses coupures, 2) petites coupures : ").toInt
              while(!(ChoixCoupures == 1 || ChoixCoupures == 2)){
                ChoixCoupures = readLine("En 1) grosses coupures, 2) petites coupures : ").toInt
              }

              } else {
                 ChoixCoupures = 2
               }

             }
            
            if(Devise == 1 && ChoixCoupures == 1){



            if(Retrait >= 500){
              NBBillet500 = (Retrait / 500).toInt
            }
            if (NBBillet500 > 0){
              println("Vous pouvez obtenir au maximum " + NBBillet500 + " billets de 500 CHF.")
              conf1 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString

              while(!(conf1 == "o" || conf1.toInt < NBBillet500)){
                conf1 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
              }


                if(conf1 == "o"){
                  Retrait -= (NBBillet500 * 500)
                if(!(Retrait == 0)){
                  println("Il reste " + Retrait + " CHF à distribuer")
                }
              }

              else {
                NBBillet500 = conf1.toInt
                Retrait -= (NBBillet500 * 500)
                println("Il reste " + Retrait + " CHF à distribuer")
              }
            }
            else{
              NBBillet500 = 0
            }
    
              if(Retrait < 500 && Retrait >= 200 || conf1 != "o"){
                NBBillet200 = (Retrait / 200).toInt
              }
              if(NBBillet200 > 0){
                println("Vous pouvez obtenir au maximum " + NBBillet200 + " billets de 200 CHF.")
                conf2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                while(!(conf2 == "o" || conf2.toInt < NBBillet200)){
                  conf2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                }
               if(conf2 == "o"){
                 Retrait -= (NBBillet200 * 200)
                 if(!(Retrait == 0)){
                   println("Il reste " + Retrait + " CHF à distribuer")
                 }
               }
                 else{
                   NBBillet200 = conf2.toInt
                   Retrait -= (NBBillet200 * 200)
                   println("Il reste " + Retrait + " CHF à distribuer") 
               }
              }
                 
                else{
                  NBBillet200 = 0
                }
                
              

  
            if(Retrait < 200 && Retrait >= 100 || conf2 != "o"){
              NBBillet100 = (Retrait / 100).toInt
            }
            if(NBBillet100 > 0){
              println("Vous pouvez obtenir au maximum " + NBBillet100 + " billets de 100 CHF.")
              conf3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
              while(!(conf3 == "o" || conf3.toInt < NBBillet100)){
                conf3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
              }
             if(conf3 == "o"){
               Retrait -= (NBBillet100 * 100)
               if(!(Retrait == 0)){
                 println("Il reste " + Retrait + " CHF à distribuer")
               }
             }
               else{
                 NBBillet100 = conf3.toInt
                 Retrait -= (NBBillet100 * 100)
                 println("Il reste " + Retrait + " CHF à distribuer") 
             }
            }
               
              else{
                NBBillet100 = 0
              }

          
                        if(Retrait < 100 && Retrait >= 50 || conf3 != "o"){
                          NBBillet50 = (Retrait / 50).toInt
                        }
                        if(NBBillet50 > 0){
                          println("Vous pouvez obtenir au maximum " + NBBillet50 + " billets de 50 CHF.")
                          conf4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                          while(!(conf4 == "o" || conf4.toInt < NBBillet50)){
                            conf4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                          }
                         if(conf4 == "o"){
                           Retrait -= (NBBillet50 * 50)
                           if(!(Retrait == 0)){
                             println("Il reste " + Retrait + " CHF à distribuer")
                           }
                         }
                           else{
                             NBBillet50 = conf4.toInt
                             Retrait -= (NBBillet50 * 50)
                             println("Il reste " + Retrait + " CHF à distribuer") 
                         }
                        }

                          else{
                            NBBillet50 = 0
                          }

             
                        if(Retrait < 50 && Retrait >= 20 || conf4 != "o"){
                          NBBillet20 = (Retrait / 20).toInt
                        }
                        if(NBBillet20 > 0){
                          println("Vous pouvez obtenir au maximum " + NBBillet20 + " billets de 20 CHF.")
                          conf5 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                          while(!(conf5 == "o" || conf5.toInt < NBBillet20)){
                            conf5 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                          }
                         if(conf5 == "o"){
                           Retrait -= (NBBillet20 * 20)
                           if(!(Retrait == 0)){
                             println("Il reste " + Retrait + " CHF à distribuer")
                           }
                         }
                           else{
                             NBBillet20 = conf5.toInt
                             Retrait -= (NBBillet20 * 20)
                             println("Il reste " + Retrait + " CHF à distribuer") 
                         }
                        }

                          else{
                            NBBillet20 = 0
                          }


              
                        if(Retrait < 20 && Retrait >= 10 || conf5 != "o"){
                          NBBillet10 = (Retrait / 10).toInt
                        }
                        if(NBBillet10 > 0){
                          println("Vous pouvez obtenir au maximum " + NBBillet10 + " billets de 10 CHF.")
                          conf6 = readLine("Tapez o pour ok : ").toString
                          while(!(conf6 == "o")){
                            println("Vous devez tapez o")
                            conf6 = readLine("Tapez o pour ok : ").toString
                          }
                         if(conf6 == "o"){
                           Retrait -= (NBBillet10 * 10)
                           if(!(Retrait == 0)){
                             println("Il reste " + Retrait + " CHF à distribuer")
                           }
                         }
                           else{
                             NBBillet10 = conf6.toInt
                             Retrait -= (NBBillet10 * 10)
                             println("Il reste " + Retrait + " CHF à distribuer") 
                         }
                        }

                          else{
                            NBBillet10 = 0
                          }

              
  }

                  if (Devise == 1 && ChoixCoupures == 2){
                    
                  
                              if( Retrait >= 100){
                                NBBillet100 = (Retrait / 100).toInt
                              }
                              if(NBBillet100 > 0){
                                println("Vous pouvez obtenir au maximum " + NBBillet100 + " billets de 100 CHF.")
                                conf3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                                while(!(conf3 == "o" || conf3.toInt < NBBillet100)){
                                  conf3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                                }
                               if(conf3 == "o"){
                                 Retrait -= (NBBillet100 * 100)
                                 if(!(Retrait == 0)){
                                   println("Il reste " + Retrait + " CHF à distribuer")
                                 }
                               }
                                 else{
                                   NBBillet100 = conf3.toInt
                                   Retrait -= (NBBillet100 * 100)
                                   println("Il reste " + Retrait + " CHF à distribuer") 
                               }
                              }

                                else{
                                  NBBillet100 = 0
                                }

                                
                                          if(Retrait < 100 && Retrait >= 50 || conf3 != "o"){
                                            NBBillet50 = (Retrait / 50).toInt
                                          }
                                          if(NBBillet50 > 0){
                                            println("Vous pouvez obtenir au maximum " + NBBillet50 + " billets de 50 CHF.")
                                            conf4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                                            while(!(conf4 == "o" || conf4.toInt < NBBillet50)){
                                              conf4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                                            }
                                           if(conf4 == "o"){
                                             Retrait -= (NBBillet50 * 50)
                                             if(!(Retrait == 0)){
                                               println("Il reste " + Retrait + " CHF à distribuer")
                                             }
                                           }
                                             else{
                                               NBBillet50 = conf4.toInt
                                               Retrait -= (NBBillet50 * 50)
                                               println("Il reste " + Retrait + " CHF à distribuer") 
                                           }
                                          }

                                            else{
                                              NBBillet50 = 0
                                            }

                                
                                          if(Retrait < 50 && Retrait >= 20 || conf4 != "o"){
                                            NBBillet20 = (Retrait / 20).toInt
                                          }
                                          if(NBBillet20 > 0){
                                            println("Vous pouvez obtenir au maximum " + NBBillet20 + " billets de 20 CHF.")
                                            conf5 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                                            while(!(conf5 == "o" || conf5.toInt < NBBillet20)){
                                              conf5 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                                            }
                                           if(conf5 == "o"){
                                             Retrait -= (NBBillet20 * 20)
                                             if(!(Retrait == 0)){
                                               println("Il reste " + Retrait + " CHF à distribuer")
                                             }
                                           }
                                             else{
                                               NBBillet20 = conf5.toInt
                                               Retrait -= (NBBillet20 * 20)
                                               println("Il reste " + Retrait + " CHF à distribuer") 
                                           }
                                          }

                                            else{
                                              NBBillet20 = 0
                                            }


                               
                                          if(Retrait < 20 && Retrait >= 10 || conf5 != "o"){
                                            NBBillet10 = (Retrait / 10).toInt
                                          }
                                          if(NBBillet10 > 0){
                                            println("Vous pouvez obtenir au maximum " + NBBillet10 + " billets de 10 CHF.")
                                            conf6 = readLine("Tapez o pour ok : ").toString
                                            while(!(conf6 == "o")){
                                              println("Vous devez tapez o")
                                              conf6 = readLine("Tapez o pour ok : ").toString
                                            }
                                           if(conf6 == "o"){
                                             Retrait -= (NBBillet10 * 10)
                                             if(!(Retrait == 0)){
                                               println("Il reste " + Retrait + " CHF à distribuer")
                                             }
                                           }
                                             else{
                                               NBBillet10 = conf6.toInt
                                               Retrait -= (NBBillet10 * 10)
                                               println("Il reste " + Retrait + " CHF à distribuer") 
                                           }
                                          }

                                            else{
                                              NBBillet10 = 0
                                            }
                  }

    if(Devise == 2){
    
    
    
              if(Retrait >= 100){
                NBBillet100EUR = (Retrait / 100).toInt
              }
              if(NBBillet100EUR > 0){
                println("Vous pouvez obtenir au maximum " + NBBillet100EUR + " billets de 100 EUR.")
                conf3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                while(!(conf3 == "o" || conf3.toInt < NBBillet100EUR)){
                  conf3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                }
               if(conf3 == "o"){
                 Retrait -= (NBBillet100EUR * 100)
                 if(!(Retrait == 0)){
                   println("Il reste " + Retrait + " EUR à distribuer")
                 }
               }
                 else{
                   NBBillet100EUR = conf3.toInt
                   Retrait -= (NBBillet100EUR * 100)
                   println("Il reste " + Retrait + " EUR à distribuer") 
               }
              }

                else{
                  NBBillet100EUR = 0
                }

               
                          if(Retrait < 100 && Retrait >= 50 || conf3 != "o"){
                            NBBillet50EUR = (Retrait / 50).toInt
                          }
                          if(NBBillet50EUR > 0){
                            println("Vous pouvez obtenir au maximum " + NBBillet50EUR + " billets de 50 EUR.")
                            conf4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                            while(!(conf4 == "o" || conf4.toInt < NBBillet50EUR)){
                              conf4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                            }
                           if(conf4 == "o"){
                             Retrait -= (NBBillet50EUR * 50)
                             if(!(Retrait == 0)){
                               println("Il reste " + Retrait + " EUR à distribuer")
                             }
                           }
                             else{
                               NBBillet50EUR = conf4.toInt
                               Retrait -= (NBBillet50EUR * 50)
                               println("Il reste " + Retrait + " EUR à distribuer") 
                           }
                          }

                            else{
                              NBBillet50EUR = 0
                            }

                
                          if(Retrait < 50 && Retrait >= 20 || conf4 != "o"){
                            NBBillet20EUR = (Retrait / 20).toInt
                          }
                          if(NBBillet20EUR > 0){
                            println("Vous pouvez obtenir au maximum " + NBBillet20EUR + " billets de 20 EUR.")
                            conf5 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                            while(!(conf5 == "o" || conf5.toInt < NBBillet20EUR)){
                              conf5 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
                            }
                           if(conf5 == "o"){
                             Retrait -= (NBBillet20EUR * 20)
                             if(!(Retrait == 0)){
                               println("Il reste " + Retrait + " EUR à distribuer")
                             }
                           }
                             else{
                               NBBillet20EUR = conf5.toInt
                               Retrait -= (NBBillet20EUR * 20)
                               println("Il reste " + Retrait + " EUR à distribuer") 
                           }
                          }

                            else{
                              NBBillet20EUR = 0
                            }


                
                          if(Retrait < 20 && Retrait >= 10 || conf5 != "o"){
                            NBBillet10EUR = (Retrait / 10).toInt
                          }
                          if(NBBillet10EUR > 0){
                            println("Vous pouvez obtenir au maximum " + NBBillet10EUR + " billets de 10 EUR.")
                            conf6 = readLine("Tapez o pour ok : ").toString
                            while(!(conf6 == "o")){
                              println("Vous devez tapez o")
                              conf6 = readLine("Tapez o pour ok : ").toString
                            }
                           if(conf6 == "o"){
                             Retrait -= (NBBillet10EUR * 10)
                             if(!(Retrait == 0)){
                               println("Il reste " + Retrait + " EUR à distribuer")
                             }
                           }
                             else{
                               NBBillet10EUR = conf6.toInt
                               Retrait -= (NBBillet10EUR * 10)
                               println("Il reste " + Retrait + " EUR à distribuer") 
                           }
                          }

                            else{
                              NBBillet10EUR = 0
                            }

    }
    if(choix == 2 && Devise == 1){
      ArgentSurCompte = ArgentSurCompte - RetraitTotal
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ArgentSurCompte)
    }
    if(choix == 2 && Devise == 2){
      ArgentSurCompte = ArgentSurCompte - (RetraitTotal * 0.95)
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ArgentSurCompte)
    }
    
    
 
    if (choix == 2 && Retrait == 0 && Devise == 1){
if (Devise == 1){
  println("Veuillez retirer la somme demandée : ")
  if(NBBillet500 > 0){
  println(NBBillet500 + " de 500 CHF.")
  }
  if(NBBillet200 > 0){
  println(NBBillet200 + " de 200 CHF.")
    }
  if(NBBillet100 > 0){
  println(NBBillet100 + " de 100 CHF.")
    }
  if(NBBillet50 > 0){
  println(NBBillet50 + " de 50 CHF.")
  }
  if(NBBillet20 > 0){
  println(NBBillet20 + " de 20 CHF.")
  }
  if(NBBillet10 > 0){
  println(NBBillet10 + " de 10 CHF.")
  }
  
}

      if(choix == 2 && Devise == 2 && Retrait == 0){
        println("Veuillez retirer la somme demandée : ")
        if(NBBillet100EUR > 0){
          println(NBBillet100EUR + " de 100 EUR.")
        }
       if(NBBillet50EUR > 0){
         println(NBBillet50EUR + " de 50 EUR.")
       }  
        if(NBBillet20EUR > 0){
          println(NBBillet20EUR + " de 20 EUR.")
        }
        if(NBBillet10EUR > 0){
          println(NBBillet10EUR + " de 10 EUR.")
        }
        
      }
    }
    if(TentativesRestantes != 1)
    println("Fin des opérations, n'oubliez pas de récuperer votre carte. ")

    
}
}
      
