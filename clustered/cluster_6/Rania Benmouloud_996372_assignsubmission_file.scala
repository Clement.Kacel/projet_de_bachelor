//Assignment: Rania Benmouloud_996372_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine

//Opération de changement de code pin

object Main{

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var nouveaucodepin = ""
    while (8 > nouveaucodepin.length) { println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères") Demande à l’utilisateur de saisir un nouveau code pin jusqu’à ce qu’il en saisisse un qui ait au moins 8 caractères.
       nouveaucodepin = readLine()
      if ( nouveaucodepin.length < 8) { println("Votre code pin ne contient pas au moins 8 caractères")//La saisie se répète tant que l’utilisateur n’a pas entré un nouveau code pin correspondant aux critères.
      }
                                    }
    codespin(id) = nouveaucodepin//Permet de remplacer l’ancien code pin de l’utilisateur par le nouveau code pin qu’il vient de saisir
  }

  
//Opération de retrait

  def retrait(id : Int, accounts : Array[Double]) : Unit = {
    var prelevement_devise = 3 
    var billet_choisi_10= 0 
    var billet_choisi_20= 0
    var billet_choisi_50 = 0
    var billet_choisi_100 = 0
    var billet_choisi_200 = 0
    var billet_choisi_500 = 0 //les variables qui représentent les différentes valeurs que l’utilisateur peut retirer du bancomat

    while (prelevement_devise!= 1 && prelevement_devise != 2) {
      println("Indiquez la devise : 1 CHF; 2 EUR >")//Tant que l’utilisateur n’a pas choisi la devise (EUR ou CHF) le bancomat continuera à lui demander.

      prelevement_devis = readLine().toInt

      if(prelevement_devis != 1 && prelevement_devis != 2 ){

        println("choix invalide")// Si l’utilisateur ne choisit ni le choix ‘1’ ni le choix ‘2’.
      }
    }

    var logique = false
    var x = 0; //stock le montant demander par l’utilisateur
    val plafond_comptes = comptes(id) * 0.1 //valeur qui  stocke 10 % du solde du compte à l'indice ’id ’dans le tableau ‘comptes’

    while (!logique){
    println("Indiquez le montant du retrait. >") /la boucle continue à s'exécuter tant que ‘logique’ est ‘false’. La boucle s’arrête lorsque logique’ est true.
    x = readInt() //la saisie de l’utilisateur est assignée à la variable x


      //vérifie si le montant saisi par l'utilisateur est un multiple de 10
              if(x % 10 == 0 ){

                if(x <= plafond_comptes  && x>0){
                  logique = true
                }else{
                  println("Votre plafond de retrait est de :" + plafond_compte)//vérifie si le montant saisi est inférieur au plafond autorisé et positif.
                }

                }else{
                    println("Le montant doit être un multiple de 10.")// Si le montant saisi par l’utilisateur n’est pas un multiple de 10
                  }
                }
     var montant_billet = 3

    //Si l’utilisateur choisit la devise ‘CHF’
          if((prelevement_devise == 1){

            if(x >= 200){

              while (montant_billet != 1 &&montant_billet != 2) {
                println("En 1) grosses coupures, 2) petites coupures >")//L’utilisateur doit choisir entre un retrait en grosse coupure ou en petites coupures
                montant_billet = readInt()//la saisie de l’utilisateur est assignée à cette variable
              }
            }
          }
             println("Veuillez retirer le montant demandé:")

             var reste = x
             //Vérifie si la devise sélectionnée pour le retrait est en ‘CHF
             if((prelevement_devise == 1) {

//Si utilisateur choisit grosses coupures
//Affiche le montant restant et le nombre maximal de billets de 500 CHF que l'utilisateur peut obtenir.
//Demande à l'utilisateur de choisir combien de billets de 500 CHF il souhaite retirer           
               if(montant billet == 1){

                 while (reste > 0){  
                   if(reste / 500 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/500)
                     println(" billet(s) de 500 CHF")

                     var  billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi= readLine()

                       if (billet_choisi != "o") {
                          billet_choisi_500 = billet_choisi.toInt
                         if (billet_choisi_500 < 0 || billet_choisi_500 > (reste / 500)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                            reste -= (billet_choisi_500  * 500)  
                            billet_choisi="o"  
                         }
                       }
                       else
                       {
                         billet_choisi_500 = reste/500
                         reste -= ((reste/500) * 500)
                       }
                     } while (billet_choisi != "o")
                   }
                   
//Permet à l'utilisateur de choisir le nombre de billets de 200 CHF à retirer
                   if(reste  / 200 >= 1){
                     println(s"Il reste $reste  CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/200)
                     println(" billet(s) de 200 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi = readLine()

                       if (billet_choisi != "o") {
                          billet_choisi_200 = billet_choisi.toInt
                         if (billet_choisi_200 < 0 || billet_choisi_200 > (reste / 200)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_200 * 200)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_200 = reste/200
                         reste -= ((reste/200) * 200)
                       }

                     } while (billet_choisi != "o")

                   }

// nombre de billets de 100 CHF que l'utilisteur souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer
                   if(reste / 100 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/100)
                     println(" billet(s) de 100 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi= readLine()

                       if (billet_choisi != "o") {
                          billet_choisi_100 = billet_choisi.toInt
                         if (billet_choisi_100 < 0 || billet_choisi_100 > (reste / 100)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_100  * 100)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_100  =reste/100
                         reste -= ((reste/100) * 100)
                       }

                     } while (billet_choisi != "o")

                   }

//nombre de billets de 50 CHF que l'utilisteur souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer
                   if(reste / 50 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/50)
                     println(" billet(s) de 50 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi = readLine()

                       if (billet_choisi != "o") {
                         billet_choisi_50 = billet_choisi.toInt
                         if (billet_choisi_50 < 0 || billet_choisi_50 > (reste / 50)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_50 * 50)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_50 = reste/50
                         reste -= ((reste/50) * 50)
                       }
                     } while (billet_choisi != "o")
                   }
                   

//nombre de billets de 20 CHF que l'utilisteur souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer
                   if(reste / 20 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/20)
                     println(" billet(s) de 20 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisit = readLine()

                       if (billet_choisi != "o") {
                         billet_choisi_20 = billet_choisi.toInt
                         if (billet_choisi_20 < 0 || billet_choisi_20 > (reste / 20)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_20 * 20)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_20 = reste/20
                         reste -= ((reste/20) * 20)
                       }

                     } while (billet_choisi != "o")

                   }

                  
//nombre de billets de 10 CHF que l'utilisteur souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer
                   if(reste / 10 >= 1) {  
                         println(s"Il reste $reste CHF à distribuer.")
                         print("Vous pouvez obtenir au maximum ")
                         print(reste/10)
                         billet_choisi_10 = (reste/10)

                         println(" billet(s) de 10 CHF")

                         reste -= ((reste/10) * 10)
                       }
                     }
                   }


//permet à l'utilisateur de spécifier le nombre de billets de 50 CHF qu'il souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer.
               else{
                 while (reste > 0){

                   if(reste / 100 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/100)
                     println(" billet(s) de 100 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi = readLine()

                       if (billet_choisi != "o") {
                          billet_choisi_100 = billet_choisi.toInt
                         if (billet_choisi_100 < 0 || billet_choisi_100 > (reste / 100)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_100 * 100)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_100 = reste/100
                         reste -= ((reste/100) * 100)
                       }
                     } while (billet_choisi != "o")
                   }
  ///permet à l'utilisateur de spécifier le nombre de billets de 100 CHF qu'il souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer.
                   if(reste / 50 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/50)
                     println(" billet(s) de 50 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi = readLine()

                       if (billet_choisi != "o") {
                         billet_choisi_50 = billet_choisi.toInt
                         if (billet_choisi_50 < 0 || billet_choisi_50 > (reste / 50)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_50 * 50)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_50 = reste/50
                         reste -= ((reste/50) * 50)
                       }

                     } while (billet_choisi != "o")

                   }

                
//permet à l'utilisateur de spécifier le nombre de billets de 20 CHF qu'il souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer.
                   if(reste / 20 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/20)
                     println(" billet(s) de 20 CHF")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi= readLine()

                       if (billet_choisi != "o") {
                         billet_choisi_20 = billet_choisi.toInt
                         if (billet_choisi_20 < 0 || billet_choisi_20> (reste / 20)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_20 * 20)
                           billet_choisi="o"
                         }
                       }
                       else
                       {
                         billet_choisi_20 = reste/20
                         reste -= ((reste/20) * 20)
                       }
                     } while (billet_choisi != "o")
                   }

                   
                   //permet à l'utilisateur de spécifier le nombre de billets de 10 CHF qu'il souhaite retirer, en s'assurant que le montant total retiré ne dépasse pas le montant restant à distribuer.

                       if(reste / 10 >= 1) {
                         println(s"Il reste $reste CHF à distribuer.")
                         print("Vous pouvez obtenir au maximum ")
                         print(reste/10)
                         billet_choisi_10 = (reste/10)

                         println(" billet(s) de 10 CHF")

                         reste -= ((reste/10) * 10)
                       }
                     }
                   }

    // afficher les détails d'un retrait d'argent en termes de nombre de billets. 
               println("Veuillez retirer le montant demandé : ")
               if(billet_choisi_500>0)
               {
                 print(billet_choisi_500)
                 println(" billet(s) de 500 CHF")
               }
               if(billet_choisi_200>0)
               {
                 print(billet_choisi_200)
                 println(" billet(s) de 200 CHF")
               }
               if(billet_choisi_100>0)
               {
                 print(billet_choisi_100)
                 println(" billet(s) de 100 CHF")
               }
               if(billet_choisi_50>0)
               {
                 print(billet_choisi_50)
                 println(" billet(s) de 50 CHF")
               }
               if(billet_choisi_20>0)
               {
                 print(billet_choisi_20)
                 println(" billet(s) de 20 CHF")
               }
               if(billet_choisi_10>0)
               {
                 print(billet_choisi_10)
                 println(" billet(s) de 10 CHF")
               }


               //pécifier le nombre de billets pour chaque (100, 50, 20, 10 EUR)
               }else{

                 while (reste > 0){

                   if(reste / 100 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/100)
                     println(" billet(s) de 100 EUR")

                     var billet_choisi = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi = readLine()

                       if (billet_choisi != "o") {
                          billet_choisi _100 = billet_choisi.toInt
                         if (billet_choisi_100 < 0 || billet_choisi _100 > (reste / 100)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi _100 * 100)
                           billet_choisi ="o"
                         }
                       }
                       else
                       {
                         billet_choisi _100 = reste/100
                         reste -= ((reste/100) * 100)
                       }

                     } while (billet_choisi != "o")

                   }
                   if(reste / 50 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/50)
                     println(" billet(s) de 50 EUR")

                     var billet_choisi  = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi  = readLine()

                       if (billet_choisi  != "o") {
                         billet_choisi _50 = billet_choisi.toInt
                         if (billet_choisi_50 < 0 || billet_choisi_50 > (reste / 50)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_50 * 50)
                           billet_choisi ="o"
                         }
                       }
                       else
                       {
                         billet_choisi_50 = reste/50
                         reste -= ((reste/50) * 50)
                       }

                     } while (billet_choisi  != "o")


                   }
                   if(reste / 20 >= 1){
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/20)
                     println(" billet(s) de 20 EUR")

                     var billet_choisi  = ""
                     do {
                       println(s"Tapez 'o' pour OK ou toute autre valeur inférieure à celle proposée.>")
                       billet_choisi  = readLine()

                       if (billet_choisi  != "o") {
                         billet_choisi_20 = billet_choisi.toInt
                         if (billet_choisi_20 < 0 || billet_choisi_20 > (reste/ 20)) {
                           println("Veuillez entrer une valeur valide.")
                         } else {
                           reste -= (billet_choisi_20 * 20)
                           billet_choisi ="o"
                         }
                       }
                       else
                       {
                         billet_choisi_20 = reste/20
                         reste -= ((reste/20) * 20)
                       }
                     } while (billet_choisi != "o")

                   }
                   if(reste / 10 >= 1) {
                     println(s"Il reste $reste CHF à distribuer.")
                     print("Vous pouvez obtenir au maximum ")
                     print(reste/10)
                     billet_choisi_10 = (reste/10)

                     println(" billet(s) de 10 EUR")

                     reste -= ((reste/10) * 10)
                   }
               }
               }

                //Si des euros sont retirés
                if(retrait_devise == 1) {

                      comptes(id) -= x
                    }else{
                      println("Veuillez retirer le montant demandé : ")

                      if(billet_choisi_100>0)
                      {
                        print(billet_choisi_100)
                        println(" billet(s) de 100 EUR")
                      }
                      if(billet_choisi_50>0)
                      {
                        print(sbillet_choisi_50)
                        println(" billet(s) de 50 EUR")
                      }
                      if(billet_choisi_20>0)
                      {
                        print(billet_choisi-20)
                        println(" billet(s) de 20 EUR")
                      }
                      if(billet_choisi_10>0)
                      {
                        print(billet_choisi_10)
                        println(" billet(s) de 10 EUR")
                      }

                      accounts(id) -= x * 0.95
                    }
                    printf("Votre retrait a été traité, le nouveau montant disponible sur votre compte est : %.2f CHF\n", accounts(id))
                }


        // Operation de dépot

      //Gérer les dépôts en CHF et EUR, modifiant le solde du compte en conséquence. Garantit également que le montant déposé est un multiple de 10 et arrondit le solde du compte à deux décimales après le dépôt.

                def depot(id : Int, comptes : Array[Double]) : Unit = {
                  var devise_depot = 3
                  while (devise_depot != 1 && devise_depot != 2) {
                    println("Indiquez la devise du dépôt : 1) CHF; 2) EUR >")
                    devise_depot = readInt()
                  }
                  var montant_depot = 0;
                  var logique = !true
                  while (!logique){ println("Indiquez le montant du dépôt >")
                    montant_depot = readInt()
                    if(montant_depot % 10 == 0){ logique = true
                    }
                  }
                  if(devise_depot == 1) { accounts(id) += montant_depot
                  }else{ comptes(id) += 0.95 * montant_depot }

                  comptes(id) = math.floor(comptes(id) * 100) / 100
                  println("Votre dépôt a été traité, le nouveau montant disponible sur votre compte est > " + comptes(id))
                }


        // Permet aux utilisateurs de saisir leur ID et de procéder à différentes opérations une fois identifiés

                def main(args: Array[String]): Unit = {

                 var logique = false
                 var choix_operation = 0
                 var essais = 3
                 var vald = 5
                 var choix_operation = 0
                 var correct_id = true
                 var utilisateurs_id = 0
                 var codespin = Array.fill(100)("INTRO1234")
                 var correct_id = true
                 var nbclients = 100
                 var comptes = Array.fill(100) (1200.0)

                  while (correct_id){

                    var logique = false

                    while(!logique && correct_id){
                      println("Entrez votre  code >")
                      utilisateurs_id = readInt()
                      if(utilisateurs_id >= nbclients){ println("Cette identifiant n'est pas valide.")
                        correct_id = false
                      }


                      // Permet à  l'utilisateur de saisir son code PIN et vérifie si ce code correspond à celui enregistré dans le système. L'utilkisateur a 3 tentatives pour saisir le bon code PIN.
                      
                       if(correct_id){

                          essais = 3
                          while (essais > 0 && !logique) {  
                            println("Entrez votre code pin >")
                            var pin_utilisateur = readLine()
                            if (pin_utilisateur == codespin(utilisateurs_id)) {            
                             logique = true
                            }else { essais -= 1
                              if(essais == 0){ println("Trop d'erreurs, abandon de l'identification")
                              }else{ println("Code pin erroné, il vous reste " + essais + " tentatives >")
                              }
                            }
                          }
                        }
                      }

                    // Exécute diverses opérations bancaires comme le dépôt, le retrait, la consultation du solde ou la modification du code PIN, et de terminer la session.

                    choix_operation = 0

                          while (choix_operation != vald && logique) {
                            println("Choisissez votre opération : : \n1) Dépot \n2) Retrait \n3) Consultation du compte \n4) Modification du code PIN \n5) Terminer")
                            choix_operation = readInt()

                            if(choix_operation == 1 && logique){            
                              depot(utilisateurs_id,comptes)

                            }else if(monchoix == 2 && logique){
                              retrait(utilisateurs_id,comptes)

                            }else if(choix_operation == 5){

                              println("Fin des opérations, n'oubliez pas de récupérer votre carte.") }else if(choix_operation == 3 && logique){

                              println("Le montant disponible sur votre compte est : " + comptes(utilisateurs_id))
                            }else if(choix_operation == 4 && logique){
                              changepin(utilisateurs_id,codespin)
                            }
                          }
                        }
                      }
                    }
