//Assignment: Gabriel Mathieu Borfiga_996426_assignsubmission_file


// Le bancomat : dépôt d’argent, retrait d’argent et consultationde l’état du compte en banque:

import io.StdIn._

object Main {
  val nbclients = 100
  val comptes: Array[Double] = Array.fill(nbclients)(1200.0)
  val codespin: Array[String] = Array.fill(nbclients)("INTRO1234")

  
  def main(args: Array[String]): Unit = {
    var choix = 0
    var acceuil = "\nChoisissez votre opération :\n" +
      "1) Dépôt\n" +
      "2) Retrait\n" +
      "3) Consultation du compte\n" +
      "4) Changement du code pin\n" +
      "5) Terminer\n" +
      "Votre choix: "
    var clientId = 0

    while (true) {
      clientId = getIdentifiant()
      var tentativepin = 3
      var moncodepinestjuste = false

      while (!moncodepinestjuste && tentativepin > 0) {
        print("\nSaisissez votre code pin > ")
        val codepin = readLine()
        if (codepin == codespin(clientId)) {
          moncodepinestjuste = true
        } else {
          tentativepin -= 1
          if (tentativepin > 0){
            println("\nCode pin erroné, il vous reste " + tentativepin + " tentatives >")
          }
        }
      }

      if (!moncodepinestjuste) {
        println("\nTrop d’erreurs, abandon de l’identification")
      } else {
        while (choix != 5){
          print(acceuil)
          choix = readInt()
          if (choix==1) {
            depot(clientId, comptes)
            choix = 0
          }else if (choix==2){
            retrait(clientId, comptes)
            choix = 0
          }else if (choix==3){
            println("\nLe montant disponible sur votre compte est de : " + comptes(clientId) + " CHF\n\n")
            choix = 0
          }else if (choix==4) {
            changepin(clientId, codespin)
            choix = 0
          }
        }
        if (choix==5){
            println("\nFin des opérations, n’oubliez pas de récupérer votre carte.")
          }
          choix = 0
      }
    }
  }

 def getIdentifiant(): Int = {
    print("\nSaisissez votre code identifiant > ")
    var id = readInt()
    if (id >= nbclients) {
      println("Cet identifiant n’est pas valable.\n")
      sys.exit(1)
    }
    id
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    println("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      val devise = readInt()
      println("\nIndiquez le montant du dépôt >")
      var montant = readInt()

      while (montant % 10 != 0) {
        println("\nLe montant doit être un multiple de 10")
        println("\nIndiquez le montant du dépôt >")
        montant = readInt()
      }

      if (devise == 2) { // EUR
        comptes(id) += montant * 0.95 // Conversion rate
      } else { // CHF
        comptes(id) += montant
      }
      println(s"\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : ${comptes(id)} CHF\n")
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {

    // Initial setup
    var deviseR = 0
    var retrait = 0
    var coupuresCHF = 0
    var billets500 = 0
    var reste500 = 0
    var billets200 = 0
    var reste200 = 0
    var billets100 = 0
    var reste100 = 0
    var billets50 = 0
    var reste50 = 0
    var billets20 = 0
    var reste20 = 0
    var billets10 = 0
    var reste10 = 0
    var valeurproposee = ""
    var plafondretrait = comptes(id) * 0.1

  while (deviseR < 1 || deviseR > 2) {
    print ("\n"+"Indiquez la devise du retrait : 1) CHF ; 2) EUR >") 
    deviseR = readInt()
  }
  print ("\n"+"Indiquez le montant du retrait >")
  retrait = readInt()

    while (retrait > plafondretrait || retrait % 10 != 0 || retrait < 10){

    while(retrait < 10){
      println ("\n"+"Le montant demandé doit être supérieur à 10") 
      print ("\n"+"Indiquez le montant du retrait >")
      retrait = readInt()
    }
    while (retrait % 10 != 0) {
      println ("\n"+"Le montant doit être un multiple de 10")
      print ("\n"+"Indiquez le montant du retrait >")
      retrait = readInt()
    }
    while (retrait > plafondretrait) {
      println ("\n"+"Votre plafond de retrait autorisé est de :" + plafondretrait)
      print ("\n"+"Indiquez le montant du retrait >")
      retrait = readInt()  
    }
  }
  if (deviseR == 1){
    if (retrait >= 200){
    while (coupuresCHF != 2 && coupuresCHF != 1){
    println ("\n"+"En 1) grosses coupures, 2) petites coupures >")
    coupuresCHF = readInt()
    }
    // Grosses coupures CHF
    if (coupuresCHF == 1) { 
      billets500 = retrait / 500
      reste500 = retrait % 500 
      billets200 = reste500 / 200
      reste200 = reste500 % 200 
      billets100 = reste200 / 100
      reste100 = reste200 % 100 
      billets50 = reste100 / 50
      reste50 = reste100 % 50 
      billets20 = reste50 / 20
      reste20 = reste50 % 20
      billets10 = reste20 / 10
      reste10 = reste20 % 10 
      if (billets500 > 0){
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets500 + " billet(s) de 500 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets500) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste500 = retrait - 500 * billets500
            println ("\n"+"il reste " + reste500 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets500) {
            billets500 = valeurproposee.toInt
            reste500 = retrait - 500 * billets500
        }
      }
      billets200 = reste500 / 200
      if (billets200 > 0){
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets200 + " billet(s) de 200 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets200) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste200 = retrait - 200 * billets200 - 500 * billets500
            println ("\n"+"il reste " + reste200 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets200) {
            billets200 = valeurproposee.toInt
            reste200 = retrait - 200 * billets200 - 500 * billets500
        }
      }
      billets100 = reste200 / 100
      if (billets100 > 0){
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets100 + " billet(s) de 100 CHF") 
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets100) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste100 = retrait - 100 * billets100 - 200 * billets200 - 500 * billets500
            println ("\n"+"il reste " + reste100 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets100) {
            billets100 = valeurproposee.toInt
            reste100 = retrait - 100 * billets100 - 200 * billets200 - 500 * billets500
        }
      }
      billets50 = reste100 / 50
      if (billets50 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets50 + " billet(s) de 50 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets50) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste50 = retrait - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
            println ("\n"+"il reste " + reste50 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets50) {
            billets50 = valeurproposee.toInt
            reste50 = retrait - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
        }
      }
      billets20 = reste50 / 20
      if (billets20 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets20 + " billet(s) de 20 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets20) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
            println ("\n"+"il reste " + reste20 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets20) {
            billets20 = valeurproposee.toInt
            reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
        }
      }
      billets10 = reste20 / 10
      if (billets10 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets10 + " billet(s) de 10 CHF")

      }
      println ("\n"+"Veuillez retirer la somme demandée : ")
      if (billets500 > 0){
      println (billets500 + "billet(s) de 500 CHF")
      }
      if (billets200 > 0){
      println (billets200 + "billet(s) de 200 CHF")
      }
      if (billets100 > 0){
      println (billets100 + "billet(s) de 100 CHF")
      }
      if (billets50 > 0){
      println (billets50 + "billet(s) de 50 CHF")
      }
      if (billets20 > 0){
      println (billets20 + "billet(s) de 20 CHF")
      }
      if (billets10 > 0){
      println (billets10 + "billet(s) de 10 CHF")
      }
      comptes(id) = comptes(id) - retrait
      printf ("\n"+"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f", comptes(id))
      println(" CHF\n")
    
    // Petites coupures CHF
    }else if (coupuresCHF == 2){ 
      billets100 = retrait / 100
      reste100 = reste200 % 100 
      billets50 = reste100 / 50
      reste50 = reste100 % 50 
      billets20 = reste50 / 20
      reste20 = reste50 % 20
      billets10 = reste20 / 10
      reste10 = reste20 % 10 

      if (billets100 > 0){
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets100 + " billet(s) de 100 CHF") 
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets100) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste100 = retrait - 100 * billets100
            println ("\n"+"il reste " + reste100 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets100) {
            billets100 = valeurproposee.toInt
            reste100 = retrait - 100 * billets100
        }
      }
      billets50 = reste100 / 50
      if (billets50 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets50 + " billet(s) de 50 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets50) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste50 = retrait - 50 * billets50 - 100 * billets100
            println ("\n"+"il reste " + reste50 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets50) {
            billets50 = valeurproposee.toInt
            reste50 = retrait - 50 * billets50 - 100 * billets100
        }
      }
      billets20 = reste50 / 20
      if (billets20 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets20 + " billet(s) de 20 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets20) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100
            println ("\n"+"il reste " + reste20 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets20) {
            billets20 = valeurproposee.toInt
            reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100
        }
      }
      billets10 = reste20 / 10
      if (billets10 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets10 + " billet(s) de 10 CHF")
      }
      println ("\n"+"Veuillez retirer la somme demandée : ")
      if (billets100 > 0){
      println (billets100 + "billet(s) de 100 CHF")
      }
      if (billets50 > 0){
      println (billets50 + "billet(s) de 50 CHF")
      }
      if (billets20 > 0){
      println (billets20 + "billet(s) de 20 CHF")
      }
      if (billets10 > 0){
      println (billets10 + "billet(s) de 10 CHF")
      }
      comptes(id) = comptes(id) - retrait
      printf ("\n"+"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f", comptes(id))
      println(" CHF\n")
    }
    //Devise CHF retrait plus petit que 200
    }else if (retrait < 200){
      billets100 = retrait / 100
      reste100 = retrait % 100 
      billets50 = reste100 / 50
      reste50 = reste100 % 50 
      billets20 = reste50 / 20
      reste20 = reste50 % 20
      billets10 = reste20 / 10
      reste10 = reste20 % 10 
      if (billets100 > 0){
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets100 + " billet(s) de 100 CHF") 
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets100) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste100 = retrait - 100 * billets100
            println ("\n"+"il reste " + reste100 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets100) {
            billets100 = valeurproposee.toInt
            reste100 = retrait - 100 * billets100
          }
        }
      billets50 = reste100 / 50
      if (billets50 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets50 + " billet(s) de 50 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets50) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste50 = retrait - 50 * billets50 - 100 * billets100
            println ("\n"+"il reste " + reste50 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets50) {
            billets50 = valeurproposee.toInt
            reste50 = retrait - 50 * billets50 - 100 * billets100
        }
      }
      billets20 = reste50 / 20
      if (billets20 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets20 + " billet(s) de 20 CHF")
        println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        valeurproposee = readLine()
          while (valeurproposee != "o" && valeurproposee.toInt >= billets20) {
            println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            valeurproposee = readLine()
          }         
          if (valeurproposee == "o") {
            reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100
            println ("\n"+"il reste " + reste20 + " à distribuer")
          }
        else if (valeurproposee.toInt <= billets20) {
            billets20 = valeurproposee.toInt
            reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100
        }
      }
      billets10 = reste20 / 10
      if (billets10 > 0) {
        println ("\n"+"Vous pouvez obtenir au maximum: " + billets10 + " billet(s) de 10 CHF")
      }
      println ("Veuillez retirer la somme demandée : ")
      if (billets100 > 0){
      println (billets100 + "billet(s) de 100 CHF")
      }
      if (billets50 > 0){
      println (billets50 + "billet(s) de 50 CHF")
      }
      if (billets20 > 0){
      println (billets20 + "billet(s) de 20 CHF")
      }
      if (billets10 > 0){
      println (billets10 + "billet(s) de 10 CHF")
      }
      comptes(id) = comptes(id) - retrait
      printf ("\n"+"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f", comptes(id))
      println(" CHF\n")
    
    }
  //Devise EUR, pas de grosses ou petites coupures 
  }else if (deviseR == 2){
    billets500 = retrait / 500
    reste500 = retrait % 500 
    billets200 = reste500 / 200
    reste200 = reste500 % 200 
    billets100 = reste200 / 100
    reste100 = reste200 % 100 
    billets50 = reste100 / 50
    reste50 = reste100 % 50 
    billets20 = reste50 / 20
    reste20 = reste50 % 20
    billets10 = reste20 / 10
    reste10 = reste20 % 10 
    if (billets500 > 0){
      println ("\n"+"Vous pouvez obtenir au maximum: " + billets500 + " billet(s) de 500 EUR")
      println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      valeurproposee = readLine()
        while (valeurproposee != "o" && valeurproposee.toInt >= billets500) {
          println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          valeurproposee = readLine()
        }         
        if (valeurproposee == "o") {
          reste500 = retrait - 500 * billets500
          println ("\n"+"il reste " + reste500 + " à distribuer")
        }
      else if (valeurproposee.toInt <= billets500) {
          billets500 = valeurproposee.toInt
          reste500 = retrait - 500 * billets500
        }
    }
    billets200 = reste500 / 200
    if (billets200 > 0){
      println ("\n"+"Vous pouvez obtenir au maximum: " + billets200 + " billet(s) de 200 EUR")
      println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      valeurproposee = readLine()
        while (valeurproposee != "o" && valeurproposee.toInt >= billets200) {
          println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          valeurproposee = readLine()
        }         
      if (valeurproposee == "o") {
        reste200 = retrait - 200 * billets200 - 500 * billets500
        println ("\n"+"il reste " + reste200 + " à distribuer")
      }else if (valeurproposee.toInt <= billets200) {
          billets200 = valeurproposee.toInt
          reste200 = retrait - 200 * billets200 - 500 * billets500
      }
    }
    billets100 = reste200 / 100
    if (billets100 > 0){
      println ("\n"+"Vous pouvez obtenir au maximum: " + billets100 + " billet(s) de 100 EUR") 
      println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      valeurproposee = readLine()
        while (valeurproposee != "o" && valeurproposee.toInt >= billets100) {
          println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          valeurproposee = readLine()
        }         
      if (valeurproposee == "o") {
        reste100 = retrait - 100 * billets100 - 200 * billets200 - 500 * billets500
        println ("\n"+"il reste " + reste100 + " à distribuer")
      }else if (valeurproposee.toInt <= billets100) {
          billets100 = valeurproposee.toInt
          reste100 = retrait - 100 * billets100 - 200 * billets200 - 500 * billets500
      }
    }
    billets50 = reste100 / 50
    if (billets50 > 0) {
      println ("\n"+"Vous pouvez obtenir au maximum: " + billets50 + " billet(s) de 50 EUR")
      println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      valeurproposee = readLine()
        while (valeurproposee != "o" && valeurproposee.toInt >= billets50) {
          println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          valeurproposee = readLine()
        }         
      if (valeurproposee == "o") {
        reste50 = retrait - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
        println ("\n"+"il reste " + reste50 + " à distribuer")
      }else if (valeurproposee.toInt <= billets50) {
          billets50 = valeurproposee.toInt
          reste50 = retrait - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
      }
    }
    billets20 = reste50 / 20
    if (billets20 > 0) {
      println ("\n"+"Vous pouvez obtenir au maximum: " + billets20 + " billet(s) de 20 EUR")
      println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      valeurproposee = readLine()
        while (valeurproposee != "o" && valeurproposee.toInt >= billets20) {
          println ("\n"+"Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          valeurproposee = readLine()
        }         
      if (valeurproposee == "o") {
        reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
        println ("\n"+"il reste " + reste20 + " à distribuer")
      }else if (valeurproposee.toInt <= billets20) {
          billets20 = valeurproposee.toInt
          reste20 = retrait - 20 * billets20 - 50 * billets50 - 100 * billets100 - 200 * billets200 - 500 * billets500
      }
    }
    billets10 = reste20 / 10
    if (billets10 > 0) {
      println ("\n"+"Vous pouvez obtenir au maximum: " + billets10 + " billet(s) de 10 EUR")
    }
    println ("\n"+"Veuillez retirer la somme demandée : ")
    if (billets500 > 0){
    println (billets500 + "billet(s) de 500 EUR")
    }
    if (billets200 > 0){
    println (billets200 + "billet(s) de 200 EUR")
    }
    if (billets100 > 0){
    println (billets100 + "billet(s) de 100 EUR")
    }
    if (billets50 > 0){
    println (billets50 + "billet(s) de 50 EUR")
    }
    if (billets20 > 0){
    println (billets20 + "billet(s) de 20 EUR")
    }
    if (billets10 > 0){
    println (billets10 + "billet(s) de 10 EUR")
    }
    comptes(id) = comptes(id) - retrait * 0.95 //taux fixe 1eur = 0.95 chf
    printf ("\n"+"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f", comptes(id))
    println(" CHF\n")
  
  }
}

  
def changepin(id: Int, codespin: Array[String]): Unit = {
      
      println("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      var newPin = readLine()
  
      while (newPin.length < 8) {
        println("\nVotre code pin ne contient pas au moins 8 caractères")
        println("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        newPin = readLine()
      }
  
      codespin(id) = newPin
      println("\nVotre code pin a été modifié avec succès.")
    }
}
