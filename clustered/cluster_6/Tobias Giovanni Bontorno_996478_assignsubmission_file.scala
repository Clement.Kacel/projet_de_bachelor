//Assignment: Tobias Giovanni Bontorno_996478_assignsubmission_file


import scala.io.StdIn._

object Main {

  val EURaCHF: Double = 0.95
  val CHFaEUR: Double = 1.05
  val clients: Int = 100
  

def main(args: Array[String]): Unit = {

  var continuerMain = true

  var comptes: Array[Double] = Array.fill(100)(1200)
  var codespin: Array[String] = Array.fill(100)("INTRO1234")

  while(continuerMain){
    var ID_name = 1000
    while(ID_name>=clients){
      println("Saisissez votre code identifiant >")
      ID_name = readInt().toInt
      if(ID_name>=clients){
        println("Cet identifiant n’est pas valable.")
      }
    }
    var argent = comptes(ID_name)
    var MDP = codespin(ID_name)
    var continuer = true
    var pinCorrect = false
    
    while (continuer) {

      if(pinCorrect == false){
        pinCorrect = checkPin(MDP)
      }
      if(pinCorrect){
      println("Choisissez votre opération :")
      println("  1) Dépôt")
      println("  2) Retrait")
      println("  3) Consultation du compte")
      println("  4) Changement du code pin")
      println("  5) Terminer")
      println("Votre choix :")
      var instruction = readLine()
  
      if(instruction == "1" || instruction == "2" || instruction == "3"|| instruction == "4" || instruction == "5"){ 
  
        
  
        
  
          if(instruction == "1"){
            argent = Depot(argent)
          }
  
          if(instruction == "3"){
            printf("Le montant disponible sur votre compte est de: %.2f \n", argent)
          }
  
          if(instruction == "2"){
            argent = Retrait(argent)
  
        }

          if(instruction == "4"){
            MDP = changePin()
          }

          if(instruction == "5"){
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            continuer = false
          }
          
            } 
        } else {
    println("Trop d’erreurs, abandon de l’identification")
    continuer = false
        }
  
      
    }
    comptes(ID_name) = argent
    codespin(ID_name) = MDP
    

  }
  }





  def changePin(): String ={
    var MDP = ""
    while(MDP.length < 8){
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères)")
      MDP = readLine()
    }

    MDP
  }

  

  def checkPin(MDP: String): Boolean = {
    var pinCorrect = false
    var nombreEssay = 0
    var pin = ""
    println("Saisissez votre code pin >")
    pin = scala.io.StdIn.readLine()

    while (!pinCorrect && nombreEssay < 2) {
      nombreEssay = nombreEssay + 1
      if (pin == MDP) {
        pinCorrect = true
      } else {
        println("Code pin érroné, il vous reste " + (3 - nombreEssay) + " tentatives >")
        pin = scala.io.StdIn.readLine()
      }
    }
    if(pin == MDP){
      pinCorrect = true
    }
    
    pinCorrect
  }

  def Depot(argent: Double): Double = {
    var moula = argent
    println("Indiquer la devise du dépôt: 1) CHF ; 2) EUR >")
      var devise = readLine()

      while(devise != "1" && devise != "2"){ 
        println("Indiquer la devise du dépôt: 1) CHF ; 2) EUR >")
        devise = readLine()
      }

      println("Indiquer le montant du dépôt >")
      var montant = readLine().toInt

      while(montant % 10 != 0){
        println("Le montant n'est pas un multiple de 10.")
        println("Indiquer le montant du dépôt >")
        montant = readLine().toInt
      }

      var montant2 = montant.toDouble
      if(devise == "2"){
        montant2 = montant2 * EURaCHF
      }
      moula = argent + montant2
      printf("Votre dépôt a été pris en compte, le montant disponible sur votre compte est de: %.2f \n", moula)

    moula
  }

  def Retrait(argent: Double): Double ={
    var moula = argent
    
    println("Indiquer la devise du retrait: 1) CHF ; 2) EUR >")
      var devise = readLine()
      while(devise != "1" && devise != "2"){
        println("Indiquer la devise du retrait: 1) CHF ; 2) EUR >")
        devise = readLine()
      }

      println("Indiquer le montant du retrait >")
      var montant = readLine().toInt
      var montantMax = moula * 0.1

           if(devise == "1"){ 
              while(montant.toDouble > montantMax || montant % 10 != 0){
                if(montant % 10 != 0){
                  println("Le montant n'est pas un multiple de 10.")
                  println("Indiquer le montant du dépôt >")
                } else {
                  printf("Votre plafond de retrait est de: %.2f \n CHF", montantMax)
                  println("Indiquer le montant du dépôt >")
                }
                montant = readLine().toInt
              }
           } else {
             while((montant.toDouble*EURaCHF) > montantMax || montant % 10 != 0){
               if(montant % 10 != 0){
                 println("Le montant n'est pas un multiple de 10.")
                 println("Indiquer le montant du dépôt >")
               } else {
                 printf("Votre plafond de retrait est de: %.2f \n EUR", montantMax*CHFaEUR)
                 println("Indiquer le montant du dépôt >")
               }
               montant = readLine().toInt
                }
          }

            if(devise == "1"){
              moula = moula - montant.toDouble
              var Coupure = "0"
              if(montant >=200){
                println("En 1) grosses coupures 2) petites coupures")
                Coupure =  readLine()
                while(Coupure != "1" && Coupure != "2"){
                  println("En 1) grosses coupures 2) petites coupures")
                  Coupure =  readLine()
                }}
                if(Coupure == "1"){
                var montant_apres_500 = montant
                var nombre_de_500: Int= 0
                while(montant_apres_500 >= 500){
                  nombre_de_500 = nombre_de_500 + 1
                  montant_apres_500 = montant_apres_500 - 500
                }

                if(nombre_de_500>0){
                  var choosen = false

                  while(!choosen){
                    println("Il vous restes "+ montant +" à distribuer")
                    println("Vous pouvez obtenir au maximum "+ nombre_de_500 +" billet(s) de 500 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    var choix = readLine()
                    if(choix == "o"){
                      choosen = true
                        montant = montant - 500*nombre_de_500
                      }
                    if(choix.matches("\\d+")){
                      if(choix.toInt < nombre_de_500){
                        choosen = true
                        montant = montant - 500*choix.toInt
                        nombre_de_500 = choix.toInt
                      }
                    }
                    }
                  }
                var montant_apres_200 = montant
                var nombre_de_200: Int= 0
                while(montant_apres_200 >= 200){
                  nombre_de_200 = nombre_de_200 + 1
                  montant_apres_200 = montant_apres_200 - 200
                }

                if(nombre_de_200>0){
                  var choosen = false

                  while(!choosen){
                    println("Il vous restes "+ montant +" à distribuer")
                    println("Vous pouvez obtenir au maximum "+ nombre_de_200 +" billet(s) de 200 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    var choix = readLine()
                    if(choix == "o"){
                      choosen = true
                        montant = montant - 200*nombre_de_200
                      }
                    if(choix.matches("\\d+")){
                      if(choix.toInt < nombre_de_200){
                        choosen = true
                        montant = montant - 200*choix.toInt
                        nombre_de_200 = choix.toInt
                      }
                    }
                    }
                  }

                var montant_apres_100 = montant
                        var nombre_de_100: Int= 0
                        while(montant_apres_100 >= 100){
                          nombre_de_100 = nombre_de_100 + 1
                          montant_apres_100 = montant_apres_100 - 100
                        }

                        if(nombre_de_100>0){
                          var choosen = false

                          while(!choosen){
                            println("Il vous restes "+ montant +" à distribuer")
                            println("Vous pouvez obtenir au maximum "+ nombre_de_100 +" billet(s) de 100 CHF")
                            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                            var choix = readLine()
                            if(choix == "o"){
                              choosen = true
                                montant = montant - 100*nombre_de_100
                              }
                            if(choix.matches("\\d+")){
                              if(choix.toInt < nombre_de_100){
                                choosen = true
                                montant = montant - 100*choix.toInt
                                nombre_de_100 = choix.toInt
                              }
                            }
                            }
                          }


                var montant_apres_50 = montant
                var nombre_de_50: Int= 0
                while(montant_apres_50 >= 50){
                  nombre_de_50 = nombre_de_50 + 1
                  montant_apres_50 = montant_apres_50 - 50
                }

                if(nombre_de_50>0){
                  var choosen = false

                  while(!choosen){
                    println("Il vous restes "+ montant +" à distribuer")
                    println("Vous pouvez obtenir au maximum "+ nombre_de_50 +" billet(s) de 50 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    var choix = readLine()
                    if(choix == "o"){
                      choosen = true
                        montant = montant - 50*nombre_de_50
                      }
                    if(choix.matches("\\d+")){
                      if(choix.toInt < nombre_de_50){
                        choosen = true
                        montant = montant - 50*choix.toInt
                        nombre_de_50 = choix.toInt
                      }
                    }
                    }
                  }

                var montant_apres_20 = montant
                var nombre_de_20: Int= 0
                while(montant_apres_20 >= 20){
                  nombre_de_20 = nombre_de_20 + 1
                  montant_apres_20 = montant_apres_20 - 20
                }

                if(nombre_de_20>0){
                  var choosen = false

                  while(!choosen){
                    println("Il vous restes "+ montant +" EUR à distribuer")
                    println("Vous pouvez obtenir au maximum "+ nombre_de_20 +" billet(s) de 20 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    var choix = readLine()
                    if(choix == "o"){
                      choosen = true
                        montant = montant - 20*nombre_de_20
                      }
                    if(choix.matches("\\d+")){ 
                      if(choix.toInt < nombre_de_20){
                        choosen = true
                        montant = montant - 20*choix.toInt
                        nombre_de_20 = choix.toInt
                      }
                    }
                    }
                  }

                  var montant_apres_10 = montant
                  var nombre_de_10: Int= 0
                  while(montant_apres_10 >= 10){
                    nombre_de_10 = nombre_de_10 + 1
                    montant_apres_10 = montant_apres_10 - 10
                  }

                  if(nombre_de_10>0){
                    println("Vous pouvez aller obtenir "+ nombre_de_10 +" billet(s) de 10 CHF")
                        }

                  println("Veuillez retirer la somme demandée :")
                  if(nombre_de_500>0){
                    println(nombre_de_200 +" billet(s) de 500 CHF")
                  }
                  if(nombre_de_200>0){
                    println(nombre_de_200 +" billet(s) de 200 CHF")
                  }
                  if(nombre_de_100>0){
                    println(nombre_de_100 +" billet(s) de 100 CHF")
                  }
                  if(nombre_de_50>0){
                    println(nombre_de_50 +" billet(s) de 50 CHF")
                  }
                  if(nombre_de_20>0){
                    println(nombre_de_20 +" billet(s) de 20 CHF")
                  }
                  if(nombre_de_10>0){
                    println(nombre_de_10 +" billet(s) de 10 CHF")
                  }

              } else {
                var montant_apres_100 = montant
                        var nombre_de_100: Int= 0
                        while(montant_apres_100 >= 100){
                          nombre_de_100 = nombre_de_100 + 1
                          montant_apres_100 = montant_apres_100 - 100
                        }

                        if(nombre_de_100>0){
                          var choosen = false

                          while(!choosen){
                            println("Il vous restes "+ montant +" à distribuer")
                            println("Vous pouvez obtenir au maximum "+ nombre_de_100 +" billet(s) de 100 CHF")
                            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                            var choix = readLine()
                            if(choix == "o"){
                              choosen = true
                                montant = montant - 100*nombre_de_100
                              }
                            if(choix.matches("\\d+")){ 
                              if(choix.toInt < nombre_de_100){
                                choosen = true
                                montant = montant - 100*choix.toInt
                                nombre_de_100 = choix.toInt
                              }
                            }
                            }
                          }


                var montant_apres_50 = montant
                var nombre_de_50: Int= 0
                while(montant_apres_50 >= 50){
                  nombre_de_50 = nombre_de_50 + 1
                  montant_apres_50 = montant_apres_50 - 50
                }

                if(nombre_de_50>0){
                  var choosen = false

                  while(!choosen){
                    println("Il vous restes "+ montant +" à distribuer")
                    println("Vous pouvez obtenir au maximum "+ nombre_de_50 +" billet(s) de 50 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    var choix = readLine()
                    if(choix == "o"){
                      choosen = true
                        montant = montant - 50*nombre_de_50
                      }
                    if(choix.matches("\\d+")){ 
                      if(choix.toInt < nombre_de_50){
                        choosen = true
                        montant = montant - 50*choix.toInt
                        nombre_de_50 = choix.toInt
                      }
                    }
                    }
                  }

                var montant_apres_20 = montant
                var nombre_de_20: Int= 0
                while(montant_apres_20 >= 20){
                  nombre_de_20 = nombre_de_20 + 1
                  montant_apres_20 = montant_apres_20 - 20
                }

                if(nombre_de_20>0){
                  var choosen = false

                  while(!choosen){
                    println("Il vous restes "+ montant +" EUR à distribuer")
                    println("Vous pouvez obtenir au maximum "+ nombre_de_20 +" billet(s) de 20 CHF")
                    println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                    var choix = readLine()
                    if(choix == "o"){
                      choosen = true
                        montant = montant - 20*nombre_de_20
                      }
                    if(choix.matches("\\d+")){ 
                      if(choix.toInt < nombre_de_20){
                        choosen = true
                        montant = montant - 20*choix.toInt
                        nombre_de_20 = choix.toInt
                      }
                    }
                    }
                  }

                  var montant_apres_10 = montant
                  var nombre_de_10: Int= 0
                  while(montant_apres_10 >= 10){
                    nombre_de_10 = nombre_de_10 + 1
                    montant_apres_10 = montant_apres_10 - 10
                  }

                  if(nombre_de_10>0){
                    println("Vous pouvez aller obtenir "+ nombre_de_10 +" billet(s) de 10 CHF")
                        }

                  println("Veuillez retirer la somme demandée :")
                  if(nombre_de_100>0){
                    println(nombre_de_100 +" billet(s) de 100 CHF")
                  }
                  if(nombre_de_50>0){
                    println(nombre_de_50 +" billet(s) de 50 CHF")
                  }
                  if(nombre_de_20>0){
                    println(nombre_de_20 +" billet(s) de 20 CHF")
                  }
                  if(nombre_de_10>0){
                    println(nombre_de_10 +" billet(s) de 10 CHF")
                  }
          }


        }else{
            moula = moula - montant.toDouble*EURaCHF

            var montant_apres_100 = montant
            var nombre_de_100: Int= 0
            while(montant_apres_100 >= 100){
              nombre_de_100 = nombre_de_100 + 1
              montant_apres_100 = montant_apres_100 - 100
            }

            if(nombre_de_100>0){
              var choosen = false

              while(!choosen){
                println("Il vous restes "+ montant +" à distribuer")
                println("Vous pouvez obtenir au maximum "+ nombre_de_100 +" billet(s) de 100 EUR")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                var choix = readLine()
                if(choix == "o"){
                  choosen = true
                    montant = montant - 100*nombre_de_100
                  }
                if(choix.matches("\\d+")){
                  if(choix.toInt < nombre_de_100){
                    choosen = true
                    montant = montant - 100*choix.toInt
                    nombre_de_100 = choix.toInt
                  }
                }
                }
              }


    var montant_apres_50 = montant
    var nombre_de_50: Int= 0
    while(montant_apres_50 >= 50){
      nombre_de_50 = nombre_de_50 + 1
      montant_apres_50 = montant_apres_50 - 50
    }

    if(nombre_de_50>0){
      var choosen = false

      while(!choosen){
        println("Il vous restes "+ montant +" à distribuer")
        println("Vous pouvez obtenir au maximum "+ nombre_de_50 +" billet(s) de 50 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        var choix = readLine()
        if(choix == "o"){
          choosen = true
            montant = montant - 50*nombre_de_50
          }
        if(choix.matches("\\d+")){
          if(choix.toInt < nombre_de_50){
            choosen = true
            montant = montant - 50*choix.toInt
            nombre_de_50 = choix.toInt
          }
        }
        }
      }

    var montant_apres_20 = montant
    var nombre_de_20: Int= 0
    while(montant_apres_20 >= 20){
      nombre_de_20 = nombre_de_20 + 1
      montant_apres_20 = montant_apres_20 - 20
    }

    if(nombre_de_20>0){
      var choosen = false

      while(!choosen){
        println("Il vous restes "+ montant +" EUR à distribuer")
        println("Vous pouvez obtenir au maximum "+ nombre_de_20 +" billet(s) de 20 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        var choix = readLine()
        if(choix == "o"){
          choosen = true
            montant = montant - 20*nombre_de_20
          }
        if(choix.matches("\\d+")){ 
          if(choix.toInt < nombre_de_20){
            choosen = true
            montant = montant - 20*choix.toInt
            nombre_de_20 = choix.toInt
          }
        }
        }
      }

      var montant_apres_10 = montant
      var nombre_de_10: Int= 0
      while(montant_apres_10 >= 10){
        nombre_de_10 = nombre_de_10 + 1
        montant_apres_10 = montant_apres_10 - 10
      }

      if(nombre_de_10>0){
        println("Vous pouvez et allez obtenir "+ nombre_de_10 +" billet(s) de 10 EUR")
            }

      println("Veuillez retirer la somme demandée :")
      if(nombre_de_100>0){
        println(nombre_de_100 +" billet(s) de 100 EUR")
      }
      if(nombre_de_50>0){
        println(nombre_de_50 +" billet(s) de 50 EUR")
      }
      if(nombre_de_20>0){
        println(nombre_de_20 +" billet(s) de 20 EUR")
      }
      if(nombre_de_10>0){
        println(nombre_de_10 +" billet(s) de 10 EUR")
      }
          }

    moula
  }



}
