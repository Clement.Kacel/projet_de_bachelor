//Assignment: Alexandre Gaston Darioli_996397_assignsubmission_file

import scala.io.StdIn._
 
object Main {
  def depot(identifiant : Int, montantcompte : Array[Double]) : Unit = {
    var tauxchange1 = 0.95
    var depot1 = 0.0
    var devise = readLine("\nIndiquer la devise du dépôt: CHF ; EUR \nVotre choix : ").toString 
    var depot = readLine("\nIndiquez le montant du dépôt : ").toInt
    while (depot % 10 != 0) {
        println("\nLe montant doit être un multiple de 10.")
        depot = readLine("\nIndiquez le montant du dépôt : ").toInt
    }
    if (devise == "CHF") {
      montantcompte(identifiant) = montantcompte(identifiant) + depot
      printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantcompte(identifiant))
      print("CHF\n")
    } 
    else if (devise == "EUR") {
      depot1 = tauxchange1 * depot.toDouble
      montantcompte(identifiant) = montantcompte(identifiant) + depot1
      printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantcompte(identifiant))
      print("CHF\n")
    }
  }
  
  def retrait(identifiant : Int, montantcompte : Array[Double]) : Unit = {
    var coupures = 0
    var b500ret = 0
    var b200ret = 0
    var b100ret = 0
    var b50ret = 0
    var b20ret = 0
    var b10ret = 0
    var retrait1 = 0.0
    var tauxchange2 = 0.95
      var devise = readLine("\nIndiquer la devise du retrait: CHF ; EUR \nVotre choix : ").toString
      while (devise != "CHF" && devise != "EUR") {
        devise = readLine("\nIndiquer la devise du retrait: CHF ; EUR \nVotre choix : ").toString
      }
      var retrait = readLine("\nIndiquer le montant du retrait : ").toInt
      while (retrait > 0.1 * montantcompte(identifiant) || retrait % 10 != 0) {
        if (retrait >= 0.1 * montantcompte(identifiant)) {
          println("\nVotre plafond de retrait autorisé est de : " + 0.1 * montantcompte(identifiant))
          retrait = readLine("\nIndiquez le montant du retrait : ").toInt
        }
        else if (retrait % 10 != 0) {
          println("\nLe montant doit être un multiple de 10.")
          retrait = readLine("\nIndiquez le montant du retrait : ").toInt
        }
      }  
      if (devise == "CHF") {
        if (retrait < 200) {
          coupures = 2
        }
        else if (retrait >= 200) {
          coupures = readLine("\nEn 1) grosses coupures ; 2) petites coupures \nVotre choix : ").toInt
          while (coupures < 1 || coupures > 2) {
            coupures = readLine("\nEn 1) grosses coupures ; 2) petites coupures \nVotre choix : ").toInt
          }
        }
        if (coupures == 1) { 
          var b500disp = retrait / 500
          if (b500disp > 0) {
            var b500retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b500disp + " billet(s) de 500 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b500retInput == "o") {
              b500ret = b500disp
            }
            else {
              b500ret = (b500retInput).toInt
            }
            retrait = retrait - 500 * b500ret
          }
          var b200disp = retrait / 200
          if (b200disp > 0) {
            var b200retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b200disp + " billet(s) de 200 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b200retInput == "o") {
              b200ret = b200disp
            }
            else {
              b200ret = (b200retInput).toInt
            }
            retrait = retrait - 200 * b200ret
          }
          var b100disp = retrait / 100
          if (b100disp > 0) {
            var b100retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b100disp + " billet(s) de 100 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b100retInput == "o") {
              b100ret = b100disp
            }
            else {
              b100ret = (b100retInput).toInt
            }
            retrait = retrait - 100 * b100ret
          }
          var b50disp = retrait / 50
          if (b50disp > 0) {
            var b50retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b50disp + " billet(s) de 50 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b50retInput == "o") {
              b50ret = b50disp
            }
            else {
              b50ret = (b50retInput).toInt
            }
            retrait = retrait - 50 * b50ret
          }
          var b20disp = retrait / 20
          if (b20disp > 0) {
            var b20retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b20disp + " billet(s) de 20 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b20retInput == "o") {
              b20ret = b20disp
            }
            else {
              b20ret = (b20retInput).toInt
            }
            retrait = retrait - 20 * b20ret
          }
          var b10disp = retrait / 10
          if (b10disp > 0) {
            var b10retInput = b10disp
            println("\nIl reste " + retrait + " CHF à distribuer. \nVous obtenez " + b10disp + " billet(s) de 10 CHF.")
            b10ret = b10retInput
            retrait = retrait - 10 * b10ret
          }
          println("\nVeuillez retirer la somme demandée :")
          if (b500ret > 0) {
            println(b500ret + " billet(s) de 500 CHF")
          }
          if (b200ret > 0) {
            println(b200ret + " billet(s) de 200 CHF")
          }
          if (b100ret > 0) {
            println(b100ret + " billet(s) de 100 CHF")
          }
          if (b50ret > 0) {
            println(b50ret + " billet(s) de 50 CHF")
          }
          if (b20ret > 0) {
            println(b20ret + " billet(s) de 20 CHF")
          }
          if (b10ret > 0) {
            println(b10ret + " billet(s) de 10 CHF")
          }
        }
        else if (coupures == 2) {
          var b100disp = retrait / 100
          if (b100disp > 0) {
            var b100retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b100disp + " billet(s) de 100 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b100retInput == "o") {
              b100ret = b100disp
            }
            else {
              b100ret = (b100retInput).toInt
            }
            retrait = retrait - 100 * b100ret
          }
          var b50disp = retrait / 50
          if (b50disp > 0) {
            var b50retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b50disp + " billet(s) de 50 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b50retInput == "o") {
              b50ret = b50disp
            }
            else {
              b50ret = (b50retInput).toInt
            }
            retrait = retrait - 50 * b50ret
          }
          var b20disp = retrait / 20
          if (b20disp > 0) {
            var b20retInput = readLine("\nIl reste " + retrait + " CHF à distribuer. \nVous pouvez obtenir au maximum " + b20disp + " billet(s) de 20 CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
            if (b20retInput == "o") {
              b20ret = b20disp
            }
            else {
              b20ret = (b20retInput).toInt
            }
            retrait = retrait - 20 * b20ret
          }
          var b10disp = retrait / 10
          if (b10disp > 0) {
            var b10retInput = b10disp
            println("\nIl reste " + retrait + " CHF à distribuer. \nVous obtenez " + b10disp + " billet(s) de 10 CHF.")
            b10ret = b10retInput
            retrait = retrait - 10 * b10ret
          }
          println("\nVeuillez retirer la somme demandée :")
          if (b100ret > 0) {
            println(b100ret + " billet(s) de 100 CHF")
          }
          if (b50ret > 0) {
            println(b50ret + " billet(s) de 50 CHF")
          }
          if (b20ret > 0) {
            println(b20ret + " billet(s) de 20 CHF")
          }
          if (b10ret > 0) {
            println(b10ret + " billet(s) de 10 CHF")
          }
        }
        retrait = b500ret * 500 + b200ret * 200 + b100ret * 100 + b50ret * 50 + b20ret * 20 + b10ret * 10
        montantcompte(identifiant) = montantcompte(identifiant) - retrait
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantcompte(identifiant))
        print("CHF\n")
      }
      if (devise == "EUR") {
        var b100disp = retrait / 100
        if (b100disp > 0) {
          var b100retInput = readLine("\nIl reste " + retrait + " EUR à distribuer. \nVous pouvez obtenir au maximum " + b100disp + " billet(s) de 100 EUR. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
          if (b100retInput == "o") {
            b100ret = b100disp
          }
          else {
            b100ret = (b100retInput).toInt
          }
          retrait = retrait - 100 * b100ret
        }
        var b50disp = retrait / 50
        if (b50disp > 0) {
          var b50retInput = readLine("\nIl reste " + retrait + " EUR à distribuer. \nVous pouvez obtenir au maximum " + b50disp + " billet(s) de 50 EUR. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
          if (b50retInput == "o") {
            b50ret = b50disp
          }
          else {
            b50ret = (b50retInput).toInt
          }
          retrait = retrait - 50 * b50ret
        }
        var b20disp = retrait / 20
        if (b20disp > 0) {
          var b20retInput = readLine("\nIl reste " + retrait + " EUR à distribuer. \nVous pouvez obtenir au maximum " + b20disp + " billet(s) de 20 EUR. \nTapez o pour ok ou une autre valeur inférieure à celle proposée. \nVotre choix : ").toString
          if (b20retInput == "o") {
            b20ret = b20disp
          }
          else {
            b20ret = (b20retInput).toInt
          }
          retrait = retrait - 20 * b20ret
        }
        var b10disp = retrait / 10
        if (b10disp > 0) {
          var b10retInput = b10disp
          println("\nIl reste " + retrait + " EUR à distribuer. \nVous obtenez " + b10disp + " billet(s) de 10 EUR.")
          b10ret = b10retInput
          retrait = retrait - 10 * b10ret
        }
        println("\nVeuillez retirer la somme demandée :")
        if (b100ret > 0) {
          println(b100ret + " billet(s) de 100 EUR")
        }
        if (b50ret > 0) {
          println(b50ret + " billet(s) de 50 EUR")
        }
        if (b20ret > 0) {
          println(b20ret + " billet(s) de 20 EUR")
        }
        if (b10ret > 0) {
          println(b10ret + " billet(s) de 10 EUR")
        }
        retrait = b100ret * 100 + b50ret * 50 + b20ret * 20 + b10ret * 10
        retrait1 = tauxchange2 * retrait.toDouble
        montantcompte(identifiant) = montantcompte(identifiant)  - retrait1
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", montantcompte(identifiant))
        print("CHF\n")
    }
  }
  
   def changepin(identifiant : Int, pincorrect : Array[String]) : Unit = {
    var newpin = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : ")
    while (newpin.length < 8 ) {
      println("\nVotre code pin ne contient pas au moins 8 caractères.")
      newpin = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : ")
    }
    pincorrect(identifiant) = newpin 
    println("\nVotre code PIN a été changé avec succès !")
  }
  
    def main(args: Array[String]): Unit = {
      var optionDepot = 1
      var optionRetrait = 2
      var consultcompte = 3
      var optionchangepin = 4
      var terminer = 5
      var nbclient = 100
      var identifiant = 0
      var montantcompte = Array.fill(nbclient)(1200.0)

      var pincorrect = Array.fill(nbclient)("INTRO1234")
      var pin = "0"
      while (pin != pincorrect(identifiant)) {
        identifiant = readLine("\nSaisissez votre code identifiant : ").toInt
        if (identifiant > nbclient) {
          println("\nCet identifiant n’est pas valable. ")
          sys.exit()
        }
        pin = readLine("\nSaisissez votre code PIN : ").toString
        var tentativerestantes = 3
        while (tentativerestantes > 1 && pin != pincorrect(identifiant)) {
          tentativerestantes -= 1
          println("\nCode PIN erroné, il vous reste " + tentativerestantes + " tentatives.")
          pin = readLine("\nSaisissez votre code PIN : ").toString
          if (tentativerestantes == 1 && pin != pincorrect(identifiant)) {
           println("\nTrop d’erreurs, abandon de l’identification")
          }
        }
      }
      var choix = readLine("\nChoisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code PIN \n5) Terminer \nVotre choix : ").toInt

      while (choix == optionDepot || choix == optionRetrait || choix == 3 || choix == optionchangepin || choix == 5) {
        if (choix == optionDepot) {
          depot(identifiant, montantcompte)
          choix = readLine("\nChoisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code PIN \n5) Terminer \nVotre choix : ").toInt
      }

      else if (choix == optionRetrait) {
      retrait(identifiant, montantcompte)
      choix = readLine("\nChoisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code PIN \n5) Terminer \nVotre choix : ").toInt
    }

      else if (choix == 3) {
        printf("\nLe montant disponible sur votre compte est de : %.2f ", montantcompte(identifiant))
        print("CHF\n")
        choix = readLine("\nChoisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code PIN \n5) Terminer \nVotre choix : ").toInt
      }

      else if (choix == optionchangepin) {
        changepin(identifiant, pincorrect)
        choix = readLine("\nChoisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code PIN \n5) Terminer \nVotre choix : ").toInt
      }

      else if (choix == 5) {
        println("\nFin des opérations, n'oubliez pas de récupérer votre carte.")
        var pin = "0"
        while (pin != pincorrect(identifiant)) {
          identifiant = readLine("\nSaisissez votre code identifiant : ").toInt
          if (identifiant > nbclient) {
            println("\nCet identifiant n’est pas valable. ")
            sys.exit()
          }
          pin = readLine("\nSaisissez votre code PIN : ").toString 
          var tentativerestantes = 3
          while (tentativerestantes > 1 && pin != pincorrect(identifiant)) {
            tentativerestantes -= 1
            println("\nCode PIN erroné, il vous reste " + tentativerestantes + " tentatives.")
            pin = readLine("\nSaisissez votre code PIN : ").toString
            if (tentativerestantes == 1 && pin != pincorrect(identifiant)) {
             println("\nTrop d’erreurs, abandon de l’identification")
            }
          }
        }
        choix = readLine("\nChoisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code PIN \n5) Terminer \nVotre choix : ").toInt
      }
    }
  }
}





