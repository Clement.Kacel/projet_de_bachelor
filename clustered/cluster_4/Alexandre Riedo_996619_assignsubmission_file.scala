//Assignment: Alexandre Riedo_996619_assignsubmission_file

import scala.io.StdIn.readLine

object Main {
  // DECLARATION ET INITIALISATION DE VARIABLES
  var choixOperation:Int = 0

  /*val PIN:String = "INTRO1234" (EXO 1, déprécié)*/
  var PINCheck:Int = 0 // On donne la valeure de "1" lorsque le PIN est validé, et "2" quand on dépasse les 3 tentatives.
  var tentativesPINMax = 3 // Le nombre de fois max.
  var tentativesPIN:Int = tentativesPINMax // Le nombre de fois que le user peut se tenter l'entrée du PIN. Se comporte comme un compteur.
  var userPIN = ""

  var montantDepot:Int = 0
  var montantDepotEuro:Double = 0 // Vu que montantDepot est obligatoirement un Int, on utilise cette variable pour chopper les décimales résultantes de la conversion Euro en CHF.
  var choixDeviseDepot:Int = 0
  var montantDisponible:Double = 1200.0
  val tauxCHFEnEuro:Double = 1.05
  val tauxEuroEnCHF:Double = 0.95

  var montantRetrait:Int = 0
  var montantRetraitEuro:Double = 0 // Pour les retraits en Euro.
  var montantRetraitSansDifference:Int = 0 // A utilise en dehors de la boucle while, pour le calcul montantDisponible - montantRetraitSansDifferences
  var tauxRetraitAutorise:Double = 0.1
  var choixDeviseRetrait:Int = 0 // En soit, on aurait pu faire aussi choixDevise comme variable pour retrait et depot. "1" pour chf, "2" pour euro.
  var choixDeviseCoupures:Int = 0 // Pour grosses ou petites coupures. "1" pour grosses, "2" pour petites.
  var coupure:Int = 0
  var nbrBilletsMax:Int = 0
  var choixBillets:String = "" // "0" pour aucun billet, "1,2,3,4,etc..." pour un nombre de billets, "o" pour la proposition du code.

  var nbrBillets500 = "0"
  var nbrBillets200 = "0"
  var nbrBillets100 = "0"
  var nbrBillets50 = "0"
  var nbrBillets20 = "0"
  var nbrBillets10 = "0"

  var nbClients = 100 // Le nombre de comptes possibles dans le système.
  val comptes = Array.fill(nbClients)(montantDisponible) // tableau des montantDisponible des comptes.
  val codesPIN = Array.fill(nbClients)("INTRO1234") // tableau des codes pins.
  var choixIdentifiant = 0 // On va utiliser un readLine() après pour défini ce que le user choisi. Valide sur [0, nbClients[.

  var askID = true // true : on demande l'ID dans le code. false : on ne demande pas l'ID.
  var IDFail = false // pour le print final, indiquer quand saisie d'ID s'est plantée.

  var whileBreak = true // Contrôle l'éxécution de la boucle principale

  
  // METHODE depot
  def depot(ID:Int, comptes:Array[Double]):Unit = {
    montantDisponible = comptes(ID) // Récupérage de la valeur de montant dans le compte.
    
    do { // Test et feeback du choix de devis.
      choixDeviseDepot = readLine("\nIndiquez la devise du dépôt 1) CHF ; 2) EUR > ").toInt
      if ( !(choixDeviseDepot  == 1 || choixDeviseDepot == 2) ) println("L'entrée écrite n'est pas valide, réesayez.")
    } while ( !(choixDeviseDepot  == 1 || choixDeviseDepot == 2) )

    do { // Test et feedback du montantDepot juste ou faux.
      montantDepot = readLine("\nIndiquez le montant du dépôt > ").toInt
      if ( ((montantDepot % 10) != 0) || montantDepot == 0) println("Le montant doit être un mulitple de 10.")
    } while ( ((montantDepot % 10) != 0) || montantDepot == 0)

    if (choixDeviseDepot == 1) montantDisponible += montantDepot // Update du montant dans le compte dans le cas CHF.
    if (choixDeviseDepot == 2) { // Conversion Euro en CHF.
      montantDepotEuro = montantDepot.toDouble * tauxEuroEnCHF
      montantDisponible += montantDepotEuro // Update du montant dans le compte dans le cas Euro.
    }

    comptes(ID) = montantDisponible // Update du montant dans l'array

    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", comptes(ID)) // Affichage du montant disponible.
  }


  // METHODE retrait
  def retrait(ID:Int, comptes:Array[Double]):Unit = {
    // OPERATION DE RETRAIT
      do { // Test et feeback du choix de devis.
        choixDeviseRetrait = readLine("\nIndiquez la devise du retrait 1) CHF ; 2) EUR > ").toInt
        if ( !(choixDeviseRetrait  == 1 || choixDeviseRetrait == 2) ) println("L'entrée écrite n'est pas valide, réesayez.")
      } while ( !(choixDeviseRetrait  == 1 || choixDeviseRetrait == 2) )

      do { // Test et feedback du montantRetrait juste ou faux.
        montantRetrait = readLine("\nIndiquez le montant du retrait > ").toInt
        if ( ((montantRetrait % 10) != 0) || montantRetrait == 0) println("Le montant doit être un mulitple de 10.") // Condition de multiple de 10.
        if (montantRetrait > (montantDisponible * tauxRetraitAutorise)) println("Votre plafond de retrait autorisé est : " + (montantDisponible * tauxRetraitAutorise)) // Condition de retrait autorisé maximal.
      } while ((montantRetrait % 10) != 0 || montantRetrait > (montantDisponible * tauxRetraitAutorise) || montantRetrait == 0)

      montantRetraitSansDifference = montantRetrait // Affectation pour le cacul en dehors de la boucle while principale.        

      if (choixDeviseRetrait == 1 || choixDeviseRetrait == 2) { // Traitement du cas CHF et euro. Petit test d'entrée correct pour choixDeviseRetrait.
        if (montantRetrait >= 200 && choixDeviseRetrait == 1) {
          do { // Test et feedback du choix de grosses ou petites coupures. Si le user met autre qu'un nombre, le code explose
            choixDeviseCoupures = readLine("\nEn 1) grosses coupures, 2) petites coupures > ").toInt
            if ( !(choixDeviseCoupures == 1 || choixDeviseCoupures == 2) ) println("L'entrée écrite n'est pas valide, réesayez.")
          } while ( !(choixDeviseCoupures == 1 || choixDeviseCoupures == 2) )
        }

        // Calcul des billets et propositions
        while (montantRetrait != 0) {
          while ( !( (montantRetrait < 500) || choixBillets == "o" || choixBillets == "0" || choixDeviseCoupures == 2 || choixDeviseRetrait == 2)) { // Pour les billets de 500.
            println("\nIl reste " + montantRetrait + " à distribuer.")
            coupure = 500
            nbrBilletsMax = Math.floorDiv(montantRetrait, coupure)
            println("Vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " .")

            do { // Test et feedback de l'entrée choixBillets
              choixBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              if (choixBillets != "o") { // Super-moche et redondant, mais c'est pour éviter que choixBillets.toInt explose le code quand on choisit "o"
                if (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0) println("\nVotre entrée est invalide. Pour rappel, vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " . Ré-essayez.\n")
              }
              if (choixBillets == "o") choixBillets = nbrBilletsMax.toString // Pour éviter que le code explose à la ligne suivante choixBillets.toInt
            } while (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0 )

            if ( !(choixBillets == "o" || choixBillets == "0") ) { // Traitement du cas "1 ou 2 ou 3 ou 4 ou etc."
              nbrBillets500 = choixBillets
              montantRetrait = montantRetrait - (nbrBillets500.toInt * coupure)
            }

            if (choixBillets == "o") { // Traitement du cas "o"
              nbrBillets500 = nbrBilletsMax.toString
              montantRetrait = montantRetrait - (nbrBillets500.toInt * coupure)
            }

            if (choixBillets == "0") { // Traitment du cas "0"
              nbrBillets500 = "0"
            }

            choixBillets = "0" // Après avoir entré le nombre de billets, on sort de la boucle while.

          }
          choixBillets = "" // Pour que la boucle while ne soit pas sauté.
          while ( !( (montantRetrait < 200) || choixBillets == "o" || choixBillets == "0" || choixDeviseCoupures == 2 || choixDeviseRetrait == 2)) { // Pour les billets de 200
                   println("\nIl reste " + montantRetrait + " à distribuer.")
                   coupure = 200
                   nbrBilletsMax = Math.floorDiv(montantRetrait, coupure)
                   println("Vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " .")

                   do { // Test et feedback de l'entrée choixBillets
                      choixBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                      if (choixBillets != "o") { // Super-moche et redondant, mais c'est pour éviter que choixBillets.toInt explose le code quand on choisit "o"
                        if (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0) println("\nVotre entrée est invalide. Pour rappel, vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " . Ré-essayez.\n")
                      }
                      if (choixBillets == "o") choixBillets = nbrBilletsMax.toString // Pour éviter que le code explose à la ligne suivante choixBillets.toInt
                    } while (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0 )

                   if ( !(choixBillets == "o" || choixBillets == "0") ) { // Traitment du cas "1 ou 2 ou 3 ou 4 ou etc."
                     nbrBillets200 = choixBillets
                     montantRetrait = montantRetrait - (nbrBillets200.toInt * coupure)
                   }

                   if (choixBillets == "o") { // Traitement du cas "o"
                     nbrBillets200 = nbrBilletsMax.toString
                     montantRetrait = montantRetrait - (nbrBillets200.toInt * coupure)
                   }

                   if (choixBillets == "0") { // Traitment du cas "0"
                     nbrBillets200 = "0"
                   }
                   choixBillets = "0" // Après avoir entré le nombre de billets, on sort de la boucle while.

                 }
                 choixBillets = ""
                 while ( !( (montantRetrait < 100) || choixBillets == "o" || choixBillets == "0" )) {
                     println("\nIl reste " + montantRetrait + " à distribuer.")
                     coupure = 100
                     nbrBilletsMax = Math.floorDiv(montantRetrait, coupure)
                     println("Vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " .")

                     do { // Test et feedback de l'entrée choixBillets
                       choixBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                       if (choixBillets != "o") { // Super-moche et redondant, mais c'est pour éviter que choixBillets.toInt explose le code quand on choisit "o"
                         if (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0) println("\nVotre entrée est invalide. Pour rappel, vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " . Ré-essayez.\n")
                       }
                       if (choixBillets == "o") choixBillets = nbrBilletsMax.toString // Pour éviter que le code explose à la ligne suivante choixBillets.toInt
                     } while (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0 )

                     if ( !(choixBillets == "o" || choixBillets == "0") ) { // Traitment du cas "1 ou 2 ou 3 ou 4 ou etc."
                       nbrBillets100 = choixBillets
                       montantRetrait = montantRetrait - (nbrBillets100.toInt * coupure)
                     }
                     if (choixBillets == "o") { // Traitement du cas "o"
                       nbrBillets100 = nbrBilletsMax.toString
                       montantRetrait = montantRetrait - (nbrBillets100.toInt * coupure)
                     }

                     if (choixBillets == "0") { // Traitment du cas "0"
                       nbrBillets100 = "0"
                     }

                     choixBillets = "0" // Après avoir entré le nombre de billets, on sort de la boucle while.

                   } 
                   choixBillets = ""
                   while ( !( (montantRetrait < 50) || choixBillets == "o" || choixBillets == "0" )) {
                            println("\nIl reste " + montantRetrait + " à distribuer.")
                            coupure = 50
                            nbrBilletsMax = Math.floorDiv(montantRetrait, coupure)
                            println("Vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " .")
                            do { // Test et feedback de l'entrée choixBillets
                              choixBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                              if (choixBillets != "o") { // Super-moche et redondant, mais c'est pour éviter que choixBillets.toInt explose le code quand on choisit "o"
                                if (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0) println("\nVotre entrée est invalide. Pour rappel, vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " . Ré-essayez.\n")
                              }
                              if (choixBillets == "o") choixBillets = nbrBilletsMax.toString // Pour éviter que le code explose à la ligne suivante choixBillets.toInt
                            } while (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0 )

                            if ( !(choixBillets == "o" || choixBillets == "0") ) { // Traitment du cas "1 ou 2 ou 3 ou 4 ou etc."
                              nbrBillets50 = choixBillets
                              montantRetrait = montantRetrait - (nbrBillets50.toInt * coupure)                                
                            }

                            if (choixBillets == "o") { // Traitement du cas "o"
                              nbrBillets50 = nbrBilletsMax.toString
                              montantRetrait = montantRetrait - (nbrBillets50.toInt * coupure)
                            }

                            if (choixBillets == "0") { // Traitment du cas "0"
                              nbrBillets50 = "0"
                            }

                            choixBillets = "0" // Après avoir entré le nombre de billets, on sort de la boucle while.

                          } 
                          choixBillets = ""
                          while ( !( (montantRetrait < 20) || choixBillets == "o" || choixBillets == "0" )) {
                              println("\nIl reste " + montantRetrait + " à distribuer.")
                              coupure = 20
                              nbrBilletsMax = Math.floorDiv(montantRetrait, coupure)
                              println("Vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " .")
                              do { // Test et feedback de l'entrée choixBillets
                                choixBillets = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                                if (choixBillets != "o") { // Super-moche et redondant, mais c'est pour éviter que choixBillets.toInt explose le code quand on choisit "o"
                                  if (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0) println("\nVotre entrée est invalide. Pour rappel, vous pouvez obtenir au maximum " + nbrBilletsMax + " billet(s) de " + coupure + " . Ré-essayez.\n")
                                }
                                if (choixBillets == "o") choixBillets = nbrBilletsMax.toString // Pour éviter que le code explose à la ligne suivante choixBillets.toInt
                              } while (choixBillets.toInt > nbrBilletsMax || choixBillets.toInt < 0 )

                              if ( !(choixBillets == "o" || choixBillets == "0") ) { // Traitment du cas "1 ou 2 ou 3 ou 4 ou etc."
                                nbrBillets20 = choixBillets
                                montantRetrait = montantRetrait - (nbrBillets20.toInt * coupure)              
                              }

                              if (choixBillets == "o") { // Traitement du cas "o"
                                nbrBillets20 = nbrBilletsMax.toString
                                montantRetrait = montantRetrait - (nbrBillets20.toInt * coupure)
                              }

                              if (choixBillets == "0") { // Traitment du cas "0"
                                nbrBillets20 = "0"
                              }

                              choixBillets = "0" // Après avoir entré le nombre de billets, on sort de la boucle while.

                            } 
                            choixBillets = ""
                            while ( !( (montantRetrait < 10) || choixBillets == "o" || choixBillets == "0" )) {
                                     println("\nIl reste " + montantRetrait + " à distribuer.")
                                     coupure = 10
                                     nbrBilletsMax = Math.floorDiv(montantRetrait, coupure)
                                     println("Vous obtenez " + nbrBilletsMax + " billet(s) de " + coupure + ".")

                                     nbrBillets10 = nbrBilletsMax.toString
                                     montantRetrait = montantRetrait - (nbrBillets10.toInt * coupure)
                                   }           
        }
      if (choixDeviseRetrait == 2) { // Pour corriger un bug avec les petites coupures.
        nbrBillets500 = "0"
        nbrBillets200 = "0"
      }

      println("\nVeuillez retirez la somme demandée :") // Affichage des billets 
      if (nbrBillets500 != "0") println(nbrBillets500 + " billet(s) de 500.")
      if (nbrBillets200 != "0") println(nbrBillets200 + " billet(s) de 200.")
      if (nbrBillets100 != "0") println(nbrBillets100 + " billet(s) de 100.")
      if (nbrBillets50 != "0") println(nbrBillets50 + " billet(s) de 50.")
      if (nbrBillets20 != "0") println(nbrBillets20 + " billet(s) de 20.")
      if (nbrBillets10 != "0") println(nbrBillets10 + " billet(s) de 10.")

      nbrBillets500 = "0" // Reset pour le retour dans la boucle while.
      nbrBillets200 = "0"
      nbrBillets100 = "0"
      nbrBillets50 = "0"
      nbrBillets20 = "0"
      nbrBillets10 = "0"

      if (choixDeviseRetrait == 2) { // Conversion montantRetrait de CHF en Euro, et différence montantDispnoible montantRetrait.
        montantRetraitEuro = montantRetraitSansDifference.toDouble * tauxEuroEnCHF
        montantDisponible = montantDisponible - montantRetraitEuro
      } else if (choixDeviseRetrait == 1) { // Différence en chf.
        montantDisponible = montantDisponible - montantRetraitSansDifference
      }

      comptes(ID) = montantDisponible //Update de l'array!
      
      printf("\nVotre retrait a été pris en compte. Le montant disponible sur votre compte est de : %.2f \n", montantDisponible)         
    }        
  }

  
  // METHODE changePIN
  def changePIN(ID:Int, codesPIN:Array[String]):Unit = {
    var userInput = ""
    
    do { // Entree du user pour le codePin
      userInput = readLine("\nSaissez votre nouveau code PIN (il doit contenir au moins 8 caractères) > ")
      if (userInput.length() < 8) println("Votre code PIN ne contient pas au moins 8 caractères.")
    } while (userInput.length() < 8)

    codesPIN(ID) = userInput // Changement de codesPIN.
  }

  
  // METHODE Main
  def main(args: Array[String]): Unit = {
    // BOUCLE CODE PRINCIPALE.
    while (whileBreak == true) {
      // ENTREE CODE IDENTIFIANT.
      while (askID == true) {
        choixIdentifiant = readLine("\nSaissez votre code identifiant > ").toInt

        choixIdentifiant = choixIdentifiant - 1 // Pour respecter la consigne de numero ID inférieur à nbClients (100). Le programme utilise choixIdentifiant-1, mais le user verra choixIdentifiant.
        
        if (choixIdentifiant > (nbClients-1) || choixIdentifiant < 0 ) { // Cas ID faux.
          IDFail = true
          whileBreak = false // On arrête le code.
          askID = false // On sort de la boucle while (askID == true).
          PINCheck = 2 // On évite le testage et demande du PIN.
        } else { // Cas juste.
          askID = false // On sort de la boucle while (askID == true).
          PINCheck = 0 // On demande le PIN.
        }
      }

      // ENTREE DU CODE PIN.
      tentativesPIN = tentativesPINMax // Peu élégant, mais si on rate l'entrée PIN, et que on le réésaie ensuite, il faut cette ligne pour que ça marche.
      while (PINCheck == 0) { // Boucle d'input.
        userPIN = readLine("\nSaissez votre code PIN > ")

        if (userPIN == codesPIN(choixIdentifiant)) { // Test et feedback du PIN juste ou faux.
          println("Code PIN correct.")
          PINCheck = 1
        } else if (userPIN != codesPIN(choixIdentifiant)) {
          tentativesPIN = tentativesPIN - 1
          println("Code PIN erroné, il vous reste " + tentativesPIN + " tentative(s).")
        }

        if (tentativesPIN == 0) { // Cas d'échec total PIN.
          PINCheck = 2 // Test des 3 tentatives.
          askID = true // On re-demande l'identification.
          println("\nTrop d'erreurs, abandon de l'identification.")
        }
      }

      // AFFICHAGE DES 5 OPERATIONS
      while (!(choixOperation == 1 || choixOperation == 2 || choixOperation == 3 || choixOperation == 4 || choixOperation == 5 || IDFail == true || PINCheck == 2)) { // Test et feedback de choixOperation.
        println("\nChoissez votre opération :\n\t1) Dépot\n\t2) Retrait\n\t3) Consultation du compte\n\t4) Changement du code PIN\n\t5) Terminer")
        choixOperation = readLine("Votre choix : ").toInt
        if ( !(choixOperation == 1 || choixOperation == 2 || choixOperation == 3 || choixOperation == 4 || choixOperation == 5 || IDFail == true || PINCheck == 2) ) println("\nEntrée invalide. Rééesayez.")
      }

      // DEPOT
      if (choixOperation == 1) {
        depot(choixIdentifiant, comptes)
        choixOperation = 0 // Pour réactiver la boucle "AFFICHAGE DES 5 OPERATIONS".
      }

      // RETRAIT
      if (choixOperation == 2) {
        retrait(choixIdentifiant, comptes)
        choixOperation = 0 // Pour réactiver la boucle "AFFICHAGE DES 5 OPERATIONS".
      }

      // CONSULTATION DU COMPTE
      if (choixOperation == 3) {
        printf("\nLe montant disponible sur votre compte est de : %.2f CHF.\n", comptes(choixIdentifiant))
        choixOperation = 0
      }

      // CHANGEMENT DU CODE PIN
      if (choixOperation == 4) {
        changePIN(choixIdentifiant, codesPIN) // On change la valeur dans codesPIN.
        choixOperation = 0 // Pour réactiver la boucle "AFFICHAGE DES 5 OPERATIONS".
      }

      // TERMINER LES OPERATIONS
      if (choixOperation == 5) {
        println("\nFin des opérations, n'oubliez pas de récupérer votre carte.")
        choixOperation = 0 // Pour réactiver la boucle "AFFICHAGE DES 5 OPERATIONS".
        askID = true // Pour réactiver la boucle "ENTREE CODE IDENTIFIANT".
      }
      
    } // Fin Boucle Code Principale

    // Affichage en cas de ID Fail.
    if (IDFail == true) println("\nCet identifiant n'est pas valable. Il doit être entier et inférieur ou égal à " + (nbClients) + ".\nAssurez-vous aussi qu'il est supérieur à 0.")

  } // Fin def main.
}// Fin Object main.