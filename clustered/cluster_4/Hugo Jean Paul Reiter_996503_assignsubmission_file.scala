//Assignment: Hugo Jean Paul Reiter_996503_assignsubmission_file

import io.StdIn._
import math._ 

object Main {
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères)>")
    var newpin = readLine()
    while (newpin.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      newpin = readLine()
    }
    codespin(id) = newpin 
  }
  def retrait(id: Int, comptes: Array[Double]): Unit = {
    var devise = 0
    var retraitauto = 0.0
    var billet500 = 0
    var billet200 = 0
    var billet100 = 0
    var billet50 = 0
    var billet20 = 0
    var billet10 = 0
    var reste = 0
    var coupure = 0
    println("Indiquez la devise: 1 CHF, 2: EUR>") //choix de la devise
    devise = readInt()
    while (devise != 1 && devise != 2){ //Tant que la devise n'est pas 1 ou 2
      println ("Indiquez la devise: 1 CHF, 2: EUR>")
      devise = readInt()
    }
    var montant = 0
    println("Indiquez le montant du retrait>") //Indiquer le montant du retrait
    montant = readInt()
    while ((montant % 10) != 0){ //Tant que le montant n'est pas un mutliple de 10
      println("Le montant doit être un multiple de 10.")
      montant = readInt()
    }
    retraitauto = comptes(id) * 0.1 //Montant de retrait autorisé
    while (montant > retraitauto){ //Tant que le montant entré est supérieur au montant autorisé
      printf("Votre plafond de retrait autorisé est de: %2f\n", retraitauto)
      montant = readInt()
    }
    if (devise == 1){ //Pour des CHF
      var choix = 0
      if (montant >= 200){ //Pour un montant supérieur ou égal à 200 on propose différentes coupures
        println("1) grosses coupures, 2) petites coupures>")
        choix = readInt()
      }
      else if (montant < 200){ //On ne propose pas le choix dans le cas inverse
        choix = 2
      }
      while (choix != 1 && choix != 2){ //Tant que le choix de la coupure n'est pas valide
        println("1) grosses coupures, 2) petites coupures>")
        choix = readInt()
      }
      var confirmation = "r" //Déclration des variables utiles
      billet500 = montant / 500
      reste = montant 
      if (reste > 0 && choix == 1 && reste >= 500){ //Si le montant est supérieur ou égal à 500 et que le choix est 1
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum" +billet500+ "billet(s) de 500 CHF")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet500){ //Tant que le choix n'est pas valide
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){ //Si on n'a pas "o" alors on peut convertir en entier
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){ //Si la confirmation est "o" alors on effectue
          reste = reste - (billet500 * 500)
        }else if(confirmation != "o" && coupure < billet500){ //Si la confirmation n'est pas "o" alors on divise en coupure les billets de 500
          reste = reste - (coupure * 500)
          billet500 = coupure
        }
      }
      if (choix == 1 && reste >= 200){ //Même chose pour 200 etc...
        billet200 = reste / 200
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet200+" billet(s) de 200 CHF")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet200){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet200 * 200)
        }else if(!(confirmation == "o") && coupure < billet200){
          reste = reste - (coupure * 200)
          billet200 = coupure
        }
      }
      if (reste >= 100){
        billet100 = reste / 100
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet100+" billet(s) de 100 CHF")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet100){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet100 * 100)
        }else if(!(confirmation == "o") && coupure < billet100){
          reste = reste - (coupure * 100)
          billet100 = coupure
        }
      }
      if (reste >= 50){
        billet50 = reste / 50
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet50+" billet(s) de 50 CHF")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet50){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet50 * 50)
        }else if(!(confirmation == "o") && coupure < billet50){
          reste = reste - (coupure * 50)
          billet50 = coupure
        }
      }
      if (reste >= 20){
        billet20 = reste / 20
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet20+" billet(s) de 20 CHF")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet20){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet20 * 20)
        }else if(!(confirmation == "o") && coupure < billet20){
          reste = reste - (coupure * 20)
          billet20 = coupure
        }
      }
      if (reste > 0){ //Si le reste n'est pas 0, il est automatiquement réparti en billets de 10
        billet10 = reste / 10
        println("Il reste "+reste)
        println("Vous allez obtenir au maximum "+billet10+" billet(s) de 10 CHF")
      }
      println("Veuillez retirer la somme demandée: ")
      if (billet500 > 0){println(billet500+ "billet(s) de 500 CHF")}
      if (billet200 > 0){println(billet200+ "billet(s) de 200 CHF")}
      if (billet100 > 0){println(billet100+ "billet(s) de 100 CHF")}
      if (billet50 > 0){println(billet50+ "billet(s) de 50 CHF")}
      if (billet20 > 0){println(billet20+ "billet(s) de 20 CHF")}
      if (billet10 > 0){println(billet10+ "billet(s) de 10 CHF")}
      comptes(id) = comptes(id) - montant //Le montant du compte est mis à jour
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f\n",comptes(id))
    }
    else if (devise == 2){ //Même chose pour les euros sans le choix entre grosses et petites coupures
      var confirmation = "r"
      var coupure = 0
      var reste = montant
      if (reste >= 100){
        billet100 = reste / 100
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet100+" billet(s) de 100 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet100){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet100 * 100)
        }else if(!(confirmation == "o") && coupure < billet100){
          reste = reste - (coupure * 100)
          billet100 = coupure
        }
      }
      if (reste >= 50){
        billet50 = reste / 50
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet50+" billet(s) de 50 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet50){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet50 * 50)
        }else if(!(confirmation == "o") && coupure < billet50){
          reste = reste - (coupure * 50)
          billet50 = coupure
        }
      }
      if (reste >= 20){
        billet20 = reste / 20
        println("Il reste "+reste)
        println("Vous pouvez obtenir au maximum "+billet20+" billet(s) de 20 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while (confirmation != "o" && confirmation.toInt > billet20){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine()
        }
        if (confirmation != "o"){
          coupure = confirmation.toInt
        }
        if (confirmation == "o"){
          reste = reste - (billet20 * 20)
        }else if(!(confirmation == "o") && coupure < billet20){
          reste = reste - (coupure * 20) 
          billet20 = coupure
        }
        if (reste > 0){
          billet10 = reste / 10
          println("Il reste "+reste)
          println("Vous allez obtenir au maximum "+billet10+" billet(s) de 10 EUR")
        }
        println("Veuillez retirer la somme demandée: ")
        if (billet100 > 0){println(billet100+ "billet(s) de 100 EUR")}
        if (billet50 > 0){println(billet50+ "billet(s) de 50 EUR")}
        if (billet20 > 0){println(billet20+ "billet(s) de 20 EUR")}
        if (billet10 > 0){println(billet10+ "billet(s) de 10 EUR")}
        comptes(id) = comptes(id) - (montant * 0.95)
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f\n",comptes(id))
      }
    }
  } 
  def depot(id: Int, comptes: Array[Double]): Unit = {
    var devisee = 0
    println("Indiquez la devise du dépôt: 1) CHF; 2) EUR>") //Choix de la devise
    devisee = readInt()
    while (devisee != 1 && devisee != 2){ //Tant que la devise entrée n'est pas 1 ou 2
      println ("Veuillez choisir une valeur acceptée: 1) CHF; 2) EUR>")
      devisee = readInt()
    }
    if (devisee== 1) { //Quand la devise est 1
      var montant = 0
      println("Indiquez le montant du dépôt>")
      montant = readInt()
      while ((montant % 10) != 0) { //Tant que le montant n'est pas multiple de 10
        println("Le montant doit être un multiple de 10")
        montant = readInt()
      }
      comptes(id) = comptes(id) + montant //Solde du compte mis a jour
    }else if (devisee == 2) { //Quand la devise est 2
      var montant = 0
      var montantconverti = 0.0
      println("Indiquez le montant du dépôt>")
      montant = readInt()
      while ((montant % 10) != 0) {
        println("Le montant doit être un multiple de 10")
        montant = readInt()
      }
      montantconverti = montant * 0.95 //On converti le montant déposé en CHF
      comptes(id) = comptes(id) + montantconverti //Mise a jour du solde du compte
    }
    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : "+comptes(id))  
  } 
 

  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var id = 0
    var control = 0
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    
    while(control == 0){
   
    var pin = "PIN" //Déclaration des valeurs et variables utiles
    var nombreop = 1 //incrémenter après 1ere op
    var chances = 3
    println("Saisissez votre code identifiant>")
      id = readInt()
      if(id >= nbclients){
        println("Cet identifiant n'est pas valable.")
        return
      }
      if(id < nbclients){
        println("Saisissez votre code pin>")
        pin = readLine()
        while (pin != codespin(id) && chances > 0){
          printf("Code pin erroné, il vous reste "+chances+" tentatives>")
          chances -= 1
          pin = readLine()
        }
      }
      if (chances == 0) { //Il ne reste plus de tentatives
        println("Trops d'erreurs, abandon de l'identification") 
        
      } 
      println("Choisissez votre opération:\n  1) Dépôt\n  2) Retrait\n  3) Consultation du compte\n  4) Changement du code pin\n  5) Terminer\nVotre choix:")
      var choix = readInt() //Choix de l'opération
      while (choix != 5 && chances != 0){ //Tant que le choix n'est pas de terminer et qu'il reste des tentatives
        if (choix == 1){//opération de dépôt
          depot(id, comptes)
          nombreop += 1
        } 
        else if (choix == 2){//opération de retrait
          retrait(id, comptes)
          nombreop += 1
        }
        else if (choix == 3){//opération de consultation
          printf("Le montant disponible sur votre compte est de: %.2f\n",comptes(id))
        } 
        else if (choix == 4){//opération de changement de code pin 
          changepin(id, codespin)
          nombreop += 1
        }
        println("Choisissez votre opération:\n  1) Dépôt\n  2) Retrait\n  3) Consultation du compte\n  4) Changement du code pin\n  5) Terminer\nVotre choix:")
        choix = readInt() //On redemande le choix de l'opération à chaque itération
      }
      if (choix == 5 && !(chances == 0)){
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
      } //Terminer 
    }
  }
}



