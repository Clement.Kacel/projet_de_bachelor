//Assignment: Alexandre Lamigueiro Santalla_996523_assignsubmission_file

import scala.io.StdIn._

class Gestion_Identifiant_CodesPIN(val comptes: Array[Double], val codespin: Array[String], val nbclients: Int) {
  def verifier_Identifiant(id: Int): Boolean = {
    id >= 0 && id < nbclients
  }

  def verifier_CodePIN(id: Int, pin: String): Boolean = {
    codespin(id) == pin
  }
}

class Gestion_Choix(val comptes: Array[Double], val codespin: Array[String]) {
  val taux_EUR_a_CHF = 0.95
  var choix_devise = 0
  //Dépôt
  
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    choix_devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
    while (choix_devise != 1 && choix_devise != 2) {
      choix_devise = readLine("Choix de devise invalide. Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
    }
    var montant_depot = readLine("Indiquez le montant du dépôt > ").toDouble
    while (montant_depot % 10 != 0) {
      println("Le montant doit être un multiple de 10.")
      montant_depot = readLine("Indiquez le montant du dépôt > ").toDouble
    }
    if (choix_devise == 2) {
      montant_depot = montant_depot * taux_EUR_a_CHF
    }
    comptes(id) += montant_depot
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }
  
  //Retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    choix_devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
      while (choix_devise != 1 && choix_devise != 2) {
        choix_devise = readLine("Choix de devise invalide. Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
      }
      var montant_retrait = readLine("Indiquez le montant du retrait > ").toInt
      while (montant_retrait % 10 != 0 || montant_retrait > comptes(id) * 0.1) {
        if (montant_retrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        } else if (montant_retrait > comptes(id) * 0.1) {
          println("Votre plafond de retrait autorisé est de : " + comptes(id) * 0.1 + " CHF.")
        }
        montant_retrait = readLine("Indiquez le montant du retrait > ").toInt
      }
    
      var choix = ""
      var choix_coupure = 0
      if (choix_devise == 1) {
        if (montant_retrait >= 200) {
          choix_coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          while (choix_coupure != 1 && choix_coupure != 2) {
          choix_coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          }
        } else choix_coupure = 2

      var montant_restant = montant_retrait
      var montant_distribue = 0
      var nb_billets_500_CHF = 0
      var nb_billets_200_CHF = 0
      var nb_billets_100_CHF = 0
      var nb_billets_50_CHF = 0
      var nb_billets_20_CHF = 0
      var nb_billets_10_CHF = 0

      if (choix_coupure == 1 && montant_restant >= 500) {
        nb_billets_500_CHF = montant_restant / 500
        montant_restant = montant_restant % 500
        montant_distribue = montant_retrait
        println("Il reste " + montant_distribue + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_500_CHF + " billet(s) de 500 CHF")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == "o") {
        // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_500_CHF = 0
          montant_restant = montant_retrait
        } else if (choix.toInt < nb_billets_500_CHF) {
          nb_billets_500_CHF = choix.toInt
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500)
        }
      }

      if (choix_coupure == 1 && montant_restant >= 200) {
        nb_billets_200_CHF = montant_restant / 200
        montant_restant = montant_restant % 200
        montant_distribue = montant_retrait - (nb_billets_500_CHF * 500)
        println("Il reste " + montant_distribue + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_200_CHF + " billet(s) de 200 CHF")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == ("o")) {
        // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_200_CHF = 0
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500)
        } else if (choix.toInt < nb_billets_200_CHF) {
          nb_billets_200_CHF = choix.toInt
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200)
        }
      }

      if ((choix_coupure == 1 || choix_coupure == 2) && montant_restant >= 100) {
          nb_billets_100_CHF = montant_restant / 100
          montant_restant = montant_restant % 100
          montant_distribue = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200)
          println("Il reste " + montant_distribue + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_billets_100_CHF + " billet(s) de 100 CHF")
          choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
          if (choix == ("o")) {
            // On passe à la suite
          } else if (choix.toInt == 0) {
            nb_billets_100_CHF = 0
            montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200)
          } else if (choix.toInt < nb_billets_100_CHF) {
            nb_billets_100_CHF = choix.toInt
            montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100)
          }
        }

      if ((choix_coupure == 1 || choix_coupure == 2) && montant_restant >= 50) {
        nb_billets_50_CHF = montant_restant / 50
        montant_restant = montant_restant % 50
        montant_distribue = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100)
        println("Il reste " + montant_distribue + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_50_CHF + " billet(s) de 50 CHF")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == ("o")) {
          // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_50_CHF = 0
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100)
        } else if (choix.toInt < nb_billets_50_CHF) {
          nb_billets_50_CHF = choix.toInt
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100) - (nb_billets_50_CHF * 50)
        }
      }

      if ((choix_coupure == 1 || choix_coupure == 2) && montant_restant >= 20) {
        nb_billets_20_CHF = montant_restant / 20
        montant_restant = montant_restant % 20
        montant_distribue = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100) - (nb_billets_50_CHF * 50)
        println("Il reste " + montant_distribue + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_20_CHF + " billet(s) de 20 CHF")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == ("o")) {
          // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_20_CHF = 0
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100) - (nb_billets_50_CHF * 50)
        } else if (choix.toInt <= nb_billets_20_CHF) {
          nb_billets_20_CHF = choix.toInt
          montant_restant = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100) - (nb_billets_50_CHF * 50) - (nb_billets_20_CHF * 20)
        }
      }

      if ((choix_coupure == 1 || choix_coupure == 2) && montant_restant >= 10) {
        nb_billets_10_CHF = montant_restant / 10
        montant_restant = montant_restant % 10
        montant_distribue = montant_retrait - (nb_billets_500_CHF * 500) - (nb_billets_200_CHF * 200) - (nb_billets_100_CHF * 100) - (nb_billets_50_CHF * 50) - (nb_billets_20_CHF * 20)
        println("Il reste " + montant_distribue + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_10_CHF + " billet(s) de 10 CHF")
      }

      if (nb_billets_500_CHF > 0) {
        println(nb_billets_500_CHF + " billet(s) de 500 CHF")
      }

      if (nb_billets_200_CHF > 0) {
        println(nb_billets_200_CHF + " billet(s) de 200 CHF")
      }

      if (nb_billets_100_CHF > 0) {
        println(nb_billets_100_CHF + " billet(s) de 100 CHF")
      }

      if (nb_billets_50_CHF > 0) {
        println(nb_billets_50_CHF + " billet(s) de 50 CHF")
      }

      if (nb_billets_20_CHF > 0) {
        println(nb_billets_20_CHF + " billet(s) de 20 CHF")
      }

      if (nb_billets_10_CHF > 0) {
        println(nb_billets_10_CHF + " billet(s) de 10 CHF")
      }
      comptes(id) = comptes(id) - montant_retrait
    }

    if (choix_devise == 2){
      var montant_restant = montant_retrait
      var montant_distribue = 0
      var nb_billets_100_EUR = 0
      var nb_billets_50_EUR = 0
      var nb_billets_20_EUR = 0
      var nb_billets_10_EUR = 0

      if (montant_restant >= 100) {
        nb_billets_100_EUR = montant_restant / 100
        montant_restant = montant_restant % 100
        montant_distribue = montant_retrait
        println("Il reste " + montant_distribue + " € à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_100_EUR + " billet(s) de 100 €")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == ("o")) {
          // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_100_EUR = 0
          montant_restant = montant_retrait
        } else if (choix.toInt < nb_billets_100_EUR) {
          nb_billets_100_EUR = choix.toInt
          montant_restant = montant_retrait - (nb_billets_100_EUR * 100)
        }
      }

      if (montant_restant >= 50) {
        nb_billets_50_EUR = montant_restant / 50
        montant_restant = montant_restant % 50
        montant_distribue = montant_retrait - (nb_billets_100_EUR * 100)
        println("Il reste " + montant_distribue + " € à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_50_EUR + " billet(s) de 50 €")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == ("o")) {
          // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_50_EUR = 0
          montant_restant = montant_retrait - (nb_billets_100_EUR * 100)
        } else if (choix.toInt < nb_billets_50_EUR) {
          nb_billets_50_EUR = choix.toInt
          montant_restant = montant_retrait - (nb_billets_100_EUR * 100) - (nb_billets_50_EUR * 50)
        }
      }

      if (montant_restant >= 20) {
        nb_billets_20_EUR = montant_restant / 20
        montant_restant = montant_restant % 20
        montant_distribue = montant_retrait - (nb_billets_100_EUR * 100) - (nb_billets_50_EUR * 50)
        println("Il reste " + montant_distribue + " € à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_20_EUR + " billet(s) de 20 €")
        choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ").toString
        if (choix == ("o")) {
          // On passe à la suite
        } else if (choix.toInt == 0) {
          nb_billets_20_EUR = 0
          montant_restant = montant_retrait - (nb_billets_100_EUR * 100) - (nb_billets_50_EUR * 50)
        } else if (choix.toInt < nb_billets_20_EUR) {
          nb_billets_20_EUR = choix.toInt
          montant_restant = montant_retrait - (nb_billets_100_EUR * 100) - (nb_billets_50_EUR * 50) - (nb_billets_20_EUR * 20)
        }
      }

      if (montant_restant >= 10) {
        nb_billets_10_EUR = montant_restant / 10
        montant_restant = montant_restant % 10
        montant_distribue = montant_retrait - (nb_billets_100_EUR * 100) - (nb_billets_50_EUR * 50) - (nb_billets_20_EUR * 20)
        println("Il reste " + montant_distribue + " € à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_billets_10_EUR + " billet(s) de 10 €")
      }

      if (nb_billets_100_EUR > 0) {
        println(nb_billets_100_EUR + " billet(s) de 100 €")
      }

      if (nb_billets_50_EUR > 0) {
        println(nb_billets_50_EUR + " billet(s) de 50 €")
      }

      if (nb_billets_20_EUR > 0) {
        println(nb_billets_20_EUR + " billet(s) de 20 €")
      }

      if (nb_billets_10_EUR > 0) {
        println(nb_billets_10_EUR + " billet(s) de 10 €")
      }
      comptes(id) = comptes(id) - (montant_retrait.toDouble * taux_EUR_a_CHF)
    }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

  //Consultation du compte
  def consultation(id : Int, comptes : Array[Double]) : Unit = {
    printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

  //Changement de code pin
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var NewPIN = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toString
    while (NewPIN.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères")
      NewPIN = readLine("Entrez votre nouveau code PIN > ").toString
  }
    codespin(id) = NewPIN
    println("Le nouveau code PIN a été enregistré")
  }
}


object Main {
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    val comptes = Array.fill(nbclients)(1200.0)
    val codespin = Array.fill(nbclients)("INTRO1234")
    val gestionnaire = new Gestion_Identifiant_CodesPIN(comptes, codespin, nbclients)

    while (true) {
      val id = readLine("Saisissez votre code identifiant > ").toInt

      if (!gestionnaire.verifier_Identifiant(id)) {
        println("Cet identifiant n’est pas valable.")
        return // Arrête le programme si l'identifiant est invalide
      }

      var tentatives = 3
      var codePIN_correct = false
      while (tentatives > 0 && !codePIN_correct) {
        val pin = readLine("Saisissez votre code pin > ")
        if (gestionnaire.verifier_CodePIN(id, pin)) {
          codePIN_correct = true
        } else {
          tentatives -= 1
          println("Code pin erroné, il vous reste " + tentatives + " tentatives > ")
        }
      }

      if (codePIN_correct) {
        println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
        var choix = readLine("Votre choix : ").toInt
        while (choix < 1 || choix > 5) {
          println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
          choix = readLine().toInt
        }
        while (choix != 5) {
          if (choix == 1) {
            val gestionnaire_choix1 = new Gestion_Choix(comptes, codespin)
            gestionnaire_choix1.depot(id, comptes)
            println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
            choix = readLine("Votre choix : ").toInt
            while (choix < 1 || choix > 5) {
              println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
              choix = readLine().toInt
            }
          } 
        
          if (choix == 2) {
            val gestionnaire_choix2 = new Gestion_Choix(comptes, codespin)
          gestionnaire_choix2.retrait(id, comptes)
            println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
            choix = readLine("Votre choix : ").toInt
            while (choix < 1 || choix > 5) {
              println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
              choix = readLine().toInt
            }
          }

          if (choix == 3) {
            val gestionnaire_choix3 = new Gestion_Choix(comptes, codespin)
          gestionnaire_choix3.consultation(id, comptes)
            println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
            choix = readLine("Votre choix : ").toInt
            while (choix < 1 || choix > 5) {
              println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
              choix = readLine().toInt
            }
          }

          if (choix == 4) {
            val gestionnaire_choix4 = new Gestion_Choix(comptes, codespin)
          gestionnaire_choix4.changepin(id, codespin)
            println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
            choix = readLine("Votre choix : ").toInt
            while (choix < 1 || choix > 5) {
              println("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement de code pin\n5) Terminer")
              choix = readLine().toInt
            }
          }
        }
        if (choix == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        }
      } else {
        println("Trop d’erreurs, abandon de l’identification")
      }
    }
  }
}