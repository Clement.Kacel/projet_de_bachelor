//Assignment: Roman Parfeniuk_996502_assignsubmission_file

import scala.io.StdIn
import scala.math.floor
import scala.collection.mutable.ListBuffer

object Main {

  case class Client(id: Int, pin: String, var solde: Double)

  def main(args: Array[String]): Unit = {
    val nbclients = 100
    val codespin: Array[String] = Array.fill(nbclients)("INTRO1234")
    var comptes: Array[Double] = Array.fill(nbclients)(1200.0)

    var id: Int = -1

    while (true) { 
      var idVerif = false
      while (!idVerif) {

        // IDENTIFIANT
        print("Saisissez votre code identifiant >  ")
        val idSaisi: Int = StdIn.readInt()

        if (idSaisi >= 0 && idSaisi < nbclients) {
          idVerif = true
          var essaiRestant = 3
          var pinCorrect = false

          // CODE PIN
          while (essaiRestant > 0 && !pinCorrect) {
            print("Saisissez votre code pin > ")
            val codeInjecte: String = StdIn.readLine()


            if (codeInjecte == codespin(idSaisi)) {
              println("----------\nBienvenue!\n----------")
              pinCorrect = true
            } else {
              essaiRestant -= 1
              println(s"Code pin erroné, il vous reste $essaiRestant tentatives >\n")
            }
          }
            // ERROR PIN ET TENTATIVES
          if (!pinCorrect) {
            println("Trop d’erreurs, abandon de l’identification\n")
            idVerif = false
          } else {
            id = idSaisi
          }
        } else {
          println("Cet identifiant n’est pas valable.")
        }
      }
      // MENU
      var choixUtilisateur: Int = 0
      do {
        print("""Choisissez votre opération :
                ----------
                1) Dépôt 
                2) Retrait 
                3) Consultation du compte 
                4) Changement du code pin 
                5) Terminer 
                ----------
                Votre choix : """)
        choixUtilisateur = StdIn.readInt()

        if (choixUtilisateur == 1) {
          depot(id, comptes)
        } else if (choixUtilisateur == 2) {
          retrait(id, comptes)
        } else if (choixUtilisateur == 3) {
          consultation(id, comptes)
        } else if (choixUtilisateur == 4) {
          changepin(id, codespin)
        } else if (choixUtilisateur == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.\n")
        } else {
          println("\nErreur : choix invalide")
        }
      } while (choixUtilisateur != 5)
    }

    // 1) Dépôt

    def depot(id: Int, comptes: Array[Double]): Unit = {
      print("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")

      var DevDep = 0

      do {
        DevDep = StdIn.readInt()

        if (DevDep != 1 && DevDep != 2) {
          print("Erreur : Veuillez choisir 1 (CHF) ou 2 (EUR) -> ")
        }
      } while (DevDep != 1 && DevDep != 2)

      print("Indiquez le montant du dépôt ->")


      var deposit = 0

      do {
        deposit = StdIn.readInt()

        if (deposit % 10 != 0) {
          println("Erreur : Veuillez entrer un montant multiple de 10")
        }

      } while (deposit % 10 != 0)

      //CONVERTIR LE MONTANT EN CHF

      var NewMontant = comptes(id)

      if (DevDep == 2) {
        val tauxEUR = 0.95
        val MontantDepoEur = deposit * tauxEUR
        println("Montant converti en CHF : " + MontantDepoEur)
        NewMontant += MontantDepoEur

      } else if (DevDep == 1) {

        val tauxCHF = 1.05
        val MontantDepoChf = deposit * tauxCHF
        println("Montant converti en EUR : " + MontantDepoChf)
        NewMontant += deposit

      }

      comptes(id) = NewMontant

      println(s"Le dépôt de $deposit a été effectué avec succès. Nouveau solde : ${comptes(id)}")
    }

    // 2) Retrait 

    def retrait(id: Int, comptes: Array[Double]): Unit = {
      print(" Indiquez la devise : 1 CHF, 2 EUR -> ")

      var IndiqDev = 0

      do {
        IndiqDev = StdIn.readInt()

        if (IndiqDev != 1 && IndiqDev != 2) {
          print("Erreur : Veuillez choisir 1 (CHF) ou 2 (EUR). -> ")
        }

      } while (IndiqDev != 1 && IndiqDev != 2)

      print("Indiquez le montant devise -> ")

      var MontDevise = 0
      val limite = comptes(id) * 0.1

      do {
        MontDevise = StdIn.readInt()

        if (MontDevise % 10 != 0 || MontDevise < 0 || MontDevise > limite) {
          println("Erreur : Veuillez entrer un montant multiple de 10 ou egal à zero la limite de retrait autorisee  " + limite + "  -> ")
        }

      } while (MontDevise % 10 != 0 || MontDevise < 0 || MontDevise > comptes(id) * 0.1)

      var NewMontant = comptes(id)
      var choixBillets = 0
      if (IndiqDev == 2) {
        val tauxCHFdevise = 0.95
        val MontantDeviseCHF = MontDevise * tauxCHFdevise
        println("montant à retirer en CHF : " + MontantDeviseCHF)

        // EUR

        BilletsEur(MontDevise)

        NewMontant -= MontantDeviseCHF
      } else if (IndiqDev == 1) {

        println("montant à retirer en CHF : " + MontDevise)

        // CHF

        BilletsChf(MontDevise)

        NewMontant -= MontDevise
      }
      comptes(id) = NewMontant
    }

    // COUPURES CHF
def BilletsChf(montantRetrait: Int): Unit = {
  var BillChf = Array(500, 200, 100, 50, 20, 10)
  var BillpetitChf = Array(100, 50, 20, 10)
  var montantRestant = montantRetrait
  var choix: Int = -1

  if (montantRestant >= 200) {
    println("En 1) grosses coupures, 2) petites coupures > ")

    while (choix != 1 && choix != 2) {
      println("Veuillez saisir 1 ou 2")
      choix = StdIn.readInt()
    }

    if (choix == 1) {
      while (montantRestant != 0) {
        var detailsBillets = ListBuffer[(Int, Int)]()

        for ((billet, index) <- BillChf.zipWithIndex) {
          if (montantRestant / billet != 0) {
            println(s"Vous pouvez obtenir au maximum de ${montantRestant / billet} billets de $billet CHF")
            println("Tapez 'o' pour ok ou toute autre valeur inférieure à celle proposée >")
            var saisieAlternative = StdIn.readLine()

            if (saisieAlternative == "o") {
              val nombreDeBillets = montantRestant / billet
              montantRestant -= billet * nombreDeBillets
              detailsBillets += ((billet, nombreDeBillets))

              if (montantRestant != 0) {
                println(s"Montant restant : $montantRestant CHF")
              } else {
                println("Veuillez retirer la somme demandée :")
                for ((billet, nombreDeBillets) <- detailsBillets) {
                  if (nombreDeBillets != 0) {
                    println(s"$nombreDeBillets billet(s) de $billet CHF")
                  }
                }
              }
            } else {
              val saisieAlternativeInt = saisieAlternative.toInt
              montantRestant -= saisieAlternativeInt * billet
              detailsBillets += ((billet, saisieAlternativeInt))
              println(s"Montant restant : $montantRestant CHF")
            }
          }
        }
      }
    } else if (choix == 2) {
      var montantRestant2 = montantRestant
      var detailsBillets = ListBuffer[(Int, Int)]()

      for ((billet, index) <- BillpetitChf.zipWithIndex) {
        while (montantRestant2 >= billet) {
          var nombreDeBillets = montantRestant2 / billet
          println(s"Vous pouvez obtenir au maximum de $nombreDeBillets billets de $billet CHF")
          print("Tapez 'o' pour ok ou toute autre valeur inférieure à celle proposée > ")
          var saisieAlternative = StdIn.readLine()

          if (saisieAlternative == "o") {
            val nombreDeBillets = montantRestant2 / billet
            montantRestant2 -= billet * nombreDeBillets
            detailsBillets += ((billet, nombreDeBillets))

            if (montantRestant2 != 0) {
              println(s"Montant restant : $montantRestant2 CHF")
            } else {
              println("Veuillez retirer la somme demandée :")
              for ((billet, nombreDeBillets) <- detailsBillets) {
                if (nombreDeBillets != 0) {
                  println(s"$nombreDeBillets billet(s) de $billet CHF")
                }
              }

              return
            }
          } else {
            val saisieAlternativeInt = saisieAlternative.toInt
            nombreDeBillets = saisieAlternativeInt
            montantRestant2 -= billet * nombreDeBillets
            detailsBillets += ((billet, nombreDeBillets))
            println(s"Montant restant : $montantRestant2 CHF")
          }
        }
      }
    }
  } else if (montantRestant <= 200) {
    var montantRestant2 = montantRestant
    var detailsBillets = ListBuffer[(Int, Int)]()

    for ((billet, index) <- BillpetitChf.zipWithIndex) {
      while (montantRestant2 >= billet) {
        var nombreDeBillets = montantRestant2 / billet
        println(s"Vous pouvez obtenir au maximum de $nombreDeBillets billets de $billet CHF")
        print("Tapez 'o' pour ok ou toute autre valeur inférieure à celle proposée > ")
        var saisieAlternative = StdIn.readLine()

        if (saisieAlternative == "o") {
          val nombreDeBillets = montantRestant2 / billet
          montantRestant2 -= billet * nombreDeBillets
          detailsBillets += ((billet, nombreDeBillets))

          if (montantRestant2 != 0) {
            println(s"Montant restant : $montantRestant2 CHF")
          } else {
            println("Veuillez retirer la somme demandée :")
            for ((billet, nombreDeBillets) <- detailsBillets) {
              if (nombreDeBillets != 0) {
                println(s"$nombreDeBillets billet(s) de $billet CHF")
              }
            }

            return
          }
        } else {
          val saisieAlternativeInt = saisieAlternative.toInt
          nombreDeBillets = saisieAlternativeInt
          montantRestant2 -= billet * nombreDeBillets
          detailsBillets += ((billet, nombreDeBillets))
          println(s"Montant restant : $montantRestant2 CHF")
        }
      }
    }
  }
}



    // COUPURES EUR
    def BilletsEur(amount: Double): Unit = {
      val denominations = Array(100, 50, 20, 10)
      var remainingAmount = amount

      println(s"La répartition pourra se faire en :")

      for (banknote <- denominations) {
        val numberOfBills = (remainingAmount / banknote).toInt
        if (numberOfBills > 0) {
          println(s"$numberOfBills billet(s) de EUR $banknote")
          remainingAmount -= numberOfBills * banknote
        }
      }

      if (remainingAmount > 0.0) {
        println(s"Il reste $remainingAmount EUR à distribuer en pièces.")
      } else {
        println("Veuillez retirer la somme demandée.")
      }
    }

    // ____CONSULTATION DU COMPTE_____

    def consultation(id: Int, comptes: Array[Double]): Unit = {
      println(s"\nLe montant disponible sur votre compte est de : ${comptes(id)} CHF\n")
    }

    // ____CHANGE PIN CODE _____
    def changepin(id: Int, codespin: Array[String]): Unit = {
      var newpin = ""
      while (newpin.length < 8) {
        print("Veuillez entrer votre nouveau code pin -> ")
        newpin = StdIn.readLine()
        if (newpin.length < 8) {
          println("Votre PIN doit contenir au moins 8 caractères. Veuillez réessayer.")
        }
      }
      codespin(id) = newpin
    }
  }
}