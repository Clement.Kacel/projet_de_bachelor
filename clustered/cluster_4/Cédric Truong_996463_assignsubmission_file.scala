//Assignment: Cédric Truong_996463_assignsubmission_file

import scala.io.StdIn._

object Main {

  //fonction de changement de mot de passe
  def changepin(id : Int, codespin : Array[String]) :Unit ={
    print("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : ")
    codespin(id) = readLine()
    println()
    while (codespin(id).length < 8){
      print("Votre code pin ne contient pas au moins 8 caractères : ")
      codespin(id) = readLine()
      println()
    }
  }

  
  //fonction de dépôt
  def depot(id : Int, comptes : Array[Double]) : Unit ={
    var montantDepot = 0
    //choix de la devise
    print("Indiquez la devise du dépôt :  CHF ou EUR : ")
    var devise = readLine()
    println()
    while (devise != "CHF" && devise != "EUR"){ //si la devise n'est pas "CHF" ou "EUR"
      print("Indiquez la devise du dépôt :  CHF ou EUR : ")
      devise = readLine()
      println()
    }
    //choix du montant de dépôt
    print("Indiquez le montant du dépôt : ")
    montantDepot = readInt()
    println()
    //boucle si le montant n'est pas correcte
    while ((montantDepot % 10 != 0) || (montantDepot <= 0)){
      if (montantDepot <= 0){ //si le dépôt n'est pas assez élevé 
        print("Ce dépôt est impossible. Veuillez entrer un montant plus élevé : ")
        montantDepot = readInt()
        println()
      } else if(montantDepot % 10 != 0){ //si le dépôt n'est pas divisible par 10
        println("Le montant doit être un multiple de 10 : ")
        montantDepot = readInt()
        println()
      }
    } 
    if (devise == "CHF"){ //si dépôt en CHF 
      comptes(id) = comptes(id) + montantDepot
    } else if (devise == "EUR"){ //si dépôt en EUR
      val ConversionDepot = montantDepot * 0.95
      comptes(id) = comptes(id) + ConversionDepot
    }
    printf("Votre dépot a été pris en compte, le nouveau montant disponible sur votre compte est de:  %.2f CHF \n", comptes(id))
    println()
  }

  //fonction de retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit ={

    //variable du montant de retrait
    var montantRetrait = 0
    
    //valeur du plafond de retrait
    val plafondRetrait = comptes(id) * 0.1
    
    //variable permettant le changement de billet pendant le découpage
    var changementBillet = 0
    
    //variable de décision de la coupure
    var coupure = 0

    //choix de la devise
    print("Indiquez la devise du retrait :  CHF ou EUR : ")
    var devise = readLine()
    println()

    //si la devise n'est pas "CHF" ou "EUR"
    while (devise != "CHF" && devise != "EUR"){ 
      print("Indiquez la devise du retrait :  CHF ou EUR : ")
      devise = readLine()
      println()
    }

    //1e saisie du montant de retrait
    print("Indiquez le montant du retrait : ") 
    montantRetrait = readInt()
    println()

    //boucle si le montant de retrait n'est pas bon
    while ((montantRetrait % 10 != 0) || (montantRetrait > plafondRetrait) || (montantRetrait <= 0)){ 

      //si le montant n'est pas divisible par 10 (et ne respecte pas le plafond)
      if (montantRetrait % 10 != 0){ 
        if (montantRetrait > plafondRetrait){
          println("Le montant doit être un multiple de 10 et votre plafond de retrait autorisé est de : "+ plafondRetrait)
          print("Indiquez le montant du retrait : ")
          montantRetrait = readInt()
          println()
          
        } else{
          println("Le montant doit être un multiple de 10")
          print("Indiquez le montant du retrait : ")
          montantRetrait = readInt()
          println()
        }
        
      } else if (montantRetrait > plafondRetrait){ //si le montant dépasse le plafond de retrait
        println("Votre plafond de retrait autorisé est de : "+ plafondRetrait)
        print("Indiquez le montant du retrait : ")
        montantRetrait = readInt()
        println()
        
      } else if (montantRetrait <= 0){ //si le retrait n'est pas assez élevé
        println("Ce retrait est impossible. Veuillez entrer un montant plus élevé.")
        print("Indiquez le montant du retrait : ")
        montantRetrait = readInt()
        println()
      }
    }
    if (devise == "CHF" && montantRetrait >= 200){ //si le montant est suffisamment grand pour choisir la coupure
      while ((coupure != 1) && (coupure != 2)){ //choix de la coupure
        print("En 1) grosses coupures, 2) petites coupures : ")
        coupure = readInt()
        println()
      } 
      if (coupure == 2){ //si retrait en petites coupures
        changementBillet = 2
      }   
    } else if (devise == "CHF" && montantRetrait < 200){ //si le montant est suffisamment petit pour choisir la coupure
      println("La somme sera retirée en petite coupure")
      changementBillet = 2
    }

    //listes des types de billets
    var BilletCHF = Array(500, 200, 100, 50, 20, 10)
    var BilletEUR = Array(100, 50, 20, 10)

    //variable pour stopper les boucles de découpage de billet
    var BoucleBilletCHF = true
    var BoucleBilletEUR = true
    
    //listes des nombres de billets
    var NbBilletCHF = Array(0,0,0,0,0,0)
    var NbBilletEUR = Array(0,0,0,0)

    //variable de décision du nb de billet
    var ChoixRetrait = " "

    
    if (devise == "CHF"){  //si le retrait est en CHF

      //retrait du compte
      comptes(id) = comptes(id) - montantRetrait

      //boucle de découpage des billets
      while (BoucleBilletCHF){

        //si la somme est entièrement découpée
        if (montantRetrait == 0){
          BoucleBilletCHF = !BoucleBilletCHF
        } else if(montantRetrait < BilletCHF(changementBillet)){ //Passage au prochain type de billet
          changementBillet += 1
        } else if (montantRetrait >= BilletCHF(changementBillet)){ //si la somme n'est pas encore entièrement découpée

          if (changementBillet < 5){
            println("Il reste "+ montantRetrait +" CHF à distribuer")
          }
          //boucle de comptage des billets
          while (montantRetrait >= BilletCHF(changementBillet)){
            montantRetrait -= BilletCHF(changementBillet)
            NbBilletCHF(changementBillet) += 1
          }

          //si découpage en billet de 10 CHF
          if (changementBillet == 5){
            BoucleBilletCHF = !BoucleBilletCHF
          } else if (changementBillet < 5){ //si découpage en billet de 20 CHF ou plus
            println("Vous pouvez obtenir au maximum "+ NbBilletCHF(changementBillet) +" billets de "+ BilletCHF(changementBillet)+" CHF")
            println()
            print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
            ChoixRetrait = readLine()
            println()

            //boucle si la confirmaion du retrait n'est pas bonne
            while ((ChoixRetrait != "o") && (ChoixRetrait.toInt >= NbBilletCHF(changementBillet))){
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée : " )
              ChoixRetrait = readLine()
              println()
            }
            if (ChoixRetrait == "o"){} //confirmation du 1er nb de billets
            else if (ChoixRetrait.toInt < NbBilletCHF(changementBillet)){ //nouveau nb de billets
              val NewChoix = ChoixRetrait.toInt
              montantRetrait = montantRetrait + (NbBilletCHF(changementBillet) - NewChoix) * BilletCHF(changementBillet)
              NbBilletCHF(changementBillet) = NewChoix
            }

            //Passage au prochain type de billet
            changementBillet += 1
          }
        }
      }
      //Affichage du découpage des billets
      if (NbBilletCHF(0) > 0){ //si le nb de billet de 500 CHF > 0
        println(s"${NbBilletCHF(0)} billet(s) de 500 CHF")
      }
      if (NbBilletCHF(1) > 0){ //si le nb de billet de 200 CHF > 0
        println(s"${NbBilletCHF(1)} billet(s) de 200 CHF")
      }
      if (NbBilletCHF(2) > 0){ //si le nb de billet de 100 CHF > 0
        println(s"${NbBilletCHF(2)} billet(s) de 100 CHF")
      }
      if (NbBilletCHF(3) > 0){ //si le nb de billet de 50 CHF > 0
        println(s"${NbBilletCHF(3)} billet(s) de 50 CHF")
      }
      if (NbBilletCHF(4) > 0){ //si le nb de billet de 20 CHF > 0
        println(s"${NbBilletCHF(4)} billet(s) de 20 CHF")
      }
      if (NbBilletCHF(5) > 0){ //si le nb de billet de 10 CHF > 0
        println(s"${NbBilletCHF(5)} billet(s) de 10 CHF")
      }

    } else if (devise == "EUR"){//si le retrait est en EUR

      //retrait du compte
      comptes(id) = comptes(id) - montantRetrait * 0.95

      //boucle de découpage des billets
      while (BoucleBilletEUR){

        //si la somme est entièrement découpée
        if (montantRetrait == 0){
          BoucleBilletEUR = !BoucleBilletEUR
        } else if(montantRetrait < BilletEUR(changementBillet)){ //Passage au prochain type de billet
          changementBillet += 1
        } else if (montantRetrait >= BilletEUR(changementBillet)){ //si la somme n'est pas encore entièrement découpée
          if(changementBillet < 3){
            println("Il reste "+ montantRetrait +" € à distribuer")
          }
          //boucle de comptage des billets
          while (montantRetrait >= BilletEUR(changementBillet)){
            montantRetrait -= BilletEUR(changementBillet)
            NbBilletEUR(changementBillet) += 1
          }
          //si découpage en billet de 10 €
          if (changementBillet == 3){
            BoucleBilletEUR = !BoucleBilletEUR
          } else if (changementBillet < 3){ //si découpage en billet de 20 € ou plus
            println("Vous pouvez obtenir au maximum "+ NbBilletEUR(changementBillet) +" billets de "+ BilletEUR(changementBillet) +" €")
            println()
            print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ")
            ChoixRetrait = readLine()
            println()
            while ((ChoixRetrait != "o") && (ChoixRetrait.toInt >= NbBilletEUR(changementBillet))){
              print("Tapez o pour ok ou une autre valeur inférieure à celle proposée : " )
              ChoixRetrait = readLine()
              println()
            }
            if (ChoixRetrait == "o"){} //confirmation du 1er nb de billets
            else if (ChoixRetrait.toInt < NbBilletEUR(changementBillet)){ //nouveau nb de billets
              val NewChoix = ChoixRetrait.toInt
              montantRetrait = montantRetrait + (NbBilletEUR(changementBillet) - NewChoix) * BilletEUR(changementBillet)
              NbBilletEUR(changementBillet) = NewChoix
            }

            //Passage au prochain type de billet
            changementBillet += 1
          }
        }
      }
      //Affichage du découpage des billets
      if (NbBilletEUR(0) > 0){ //si le nb de billet de 100 € > 0
        println(s"${NbBilletEUR(0)} billet(s) de 100 €")
      }
      if (NbBilletEUR(1) > 0){ //si le nb de billet de 50 € > 0
        println(s"${NbBilletEUR(1)} billet(s) de 50 €")
      }
      if (NbBilletEUR(2) > 0){ //si le nb de billet de 20 € > 0
        println(s"${NbBilletEUR(2)} billet(s) de 20 €")
      }
      if (NbBilletEUR(3) > 0){ //si le nb de billet de 10 € > 0
        println(s"${NbBilletEUR(3)} billet(s) de 10 €")
      }
    } 
    println()
  }
  
  def main(args: Array[String]): Unit = {
    
    //var pour faire boucler les opérations
    var BoucleChoix = true

    //variable et liste des comptes
    val NbClients = 100
    var comptes = Array.fill(NbClients)(1200.0)
    var codespin = Array.fill(NbClients)("INTRO1234")

    //variables de mot de passe
    var NbEssai = 3
    var password = " "

    //variable de choix d'opération
    var Choix = 0

    //1er entrée de l'identifiant
    print("Saisissez votre code identifiant : ")
    var id = readInt()
    println()
    //si identifiant trop grand
    if (id > NbClients) {
      println("Cet identifiant n’est pas valable.")
      BoucleChoix = !BoucleChoix
    }
    

    //boucle des opérations
    while (BoucleChoix) {

      //Boucle password
      while(password != codespin(id)){
        print("Saisissez votre code pin : ")
        password = readLine()
        println()
        //si le code est faux
        if (password != codespin(id)){ 
          NbEssai -= 1
          println("Code pin erroné, il vous reste "+ NbEssai +" tentatives")
        }
        //si le nb de tentative est dépassé
        if (NbEssai == 0){ 
          println()
          println("Trop d’erreurs, abandon de l’identification")
          println()
          NbEssai = 3
          print("Saisissez votre code identifiant : ")
          id = readInt()
          println()
          //si identifiant trop grand
          if (id > NbClients) {
            println("Cet identifiant n’est pas valable.")
            BoucleChoix = !BoucleChoix
          }
        }
      }
      //si le password est bon
        while (Choix != 5){ //boucle quand les opération ne sont pas terminées
          println("Choisissez votre opération :")
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          print("Votre choix :  ")
          Choix = readInt()
          println()
          
          if (Choix == 1){ //Dépôt
            depot(id, comptes)

          } else if (Choix == 2){ //Retrait
            retrait(id, comptes)

          } else if (Choix == 3){ //Consultation de compte
            printf("Le montant disponible sur votre compte est de: %.2f CHF \n", comptes(id))
            println()
            
          } else if (Choix == 4){ //Changement de mot de passe
            changepin(id, codespin)
          }
        }
      if (Choix == 5){ //Terminer
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        println()
        NbEssai = 3
        password = ""
        Choix = 0
        print("Saisissez votre code identifiant :  ")
        id = readInt()
        println()
        //si identifiant trop grand
        if (id > NbClients) {
          println("Cet identifiant n’est pas valable.")
          BoucleChoix = !BoucleChoix
        }
      }
      
    }
  }
}


