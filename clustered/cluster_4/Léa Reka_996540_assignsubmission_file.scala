//Assignment: Léa Reka_996540_assignsubmission_file

import scala.io.StdIn._

object Main {
  def main(args: Array[String]): Unit = {

    val nbclients = 100

    var comptes = Array.fill[Double](nbclients)(1200.0)
    var codespin = Array.fill[String](nbclients)("INTRO1234")

    var menu = true
    var tentatives = 3
    var demandepin = true
    var demandeId = true
    var estAuthentifie = false
    val tauxConversionEURenCHF = 0.95
    val tauxConversionCHFenEUR = 1.05
    var montantInitiale: Double = 1200.0
    var idClient: Option[Int] = None
    var montantRetrait = 0
    var montantDepot: Double = 0.0

    
    // depot 
    def depot(id: Int, comptes: Array[Double]): Unit = {
      var devise = 0
      println("Indiquez la devise : 1 CHF, 2 : EUR >")
      devise = readInt()
      while (!(devise == 1 || devise == 2)) {
        println("Indiquez la devise : 1 CHF, 2 : EUR >")
        devise = readInt()
      }

      println("Indiquez le montant du dépôt > ")
      var montantDepot = readDouble()

      // multiple de 10
      while (montantDepot % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
        println("Indiquez le montant du dépôt > ")
        montantDepot = readDouble()
      }

      if (devise == 1) {
        comptes(id) += montantDepot
        println("Votre dépôt en EUR a été pris en compte, le nouveau montant disponible sur votre compte est de : " + {comptes(id)} + " CHF")
      } else {
        // Convertir de EUR en CHF
        val montantDepotCHF = montantDepot * tauxConversionEURenCHF
        comptes(id) += montantDepotCHF
        println("Votre dépôt en CHF a été pris en compte, le nouveau montant disponible sur votre compte est de : " + {comptes(id)} + " CHF")
      }
    }


    // retrait
    def retrait(id : Int, comptes : Array[Double]) : Unit = {
      var deviseRetrait = 0
        var choixCoupures = 0

        // devise
        println("Indiquez la devise :1 CHF, 2 : EUR > ")
        deviseRetrait = readInt()
        while (!(deviseRetrait == 1 || deviseRetrait == 2)) {
          println("Indiquez la devise :1 CHF, 2 : EUR > ")
          deviseRetrait = readInt()
        }

        // plafond
        println("Indiquez le montant du retrait > ")
        montantRetrait = readInt()
        var plafondRetrait = comptes(id) * 0.1
        while (plafondRetrait < montantRetrait) {
          println("Votre plafond de retrait autorisé est de : " + plafondRetrait)
          println("Indiquez le montant du retrait > ")
          montantRetrait = readInt()
          while (plafondRetrait < montantRetrait) {
            println("Votre plafond de retrait autorisé est de : " + plafondRetrait)
            println("Indiquez le montant du retrait > ")
            montantRetrait = readInt()
          }
        }

        // multiple de 10
        while (montantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
          println("Indiquez le montant du retrait > ")
          montantRetrait = readInt()
        }

        // verification si le montant peut être réparti en grosses coupures
        if (montantRetrait >= 200 && deviseRetrait == 1) {
          println("Choisissez le type de coupures :")
          println("1) Grosses coupures")
          println("2) Petites coupures")
          println("Votre choix : ")
          choixCoupures = readInt()
          while (!(choixCoupures == 1 || choixCoupures == 2)) {
            println("Choisissez le type de coupures :")
            println("1) Grosses coupures")
            println("2) Petites coupures")
            println("Votre choix : ")
            choixCoupures = readInt()
          }
        } else {
          choixCoupures = 2
        }

        // retrait en CHF
        if (deviseRetrait == 1) {
          val coupuresCHFGrosses = List(500, 200, 100, 50, 20, 10)
          val coupuresCHFPetites = List(100, 50, 20, 10)
          var reste = montantRetrait
          var retraitFinal = Map[Int, Int]()

          var exitLoop = false

          while (reste > 0 && !exitLoop) {
            val coupures = if (deviseRetrait == 1) {
              coupuresCHFGrosses
            } else {
              coupuresCHFPetites
            }
            for (coupure <- coupures) {
              if (reste >= coupure) {
                println("Il reste " + reste + " CHF à distribuer")
                println("Vous pouvez obtenir au maximum " + { reste / coupure } + " billet(s) de " + coupure + " CHF")
                print("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

                val choix = readLine()
                if (choix == "o") {
                  val nombreCoupures = reste / coupure
                  retraitFinal = retraitFinal + (coupure -> nombreCoupures)
                  reste = reste - nombreCoupures * coupure
                } else {
                  exitLoop = true
                }
              }
            }
          }

          if (reste == 0) {
            println("Veuillez retirer la somme demandée :")
            for ((coupure, nombre) <- retraitFinal) {
              if (nombre > 0) {
                println(nombre + " billet(s) de " + coupure + " CHF")
              }
            }
          }

          // Mettre à jour le montant du compte en banque en CHF
          comptes(id) = comptes(id) - montantRetrait
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        }

        // retrait en EUR
        else if (deviseRetrait == 2) {
          val coupuresEUR = List(100, 50, 20, 10)
          var reste = montantRetrait
          var retraitFinal = Map[Int, Int]()

          var exitLoop = false

          while (reste > 0 && !exitLoop) {
            for (coupure <- coupuresEUR) {
              if (reste >= coupure) {
                println("Il reste " + reste + " EUR à distribuer")
                println("Vous pouvez obtenir au maximum " + { reste / coupure } + " billet(s) de " + coupure + " EUR")
                print("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

                val choix = readLine()
                if (choix == "o") {
                  val nombreCoupures = reste / coupure
                  retraitFinal = retraitFinal + (coupure -> nombreCoupures)
                  reste = reste - nombreCoupures * coupure
                } else {
                  exitLoop = true
                }
              }
            }
          }

          if (reste == 0) {
            println("Veuillez retirer la somme demandée :")
            for ((coupure, nombre) <- retraitFinal) {
              if (nombre > 0) {
                println(nombre + " billet(s) de " + coupure + " EUR")
              }
            }
          }

          // Mettre à jour le montant du compte en banque en CHF
          comptes(id) = comptes(id) - montantRetrait * tauxConversionEURenCHF
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        }
    }


    // changement pin
    def changepin(id: Int, codespin: Array[String]): Unit = {
      var nouveauCodePin = ""
      do {
        println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        nouveauCodePin = readLine()
  
        if (nouveauCodePin.length < 8) {
          println("Votre code pin ne contient pas au moins 8 caractères.")
        }
      } while (nouveauCodePin.length < 8)
  
      // Mettre à jour le code pin
      codespin(id) = nouveauCodePin
      println("Le code pin a été mis à jour avec succès.")
    }

    

    // Lancement du programme
    while (demandeId) {
      println("Saisissez votre identifiant > ")
      val saisieId = readInt()

      if (saisieId >= 0 && saisieId < nbclients && codespin(saisieId) != null) {
        idClient = Some(saisieId)
        demandepin = true
        tentatives = 3

        // Demande du code pin
        while (tentatives > 0 && !estAuthentifie && demandepin) {
          print("Saisissez votre code pin > ")
          val saisieCodePin = readLine()

          if (saisieCodePin == codespin(idClient.get)) {
            estAuthentifie = true
            println("Code pin correct. Accès autorisé.")
            demandeId = false
            demandepin = false

          } else {
            tentatives = tentatives - 1
            if (tentatives > 0) {
              println(s"Code pin erroné, il vous reste $tentatives tentatives.")
            } else {
              println("Trop d’erreurs, abandon de l’identification")

            }
          }
        }

        // Réinitialisation des variables
        estAuthentifie = false
      } else {
        println("Cet identifiant n’est pas valable.")
        // Le programme s'arrête si l'identifiant n'est pas valable

      }
    }

    while (menu) {
      println("Choisissez votre opération :")
      println("51) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")
      val choix = readInt()


      
      // dépot
      if (choix == 1) {
        depot(idClient.get, comptes)
      }


      // Retrait
      if (choix == 2) {
        retrait(idClient.get, comptes)
      }

      // Consultation du compte
      if (choix == 3) {
        idClient.foreach { id =>
          comptes(id) = comptes(id) 
          println("Le montant disponible sur votre compte est de : " +  comptes(id))
        }
      }

      // changement de code pin
      if (choix == 4) {
        changepin(idClient.get, codespin)
      }


      // terminer
      if (choix == 5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        demandeId = true
        demandepin = true

        while (demandeId) {
          println("Saisissez votre identifiant > ")
          val saisieId = readInt()

          if (saisieId >= 0 && saisieId < nbclients && codespin(saisieId) != null) {
            idClient = Some(saisieId)
            demandepin = true
            tentatives = 3

            // Demande du code pin
            while (tentatives > 0 && !estAuthentifie && demandepin) {
              print("Saisissez votre code pin > ")
              val saisieCodePin = readLine()

              if (saisieCodePin == codespin(idClient.get)) {
                estAuthentifie = true
                println("Code pin correct. Accès autorisé.")
                demandeId = false
                demandepin = false

              } else {
                tentatives = tentatives - 1
                if (tentatives > 0) {
                  println(s"Code pin erroné, il vous reste $tentatives tentatives.")
                } else {
                  println("Trop d’erreurs, abandon de l’identification")

                }
              }
            }

            // Réinitialisation des variables
            estAuthentifie = false
          } else {
            println("Cet identifiant n’est pas valable.")
            // Le programme s'arrête si l'identifiant n'est pas valable

          }
        }
      } 
    }
  }
}

