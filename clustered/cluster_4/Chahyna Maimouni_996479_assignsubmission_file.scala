//Assignment: Chahyna Maimouni_996479_assignsubmission_file

//Exercice 2 : Suite Banque
import scala.io.StdIn._
object Main {
  
  //Fonction dépot (1)
  def depot(id: Int, comptes: Array[Double]): Unit = {
    var deviseDepot = 0
    //tant que devise n'est pas égal à 1 ou 2
    while (deviseDepot != 1 && deviseDepot != 2) {
      deviseDepot = readLine("Indiquez la devise du dépôt : 1) CHF  2) EUR > ").toInt
    } //fin du while qui demande une devise

    //test pour que montant soit multiple de 10
    var montantDepot = 1.0
    while (montantDepot % 10 != 0) {
      montantDepot = readLine("Indiquez le montant du dépot > ").toDouble
      if (montantDepot % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
      } //fin du if multiple de 10
    } //fin while montant 
    
    //si devise égal à 2, euros
    if (deviseDepot == 2) { 
      //formule pour convertir
      montantDepot = 0.95 * montantDepot.toDouble 
    } //fin du if euro
    
    //ajout du montant dans compte 
    comptes(id) += montantDepot
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :\n %.2f \n", comptes(id))
  } //fin def dépot
  


  //Fonction retrait (2)
  def retrait(id: Int, comptes: Array[Double]): Unit = {
    var deviseRetrait = 0
    while (deviseRetrait != 1 && deviseRetrait != 2) {
      deviseRetrait = readLine("Indiquez la devise du dépôt : 1) CHF  2) EUR > ").toInt
    } //fin du while devise
    
    var montantRetrait = 1
    var testMontant: Boolean = false
    var retraitAutorise = comptes(id) - comptes(id) * 0.1
    
    // while retrait
    while (!testMontant) {
      montantRetrait = readLine("Indiquez le montant du retrait > ").toInt
      if (montantRetrait % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
      } 
      else if (montantRetrait > retraitAutorise) {
        println("Votre plafond de retrait autorisé est de : " + retraitAutorise)
      }
      else { 
        testMontant = true 
      }
    } //fin du while montant autorité
    var choixCoupure = 0
    // si CHF ET >= 200 on donne le choix pour les grosses coupures ou pas.
    if (deviseRetrait == 1 && montantRetrait >= 200) { 
      while (choixCoupure != 1 && choixCoupure != 2) {
        choixCoupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
      }
    } 
    // pour les euros seulement petites coupures
    else { 
      choixCoupure = 2
    }
    
    // Map : on associe une clé à une valeur, ici : clé = valeur du billet, "valeur" = valeur du billet
    var billets = Map((500, 0), (200, 0), (100, 0), (50, 0), (20, 0), (10, 0))
    var coupures = List(1,2,3)
    var grosseCoupures = List(500, 200, 100, 50, 20, 10)
    var petitesCoupures = List(100, 50, 20, 10)
    var choixBillet = ""
    
    // si c'est des CHF
    //si choix de Grosses coupures
    if (deviseRetrait == 1) { 
      comptes(id) -= montantRetrait
      //dépend des coupures choisies
      if (choixCoupure == 1) {
        var grosseCoupures = List(500, 200, 100, 50, 20, 10)
        for (coup <- grosseCoupures) {
          var nbbilletsPossibles = montantRetrait / coup
          if (nbbilletsPossibles != 0) {
            println("Il reste " + montantRetrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nbbilletsPossibles + " billet(s) de " + coup + " CHF")

            if (coup == 10) { // on laisse pas le choix si on arrive au billet de 10.
              choixBillet = "o"
            }

            testMontant = false
            while (!testMontant && choixBillet != "o") {
              choixBillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              if (choixBillet == "o") {
                testMontant = true
              } 
              else {
                //test pour erreur
                try {
                  if (choixBillet.toInt < nbbilletsPossibles) {
                    testMontant = true
                  }
                } //fin try
                // si on donne par exemple 'djfsf' : c'est pas un nombre, et c'est pas 'o'
                catch { 
                  // on fait rien quand on a cette erreur, on redemande juste d'entrer une valeur
                  case e: NumberFormatException => () 
                }
              } // else pour erreur
            } //fin while pour le "o"

            if (choixBillet == "o") {
              billets = billets.updated(coup, nbbilletsPossibles)
              montantRetrait -= nbbilletsPossibles * coup
            } //fin du if "o"

            else {
              billets = billets.updated(coup, choixBillet.toInt)
              montantRetrait -= choixBillet.toInt * coup
            } //fin else
          } //if billet possible
        } //for pour billets


        println("Veuillez retirer la somme demandée : ")
        // on affiche les résultats (b = type de billet, n = nombre de ce billet)
        for (b <- grosseCoupures) { 
          if (billets(b) != 0) { // on affiche seulement les billets dont le nombre n'est pas 0
            println(billets(b) + " billet(s) de " + b) 
          }
        } //fin du for (coupures) 
      } //fin if grosses coupures

      //Si petites coupures
      else {
        var petitesCoupures = List(100, 50, 20, 10)
        for (coup <- petitesCoupures) {
          var nbbilletsPossibles = montantRetrait / coup
          if (nbbilletsPossibles != 0) {
            println("Il reste " + montantRetrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nbbilletsPossibles + " billet(s) de " + coup + " CHF")

            if (coup == 10) { // on laisse pas le choix si on arrive au billet de 10.
              choixBillet = "o"
            }

            testMontant = false
            while (!testMontant && choixBillet != "o") {
              choixBillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              if (choixBillet == "o") {
                testMontant = true
              } 
              else {
                //test pour erreur
                try {
                  if (choixBillet.toInt < nbbilletsPossibles) {
                    testMontant = true
                  }
                } //fin try
                // si on donne par exemple 'djfsf' : c'est pas un nombre, et c'est pas 'o'
                catch { 
                  // on fait rien quand on a cette erreur, on redemande juste d'entrer une valeur
                  case e: NumberFormatException => () 
                }
              } // else pour erreur
            } //fin while pour le "o"

            if (choixBillet == "o") {
              billets = billets.updated(coup, nbbilletsPossibles)
              montantRetrait -= nbbilletsPossibles * coup
            } //fin du if "o"

            else {
              billets = billets.updated(coup, choixBillet.toInt)
              montantRetrait -= choixBillet.toInt * coup
            } //fin else
          } //if billet possible
        } //for pour billets


        println("Veuillez retirer la somme demandée : ")
        // on affiche les résultats (b = type de billet, n = nombre de ce billet)
        for (b <- petitesCoupures) { 
          if (billets(b) != 0) { // on affiche seulement les billets dont le nombre n'est pas 0
            println(billets(b) + " billet(s) de " + b) 
          }
        } //fin du for (coupures)
      } //fin du else petites coupures
    } //fin retrait chf

    
    // si devise égal à 2, euro
    else if (deviseRetrait == 2) { 
      //calcul convertis en euro
      comptes(id) -= 0.95 * montantRetrait 
      //for parcours les coupures
      for (coup <- coupures) {
        var nbbilletsPossibles = montantRetrait / coup
        if (nbbilletsPossibles != 0) {
          println("Il reste " + montantRetrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nbbilletsPossibles + " billet(s) de " + coup + " EUR")
          
          // on laisse pas le choix si on arrive au billet de 10.
          if (coup == 10) { 
            choixBillet = "o"
          }
          testMontant = false
          while (!testMontant && choixBillet != "o") {
            choixBillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            if (choixBillet == "o") {
              testMontant = true
            } 
            else {
              //test pour erreur
              try {
                if (choixBillet.toInt < nbbilletsPossibles) {
                  testMontant = true
                }
              } //fin try
              // si on donne par exemple 'djfsf' : c'est pas un nombre, et c'est pas 'o'
              catch { 
                // on fait rien quand on a cette erreur, on redemande juste d'entrer une valeur
                case e: NumberFormatException =>() 
              }
            } // else pour erreur
          } //fin while pour le "o"

          //on s'arrête et programme donne ces coupures
          if (choixBillet == "o") {
            billets = billets.updated(coup, nbbilletsPossibles)
            montantRetrait -= nbbilletsPossibles * coup
          } //fin du if "o"

          //autres coupures proposés, mais plus petites que celles précédentes
          else {
            billets = billets.updated(coup, choixBillet.toInt)
            montantRetrait -= choixBillet.toInt * coup
          } //fin else
        } //if billet possible
      } //for pour billets

    println("Veuillez retirer la somme demandée : ")
    // on affiche les résultats (b = type de billet, n = nombre de ce billet)
    for (b <- coupures) { 
      if (billets(b) != 0) { // on affiche seulement les billets dont le nombre n'est pas 0
        println(billets(b) + " billet(s) de " + b) 
        }
      }
    }//FIN DEVISE = 2 (euros)

    //message qui s'affiche montant dispo après retrait
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n ",comptes(id))
  }

  //fonction pour changer mot de passe (4)
  def changepin(id: Int, codespin: Array[String]): Unit = {
    var nouveauPin = ""

    //condition de longueur
    while (nouveauPin.length() < 8) {
      nouveauPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      if (nouveauPin.length() < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      } //fin if longueur, mot de passe
    } // fin du while mot de passe
    codespin(id) = nouveauPin
  } //fin def changer mot de passe
  
  
  // connexion de base
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")

    while (true) {
      var client_ID = readLine("Saisissez votre code identifiant > ").toInt
      //si on entre par exemple 101 ou plus
      if (client_ID >= nbclients){
        println("Cet identifiant n'est pas valable.")
        //arret total
        System.exit(0)
      }
      
      var tentativesRestantes = 3
      var pin_tentative = ""
      
      while (pin_tentative != codespin(client_ID) && tentativesRestantes > 0) {
        tentativesRestantes -= 1
        
        pin_tentative = readLine("Saisissez votre code pin > ")
        if (pin_tentative != codespin(client_ID) && tentativesRestantes > 0) {
          println("Code pin erroné, il vous reste " + tentativesRestantes + " tentatives.")
        }
        
        if (tentativesRestantes == 0) {
          println("Trop d'erreurs, abandon de l'identification")
        } //fin if des tentatives
        
      }//fin while codepin/tentatives
      println(pin_tentative)

      //Affichage des choix quand mot de passe juste
      if (pin_tentative == codespin(client_ID)) {
        var choix = 0
        var continueOperations: Boolean = true
        
        while (continueOperations){
          choix = readLine("Choisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : ").toInt

          if(choix == 1){
            depot(client_ID, comptes)
          }
          
          else if(choix == 2){
            retrait(client_ID, comptes)
          }
          
          else if(choix == 3){
            printf("Le montant disponible sur votre compte est de : %.2f \n",comptes(client_ID))
          }
          
          else if(choix == 4){
            changepin(client_ID, codespin)
          }
          
          else if(choix == 5){
            println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
            continueOperations = false
          }

        } //fin while choix

      }//if choix

    }// while identifiant
  } // def
} //object
