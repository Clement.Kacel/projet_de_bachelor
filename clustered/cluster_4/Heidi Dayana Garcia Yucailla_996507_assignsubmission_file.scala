//Assignment: Heidi Dayana Garcia Yucailla_996507_assignsubmission_file

import scala.io.StdIn._

object Main {

  // définition de la méthode dépot :
  def depot(id: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la dévise du dépôt :\n" + 
            "1) CHF\n" +
            "2) EUR > ")
    var devise = readInt()
    while(devise > 2 || devise < 1){
      println("Indiquez la dévise du dépôt :\n" + 
              "1) CHF\n" +
              "2) EUR > ")
      devise = readInt()
    }
    println("Indiquez le montant du dépôt.")
    var montantDepot = readInt()

    while(montantDepot % 10 != 0){
      println("Le montant doit être un multiple de 10")
      println("Indiquez le montant du dépôt.")
      montantDepot = readInt()
    }
    if(devise == 1){
      comptes(id) += montantDepot.toDouble
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
    }
    if(devise == 2){
      comptes(id) = montantDepot * 0.95 
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
    }
  }

  // définition de la méthode retrait :
  def retrait(id: Int, comptes: Array[Double]): Unit = {
    //println ("0k") (c'était pour voir si ça marchait) 
    var billet10 = 0
    var billet20 = 0
    var billet50 = 0
    var billet100 = 0
    var billet200 = 0
    var billet500 = 0
    var montantRetrait = 0
    var devise = 0
    var coupures = 0
    var deviseTexte = "devise"
    var retraitTotal = 0

    println("Indiquez la dévise du dépôt :\n" + 
            "1) CHF\n" +
            "2) EUR > ")
    devise = readInt()
    while (devise > 2 || devise < 1) {
      println("Indiquez la dévise du dépôt :\n" + 
              "1) CHF\n" +
              "2) EUR > ")
      devise = readInt()
    }
    if (devise == 1) {
      deviseTexte = "CHF"
    }
    if (devise == 2) {
      deviseTexte = "EUR"
    }
    println("Indiquez le montant du retrait")
    montantRetrait = readInt()
    while (montantRetrait % 10 != 0 || montantRetrait > comptes(id) / 10) {
      if (montantRetrait % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
      if (montantRetrait > comptes(id) / 10) {
        println("Votre plafond de retrait autorisé est de : " + comptes(id) / 10)
      }
      println("Indiquez le montant du retrait")
      montantRetrait = readInt()
    }
    retraitTotal = montantRetrait
    if (devise == 1 && montantRetrait >= 200) {
      println("En 1) grosses coupures, 2) petites coupures")
      coupures = readInt()
      while (coupures > 2 || devise < 1) {
        println("En 1) grosses coupures, 2) petites coupures")
        coupures = readInt()
      }
    }
    while (montantRetrait > 0) {
      if (coupures == 1 && devise == 1) {
        if (montantRetrait >= 500 && coupures == 1) {
          println("Il reste " + montantRetrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + (montantRetrait / 500) + " billet(s) de 500 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if (input == "o") {
            billet500 = montantRetrait / 500.toInt
          } else {
            billet500 = input.toInt
          }
          while (billet500 > montantRetrait / 500.toInt) {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            var input = readLine()
            if (input == "o") {
              billet500 = montantRetrait / 500.toInt
            } else {
              billet500 = input.toInt
            }
          }
          montantRetrait = montantRetrait - 500 * billet500
        }
      }
      if(montantRetrait >= 200 && coupures == 1 && devise == 1){
        println("Il reste " + montantRetrait + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + (montantRetrait/200) + " billet(s) de 200 CHF")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
        var input = readLine()
        if(input == "o"){
          billet200 = montantRetrait/200.toInt
        } else{
          billet200 = input.toInt
        }
        while(billet200 > montantRetrait/200.toInt){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            billet200 = montantRetrait/200.toInt
          } else{
            billet200 = input.toInt
          }
        }
        montantRetrait = montantRetrait - 200*billet200
      }
      if(montantRetrait >= 100){
        println("Il reste " + montantRetrait + deviseTexte + " à distribuer")
        println("Vous pouvez obtenir au maximum " + (montantRetrait/100) + " billet(s) de 100 " + deviseTexte)
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
        var input = readLine()
        if(input == "o"){
          billet100 = montantRetrait/100.toInt
        }else{
          billet100 = input.toInt
        }
        while(billet100 > montantRetrait/100.toInt){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            billet100 = montantRetrait/100.toInt
          }else{
            billet100 = input.toInt
          }
        }
        montantRetrait = montantRetrait - 100*billet100
      }
      if(montantRetrait >= 50){
        println("Il reste " + montantRetrait + deviseTexte + " à distribuer")
        println("Vous pouvez obtenir au maximum " + (montantRetrait/50) + " billet(s) de 50 " + deviseTexte)
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
        var input = readLine()
        if(input == "o"){
          billet50 = montantRetrait/50.toInt
        }else{
          billet50 = input.toInt
        }
        while(billet50 > montantRetrait/50){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            billet50 = montantRetrait/50.toInt
          }else{
            billet50 = input.toInt
          }
        }
        montantRetrait = montantRetrait - 50*billet50
      }
      if(montantRetrait >= 20){
        println("Il reste " + montantRetrait + deviseTexte + " à distribuer")
        println("Vous pouvez obtenir au maximum " + (montantRetrait/20) + " billet(s) de 20 " + deviseTexte)
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
        var input = readLine()
        if(input == "o"){
          billet20 = montantRetrait/20.toInt
        }else{
          billet20 = input.toInt
        }
        while(billet20 > montantRetrait/20.toInt){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            billet20 = montantRetrait/20.toInt
          }else{
            billet20 = input.toInt
          }
        }
        montantRetrait = montantRetrait - 20*billet20
      }
      if(montantRetrait >= 10){
        println("Il reste : " + montantRetrait + deviseTexte + " à distribuer")
        println("Vous pouvez obtenir au maximum " + (montantRetrait/10) + " billet(s) de 10 " + deviseTexte)
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
        var input = readLine()
        if(input == "o"){
          billet10 = montantRetrait/10.toInt
        }else{
          billet10 = input.toInt
        }
        while(billet10 > montantRetrait/10.toInt || billet10 != montantRetrait/10.toInt){
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            billet10 = montantRetrait/10.toInt
          }else{
            billet10 = input.toInt
          }
        }
        montantRetrait = montantRetrait - 10*billet10
      }
      if(montantRetrait == retraitTotal){
        billet10 = montantRetrait/10
      }
      montantRetrait = montantRetrait - 10*billet10
    }
    if(devise == 1){
      comptes(id) -= retraitTotal
      println("Veuillez retirer la somme demandée : ")
      if(billet500>0){
        println(billet500 + " billet(s) de 500 CHF")
      }
      if(billet200>0){
        println(billet200 + " billet(s) de 200 CHF")
      }
      if(billet100>0){
        println(billet100 + " billet(s) de 100 CHF")
      }
      if(billet50>0){
        println(billet50 + " billet(s) de 50 CHF")
      }
      if(billet20>0){
        println(billet20 + " billet(s) de 20 CHF")
      }
      if(billet10>0){
        println(billet10 + " billet(s) de 10 CHF")
      }
      println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " CHF")
    }
    if(devise == 2){
      comptes(id) -= retraitTotal * 0.95
      println("Veuillez retirer la somme demandée : ")
      if(billet100>0){
        println(billet100+ " billet(s) de 100 EUR")
      }
      if(billet50>0){
        println(billet50+ " billet(s) de 50 EUR")
      }
      if(billet20>0){
        println(billet20+ " billet(s) de 20 EUR")
      }
      if(billet10>0){
        println(billet10+ " billet(s) de 10 EUR")
      }
      println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) + " EUR")
    }
  }

  // définition de la méthode changement du code pin :
  def changepin(id: Int, codespin: Array[String]): Unit = {
    // println("ok")
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var nouveauPin = readInt()
    while(nouveauPin.toString.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      nouveauPin = readInt()
    }
  }

  // méthode main :
  def main(args: Array[String]): Unit = {
    
    // nombre de clientes :
    val nbClients = 100

    // variable pour le choix de l'identifiant : 
    var identificationReussi = false
    
    // variable compte remplacé par tableau comptes :
    var comptes = Array.fill(nbClients)(1200.0) 
    //for (x <- comptes)println(x)
    
    // variable pin remplacé par tableau codepin :
    var codespin = Array.fill(nbClients)("INTRO1234") 
    
    // demande de l'identifiant et du code pin :
    while (!identificationReussi) {
      println("Saisissez votre code identifiant >")
      val nbClient = readInt()

      if (nbClient < 1 || nbClient > nbClients) {
        println("Cet identifiant n'est pas valable.")
      } else {
        var essais = 0
        val nbEssaisMax = 3
        var pinCorrect = false

        while (essais < nbEssaisMax && !pinCorrect) {
          println("Saisissez votre code pin >")
          val code = readLine()

          if (code == codespin(nbClient - 1)) {
            pinCorrect = true
            identificationReussi = true
          } else {
            essais += 1
            val essaisRestants = nbEssaisMax - essais

            if (essaisRestants > 0) {
              println("Code pin erroné, il vous reste " + essaisRestants + " tentatives.")
            } else {
              println("Trop d'erreurs, abandon de l'identification.")
            }
          }
        }
        if (!pinCorrect) {
          // Demander à nouveau un identifiant puis un code pin
          println(" ")
        }
      }
    }
    
    // accès au menu :
    var choix = 0
    var client = nbClients - 1
    do {
      println("Choisissez votre opération :\n" +
              "1) Dépôt \n" +
              "2) Retrait \n" +
              "3) Consultation du compte \n" +
              "4) Changement du code pin \n" +
              "5) Terminer \n" +
              "Votre choix : ")
      choix = readInt()

      if (choix == 1) {
        depot(client, comptes)
      } 
      if (choix == 2) {
        retrait(client, comptes)
      } 
      if (choix == 3) {
        println("Le montant disponible sur votre compte est de : " + comptes(client) + " CHF")
      }
      if (choix == 4) {
        changepin (client, codespin)
      }
      if (choix == 5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
      }
    } while (choix != 5)
  }
}
  
