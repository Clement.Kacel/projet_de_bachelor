//Assignment: Soraya Perez Rosado_996545_assignsubmission_file

import io.StdIn._
object Main {
  def depot(id: Int, comptes: Array[Double]): Unit = {
    val n = "%.2f"
    println("Indiquez la devise du dépôt: 1)CHF; 2)EUR >")
    var DeviseDepot = readInt()
    //Devise incorrecte
    while(DeviseDepot<1 || DeviseDepot>2){
      println("Votre sélection n'est pas correcte, choissisez une autre option > ")
      DeviseDepot = readInt()
    }
    println("Indiquez le montant du dépôt > ")
    var MontantDepot = readDouble()
    //montant pas divisible par 10
    while(!(MontantDepot%10 == 0)){
      println("Le montant doit être un multiple de 10 >")
      MontantDepot = readDouble()
    }
    //si la devise est en euros
    if(DeviseDepot == 2){
      MontantDepot = MontantDepot * 0.95
      comptes(id) += MontantDepot
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + n.format(comptes(id)) + "CHF")
    }else{
      //Devise en CHF
      comptes(id) += MontantDepot 
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: " + n.format(comptes(id)) + " CHF")
    }  
  }
  def retrait(id: Int, comptes: Array[Double]): Unit = {
    val n = "%.2f"
    var billets500 = 0
    var billets200 = 0
    var billets100 = 0
    var billets50 = 0
    var billets20 = 0
    var billets10 = 0
    println("Indiquez la devise du retrait: 1)CHF; 2)EUR >")
    var DeviseRetrait = readInt()
    //devise incorrecte
    while(DeviseRetrait < 1 || DeviseRetrait > 2){
      println("Votre sélection n'est pas correcte, choissisez une autre option > ")
      DeviseRetrait = readInt()
    }
    //le montant autorise est le 10% de l'argent sur le compte
    var MontantAutorise = (10 * comptes(id))/100
    println("Indiquez le montant du retrait > ")
    var MontantRetrait = readDouble()
    //montant pas divisible par 10 et inferieur au montant de retrait autorisé
    while(!(MontantRetrait%10 == 0) || (MontantRetrait > MontantAutorise)){
      if(!(MontantRetrait%10 == 0)){
        println("Le montant doit être un multiple de 10 ")
        MontantRetrait = readDouble()
      }
      else if(MontantRetrait > MontantAutorise){
        println("Votre plafond de retrait autorisé est de: " + n.format(MontantAutorise) + "CHF")
         MontantRetrait = readDouble()
      }
    }
    //taille des coupures en CHF
    var coupurespetitesCHF = Array(100.0, 50.0, 20.0, 10.0)
    var coupuresgrossesCHF = Array(500.0, 200.0, 100.0, 50.0, 20.0, 10.0)
    if(DeviseRetrait ==1){
      if(DeviseRetrait == 1 && MontantRetrait >= 200){
        println("En 1) Grosses coupures, 2) Petites coupures >")
        var choixCoupures = readInt()
        while(choixCoupures<1 || choixCoupures>2){
          println("Votre sélection n'est pas correcte, choissisez une autre option > ")
          choixCoupures = readInt()
        }
        //grosses coupures
        var montantRestant = MontantRetrait
        var coupure = if(choixCoupures == 1) coupuresgrossesCHF  else coupurespetitesCHF
        for(coupuresgrossesCHF <- coupure){
          if(coupuresgrossesCHF == 500.0 || coupuresgrossesCHF == 200.0 || coupuresgrossesCHF == 100.0 || coupuresgrossesCHF == 50.0 || coupuresgrossesCHF == 20.0){
          if(montantRestant >= coupuresgrossesCHF){
          var nbCoupures = (montantRestant / coupuresgrossesCHF).toInt
          if(nbCoupures > 0){
            println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nbCoupures + " billet(s) de " + n.format(coupuresgrossesCHF) + "CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée> ")
              var saisie = readLine()
            if(saisie == "o"){
              //Il choisit ok
              montantRestant = montantRestant % coupuresgrossesCHF
              if (coupuresgrossesCHF == 500.0) billets500 += nbCoupures
              else if (coupuresgrossesCHF == 200.0) billets200 += nbCoupures
              else if (coupuresgrossesCHF == 100.0) billets100 += nbCoupures
              else if (coupuresgrossesCHF == 50.0) billets50 += nbCoupures
              else if (coupuresgrossesCHF == 20.0) billets20 += nbCoupures
            }else{
              //valeur de coupures inférieure 
              var nbchoisi = saisie.toInt 
              while(nbchoisi >= nbCoupures || nbchoisi < 0){
              //valeur pas correcte
              println("Valeur non valide, tapez une vaelur inférieure à celle proposée >")
              nbchoisi = readInt()
              }
              if(nbchoisi < nbCoupures){
                montantRestant = montantRestant - (nbchoisi * coupuresgrossesCHF)
                if (coupuresgrossesCHF == 500.0) billets500 += nbchoisi
                else if (coupuresgrossesCHF == 200.0) billets200 += nbchoisi
                else if (coupuresgrossesCHF == 100.0) billets100 += nbchoisi
                else if (coupuresgrossesCHF == 50.0) billets50 += nbchoisi
                else if (coupuresgrossesCHF == 20.0) billets20 += nbchoisi
              }
            }
          }
          }
          }
          //Si les coupures sont de 10CHF ne pas demander
          else if(coupuresgrossesCHF == 10.0 && montantRestant >= coupuresgrossesCHF){
              var nbCoupures = (montantRestant / coupuresgrossesCHF).toInt
              println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
              println("Vous allez obtenir " + nbCoupures + " billet(s) de " +      n.format(coupuresgrossesCHF) + "CHF")
              if (coupuresgrossesCHF == 10.0) billets10 += nbCoupures
              montantRestant = 0
          }
        }
        //Coupures petites
        for(coupurespetitesCHF <- coupure){
          if(coupurespetitesCHF == 100.0 || coupurespetitesCHF == 50.0 || coupurespetitesCHF == 20.0){
          if(montantRestant >= coupurespetitesCHF){
          var nbCoupures = (montantRestant / coupurespetitesCHF).toInt
          if(nbCoupures > 0){
            println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + nbCoupures + " billet(s) de " + n.format(coupurespetitesCHF) + "CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var saisie = readLine()
              if(saisie == "o"){
                montantRestant = montantRestant %  coupurespetitesCHF
                if (coupurespetitesCHF == 100.0) billets100 += nbCoupures
                else if (coupurespetitesCHF == 50.0) billets50 += nbCoupures
                else if (coupurespetitesCHF == 20.0) billets20 += nbCoupures
              }else{
                var nbchoisi = saisie.toInt
                while(nbchoisi >= nbCoupures || nbchoisi < 0){//valeur pas correcte
                println("Valeur non valide, tapez une vaelur inférieure à celle proposée >")
                nbchoisi = readInt()
                }
                if(nbchoisi < nbCoupures){
                  montantRestant = montantRestant - (nbchoisi * coupurespetitesCHF)
                  if (coupurespetitesCHF == 100.0) billets100 += nbchoisi
                  else if (coupurespetitesCHF == 50.0) billets50 += nbchoisi
                  else if (coupurespetitesCHF == 20.0) billets20 += nbchoisi
                }
              }
          }
              }
        //coupure plus petite que 10 ne pas demander
        }else if(coupurespetitesCHF == 10.0 && montantRestant >= coupurespetitesCHF){
            var nbCoupures = (montantRestant / coupurespetitesCHF).toInt
            println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
            println("Vous allez obtenir " + nbCoupures  + " billet(s) de " + n.format(coupurespetitesCHF) + "CHF")
            if (coupurespetitesCHF == 10.0) billets10 += nbCoupures
            montantRestant = 0
        }
          }
      }
      //Montant retrait < 200
      else if(DeviseRetrait == 1 && MontantRetrait < 200){
        var montantRestant = MontantRetrait
        var coupure = coupurespetitesCHF
        for(coupurespetitesCHF <- coupure){
          if(coupurespetitesCHF == 100.0 || coupurespetitesCHF == 50.0 || coupurespetitesCHF == 20.0){
        if(montantRestant >= coupurespetitesCHF){
        var nbCoupures = (montantRestant / coupurespetitesCHF).toInt
        if(nbCoupures > 0){
          println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nbCoupures + " billet(s) de " + n.format(coupurespetitesCHF) + "CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var saisie = readLine()
            if(saisie == "o"){
              montantRestant = montantRestant %  coupurespetitesCHF
              if (coupurespetitesCHF == 100.0) billets100 += nbCoupures
              else if (coupurespetitesCHF == 50.0) billets50 += nbCoupures
              else if (coupurespetitesCHF == 20.0) billets20 += nbCoupures
              else if (coupurespetitesCHF == 10.0) billets10 += nbCoupures
            }else{
              var nbchoisi = saisie.toInt 
              while(nbchoisi >= nbCoupures || nbchoisi < 0){//valeur pas correcte
              println("Valeur non valide, tapez une vaelur inférieure à celle proposée >")
              nbchoisi = readInt()
              }
              if(nbchoisi < nbCoupures){
                montantRestant = montantRestant - (nbchoisi * coupurespetitesCHF)
                if (coupurespetitesCHF == 100.0) billets100 += nbchoisi
                else if (coupurespetitesCHF == 50.0) billets50 += nbchoisi
                else if (coupurespetitesCHF == 20.0) billets20 += nbchoisi
              }
            }
        }
            }//Coupure plus petite que 10 ne pas demander
        }else if(coupurespetitesCHF == 10.0 && montantRestant >= coupurespetitesCHF){
            var nbCoupures = (montantRestant / coupurespetitesCHF).toInt
            println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
            println("Vous allez obtenir " + nbCoupures + " billet(s) de " + n.format(coupurespetitesCHF) + "CHF")
            if (coupurespetitesCHF == 10.0) billets10 += nbCoupures.toInt
            montantRestant = 0
        }
        }
          }
      //Afficher les billets choisis à la fin
      println("Veuillez retirer la somme demandée: ")
      if (billets500 > 0){
        print(billets500)
        println(" billet(s) de 500CHF")
      }
      if (billets200 > 0){
        print(billets200)
        println(" billet(s) de 200CHF")
      }
      if (billets100 > 0){
        print(billets100)
        println(" billet(s) de 100CHF")
      }
      if (billets50 > 0){
        print(billets50)
        println(" billet(s) de 50CHF")
      }
      if (billets20 > 0){
        print(billets20)
        println(" billet(s) de 20CHF")
      }
      if (billets10 > 0){
        print(billets10)
        println(" billet(s) de 10CHF")
      }
      comptes(id) -= MontantRetrait
      println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: " + n.format(comptes(id)) + "CHF")
    }
    //Si la devise de retrait est en EUR
    if(DeviseRetrait == 2){
      var montantRestant = MontantRetrait
      var coupuresEUR = Array(100.0, 50.0, 20.0, 10.0)
      var coupure = coupuresEUR
      for(coupuresEUR <- coupure){
        if(coupuresEUR == 100.0 || coupuresEUR == 50.0 || coupuresEUR == 20.0){
      if(montantRestant >= coupuresEUR){
      var nbCoupures = montantRestant / coupuresEUR
      if(nbCoupures > 0){
        println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nbCoupures.toInt + " billet(s) de " + n.format(coupuresEUR) + "CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var saisie = readLine()
          if(saisie == "o"){
            montantRestant = montantRestant %  coupuresEUR
            if (coupuresEUR == 100.0) billets100 += nbCoupures.toInt
            else if (coupuresEUR == 50.0) billets50 += nbCoupures.toInt
            else if (coupuresEUR == 20.0) billets20 += nbCoupures.toInt
          }else{
            var nbchoisi = saisie.toInt
            while(nbchoisi >= nbCoupures || nbchoisi < 0){
            //valeur pas correcte
            println("Valeur non valide, tapez une vaelur inférieure à celle proposée >")
            nbchoisi = readInt()
            }
            if(nbchoisi < nbCoupures){
              montantRestant = montantRestant - (nbchoisi * coupuresEUR)
              if (coupuresEUR == 100.0) billets100 += nbchoisi
              else if (coupuresEUR == 50.0) billets50 += nbchoisi
              else if (coupuresEUR == 20.0) billets20 += nbchoisi
            }
          }
      }
          }
      }else if(coupuresEUR == 10.0 && montantRestant >= coupuresEUR){
          var nbCoupures = (montantRestant / coupuresEUR).toInt
          println("Il reste " + n.format(montantRestant) + "CHF à distribuer")
          println("Vous allez obtenir " + nbCoupures + " billet(s) de " + n.format(coupuresEUR) + "CHF")
          if (coupuresEUR == 10.0) billets10 += nbCoupures.toInt
          montantRestant = 0
      }
      }// Afficher les billets choisis à la fin
      println("Veuillez retirer la somme demandée: ")
      if (billets100 > 0){
        print(billets100)
        println(" billet(s) de 100EUR")
      }
      if (billets50 > 0){
        print(billets50)
        println(" billet(s) de 50EUR")
      }
      if (billets20 > 0){
        print(billets20)
        println(" billet(s) de 20EUR")
      }
      if (billets10 > 0){
        print(billets10)
        println(" billet(s) de 10EUR")
      }
      comptes(id) -= MontantRetrait * 0.95
      println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: " + n.format(comptes(id)) + " CHF")
    }
  }
  
  def changepin(id: Int, codespin: Array[String]): Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var nouveaupin = readLine()
    //verifier la longueur du nouveau code pin
    while(nouveaupin.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères.")
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      nouveaupin = readLine()
    }
    println("Le code pin a été modifié")
    //mettre a jour le nouveau pin dans le tableau 
    codespin(id) = nouveaupin
  }
  //Problema de boucles qui se repetent 
  def main(args: Array[String]): Unit = {
    val n = "%.2f"
    //Tableaux
    var nbClients = 101
    var comptes = Array.fill(nbClients)(1200.0)
    var codespin = Array.fill(nbClients)("INTRO1234")
    var id = 0
    var reessayerId = true
    //Boucle qui permet de retenter l'Id
    while(reessayerId){
    // demander a l'utilisateur de sidentifier 
    println("Saisissez votre code identifiant>")
    id = readInt()
    //le id n'est pas correct 
    if(id <= nbClients && id >= 0){
      //L'utilisateur s'est idéntifié avec succes
      val codePin = codespin(id)
      //Le client doit saisir le PIN
      var PINSaisi = ""
      println("Saisissez votre code PIN >")
      PINSaisi = readLine()
      //Si le PIN saisi est faux 
      var nbEssais = 3
      while(!(codePin == PINSaisi) && (nbEssais > 1)){
        println("Code PIN erroné, il vous reste " + (nbEssais -1) + " tentatives >")
        PINSaisi = readLine()
        //Essai suivant
        nbEssais -= 1
      }
      //Si le PIN est correct
      if(codePin == PINSaisi){
        reessayerId = false
        var operation = 0
        while(operation != 5){
        println("Choissisez votre opération: ")
        println("1) Dépot")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
        operation = readInt()
        //Nombre choisi ne correspond à aucune option
        while(operation <1 || operation >5){
          println("Votre sélection n'est pas correcte, choissisez une autre option: ")
          operation = readInt()
        }
          if(operation == 1){
            depot(id, comptes)
          }else if(operation == 2){
            retrait(id, comptes)
          }else if(operation == 3){
            println("Le montant disponible sur votre compte est de: " + n.format(comptes(id)) + "CHF")
          }else if(operation == 4){
            //option pour modifier le pin
            changepin(id, codespin)
          }
        if(operation == 5){
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
          reessayerId = true
        }
        }
      }else{
        println("Trop d'erreurs, abandon de l'identification")
      }
    }else{
      //Id pas valide
      println("Cet identifiant n'est pas valable.")
      reessayerId = false
    }
    }
  }
}
