//Assignment: Aya El Zein_996538_assignsubmission_file

import io.StdIn._

sealed abstract class Devise(val nom: String, val taux: Double) {
  override def toString(): String = {
    return nom + " (" + taux + ")"
  }
}

case object CHF extends Devise("CHF", 1.0)
case object EUR extends Devise("EUR", 0.95)

object Main {
  val nbclients = 100
  var codespin = Array.fill(nbclients)("INTRO1234")
  var comptes = Array.fill(nbclients)(1200.0)
  var current_user_id = Option.empty[Int]

  def Montant = comptes(current_user_id.get)
  def Montant_=(new_amt: Double) = {
    comptes(current_user_id.get) = new_amt
  }

  def prompt_user_id(): Option[Int] = {
    print("Saisissez votre code identifiant > ")
    readLine().toIntOption match {
      case Some(uid) if uid >= 0 && uid < nbclients => Some(uid)
      case _                                        => None
    }
  }

  def authorize(user_id: Int): Boolean = {
    print("Saisissez votre code PIN : ")
    var tries_left = 3
    while (tries_left > 0) {
      var pin = readLine()

      if (codespin(user_id) == pin) {
        return true
      } else {
        tries_left = tries_left - 1
        if (tries_left > 0) {
          print(
            "Code pin erroné, il vous reste " + tries_left + " tentatives > "
          )
        }
      }
    }
    println(
      "Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte."
    )
    return false
  }

  def main(args: Array[String]): Unit = {
    while (true) {
      logic()
    }
  }

  def logic(): Unit = {
    if (current_user_id.isEmpty) {
      val uid = prompt_user_id()
      if (uid.isEmpty) {
        println("Cet identifiant n’est pas valable.")
        return
      }
      if (authorize(uid.get)) {
        current_user_id = uid
      } else {
        return
      }
    }

    print(
      """Choisissez votre opération : 
    1) Dépôt 
    2) Retrait 
    3) Consultation du compte
    4) Changement du code pin
    5) Terminer
Votre choix : """
    )

    var choixoperation = readLine().toInt

    // Choix de l'opération
    val OperationValidesRange = 1 to 5

    // no other than 1,2,3 and 4
    if (!OperationValidesRange.contains(choixoperation)) {
      println("Opération invalide")
      return
    }

    if (choixoperation == 5) {
      println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
      current_user_id = None
      return
    }

    if (choixoperation == 1) {
      deposit()
    } else if (choixoperation == 3) { // OPERATIONS DE CONSULTATION
      println("Le montant disponible sur votre compte est de : " + Montant)
    } else if (choixoperation == 2) { // OPERATION DE RETRAIT
      var deviseretrait = 0
      var montantretiré = 0
      val MontantAutorisé = Montant * 0.1 // montant plafond

      while (deviseretrait != 1 && deviseretrait != 2) {
        println("Indiquez la devise: 1) CHF ; 2) EUR > ")
        deviseretrait = readLine().toInt
        if (deviseretrait != 1 && deviseretrait != 2) {
          println("devise invalide")
        }
      }

      var Somme01 = false
      while (!Somme01) {
        println("Indiquez le montant du retrait > ")
        montantretiré = readLine().toInt
        if (
          (montantretiré % 10 == 0) || (MontantAutorisé - montantretiré > 0)
        ) {
          Somme01 = !false
        } else {
          println(
            "Le montant doit être multiple de 10 et votre plafond de retrait autorisé est de : " + MontantAutorisé
          )
        }

        // coupure methode:
        var coupure = 0
        var SommeRE = montantretiré
        var Maxbillets10 = SommeRE / 10
        var ok = 0
        var choixutilisateur = 0
        if (deviseretrait == 1) { // RETRAIT EN CHF
          if (montantretiré >= 20) {
            while (coupure != 1 && coupure != 2) {
              println(
                "Indiqué la coupure : 1) grosses coupures, 2) petite coupure > "
              )
              coupure = readLine().toInt // STOPPED HERE

              println("Veuillez retirer la somme demandé: ")
              var SommeRE = montantretiré
              var Maxbillets10 = SommeRE / 10
              var ok = 0
              var choixutilisateur = 0
              if (coupure == 2) { //////////// BLOC DE SECOURS
                while (SommeRE > 0) { // MEh
                  if (SommeRE >= 10) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 10).toInt + "billets de 10 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 10)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 10
                    }
                  } else if (SommeRE >= 50) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 50).toInt + "billets de 50 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 50)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 50
                    }
                  } else if (SommeRE >= 20) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 20).toInt + "billets de 20 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 20)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 20
                    }
                  } else if (SommeRE >= 10) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 10).toInt + "billets de 10 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 10)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 10
                    }
                  }
                }
              } //////// BLOC DE SECORS

              if (coupure == 1) { /// BLOC GRANDE COUPURE EN CHF
                while (SommeRE > 0) { // MEh
                  if (SommeRE >= 50) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 50).toInt + "billets de 50 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 50)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 50
                    }
                  } else if (SommeRE >= 20) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 20).toInt + "billets de 20 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 20)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 20
                    }
                  } else if (SommeRE >= 10) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 10).toInt + "billets de 10 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 10)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 10
                    }
                  } else if (SommeRE >= 50) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 50).toInt + "billets de 50 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 50)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 50
                    }
                  } else if (SommeRE >= 20) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 20).toInt + "billets de 20 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 20)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 20
                    }

                  } else if (SommeRE >= 10) {
                    println(
                      "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 10).toInt + "billets de 10 CHF. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
                    )
                    choixutilisateur = readLine().toInt
                    if (choixutilisateur != 0) {
                      SommeRE = SommeRE - (choixutilisateur * 10)
                    } else { // donc si choixutilisateur est bien o
                      SommeRE = SommeRE % 10
                    }
                  }
                } // meh
              } ///// BLOC GRANDE COUPURE
            } // l 118
          } // l 115

          if (deviseretrait == 1) {

            Montant -= montantretiré
            Montant = math.floor(Montant * 10) / 10
            println(
              "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est:  " + Montant
            )
          }
        } else { // RETRAIT EN EUR
          while (SommeRE > 0) { // MEh
            if (SommeRE >= 10) {
              println(
                "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 10).toInt + "billets de 10 EUR. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
              )
              choixutilisateur = readLine().toInt
              if (choixutilisateur != 0) {
                SommeRE = SommeRE - (choixutilisateur * 10)
              } else { // donc si choixutilisateur est bien o
                SommeRE = SommeRE % 10
              }
            } else if (SommeRE >= 50) {
              println(
                "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 50).toInt + "billets de 50 EUR. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
              )
              choixutilisateur = readLine().toInt
              if (choixutilisateur != 0) {
                SommeRE = SommeRE - (choixutilisateur * 50)
              } else { // donc si choixutilisateur est bien o
                SommeRE = SommeRE % 50
              }
            } else if (SommeRE >= 20) {
              println(
                "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 20).toInt + "billets de 20 EUR. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
              )
              choixutilisateur = readLine().toInt
              if (choixutilisateur != 0) {
                SommeRE = SommeRE - (choixutilisateur * 20)
              } else { // donc si choixutilisateur est bien o
                SommeRE = SommeRE % 20
              }
            } else if (SommeRE >= 10) {
              println(
                "Il reste " + SommeRE + " CHF à distribué. Vous pouvez obtenir " + (SommeRE / 10).toInt + "billets de 10 EUR. Tapez 0 si ok ou une autre valeur inférieure à celle proposée > "
              )
              choixutilisateur = readLine().toInt
              if (choixutilisateur != 0) {
                SommeRE = SommeRE - (choixutilisateur * 10)
              } else { // donc si choixutilisateur est bien o
                SommeRE = SommeRE % 10
              }
            }
          }
          if (deviseretrait == 2) {
            Montant -= montantretiré * 0.95
            Montant = math.floor(Montant * 10) / 10
            printf(
              "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n ",
              +Montant
            )
          }
        } // RETRAIT EUR

      }

    } else if (choixoperation == 4) { // OPERATION DE CHANGEMENT DE PIN
      change_pin()
    }
  }

  def deposit(): Unit = {
    var devise = prompt_devise()
    var amt = 0

    var amt_valid = false
    while (!amt_valid) {
      println("Indiquez le montant du dépôt > ")
      amt = readLine().toInt

      if (amt % 10 != 0) {
        println("Montant invalide. Le montant doit etre un multiple de 10")
      } else { // si le montant n'est pas un multiple de 10 (ici c'est le else du if en l 71)
        amt_valid = true
      }
    }

    val amt_conv = devise.taux * amt
    Montant += amt_conv
    println(
      "Votre depot a été pris en compte, le nouveau montant disponible est " + Montant
    )
  }

  def change_pin(): Unit = {
    while (true) {
      println("Indiquez votre nouveau code pin > ")
      var pin = readLine()
      if (pin.length() < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères")
      } else {
        codespin(current_user_id.get) = pin
        println("Votre code pin a été changé")
        return
      }
    }
  }

  def prompt_devise(): Devise = {
    var devise = Option.empty[Devise]
    while (devise.isEmpty) {
      println("Indiquez la devise: 1) CHF ; 2) EUR > ")
      devise = readLine().toIntOption match {
        case Some(1) => Some(CHF)
        case Some(2) => Some(EUR)
        case _       => None
      }
    }
    return devise.get
  }
}