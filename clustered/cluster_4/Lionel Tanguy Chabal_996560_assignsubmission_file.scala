//Assignment: Lionel Tanguy Chabal_996560_assignsubmission_file

import io.StdIn._

object Main {

  def depot(id : Int, comptes : Array[Double]) : Unit = {
    var devise_depot_retrait = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
    var while_reutilisable2 = false
    var montant_depot_retrait = 0
    var calcul_double = 0.0
    while (while_reutilisable2 == false) {
      montant_depot_retrait = readLine("Indiquez le montant du dépôt >").toInt
      if (montant_depot_retrait >= 10 && montant_depot_retrait % 10 == 0) {
        while_reutilisable2 = true
      } else {
        println("Le montant doit être un multiple de 10")
      }
    }
    while_reutilisable2 = false
    if (devise_depot_retrait == 1) {
      comptes(id) += montant_depot_retrait
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f CHF\n",comptes(id))
    } else {
      calcul_double = montant_depot_retrait * 0.95
      comptes(id) += calcul_double
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f CHF\n",comptes(id))
    }
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    var while_reutilisable3 = false
    var devise_depot_retrait = 0
    var montant_depot_retrait = 0
    var calcul_double2 = 0.0
    var choix_coupures = 0
    var while_coupures = false
    var grosses_coupures = 500.0
    var nb_billets = 0
    var nb_boucles = 0
    var autres_coupures = ""
    var resume_coupures = "Veuillez retirer la somme demandée :"
    var petites_coupures = 100.0
    while(while_reutilisable3 == false) {
      devise_depot_retrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
      if (devise_depot_retrait == 1 || devise_depot_retrait == 2) {
        while_reutilisable3 = true
      }
    }
    while_reutilisable3 = false
    while (while_reutilisable3 == false) {
      montant_depot_retrait = readLine("Indiquez le montant du retrait >").toInt
      if (montant_depot_retrait >= 10 && montant_depot_retrait % 10 == 0) { 
        if (devise_depot_retrait == 1 && montant_depot_retrait > 0.1 * comptes(id)) {
          printf("Votre plafond de retrait autorisé est de :%.2f CHF\n",(0.1 * comptes(id)))
        }
        else if (devise_depot_retrait == 2 && montant_depot_retrait * 0.95 > 0.1 * comptes(id)) {
          printf("Votre plafond de retrait autorisé est de :%.2f EUR\n",((0.1 * comptes(id))/0.95))
        } else {
           calcul_double2 = montant_depot_retrait
          while_reutilisable3 = true
        } 
      } else {
        println("Le montant doit être un multiple de 10.")
      }
    }
    while_reutilisable3 = false
    if (devise_depot_retrait == 1) {
      if (montant_depot_retrait >= 200) {
        while (while_reutilisable3 == false) {
          choix_coupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          if (choix_coupures == 1) {
            while (while_coupures == false) {
              for (i <- 1 to 6) {
                if (i == 2 || i == 5) {
                  grosses_coupures *= 0.4
                }
                else if (i == 3 || i == 4 || i == 6) {
                  grosses_coupures /= 2     
                }
                nb_billets = montant_depot_retrait / grosses_coupures.toInt
                if (nb_billets > 0) {
                  println("Il reste " + montant_depot_retrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + nb_billets + " billet(s) de " + grosses_coupures.toInt + " CHF")
                  while (while_reutilisable3 == false) {
                    if (nb_boucles < 5) {
                      autres_coupures = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                      if (autres_coupures == "o") {
                        while_reutilisable3 = true
                      }
                      else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                        while_reutilisable3 = true
                      }
                    } else {
                      autres_coupures = "o"
                      while_reutilisable3 = true
                    }
                  }
                  while_reutilisable3 = false
                  if (autres_coupures == "o") {
                    nb_boucles += 1
                    montant_depot_retrait %= grosses_coupures.toInt
                    resume_coupures += "\n" + nb_billets + " billet(s) de " + grosses_coupures.toInt + " CHF"
                    if (montant_depot_retrait == 0) {
                      print(resume_coupures)
                      while_coupures = true
                    }
                  } else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                    montant_depot_retrait -= autres_coupures.toInt * grosses_coupures.toInt
                    if (autres_coupures.toInt != 0) {
                      resume_coupures += "\n" + autres_coupures.toInt + " billet(s) de " + grosses_coupures.toInt + " CHF"
                    }
                    nb_boucles += 1
                  }
                } else {
                  nb_boucles += 1
                }
              }
            }
            while_reutilisable3 = true
            while_coupures = false
            grosses_coupures = 500.0
            nb_boucles = 0
            comptes(id) -= calcul_double2
            resume_coupures = "Veuillez retirer la somme demandée :"
            printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
          }
          else if (choix_coupures == 2) {
            while (while_coupures == false) {
              for (i <- 1 to 4) {
                if (i == 3) {
                  petites_coupures *= 0.4
                }
                else if (i == 2 || i == 4) {
                  petites_coupures /= 2     
                }
                nb_billets = montant_depot_retrait / petites_coupures.toInt
                if (nb_billets > 0) {
                  println("Il reste " + montant_depot_retrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + nb_billets + " billet(s) de " + petites_coupures.toInt + " CHF")
                  while (while_reutilisable3 == false) {
                    if (nb_boucles < 3) {
                      autres_coupures = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                      if (autres_coupures == "o") {
                        while_reutilisable3 = true
                      }
                      else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                        while_reutilisable3 = true
                      }
                    } else {
                      autres_coupures = "o"
                      while_reutilisable3 = true
                    }
                  }
                  while_reutilisable3 = false
                  if (autres_coupures == "o") {
                    nb_boucles += 1
                    montant_depot_retrait %= petites_coupures.toInt
                    resume_coupures += "\n" + nb_billets + " billet(s) de " + petites_coupures.toInt + " CHF"
                    if (montant_depot_retrait == 0) {
                      print(resume_coupures)
                      while_coupures = true
                    }
                  } else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                    montant_depot_retrait -= autres_coupures.toInt * petites_coupures.toInt
                    if (autres_coupures.toInt != 0) {
                      resume_coupures += "\n" + autres_coupures.toInt + " billet(s) de " + petites_coupures.toInt + " CHF"
                    }
                    nb_boucles += 1
                  }
                } else {
                  nb_boucles += 1
                }
              }
            }
            while_reutilisable3 = true
            while_coupures = false
            petites_coupures = 100.0
            nb_boucles = 0
            comptes(id) -= calcul_double2
            resume_coupures = "Veuillez retirer la somme demandée :"
            printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
          }
        }
        while_reutilisable3 = false
      } else {
        while (while_coupures == false) {
          for (i <- 1 to 4) {
            if (i == 3) {
              petites_coupures *= 0.4
            }
            else if (i == 2 || i == 4) {
              petites_coupures /= 2     
            }
            nb_billets = montant_depot_retrait / petites_coupures.toInt
            if (nb_billets > 0) {
              println("Il reste " + montant_depot_retrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + nb_billets + " billet(s) de " + petites_coupures.toInt + " CHF")
              while (while_reutilisable3 == false) {
                if (nb_boucles < 3) {
                  autres_coupures = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  if (autres_coupures == "o") {
                    while_reutilisable3 = true
                  }
                  else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                    while_reutilisable3 = true
                  }
                } else {
                  autres_coupures = "o"
                  while_reutilisable3 = true
                }
              }
              while_reutilisable3 = false
              if (autres_coupures == "o") {
                nb_boucles += 1
                montant_depot_retrait %= petites_coupures.toInt
                resume_coupures += "\n" + nb_billets + " billet(s) de " + petites_coupures.toInt + " CHF"
                if (montant_depot_retrait == 0) {
                  print(resume_coupures)
                  while_coupures = true
                }
              } else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                montant_depot_retrait -= autres_coupures.toInt * petites_coupures.toInt
                if (autres_coupures.toInt != 0) {
                  resume_coupures += "\n" + autres_coupures.toInt + " billet(s) de " + petites_coupures.toInt + " CHF"
                }
                nb_boucles += 1
              }
            } else {
              nb_boucles += 1
            }
          }
        }
        while_coupures = false
        petites_coupures = 100.0
        nb_boucles = 0
        comptes(id) -= calcul_double2
        resume_coupures = "Veuillez retirer la somme demandée :"
        printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
      }
    } else {
      while (while_coupures == false) {
        for (i <- 1 to 4) {
          if (i == 3) {
            petites_coupures *= 0.4
          }
          else if (i == 2 || i == 4) {
            petites_coupures /= 2     
          }
          nb_billets = montant_depot_retrait / petites_coupures.toInt
          if (nb_billets > 0) {
            println("Il reste " + montant_depot_retrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + nb_billets + " billet(s) de " + petites_coupures.toInt + " EUR")
            while (while_reutilisable3 == false) {
              if (nb_boucles < 3) {
                autres_coupures = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                if (autres_coupures == "o") {
                  while_reutilisable3 = true
                }
                else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
                  while_reutilisable3 = true
                }
              } else {
                autres_coupures = "o"
                while_reutilisable3 = true
              }
            }
            while_reutilisable3 = false
            if (autres_coupures == "o") {
              nb_boucles += 1
              montant_depot_retrait %= petites_coupures.toInt
              resume_coupures += "\n" + nb_billets + " billet(s) de " + petites_coupures.toInt + " EUR"
              if (montant_depot_retrait == 0) {
                print(resume_coupures)
                while_coupures = true
              }
            } else if (autres_coupures.toInt >= 0 && autres_coupures.toInt < nb_billets && autres_coupures.toInt % 1 == 0) {
              montant_depot_retrait -= autres_coupures.toInt * petites_coupures.toInt
              if (autres_coupures.toInt != 0) {
                resume_coupures += "\n" + autres_coupures.toInt + " billet(s) de " + petites_coupures.toInt + " EUR"
              }
              nb_boucles += 1
            }
          } else {
            nb_boucles += 1
          }
        }
      }
      while_coupures = false
      petites_coupures = 100.0
      nb_boucles = 0
      calcul_double2 *= 0.95
      comptes(id) -= calcul_double2
      resume_coupures = "Veuillez retirer la somme demandée :"
      printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
    }
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var nv_codepin = ""
    var while_reutilisable4 = false
    while (while_reutilisable4 == false) {
      nv_codepin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      if (nv_codepin.length >= 8) {
        codespin(id) = nv_codepin
        while_reutilisable4 = true
      } else {
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
    }
    while_reutilisable4 = false
  }
  
  def main(args: Array[String]): Unit = {
    var id = 0
    val nbclients = 100
    val comptes = Array.fill(nbclients)(1200.0)
    val codespin = Array.fill(nbclients)("INTRO1234")
    var fin = false
    var while_reutilisable = false
    var id_apres_pin = false
    var tentatives = 0
    var verif_pin = ""
    var choix_operation = 0
    
    while (fin == false) {
      id = readLine("Saisissez votre code identifiant >").toInt
      if (id >= nbclients) {
        print("Cet identifiant n’est pas valable.")
        fin = true
      }
      while (while_reutilisable == false && fin == false) {
        if (tentatives == 0) {
          verif_pin = readLine("Saisissez votre code pin >")
          tentatives += 1
        }
        if (verif_pin != codespin(id)) {
          verif_pin = readLine("Code pin erroné, il vous reste " + (3 - tentatives) + " tentatives >")
          tentatives += 1
          if (tentatives == 3 && verif_pin != codespin(id)) {
            println("Trop d’erreurs, abandon de l’identification")
            while_reutilisable = true
            id_apres_pin = true
          }
        } else {
          while_reutilisable = true
        }
      }
      while_reutilisable = false
      tentatives = 0
      if (id_apres_pin == false && fin == false) {
        choix_operation = readLine("Choisissez votre opération :\n\t1) Dépôt\n\t2) Retrait\n\t3) Consultation du compte\n\t4) Changement du code pin\n\t5) Terminer\nVotre choix :").toInt
        if (choix_operation == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        } else {
          while (while_reutilisable == false) {
            if (choix_operation == 1 && while_reutilisable == false) {
              depot(id, comptes)
            }
            else if (choix_operation == 2 && while_reutilisable == false) {
              retrait(id, comptes)
            }
            else if (choix_operation == 3 && while_reutilisable == false) {
              printf("Le montant disponible sur votre compte est de : %.2f CHF\n",comptes(id))
            }
            else if (choix_operation == 4 && while_reutilisable == false) {
              changepin(id, codespin)
            }
            else if (choix_operation == 5 && while_reutilisable == false){
              println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
              while_reutilisable= true
            }
            if (while_reutilisable == false) {
              choix_operation = readLine("Choisissez votre opération :\n\t1) Dépôt\n\t2) Retrait\n\t3) Consultation du compte\n\t4) Changement du code pin\n\t5) Terminer\nVotre choix :").toInt
            }
          }
          while_reutilisable = false
        }
      }
      id_apres_pin = false
    }
  }
}