//Assignment: Matthew Kevin Fabien Pellecuier_996543_assignsubmission_file


object Main {
  import scala.io.StdIn.readInt
  import scala.io.StdIn.readLine
  import scala.io.StdIn.readDouble


  // MAIN
  def main(args: Array[String]): Unit = {

    val nbclients = 100
    var argent = Array.fill[Double](nbclients)(1200.0)
    var codespin = Array.fill[String](nbclients)("INTRO1234")

    println("") // Login + PIN
    var id = readLine("Saisissez votre code identifiant >")
    if (id.toInt > nbclients) {
      println("Cet identifiant n’est pas valable. ")
      println("")
      id = readLine("Saisissez votre code identifiant >")
    }
    println("")
    var tentatives = 3
    var pin = readLine("Saisissez votre code pin > ")
    while (pin != codespin(id.toInt)) {
      tentatives -= 1
      println("")
      println("Code pin erroné, il vous reste " + tentatives +" tentatives >")
      pin = readLine("Saisissez votre code pin > ")
      if (tentatives == 1) {
        println("")
        println("Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte.")
        println("")
        id = readLine("Saisissez votre code identifiant")
      }
    }
      // Menu 
      println("")
      var numeromenu = readLine(
        "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : "
      ).toInt

      while (numeromenu < 1 || numeromenu > 5) {
        println("")
        println("Veuillez saisir un choix valide. ")
        println("")
        numeromenu = readLine(
          "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : "
        ).toInt
      }

    while ((numeromenu == 1)||(numeromenu == 2 )||(numeromenu ==  3)||(numeromenu ==  4)||(numeromenu != 5)) { 
      println("")

      while(numeromenu == 1) { // Depot
        depot(id.toInt, argent)
        numeromenu = readLine(
          "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : ").toInt
      }
      while(numeromenu == 2) { // Retrait
        retrait(id.toInt, argent)
        numeromenu = readLine(
          "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : ").toInt
      }
      while(numeromenu == 3) { // Consultation
        println("")
          printf("Le montant disponible sur votre compte est de : %.2f CHF",argent(id.toInt))
        println("")
        numeromenu = readLine(
          "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5)Terminer\nVotre choix : ").toInt
      }
      while(numeromenu == 4) { // Changement PIN
        changepin(id.toInt, codespin)
        numeromenu = readLine(
          "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : "
        ).toInt
      }
      while (numeromenu == 5) { // Terminer
        println("")
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        println("")
        id = readLine("Saisissez votre code identifiant >")
        if (id.toInt > nbclients) {
          println("Cet identifiant n’est pas valable. ")
          println("")
          id = readLine("Saisissez votre code identifiant >")
        }
        println("")
        var tentatives = 3
        var pin = readLine("Saisissez votre code pin > ")
        while (pin != codespin(id.toInt)) {
          tentatives -= 1
          println("")
          println("Code pin erroné, il vous reste " + tentatives +" tentatives >")
          pin = readLine("Saisissez votre code pin > ")
          if (tentatives == 1) {
            println("")
            println("Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte.")
            println("")
            id = readLine("Saisissez votre code identifiant")
            println("")
            pin = readLine("Saisissez votre code pin > ")
          }
        }
        println("")
        numeromenu = readLine(
          "Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix : "
        ).toInt
      }
    }
  }
  // Dépot 
  def depot(id : Int, argent : Array[Double]) : Unit = {
    println("")
    var argentD = readLine("Indiquez la devise du dépôt : \n1) CHF\n2) EUR\n> ").toInt
    while (argentD < 1 || argentD > 2) {
      println("")
      println("Veuillez saisir un choix valide.")
      println("")
      argentD = readLine("Indiquez la devise du dépôt : \n1) CHF\n2) EUR\n> ").toInt
    }
    println("")
    var montantD = readLine("Indiquez le montant du dépôt > ").toDouble
    println("")
    while (montantD % 10 != 0 || montantD < 10) {
      println("")
      println("Le montant doit être un multiple de 10")
      println("")
      montantD = readLine("Indiquez le montant du dépôt > ").toDouble
    }
    println("")
    if (argentD == 1) {
      argent(id.toInt) += montantD
      printf(
        "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF",argent(id.toInt))

    } else if (argentD == 2) {
      montantD = (montantD * 0.95)
      argent(id.toInt) += montantD
      printf(
        "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF",argent(id.toInt))

    }

    println("")
  }

  // Retrait 
  def retrait(id : Int, argent : Array[Double]) : Unit = {
    println("")
        var argentR = readLine("Indiquez la devise du retrait : \n1) CHF\n2) EUR\n> ").toInt
        while (argentR < 1 || argentR > 2) {
          println("")
          println("Veuillez saisir un choix valide.")
          println("")
          argentR = readLine("Indiquez la devise du dépôt : \n1) CHF\n2) EUR\n> ").toInt
        }
        println("")
        var montantR = readLine("Indiquez le montant du retrait > ").toInt
        println("")
        while (montantR % 10 != 0 ) {
          println("")
          println("Le montant doit être un multiple de 10")
          println("")
          montantR = readLine("Indiquez le montant du dépôt > ").toInt
        }
        while (montantR > (argent(id.toInt)*0.1)) {
          println("")
          printf("Votre plafond de retrait autorisé est de : %.2f CHF",argent(id.toInt)*0.1)
          println("")
          montantR = readLine("Indiquez le montant du dépôt > ").toInt
        }
        if (argentR == 1 && montantR >= 200) {
          print("")
          var coupure = readLine("\n1) grosses coupures \n2) petites coupures \n>").toInt
          while (coupure < 1 || coupure > 2) {
            println("")
            println("Veuillez saisir un choix valide.")
            println("")
            argentR = readLine("\n1) grosses coupures \n2) petites coupures \n>").toInt
          }  
          if (coupure == 2) { // petites coupures
            argent(id.toInt) -= montantR
            var reste: Double = montantR
            var nombrecoupures: Int = 0
            var listebilletnombre = List[(Int, Int)]()

            val pcoupures = Array(100, 50, 20, 10)

            for (coupures <- pcoupures) {
              if (reste >= coupures) {
                val coupuresmax = (reste / coupures).toInt
                do {
                  println("Il reste " + reste + "CHF à distribuer")
                  println("")
                  println("Vous pouvez obtenir au maximum " + coupuresmax + " billet(s) de "+ coupures + " CHF")
                  val choixbillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  println("")
                  if (choixbillet == "o") {
                    nombrecoupures += coupuresmax
                    reste -= coupuresmax * coupures
                    listebilletnombre :+= (coupures, coupuresmax)
                  } else {
                    val choixbilletInt = choixbillet.toInt
                    if (choixbilletInt < coupuresmax) {
                      nombrecoupures += choixbilletInt
                      reste -= choixbilletInt * coupures
                      listebilletnombre :+= (coupures, choixbilletInt)

                    }
                  }
                } while (reste >= coupures && nombrecoupures == 0)
              }
            }
              println("Veuillez retirer la somme demandée :")
                for ((coupures, nombrebillet) <- listebilletnombre) {
                  for (x <- 1 to nombrebillet) {
                    println(x + " billet(s) de " + coupures + " CHF")
                  }
                }
            printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte : %.2f CHF",argent(id.toInt))
          }
          if (coupure == 1) {
            argent(id.toInt) -= montantR
            var reste: Double = montantR
            var nombrecoupures: Int = 0
            var listebilletnombre = List[(Int, Int)]()

            val gcoupures = Array(500, 200, 100, 50, 20, 10)

            for (coupures <- gcoupures) {
              if (reste >= coupures) {
                val coupuresmax = (reste / coupures).toInt
                do {
                  println("Il reste " + reste + "CHF à distribuer")
                  println("")
                  println("Vous pouvez obtenir au maximum " + coupuresmax + " billet(s) de "+ coupures + " CHF")
                  val choixbillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                  println("")
                  if (choixbillet == "o") {
                    nombrecoupures += coupuresmax
                    reste -= coupuresmax * coupures
                    listebilletnombre :+= (coupures, coupuresmax)
                  } else {
                    val choixbilletInt = choixbillet.toInt
                    if (choixbilletInt < coupuresmax) {
                      nombrecoupures += choixbilletInt
                      reste -= choixbilletInt * coupures
                      listebilletnombre :+= (coupures, choixbilletInt)

                    }
                  }
                } while (reste >= coupures && nombrecoupures == 0)
              }
            }
              println("Veuillez retirer la somme demandée :")
                for ((coupures, nombrebillet) <- listebilletnombre) {
                  for (x <- 1 to nombrebillet) {
                    println(x + " billet(s) de " + coupures + " CHF")
                  }
                }
            printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte : %.2f CHF",argent(id.toInt))
          }
          }
        if (argentR == 2) {
          argent(id.toInt) -= montantR * 0.95
        var reste: Double = montantR
        var nombrecoupures: Int = 0
        var listebilletnombre = List[(Int, Int)]()

        val pcoupures = Array(100, 50, 20, 10)

        for (coupures <- pcoupures) {
          if (reste >= coupures) {
            val coupuresmax = (reste / coupures).toInt
            do {
              println("Il reste " + reste + "EUR à distribuer")
              println("")
              println("Vous pouvez obtenir au maximum " + coupuresmax + " billet(s) de "+ coupures + " EUR")
              val choixbillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              println("")
              if (choixbillet == "o") {
                nombrecoupures += coupuresmax
                reste -= coupuresmax * coupures
                listebilletnombre :+= (coupures, coupuresmax)
              } else {
                val choixbilletInt = choixbillet.toInt
                if (choixbilletInt < coupuresmax) {
                  nombrecoupures += choixbilletInt
                  reste -= choixbilletInt * coupures
                  listebilletnombre :+= (coupures, choixbilletInt)

                }
              }
            } while (reste >= coupures && nombrecoupures == 0)
          }
        }
          println("Veuillez retirer la somme demandée :")
            for ((coupures, nombrebillet) <- listebilletnombre) {
              for (x <- 1 to nombrebillet) {
                println(x + " billet(s) de " + coupures + " CHF")
              }
            }
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte : %.2f CHF",argent(id.toInt))
          }
        if (argentR == 1 && montantR < 200) {
      argent(id.toInt) -= montantR
      var reste: Double = montantR
      var nombrecoupures: Int = 0
      var listebilletnombre = List[(Int, Int)]()

      val pcoupures = Array(100, 50, 20, 10)

      for (coupures <- pcoupures) {
        if (reste >= coupures) {
          val coupuresmax = (reste / coupures).toInt
          do {
            println("Il reste " + reste + "CHF à distribuer")
            println("")
            println("Vous pouvez obtenir au maximum " + coupuresmax + " billet(s) de "+ coupures + " CHF")
            val choixbillet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            println("")
            if (choixbillet == "o") {
              nombrecoupures += coupuresmax
              reste -= coupuresmax * coupures
              listebilletnombre :+= (coupures, coupuresmax)
            } else {
              val choixbilletInt = choixbillet.toInt
              if (choixbilletInt < coupuresmax) {
                nombrecoupures += choixbilletInt
                reste -= choixbilletInt * coupures
                listebilletnombre :+= (coupures, choixbilletInt)

              }
            }
          } while (reste >= coupures && nombrecoupures == 0)
        }
      }
        println("Veuillez retirer la somme demandée :")
          for ((coupures, nombrebillet) <- listebilletnombre) {
            for (x <- 1 to nombrebillet) {
              println(x + " billet(s) de " + coupures + " CHF")
            }
          }
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte : %.2f CHF",argent(id.toInt))
    }
  }

  // Changement du code pin 
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    println("")
    while (codespin(id).length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      println("")
      codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >") 
    }
  }

}