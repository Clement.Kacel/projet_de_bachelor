//Assignment: Hiba Chbib_996460_assignsubmission_file

object Main {
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn.readLine

    /*1.---------------------Création des 2 tableaux compte et codepin + saisir l'identifiant et le codepin---------------------*/

    var nbclients = 100
    val compte:  Array[Double] = Array.fill(nbclients)(1200.0)
    var codepin = Array.fill(nbclients)("INTRO1234")
    var id = readLine("Saisissez votre code identifiant > ").toInt
    var saisicode =""
    var tentative = 3
    var choix = 0
    var compteur = 1
    var compteur2 = 1

    //---------------------Quitter le programme si l'id n'existe pas
    if(id >= nbclients)println("Cet identifiant n'est pas valable")

    //---------------------Demander le code pin avec 3 tentatives et le renvoyer vers l'id si 3 tentatives incorect
    while(id < nbclients){
      if(compteur == 1)saisicode = readLine("Saisissez votre code pin > ")
      if(compteur2 == 0)saisicode = codepin(id)
      while(saisicode != codepin(id) && tentative != 0){
        tentative -= 1
        if(tentative > 0) saisicode = readLine("Code pin erroné, Il vous reste "+ tentative + " tentative(s) > ")}
      if(tentative == 0){
        println("Trop d'erreurs, abandon de l'identifictation")
        tentative = 3
        id = readLine("Saisissez votre code identifiant > ").toInt}
      if(id >= nbclients)println("Cet identifiant n'est pas valable")


      /*2.---------------------Proposer	à	l’usager/ère de	choisir	l'opération	qu'il souhaite faire et lancer l'opération---------------------*/

      if(saisicode == codepin(id)){
        tentative = 3
        println("")
        println("***************************************")
        println("Choisissez votre opération:")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
        choix = readLine(">>> ").toInt

        if(choix == 1){
          depot(id,compte)
          compteur = 0}

        if(choix == 2){
          retrait(id, compte)
          compteur = 0}

        if (choix == 3){
          printf("Le montant disponible est de : %.2f CHF \n", compte(id))
          compteur = 0}

        if(choix == 4){
          change(id,codepin)
          compteur = 0
          compteur2 = 0}

        if(choix == 5){
          println("Fin des opérations, n'oubliez pas de récupérer votre carte")
          compteur = 1
          compteur2 =1
          id = readLine("Saisissez votre code identifiant > ").toInt
          if(id >= nbclients)println("Cet identifiant n'est pas valable")}

      }
    }



    /*3.---------------------Opération de dépot---------------------*/

    def depot(id : Int, compte : Array[Double]) : Unit = {

      var devise = 0
      var depot = 0.0

      devise = readLine("Indiquez la devise du dépôt: 1) CHF ; 2) EUR > ").toInt

      if (devise == 1){
        depot = readLine("Indiquez le montant du dépôt > ").toInt
        while (depot % 10 != 0) {
          println("Le montant doit être un multiple de 10")
          depot = readLine("Indiquez le montant du dépôt > ").toDouble}
        compte(id) += depot
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f CHF \n", compte(id))}

      if (devise == 2){
        depot = readLine("Indiquez le montant du dépôt > ").toInt
        while (depot % 10 != 0) {
          println("Le montant doit être un multiple de 10")
          depot = readLine("Indiquez le montant du dépôt > ").toDouble}
        depot = depot*0.95
        compte(id) += depot
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f CHF \n", compte(id))}}



    /*4.---------------------Opération de retrait---------------------*/

    def retrait(id : Int, compte : Array[Double]) : Unit = {

      var devise2 = 0
      var retrait = 0
      var coupure = 0
      var billet500 = 0
      var billet200 = 0
      var billet100 = 0
      var billet50 = 0
      var billet20 = 0
      var billet10 = 0

      devise2 = readLine("Indiquez la devise du retrait: 1) CHF ; 2) EUR > ").toInt
      while (devise2 != 1 && devise2 != 2) {
        devise2 = readLine("Indiquez la devise du retrait: 1) CHF ; 2) EUR > ").toInt}
      retrait = readLine("Indiquez le montant du retrait > ").toInt
      while (retrait % 10 != 0 || retrait > 0.1*compte(id)) {
        if (retrait % 10 != 0) {
          println("Le montant doit être un multiple de 10")
          retrait = readLine("Indiquez le montant du retrait > ").toInt}
        if (retrait > 0.1 * compte(id)) {
          println("Votre plafond autorisé est de " + compte(id) * 0.1 + " CHF")
          retrait = readLine("Indiquez le montant du retrait > ").toInt}}


      /*-----Lorsque la devise est en CHF-----*/

      if (devise2 == 1){
        if (retrait >= 200) {
          coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
          while (coupure != 1 && coupure != 2) {
            coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt}}
        else {coupure = 2}

        /*Grosse coupures*/

        if (coupure == 1) {

          println("il reste " + retrait + " CHF à distribuer")

          if (retrait >= 500){
            println("Vous pouvez obtenir au maximum " + (retrait / 500).toInt + " billet(s) de 500 CHF")
            var proposition500 = "o"
            proposition500 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition500 != "o" && proposition500.toInt > (retrait / 500).toInt) {
              proposition500 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition500 == "o") billet500 = (retrait / 500).toInt

            if( proposition500 != "o") billet500 = proposition500.toInt
            retrait = retrait - 500 * billet500
            compte(id) = compte(id) - 500 * billet500}

          if (retrait >= 200){
            println("Vous pouvez obtenir au maximum " + (retrait / 200).toInt + " billet(s) de 200 CHF")
            var proposition200 = "o"
            proposition200 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition200 != "o" && proposition200.toInt > (retrait / 200).toInt) {
              proposition200 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition200 == "o") billet200 = (retrait / 200).toInt

            if( proposition200 != "o") billet200 = proposition200.toInt
            retrait = retrait - 200 * billet200
            compte(id) = compte(id) - 200 * billet200}

          if (retrait >= 100){
            println("Vous pouvez obtenir au maximum " + (retrait / 100).toInt + " billet(s) de 100 CHF")
            var proposition100 = "o"
            proposition100 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition100 != "o" && proposition100.toInt > (retrait / 100).toInt) {
              proposition100 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition100 == "o") billet100 = (retrait / 100).toInt

            if( proposition100 != "o") billet100 = proposition100.toInt
            retrait = retrait - 100 * billet100
            compte(id) = compte(id) - 100 * billet100}

          if (retrait >= 50){
            println("Vous pouvez obtenir au maximum " + (retrait / 50).toInt + " billet(s) de 50 CHF")
            var proposition50 = "o"
            proposition50 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition50 != "o" && proposition50.toInt > (retrait / 50).toInt) {
              proposition50 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition50 == "o") billet50 = (retrait / 50).toInt

            if( proposition50 != "o") billet50 = proposition50.toInt
            retrait = retrait - 50 * billet50
            compte(id) = compte(id) - 50 * billet50}

          if (retrait >= 20){
            println("Vous pouvez obtenir au maximum " + (retrait / 20).toInt + " billet(s) de 20 CHF")
            var proposition20 = "o"
            proposition20 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition20 != "o" && proposition20.toInt > (retrait / 20).toInt) {
              proposition20 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition20 == "o") billet20 = (retrait / 20).toInt

            if( proposition20 != "o") billet20 = proposition20.toInt
            retrait = retrait - 20 * billet20
            compte(id) = compte(id) - 20 * billet20}

          if (retrait >= 10) {
            println("Vous pouvez obtenir au maximum " + (retrait / 10).toInt + " billet(s) de 10 CHF")
            billet10 = (retrait / 10).toInt
            retrait = retrait - 10 * billet10
            compte(id) = compte(id) - 10 * billet10}

          println("Veuillez retirer la somme demandée: ")
          println("")
          if (billet500 >= 1) println(billet500 + " billet(s) de  500 CHF")
          if (billet200 >= 1) println(billet200 + " billet(s) de  200 CHF")
          if (billet100 >= 1) println(billet100 + " billet(s) de  100 CHF")
          if (billet50 >= 1) println(billet50 + " billet(s) de  50 CHF")
          if (billet20 >= 1) println(billet20 + " billet(s) de  20 CHF")
          if (billet10 >= 1) println(billet10 + " billet(s) de  10 CHF")

          println("")
          printf("Votre retrait a été pris en compte. Le nouveau montant disponible est de: %.2f CHF \n", compte(id))}

        /*Petites coupures*/

        if(coupure == 2){

          if (retrait >= 100){
            println("Vous pouvez obtenir au maximum " + (retrait / 100).toInt + " billet(s) de 100 CHF")
            var proposition100 = "o"
            proposition100 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition100 != "o" && proposition100.toInt > (retrait / 100).toInt) {
              proposition100 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition100 == "o") billet100 = (retrait / 100).toInt

            if( proposition100 != "o") billet100 = proposition100.toInt
            retrait = retrait - 100 * billet100
            compte(id) = compte(id) - 100 * billet100}

          if (retrait >= 50){
            println("Vous pouvez obtenir au maximum " + (retrait / 50).toInt + " billet(s) de 50 CHF")
            var proposition50 = "o"
            proposition50 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition50 != "o" && proposition50.toInt > (retrait / 50).toInt) {
              proposition50 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition50 == "o") billet50 = (retrait / 50).toInt

            if( proposition50 != "o") billet50 = proposition50.toInt
            retrait = retrait - 50 * billet50
            compte(id) = compte(id) - 50 * billet50}

          if (retrait >= 20){
            println("Vous pouvez obtenir au maximum " + (retrait / 20).toInt + " billet(s) de 20 CHF")
            var proposition20 = "o"
            proposition20 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")

            while(proposition20 != "o" && proposition20.toInt > (retrait / 20).toInt) {
              proposition20 = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")}

            if( proposition20 == "o") billet20 = (retrait / 20).toInt

            if( proposition20 != "o") billet20 = proposition20.toInt
            retrait = retrait - 20 * billet20
            compte(id) = compte(id) - 20 * billet20}

          if (retrait >= 10) {
            println("Vous pouvez obtenir au maximum " + (retrait / 10).toInt + " billet(s) de 10 CHF")
            billet10 = (retrait / 10).toInt
            retrait = retrait - 10 * billet10
            compte(id) = compte(id) - 10 * billet10}

          println("")
          println("Veuillez retirer la somme demandée: ")
          if (billet500 >= 1) println(billet500 + " billet(s) de  500 CHF")
          if (billet200 >= 1) println(billet200 + " billet(s) de  200 CHF")
          if (billet100 >= 1) println(billet100 + " billet(s) de  100 CHF")
          if (billet50 >= 1) println(billet50 + " billet(s) de  50 CHF")
          if (billet20 >= 1) println(billet20 + " billet(s) de  20 CHF")
          if (billet10 >= 1) println(billet10 + " billet(s) de  10 CHF")

          printf("Votre retrait a été pris en compte. Le nouveau montant disponible est de: %.2f CHF \n ", compte(id))}}

      /*-----Lorsque la devise est en EUR-----*/

      var billet100eur = 0
      var billet50eur = 0
      var billet20eur = 0
      var billet10eur = 0

      if (devise2 == 2){
        var soldeeuro = compte(id)/0.95
        println("il reste " + retrait + " EUR à distribuer")

        if (retrait >= 100) {
          println("Vous pouvez obtenir au maximum " + (retrait / 100).toInt + " billet(s) de 100 EUR")
          var proposition100eur = "o"
          proposition100eur = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")
          while(proposition100eur != "o" && proposition100eur.toInt > (retrait / 100).toInt) {
            proposition100eur = readLine("Tapez une valeur inférieure à celle proposée > ")}

          if( proposition100eur == "o") billet100eur = (retrait / 100).toInt

          if( proposition100eur != "o") billet100eur = proposition100eur.toInt

          retrait = retrait - 100 * billet100eur
          soldeeuro = soldeeuro - 100*billet100eur
          compte(id) = soldeeuro*0.95}

        if (retrait >= 50) {
          println("Vous pouvez obtenir au maximum " + (retrait / 50).toInt + " billet(s) de 50 EUR")
          var proposition50eur = "o"
          proposition50eur = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")
          while(proposition50eur != "o" && proposition50eur.toInt > (retrait / 50).toInt) {
            proposition50eur = readLine("Tapez une valeur inférieure à celle proposée > ")}

          if( proposition50eur == "o") billet50eur = (retrait / 50).toInt

          if( proposition50eur != "o") billet50eur = proposition50eur.toInt

          retrait = retrait - 50 * billet50eur
          soldeeuro = soldeeuro - 50*billet50eur
          compte(id) = soldeeuro*0.95}

        if (retrait >= 20) {
          println("Vous pouvez obtenir au maximum " + (retrait / 20).toInt + " billet(s) de 20 EUR")
          var proposition20eur = "o"
          proposition20eur = readLine("Tapez o pour ok ou une valeur inférieure à celle proposée > ")
          while(proposition20eur != "o" && proposition20eur.toInt > (retrait / 20).toInt) {
            proposition20eur = readLine("Tapez une valeur inférieure à celle proposée > ")}

          if( proposition20eur == "o") billet20eur = (retrait / 20).toInt

          if( proposition20eur != "o") billet20eur = proposition20eur.toInt

          retrait = retrait - 20 * billet20eur
          soldeeuro = soldeeuro - 20*billet20eur
          compte(id) = soldeeuro*0.95}

        if (retrait >= 10) {
          println("Vous pouvez obtenir au maximum " + (retrait / 10).toInt + " billet(s) de 10 EUR")
          billet10eur = (retrait / 10).toInt
          retrait = retrait - 10 * billet10eur
          soldeeuro = soldeeuro - 10*billet10eur
          compte(id) = soldeeuro*0.95}

        println("")
        println("Veuillez retirer la somme demandée: ")
        if (billet100eur >= 1) println(billet100eur + " billet(s) de  100 EUR")
        if (billet50eur >= 1) println(billet50eur + " billet(s) de  50 EUR")
        if (billet20eur >= 1) println(billet20eur + " billet(s) de  20 EUR")
        if (billet10eur >= 1) println(billet10eur + " billet(s) de  10 EUR")


        printf("Votre retrait a été pris en compte. Le nouveau montant disponible est de: %.2f CHF \n", compte(id))}}



    /*5.---------------------Opération de changement de code pin---------------------*/

    def change(id : Int, codepin : Array[String]) : Unit = {

      var saisicode = readLine("Saisissez votre nouveau code pin (il doit contenir 8 caractères) > ")
      while(saisicode.length < 8){
        println("Votre code pin ne contient pas au moins 8 caractères")
        saisicode = readLine("Saisissez votre nouveau code pin (il doit contenir 8 caractères > ")}

      codepin(id) = saisicode}


























  }
}