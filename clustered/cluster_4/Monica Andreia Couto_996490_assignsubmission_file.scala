//Assignment: Monica Andreia Couto_996490_assignsubmission_file

import scala.io.StdIn._
object Main {

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var pinvoulu = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString
    while (pinvoulu.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      pinvoulu = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString
    }
    codespin(id) = pinvoulu
  }

  def depot (id : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    var DeviseDep = readInt()
    while (DeviseDep < 1 || DeviseDep > 2){
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      var DeviseDep = readInt()
    }
    println("Indiquez le montant du dépôt >")
    var MontantDep = readInt().toDouble
    while (!(MontantDep%10==0)){
      println("Le montant doit être un multiple de 10")
      println("Indiquez le montant du dépôt >")
      MontantDep = readInt().toDouble
    }
    if (DeviseDep == 1){
      comptes(id) = comptes(id) + MontantDep
    } else if (DeviseDep == 2){
      comptes(id) = comptes(id) + MontantDep*0.95
    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
  }

  def retrait (id : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise :1 CHF, 2 : EUR >")
    var DeviseRe = readInt()
    while ((DeviseRe < 1) || (DeviseRe > 2)){
      println("Indiquez la devise :1 CHF, 2 : EUR >")
      DeviseRe = readInt()
    }
    println("Indiquez le montant du retrait >")
    var MontantRe = readInt().toDouble
    while (!(MontantRe%10==0)){
      println("Le montant doit être un multiple de 10")
      println("Indiquez le montant du retrait >")
      MontantRe = readInt().toDouble 
    }
    var MontantReAutorisé = comptes(id)*0.1
    var Coupures = 0
    var RépartitionCoupures500 = 0
    var RépartitionCoupures200 = 0
    var RépartitionCoupures100 = 0
    var RépartitionCoupures50 = 0
    var RépartitionCoupures20 = 0
    var RépartitionCoupures10 = 0
    var RépartitionCoupures100EUR = 0
    var RépartitionCoupures50EUR = 0
    var RépartitionCoupures20EUR = 0
    var RépartitionCoupures10EUR = 0

    if (DeviseRe == 1){ 
      while (MontantReAutorisé < MontantRe){
        printf("Votre plafond de retrait autorisé est de : %.2f", MontantReAutorisé )
        println('\n'+"Indiquez le montant du retrait >")
        MontantRe = readInt().toDouble  
        while (!(MontantRe%10==0)){
          println("Le montant doit être un multiple de 10")
          println("Indiquez le montant du retrait >")
          MontantRe = readInt().toDouble 
        }
      }

      comptes(id) = comptes(id)-MontantRe

      if (MontantRe >= 200.00){
        println("En 1) grosses coupures, 2) petites coupures >")
        Coupures = readInt()
        while ((Coupures < 1) || (Coupures > 2)){
          println("En 1) grosses coupures, 2) petites coupures >")
          Coupures = readInt()
        }
      } else if ((MontantRe < 200.00) && (MontantRe > 0)){
        Coupures = 2
      }

      if(Coupures == 1){
        if (MontantRe >= 500){
          var NbCoupures500 = (MontantRe/500).toInt
          printf("Il reste  %.2f " , MontantRe)
          println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures500 + " billet(s) de 500 CHF" + '\n' + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var CoupuresSouhaitées = readLine()
          if (CoupuresSouhaitées == "o"){
            RépartitionCoupures500 = NbCoupures500

          }else {
            RépartitionCoupures500 = CoupuresSouhaitées.toInt
          } 
          MontantRe = MontantRe-(RépartitionCoupures500*500)  
        } 

        if (MontantRe>=200){
          var NbCoupures200 = (MontantRe/200).toInt
          printf("Il reste  %.2f " , MontantRe) 
          println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures200 + " billet(s) de 200 CHF" + '\n' + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var CoupuresSouhaitées = readLine()
          if (CoupuresSouhaitées == "o"){
            RépartitionCoupures200 = NbCoupures200
          } else {
              RépartitionCoupures200 = CoupuresSouhaitées.toInt

            }
          MontantRe = MontantRe-(RépartitionCoupures200*200)  
        }

        if (MontantRe>=100){
          var NbCoupures100 = (MontantRe/100).toInt
          printf("Il reste  %.2f " , MontantRe)
          println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures100 + " billet(s) de 100 CHF" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var CoupuresSouhaitées = readLine()
          if (CoupuresSouhaitées == "o"){
            RépartitionCoupures100 = NbCoupures100
          } else {
            RépartitionCoupures100 = CoupuresSouhaitées.toInt
          }
        MontantRe = MontantRe-(RépartitionCoupures100*100)
      }

      if (MontantRe>=50){
        var NbCoupures50 = (MontantRe/50).toInt
        printf("Il reste  %.2f " , MontantRe) 
        println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures50 + " billet(s) de 50 CHF"  + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        var CoupuresSouhaitées = readLine()
        if (CoupuresSouhaitées == "o"){
          RépartitionCoupures50 = NbCoupures50
        } else {
            RépartitionCoupures50 = CoupuresSouhaitées.toInt
          }
        MontantRe = MontantRe-(RépartitionCoupures50*50)
      }

      if (MontantRe>=20){
        var NbCoupures20 = (MontantRe/20).toInt
        printf("Il reste  %.2f " , MontantRe) 
        println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures20 + " billet(s) de 20 CHF"  + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        var CoupuresSouhaitées = readLine()
        if (CoupuresSouhaitées == "o"){
          RépartitionCoupures20 = NbCoupures20
        } else {
            RépartitionCoupures20 = CoupuresSouhaitées.toInt
          }

        MontantRe = MontantRe-(RépartitionCoupures20*20)
      }
      if (MontantRe>=10){ 
        var NbCoupures10 = (MontantRe/10).toInt
        printf("Il reste  %.2f " , MontantRe)
        println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures10 + " billet(s) de 10 CHF" + '\n'+ "Tapez o pour ok >")
        var CoupuresSouhaitées = readLine()
        RépartitionCoupures10 = NbCoupures10
        MontantRe = MontantRe-(RépartitionCoupures10*10)
      } 
      if ((MontantRe == 0)){
        println("Veuillez retirer la somme demandée :")
        if(RépartitionCoupures500>0){
          println(RépartitionCoupures500+" billet(s) de 500 CHF")
        }
        if(RépartitionCoupures200>0){
          println(RépartitionCoupures200+" billet(s) de 200 CHF")
        }
        if(RépartitionCoupures100>0){
          println(RépartitionCoupures100+" billet(s) de 100 CHF")
        }
        if(RépartitionCoupures50>0){
          println(RépartitionCoupures50+" billet(s) de 50 CHF")
        }
        if(RépartitionCoupures20>0){
          println(RépartitionCoupures20+" billet(s) de 20 CHF")
        }
        if(RépartitionCoupures10>0){
          println(RépartitionCoupures10+" billet(s) de 10 CHF")
        }
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
      }


      } else if (Coupures == 2){
          if (MontantRe>=100){
            var NbCoupures100 = (MontantRe/100).toInt
            printf("Il reste  %.2f " , MontantRe)
            println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures100 + " billet(s) de 100 CHF" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var CoupuresSouhaitées = readLine()
            if (CoupuresSouhaitées == "o"){
                RépartitionCoupures100 = NbCoupures100
            } else {
                RépartitionCoupures100 = CoupuresSouhaitées.toInt
              }
            MontantRe = MontantRe-(RépartitionCoupures100*100)
          }

          if (MontantRe>=50){
            var NbCoupures50 = (MontantRe/50).toInt
            printf("Il reste  %.2f " , MontantRe) 
            println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures50 + " billet(s) de 50 CHF" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var CoupuresSouhaitées = readLine()
            if (CoupuresSouhaitées == "o"){
              RépartitionCoupures50 = NbCoupures50
            } else {
                RépartitionCoupures50 = CoupuresSouhaitées.toInt
              }
            MontantRe = MontantRe-(RépartitionCoupures50*50)
          }

          if (MontantRe>=20){
            var NbCoupures20 = (MontantRe/20).toInt
            printf("Il reste  %.2f " , MontantRe) 
            println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures20 + " billet(s) de 20 CHF" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var CoupuresSouhaitées = readLine()
            if (CoupuresSouhaitées == "o"){
              RépartitionCoupures20 = NbCoupures20
            } else {
                RépartitionCoupures20 = CoupuresSouhaitées.toInt
              }

            MontantRe = MontantRe-(RépartitionCoupures20*20)
          }
          if (MontantRe>=10){ 
            var NbCoupures10 = (MontantRe/10).toInt
            printf("Il reste  %.2f " , MontantRe)
            println(" CHF à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures10 + " billet(s) de 10 CHF" + '\n'+ "Tapez o pour ok >")
            var CoupuresSouhaitées = readLine()
            RépartitionCoupures10 = NbCoupures10
            MontantRe = MontantRe-(RépartitionCoupures10*10)
          } 
        if ((MontantRe == 0)){
          println("Veuillez retirer la somme demandée :")

          if(RépartitionCoupures100>0){
            println(RépartitionCoupures100+" billet(s) de 100 CHF")
          }
          if(RépartitionCoupures50>0){
            println(RépartitionCoupures50+" billet(s) de 50 CHF")
          }
          if(RépartitionCoupures20>0){
            println(RépartitionCoupures20+" billet(s) de 20 CHF")
          }
          if(RépartitionCoupures10>0){
            println(RépartitionCoupures10+" billet(s) de 10 CHF")
          }
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
        }
      }

    } else if (DeviseRe == 2){ 
        MontantReAutorisé = comptes(id)*1.05*0.1
        while (MontantReAutorisé < MontantRe){
          printf("Votre plafond de retrait autorisé est de : %.2f", MontantReAutorisé )
          println('\n'+"Indiquez le montant du retrait >")
          MontantRe = readInt().toDouble 
          while (!(MontantRe%10==0)){
            println("Le montant doit être un multiple de 10")
            println("Indiquez le montant du retrait >")
            MontantRe = readInt().toDouble 
          }
        }

        comptes(id) = comptes(id)-(MontantRe*0.95)



        if (MontantRe>=100){
          var NbCoupures100EUR = (MontantRe/100).toInt
          printf("Il reste  %.2f " , MontantRe)
          println(" EUR à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures100EUR + " billet(s) de 100 EUR" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var CoupuresSouhaitées = readLine()
          if (CoupuresSouhaitées == "o"){
              RépartitionCoupures100EUR = NbCoupures100EUR
          } else {
              RépartitionCoupures100EUR = CoupuresSouhaitées.toInt
            }
          MontantRe = MontantRe-(RépartitionCoupures100EUR*100)
        }

        if (MontantRe>=50){
          var NbCoupures50EUR = (MontantRe/50).toInt
          printf("Il reste  %.2f " , MontantRe) 
          println(" EUR à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures50EUR + " billet(s) de 50 EUR" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var CoupuresSouhaitées = readLine()
          if (CoupuresSouhaitées == "o"){
            RépartitionCoupures50EUR = NbCoupures50EUR
          } else {
              RépartitionCoupures50EUR = CoupuresSouhaitées.toInt
            }
          MontantRe = MontantRe-(RépartitionCoupures50EUR*50)
        }

        if (MontantRe>=20){
          var NbCoupures20EUR = (MontantRe/20).toInt
          printf("Il reste  %.2f " , MontantRe) 
          println(" EUR à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures20EUR + " billet(s) de 20 EUR" + '\n'+ "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          var CoupuresSouhaitées = readLine()
          if (CoupuresSouhaitées == "o"){
            RépartitionCoupures20EUR = NbCoupures20EUR
          } else {
              RépartitionCoupures20EUR = CoupuresSouhaitées.toInt
            }

          MontantRe = MontantRe-(RépartitionCoupures20EUR*20)
        }
        if (MontantRe>=10){ 
          var NbCoupures10EUR = (MontantRe/10).toInt
          printf("Il reste  %.2f " , MontantRe)
          println(" EUR à distribuer" + '\n'+ "Vous pouvez obtenir au maximum " + NbCoupures10EUR + " billet(s) de 10 EUR" + '\n'+ "Tapez o pour ok >")
          var CoupuresSouhaitées = readLine()
          RépartitionCoupures10EUR = NbCoupures10EUR
          MontantRe = MontantRe-(RépartitionCoupures10EUR*10)
        } 

      if ((MontantRe == 0)){
        println("Veuillez retirer la somme demandée :")
        if(RépartitionCoupures100EUR>0){
          println(RépartitionCoupures100EUR+" billet(s) de 100 EUR")
        }
        if(RépartitionCoupures50EUR>0){
          println(RépartitionCoupures50EUR+" billet(s) de 50 EUR")
        }
        if(RépartitionCoupures20EUR>0){
          println(RépartitionCoupures20EUR+" billet(s) de 20 EUR")
        }
        if(RépartitionCoupures10EUR>0){
          println(RépartitionCoupures10EUR+" billet(s) de 10 EUR")
        }
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
      }
    }
  }

  def main(args: Array[String]): Unit = {

    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var id = 0
    var tentatives = 3
    var validation = false
    var pin = ""
    var MenuDOP = "Choisissez votre opération :"+'\n'+"1) Dépôt"+'\n'+"2) Retrait"+'\n'+"3) Consultation du compte"+'\n'+ "4) Changement du code pin"+'\n'+"5) Terminer"+'\n'+"Votre choix :"
    var choix = 5

    while (choix == 5){
      choix = 0
      while (!validation){
        id = readLine( "Saisissez votre code identifiant >").toInt
        if (id > nbclients){
          println("Cet identifiant n'est pas valable.")
          choix = 10
          validation = true
        } else {
          pin = readLine("Saisissez votre code pin >").toString

          if (pin != codespin(id)){
            while (tentatives > 1 && pin != codespin(id)){
              tentatives -= 1
              pin = readLine("Code pin erroné, il vous reste " + tentatives + " tentatives > ").toString
            } 
            if (tentatives == 1 && pin != codespin(id)){
              println("Trop d’erreurs, abandon de l’identification")
              validation = false
              tentatives = 3 
            } else if (pin == codespin(id)){
                validation = true
                tentatives = 3
            }
          } else if (pin == codespin(id)){
              validation = true
              tentatives = 3
            }
          }

      }
      while (choix != 5 && choix !=10){
        println(MenuDOP)
        choix = readInt()
        if (choix == 5){
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          validation = false
          tentatives = 3
        } else if (choix == 3) {
          printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
        } else if (choix == 4) {
          changepin(id, codespin) 
        } else if (choix == 1) {
          depot(id, comptes)
        } else if (choix == 2) {
          retrait(id, comptes)
        }
      }
    }
  }
}
