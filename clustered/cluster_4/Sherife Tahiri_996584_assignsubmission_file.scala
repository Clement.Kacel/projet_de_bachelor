//Assignment: Sherife Tahiri_996584_assignsubmission_file

import scala.io.StdIn._
import scala.math

object Main {
  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")
  
  def identification(): Int = {
    println("Saisissez votre code identifiant >")
    var choixidentifiant = readInt()
    if (choixidentifiant <= -1 || choixidentifiant >= nbclients){
      println("Cet identifiant n'est pas valable.")
      return -1 // identifiant invalide pour échec d'identification
    } else {

      ///// code PIN que l0utilisateur rentre  //////
      println("Saisissez votre code pin >")
      var pin = readLine().toString

      // codespin(i)
      var tentatives = 3
      while(tentatives > 1 && pin != codespin(choixidentifiant)) { 
        tentatives=tentatives - 1
        println("Code pin erroné, il vous reste " + tentatives + " tentatives")

        ///// code PIN que l0utilisateur rentre  //////
        println("Saisissez votre code pin >")
        pin = readLine().toString
      }

      if(pin != codespin(choixidentifiant)) {
          println("Trop d'erreurs, abandon de l'identification")
          identification()
      } else {
        return choixidentifiant
      }
    }
  }

  def interface(id: Int): Unit = {
    println("choisissez votre opération :")
    println("1) Dépôt")
    println("2) Retrait")
    println("3) Consultation du compte ")
    println("4) Changement de code pin")
    println("5) Terminer")
    println("Votre choix: ")
    var choix=readInt()
    if(choix == 1){ // depot
      depot(id, comptes)
    } else if(choix == 2) { // retrait
      retrait(id, comptes)
    } else if(choix == 3) { // consultation compte
      consultation(id, comptes)
    } else if(choix == 4) { // changer pin
     changercodepin(id)
    } else if(choix == 5) { // terminer
      terminer()
    }
  }

  def consultation(id: Int, comptes: Array[Double]): Unit = {
    printf("Le montant disponible sur votre compte est de: %.2f \n", comptes(id))
    interface(id)
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la devise du depot: 1) CHF ; 2) EUR: ")
    var devise = readInt()
    while(!(devise==1 || devise==2)){
      print("Indiquez la devise: 1)CHF ; 2:EUR : ")
      devise = readInt()
    }
    
    print("Indiquez le montant du dépôt : ")
    var montantdepot = readInt()
    
    while(montantdepot % 10 != 0){
      println("Veuillez entrer un nombre qui est un multiple de 10")
      println("Indique le montant du dépôt : ")
      montantdepot = readInt()
    }

    var montantdispo = comptes(id)
      var nouveaumontantdispo = 0.0
      if(devise == 1){
        nouveaumontantdispo = montantdispo + montantdepot
      } else {
        nouveaumontantdispo = montantdispo + (montantdepot * 0.95)
      }

      comptes(id) = nouveaumontantdispo
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", nouveaumontantdispo)
      interface(id) // On retourne à l'interface de notre identifiant
    }

    def retrait(id: Int, comptes: Array[Double]): Unit = {
      println("Indiquez la devise: 1)CHF ; 2)EUR : ")
      var devise =readInt()
      while(!(devise==1 || devise==2)){
        println("Indiquez la devise: 1)CHF ; 2)EUR : ")
        devise =readInt()
      }

      var retrait = 0
      var montantretrait = 0.0
      var plafond = -1.0
      while(montantretrait > plafond) {
        println("Indiquez le montant du retrait : ")
        retrait = readInt()
        while (retrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
          retrait = readInt() 
        }

        if(devise == 1) {
          montantretrait = retrait
          plafond = 0.1 * comptes(id)
        } else if (devise == 2) {
          montantretrait = retrait * 0.95
          plafond = 0.1 * comptes(id) * 0.95
        }

        if(montantretrait > plafond){
          printf("Votre plafond de retrait autorisé est de : %.2f \n", plafond)
        }
      }

      var coupures = 2
      if(devise == 1 && montantretrait >= 200){
        println("En 1) Grosses coupures ; 2) Petites coupures")
        coupures = readInt()
        while(coupures != 1 && coupures != 2){
          println("En 1) Grosses coupures ; 2) Petites coupures")
          coupures = readInt()
        }
      }

      if(devise == 1){ // CHF
        var nouveauretrait = retrait

        var nombrebillets500 = 0
        var nombrebillets200 = 0
        if(coupures == 1) { // grosses coupures
          // 500
          if(nouveauretrait >= 500) {
            println("Il reste "+ nouveauretrait +" CHF à distribuer")
            var nombrebillets = nouveauretrait / 500
            println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 500 CHF")
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            var selection = readLine()
            while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
              println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
              selection = readLine()
            }
  
            if(selection.equals("o")) {
              nombrebillets500 = nombrebillets
            } else {
              nombrebillets500 = selection.toInt
            }
  
            nouveauretrait = nouveauretrait - nombrebillets500 * 500
          }
  
          // 200
          if(nouveauretrait >= 200) {
            println("Il reste "+ nouveauretrait +" CHF à distribuer")
            var nombrebillets = nouveauretrait / 200
            println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 200 CHF")
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            var selection = readLine()
            while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
              println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
              selection = readLine()
            }
  
            if(selection.equals("o")) {
              nombrebillets200 = nombrebillets
            } else {
              nombrebillets200 = selection.toInt
            }
  
            nouveauretrait = nouveauretrait - nombrebillets200 * 200
          }
        }

        // 100
        var nombrebillets100 = 0
        if(nouveauretrait >= 100) {
          println("Il reste "+ nouveauretrait +" CHF à distribuer")
          var nombrebillets = nouveauretrait / 100
          println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 100 CHF")
          println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
          var selection = readLine()
          while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            selection = readLine()
          }

          if(selection.equals("o")) {
            nombrebillets100 = nombrebillets
          } else {
            nombrebillets100 = selection.toInt
          }

          nouveauretrait = nouveauretrait - nombrebillets100 * 100
        }

        // 50
        var nombrebillets50 = 0
        if (nouveauretrait >= 50) {
          println("Il reste "+ nouveauretrait +" CHF à distribuer")
          var nombrebillets = nouveauretrait / 50
          println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 50 CHF")
          println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
          var selection=readLine()
          while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            selection=readLine()
          }

          if(selection.equals("o")) {
            nombrebillets50 = nombrebillets
          } else {
            nombrebillets50 = selection.toInt
          }

          nouveauretrait = nouveauretrait - nombrebillets50 * 50
        }

        // 20
        var nombrebillets20 = 0
        if (nouveauretrait >= 20) {
          println("Il reste "+ nouveauretrait +" CHF à distribuer")
          var nombrebillets = nouveauretrait / 20
          println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 20 CHF")
          println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
          var selection=readLine()
          while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            selection=readLine()
          }

          if(selection.equals("o")) {
            nombrebillets20 = nombrebillets
          } else {
            nombrebillets20 = selection.toInt
          }

          nouveauretrait = nouveauretrait - nombrebillets20 * 20
        }

        // 10
        var nombrebillets10 = 0
        if (nouveauretrait >= 10) {
          println("Il reste "+ nouveauretrait +" CHF à distribuer")
          nombrebillets10 = nouveauretrait / 10
          println("Vous devez prendre " + nombrebillets10 + " billet(s) de 10 CHF")

          nouveauretrait = nouveauretrait - nombrebillets10 * 10
        }

        println("Veuillez retirer la somme demandée : ")
        if(nombrebillets500 > 0) {
          println(nombrebillets500 + " billet(s) de 500 CHF")
        }
        if(nombrebillets200 > 0) {
          println(nombrebillets200 + " billet(s) de 200 CHF")
        }
        if(nombrebillets100 > 0) {
          println(nombrebillets100 + " billet(s) de 100 CHF")
        }
        if(nombrebillets50 > 0) {
          println(nombrebillets50 + " billet(s) de 50 CHF")
        }
        if(nombrebillets20 > 0) {
          println(nombrebillets20 + " billet(s) de 20 CHF")
        }
        if(nombrebillets10 > 0) {
          println(nombrebillets10 + " billet(s) de 10 CHF")
        }
      } else if (devise == 2){ // EUR
        var nouveauretrait = retrait

        // 100
        var nombrebillets100 = 0
        if(nouveauretrait >= 100) {
          println("Il reste "+ nouveauretrait +" EUR à distribuer")
          var nombrebillets = nouveauretrait / 100
          println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 100 EUR")
          println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
          var selection = readLine()
          while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            selection = readLine()
          }

          if(selection.equals("o")) {
            nombrebillets100 = nombrebillets
          } else {
            nombrebillets100 = selection.toInt
          }

          nouveauretrait = nouveauretrait - nombrebillets100 * 100
        }

        // 50
        var nombrebillets50 = 0
        if (nouveauretrait >= 50) {
          println("Il reste "+ nouveauretrait +" EUR à distribuer")
          var nombrebillets = nouveauretrait / 50
          println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 50 EUR")
          println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
          var selection=readLine()
          while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            selection=readLine()
          }

          if(selection.equals('o')) {
            nombrebillets50 = nombrebillets
          } else {
            nombrebillets50 = selection.toInt
          }

          nouveauretrait = nouveauretrait - nombrebillets50 * 50
        }

        // 20
        var nombrebillets20 = 0
        if (nouveauretrait >= 20) {
          println("Il reste "+ nouveauretrait +" EUR à distribuer")
          var nombrebillets = nouveauretrait / 20
          println("Vous pouvez obtenir au maximum " + nombrebillets + " billet(s) de 20 EUR")
          println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
          var selection=readLine()
          while(!selection.equals("o") && (selection.toInt < 0 || selection.toInt >= nombrebillets)) {
            println("Tapez o pour ok ou une autre valeur inférieur à celle proposée >")
            selection=readLine()
          }

          if(selection.equals("o")) {
            nombrebillets20 = nombrebillets
          } else {
            nombrebillets20 = selection.toInt
          }

          nouveauretrait = nouveauretrait - nombrebillets20 * 20
        }

        // 10
        var nombrebillets10 = 0
        if (nouveauretrait >= 10) {
          println("Il reste "+ nouveauretrait +" EUR à distribuer")
          nombrebillets10 = nouveauretrait / 10
          println("Vous devez prendre " + nombrebillets10 + " billet(s) de 10 EUR")

          nouveauretrait = nouveauretrait - nombrebillets10 * 10
        }

        println("Veuillez retire la somme demandée : ")
        if(nombrebillets100 > 0) {
          println(nombrebillets100 + " billet(s) de 100 EUR")
        }
        if(nombrebillets50 > 0) {
          println(nombrebillets50 + " billet(s) de 50 EUR")
        }
        if(nombrebillets20 > 0) {
          println(nombrebillets20 + " billet(s) de 20 EUR")
        }
        if(nombrebillets10 > 0) {
          println(nombrebillets10 + " billet(s) de 10 EUR")
        }
      }

      // update compte 
      comptes(id) = comptes(id) - montantretrait
      printf("Le montant du compte passe à %.2f \n", comptes(id))
      interface(id)
    }

  def changercodepin(id: Int): Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères)")
    var nouveaucodepin = readLine().toString
  
    while(nouveaucodepin.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères")
  
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères)")
      nouveaucodepin = readLine().toString
    }
  
    codespin(id) = nouveaucodepin
    println("Code pin modifé.")
    interface(id)
  }

  def terminer(): Unit = {
    println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
    bancomat()
  }

  def bancomat(): Unit = {
    var choixidentifiant = identification()
    // le code continue seulement si l'identification est bonne
    if(choixidentifiant != -1) {
        interface(choixidentifiant)
    }
  }
  
  def main(args: Array[String]): Unit = {
    bancomat()
  }
}