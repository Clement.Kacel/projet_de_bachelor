//Assignment: Margot Silvia Barben_996613_assignsubmission_file

object Main {
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn._
    import scala.util.control.Breaks._

    // Oubli exo 1 -> "breakable" pour que "break" fonctionne
    breakable {
      val nbclients = 100

      var codespin:Array[String] = Array.fill(nbclients)("INTRO1234")
      var pin_Utilisateur = ""
      var tentatives = 0
      var operation = 0
      var devise = 0
      var depott = 0
      var depot_EURO = 0.0
      var retraitt = 0
      // Pour pouvoir modifier la valeur ajoutée au montant si devise = EURO sans que billets soient affectés
      var retrait_init = 0.0
      var plafond = 0.0
      var coupures = 0
      var n_500 = 0
      var n_200 = 0
      var n_100 = 0
      var n_50 = 0
      var n_20 = 0
      var n_10 = 0
      var ok = ""
      // Oubli exo 1 -> string vide pour affichage nb billets retrait
      var ph = ""
      // Chgmt nom variables "depott" et "retraitt" pour ne pas créer d'erreur avec nom méthodes
      
      var comptes:Array[Double] = Array.fill(nbclients)(1200.0)
      var identifiant = 0

      // Méthodes placées avant boucle pin pour pouvoir être utilisées dans celle-ci
      def depot(id : Int, comptes : Array[Double]) : Unit = {
        
        devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
        depott = readLine("Indiquez le montant du dépôt >").toInt

        // Controle multiple de 10 avec opération % :
        while (depott % 10 != 0) depott = readLine("Le montant doit être un multiple de 10 >").toInt

        //Conversion et ajout au montant selon devise :
        if (devise == 2) {
          depot_EURO = depott
          depot_EURO *= 0.95
          comptes(id) += depot_EURO
        } else comptes(id) += depott

        // printf pour affichage montant 
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
      }

      
      def retrait(id : Int, comptes : Array[Double]) : Unit = {
        // "do...while" et non "while" afin que la devise soit demandée à l'utilisateur même si déjà = 1 ou = 2 (opération dépôt)
        do {
          devise = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
        } while ((devise != 1) && (devise != 2))

        retraitt = readLine("Indiquez le montant du retrait >").toInt
        while (retraitt % 10 != 0) retraitt = readLine("Le montant doit être un multiple de 10 >").toInt

        // Calul plafond et vérification si montant retrait <= plafond
        plafond = 0.1 * comptes(id)
        while (retraitt > plafond) {
          // Rajout printf pour que montant plafond = bon format
          printf("Votre plafond de retrait autorisé est de : %.2f \n", plafond)
          retraitt = readLine("Indiquez le montant du retrait >").toInt
          // Oubli exo 1 -> nouveau retrait doit être multiple de 10
          while (retraitt % 10 != 0) retraitt = readLine("Le montant doit être un multiple de 10 >").toInt
        }
        retrait_init = retraitt
        if (devise == 2) retrait_init *= 0.95
        comptes(id)-= retrait_init

        // Conditionelle grosses coupures 
        if ((retraitt >= 200) && (devise == 1)) {
          do {
            coupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          } while ((coupures != 1) && (coupures != 2))
        }

        // Répartition grosses coupures
        if (coupures == 1) {
          if (retraitt >= 500) n_500 = retraitt / 500

          if (n_500 != 0){
            println("Il reste " + retraitt + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + n_500 + " billet(s) de 500 CHF")
            do {
              ok = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } while ((ok != "o") && (ok.toInt >= n_500))

            //Conditionnelles pour cas de figures : 1) "o" 2) nouvelle valeur
            if (ok == "o") {
              retraitt -= (n_500 * 500)
              ph += n_500 + " billet(s) de 500 CHF\n"
            }
            
            else if (ok.toInt > 0) {
              n_500 = ok.toInt
              retraitt -= (n_500 * 500)
              ph += n_500 + " billet(s) de 500 CHF\n"
            }
            }
          

          if (retraitt >= 200) n_200 = retraitt / 200

          if (n_200 != 0) {
            println("Il reste " + retraitt + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + n_200 + " billet(s) de 200 CHF")
            do {
              ok = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            } while ((ok != "o") && (ok.toInt >= n_200))

            if (ok == "o") {
              retraitt -= (n_200 * 200)
              ph += n_200 + " billet(s) de 200 CHF\n"
            }
             
            else if (ok.toInt > 0) {
              n_200 = ok.toInt
              retraitt -= (n_200 * 200)
              ph += n_200 + " billet(s) de 200 CHF\n"
            }
          }
        }
        // Sortie de la conditionnelle grosses coupures car passage à billets <= 100
        if (retraitt >= 100) n_100 = retraitt / 100

        if (n_100 != 0) {
          if (devise == 1) {
            println("Il reste " + retraitt + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + n_100 + " billets de 100 CHF")
          }
          else {
            println("Il reste " + retraitt + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + n_100 + " billets de 100 EUR")
            }
          do {
            ok = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          } while ((ok != "o") && (ok.toInt >= n_100))

          if (ok == "o") {
            retraitt -= (n_100 * 100)
            if (devise == 1) ph += n_100 + " billet(s) de 100 CHF\n"
            else ph += n_100 + " billet(s) de 100 EUR\n"
          }
            
          else if (ok.toInt > 0){
            n_100 = ok.toInt
            retraitt -= (n_100 * 100)
            if (devise == 1) ph += n_100 + " billet(s) de 100 CHF\n"
            else ph += n_100 + " billet(s) de 100 EUR\n"
          }
        }

        if (retraitt >= 50) n_50 = retraitt / 50

        if (n_50 != 0) {
          if (devise == 1) {
            println("Il reste " + retraitt + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + n_50 + " billets de 50 CHF")
          }
          else {
            println("Il reste " + retraitt + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + n_50 + " billets de 50 EUR")
          }
          do {
            ok = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          } while ((ok != "o") && (ok.toInt >= n_50))

          if (ok == "o") {
            retraitt -= (n_50 * 50)
            if (devise == 1) ph += n_50 + " billet(s) de 50 CHF\n"
            else ph += n_50 + " billet(s) de 50 EUR\n"
          }
           
          else if (ok.toInt > 0){
            n_50 = ok.toInt
            retraitt -= (n_50 * 50)
            if (devise == 1) ph += n_50 + " billet(s) de 50 CHF\n"
            else ph += n_50 + " billet(s) de 50 EUR\n"
          }
        }

        if (retraitt >= 20) n_20 = retraitt / 20

        if (n_20 != 0) {
          if (devise == 1) {
            println("Il reste " + retraitt + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + n_20 + " billets de 20 CHF")
            }
          else {
            println("Il reste " + retraitt + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + n_20 + " billets de 20 EUR")
           }
          do {
            ok = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          } while ((ok != "o") && (ok.toInt >= n_20))

          if (ok == "o") {
            retraitt -= (n_20 * 20)
            if (devise == 1) ph += n_20 + " billet(s) de 20 CHF\n"
            else ph += n_20 + " billet(s) de 20 EUR\n"
          }
          
          else if (ok.toInt > 0){
            n_20 = ok.toInt
            retraitt -= (n_20 * 20)
            if (devise == 1) ph += n_20 + " billet(s) de 20 CHF\n"
            else ph += n_20 + " billet(s) de 20 EUR\n"
          }
        }

        if (retraitt >= 10) n_10 = retraitt / 10

        if (n_10 != 0) {
          if (devise == 1) {
            println("Il reste " + retraitt + " CHF à distribuer")
            println("Vous obtiendrez " + n_10 + " billets de 10 CHF")
            ph += n_10 + " billet(s) de 10 CHF"
          }
          else {
            println("Il reste " + retraitt + " EUR à distribuer")
            println("Vous obtiendrez " + n_10 + " billets de 10 EUR")
            ph += n_10 + " billet(s) de 10 EUR"
          }
          retraitt -= (n_10 * 10)
        }
        println("Veuillez retirer la somme demandée :")
        println(ph)
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
        // Réinit nb billets
        n_500 = 0
        n_200 = 0
        n_100 = 0
        n_50 = 0
        n_20 = 0
        n_10 = 0
        ph = ""
      }

      def changepin(id : Int, codespin : Array[String]) : Unit = {

        codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        
        while (codespin(id).length() < 8) {
          println("Votre code pin ne contient pas au moins 8 caractères")
          codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
          }
        // Màj "pin_Utilisateur" pour correspondre à 1ère conditionnelle boucle opérations
        pin_Utilisateur = codespin(id)
        }

      
      
      // Se répète tant que choix = 5 ou mauvais pin
      while ((pin_Utilisateur != codespin(identifiant)) || (operation == 5)) {
        
        identifiant = readLine("Saisissez votre code identifiant >").toInt
        // Index va jusqu'à 99 donc utilisation de ">="
        if (identifiant >= nbclients) {
          println("Cet identifiant n’est pas valable.")
          // seule manière de sortir du programme = si id > 99
          break
        }

        pin_Utilisateur = readLine("Saisissez votre code pin >")
        // Initiation à 2 car 1ère tentaive au-dessus
        tentatives = 2
        while ((pin_Utilisateur != codespin(identifiant)) && (tentatives != 0)) {
          pin_Utilisateur = readLine("Code pin erroné, il vous reste " + tentatives + " tentative(s) >")
          tentatives -= 1
        }

        // 2ème condition pour éviter que message soit affiché quand pin est juste et qu'il restait 1 tentative
        if ((tentatives == 0) && (pin_Utilisateur != codespin(identifiant))) println("Trop d’erreurs, abandon de l’identification")

        // Pour éviter que boucle = infinie si pas 1er passage (5 sélectionné avant)
        operation = 0
        
        // Boucle dans boucle pour continuer opérations dans même compte
        while ((pin_Utilisateur == codespin(identifiant)) && (operation != 5)) {
          // Triple guillemets pour faire un multi-line string :
          operation = readLine("""Choisissez votre opération :
            1) Dépôt
            2) Retrait
            3) Consultation du compte
            4) Changement du code pin
            5) Terminer
            Votre choix : """).toInt

          if (operation == 1) depot(identifiant, comptes)
          else if (operation == 2) retrait(identifiant, comptes)
          else if (operation == 3) println("Le montant disponible sur votre compte est de : " + comptes(identifiant))
          else if (operation == 4) changepin(identifiant, codespin)
          // Message opération 5 avant retour au début de boucle parente
          else println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        } 
      }
    }
  }
}
