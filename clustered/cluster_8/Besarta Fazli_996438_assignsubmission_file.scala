//Assignment: Besarta Fazli_996438_assignsubmission_file

import io.StdIn._
object Main {

  var operation = 0

  // Depot 
  var depotenChf = 0
  var depotenEur = 0
  var depotDevise = 0

  // Retrait 
  var retraitenChf = 0
  var retraitenEur = 0
  var retraitDevise = 0
  var retraitValide = false

  // Coupures 


  var codeErrone = false 

  // Les différentes coupures possibles de retirer 
  var billet500 = 0
  var billet200 = 0
  var billet100 = 0
  var billet50 = 0
  var billet20 = 0
  var billet10 = 0

  var nombreClients = 100 
  var comptes: Array [Double] = Array.fill(nombreClients)(1200.0)
  var codePin: Array [String] = Array.fill(nombreClients)("INTRO1234")

  def main(args: Array[String]): Unit = {


    var id = readLine("Saisissez votre code identifiant ").toInt

    if(id > nombreClients){
      println("Cet identifiant n'est pas valable.")

    System.exit(0)

    }

    var nbrEssai = 3
    var codepin: String = "Saisissez votre code pin "

    while (codepin != codePin(id)) {

      codepin = readLine("Saisissez votre code pin ").toString
      nbrEssai = nbrEssai - 1

      if (codepin != codePin(id) && nbrEssai != 0) {

    // chaque fois que l'on se trompe de code pin, on perd une tentative 

    println("Code pin erroné, il vous reste " + nbrEssai + " tentatives")
      }

    if ((nbrEssai == 0)  && (codepin != codePin(id))) {
      println("Trop d'erreurs, abandon de l'indentification")
    var id = readLine("Saisissez votre code identifiant ").toInt

    } 


    }

    var operation : Int = 0

    while (operation != 5) {

    operation = readLine("Choisissez votre opération : \n 1. Dépot \n 2. Retrait \n 3. Consultation du compte \n 4. Changement de code pin \n 5. Terminer \n").toInt

    operation match {
      case 1 => depot(id, comptes)
      case 2 => retrait(id, comptes)
      case 3 => consultation(id)
      case 4 => changepin(id, codePin)
      case 5 => terminer()

    }

      }



def depot(id: Int, comptes: Array[Double]) : Unit = {

    if (operation == 1) {
      depotDevise = readLine("Indiquez la devise du dépot : 1) CHF ; 2) EUR ").toInt
      if (depotDevise == 1) {
        depotenChf = readLine("Indiquez le montant à déposer :").toInt
        while(depotenChf%10 != 0) {
          depotenChf = readLine("Indiquez le montant à déposer :").toInt
          }

      // somme totale aujoutée sur le compte bancaire + somme déjà disponible 
      comptes(id) = comptes(id) + depotenChf
        }

      if (depotDevise == 2) {
        depotenEur = readLine("Indiquez le montant à déposer :").toInt

      while (depotenEur%10 != 0) {
        depotenEur = readLine("Indiquez le montant à déposer :").toInt
        }
      comptes(id) = comptes(id) + depotenEur*0.95
      comptes(id) = math.floor(comptes(id) * 10)/10
        }

      println("Votre dépot a été pris en compte, le nouveau montant disponible sur votre compte est de " + comptes(id))
        }


    } // fin def depot

def retrait(id: Int, comptes: Array[Double]) : Unit = {

  var billet10 = 0
  var billet20 = 0
  var billet50 = 0
  var billet100 = 0
  var billet200 = 0
  var billet500 = 0


    retraitDevise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR ").toInt
    while((retraitDevise < 1 || retraitDevise > 2)) {
      retraitenChf = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR ").toInt
      }

  if (retraitDevise == 1) {
    while (retraitValide == false) {
      retraitenChf = readLine("Indiquez le montant à retirer :").toInt
      retraitValide = true 
  if (retraitenChf%10 != 0) {
    println("Le montant doit être un multipe de 10")
      retraitValide = false 
    }
  if (retraitenChf > comptes(id)/10) {
    println("Votre plafond de retrait autorisé est de : " + (comptes(id)/10))
    retraitValide = false 
    }
  }

  // somme d'argent retirée du compte bancaire en francs moins la somme disponible avant cette opération 
  comptes(id) = comptes(id) - retraitenChf 
  comptes(id) = math.floor(comptes(id) * 10)/10

var coupure: String = ""

  if (retraitenChf >= 200) {
    coupure = readLine("En 1) grosse coupure ; 2) petite coupure").toString

  }

  else coupure == "2"
  if (coupure == "1") {
    billet500 = (retraitenChf/500).toInt
    billet200 = (retraitenChf%500/200).toInt
    billet100 = (retraitenChf%500%200/100).toInt
    billet50 = (retraitenChf%500%200%100/50).toInt
    billet20 = (retraitenChf%500%200%100%50/20).toInt
    billet10 = (retraitenChf%500%200%100%50%20/10).toInt

    }

  if (coupure == "2") {
    billet500 = 0
    billet200 = 0
    billet100 = (retraitenChf/100).toInt
    billet50 = (retraitenChf%100/50).toInt
    billet20 = (retraitenChf%100%50/20).toInt
    billet10 = (retraitenChf%100%50%20/10).toInt
    }

  println("Veuillez retirer la somme demandée :")

    if (billet500 != 0)
      println(billet500 + " billet(s) de 500 CHF")

    if (billet200 != 0)
      println(billet200 + " billet(s) de 200 CHF")

    if (billet100 != 0)
      println(billet100 + " billet(s) de 100 CHF")

    if (billet50 != 0)
      println(billet50 + " billet(s) de 50 CHF")

    if (billet20 != 0)
      println(billet20 + " billet(s) de 20 CHF")

    if (billet10 != 0)
      println(billet10 + " billet(s) de 10 CHF")

    }

    if (retraitDevise == 2) {
      while (retraitValide == false) {

    retraitenEur = readLine("Indiquez le montant à retirer :").toInt
    retraitValide = true 

    if (retraitenEur%10 != 0) {
    retraitValide = false 
    println("Le montant doit être un multipe de 10")
    }

    if (retraitenEur*0.95 > comptes(id)/10) {
    retraitValide = false 
    println("Votre plfond de retrait autorisé est de : " + (math.floor((comptes(id)/0.95)*10))/100)
      }
    }

    //
    comptes(id) = comptes(id) - retraitenEur*0.95
    comptes(id) = math.floor(comptes(id)*10)/10

    billet100 = (retraitenEur/100).toInt
    billet50 = (retraitenEur%100/50).toInt
    billet20 = (retraitenEur%100%50/20).toInt
    billet10 = (retraitenEur%100%50%20/10).toInt

    if (billet100 != 0)
      println(billet100 + " billet(s) de 100 CHF")

    if (billet50 != 0)
      println(billet50 + " billet(s) de 50 CHF")

    if (billet20 != 0)
      println(billet20 + " billet(s) de 20 CHF")

    if (billet10 != 0)
      println(billet10 + " billet(s) de 10 CHF")
    }

    // retrait


    retraitenChf = 0
    retraitenEur = 0
    retraitDevise = 0
    retraitValide = false 

      println("Votre retrait a été pris en compte, le nouveau montant disponible est de : " + comptes(id))

    // Coupures 
    var coupure = 0


  } // fin def retrait 


def changepin(id: Int, codespin: Array[String]) : Unit = {
  var nouveauPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères") 

  while(nouveauPin.length < 8){
    readLine("Votre code pin ne contient pas au moins 8 caractères")

  codePin(id) = nouveauPin
    if(nouveauPin.length >= 8){
      println("Votre code pin ne contient pas au moins 8 caractères")

  }
  }
  } // fin def changepin


def consultation(id: Int) : Unit = {
  println("Le montant disponible sur vorte compte est de :" + comptes(id))

} // fin def consultation 


def terminer () : Unit = {
println("Fin des opérations, n'oubliez pas de récupérer votre carte.")

} // fin def terminer 


    } // fin def Main

} // fin object Main

