//Assignment: Catherine Decorzant Carrasco_996566_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine

object Main {

  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.00)
    var codePINS = Array.fill(nbclients)("INTRO1234")
    println ("Saisissez votre nombre d'identifiant")
    var ID = readInt()
  
    var CHOIXX = 0 // choix de l'utilisateur
    var pindem = false // variable qui permet de savoir si le client a déjà demandé son code PIN

    var tent_pin = 3 // nombre de tentatives.
    var PIN_correct = false

    while (ID < 0 || ID > nbclients){
      println ("Cet identifiant n'est pas valable")
      ID = readInt()
    }
    var codePIN = codePINS(ID) // code PIN
    var totallm = comptes(ID) // Montant total du compte
    
    def depot(ID:Int, comptes : Array[Double]) : Unit ={ // Depot du compte 
      var deviseDepot = " " // variable qui permet de savoir quelle devise le client veut utiliser
      while (deviseDepot != "CHF" && deviseDepot != "EUR") {
        println("Indiquez la devise du dépôt : CHF ou EUR >")
        deviseDepot = readLine()
        if(deviseDepot != "CHF" && deviseDepot != "EUR" ){
        }
      }

      var tentativesessaie = 0
      var is_correct = false
      var montantDepot = 0;
      while (is_correct == false){ println("Indiquez le montant du dépôt >")
        montantDepot = readLine().toInt
        if(montantDepot % 10 == 0 && montantDepot>0){ is_correct = !false
        }else{ println("Le montant doit être un multiple de 10") }
      }
      // si le client veut déposer en CHF
      if(deviseDepot == "CHF") { 
        totallm += montantDepot
        comptes(ID) = totallm

      }else{ // si le client veut déposer en EUR
        totallm += montantDepot * 0.95
        comptes(ID) = totallm
      }
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(ID))
    }
    
    def retrait(ID : Int, comptes : Array[Double]) : Unit ={ // Retrait du compte 
      var selectionBillet_INT500 = 0
      var deviseRetrait = " " // la devise le client a utiliser
      while (deviseRetrait != "CHF" && deviseRetrait != "EUR") {
        println("Indiquez la devise CHF ou EUR")
        deviseRetrait = readLine()
        if(deviseRetrait != "CHF" && deviseRetrait != "EUR" ){
          println("opération invalide")
        }
      }
      var is_correct = false
      var montant = 0;
      val dixPourcent = totallm * 0.1
      while (!is_correct){
        println("Indiquez le montant du retrait >")
        montant = readInt()
        if(montant % 10 == 0 ){
          if(montant <= dixPourcent && montant>0){
            is_correct = true
          }else{
            println("Votre plafond de retrait autorisé est de :" + dixPourcent)
          }
        }else{
          println("Le montant doit être un multiple de 10")
        }
      }

      var coupure = 3 // variable qui permet de savoir quelle coupure le client veut utiliser
      if(deviseRetrait == "CHF"){ // en CHF
        if(montant >= 200){ // si le client veut retirer plus de 200 CHF
          while (coupure != 1 && coupure != 2) { // tant que le client n'a pas choisi une coupure valide
            println("En, 1) Grosses coupures 2) Petites coupures >")
            coupure = readInt()
            if(coupure != 1 && coupure != 2 ){
              println("choix invalide")
            }
          }
        }
      }
      println("Veuillez retirer la somme demandée :")
      var montant_restant = montant
      // CHF

      if (deviseRetrait == "CHF") { // en CHF
        var Grossecoupure = List(500, 200, 100, 50, 20, 10)
        var Petitecoupure = List(100, 50, 20, 10)
        var choixbillet : List[Int] = List()
        var i = 0
        if (coupure == 1) { // grosse coupure
          while (montant_restant > 0) {
            for (i <- 0 to Grossecoupure.length-1) {
              if (montant_restant >= Grossecoupure(i)) {
                var nbdebillet = montant_restant / Grossecoupure(i)
                println("Il reste " + montant_restant + " CHF")
                println("Vous pouvez obtenir au maximum " + nbdebillet + " billet(s) de " + Grossecoupure(i) + " CHF")
                println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée >")
                var selectionbillet = readLine()

                if (selectionbillet != "o") {
                  var selectionBillet_INT500 = selectionbillet.toInt // alors on la convertit en entier
                  if (selectionBillet_INT500 < 0 || selectionBillet_INT500 > nbdebillet) {
                    println("Veuillez saisir une valeur valide.")
                  }
                  else {
                    montant_restant -= (selectionBillet_INT500 * Grossecoupure(i))
                    choixbillet = choixbillet :+ selectionBillet_INT500
                    selectionbillet = "o"
                  }
                } 
                else {
                  selectionBillet_INT500 = (nbdebillet * Grossecoupure(i)).toInt
                  choixbillet = choixbillet :+ nbdebillet
                  montant_restant -= ((montant_restant/Grossecoupure(i)) * Grossecoupure(i))
                }
              }
            }
          } 
          for (i <- 0 to choixbillet.length-1){
            if (choixbillet(i) > 0) {
              println (s"${choixbillet(i)} billet(s) de ${Grossecoupure(i)} chf")
            }
          }
        }
        if (coupure == 2) { // petite coupure
          while (montant_restant > 0) {
            for (i <- 0 to Petitecoupure.length-1) {
              if (montant_restant >= Petitecoupure(i)) {
                var nbdebillet = montant_restant / Petitecoupure(i)
                println("Il reste " + montant_restant + " CHF")
                println("Vous pouvez obtenir au maximum " + nbdebillet + " billet(s) de " + Petitecoupure(i) + " CHF")
                println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée >")
                var selectionbillet = readLine()

                if (selectionbillet != "o") {
                  var selectionBillet_INT500 = selectionbillet.toInt // alors on la convertit en entier
                  if (selectionBillet_INT500 < 0 || selectionBillet_INT500 > nbdebillet) {
                    println("Veuillez saisir une valeur valide.")
                  }
                  else {
                    montant_restant -= (selectionBillet_INT500 * Petitecoupure(i))
                    choixbillet = choixbillet :+ selectionBillet_INT500
                    selectionbillet = "o"
                  }
                } 
                else {
                  selectionBillet_INT500 = (nbdebillet * Petitecoupure(i)).toInt
                  choixbillet = choixbillet :+ nbdebillet
                  montant_restant -= ((montant_restant/Petitecoupure(i)) * Petitecoupure(i))
                }
              }
            }
          } 
          for (i <- 0 to choixbillet.length-1){
            if (choixbillet(i) > 0) {
              println (s"${choixbillet(i)} billet(s) de ${Petitecoupure(i)} chf")
            }
          }
        }
      }else { // Euro
        var Petitecoupure = List(100, 50, 20, 10)
        var choixbillet : List[Int] = List()
        var i = 0
        while (montant_restant > 0) {
          for (i <- 0 to Petitecoupure.length-1) {
            if (montant_restant >= Petitecoupure(i)) {
              var nbdebillet = montant_restant / Petitecoupure(i)
              println("Il reste " + montant_restant + " eur")
              println("Vous pouvez obtenir au maximum " + nbdebillet + " billet(s) de " + Petitecoupure(i) + " eur")
              println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée >")
              var selectionbillet = readLine()

              if (selectionbillet != "o") {
                var selectionBillet_INT500 = selectionbillet.toInt // alors on la convertit en entier
                if (selectionBillet_INT500 < 0 || selectionBillet_INT500 > nbdebillet) {
                  println("Veuillez saisir une valeur valide.")
                }
                else {
                  montant_restant -= (selectionBillet_INT500 * Petitecoupure(i))
                  choixbillet = choixbillet :+ selectionBillet_INT500
                  selectionbillet = "o"
                }
              } 
              else {
                selectionBillet_INT500 = (nbdebillet * Petitecoupure(i)).toInt
                choixbillet = choixbillet :+ nbdebillet
                montant_restant -= ((montant_restant/Petitecoupure(i)) * Petitecoupure(i))
              }
            }
          }
        } 
        for (i <- 0 to choixbillet.length-1){
          if (choixbillet(i) > 0) {
            println (s"${choixbillet(i)} billet(s) de ${Petitecoupure(i)} eur")
          }
        }
      }
      if (deviseRetrait == 1){
        totallm -= montant
        comptes(ID) = totallm 
      }else{
        totallm -= montant * 0.95
        comptes(ID) = totallm
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est : %.2f CHF\n", comptes(ID))
    }
    def changepin(ID : Int,codePINS : Array[String]) : Unit = {
      println("Saisissez votre noveau code PIN (doit contenir 8 caractères) > ")
      codePINS(ID) = readLine()
      while(codePINS(ID).length < 8){
        println("Saisissez votre noveau code PIN (doit contenir 8 caractères) > ")
        codePINS(ID) = readLine()
      }
      println("Le code PIN a été mis à jour")
    }
    while (CHOIXX != 5 && tent_pin != 0 && ID >= 0 && ID <= 100) {
      if(pindem == false){ // si le client n'a pas encore demandé son code PIN et qu'il ne veut pas quitter
        while (PIN_correct == false && tent_pin > 0) { // tant que le client n'a pas entré le bon code PIN 
          println("Saisissez votre code PIN >") 
          var test = 0
          var tentativesessaie = 0
          var PIN = readLine()
          if (PIN == codePIN) {
          PIN_correct = true 
          pindem = true 
          } else {
            
            tent_pin -= 1 // on enlève une tentative
            if(tent_pin == 0){ // si le client n'a plus de tentative
              println("Pour votre protection, les opérations bancaires vont s'interrompre. récupérez votre carte.")
            }else{
              println("Code pin erroné, il vous reste " + tent_pin + " tentatives >")
            }
          }
        }
      }  
      println("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement de PIN \n5) Terminer")

      //verification
      CHOIXX = readInt()
           
        if(CHOIXX == 1 && PIN_correct == true){ // dépôt d'argent
          depot(ID : Int, comptes : Array[Double])
        }else if(CHOIXX == 2 && PIN_correct){ // retrait de l'argent
          retrait(ID : Int,comptes : Array[Double])
        }else if(CHOIXX == 3 && PIN_correct){ //  consultation du compte
          printf("Le montant disponible sur votre compte est : %.2f", totallm)
          println(" CHF")
        }else if(CHOIXX == 4 && PIN_correct){
          changepin(ID : Int, codePINS : Array[String])
        }else if(CHOIXX == 5){ // terminer

          println("Fin des opérations, n'oubliez pas de récupérer votre carte")
        }
      }
    }
  }
