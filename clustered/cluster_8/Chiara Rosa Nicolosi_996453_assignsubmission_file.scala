//Assignment: Chiara Rosa Nicolosi_996453_assignsubmission_file

import scala.io.StdIn._
import scala.math._

object Main {
 
  //Déclaration de la méthode de dépôt
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    if(ChoixOpérations == 1){
      DeviseDépôt = readLine("Indiquez la devise du dépôt : 1) CHF  ;  2) EUR").toInt  
      do{
        MontantDépôt = readLine("Indiquez le montant du dépôt :").toInt
        if(MontantDépôt%10 !=0){
          println("Le montant doit être un multiple de 10.")
        }
        else if(DeviseDépôt == 2){
          comptes(id) += MontantDépôt*EURtoCHF
          printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
        }
        else if(DeviseDépôt == 1){
          comptes(id) += MontantDépôt
          printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
        }
      }while(MontantDépôt%10 != 0)
    } 
  }
  
  //Déclaration de la méthode de retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    if(ChoixOpérations == 2){
      do{ 
      DeviseRetrait = readLine("Indiquez la devise : 1) CHF ; 2) EUR").toInt
      }while(DeviseRetrait != 1 && DeviseRetrait !=2)
      if(DeviseRetrait == 1){  
        do{
        MontantRetrait = readLine("Indiquez le montant du retrait :").toInt
        if(MontantRetrait%10 != 0){
          println("Le montant doit être un multiple de 10.")
        }
        else if(MontantRetrait > (comptes(id))/10 ){
          println("votre plafond de retrait autorisé est de " + (comptes(id))/10 + " CHF.")
        }
        }while(MontantRetrait > (comptes(id))/10  || MontantRetrait%10 != 0 )
      if(MontantRetrait >= 200){
        do{
          TailleCoupures = readLine("En 1) grosses coupures 2) petites coupures").toInt
        }while(TailleCoupures != 1 && TailleCoupures != 2)
        MontantRestant = MontantRetrait
        while(MontantRestant != 0){

          //Utilisation de la commande : floor ou round pour arrondir vers le bas (ex : floor/round(5,9) = 5)

          //Grosses coupures CHF
          if(TailleCoupures == 1){
            for(i <- 0 to 4){
              if(round(MontantRestant/Billets(i)) > 0){
                choix = readLine("Vous pouvez obtenir au maximum " + floor(MontantRestant/Billets(i)).toInt + " billets de " + Billets(i) + " CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée").toString 
                if(choix == "o"){
                  BilletsCHF(i)=round(MontantRestant/Billets(i))
                  MontantRestant -= BilletsCHF(i) * Billets(i)
                }
                else if(choix.toInt < round(MontantRestant/Billets(i)) && choix.toInt >= 0){
                  BilletsCHF(i)=choix.toInt
                  MontantRestant -= BilletsCHF(i)*Billets(i)
                }
              }
            }
            if(MontantRestant != 0){
              BilletsCHF(5)=round(MontantRestant/10)
            }
            MontantRestant=0
          }

          //Petites coupures CHF
          else if(TailleCoupures == 2){
            for(i <- 2 to 4){
              if(round(MontantRestant/Billets(i)) > 0){
                choix = readLine("Vous pouvez obtenir au maximum " + floor(MontantRestant/Billets(i)).toInt + " billets de " + Billets(i) + " CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée").toString
                if(choix == "o"){
                  BilletsCHF(i)=round(MontantRestant/Billets(i))
                  MontantRestant -= BilletsCHF(i)*Billets(i)
                }
                else if(choix.toInt < round(MontantRestant/Billets(i)) && choix.toInt >= 0){
                  BilletsCHF(i)=choix.toInt
                  MontantRestant -= BilletsCHF(i)*Billets(i)
                }
              }
            }
            if(MontantRestant != 0){
              BilletsCHF(5)=round(MontantRestant/10)
            }
            MontantRestant=0
          }
        }
        //Impression finale des coupures qui apparaissent au moins une fois
        println("Veuillez retirer la somme demandée : \n ")
        for(i <- 0 to 5){
          if(BilletsCHF(i)!=0){
            println(BilletsCHF(i) + " billets de " + Billets(i) + " CHF")
          }
        }
        comptes(id) = (comptes(id)) - MontantRetrait
        printf("Votre retrait a été pris en compte. Votre nouveau montant est de : %.2f \n", comptes(id))
      }
      //Montant retrait < 200
      else{
        MontantRestant = MontantRetrait
        while(MontantRestant != 0){
          for(i <- 2 to 4){
            if(round(MontantRestant/Billets(i)) > 0){
              choix = readLine("Vous pouvez obtenir au maximum " + floor(MontantRestant/Billets(i)).toInt + " billets de " + Billets(i) + " CHF. \nTapez o pour ok ou une autre valeur inférieure à celle proposée").toString
              if(choix == "o"){
                BilletsCHF(i)=round(MontantRestant/Billets(i))
                MontantRestant -= BilletsCHF(i)*Billets(i)
              }
              else if(choix.toInt < round(MontantRestant/Billets(i)) && choix.toInt >= 0){
                BilletsCHF(i)=choix.toInt
                MontantRestant -= BilletsCHF(i)*Billets(i)
              }
            }
          }
          if(MontantRestant != 0){
            BilletsCHF(5)=round(MontantRestant/10)
          }
          MontantRestant=0
        }
      }
      //Impression finale des coupures qui apparaissent au moins une fois
      println("Veuillez retirer la somme demandée : \n ")
      for(i <- 0 to 5){
        if(BilletsCHF(i)!=0){
          println(BilletsCHF(i) + " billets de " + Billets(i) + " CHF")
        }
      }
      comptes(id) = (comptes(id)) - MontantRetrait
      printf("Votre retrait a été pris en compte. Votre nouveau montant est de : %.2f \n", comptes(id))
    }
    //Retrait en EUR
    else if(DeviseRetrait == 2){
      do{
        MontantRetrait = readLine("Indiquez le montant du retrait :").toInt
        if(MontantRetrait%10 != 0){
          println("Le montant doit être un multiple de 10.")
        }
        else if(MontantRetrait*EURtoCHF > (comptes(id)/10)){
          println("votre plafond de retrait autorisé est de " + (comptes(id))/10 + " CHF.")
        }
        //Coupures en EUR
      }while(MontantRetrait%10 != 0 || MontantRetrait*EURtoCHF > (comptes(id))/10)
      if(MontantRetrait > 0){
        MontantRestant=MontantRetrait
        while(MontantRestant != 0){
          for(i <- 0 to 2){
            if(round(MontantRestant/Billets1(i)) > 0){
              choix = readLine("Vous pouvez obtenir au maximum " + floor(MontantRestant/Billets1(i)).toInt + " billet(s) de " + Billets1(i) + " EUR. \nTapez o pour ok ou une autre valeur inférieure à celle proposée").toString
              if(choix == "o"){
                BilletsEUR(i)=round(MontantRestant/Billets1(i))
                MontantRestant -= BilletsEUR(i)*Billets1(i)
              }
              else if(choix.toInt < round(MontantRestant/Billets1(i)) && choix.toInt >= 0){
                BilletsEUR(i)=choix.toInt
                MontantRestant -= BilletsEUR(i)*Billets1(i)
              }
            }
          }
          if(MontantRestant != 0){
            BilletsEUR(3)=round(MontantRestant/10)
          }
          MontantRestant=0
        }
      }
      //Impression finales des coupures qui apparaissent au moins une fois
      println("Veuillez retirer la somme demandée : \n ")
      for(i <- 0 to 3){
        if(BilletsEUR(i)!=0){ 
          println(BilletsEUR(i) + " billets de " + Billets1(i) + " EUR")
        }
      }
      comptes(id) = (comptes(id))- MontantRetrait*EURtoCHF
      printf("Votre retrait a été pris en compte. Votre nouveau montant est de : %.2f \n", comptes(id))
    }

  }
}
  //Déclaration de la méthode changement de code pin
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    if (ChoixOpérations == 4){
      while(nouveaucode.length < 8){ 
      nouveaucode = readLine("Saisissez votre nouveau code pin (il doit contenir au moin 8 caractères) > ").toString
        if(nouveaucode.length < 8){
        println("Votre code pin ne contient pas au moins 8 caractères")
        }
        else if(nouveaucode.length >= 8){
          codespin(id) = nouveaucode
          println("Votre code pin a été modifié avec succès")
        }
      }
    }
  }

// les variables à déclarer
  val CHFtoEUR = 1.05
  val EURtoCHF = 0.95

  var CodePinSaisi = ""
  var TentativesCodePin = 0
  var nouveaucode = ""
  var ChoixOpérations = 0
  var DeviseDépôt = 0 
  var MontantDépôt = 1
  var DeviseRetrait = 0
  var MontantRetrait = 1
  var TailleCoupures = 0
  var MontantRestant = 1
  var choix = ""
  var BilletsCHF = Array(0,0,0,0,0,0)
  var Billets = Array(500,200,100,50,20,10)
  var Billets1 = Array(100,50,20,10)
  var BilletsEUR = Array(0,0,0,0)

  var Solde = 0
  var id = 0

  var comptes = new Array[Double](100)
  var codespin = new Array[String](100)
  var nbclients = 100
  CodePinSaisi = " "
  for(i <- 0 to 99){
    comptes(i) = 1200.0
    codespin(i) = "INTRO1234"
  }

  def main(args: Array[String]): Unit = {

   //saisi de l'identifiant et du code pin
  while (TentativesCodePin < 3){
    do{
    TentativesCodePin = 0
    
    id = readLine("Saisissez votre code identifiant > ").toInt
    if(id <= 99 && id > 0){
      do{
        CodePinSaisi = readLine("Saisissez votre code pin > ").toString
        
        //Si code pin est correct
        if(CodePinSaisi == codespin(id)){
          println ("Choisissez votre opération :")
          println ("1) Dépôt")
          println ("2) Retrait")
          println ("3) Consultation du compte")
          println ("4) Changement du code pin")
          println ("5) Terminer")
          ChoixOpérations = readLine("Quelle opération choisissez-vous ?").toInt 
          println ("Votre choix est : " + ChoixOpérations)
          
          // Opération de dépôt
          if(ChoixOpérations == 1){
            depot(id, comptes)
          }
          
          //Opération de retrait
          if(ChoixOpérations == 2){
            retrait(id, comptes)
          }
          
          //Opération de consultation
          if(ChoixOpérations == 3){
            printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
          } 

          //Opération de changement de code pin
          if(ChoixOpérations == 4){
            changepin(id, codespin)
          }          
          //Opération : terminer
          if(ChoixOpérations == 5){
           println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          }
        }
        //Si code pin est faux  
        else{
          TentativesCodePin += 1
          if(TentativesCodePin == 3){
            println("Trop d'erreurs, abandon de l'identification.")
          }
          else{
            println("Code pin erroné, il vous reste " + (3 - TentativesCodePin) + " tentatives.")
          }
        } 
      }while (CodePinSaisi != codespin(id) && TentativesCodePin < 3)
        
        //Si code pin est faux  
    }
    else{
      println("Cet identifiant n’est pas valable.")
    }
  }while(id <= 99 && id > 0 && CodePinSaisi != codespin(id))
  }
 }
}
