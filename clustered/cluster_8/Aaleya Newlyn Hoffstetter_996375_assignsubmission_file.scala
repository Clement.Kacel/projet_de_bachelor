//Assignment: Aaleya Newlyn Hoffstetter_996375_assignsubmission_file

import scala.io.StdIn._
import scala.collection.mutable.ArrayBuffer

object Main {
  val nbclients = 100
  val EURenCH = 0.95
  val CHenEUR = 1.05
  var comptes = ArrayBuffer.fill[Double](nbclients + 1)(1200.0)
  val codespin = Array.fill[String](nbclients + 1)("INTRO1234")

  def main(args: Array[String]): Unit = {
    while (true) {
      println("Saisissez votre code identifiant > ")
      val id = readInt()

      if (id < 1 || id > nbclients) {
        println("Cet identifiant n'est pas valable.")
        sys.exit()
      } else {
        var tentatives = 0
        var pinCorrect = false

        while (tentatives < 3 && !pinCorrect) {
          println("Saisissez votre code pin > ")
          val codeSaisi = readLine()

          if (codeSaisi == codespin(id - 1)) {
            pinCorrect = true
          } else {
            tentatives += 1
            if (tentatives < 3){
               println(s"Code pin erroné, il vous reste ${3 - tentatives} tentatives >") 
            }
          }
        }

        if (pinCorrect) {
          afficherMenu(id)
        } else {
          println("Trop d'erreurs, abandon de l'identification")
        }
      }
    }
  }

  def afficherMenu(id: Int): Unit = {
    var continuerMenu = true

    while (continuerMenu) {
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")

      println("Votre choix :")
      val choix = readInt()

      if (choix == 1) {
        depot(id, comptes)
      } else if (choix == 2) {
        retrait(id, comptes)
      } else if (choix == 3) {
        consultationCompte(id, comptes(id))
      } else if (choix == 4) {
        changepin(id: Int, codespin)
      } else if (choix == 5) {
        println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
        continuerMenu = false
      } else {
        println("Choix non valide. Veuillez choisir une option valide.")
      }
    }
  }
  def depot(id: Int , comptes:ArrayBuffer[Double]): Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR")
    val deviseDepot = readInt()

    println("Indiquez le montant du dépôt > ")
    var montantDepot = readInt()

    while (montantDepot % 10 != 0) {
      println("Le montant doit être un multiple de 10.")
      println("Indiquez le montant du dépôt > ")
      montantDepot = readInt()
    }

    if (deviseDepot == 2) {
      montantDepot = (montantDepot * EURenCH).toInt
    }

    comptes.update(id, comptes(id) + montantDepot)
    
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

  def retrait(id: Int, comptes : ArrayBuffer[Double]): Unit = {
    
    var deviseRetrait = 0
    
     println("Indiquez la devise :1 CHF, 2 : EUR > ")
        deviseRetrait = readInt()

    
    
    while (deviseRetrait != 1 && deviseRetrait != 2) {
        println("Indiquez la devise : 1 CHF, 2 : EUR")
        deviseRetrait = readInt()
    }


      println("Indiquez le montant du retrait > ")
      var montantRetrait = readInt()

      while (montantRetrait % 10 != 0 || montantRetrait > (0.1 * comptes(id)).toInt) {
        if (montantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        }
        if (montantRetrait > (0.1 * comptes(id)).toInt) {
          println("Votre plafond de retrait autorisé est de " + (0.1 * comptes(id)).toDouble)
        } 
       
        println("Indiquez le montant du retrait > ")
        montantRetrait = readInt()
      }

      if (deviseRetrait == 1 && montantRetrait >= 200) {
        println("En 1) grosses coupures, 2) petites coupures > ")
        var choixCoupure = readInt()
      

        while (choixCoupure != 1 && choixCoupure != 2) {
          println("En 1) grosses coupures, 2) petites coupures > ")
          choixCoupure = readInt()
        }

        val coupuresPossibles: Array[Int] =
          if (choixCoupure == 1) {
            Array(500, 200, 100, 50, 20, 10)
          } else {
            Array(100, 50, 20, 10)
          }

        var montantRestant = montantRetrait
        var i = 0

        var choixAccepte = ArrayBuffer[String]()

      while (montantRestant > 0 && i < coupuresPossibles.length) {
        val coupure = coupuresPossibles(i)
        var nbCoupures = montantRestant / coupure

        if (coupure == 10 && nbCoupures > 0) {
          montantRestant -= coupure * nbCoupures
          choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure CHF\n"
          println(s"Vous avez obtenu ${nbCoupures.toInt} billet(s) de $coupure CHF.")
          
        } else if (nbCoupures > 0) {
      println("Il reste " + montantRestant + " CHF à distribuer")
      println(s"Vous pouvez obtenir au maximum $nbCoupures billet(s) de $coupure CHF")
      print("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          
          val input = readLine()

          if (input == "o") {
            montantRestant -= coupure * nbCoupures
            if (nbCoupures > 0) {
              choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure CHF\n"
            }
          } else if (input.nonEmpty && input.forall(_.isDigit)) {
            var choixUtilisateur = input.toInt
            
            if (choixUtilisateur >= 0 && choixUtilisateur <= nbCoupures) {
              nbCoupures = choixUtilisateur
              montantRestant -= coupure * nbCoupures
              if (choixUtilisateur  > 0) {
                choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure CHF\n"
              }
            } 
          }
        }

        i += 1
      }

        var montantRetireFinal: Double = (montantRetrait - montantRestant).toDouble

        comptes(id) -= montantRetireFinal

        println("Veuillez retirer la somme demandée : ")
        for (choix <- choixAccepte) {
          println(choix)
        }

        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))

      }

      if (deviseRetrait == 1 && montantRetrait < 200) {
        val coupuresPossibles: Array[Int] = Array(100, 50, 20, 10)

        var montantRestant = montantRetrait
        var i = 0

        var choixAccepte = ArrayBuffer[String]()

        while (montantRestant > 0 && i < coupuresPossibles.length) {
          val coupure = coupuresPossibles(i)
          var nbCoupures = montantRestant / coupure

          if (coupure == 10 && nbCoupures > 0) {
            montantRestant -= coupure * nbCoupures
            choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure CHF\n"
            println(s"Vous avez obtenu ${nbCoupures.toInt} billet(s) de $coupure CHF.")
          }
            else if (nbCoupures > 0) {
            println("Il reste " + montantRestant + " CHF à distribuer")

            println("Vous pouvez obtenir au maximum " + nbCoupures + " billet(s) de " + coupure + " CHF")
            print("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")

            val input = readLine()

            if (input == "o") {
              montantRestant -= coupure * nbCoupures
              if (nbCoupures > 0) {
                choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure CHF\n"
              }
            } else if (input.nonEmpty && input.forall(_.isDigit)) {
              var choixUtilisateur = input.toInt
              if (choixUtilisateur >= 0 && choixUtilisateur <= nbCoupures) {
                nbCoupures = choixUtilisateur
                montantRestant -= coupure * nbCoupures
                if (nbCoupures > 0) {
                  choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure CHF\n"
                } 
              }
            }
          }

          i += 1
        }

        var montantRetire: Double = (montantRetrait - montantRestant).toDouble

        comptes(id) -= montantRetire
        
        println("Veuillez retirer la somme demandée : \n")
        for (choix <- choixAccepte) {
          println(choix)
        }
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        
      }

        if (deviseRetrait == 2) {
          val coupuresPossiblesEUR: Array[Int] = Array(100, 50, 20, 10)

          var montantRestant = montantRetrait

          var i = 0

          var choixAccepte = ArrayBuffer[String]()

          while (montantRestant > 0 && i < coupuresPossiblesEUR.length) {
            val coupure = coupuresPossiblesEUR(i)
            var nbCoupures = montantRestant / coupure

            if (coupure == 10 && nbCoupures > 0) {
              montantRestant -= coupure * nbCoupures
              choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure EUR\n"
              println(s"Vous avez obtenu ${nbCoupures.toInt} billet(s) de $coupure EUR.")
            }
            else if (nbCoupures > 0) {
              println("Il reste " + montantRestant + " CHF à distribuer")

              println("Vous pouvez obtenir au maximum " + nbCoupures + " billet(s) de " + coupure + " EUR")
              print("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")

              val input = readLine()

              if (input == "o") {
                montantRestant -= coupure * nbCoupures
                if (nbCoupures > 0) {
                  choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure EUR\n"
                }
              } else if (input.nonEmpty && input.forall(_.isDigit)) {
                var choixUtilisateur = input.toInt
                if (choixUtilisateur >= 0 && choixUtilisateur <= nbCoupures) {
                  nbCoupures = choixUtilisateur
                  montantRestant -= coupure * nbCoupures
                  if (nbCoupures > 0) {
                    choixAccepte += s"${nbCoupures.toInt} billet(s) $coupure EUR\n"
                  } 
                }
              }
            }

            i += 1
          }

          var montantRetireCHF: Double = montantRetrait * EURenCH

           comptes(id) -= montantRetireCHF

          println("Veuillez retirer la somme demandée : \n")
          for (choix <- choixAccepte) {
            println(choix)
            
          }

          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))

        }
    
  }

    def consultationCompte(id: Int, comptes: Double): Unit = {
   printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes) 
  }
    
  def changepin(id: Int, codespin : Array[String]): Unit = {
    var newPin = ""

    while (newPin.length < 8) {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      newPin = readLine()

      if (newPin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      }
    }

    codespin(id) = newPin                               
    println("Code pin changé avec succès.")
  }
}