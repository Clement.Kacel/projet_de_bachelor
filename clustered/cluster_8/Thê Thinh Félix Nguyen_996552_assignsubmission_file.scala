//Assignment: Thê Thinh Félix Nguyen_996552_assignsubmission_file

// Si le code ne se compile pas, veillez à vérifier que le fichier est bien renommé " main.scala "

object Main {
// Déclaration des variables globales du code
  var solde: Double = 1200.0
  var nboperations: Int = 0
  var operation: Int = 0
  var etat: Boolean = true

  var nbclients: Int = 100
  // Initialisation du tableau des comptes avec un solde de 1200.0 CHF et le MDP pour tous les clients
  // "Array.fill(nbclients)(new Compte(1200.0, INTRO1234))" remplit le tableau avec "nbclients", instances de la classe "Compte" avec un solde initial de 1200.0 CHF et un code PIN initial "INTRO1234"
  var comptes: Array[Compte] = Array.fill(nbclients)(new Compte(1200.0, "INTRO1234"))

  var idClient: Int = -1

  def main(args: Array[String]): Unit = {

    while (etat == true)
    {
      nboperations = 0
      operation = 0
      etat = true

      // Fonction Menu Terminal
      Menu()
      println("Choisissez votre opération (1/2/3/4/5) : ")
      val choix = scala.io.StdIn.readLine()
      
      // "choix match" sert à exécuter le code correspondant au motif correspondant pour différentes "case"
      // "case..." est la valeur que vous attendez pour "choix match" et ainsi exécuter une fonction ou un fragment de code
      // Pour chaque option : vérification si "idClient" est égal à -1. Si c'est le cas, alors l'utilisateur n'est pas encore identifié et doit entrer son MDP
      choix match {
        case "1" =>
          if (idClient == -1)
          {
            idClient = CodePIN()
          }
          Depot()

        case "2" =>
          if (idClient == -1)
          {
            idClient = CodePIN()
          }
          Retrait()

        case "3" =>
          if (idClient == -1)
          {
            idClient = CodePIN()
          }
          ConsulterCompte()

        case "4" =>
          if (idClient == -1)
          {
            idClient = CodePIN()
          }
          ChangeCodePin()

        case "5" =>
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          idClient = -1
      }
    }
  }

  // Déclaration de la classe Compte pour manipuler les comptes lors des opérations bancaires
  class Compte(var solde: Double, var codePin: String)

  // Fonction CodePIN
  // Demande à l'utilisateur de choisir son numéro de compte.
  def CodePIN(): Int = {
    println(s"Choisissez votre compte (de 1 à $nbclients) : ")
    val idClientInput = scala.io.StdIn.readInt()
// Vérifie si le numéro de compte est valide
    if (idClientInput <= 0 || idClientInput > nbclients)
    {
      println("Cet identifiant n'est pas valable.")
      System.exit(0)
      // Quitte le programme si le numéro du compte<0 ou >nombre de clients
    }
    
// Compte associé au numéro de compte saisi et stock son code PIN
    val compteSelectionne = comptes(idClientInput - 1)
    var codetmp = scala.io.StdIn.readLine("Veuillez entrer votre code PIN : ")
// Comparaison du code PIN entré avec celui du compte et limite de 3 fausses tentatives
    val codepin = compteSelectionne.codePin
    var tentatives = 3

    while ((codepin != codetmp) && (nboperations == 0) && (operation != 4) && (etat == true) && tentatives > 0)
    {
      if (codepin != codetmp) tentatives -= 1

      if (tentatives == 0)
      {
        println("Trop d’erreurs, abandon de l’identification")
        operation = 0
        etat = false
        System.exit(0)
        //Quitte le programme si le code PIN est incorrect 3 fois
      }

      println(f"\nIl vous reste $tentatives tentative(s).")
      codetmp = scala.io.StdIn.readLine("Veuillez entrer votre code PIN : ")
    }
//Si code PIN est correct, la fonction renvoie le numéro, l'identifiant du client saisi
    idClientInput
  }


  // Fonction Menu
  def Menu(): Unit = {
    println("Menu des opérations :")
    println("1) Dépôt")
    println("2) Retrait")
    println("3) Consultation du compte")
    println("4) Changement du code PIN")
    println("5) Terminer")
  }


  // Fonction Dépôt
  def Depot(): Unit = {
    println("Indiquez la devise du dépôt (1) CHF 2) EUR) : ")
    var Devise = scala.io.StdIn.readInt()

    while (Devise != 1 && Devise != 2)
    {
      println("Choix de devise invalide.")
      Devise = scala.io.StdIn.readInt()
    }

    println("Indiquez la somme du dépôt :")
    var montantDepot = scala.io.StdIn.readInt()

    // Boucle pour assurer que le montant est un multiple de 10
    while (montantDepot % 10 != 0)
    {
      println("Le montant doit être un multiple de 10.")
      println("Indiquez la somme du dépôt :")
      montantDepot = scala.io.StdIn.readInt()
    }
    if (Devise == 2)
    {
      montantDepot = (montantDepot * 0.95).toInt
    }
    
    // Stock le montant du dépôt dans 
    val compteSelectionne = comptes(idClient - 1)
    // Récupère le compte sélectionné en utilisant l'idClient pour accéder au numéro correspondant dans le tableau des comptes
    compteSelectionne.solde += montantDepot
    // on ajoute ensuite le montant du dépôt au solde du compte sélectionné

    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : CHF %.2f%n", compteSelectionne.solde)
  }


  // Fonction Consulter Compte
  def ConsulterCompte(): Unit = {
    val compteSelectionne = comptes(idClient - 1)
    printf("Le montant disponible sur votre compte est de : CHF %.2f%n", compteSelectionne.solde)
  }


  // Fonction Changement du CodePIN
  def ChangeCodePin(): Unit = {
    val compteSelectionne = comptes(idClient - 1)
    println(s"Veuillez entrer votre nouveau code PIN pour le compte $idClient (au moins 8 caractères) : ")
    var nouveauCodePIN = ""
  // Boucle pour demander à l'utilisateur de saisir un nouveau code PIN tant que sa longueur < 8 caractères
    do {
      nouveauCodePIN = scala.io.StdIn.readLine()
        if (nouveauCodePIN.length < 8)
        {
        println("Le nouveau code PIN doit avoir au moins 8 caractères.")
        }
      }
    while (nouveauCodePIN.length < 8)

    compteSelectionne.codePin = nouveauCodePIN
  // Stock le nouveau code PIN dans le compte sélectionné
    println("Code PIN mis à jour avec succès.")
  }


  // Fonction Retrait
  def Retrait(): Unit = {
    
    var devise: Int = 0
    var taillecoupures = Array(100, 50, 20, 10)
    var nbBillets = Array(0, 0, 0, 0, 0, 0)
    var montantRetrait = 0
    var montantRestant = 0
    var montantRendu = 0
    var compteurBillet = 0
    var choix = ""
    var choixEntier = 0
    var maxRetrait = solde * 0.1
    //La boulce do-while s'exécute tant que l'utilisateur entre un chiffre différent de 1 ou 2 pour la devise
    do {
        println("Indiquez la devise (1) CHF, 2) EUR) :")
        devise = scala.io.StdIn.readInt()
      }
    while (devise != 1 && devise != 2)
    
    if (devise == 2) maxRetrait *= 1.10
    
    println("Indiquez le montant du retrait :")
    montantRetrait = scala.io.StdIn.readInt()
    
    //Tant que le reste de la division entière de montantRetrait par 10 n'est pas égal à zéro ou que le montant du retrait et strict. supér. au maximum du retrait alors la boucle continue de s'exécuter
    while (montantRetrait % 10 != 0 || montantRetrait > maxRetrait)
    {
      if (montantRetrait % 10 != 0)
      {
        println("Le montant doit être un multiple de 10: ")
        montantRetrait = scala.io.StdIn.readInt()
      }
      if (montantRetrait > maxRetrait)
      {
        println(s"Votre plafond de retrait autorisé est de : $maxRetrait ")
        montantRetrait = scala.io.StdIn.readInt()
      }
    }

    val compteSelectionne = comptes(idClient - 1)
    montantRestant = montantRetrait

    // Partie Coupure
    var Coupures: Int = 2
    if (devise == 1)
    {
      if (montantRetrait >= 200)
      {
        Coupures = scala.io.StdIn.readLine("En 1) grosses coupures 2) petites coupures ").toInt
        if (Coupures == 1)
        {
          println("Vous avez choisi les grosses coupures. ")
          taillecoupures = Array(500, 200, 100, 50, 20, 10)
        }
      }
    }
    // Permet d'accéder aux éléments du tableau "taillecoupures" en les parcourant un à un successivement
    for (i <- 0 until taillecoupures.length)
    {
      if ((montantRestant / taillecoupures(i)) >= 1)
      {
        compteurBillet = (montantRestant / taillecoupures(i)).toInt
        println(s"\nIl reste $montantRestant à distribuer ")
        println(s"Vous pouvez obtenir au maximum $compteurBillet billet(s) de " + taillecoupures(i))
        choix = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée: ")

        if (choix == "o")
        {
          montantRendu = (taillecoupures(i) * compteurBillet)
          montantRestant -= montantRendu
        }
        else
        {
          choixEntier = choix.toInt
          if (choixEntier < compteurBillet)
          {
            compteurBillet = choixEntier
            montantRendu = (taillecoupures(i) * compteurBillet)
            montantRestant -= montantRendu
          }
        }
        if (Coupures == 1)
          nbBillets(i) = compteurBillet
        else nbBillets(i + 2) = compteurBillet
      }
    }

    if (Coupures == 1)
    {
      for (j <- 0 until nbBillets.length)
      {
        if (nbBillets(j) > 0)
        {
          println(f"${nbBillets(j)} billet(s) de ${taillecoupures(j)}")
          //${nbBillets(j)}" et "${taillecoupures(j)}" permet d'insérer les valeurs d'expression de "nbBillets(j)" et "taillecoupures(j)" dans le print
        }
      }
    }
    else
    {
      for (j <- 2 until nbBillets.length)
      {
        if (nbBillets(j) > 0)
        {
          println(f"${nbBillets(j)} billet(s) de ${taillecoupures(j - 2)}")
        }
      }
    }

    val tauxConversion = if (devise == 2) 0.95 else 1.0
    compteSelectionne.solde -= (montantRetrait * tauxConversion)
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de CHF: %.2f%n", compteSelectionne.solde)
  }
}
