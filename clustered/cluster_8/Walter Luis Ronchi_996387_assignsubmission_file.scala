//Assignment: Walter Luis Ronchi_996387_assignsubmission_file

import scala.io.StdIn._
import scala.math._
object Main {
// methodes depot
       def depot(id : Int, comptes:Array[Double]) : Unit = {
          var devise = 0 
          var montantConverti = 0.0
         var montantD = 0
         println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
           devise = readInt()
         while (!((devise == 1 ) || (devise == 2 ))){
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
         devise = readInt()
          } 
        println ("Indiquez le montant du dépôt >")
          montantD = readInt()
        while (comptes(id) % 10 !=0){
          println ("Indiquez le montant du dépôt >")
          montantD = readInt()
        }
        if (!(devise == 1)){
          montantConverti =  0.95 * montantD
        } else {
          montantConverti = montantD
           }    
       comptes(id) = comptes(id) + montantConverti
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " +comptes(id)+ " CHF")
       }
 
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    var devise = 0  
    var choixCoupure = 0
    var montantConverti = 0.0
    var montantR = 0
    var EURversCHF = 0.95
    var billet500 :Int = 0
    var billet200 : Int = 0
    var billet100 : Int = 0
    var billet50 : Int = 0
    var billet20 : Int = 0
    var billet10 : Int = 0
       println("Indiquez la devise :1 CHF, 2 : EUR >")
      devise = readInt()
      
      while (!((devise == 1 ) || (devise == 2 ))){
       println(" Indiquez la devise :1 CHF, 2 : EUR >")
         devise = readInt()
          } 
      println("Indiquez le montant du retrait >")
      montantR = readInt()
       
   if (devise == 1) { 
var montantautorisé = comptes(id) / 10
        while ((montantR % 10 !=0) || (montantR > montantautorisé)){
          if  (montantR % 10 !=0) {
          println ("Le montant doit être un multiple de 10")
          } else if (montantR > montantautorisé){
          println (s"Votre plafond de retrait autorisé est de $montantautorisé ")  
          }
          montantR = readInt()
        }
     comptes(id) -= montantR
     if(montantR >= 200){
   println ("En 1) grosses coupures, 2) petites coupures >")
      var choixCoupure = readInt()    
        while (!((choixCoupure == 1) || (choixCoupure == 2))){
          println ("En 1) grosses coupures, 2) petites coupures >")
          choixCoupure= readInt()
        }    
     }
     println ("Veuillez retirer la somme demandée :") 
     if (choixCoupure == 1) {
        if (montantR >= 500) {
          billet500 = montantR/500 
          montantR %= 500   
        println(s"$billet500 billet(s) de 500 CHF")
        }
       if (montantR >= 200) {
          billet200 = montantR/200 
          montantR %= 200   
        println(s"$billet200 billet(s) de 200 CHF")
        }
       if (montantR >= 100) {
          billet100 = montantR/100
          montantR %= 100   
        println(s"$billet100 billet(s) de 100 CHF")
        }
       if (montantR >= 50) {
          billet50 = montantR/50
          montantR %= 50   
        println(s"$billet50 billet(s) de 50 CHF")
        }
       if (montantR >= 20) {
          billet20 = montantR/20
          montantR %= 20  
        println(s"$billet20 billet(s) de 20 CHF")
        }
       if (montantR >= 10) {
          billet10 = montantR/10
          montantR %= 10  
        println(s"$billet10 billet(s) de 10 CHF")
        }
       println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est >" + comptes(id))
     } 
     else {
        if (montantR >= 100) {
          billet100 = montantR/100
          montantR %= 100   
        println(s"$billet100 billet(s) de 100 CHF")
        }
       if (montantR >= 50) {
          billet50 = montantR/50
          montantR %= 50   
        println(s"$billet50 billet(s) de 50 CHF")
        }
       if (montantR >= 20) {
          billet20 = montantR/20
          montantR %= 20  
        println(s"$billet20 billet(s) de 20 CHF")
        }
       if (montantR >= 10) {
          billet10 = montantR/10
          montantR %= 10  
        println(s"$billet10 billet(s) de 10 CHF")
        }
       println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id))
      }  
   }       
        if (devise == 2 ) {
          var montantautorisé = (comptes(id) / 10) / EURversCHF
           montantautorisé = math.floor(montantautorisé * 100)/100 
        while ((montantR % 10 !=0) || (montantR > montantautorisé)){
          if  (montantR % 10 !=0) {
          println ("Le montant doit être un multiple de 10")
          } else if (montantR > montantautorisé){
          println (s"Votre plafond de retrait autorisé est de $montantautorisé ")  
          }
          montantR = readInt()
        }        
          comptes(id) -= montantR*EURversCHF  
        if (montantR >= 100) {
          billet100 = montantR/100
          montantR %= 100   
        println(s"$billet100 billet(s) de 100 EUR")
        }
       if (montantR >= 50) {
          billet50 = montantR/50
          montantR %= 50   
        println(s"$billet50 billet(s) de 50 EUR")
        }
       if (montantR >= 20) {
          billet20 = montantR/20
          montantR %= 20  
        println(s"$billet20 billet(s) de 20 EUR")
        }
       if (montantR >= 10) {
          billet10 = montantR/10
          montantR %= 10  
        println(s"$billet10 billet(s) de 10 EUR")
        }
       println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id))          
   }
  }
 def changepin(id : Int, codespin : Array[String]) : Unit = {
   var newcodepin = ""
   println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
        newcodepin = readLine()
        while (newcodepin.size < 8 ){
          println ("Votre code pin ne contient pas au moins 8 caractères ")
          newcodepin = readLine()
           }
 codespin(id) = newcodepin
  }
  def main(args: Array[String]): Unit = {
  //VARIABLES 
    var nbtentatives = 0
    var tentatives = 3
    var nbclients = 100
    var codepin = ""
    var codespin = Array.fill[String](nbclients)("INTRO1234")
    var comptes = Array.fill(nbclients)(1200.0)    
    var id = readLine("Saisissez votre code identifiant > ").toInt
    if (id > nbclients){
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
    }
     println("Saissisez votre code pin")
     nbtentatives = 0
     tentatives = 3
      while (nbtentatives <= 2){ 
        codepin = readLine()
        if (codepin == codespin(id)) {
         nbtentatives = nbtentatives + 3
        } else {
          nbtentatives += 1
         tentatives -= 1
         println (s"Code pin erroné, il vous reste $tentatives tentatives")
       }  
     } 
       while (!(codepin == codespin(id))) {
          println ("Trop d’erreurs, abandon de l’identification")
         var id = readLine("Saisissez votre code identifiant > ").toInt
    if (id > nbclients){
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
       }
         println("Saissisez votre code pin")
          nbtentatives = 0
     tentatives = 3
      while (nbtentatives <= 2){
        codepin = readLine()
        if (codepin == codespin(id)) {
          // initialiser le nb de tentatives dans la boucle ainsi que les tentatives 
         nbtentatives = nbtentatives + 3
        } else {
          nbtentatives += 1
         tentatives -= 1
         println (s"Code pin erroné, il vous reste $tentatives tentatives")
       }  
     } 
    }
    println("Choisissez votre opération :")
    println("1-Depot")
    println("2-Retrait")
    println("3-Consulation du compte")
    println("4-Changement du code pin")
    println("5-Terminer")
    println("Votre choix:")
    var choix = readInt()

     // selection 1ère opération
  while (true) { 
    if ( choix == 5 ) {
      println ( " Fin des opérations, n’oubliez pas de récupérer votre carte. ")
 var id = readLine("Saisissez votre code identifiant > ").toInt
    if (id > nbclients){
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
    }
     println("Saissisez votre code pin")
       nbtentatives = 0
     tentatives = 3
      while (nbtentatives <= 2){ 
        codepin = readLine()
        if (codepin == codespin(id)) {
         nbtentatives = nbtentatives + 3
        } else {
          nbtentatives += 1
         tentatives -= 1
         println ("Code pin erroné, il vous reste $tentatives tentatives")
       }  
     } 
       while (!(codepin == codespin(id))) {
          println ("Trop d’erreurs, abandon de l’identification")
         var id = readLine("Saisissez votre code identifiant > ").toInt
    if (id > nbclients){
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
       }
         println("Saissisez votre code pin")
          nbtentatives = 0
          tentatives = 3
      while (nbtentatives <= 2){ 
        codepin = readLine()
        if (codepin == codespin(id)) {
          // initialiser le nb de tentatives dans la boucle ainsi que tentatives POUR eviter de faire plusieurs variables 
         nbtentatives = nbtentatives + 3
        } else {
          nbtentatives += 1
         tentatives -= 1
         println ("Code pin erroné, il vous reste $tentatives tentatives")
       }  
     } 
    }
    println("Choisissez votre opération :")
    println("1-Depot")
    println("2-Retrait")
    println("3-Consulation du compte")
    println("4-Changement du code pin")
    println("5-Terminer")
    println("Votre choix:")
    choix = readInt()
  }    
      if (choix == 4) {
        changepin(id,codespin)         
   println("Choisissez votre opération :")
    println("1-Depot")
    println("2-Retrait")
    println("3-Consulation du compte")
    println("4-Changement du code pin")
    println("5-Terminer")
    println("Votre choix:")
    choix = readInt()
        }
    if (choix == 1){
    depot(id,comptes)  
    println("Choisissez votre opération :")
    println("1-Depot")
    println("2-Retrait")
    println("3-Consulation du compte")
    println("4-Changement du code pin")
    println("5-Terminer")
    println("Votre choix:")
   choix = readInt()
   }   
 if (choix == 2){
   retrait(id,comptes)  
    println("Choisissez votre opération :")
    println("1-Depot")
    println("2-Retrait")
    println("3-Consulation du compte")
    println("4-Changement du code pin")
    println("5-Terminer")
    println("Votre choix:")
   choix = readInt()
 }
if (choix == 3) {
 println("Le montant disponible sur votre compte est de :" + comptes(id))
    println("Choisissez votre opération :")
    println("1-Depot")
    println("2-Retrait")
    println("3-Consulation du compte")
    println("4-Changement du code pin")
    println("5-Terminer")
    println("Votre choix:")
    choix = readInt()
}   
}
}
}