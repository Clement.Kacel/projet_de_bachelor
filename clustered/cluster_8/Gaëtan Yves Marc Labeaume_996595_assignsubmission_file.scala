//Assignment: Gaëtan Yves Marc Labeaume_996595_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine

 object Main {


 val maxTentativePin : Int = 3
 val TAUX_EUR_CHF : Double = 0.95
 val nbclients : Int = 100
 

 var bonPin : Boolean = false
 var id : Int = 0
 val comptes : Array[Double] = Array.fill(nbclients)(1200.0)
 val codespin : Array[String] = Array.fill(nbclients)("INTRO1234")




   def main(args: Array[String]): Unit = {
    operationBoucle1() 
   }






  
   def selectionOperation : Int = {
    println("Choisissez votre opération :")
    println("    1) Dépôt")
    println("    2) Retrait")
    println("    3) Consultation du compte")
    println("    4) Changement du code pin")
    println("    5) Terminer")
    println("Votre choix :")
    readInt()
   }

   def pinVerification : Boolean = {

    def verif(essaiNumber : Int) : Boolean ={

        if (essaiNumber == 0)  {
            val pin = readLine("Saissisez votre code PIN > \n ")
            if (pin == codespin(id)) {bonPin = true; true} else {verif(1)}
        }
        else {
          if (essaiNumber < 3)  {
            val pin = readLine("Code pin erroné, il vous reste "+ (3 - essaiNumber) +" tentatives > \n ")
            if (pin == codespin(id))  {bonPin = true; true} else {verif(essaiNumber + 1)}
          }
          else {
            println("Trop d’erreurs, abandon de l’identification")
            false
          }
        }
      }
    verif(0) 
   }

   def operationBoucle1() : Unit = {
    id = selectionId
    if (id == -1)  
      {}
    else {
      val pin = pinVerification
      if (!pin)  {
        operationBoucle1()
      }
      else {
        operationBoucle2()
      }
    }        
   }

   def operationBoucle2() : Unit = {
    val operation = selectionOperation
          if (operation == 5)  {
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            operationBoucle1()
          }
          else {
            effectuerOperation(operation)
            operationBoucle2()
          }
   }

   def selectionId : Int = {
    val id = readLine("Saisissez votre code identifiant >").toInt
    if (id < 0 || id >= nbclients)  {
      println("Cet identifiant n’est pas valable.")
      -1
    }  
    else {id}
   }

   def effectuerOperation(n : Int) : Unit = {
    if (n == 1)  {
      depot(id, comptes)
    }
    else {
      if (n == 2)  {
        retrait(id, comptes)
      }
      else {
        if (n == 3)  {
          consultationCompte(id, comptes)
        }
        else {
          if (n == 4)  {
            changepin(id, codespin)
          }
        }
      }
    }
   }

   def changepin(id : Int, codespin : Array[String]) : Unit = {
    val newPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    if (newPin.length() < 8)  {
      println("Votre code pin ne contient pas au moins 8 caractères")
      changepin(id, codespin)
    }
    else {
      codespin(id) = newPin
    }
   }



   def depot(id : Int, comptes : Array[Double]) : Unit = {

    val devise : Int = boucleChoixDeviseDépôt()

    val depotMontant : Int = boucleDepotMontant()

    if (devise == 1)  {
      comptes(id) = comptes(id) + depotMontant
    }
    else {
      comptes(id) = comptes(id) + TAUX_EUR_CHF * depotMontant
    }
     printf("Votre montant a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
      
   }

   def consultationCompte(id : Int, comptes : Array[Double]) : Unit = {
    printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
   }

   def retrait(id : Int, comptes : Array[Double]) : Unit = {

    val devise = boucleChoixDeviseRetrait()

    val deviseString = if (devise == 1)  {"CHF"} else {"EUR"}

    val montant = boucleRetraitMontant

    comptes(id) = if (devise == 1)  {comptes(id) - montant} else {comptes(id) - TAUX_EUR_CHF * montant}

    val typeCoupures = if (devise == 1 && montant >= 200)  {boucleChoixCoupures} else {2}

    if (typeCoupures == 1)  {

      val maxCoupures500 = montant / 500
      val coupures500 = boucleCoupures(montant, devise, maxCoupures500, 500)

      val montantRestant500 = montant - coupures500 * 500
      val maxCoupures200 = montantRestant500 / 200
      val coupures200 = boucleCoupures(montantRestant500, devise, maxCoupures200, 200)

      val montantRestant200 = montantRestant500 - coupures200 * 200
      val maxCoupures100 = montantRestant200 / 100
      val coupures100 = boucleCoupures(montantRestant200, devise, maxCoupures100, 100)

      val montantRestant100 = montantRestant200 - coupures100 * 100
      val maxCoupures50 = montantRestant100 / 50
      val coupures50 = boucleCoupures(montantRestant100, devise, maxCoupures50, 50)

      val montantRestant50 = montantRestant100 - coupures50 * 50
      val maxCoupures20 = montantRestant50 / 20
      val coupures20 = boucleCoupures(montantRestant50, devise, maxCoupures20, 20)

      val montantRestant20 = montantRestant50 - coupures20 * 20
      val coupures10 = montantRestant20 / 10

      println("Veuillez retirer la somme demandée :")

      if (coupures500 > 0)  {println(coupures500 + " billet(s) de 500 " + deviseString)}
      if (coupures200 > 0)  {println(coupures200 + " billet(s) de 200 " + deviseString)}
      if (coupures100 > 0)  {println(coupures100 + " billet(s) de 100 " + deviseString)}
      if (coupures50 > 0)  {println(coupures50 + " billet(s) de 50 " + deviseString)}
      if (coupures20 > 0)  {println(coupures20 + " billet(s) de 20 " + deviseString)}
      if (coupures10 > 0)  {println(coupures10 + " billet(s) de 10 " + deviseString)}

      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }
    else {
      val maxCoupures100 = montant / 100
      val coupures100 = boucleCoupures(montant, devise, maxCoupures100, 100)

      val montantRestant100 = montant - coupures100 * 100
      val maxCoupures50 = montantRestant100 / 50
      val coupures50 = boucleCoupures(montantRestant100, devise, maxCoupures50, 50)

      val montantRestant50 = montantRestant100 - coupures50 * 50
      val maxCoupures20 = montantRestant50 / 20
      val coupures20 = boucleCoupures(montantRestant50, devise, maxCoupures20, 20)

      val montantRestant20 = montantRestant50 - coupures20 * 20
      val coupures10 = montantRestant20 / 10

      println("Veuillez retirer la somme demandée :")

      if (coupures100 > 0)  {println(coupures100 + " billet(s) de 100 " + deviseString)}
      if (coupures50 > 0)  {println(coupures50 + " billet(s) de 50 " + deviseString)}
      if (coupures20 > 0)  {println(coupures20 + " billet(s) de 20 " + deviseString)}
      if (coupures10 > 0)  {println(coupures10 + " billet(s) de 10 " + deviseString)}

      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }
   }

   def boucleChoixDeviseDépôt() : Int = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    val devise = readInt()
    if (devise != 1 && devise != 2) {
      boucleChoixDeviseDépôt()
    }
    else {
      devise
    }
   }

   def boucleChoixDeviseRetrait() : Int = {
    println("Indiquez la devise : 1) CHF ; 2) EUR >")
    val devise = readInt()
    if (devise != 1 && devise != 2) {
      boucleChoixDeviseRetrait()
    }
    else {
      devise
    }
   }

   def boucleChoixCoupures : Int = {
    println("En 1) grosses coupures, 2) petites coupures >")
    val coupures = readInt()
    if (coupures != 1 && coupures != 2) {
      boucleChoixCoupures
    }
    else {
      coupures
    }
   }

   def boucleDepotMontant() : Int = {
    val montant : Int = readLine("Indiquez le montant du dépôt > \n ").toInt
    if (montant % 10 == 0) {
      montant
    }
    else {
      println("Le montant doit être un multiple de 10")
      boucleDepotMontant()
    }
   }
   

   def boucleRetraitMontant : Int = {
    var valid = true
    val montant : Int = readLine("Indiquez le montant du retrait > \n ").toInt
    val maxRetrait = 0.1 * comptes(id)
    if (montant % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      valid = false
    }

    if (montant > maxRetrait) {
      printf("Votre plafond de retrait autorisé est de : %.2f \n", maxRetrait)
      valid = false
    }
    
    if (valid) {
      montant 
    }
    else {
      boucleRetraitMontant
    }
    
   }

   def boucleCoupures(montant : Int, devise : Int, maxCoupures : Int, coupureEnQuestion : Int) : Int = {
    if (maxCoupures == 0 || montant == 0) {0}
    else {
      val deviseString = if (devise == 1) {"CHF"} else {"EUR"}
      println("Il reste " + montant +" " + deviseString + " à distribuer")

      println("Vous pouvez obtenir au maximum " + maxCoupures + " billet(s) de " + coupureEnQuestion + " " + deviseString)
      val decision = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > \n ")
      if (decision == "o")  {
        maxCoupures
      }
      else {
        if ((decision.toInt >= 0) && (decision.toInt < maxCoupures)) {
          decision.toInt
        }
        else {
         boucleCoupures(montant,devise,maxCoupures,coupureEnQuestion)
        }
      }
    }
   }
}

