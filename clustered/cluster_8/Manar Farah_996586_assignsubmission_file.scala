//Assignment: Manar Farah_996586_assignsubmission_file

import scala.io.StdIn._
  
object Manager {
  // Déclaration d'une méthode permettant de réaliser un dépôt en fonction de son id.
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    // variable pour la boucle de vérification de saisie.
    // La lecture au clavier est répétée jusqu'à obtenir les valeurs de réponse possibles:
    var depot = false
    var devise = readLine("Indiquez la devise: 1) CHF, 2) EUR : ").toInt
    var montantdepot = readLine("Indiquez le montant du dépôt : ").toInt
    while ((montantdepot % 10 != 0) && (depot == false)){
      var montantdepot = readLine("Les plus petites coupures accepter sont de 10CHF ou 10EUR, veuillez indiquer un autre montant : ").toInt
      if (montantdepot % 10 == 0){
        depot = true
        //conversion pour les euros (1 EUR vaut 0.97 CHF).
        if (devise == 2){
          comptes(id) = math.floor((comptes(id) + (montantdepot * 0.97))*100)/100
          println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + " CHF.")
        }
        else {
          comptes(id) = comptes(id) + montantdepot
          println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + " CHF.")
        }
     }  
   }
    if (montantdepot % 10 == 0) {
      if (devise == 2){
        comptes(id) = math.floor((comptes(id) + (montantdepot * 0.97))*100)/100
        println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + " CHF.")
      }
      else {
        comptes(id) = comptes(id) + montantdepot
        println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + " CHF.")
      }
    } 
 }

  // Déclaration d'une méthode permettant de réaliser un retrait en fonction de son id.
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    // variable pour la boucle de vérification de saisie.
    // La lecture au clavier est répétée jusqu'à obtenir les valeurs de réponse possibles:
    var retrait = false
    var devise = readLine("Indiquez la devise: 1) CHF, 2) EUR : ").toInt
    //boucle de vérification de saisie:
    // la lecture au clavier est répétée jusqu'à obtenir les valeurs de réponse possibles.
    if ((devise != 1) && (devise != 2)){
      var devise = readLine("Indiquez la devise: 1) CHF, 2) EUR : ").toInt
    }
    var montantretrait = readLine("Indiquez le montant du retrait: ").toInt
    while ((montantretrait % 10 != 0) || (montantretrait > ((10 * comptes(id))/100)) && (retrait == false)) {
      if (montantretrait % 10 != 0){
        println ("Le montant doit être un multiple de 10.")
        montantretrait = readLine("Indiquez un nouveau montant de retrait: ").toInt
     }
      else if (montantretrait > ((10 * comptes(id))/100)){
        // conversion du plafond pour les euros.
        if(devise == 2){
          println("Votre plafond de retrait autorisé est de : " + math.floor(((10 * comptes(id))/100)*0.97) + " EUR.")
       }
        else { 
          println ("Votre plafond de retrait autorisé est de : " + math.floor((10 * comptes(id))/100) + " CHF.")
       }
        montantretrait = readLine("Indiquez un nouveau montantde retrait: ").toInt
     }   
    }
    if ((montantretrait % 10 == 0) || (montantretrait <= (10 * comptes(id))/100)) retrait = true
    if ((montantretrait % 10 == 0) || (montantretrait <= (10 * comptes(id))/100)) {
      if (devise == 1){
        if (montantretrait >= 200){
          val coupure = readLine("1) petites ou 2) grosses coupures ? ").toInt
          if ((coupure != 1) && (coupure != 2)){
           val coupure = readLine("1) petites ou 2) grosses coupures ? ").toInt
          }
          if(coupure == 1){
            val nbcent = (montantretrait / 100).toInt
            var reste = (montantretrait % 100).toInt
            val nbcinquante = (reste / 50).toInt
            reste = (reste % 50).toInt
            val nbvingt = (reste / 20).toInt
            reste = (reste % 20).toInt
            val nbdix = (reste / 10).toInt
            println("Veuillez retirer la somme demandée:")
            println("")
            println("")
            println("--------------------------------------")
            // on souhaite afficher seulement les coupures distribuées.
            if (nbcent > 0){
              println(nbcent + " Billet(s) de 100 CHF")
            }
            if (nbcinquante > 0){
              println(nbcinquante + " Billet(s) de 50 CHF")
            }
            if (nbvingt > 0){
              println(nbvingt + " Billet(s) de 20 CHF")
            }
            if (nbdix > 0){
              println(nbdix + " Billet(s) de 10 CHF")
            }
            println("--------------------------------------")
            println("")
            println("")
          }
          else {
            var nbcinqcent = (montantretrait / 500).toInt
            var reste = (montantretrait % 500).toInt
            var nbdeuxcent = (reste / 200).toInt
            reste = (reste % 200).toInt
            var nbcent = (reste / 100).toInt
            reste = (reste % 100).toInt
            var nbcinquante = (reste / 50).toInt
            reste = (reste % 50).toInt
            var nbvingt = (reste / 20).toInt
            reste = (reste % 20).toInt
            var nbdix = (reste / 10).toInt
            println("Veuillez retirer la somme demandée :")
            println("")
            println("")
            println("--------------------------------------")
            if (nbcinqcent > 0){
              println(nbcinqcent + " Billet(s) de 500 CHF")
            }
            if (nbdeuxcent > 0){
              println(nbdeuxcent + " Billet(s) de 200 CHF" )
            }
            if (nbcent > 0){
              println(nbcent + " Billet(s) de 100 CHF")
            }
            if (nbcinquante > 0){
              println(nbcinquante + " Billet(s) de 50 CHF")
            }
            if (nbvingt > 0){
              println(nbvingt + " Billet(s) de 20 CHF")
            }
            if (nbdix > 0){
              println(nbdix + " Billet(s) de 10 CHF")
            }
            println("--------------------------------------")
            println("")
            println("")
         }
        }    
        else {
          val nbcent = (montantretrait / 100).toInt
          var reste = (montantretrait % 100).toInt
          val nbcinquante = (reste / 50).toInt
          reste = (reste % 50).toInt
          val nbvingt = (reste / 20).toInt
          reste = (reste % 20)
          val nbdix = (reste / 10)
          println("Veuillez retirer la somme demandée:")
          println("")
          println("")
          println("--------------------------------------")
          if (nbcent > 0){
            println(nbcent + " Billet(s) de 100 CHF")
          }
          if (nbcinquante > 0){
            println(nbcinquante + " Billet(s) de 50 CHF")
          }
          if (nbvingt > 0){
            println(nbvingt + " Billet(s) de 20 CHF")
          }
          if (nbdix > 0){
            println(nbdix + " Billet(s) de 10 CHF")
          }
          println("--------------------------------------")
          println("")
          println("")
       }
        comptes(id) = comptes(id) - montantretrait
        println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + " CHF.")
     }
      else {
        val nbcent = (montantretrait / 100).toInt
        var reste = (montantretrait % 100).toInt
        val nbcinquante = (reste / 50).toInt
        reste = (reste % 50).toInt
        val nbvingt = (reste / 20).toInt
        reste = (reste % 20).toInt
        val nbdix = (reste / 10).toInt
        println("Veuillez retirer la somme demandée:")
        println("")
        println("")
        println("--------------------------------------")
        if (nbcent > 0){
          println(nbcent + " Billet(s) de 100 EUR")
        }
        if (nbcinquante > 0){
          println(nbcinquante + " Billet(s) de 50 EUR")
        }
        if (nbvingt > 0){
          println(nbvingt + " Billet(s) de 20 EUR")
        }
        if (nbdix > 0){
          println(nbdix + " Billet(s) de 10 EUR")
        }
        println("--------------------------------------")
        println("")
        println("")
        comptes(id) = math.floor((comptes(id) - (montantretrait * 0.97))*100)/100
        println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + " CHF.")
     }
   }
  }

  // Déclaration d'une méthode permettant de changer son code pin en fonction de son id.
  def changepin(id : Int, codespin : Array[String]) : Unit = {
     var newCodePin = ""
     var flagBonTaille = false
     while (flagBonTaille == false){
       newCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères): ").toString
       if (newCodePin.length < 8){
         println("Votre code pin ne contient pas au moins 8 caractères.")
       }
       else flagBonTaille = true
     }
     codespin(id) = newCodePin
   }
 }

object Main {
  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")
  
  def main(args: Array[String]): Unit = {
    var codebon = false
    var nbEssais = 3
    var choix = 0
    var id = readLine("Saisissez votre identifiant: ").toInt
    if (id > nbclients) {
      println("Cet identifiant n’est pas valable.") 
      return
    }
    while ((nbEssais >= 0) && (codebon == false)) {
      if (nbEssais <= 0) { 
        println("Trop d’erreurs, abandon de l’identification.")
        return
      }
      val tentative = readLine("Saisissez votre code pin: ").toString
      if (tentative == codespin(id)) codebon = true
      else println("Code pin erroné, il vous reste " + (nbEssais - 1) +  " tentatives.")
       
      nbEssais = nbEssais - 1
    }
    
    while (codebon == true){
      println("")
      println("")
      println ("Choisissez votre nouvelle opération:")
      println("1) Dépôt ")
      println("2) Retrait") 
      println("3) Consultation du compte") 
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix: ")
      choix = readInt()
      println("")
      println("")
      
      if (choix == 1) {
        // Appel de la méthode du dépôt.
        Manager.depot(id, comptes)
      }
      
      if (choix == 2){
        // Appel de la méthode du retrait.
        Manager.retrait(id, comptes)
      }
      
      if (choix == 3){
        println("Le montant disponible sur votre compte est de : " + comptes(id) + " CHF.")
      }
      
      if (choix == 4){
        // Appel de la méthode du changement de code pin.
        Manager.changepin(id, codespin)
      }
      
      if (choix == 5){
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        println("")
        println("")
        var newId = readLine("Saisissez votre identifiant: ").toInt
        if (newId > nbclients) {
          println("Cet identifiant n’est pas valable.") 
          return
        }
        id = newId
        choix = 0
        codebon = false
        nbEssais = 3
        while ((nbEssais >= 0) && (codebon == false)) {
          if (nbEssais <= 0) { 
            println("Trop d’erreurs, abandon de l’identification.")
            return
          }
          val tentative = readLine("Saisissez votre code pin: ").toString
          if (tentative == codespin(id)) codebon = true
          else println("Code pin erroné, il vous reste " + (nbEssais - 1) +  " tentatives.")
           
          nbEssais = nbEssais - 1
        }
      }
    }
  }
}
