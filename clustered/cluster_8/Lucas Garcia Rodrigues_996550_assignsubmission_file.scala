//Assignment: Lucas Garcia Rodrigues_996550_assignsubmission_file

import scala.io.StdIn._

object Main {

  def depot(id: Int, comptes: Array[Double]): Unit = {
    var choixDevise = 0
    var montantTransfert = 0

    do {
      print("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
      choixDevise = readInt()
    } while (choixDevise != 1 && choixDevise != 2)

    do {
      print("Indiquez le montant du dépôt > ")
      montantTransfert = readInt()
      if (montantTransfert % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
    } while (montantTransfert % 10 != 0)

    if (choixDevise == 1) {
      comptes(id) = comptes(id) + montantTransfert
    } else if (choixDevise == 2) {
      comptes(id) = comptes(id) + montantTransfert * 0.95
    }

    printf(
      "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n",
      comptes(id)
    )
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {
    var choixDevise = 0
    var montantTransfert = 0
    var valeurBilletG = List(500, 200, 100, 50, 20)
    var valeurBilletP = List(100, 50, 20)
    var devise = ""
    var nombreBillet = 0
    var nombreBillet500 = 0
    var nombreBillet200 = 0
    var nombreBillet100 = 0
    var nombreBillet50 = 0
    var nombreBillet20 = 0
    var nombreBillet10 = 0
    var choixCoupure = 0
    var choixBillet = ""
    var choixBilletInt = 0
    var validationCoupure = false

    do {
      print("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ")
      choixDevise = readInt()
      if (choixDevise == 1) {
        devise = "CHF"
      } else if (choixDevise == 2) {
        devise = "EUR"
      }
    } while (choixDevise != 1 && choixDevise != 2)

    do {
      print("Indiquez le montant du retrait > ")
      montantTransfert = readInt()
      if (montantTransfert % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
      if (
        (montantTransfert > comptes(id) * 0.1 && choixDevise == 1) || (montantTransfert * 0.95 > comptes(id) * 0.1 && choixDevise == 2)
      ) {
        printf("Votre plafond de retrait autorisé est de : %.2f CHF \n", comptes(id) * 0.1)
      }
    } while (
      montantTransfert % 10 != 0 || (montantTransfert > comptes(id) * 0.1 && choixDevise == 1) || (montantTransfert * 0.95 > comptes(id) * 0.1 && choixDevise == 2)
    )

    if (choixDevise == 1) {
      comptes(id) = comptes(id) - montantTransfert
    } else if (choixDevise == 2) {
      comptes(id) = comptes(id) - montantTransfert * 0.95
    }

    if (montantTransfert < 200 || choixDevise == 2) {
      choixCoupure = 2
    }

    do {
      if (montantTransfert >= 200 && choixDevise == 1) {
        print("En 1) grosses coupures, 2) petites coupures > ")
        choixCoupure = readInt()
      }
    } while (choixCoupure != 1 && choixCoupure != 2)

    if (choixCoupure == 1) {
      for (i <- valeurBilletG) {
        nombreBillet = montantTransfert / i
        if (nombreBillet >= 1) {
          do {
            validationCoupure = false
            print(
              s"Il reste $montantTransfert $devise à distribuer \nVous pouvez obtenir au maximum $nombreBillet billet(s) de $i $devise \nTapez o pour ok ou une autre valeur inférieure à celle proposée > "
            )
            choixBillet = readLine()
            if (choixBillet.forall(_.isDigit)) {
              choixBilletInt = choixBillet.toInt
              if (choixBilletInt < nombreBillet) {
                montantTransfert = montantTransfert - choixBilletInt * i
                if (i == 500) {
                  nombreBillet500 = choixBilletInt
                } else if (i == 200) {
                  nombreBillet200 = choixBilletInt
                } else if (i == 100) {
                  nombreBillet100 = choixBilletInt
                } else if (i == 50) {
                  nombreBillet50 = choixBilletInt
                } else if (i == 20) {
                  nombreBillet20 = choixBilletInt
                }
                validationCoupure = true
              }
            } else if (choixBillet == "o") {
              montantTransfert = montantTransfert - nombreBillet * i
              if (i == 500) {
                nombreBillet500 = nombreBillet
              } else if (i == 200) {
                nombreBillet200 = nombreBillet
              } else if (i == 100) {
                nombreBillet100 = nombreBillet
              } else if (i == 50) {
                nombreBillet50 = nombreBillet
              } else if (i == 20) {
                nombreBillet20 = nombreBillet
              }
            }
          } while (choixBillet != "o" && !validationCoupure)
        }
      }
    } else if (choixCoupure == 2) {
      for (i <- valeurBilletP) {
        nombreBillet = montantTransfert / i
        if (nombreBillet >= 1) {
          do {
            validationCoupure = false
            print(
              s"Il reste $montantTransfert $devise à distribuer \nVous pouvez obtenir au maximum $nombreBillet billet(s) de $i $devise \nTapez o pour ok ou une autre valeur inférieure à celle proposée > "
            )
            choixBillet = readLine()
            if (choixBillet.forall(_.isDigit)) {
              choixBilletInt = choixBillet.toInt
              if (choixBilletInt < nombreBillet) {
                montantTransfert = montantTransfert - choixBilletInt * i
                if (i == 500) {
                  nombreBillet500 = choixBilletInt
                } else if (i == 200) {
                  nombreBillet200 = choixBilletInt
                } else if (i == 100) {
                  nombreBillet100 = choixBilletInt
                } else if (i == 50) {
                  nombreBillet50 = choixBilletInt
                } else if (i == 20) {
                  nombreBillet20 = choixBilletInt
                }
                validationCoupure = true
              }
            } else if (choixBillet == "o") {
              montantTransfert = montantTransfert - nombreBillet * i
              if (i == 500) {
                nombreBillet500 = nombreBillet
              } else if (i == 200) {
                nombreBillet200 = nombreBillet
              } else if (i == 100) {
                nombreBillet100 = nombreBillet
              } else if (i == 50) {
                nombreBillet50 = nombreBillet
              } else if (i == 20) {
                nombreBillet20 = nombreBillet
              }
            }
          } while (choixBillet != "o" && !validationCoupure)
        }
      }
    }

    nombreBillet10 = montantTransfert / 10
    println("Veuillez retirer la somme demandée : ")

    if (nombreBillet500 >= 1) {
      println(s"$nombreBillet500 billet(s) de 500 $devise")
    }
    if (nombreBillet200 >= 1) {
      println(s"$nombreBillet200 billet(s) de 200 $devise")
    }
    if (nombreBillet100 >= 1) {
      println(s"$nombreBillet100 billet(s) de 100 $devise")
    }
    if (nombreBillet50 >= 1) {
      println(s"$nombreBillet50 billet(s) de 50 $devise")
    }
    if (nombreBillet20 >= 1) {
      println(s"$nombreBillet20 billet(s) de 20 $devise")
    }
    if (nombreBillet10 >= 1) {
      println(s"$nombreBillet10 billet(s) de 10 $devise")
    }

    printf(
      "Votre retrait a été pris en compte, le nouveau montant \ndisponible sur votre compte est de : %.2f CHF \n",
      comptes(id)
    )
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {
    var nouveaupin = ""
    
    do {
      print("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      nouveaupin = readLine()
      if (nouveaupin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères")
      } else if (nouveaupin.length >= 8) {
        codespin(id) = nouveaupin
      }
    } while (nouveaupin.length < 8)
  }

  def main(args: Array[String]): Unit = {

    var nbclients = 100
    var comptes = Array.fill[Double](nbclients)(1200.0)
    var codespin = Array.fill[String](nbclients)("INTRO1234")
    var pinValide = false
    var id = 0
    var pin = ""
    var tentatives = 3
    var choixOperation = 0
    var idValide = true

    do {
      print("Saisissez votre code identifiant > ")
      id = readInt()
      if (id > nbclients) {
        println("Cet identifiant n’est pas valable.")
        idValide = false
      } else {
        print("Saisissez votre code PIN > ")
        pin = readLine()
        while ((pin != codespin(id)) && (tentatives > 1)) {
          tentatives = tentatives - 1
          print(s"Code pin erroné, il vous reste $tentatives tentatives > ")
          pin = readLine()
        }
        if (tentatives == 1 && pin != codespin(id)) {
          println("Trop d’erreurs, abandon de l’identification")
          tentatives = 3
        }
        if (pin == codespin(id)) {
          pinValide = true
        }
      }

      while (pinValide) {
        do {
          print(
            "Choisissez votre opération :\n\t 1) Dépôt\n\t 2) Retrait\n\t 3) Consultation du compte\n\t 4) Changement du code pin\n\t 5) Terminer\n Votre choix : "
          )
          choixOperation = readInt()
        } while (choixOperation < 1 || choixOperation > 5)

        if (choixOperation == 1) {
          depot(id, comptes)
        } else if (choixOperation == 2) {
          retrait(id, comptes)
        } else if (choixOperation == 3) {
          printf("Le montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
        } else if (choixOperation == 4) {
          changepin(id, codespin)
        } else if (choixOperation == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          tentatives = 3
          pinValide = false
        }
      }
    } while (pinValide == false && idValide == true)
  }
}
