//Assignment: Biniam Eyob Tafere_996533_assignsubmission_file

object Main {  
  // METHOD IDENTIFIER LE CLIENT A TRAITER
  def choisirclient(arrayID: Array[Int], arrayPIN: Array[String]): Int = {
    import scala.io.StdIn._
    var identifiant : Int = 0
    var pinInsert = ""
    var identifiantEntree : Int = 0
    do{
      do {
        println("Saisissez votre code identifiant")
        identifiantEntree = readInt()
        if(!arrayID.contains(identifiantEntree)){
          println("Cet identifiant n’est pas valable")
          sys.exit()
        }
      } while (!arrayID.contains(identifiantEntree))
      var tentative = 0
      val maxTentative = 3
      var restTent = maxTentative
      do {
        println("Saisissez votre code pin : ") 
        pinInsert = readLine()
        if(pinInsert != arrayPIN(identifiantEntree)){
          tentative = tentative + 1
          restTent = maxTentative - tentative
          println("Code pin erroné, il vous reste " + restTent + " tentatives")   
        }
      } while (restTent > 0 && pinInsert != arrayPIN(identifiantEntree))
      if (restTent == 0) {
        println("Trop d’erreurs, abandon de l’identification.")
        restTent = maxTentative
      } 
    } while (pinInsert != arrayPIN(identifiantEntree))
    return identifiantEntree
  }

  // METHOD POUR VOIR ET CHOISIR L'OPTION DU MENU
  def selectionmenu(): Int = {
    import scala.io.StdIn._
    var optionSelect : Int = 0
    val menu = "Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix :"
    do{
      println(menu)
      optionSelect = readInt()
    } while (optionSelect == 0 || optionSelect > 5)
    return optionSelect
  }

  // METHOD POUR FAIRE UN DEPO A LA COMPTE
  def depot(id: Int, comptes: Array[Double]) : Unit = {
    import scala.io.StdIn._
    var montantTotal = comptes(id)
    var montantInsere = 0
    var montantConverti : Double = 0.0
    var devise = 1
    val devCHF = 1
    val EURaCHF : Double = 0.95
    // CHOISIR LA DEVISE POR LE DEPOT
    do{
      println("Indiquez la devise du dépôt :\n 1) CHF \n 2) EUR ")
      devise = readInt()
    } while (devise == 0 || devise > 2)
    // DEMANDER ET LIRE MONTANT A INSERER
    do{
      println("Indiquez le montant du dépôt :")
      montantInsere = readInt()
      if(montantInsere % 10 != 0){
        println("Le montant doit être un multiple de 10")
      }
    } while (montantInsere % 10 != 0)
    // CALCULER LE NOUVEAU MONTANT TOTAL EN CHF ET MONTRER AU CLIENT
    if (devise == devCHF) {
      montantTotal = montantTotal + montantInsere
    } else {
      montantConverti = montantInsere*EURaCHF
      montantTotal = montantTotal + montantConverti
    }
    // SOUVEGARDER LE NOUVEAU MONTANT
    comptes(id) = montantTotal
    println("Votre dépôt a été pris en compte, le nouveau montant est de : " + montantTotal + " CHF")
  } 

  // METHOD POUR RETRAIT DE LA COMPTE
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    import scala.io.StdIn._
    var devString = "CHF"
    var devise = 1
    var montantRetrait = 0
    var montantTotal = comptes(id)
    var montantRest = 0
    var montantConverti : Double = 0.00
    val devCHF = 1
    val EURaCHF : Double = 0.95 
    var coupuresChoix = 2
    var coupures: Array[Int] = Array(500, 200, 100, 50)
    val coupuresGross: Array[Int] = Array(500, 200, 100, 50, 20, 10)
    val coupuresPetit: Array[Int] = Array(100, 50, 20, 10)
    var positionArray = -1
    var positionArrayFinal = -1
    var billets = 0
    var billetsFinal : Array[Int] = Array(0, 0, 0, 0, 0, 0)
    var coupuresFinal : Array[Int] = Array(0, 0, 0, 0, 0, 0)
    var coupuresDecision = "o"
    var coupuresAccepte = 0
    var coupuresQuanite = 0
    var coupuresValeur = 0
    // CHOISIR LA DEVISE POR LE DEPOT
    do{
      println("Indiquez la devise :\n 1) CHF \n 2) EUR ")
      devise = readInt()
      if(devise == 2){
        devString = "EUR"
      }
    } while (devise == 0 || devise > 2)
    // CHOISIR LE MONTANT POR RETRAIT
    do{
      println("Indiquez le montant du retrait :")
      montantRetrait = readInt()
      if(montantRetrait % 10 != 0){
        println("Le montant doit être un multiple de 10")
      }
      if(montantRetrait > montantTotal * 0.1){
        println("Votre plafond de retrait autorisé est de : " + montantTotal * 0.1 * 1.05 + devString)
      }
    } while (montantRetrait % 10 != 0 || montantRetrait > montantTotal * 0.1)
    // CHOISIR LES COUPURES POUR LE RETRAIT
    if(montantRetrait >= 200 && devise == devCHF){
      do{
        println("En :\n 1) Grosses coupures \n 2) Petites coupures ")
        coupuresChoix = readInt()
        if(coupuresChoix == 1){
          coupures = coupuresGross
        } else {
          coupures = coupuresPetit
        }
      } while (devise == 0 || devise > 2)
    } else {
      coupures = coupuresPetit
    }
    // NOMBRE DE COUPURES A RETRATEIR
    montantRest = montantRetrait
    do {
      // OPTION DE BILLET PLUS GRAND A DONER
      do {
        billets = 0
        positionArray = positionArray + 1
        coupuresValeur = coupures(positionArray)
        if(montantRest / coupuresValeur > 0){
          billets = montantRest / coupuresValeur
        }
      } while (billets == 0)
      println("Il reste " + montantRest + " " + devString + " à distribuer")
      println("Vous pouvez obtenir au maximum " + billets + " billet(s) de " + coupuresValeur + " " + devString + ".")
      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
      coupuresDecision = readLine()
      // COUPURE MAX NO ACCEPTE   
      if(coupuresDecision != "o"){
        do {
          coupuresAccepte = coupuresDecision.toInt
          if(coupuresAccepte == 0){
            do {
              billets = 0
              positionArray = positionArray + 1
              coupuresValeur = coupures(positionArray)
              if(montantRest / coupuresValeur > 0){
                billets = montantRest / coupuresValeur
              }
            } while (billets == 0)
          }
          else if(coupuresAccepte > billets){
            println("Tapez une valeur inférieure à celle proposée")
            coupuresDecision = readLine()
          }
          else if(coupuresAccepte < billets){
            coupuresAccepte = coupuresDecision.toInt
            positionArrayFinal = positionArrayFinal + 1
            montantRest = montantRest - (coupuresAccepte * coupuresValeur)
            billetsFinal(positionArrayFinal) = coupuresAccepte
            coupuresFinal(positionArrayFinal) = coupuresValeur
            billets = 0
            do {
              billets = 0
              positionArray = positionArray + 1
              coupuresValeur = coupures(positionArray)
              if(montantRest / coupuresValeur > 0){
                billets = montantRest / coupuresValeur
              }
            } while (billets == 0)
          }
          println("Il reste " + montantRest + " " + devString + " à distribuer")
          println("Vous pouvez obtenir au maximum " + billets + " billet(s) de " + coupuresValeur + " " + devString + ".")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          coupuresDecision = readLine()
        } while (coupuresDecision != "o")
      }
      // COUPURE ACCEPTE
      if(coupuresDecision == "o"){
        positionArrayFinal = positionArrayFinal + 1
        montantRest = montantRest - (billets * coupuresValeur)
        billetsFinal(positionArrayFinal) = billets
        coupuresFinal(positionArrayFinal) = coupuresValeur
      }
    } while(montantRest > 0)
    // RETRAIT FINAL
    println("Veuillez retirer la somme demandée :") 
    for (i <- 0 to 5) {
      if(billetsFinal(i) > 0 && coupuresFinal(i) > 0){
        println(billetsFinal(i) + " billet(s) de " + coupuresFinal(i) + devString + ".") 
      }
    }
    // MONTANT RESTANT APRES RETRAIT
    if (devise == devCHF) {
      montantTotal = montantTotal - montantRetrait
    } else {
      montantConverti = montantRetrait * EURaCHF
      montantTotal = montantTotal - montantConverti
    }
    comptes(id) = montantTotal
    println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + montantTotal) 
  }

  // METHOD POUR CHANGER LE PIN DE LA COMPTE
  def changepin(id: Int, codespin: Array[String]): Unit = {
    import scala.io.StdIn._
    var newpin = "INTRO1234"
    // DEMANDER LE NOVEUX PIN
    do{
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères)")
      newpin = readLine()
      if(newpin.length < 8){
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
    } while (newpin.length < 8)
    // SAUVEGARDER LE NOUVEAU PIN
    codespin(id) = newpin
    println("Votre code pin est changé correctement")
  } 

  // PROGRAM PRINCIPAL
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn._
    val nbclients = 100
    var comptesID: Array[Int] = Array.range(1, nbclients)
    var comptesInitial : Array[Double] = Array.fill(nbclients)(1200)
    var comptes : Array[Double] = comptesInitial
    var codespin : Array[String] = Array.fill(nbclients)("INTRO1234")
    
    var idClient = 0
    var option = 0

    idClient = choisirclient(comptesID, codespin)
    
    do {
      option = selectionmenu()
      if (option == 1) {
        depot(idClient, comptes)
      }
      if (option == 2) {
        retrait(idClient, comptes)
      }
      if (option == 3) {
        println("Le montant disponible sur votre compte est de : " + comptes(idClient) + " CHF.")
      }
      if (option == 4) {
        changepin(idClient, codespin)
      }
      if (option == 5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        idClient = choisirclient(comptesID, codespin)
      }
    } while(option != 0)

  }

}