//Assignment: Ayub Eliassi_996458_assignsubmission_file

import scala.io.StdIn._
object Main {

  def changepin(id: Int, codespin: Array[String]): Unit = {
    codespin(id) = readLine("Veuillez entrer le nouveau code pin")
    val a = "12345678"
    if (codespin(id).length < a.length) while (codespin(id).length < a.length) {
      codespin(id) = readLine("Veuillez entrer le nouveau code pin" + "\n (le nouveau code pin doit contenir au moins 8 caractères")
    }
  }

  def retrait(id: Int, comptes: Array[Double]): Unit ={
    var coupure = 0
    println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
    var deviseRetrait = readInt()
    if (deviseRetrait != 2 && deviseRetrait != 1) {
      while (deviseRetrait != 2 && deviseRetrait != 1){
      println("Reindiquez la devise du retrait : 1) CHF ; 2) EUR >")
      deviseRetrait = readInt()

    }}
    println("Indiquez le montant du retrait >")
    var retrait = readInt()

    var plafond = comptes(id) / 10
    if (retrait % 10 != 0 || retrait > plafond) {
      while (retrait % 10 != 0 || retrait > plafond){
        if (retrait > plafond) {
          println("retrait supérieur au plafond")
          println("Votre plafond de retrait autorisé est de :" + plafond)
          println("Indiquez le montant du retrait >")
          retrait = readInt()
        }

        if (retrait % 10 != 0){
          println("Le montant doit être un multiple de 10")
          println("Indiquez le montant du retrait >")
          retrait = readInt()}

    }

    }

    val retrait2: Double = retrait
    if ((deviseRetrait == 1) && (retrait >= 200)) {
      println("En 1) grosses coupures, 2) petites coupures >")
      coupure = readInt()

      while (coupure > 2) {
        println("En 1) grosses coupures, 2) petites coupures >")
        coupure = readInt()
      }
    } else if ((retrait < 200) || (deviseRetrait == 2)) {
      coupure = 2
    }


    if (coupure == 1) { //en grosse coupure
      if (deviseRetrait == 1) { //en chf
        val aaaaaa = retrait / 500
        println(aaaaaa + " billet(s) de 500 CHF")
        retrait = retrait - aaaaaa * 500
        val aaaaa = retrait / 200
        println(aaaaa + " billet(s) de 200 CHF")
        retrait = retrait - aaaaa * 200
        val a = retrait / 100
        println(a + " billet(s) de 100 CHF")
        retrait = retrait - a * 100
        val aa = retrait / 50
        println(aa + " billet(s) de 50 CHF")
        retrait = retrait - aa * 50
        val aaa = retrait / 20
        println(aaa + " billet(s) de 20 CHF")
        retrait = retrait - aaa * 20
        val aaaa = retrait / 10
        println(aaaa + " billet(s) de 10 CHF")
        retrait = retrait - aaaa * 10
      }
      if (deviseRetrait == 2) { //en euro
        val g = retrait / 100
        println(g + " billet(s) de 100 EUR")
        retrait = retrait - g * 100
        val gg = retrait / 50
        println(gg + " billet(s) de 50 EUR")
        retrait = retrait - gg * 50
        val ggg = retrait / 20
        println(ggg + " billet(s) de 20 EUR")
        retrait = retrait - ggg * 20
        val gggg = retrait / 10
        println(gggg + " billet(s) de 10 EUR")
        retrait = retrait - gggg * 10
      }
    }
    if (coupure == 2) { //en petite coupure
      if (deviseRetrait == 1) { //en chf
        val b = retrait / 100
        println(b + " billet(s) de 100 CHF")
        retrait = retrait - b * 100
        val bb = retrait / 50
        println(bb + " billet(s) de 50 CHF")
        retrait = retrait - bb * 50
        val bbb = retrait / 20
        println(bbb + " billet(s) de 20 CHF")
        retrait = retrait - bbb * 20
        val bbbb = retrait / 10
        println(bbbb + " billet(s) de 10 CHF")
        retrait = retrait - bbbb * 10
      }
      if (deviseRetrait == 2) { //en euro
        val s = retrait / 100
        println(s + " billet(s) de 100 EUR")
        retrait = retrait - s * 100
        val ss = retrait / 50
        println(ss + " billet(s) de 50 EUR")
        retrait = retrait - ss * 50
        val sss = retrait / 20
        println(sss + " billet(s) de 20 EUR")
        retrait = retrait - sss * 20
        val ssss = retrait / 10
        println(ssss + " billet(s) de 10 EUR")
        retrait = retrait - ssss * 10
      }
    }


    comptes(id) = comptes(id) - retrait2
    comptes(id) = math.floor(comptes(id) * 100) / 100
    println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + "CHF \n.")
  }


  def depot(id: Int, comptes: Array[Double]): Unit = {
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      var devise = readInt()
      if (devise > 2) {
        println("Reindiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        devise = readInt()
      }

      //montantDepot correspond à l'argent ajouter au compte
      if (devise == 1) {
        println("Indiquez le montant du dépôt : >")

        var montantDepot1: Double = readInt()
        if (montantDepot1 < 10) {
          println("le dépot doit être de plus que 10-.")
        }
        if (montantDepot1 % 10 != 0) {
          println("Le montant doit être un multiple de 10")
          println("Indiquez le montant du dépot >")
          montantDepot1 = readInt()
        }
        comptes(id) = comptes(id) + montantDepot1
        comptes(id) = math.floor(comptes(id) * 100) / 100
        println("votre nouveau montant est de " + comptes(id) + "-. \n.")

      }


      if (devise == 2) {
        println("Indiquez le montant du dépôt : >")
        var montantDepot2: Double = readInt()
        if (montantDepot2 < 10) {
          println("le dépot doit être de plus que 10-.")
        }
        if (montantDepot2 % 10 != 0) {
          println("Le montant doit être un multiple de 10")
          println("Indiquez le montant du dépot >")
          var montantDepot1 = readInt()
        }


        montantDepot2 = montantDepot2 * 0.95
        comptes(id) = comptes(id) + montantDepot2
        comptes(id) = math.floor(comptes(id) * 100) / 100
        println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est " + comptes(id) + "CHF \n.") //l'argent sur le compte apres le dépot


      }


    }



  def main(args: Array[String]): Unit = {
    val nbclients = 99
    val comptes = Array.fill(nbclients)(1200.00)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var auth = false
    var inf = 5
    var id = 99
    var test = false
    var testid = false
    while (inf == 5) {
      if (!testid) {
        println("Saisissez votre code identifiant >")

        id = readInt()
        if (id > 99) {
          System.exit(0)
        }
        testid = true
      }
    print("Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte \n4) Changer de code\n5) Terminer")
    println("\nVotre choix>")
    var choix=readInt()




    //--------------------CHOIX ERREUR--------------------
    if(choix >5){
      while (choix != 1 && choix != 2 && choix != 4 && choix != 3 && choix != 5 )
      print("Choisissez votre opération : \n1) Dépôt\n2) Retrait\n3) Consultation du compte \n4) Changer de code\n5) Terminer")
      println("\nVotre choix>")
      choix=readInt()
    }
      if (!test ){
        while (!test){
          println("Saisissez votre code pin>")
          var pin = readLine()
          if (pin != codespin(id)) {
            println("Code pin erroné, il vous reste " + 2 + " tentatives >")
            println("Saisissez votre code pin>")
            var pin = readLine()
            if (pin != codespin(id)) {
              println("Code pin erroné, il vous reste " + 1 + " tentatives >")
              println("Saisissez votre code pin>")
              var pin = readLine()
              if (pin != codespin(id)) {
                println("Trop d’erreurs, abandon de l’identification")
                testid = false
                println("Saisissez votre id>")
                id = readInt()
                if (!test ){
                  while (!test){
                    println("Saisissez votre code pin>")
                    var pin = readLine()
                    if (pin != codespin(id)) {
                      println("Code pin erroné, il vous reste " + 2 + " tentatives >")
                      println("Saisissez votre code pin>")
                      var pin = readLine()
                      if (pin != codespin(id)) {
                        println("Code pin erroné, il vous reste " + 1 + " tentatives >")
                        println("Saisissez votre code pin>")
                        var pin = readLine()
                        if (pin != codespin(id)) {
                          println("Trop d’erreurs, abandon de l’identification")
                          testid = false

                        }
                        else {
                          println("Code bon")
                          test = true
                        }
                      } else {
                        println("Code bon")
                        test = true
                      }
                    } else {
                      println("Code bon")
                      test = true
                    }

                  }
                }
              }
              else {
                println("Code bon")
                test = true
              }
            } else {
              println("Code bon")
              test = true
            }
          } else {
            println("Code bon")
            test = true
          }

        }
      }
      //---------------------CHOIX 5------------------------
      if (choix == 5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte")
        testid = false
        test = false

      }





      //---------------------CHOIX 1------------------------
      if(choix == 1 ) {depot(id,comptes)}

      //---------------------CHOIX 2------------------------
      if(choix == 2){retrait(id, comptes)}

      //---------------------CHOIX 3------------------------
      if(choix == 3 ){
        println("Le montant disponible sur votre compte est de "+comptes(id)+" CHF \n.")

      }
      if (choix == 4){
        changepin(id, codespin)
      }


  }     

}}