//Assignment: Romain Anthamatten_996531_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine

object Main {
  // Depot
  def depot(IDclient : Int, comptes : Array[Double]) : Unit = {
    var devise = 0
    while (devise != 1 && devise !=2) {
        devise = readLine("\nIndiquez la devise du dépôt: \n\n1) CHF \n2) EUR \n\nVotre choix: ").toInt
      }
    var montant = readLine("\nIndiquez le montant du dépôt: ").toDouble
    while(montant % 10 != 0 || montant < 0) {
      println("\nLe montant doit être un multiple de 10 et supérieur à 0.")
      montant = readLine("\nIndiquez le montant du dépôt: ").toDouble
      }
    if (devise == 2) {
      montant *= 0.95
      }
      comptes(IDclient) = comptes(IDclient) + montant
      println(f"\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de " + comptes(IDclient) + "CHF.")  
    }


  // Retrait
  def retrait(IDclient: Int, comptes: Array[Double]): Unit = {
    val billets = Array(500, 200, 100, 50, 20)
    val petitsbillets = Array(100, 50, 20)
    var deviseR = 0
    val plafond = comptes(IDclient) * 0.1
    while (deviseR != 1 && deviseR !=2) {
      deviseR = readLine("\nIndiquez la devise du retrait: \n\n1) CHF \n2) EUR \n\nVotre choix: ").toInt
    }
    println("\nIndiquez le montant du retrait:  ")
    var montantr = readInt().toDouble
    var possible = false
    while (!possible) {
      if (montantr % 10 != 0 || montantr < 0) {
      println("\nLe montant doit être un multiple de 10 et superieur à 0.")
       montantr = readLine("\nIndiquez le montant du retrait: ").toDouble
      }
      else if (montantr > plafond) {
        println(f"\nVotre plafond de retrait autorisé est de $plafond%.2f CHF.") 
        montantr = readLine("\nIndiquez le montant du retrait: ").toDouble 
      }
      else {
        possible = true

      }
    }
    var distributionBillets = Array[(Int, Int)]()
    var reste: Int = montantr.toInt
    if (possible && deviseR == 1 && montantr > 200){
      var coupures = 0
      while (coupures != 1 && coupures != 2) {
        coupures = readLine("\nSouhaitez vous retirez ce montant en:\n\n1) Grosses coupures\n2) Petites coupures\n\nVotre choix: ").toInt
      }
      if (coupures == 1){
        for (x <- billets.indices) {
          val billet = billets (x)
          if (reste >= billet){ 
          val maxbillets: Int = reste / billet
          var saisie = -1
          while (saisie < 0 || saisie > maxbillets) {
            println(s"\nIl reste $reste CHF à distribuer.")
            println(s"Vous pouvez obtenir au maximum $maxbillets billet(s) de $billet CHF.")
            print("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            val entree = readLine()
            if (entree == "o") {
              saisie = maxbillets
            }
            else {
              try {
                saisie = entree.toInt
                  } 
              catch {
                    case _: NumberFormatException => saisie = -1

                }
              }
            }
            if (saisie > 0) {
              val montantDistribue = saisie * billet
              reste -= montantDistribue
              distributionBillets = distributionBillets :+ (saisie, billet)
            }
    }
  }
  }
    if (coupures == 2){for (x <- petitsbillets.indices) {
            val billet = petitsbillets (x)
            if (reste >= billet){ 
            val maxbillets: Int = reste / billet
            var saisie = -1
            while (saisie < 0 || saisie > maxbillets) {
              println(s"\nIl reste $reste CHF à distribuer.")
              println(s"Vous pouvez obtenir au maximum $maxbillets billet(s) de $billet CHF.")
              print("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
              val entree = readLine()
              if (entree == "o") {
                saisie = maxbillets
              } 
              else {
                try {
                  saisie = entree.toInt
                    } 
                catch {
                  case _: NumberFormatException => saisie = -1 // éviter une erreur de tape
                }
              }
            }
            if (saisie > 0) {
              val montantDistribue = saisie * billet
              reste -= montantDistribue
              distributionBillets = distributionBillets :+ (saisie, billet)
            }
          }
        }  
      }

    }
    if (deviseR == 1 && montantr < 200){
    for (x <- petitsbillets.indices) {
      val billet = petitsbillets (x)
      if (reste >= billet){ 
        val maxbillets: Int = reste / billet
        var saisie = -1
        while (saisie < 0 || saisie > maxbillets) {
          println(s"\nIl reste $reste CHF à distribuer.")
          println(s"Vous pouvez obtenir au maximum $maxbillets billet(s) de $billet CHF.")
          print("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
          val entree = readLine()
            if (entree == "o") {
              saisie = maxbillets
          }
            else {
              try {
                saisie = entree.toInt
              } 
                catch {
                  case _: NumberFormatException => saisie = -1 // éviter une erreur de tape
              }
            }
          }

        if (saisie > 0) {
          val montantDistribue = saisie * billet
          reste -= montantDistribue
          distributionBillets = distributionBillets :+ (saisie, billet)
          }
        }
      } 
    }
    if (deviseR == 2){
        for (x <- petitsbillets.indices) {
          val billet = petitsbillets (x)
          if (reste >= billet){ 
            val maxbillets: Int = reste / billet
            var saisie = -1
            while (saisie < 0 || saisie > maxbillets) {
            println(s"\nIl reste $reste EUR à distribuer.")
            println(s"Vous pouvez obtenir au maximum $maxbillets billet(s) de $billet EUR.")
            print("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            val entree = readLine()
            if (entree == "o") {
              saisie = maxbillets
            } 
              else {
                  try {
                    saisie = entree.toInt
              } 
                catch {
                      case _: NumberFormatException => saisie = -1 // éviter une erreur de tape
                    }
                  }
                }
          if (saisie > 0) {
            val montantDistribue = saisie * billet
            reste -= montantDistribue
            distributionBillets = distributionBillets :+ (saisie, billet)
          }
        }
      } 
    }
    if (distributionBillets.nonEmpty) {
      println("\nVeuillez retirer la somme demandée :")
      for ((nombre, valeur) <- distributionBillets) {
        println(s"$nombre billet(s) de $valeur EUR.")
    }
    }
    if (reste > 0 && deviseR == 1){
    var reste10 = reste / 10
      if (reste10 != 0) {
      println(s"$reste10 billet(s) de 10 CHF.")
      }
    }
    else if (reste > 0 && deviseR == 2){
      var reste10 = reste / 10
      if (reste10 != 0) {
      println(s"$reste10 billet(s) de 10 EUR.")
    }
    if (deviseR == 2){
     comptes(IDclient) -= montantr * 0.95
    } else {
    comptes(IDclient) -= montantr
    }
    println("\nVotre retrait a été pris en compte, le nouveaux montant disponible sur votre compte est de " + comptes(IDclient) + "CHF.")
    } // retrait def
  }



  //changement de pin

      def changepin(IDclient : Int, codespin : Array[String]) : Unit = {
        var nouveauPin = "1"
        while (nouveauPin.length < 8){
          print("Saisissez votre nouveau code pin ( il doit contenir au moins 8 caractères > ")
          nouveauPin = readLine()
          if (nouveauPin.length < 8) {
            print("Votre code pin ne contient pas au moins 8 caractères.")
          }
        }
        codespin(IDclient) = nouveauPin
    }
  
  def main(args: Array[String]): Unit = {
    var comptes = Array.fill(101) (1200.0)
    var codespin = Array.fill(101) ("INTRO1234")
    var nbclients = 100
    var PinOk = true
    var Essai = 3
    var IDincorrect = false
    var Choix = 5
    var terminer = 5
    var IDclient = 0
   
  while (Choix == 5) {
     IDincorrect = false
    PinOk = true
    while (!IDincorrect) {
    print("Saisissez votre code identifiant > ")
     IDclient = readInt()
      if(IDclient >= nbclients) {
        println("Cet identifiant n’est pas valable.")
        IDincorrect = true
      }
      Essai = 3
      while (Essai > 0 && PinOk && !IDincorrect) {
      print("\nSaisissez votre code pin > ")
      var Pin = readLine()
        if(Pin == codespin(IDclient)) {
            PinOk = false
          IDincorrect = true
        } else {
          Essai -= 1
          if(Essai == 0) {
            println("\nTrop d’erreurs, abandon de l’identification.\n")
          }
          else {
            println(s"\nCode pin erroné, il vous reste $Essai tentatives.")
            
               }
        }
        
    }
  } // While IDincorrect
    Choix = 0
    while (Choix != terminer && !PinOk) {
      println("\nChoisissez votre opération: \n")
      Choix = readLine("1. Dépôt \n2. Retrait\n3. Consultation du compte \n4. Changement du code pin\n5. Terminer\n\nVotre choix: ").toInt
      if (Choix == 1) {
        depot(IDclient,comptes)
      }
      else if (Choix == 2) {
        retrait(IDclient,comptes)
      }
      else if (Choix == 3) {
        println("\nLe montant disponible sur  votre compte est de "+ comptes(IDclient)+ "CHF.")
      }
      else if (Choix == 4) {
        changepin(IDclient,codespin)
      }
    }
  }
  }


}