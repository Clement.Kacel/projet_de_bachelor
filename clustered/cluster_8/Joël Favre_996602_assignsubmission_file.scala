//Assignment: Joël Favre_996602_assignsubmission_file

//Initialisation des fonctions utilisées
import scala.io.StdIn.readDouble
import scala.io.StdIn.readInt
import scala.io.StdIn.readChar
import scala.io.StdIn.readLine

object Main {
  //Initialisation des variables
  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0) //Tableau des soldes de comptes
  val codespin = Array.fill(nbclients)("INTRO1234") //Tableau des pin des utilisateurs
  var id = -1 //Identifiant du client
  var pin_entré = ""
  var reset = 0
  var depot = 0.0
  var retrait = 0.0
  var pin_reset = 0
  var pin_ok = 0
  var operation = 0 
  var devise = 0
  var monnaie = "CHF/EUR"
  var devise_valide = 0 //On crée une variable pour vérifier si la devise est valide
  var depot_valide = 0 //On crée une variable pour vérifier si le dépôt est valide
  var retrait_valide = 0
  var retrait_virtuel = 0.0 // Valeur de retrait utilisé pour simuler les retraits et faciliter le comptage des coupures
  var taille_coupures = 0
  var nombre_billetsStr = "S"
  var nombre_billetsInt = 0
  var saisie_ok = 0
  //Variables pour le nombre de coupures
  var coupure_500 = 0
  var coupure_200 = 0
  var coupure_100 = 0
  var coupure_50 = 0
  var coupure_20 = 0
  var coupure_10 = 0

  // Fonction d'opération de dépôt
  def depot(id : Int, comptes : Array[Double]) : Unit ={
    while (devise_valide != 1){
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
      devise = readInt()
      if (devise == 1 || devise == 2){
      devise_valide = 1}}
    while (depot_valide == 0 ){
    println("Indiquez le montant du dépôt >  ")
    depot = readDouble()
    if (depot / 10 == (depot / 10).toInt && depot >=10){ //On regarde si la valeur entrée est un multiple de 10 et plus grand ou égal à 10. 
      if (devise == 1){// Si on dépose des CHF
        comptes(id) = comptes(id) + depot}
      if (devise == 2){
        comptes(id) = comptes(id) + depot * 0.95
      }
      depot_valide = 1} // Le montant du dépôt est validé
    else println ("Le montant doit être un multiple de 10.")
      }
    depot_valide = 0 
    devise_valide = 0
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
  }// Fin du dépôt

  //Fonction de retrait
  def retrait(id : Int, comptes : Array[Double]) : Unit ={
    while (devise_valide != 1){
      println("Indiquez la devise du retrait : 1) CHF, 2) : EUR > ")
      devise = readInt()
      if (devise == 1 || devise == 2){
      devise_valide = 1
      if (devise == 1){monnaie = "CHF"}
      else {monnaie = "EUR"}
      }}
    while (retrait_valide == 0 ){
    println("Indiquez le montant du retrait >  ")
    retrait = readDouble()
    if (retrait / 10 == (retrait / 10).toInt && retrait >=10 && retrait <= 0.1*comptes(id)){ //On regarde si la valeur entrée est un multiple de 10 et plus grand ou égal à 10. 
      retrait_virtuel = retrait 

        if (retrait >= 200 && devise == 1){//On vérifie si le montant est suffisament inportant pour des grosses coupures, sinon on ne demande pas, uniquement pour les CHF
        while (taille_coupures == 0){
          println("En 1) grosses coupures, 2) petites coupures > ")
          taille_coupures = readInt()
          if (taille_coupures == 1 || taille_coupures == 2){}
          else {taille_coupures = 0}
        }}
          if (retrait_virtuel >=500 && taille_coupures == 1){//Retrait grosses coupures
          coupure_500 = (retrait_virtuel / 500).toInt //Retrait billets de 500
          println ("Il reste " + retrait_virtuel + " CHF à d-istribuer")
          println("Vous pouvez obtenir au maximum "+ coupure_500 +" billet(s) de 500 CHF")
          while (saisie_ok == 0){//On vérifie que la saisie est valide
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          nombre_billetsStr = readLine()
          if (nombre_billetsStr == "o"){saisie_ok = 1} // On regarde si la proposition est validée
          else if (nombre_billetsStr.toInt < coupure_500){saisie_ok = 1} // Si un nombre est proposé on vérifie qu'il soit strictement plus petit que la valeur proposée
          }
          saisie_ok = 0 // On réinitialise la valeur
          if (nombre_billetsStr == "o"){
            retrait_virtuel = retrait_virtuel - 500 * coupure_500}
          else {
            nombre_billetsInt = nombre_billetsStr.toInt
            retrait_virtuel = retrait_virtuel - 500 * nombre_billetsInt
            coupure_500 = nombre_billetsInt}
          }

          if (retrait_virtuel >=200 && taille_coupures == 1){
          coupure_200 = (retrait_virtuel / 200).toInt //Retrait billets de 200
          println ("Il reste " + retrait_virtuel + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum "+ coupure_200 +" billet(s) de 200 CHF")
            while (saisie_ok == 0){//On vérifie que la saisie est valide
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              nombre_billetsStr = readLine()
              if (nombre_billetsStr == "o"){saisie_ok = 1} // On regarde si la proposition est validée
              else if (nombre_billetsStr.toInt < coupure_200){saisie_ok = 1} // Si un nombre est proposé on vérifie qu'il soit strictement plus petit que la valeur proposée
              }
              saisie_ok = 0 // On réinitialise la valeur
          if (nombre_billetsStr == "o"){
            retrait_virtuel = retrait_virtuel - 200 * coupure_200}
          else {
            nombre_billetsInt = nombre_billetsStr.toInt
            retrait_virtuel = retrait_virtuel - 200 * nombre_billetsInt
            coupure_200 = nombre_billetsInt}
          }

          if (retrait_virtuel >=100){//Retrait petites coupures
          coupure_100 = (retrait_virtuel / 100).toInt //Retrait billets de 100
          println ("Il reste " + retrait_virtuel + monnaie +" à distribuer")
          println("Vous pouvez obtenir au maximum "+ coupure_100 +" billet(s) de 100 " + monnaie)
            while (saisie_ok == 0){//On vérifie que la saisie est valide
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              nombre_billetsStr = readLine()
              if (nombre_billetsStr == "o"){saisie_ok = 1} // On regarde si la proposition est validée
              else if (nombre_billetsStr.toInt < coupure_100){saisie_ok = 1} // Si un nombre est proposé on vérifie qu'il soit strictement plus petit que la valeur proposée
              }
              saisie_ok = 0 // On réinitialise la valeur
            if (nombre_billetsStr == "o"){
            retrait_virtuel = retrait_virtuel - 100 * coupure_100}
            else {
            nombre_billetsInt = nombre_billetsStr.toInt
            retrait_virtuel = retrait_virtuel - 100 * nombre_billetsInt
            coupure_100 = nombre_billetsInt}
          }

            if (retrait_virtuel >=50){
            coupure_50 = (retrait_virtuel / 50).toInt //Retrait billets de 50
            println ("Il reste " + retrait_virtuel + monnaie +" à distribuer")
            println("Vous pouvez obtenir au maximum "+ coupure_50 +" billet(s) de 50 " + monnaie)
              while (saisie_ok == 0){//On vérifie que la saisie est valide
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                nombre_billetsStr = readLine()
                if (nombre_billetsStr == "o"){saisie_ok = 1} // On regarde si la proposition est validée
                else if (nombre_billetsStr.toInt < coupure_50){saisie_ok = 1} // Si un nombre est proposé on vérifie qu'il soit strictement plus petit que la valeur proposée
                }
                saisie_ok = 0 // On réinitialise la valeur
            if (nombre_billetsStr == "o"){
              retrait_virtuel = retrait_virtuel - 50 * coupure_50}
            else {
              nombre_billetsInt = nombre_billetsStr.toInt
              retrait_virtuel = retrait_virtuel - 50 * nombre_billetsInt
              coupure_50 = nombre_billetsInt}
            }

            if (retrait_virtuel >=20){
            coupure_20 = (retrait_virtuel / 20).toInt //Retrait billets de 20
            println ("Il reste " + retrait_virtuel + monnaie +" à distribuer")
            println("Vous pouvez obtenir au maximum "+ coupure_20 +" billet(s) de 20 " + monnaie)
              while (saisie_ok == 0){//On vérifie que la saisie est valide
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                nombre_billetsStr = readLine()
                if (nombre_billetsStr == "o"){saisie_ok = 1} // On regarde si la proposition est validée
                else if (nombre_billetsStr.toInt < coupure_20){saisie_ok = 1} // Si un nombre est proposé on vérifie qu'il soit strictement plus petit que la valeur proposée
                }
                saisie_ok = 0 // On réinitialise la valeur
            if (nombre_billetsStr == "o"){
              retrait_virtuel = retrait_virtuel - 20 * coupure_20}
            else {
              nombre_billetsInt = nombre_billetsStr.toInt
              retrait_virtuel = retrait_virtuel - 20 * nombre_billetsInt
              coupure_20 = nombre_billetsInt}
            }

            if (retrait_virtuel >=10){
            coupure_10 = (retrait_virtuel / 10).toInt //Retrait billets de 10
            println ("Il reste " + retrait_virtuel + monnaie +" à distribuer")
            println("Vous pouvez obtenir au maximum "+ coupure_10 +" billet(s) de 10 " + monnaie)
            println("Cette coupure étant la plus petite elle va maintenant vous être distribuée.")} //L'utilisateur n'ayant pas choisi les plus grosses coupures est informé qu'il recevra le reste en coupure de 10.

        println("Veuillez retirer la somme demandée :")
        //On évite d'afficher les nombres de billets lorsque rien n'a été retiré
        if (coupure_500 > 0){printf ("%d billet(s) de 500 CHF\n", coupure_500)}
        if (coupure_200 > 0){printf ("%d billet(s) de 200 CHF\n", coupure_200)}
        if (coupure_100 > 0){printf ("%d billet(s) de 100 %s\n", coupure_100, monnaie)}
        if (coupure_50 > 0){printf ("%d billet(s) de 50 %s\n", coupure_50, monnaie)}
        if (coupure_20 > 0){printf ("%d billet(s) de 20 %s\n", coupure_20, monnaie)}
        if (coupure_10 > 0){printf ("%d billet(s) de 10 %s\n", coupure_10, monnaie)}
        if(devise == 1){comptes(id) = comptes(id) - retrait}//Si on retire des CHF
        else {comptes(id) = comptes(id) - retrait * 0.95}//Si on retire des EUR
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

      retrait_valide = 1} 
    else if (retrait > 0.1*comptes(id)){//On check si le montant demandé n'est pas trop grand
      printf("Votre plafond de retrait est de : %.2f \n", 0.1*comptes(id))} 
    else println ("Le montant doit être un multiple de 10.")
      }
    //Réinitialisation des variables
    taille_coupures = 0
    retrait_valide = 0 
    devise_valide = 0
    coupure_500 = 0
    coupure_200 = 0
    coupure_100 = 0
    coupure_50 = 0
    coupure_20 = 0
    coupure_10 = 0
  }//Fin de la fonction de retrait

  //Fonction de changement de pin
  def changepin(id : Int, codespin : Array[String]) : Unit ={
    pin_entré = ""
    while(pin_entré.length < 8){
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      pin_entré = readLine()
      if(pin_entré.length() < 8){
      println("Votre code pin ne contient pas au moins 8 caractères ")}
      else {codespin(id) = pin_entré}}
  }//Fin du changement de pin

  //Fonction principale pour l'exécution globale du programme
  def main(args: Array[String]): Unit = {

    // Bloc pour le pin et l'identification de l'utilisateur
    while (1==1) {//On reste toujours dans la boucle principale, cette condition est toujours vraie
      reset = 0
      id = -1
      pin_reset = 0
      pin_ok = 0
      operation = 0
      println("Saisissez votre code identifiant > " )
       id = readInt()
      if (id >= nbclients || id < 0) {
       println("Cet identifiant n’est pas valable.") 
      reset = 1}
      if (reset == 0){
          while(pin_ok == 0 && pin_reset < 3){
          println("Saisissez votre code pin > ")
          pin_entré = readLine()
          if (pin_entré == codespin(id)) {pin_ok = 1
              pin_entré = ""}
          else {pin_reset = pin_reset + 1
            println ("Code pin erroné, il vous reste " + (3-pin_reset) + " tentatives")
               if (pin_reset == 3){println("Trop d’erreurs, abandon de l’identification")}}
          }}

 //Bloc principal demandant les actions de l'utilisateur
while (operation !=5 && pin_reset != 3 && reset == 0){//On sortira dès que l'utilisateur le souhaite
      println("Choisissez votre opération:")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix: ")
      operation = readInt() //Lecture du choix de l'utilisateur
  // Execution de la demande de l'utilisateur

  //Dépôt
  if (operation == 1){
    depot(id, comptes)
  }//Fin du dépôt

  // Retrait
  if (operation == 2){
    retrait(id, comptes)
  }// Fin du retrait

  // Consultation du compte
  if (operation == 3){
    printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))} 
   //On affiche le solde du compte avec 2 chiffres après la virgule

  //Changement du code pin
  if(operation == 4){
    changepin(id, codespin)
  }// Fin du changement de pin

  //Operation de fin
  if(operation == 5){println("Fin des opérations, n’oubliez pas de récupérer votre carte.")}

}// Fin de la boucle principale pour les opérations
}// Fin de la boucle infinie 1 == 1
}// Fin de main(args: Array[String]
}// Fin de objet main