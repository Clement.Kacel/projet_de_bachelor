//Assignment: Cloe Christiane Liliana Cassano_996554_assignsubmission_file

import scala.io.StdIn._

object Main {
  def main(args: Array[String]): Unit = {
    val nbClients = 100
    val montantInitial: Double = 1200.0
    val codePinInitial = "INTRO1234"
    var operation = 0
    var pin = ""

    //affichage
    var comptes = Array.fill(nbClients)(1200.0)
    var codespin = Array.fill(nbClients)(codePinInitial)
    var montantDisponible = montantInitial

    //identification
    var id = -1
    var nbTentatives = 3
    var pinCorrect = false
    var pinTentatives = ""
    var choix = 0
    
      while (true) {
        id = readLine("Saisissez votre code identifiant > ").toInt

        if (id < 0 || id >= nbClients) {
          println("Cet identifiant n’est pas valable.")
          System.exit(1)
        } else {
          var essaie = 3

          while (pin != codePinInitial && essaie > 0) {
            pin = readLine("Saisissez votre code pin >")
            essaie -= 1

            if (pin != codePinInitial && essaie > 0) {
              println("Code pin erroné, il vous reste " + essaie + " tentatives.")
            }
          }

          if (pin != codePinInitial) {
            println("Trop d’erreurs, abandon de l’identification.")
          
          } else {
            while (operation != 5) {
              println("Choisissez votre opération:")
              println(" 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer")
              print("Votre choix: ")
              operation = readLine().toInt

              if (operation == 1) {
               depot(id, comptes)
              }           
              else if (operation == 2) {
               retrait(id, comptes)
              }           
              else if (operation == 3) {
                consultation() 
              }              
              else if (operation == 4) {
                changepin(id, codespin)
              }                
               /*
              else if (operation == 5) {
                terminer(id, comptes)
              }   */
              
            }
          }
        }

     // Opération de dépôt 1
            def depot(id: Int, comptes: Array[Double]): Unit = {
              var montantDepot = 0.0
              var montantDevise = 0
              var verif = 1 

           while ((verif != 0) || (montantDepot < 10)) {
                println("Indiquez la devise du dépôt: 1) CHF ; 2) EUR > ")
                montantDevise = readLine().toInt
                println("Indiquez le montant du dépôt (doit être un multiple de 10) > ")
                montantDepot = readLine().toDouble

            while (montantDepot % 10 != 0) {
                  println("Le montant doit être un multiple de 10.")
                  println("Indiquez le montant du dépôt (doit être un multiple de 10) > ")
                  montantDepot = readLine().toDouble
                }
                verif = (montantDepot % 10).toInt
              }

    // Conversion du montant EUR en CHF
              // 1EUR = 0.95CHF
          var montantRetrait = 0.0
            if (montantDevise == 2) {
            montantRetrait += (montantDepot * 0.05)
              } else if (montantDevise == 1) {
              montantRetrait += montantDepot
              }
              montantDisponible += montantRetrait

    // Maj du montant du compte
           montantDisponible = math.floor(montantDisponible * 100) / 100
           montantDisponible += montantDepot
          printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte en banque est de: %.2f \n", montantDisponible)
            } 
    
    // Opération de retrait 2
    def retrait(id: Int, comptes: Array[Double]): Unit = {
      var devise2 = readLine("Indiquez la devise: 1) CHF 2) EUR >").toInt
      while (devise2 != 1 && devise2 != 2) {
        devise2 = readLine("Indiquez la devise: 1) CHF 2) EUR (choisir 1 ou 2) >").toInt
      }
      var montantRetrait = 0.0
      print("Indiquez le montant du retrait (doit être un multiple de 10) > ")
      montantRetrait = readLine().toDouble

      while (montantRetrait % 10 != 0 || montantRetrait > montantDisponible * 0.1) {
        if (montantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
          montantRetrait = readLine("Indiquez le montant du retrait (doit être un multiple de 10) >").toDouble
        } else if (montantRetrait > montantDisponible * 0.1) {
          println("Votre plafond de retrait autorisé est de :" + montantDisponible * 0.1 + "CHF")
          montantRetrait = readLine("Indiquez le montant du retrait (doit être un multiple de 10) > ").toDouble
        }
      }
       
      if (devise2 == 1) {
        var billet1500 = 0
        var reste1500 = 0
        var billet1200 = 0
        var reste1200 = 0
        var billet1100 = 0
        var reste1100 = 0
        var billet150 = 0
        var reste150 = 0
        var billet110 = 0
        var reste110 = 0

        // Choix coupures 1) ou 2)
        var coupure = 0
        println("Choisissez votre coupure: 1) Grosses coupures ; 2) Petites coupures > ")
        coupure = readLine().toInt

        // CHF
        if (coupure == 1) {
          billet1500 = (montantRetrait / 500).toInt
          reste1500 = (montantRetrait % 500).toInt
          if (billet1500 > 0) {
            println("Vous pouvez obtenir au maximum " + billet1500 + " billet(s) de 500 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          billet1200 = reste1500 / 200
          reste1200 = reste1500 % 200
          if (billet1200 > 0) {
            println("Vous pouvez obtenir au maximum " + billet1200 + " billet(s) de 200 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }
          billet1100 = reste1200 / 100
          reste1100 = reste1200 % 100
          if (billet1100 > 0) {
            println("Vous pouvez obtenir au maximum " + billet1100 + " billet(s) de 100 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          billet150 = reste1100 / 50
          reste150 = reste1100 % 50
          if (billet150 > 0) {
            println("Vous pouvez obtenir au maximum " + billet150 + " billet(s) de 50 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          billet110 = reste150 / 20
          reste110 = reste150 % 20
          if (billet110 > 0) {
            println("Vous pouvez obtenir au maximum " + billet110 + " billet(s) de 20 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

        // EUR
        } else if (coupure == 2) {
          var billet2100 = montantRetrait / 100
          var reste2100 = montantRetrait % 100
          if (billet2100 > 0) {
            println("Vous pouvez obtenir au maximum " + billet2100 + " billet(s) de 100 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          var billet250 = reste2100 / 50
          var reste250 = reste2100 % 50
          if (billet250 > 0) {
            println("Vous pouvez obtenir au maximum " + billet250 + " billet(s) de 50 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          var billet220 = reste250 / 20
          var reste220 = reste250 % 20
          if (billet220 > 0) {
            println("Vous pouvez obtenir au maximum " + billet220 + " billet(s) de 20 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }

          var billet210 = reste220 / 10
          var reste210 = reste220 % 10
          if (billet210 > 0) {
            println("Vous pouvez obtenir au maximum " + billet210 + " billet(s) de 10 CHF")
            println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
          }
        }

        // Devise EUR <200
      } else if (montantRetrait < 200) {
        var billet3100 = montantRetrait / 100
        var reste3100 = montantRetrait % 100
        if (billet3100 > 0) {
          println("Vous pouvez obtenir au maximum " + billet3100 + " billet(s) de 100 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        var billet350 = reste3100 / 50
        var reste350 = reste3100 % 50
        if (billet350 > 0) {
          println("Vous pouvez obtenir au maximum " + billet350 + " billet(s) de 50 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        var billet320 = reste350 / 20
        var reste320 = reste350 % 20
        if (billet320 > 0) {
          println("Vous pouvez obtenir au maximum " + billet320 + " billet(s) de 20 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        var billet310 = reste320 / 10
        var reste310 = reste320 % 10
        if (billet310 > 0) {
          println("Vous pouvez obtenir au maximum " + billet310 + " billet(s) de 10 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        // Petites Coupures 2
      } else if (devise2 == 2) {
        var billet4100 = montantRetrait / 100
        var reste4100 = montantRetrait % 100
        if (billet4100 > 0) {
          println("Vous pouvez obtenir au maximum " + billet4100 + " billet(s) de 100 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        var billet450 = reste4100 / 50
        var reste450 = reste4100 % 50
        if (billet450 > 0) {
          println("Vous pouvez obtenir au maximum " + billet450 + " billet(s) de 50 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        var billet420 = reste450 / 20
        var reste420 = reste450 % 20
        if (billet420 > 0) {
          println("Vous pouvez obtenir au maximum " + billet420 + " billet(s) de 20 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        var billet410 = reste420 / 10
        var reste410 = reste420 % 10
        if (billet410 > 0) {
          println("Vous pouvez obtenir au maximum " + billet410 + " billet(s) de 10 CHF")
          println("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
        }

        // Conversion
        var retraitEUR = montantRetrait * 0.95
        montantDisponible = (retraitEUR).toInt
        montantDisponible = (math.floor(montantDisponible * 100) / 100).toInt

        printf(" Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f CHF \n", montantDisponible)
      }

      // MAJ montant disponible
      montantDisponible -= montantRetrait
      montantDisponible = math.floor(montantDisponible * 100) / 100

      printf(" Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte en banque est de: %.2f \n", montantDisponible)
     
      retrait(id, comptes)
    }

    // Opération de consultation 3
    def consultation(): Unit = {
      if (pin == "INTRO1234" && operation == 3) {
        montantDisponible = math.floor(montantDisponible * 100) / 100
        println("Le montant disponible sur votre compte est de: " + montantDisponible)
      }
    }

    // Opération de changement du code PIN 4
    def changepin(id: Int, codespin: Array[String]): Unit = {
      var nouveauPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      
          while (nouveauPin.length < 8) {
           println("Votre code pin ne contient pas au moins 8 caractères")
            nouveauPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
          }      
            codespin(id) = nouveauPin
            println("Votre code pin a été modifié avec succès")
      }

         // Fin des opérations 5
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.")        
        
     }
  }
}
