//Assignment: Gaëlan Chérif_996511_assignsubmission_file

import scala.collection.mutable.ArrayBuffer
import io.StdIn._

object Main {
    
    def idAuth(id: Int, nbClients: Int, codespin: Array[String]): Boolean = {
        if(id >= 0 && id < nbClients) {
        return true
        } else {
        return false
        }
    }

    def pinAuth(code: String, id: Int, codespin: Array[String]): Boolean = {
        if(code == codespin(id)) {
        return true
        } else {
        return false
        }
    }

    def login(nbClients: Int, codespin: Array[String], comptes: Array[Double]): Unit = {
        var id = readLine("saisissez votre code identifiant > ").toInt
        idAuth(id, nbClients, codespin)
        if(!(idAuth(id, nbClients, codespin))) {
            println("cet identifiant n'est pas valable.")
        }
        if(idAuth(id, nbClients, codespin)) {
            println("Identifiant bon")
            var attempts = 2
            var code = readLine("saisissez votre code pin > ")
            pinAuth(code, id, codespin)
            while(attempts > 0 && !(pinAuth(code, id, codespin))) {
                println(s"code incorrect, il vous reste $attempts tentative(s). ")
                code = readLine("saisissez votre code pin > ")
                pinAuth(code, id, codespin)
                attempts -= 1
            }
            if(codespin(id) == code) {
                println("Authentification réussie")
                menu(id, codespin, comptes)
            } else {
                println("Echec de l'authetification")
                while(!(pinAuth(code, id, codespin)) || !(idAuth(id, nbClients, codespin))) {
                    login(nbClients, codespin, comptes)
                }
            }
        }
    }

    def menu(id: Int = 0, codespin: Array[String], comptes: Array[Double]): Unit ={
        var choix = 0
        while(choix != 5){
            println("Choisissez votre opération :\n  1) Dépôt\n  2) Retrait\n  3) Consultation du compte\n  4) Changement du code pin\n  5) Terminer")
            choix = readLine("  Votre choix :  ").toInt
                while(choix < 1 || choix > 5){
                    choix = readLine("  Votre choix :  ").toInt
                    println("Choix invalide, veuillez recommencer.")
            }
            if(choix == 1){
                choix = 0
                depot(id, comptes)
            }
            if(choix == 2){
                choix = 0
                retrait(id, comptes)
            }
            if(choix == 3){
                choix = 0
                consultationSolde(id, comptes)
            }
            if(choix == 4){
                choix = 0
                changepin(id, codespin)
            }
        }
        println("Fin des opérations, n’oubliez pas de récupérer votre carte. \n")
    }

    def depot(id : Int, comptes : Array[Double]) : Unit = {
        var choix = 0
        while(choix != 1 && choix != 2){
            println("Dans quelle devise voulez-vous déposer de l'argent ? (la somme en EUR sera convertie en CHF)\n  1) EUR\n  2) CHF")
            choix = readLine("  Votre choix :  ").toInt
        }
        if(choix == 1){
            var devise = "EUR"
            println(s"Vous avez choisi $devise. \n")
            var montantDepot = EURtoCHFconvert(readLine("combien d'argent voulez-vous déposer > ").toDouble)
                while(!(montantDepot % 10 == 0)){
                println("montant invalide.")
                montantDepot = EURtoCHFconvert(readLine("combien d'argent voulez-vous déposer > ").toDouble)
            }
            var solde = comptes(id)
            println(s"Votre ancien solde : $solde CHF")
            comptes(id) = comptes(id) + montantDepot
            var nouveauSolde = comptes(id)
            println(s"\nVous avez déposé $montantDepot CHF et vous avez maintenant $nouveauSolde CHF sur votre compte.")
        }
        if(choix == 2){
            var devise = "CHF"
            println(s"Vous avez choisi $devise. \n")
            var montantDepot = readLine("combien d'argent voulez-vous déposer > ").toDouble
                while(!(montantDepot % 10 == 0)){
                println("montant invalide.")
                montantDepot = readLine("combien d'argent voulez-vous déposer > ").toDouble
            }
            var solde = comptes(id)
            println(s"Votre ancien solde : $solde CHF")
            comptes(id) = comptes(id) + montantDepot
            var nouveauSolde = comptes(id)
            println(s"\nVous avez déposé $montantDepot CHF et vous avez maintenant $nouveauSolde CHF sur votre compte.")
        }
    }

    def retrait(id : Int, comptes : Array[Double]) : Unit = {
        var choix = 0
        while(choix != 1 && choix != 2){
            println("Dans quelle devise voulez-vous retirer de l'argent ? (la somme en EUR sera convertie en CHF et vous recevrez des CHF)\n  1) EUR\n  2) CHF")
            choix = readLine("  Votre choix :  ").toInt
        }
        if(choix == 1){
            var devise = "EUR"
            println(s"Vous avez choisi $devise. \n")
            var montantRetrait = EURtoCHFconvert(readLine("combien d'argent voulez-vous retirer > ").toDouble)
                while(!(montantRetrait % 10 == 0) && comptes(id) - montantRetrait < 0){
                println("montant invalide.")
                montantRetrait = EURtoCHFconvert(readLine("combien d'argent voulez-vous retirer > ").toDouble)
            }
            var solde = comptes(id)
            println(s"Votre ancien solde : $solde CHF")
            comptes(id) = comptes(id) - montantRetrait
            var nouveauSolde = comptes(id)
            println(s"\nVous aller retirer $montantRetrait CHF et vous aurez alors $nouveauSolde CHF sur votre compte.")
            repartirBillets(montantRetrait, devise)
        }
        if(choix == 2){
            var devise = "CHF"
            println(s"Vous avez choisi $devise. \n")
            var montantRetrait = readLine("combien d'argent voulez-vous retirer > ").toDouble
                while(!(montantRetrait % 10 == 0) && comptes(id) - montantRetrait < 0){
                println("montant invalide.")
                montantRetrait = readLine("combien d'argent voulez-vous retirer > ").toDouble
            }
            var solde = comptes(id)
            println(s"Votre ancien solde : $solde CHF")
            comptes(id) = comptes(id) - montantRetrait
            var nouveauSolde = comptes(id)
            println(s"\nVous aller retirer $montantRetrait CHF et vous aurez alors $nouveauSolde CHF sur votre compte.")
            repartirBillets(montantRetrait, devise)
        }
    }

    def repartirBillets(montant: Double, devise: String): Unit = {
        var total = 0
        var billet500 = 0
        var billet200 = 0
        var billet100 = 0
        var billet50 = 0
        var billet20 = 0
        var billet10 = 0
        var billet5 = 0

        var nb_billet = 0
        var nb_billet500 = 0
        var nb_billet200 = 0
        var nb_billet100 = 0
        var nb_billet50 = 0
        var nb_billet20 = 0
        var nb_billet10 = 0
        var nb_billet5 = 0
        var nb_billet5supp = 0

        var retrait = montant
        var retrait1 = retrait
        
        println("> Comment voulez-vous répartir les billets ?")

        if(retrait1 >= 500) {
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet500 = readLine(s"> Combien de billets de 500 $devise ?").toInt
            while(nb_billet500 * 500 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet500 = readInt()
            }
            retrait1 = retrait1 - nb_billet500 * 500
            total = total + nb_billet500 * 500
        }
        if(retrait1 >= 200) {
            println(s"> Combien de billets de 200 $devise ?")
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet200 = readInt()
            while(nb_billet200 * 200 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet200 = readInt()
            }
            retrait1 = retrait1 - nb_billet200 * 200
            total = total + nb_billet200 * 200
        }
        if(retrait1 >= 100) {
            println(s"> Combien de billets de 100 $devise ?")
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet100 = readInt()
            while(nb_billet100 * 100 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet100 = readInt()
            }
            retrait1 = retrait1 - nb_billet100 * 100
            total = total + nb_billet100 * 100
        }
        if(retrait1 >= 50) {
            println(s"> Combien de billets de 50 $devise ?")
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet50 = readInt()
            while(nb_billet50 * 50 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet50 = readInt()
            }
            retrait1 = retrait1 - nb_billet50 * 50
            total = total + nb_billet50 * 50
        }
        if(retrait1 >= 20) {
            println(s"> Combien de billets de 20 $devise ?")
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet20 = readInt()
            while(nb_billet20 * 20 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet20 = readInt()
            }
            retrait1 = retrait1 - nb_billet20 * 20
            total = total + nb_billet20 * 20
        }
        if(retrait1 >= 10) {
            println(s"> Combien de billets de 10 $devise ?")
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet10 = readInt()
            while(nb_billet10 * 10 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet10 = readInt()
            }
            retrait1 = retrait1 - nb_billet10 * 10
            total = total + nb_billet10 * 10
        }
        if(retrait1 >= 5) {
            println(s"> Combien de billets de 5 $devise ?")
            println(s"      > Vous devez répartir $retrait1 $devise.")
            nb_billet5 = readInt()
            while(nb_billet5 * 5 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet5 = readInt()
            }
            retrait1 = retrait1 - nb_billet5 * 5
            total = total + nb_billet5 * 5

            while(retrait - total >= 5) {
                println(s"> Vous pouvez encore répartir des billets de 5 $devise.")
                println(s"      > Il reste $retrait1 $devise à répartir.")
                nb_billet5supp = readInt()
                while(nb_billet5supp * 5 > retrait1) {
                println(s"- le montant est trop élevé, choisissez-en un autre")
                nb_billet5supp = readInt()
                }
                retrait1 = retrait1 - nb_billet5supp * 5
                total = total + nb_billet5supp * 5
            }
        }
        println("Votre choix: ")
        println(s"$nb_billet500 × 500 $devise")
        println(s"$nb_billet200 × 200 $devise")
        println(s"$nb_billet100 × 100 $devise")
        println(s"$nb_billet50 × 50 $devise ")
        println(s"$nb_billet20 × 20 $devise")
        println(s"$nb_billet10 × 10 $devise")
        println(s"$nb_billet5 × 5 $devise")
    }

    def EURtoCHFconvert(montant: Double): Double = {
        var tauxChange = 0.94
        return montant * tauxChange
    }

    def consultationSolde(id: Int, comptes: Array[Double]): Unit = {
        var solde = comptes(id)
        println(s"Votre solde est de $solde CHF")
    }

    def changepin(id : Int, codespin : Array[String]) : Unit ={
        var newpin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
        while(newpin.length < 8){
            println("Votre code pin ne contient pas au moins 8 caractères")
            newpin = readLine("Saisissez votre nouveau code pin > ")
        }
        codespin(id) = newpin
    }
  
    def main(args: Array[String]): Unit = {

        var comptes = Array[Double]()
        var codespin = Array[String]()
        var codepin: String = "0"
        val nbClients = 100
        
        for(i <- 0 to nbClients) {
        comptes = comptes :+ 1200.0
        codespin = codespin :+ "INTRO1234"
        }

        ////////////////////////
        /*var t = 0
        while(t <= nbClients) {
        println(comptes(t))
        println(codespin(t))
        println(t + "------------")
        t = t + 1
        }
        */
        ////////////////////////

        var loop = true
        while(loop){
            login(nbClients, codespin, comptes)
        }
        println("La machine est hors service.")
    }
}