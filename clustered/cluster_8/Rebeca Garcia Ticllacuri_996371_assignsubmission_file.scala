//Assignment: Rebeca Garcia Ticllacuri_996371_assignsubmission_file

import scala.io.StdIn._

object Main {
  import scala.io.StdIn.readLine
  import scala.io.StdIn.readInt
  import scala.io.StdIn.readDouble
  
  def main(args: Array[String]): Unit = {
    
    var nbclients = 100 
  
    // Tableau comptes
    var comptes = Array.fill[Double](nbclients)(1200.0)
  
    // Tableau codespin 
    var codespin = Array.fill[String](nbclients)("INTRO1234") 
  
    // Identification ID
    println("")
    var id = readLine("Saissisez votre code identifiant > ").toInt
      if (id > nbclients) {
        println("Cet identifiant n'est pas valable.")
        id = readLine("Saissisez votre code identifiant > ").toInt
      } 
  
    // Sasie du code pin 
    println("")
      var tentativePIN = 3
      var pin = readLine("Saisissez votre code pin > ")
        while (pin != codespin(id)) {
          tentativePIN -= 1

          println("") 

          println("Code pin erroné, il vous reste "+ tentativePIN + " tentatives >")
          
           println("")
          
          pin = readLine("Saisissez votre code pin > ")
             if (tentativePIN == 1){
               
              println("")
               
            println("Trop d’erreurs, abandon de l’identification.")
               
              println("")
               
            id = readLine("Saissisez votre code identifiant > ").toInt 
               
              println("")
               
            pin = readLine("Saisissez votre code pin > ")
            }
          } 
  
    // Changement CODE PIN
    def changepin(id : Int, codespin : Array[String]) : Unit = {
      codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      
      while (codespin(id).length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères")
        println("")
        codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >") 
      }
    }
    
    // CHOIX OPERATIONS
    
    println("")
    var choixOperation = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt

    // redemander si pas une des options 1 à 5
      while ((choixOperation != 1) && (choixOperation != 2) && (choixOperation != 3) && (choixOperation != 4) && (choixOperation != 5)){
  
        choixOperation = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt 
      }
   
    // DEPOT
    def depot(id : Int, comptes : Array[Double]) : Unit = {
        var devise = readLine("Indiquez la devise du dépôt : 1) CHF; 2) EUR > ").toInt
      
          while (devise != 1 && devise != 2){
            devise = readLine("Indiquez la devise du dépôt : 1) CHF;2) EUR > ").toInt
          }
      
          var depot = readLine("Indiquez le montant du dépôt > ").toInt
          var reste = depot % 10
            
          while (reste != 0) {
            depot = readLine("Le montant Le montant doit être un multiple de 10 > ").toInt
            reste = depot % 10
          }
    
        if (devise == 1) {
          comptes(id) += depot
        } else if (devise == 2) {
          comptes(id) += depot*0.95
        }
    
        comptes(id) = math.floor( comptes(id) * 100 ) /100
        println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id) + " CHF")
       println(" ")
      
    }

    // RETRAIT
    def retrait(id : Int, comptes : Array[Double]) : Unit = {
  
      var devise = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt
      
        while (devise != 1 && devise != 2){ 
          devise = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt
        }
      
      var retrait = readLine("Indiquez le montant du retrait > ").toInt
  
      var reste = retrait % 10
      var plafond = comptes(id) * 0.1
        while (reste != 0 ){
          println("Le montant doit être un multiple de 10")
          retrait = readLine("Indiquez le montant du retrait : ").toInt
          reste = retrait % 10
        }
  
      if (retrait > plafond){ 
        println("Votre plafond de retrait autorisé est de : " + plafond + " CHF. ")
        retrait = readLine("Indiquez le montant du retrait > ").toInt 
        println("")
      }
  
      // découpage des billets
        if (devise == 1){ // en CH
          
          if(retrait >= 200){
            var choixCoupure = readLine("1) grosses coupures, 2) petites coupures > ")
              if (choixCoupure == 1) {
                println("Veuillez retirer la somme demandée : ")

                var billet500 = retrait/500
                var reste500 = retrait % 500
                  if (billet500 != 0){
                    println(billet500 + " billet(s) de 500 CHF")
                  }

                var billet200 = reste500 / 200
                var reste200 = reste500 % 200
                  if (billet200 != 0){
                    println(billet200 + " billet(s) de 200 CHF")
                  }

                var billet100 = reste200 / 100
                var reste100 = reste200 % 100
                  if (billet100 != 0){
                    println(billet100 + " billet(s) de 100 CHF")
                  }

                var billet50 = reste100 / 50
                var reste50 = reste100 % 50
                  if (billet50 != 0) {
                    println(billet50 + " billet(s) de 50 CHF")
                  }

                var billet20 = reste50 / 20
                var reste20 = reste50 % 20
                  if (billet20 != 0){
                    println(billet20 + " billet(s) de 20 CHF")
                  }

                var billet10 = reste20 / 10
                var reste10 = reste20 % 10
                  if (billet10 != 0){
                    println(billet10 + " billet(s) de 10 CHF")
                  }
                } else if (choixCoupure == 2) { // petites coupures
                    var billet100 = retrait / 100
                    var reste100 = retrait % 100
                     if (billet100 != 0){
                      println(billet100 + " billet(s) de 100 CH")
                      }
                    var billet50 = reste100 / 50
                    var reste50 = reste100 % 50
                      if (billet50 != 0){
                        println(billet50 + " billet(s) de 50 CH")
                      }
                    var billet20 = reste50 / 20
                    var reste20 = reste50 % 20
                      if (billet20 != 0){
                        println(billet20 + " billet(s) de 20 CH")
                      }
                    var billet10 = reste20 / 10
                    var reste10 = reste20 % 10
                      if (billet10 != 0){
                        println(billet10 + " billet(s) de 10 CH")
                      }
                  } 
            
        } else if (retrait < 200){ // petites coupures 
            println("Veuillez retirer la somme demandée : ")
            var billet100 = retrait / 100
            var reste100 = retrait % 100
            if (billet100 != 0){
              println(billet100 + " billet(s) de 100 CHF")
            }

            var billet50 = reste100 / 50
            var reste50 = reste100 % 50
              if (billet50 != 0){
                println(billet50 + " billet(s) de 50 CHF")
              }
            
            var billet20 = reste50 / 20
            var reste20 = reste50 % 20
              if (billet20 != 0){
                println(billet20 + " billet(s) de 20 CHF")
              }

            var billet10 = reste20 / 10
            var reste10 = reste20 % 10
              if (billet10 != 0){
                println(billet10 + " billet(s) de 10 CHF")
              }  
            
          } 

          comptes(id) -= retrait
          comptes(id) = math.floor( comptes(id) * 100 ) /100
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f CHF ", (comptes(id)) ) 
          println("")
             

            } else if (devise == 2) {    // en EUR
                var billet100 = retrait / 100
                var reste100 = retrait % 100
                  if (billet100 != 0){
                    println( billet100 + " billet(s) de 100 EUR")
                  }

                   var billet50 = reste100 / 50
                   var reste50 = reste100 % 50
                     if (billet50 != 0){
                       println(billet50 + " billet(s) de 50 EUR")
                     }

                   var billet20 = reste50 / 20
                   var reste20 = reste50 % 20
                     if (billet20 != 0){
                       println(billet20 + " billet(s) de 20 EUR")
                     }

                   var billet10 = reste20 / 10
                   var reste10 = reste20 % 10
                     if (billet10 != 0){
                       println(billet10 + " billet(s) de 10 EUR")
                     }

                  comptes(id) -= retrait*0.95
                  comptes(id) = math.floor(comptes(id) * 100 ) /100
                    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f CHF ", (comptes(id))) 
                   println("")
          
                  } 
            } 

          // Opérations
    
        while ((choixOperation == 1) || (choixOperation == 2) || (choixOperation == 3) || (choixOperation == 4) || (choixOperation == 5)) { 
          // dépôt
          while (choixOperation == 1){
            depot(id, comptes)
    
            choixOperation = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
            }
            // retrait
            while (choixOperation == 2){
              retrait(id, comptes)
    
                choixOperation = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
              }
          
            // consultation du compte
          while (choixOperation == 3){
            println("")
            println("Le montant  "+  comptes(id) + " CHF")
            println("")
              choixOperation= readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
              }
          
          // changement code pin
          while (choixOperation == 4){
            changepin(id, codespin)
            println("")
              choixOperation = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt
            }
          
        // terminer
        while (choixOperation == 5) {
          println("")
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          println("")
          id = readLine("Saissisez votre code identifiant > ").toInt
    
          if (id > nbclients) {
            println("Cet identifiant n'est pas valable.")
            id = readLine("Saissisez votre code identifiant > ").toInt
          }
           println("")
          tentativePIN = 3
          pin = readLine("Saisissez votre code pin > ")
            while (pin != codespin(id)) {
              tentativePIN -= 1
              println("")
              println("Code pin erroné, il vous reste " + tentativePIN + " tentatives >")
              pin = readLine("Saisissez votre code pin > ")
                if (tentativePIN == 1) {
                   println("Trop d’erreurs, abandon de l’identification.")
                   id = readLine("Saissisez votre code identifiant > ").toInt 
                  pin = readLine("Saisissez votre code pin > ") 
                  }
            }
    
          println("")
          choixOperation = readLine( "Choisissez votre opération :\n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt 
          
           } 
    
      }
    }  
  }