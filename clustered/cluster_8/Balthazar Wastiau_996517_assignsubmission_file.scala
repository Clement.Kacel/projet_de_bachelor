//Assignment: Balthazar Wastiau_996517_assignsubmission_file

object Main {
  def main(args: Array[String]): Unit = {
    var essais = 0
    var exit = false
    var pinJuste = false
    var montant = 1200.0
    var choix = 0
    var pinEntre = ""
    val correctPin = "INTRO1234"
    var nbclients = 100
    var comptes: Array[Double] = Array.fill(nbclients)(1200.0)
    var codespin: Array[String] = Array.fill(nbclients)("INTRO1234")

    while (true) {
      essais = 0
      exit = false
      pinJuste = false
      println("Entrez votre code client >")
      var ID = scala.io.StdIn.readInt()
      while (ID > nbclients - 1) {
        println("Ce code client n'est pas valide.")
        System.exit(0)
      }
      while (essais < 3 && !pinJuste) {
        print("Saisissez votre code pin : ")
        pinEntre = scala.io.StdIn.readLine()
        essais += 1
        if (pinEntre == codespin(ID) ) pinJuste = true
        else {
          val essaisRestants = 3 - essais
          if (essaisRestants == 0) {
            exit = true
          }
          if (essaisRestants > 0) println(s"Code pin erroné, il vous reste $essaisRestants tentatives.")
        }
      }
      if (pinJuste) {
        while (!exit) {
        println("Choisissez votre opération :")
        println("   1)  Dépôt")
        println("   2)  Retrait")
        println("   3)  Consultation du compte")
        println("   4)  Changement du code pin")
        println("   5)  Terminer")
        print("Votre choix : ")
        choix = scala.io.StdIn.readInt()
        while (choix < 1 || choix > 5) {
          println("Choix invalide")
          print("Votre choix : ")
          choix = scala.io.StdIn.readInt()
        }
        //--------------------------------------------------dépot---------------------------------------------------
        if (choix == 1) {
          depot(ID, comptes)
          //----------------------------------------------------retrait------------------------------------------------
        } else if (choix == 2) {
          retrait(ID, comptes)
          //--------------------------------------------------consultation------------------------------------------------------
        } else if (choix == 3) {
          // Consultation du compte operation
          printf("Le montant disponible sur votre compte est de : %.2f CHF.\n", comptes(ID))
        }
        //--------------------------------------------------changement pin------------------------------------------------------
        else if (choix == 4) {
          changepin(ID, codespin)
        }
        //--------------------------------------------------fin------------------------------------------------------
        else if (choix == 5) {
          exit = true
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        }
        //--------------------------------------------------si pin faux ------------------------------------------------------
      }
      } else if (!pinJuste) {
        println("Trop d’erreurs, abandon de l’identification")
      }

    }
        //fin du if le code est juste ou deja rentré
    }






    def depot(id : Int, comptes : Array[Double]): Unit = {
      var deviseChoisie = 0

      while (deviseChoisie != 1 && deviseChoisie != 2) {
        print("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
        deviseChoisie = scala.io.StdIn.readInt()
      }

      val devise = if (deviseChoisie == 1) "CHF" else "EUR"
      println(s"Indiquez le montant du dépôt en $devise : ")

      var montantDepose = scala.io.StdIn.readDouble()
      while (montantDepose % 10 != 0 || montantDepose <= 0) {
        println("Le montant doit être un multiple de 10")
        println(s"Indiquez le montant du dépôt en $devise : ")
        montantDepose = scala.io.StdIn.readInt()
      }
      montantDepose =
        if (devise == "EUR") (montantDepose * 0.95)
        else montantDepose

      comptes(id) = comptes(id) + montantDepose

      println(s"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id) +"CHF.")

    }
    def retrait(id : Int, comptes : Array[Double]): Unit = {
      var deviseChoisie = 0

      while (deviseChoisie != 1 && deviseChoisie != 2) {
        print("Indiquez la devise : 1) CHF ; 2) EUR > ")
        deviseChoisie = scala.io.StdIn.readInt()
      }
      var devise = if (deviseChoisie == 1) "CHF" else "EUR"

      print(s"Indiquez le montant du retrait >")

      // Ensure the amount is a multiple of 10
      var montantRetrait = scala.io.StdIn.readInt()
      val limiteRetrait = if (devise == "CHF") {
        (comptes(id) * 0.1) - (comptes(id) * 0.1 % 10)
      } else {
        (comptes(id) * 1.05 * 0.1) - (comptes(id) * 1.05 * 0.1 % 10)
      } // 10% du montant disponible

      while (montantRetrait % 10 != 0 || montantRetrait > limiteRetrait) {
        if (montantRetrait % 10 != 0) println("Le montant doit être un multiple de 10.")
        if (montantRetrait > limiteRetrait) println(s"Votre plafond de retrait autorisé est de : $limiteRetrait $devise.")
        montantRetrait = scala.io.StdIn.readInt()
      }

      val montantConverti =
        if (devise == "EUR") montantRetrait * 0.95
        else montantRetrait

      var grosseCoupure = 0
      if (devise == "CHF" && montantConverti >= 200) {
        while (grosseCoupure != 1 && grosseCoupure != 2) {
          print("En 1) grosses coupures, 2) petites coupures >")
          grosseCoupure = scala.io.StdIn.readInt()
        }
      }
      //--------------------------------------------------distribution billet------------------------------------------------------
      var bill500 = 0
      var bill200 = 0
      var bill100 = 0
      var bill50 = 0
      var bill20 = 0
      var bill10 = 0
      while (montantRetrait != 0) {

        if (montantRetrait >= 500 && grosseCoupure == 1) {

          bill500 = (montantRetrait / 500)
          print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill500 billet(s) de 500 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          var EntreeClient = scala.io.StdIn.readLine()
          while (EntreeClient != "o" && (EntreeClient.toInt > bill500 || EntreeClient.toInt < 0)) {
            println("entrée invalide")
            print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill500 billet(s) de 500 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            EntreeClient = scala.io.StdIn.readLine()
          }
          if (EntreeClient == "o") {
            montantRetrait = montantRetrait % 500
          } else {
            bill500 = EntreeClient.toInt
            montantRetrait = montantRetrait - (bill500 * 500)
          }

        }
        if (montantRetrait >= 200 && grosseCoupure == 1) {
          bill200 = montantRetrait / 200
          print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill200 billet(s) de 200 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          var EntreeClient = scala.io.StdIn.readLine()
          while (EntreeClient != "o" && (EntreeClient.toInt > bill200 || EntreeClient.toInt < 0)) {
            println("entrée invalide")
            print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill200 billet(s) de 200 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            EntreeClient = scala.io.StdIn.readLine()
          }
          if (EntreeClient == "o") {
            montantRetrait = montantRetrait % 200
          } else {
            bill200 = EntreeClient.toInt
            montantRetrait = montantRetrait - (bill200 * 200)
          }
        }
        if (montantRetrait >= 100) {
          bill100 = montantRetrait / 100
          print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill100 billet(s) de 100 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          var EntreeClient = scala.io.StdIn.readLine()
          while (EntreeClient != "o" && (EntreeClient.toInt > bill100 || EntreeClient.toInt < 0)) {
            println("entrée invalide")
            print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill100 billet(s) de 100 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            EntreeClient = scala.io.StdIn.readLine()
          }
          if (EntreeClient == "o") {
            montantRetrait = montantRetrait % 100
          } else {
            bill100 = EntreeClient.toInt
            montantRetrait = montantRetrait - (bill100 * 100)
          }
        }
        if (montantRetrait >= 50) {
          bill50 = montantRetrait / 50
          print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill50 billet(s) de 50 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          var EntreeClient = scala.io.StdIn.readLine()
          while (EntreeClient != "o" && (EntreeClient.toInt > bill50 || EntreeClient.toInt < 0)) {
            println("entrée invalide")
            print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill50 billet(s) de 50 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            EntreeClient = scala.io.StdIn.readLine()
          }
          if (EntreeClient == "o") {
            montantRetrait = montantRetrait % 50
          } else {
            bill50 = EntreeClient.toInt
            montantRetrait = montantRetrait - (bill50 * 50)
          }
        }
        if (montantRetrait >= 20) {
          bill20 = montantRetrait / 20
          print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill20 billet(s) de 20 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          var EntreeClient = scala.io.StdIn.readLine()
          while (EntreeClient != "o" && (EntreeClient.toInt > bill50 || EntreeClient.toInt < 0)) {
            println("entrée invalide")
            print(s"Il reste $montantRetrait $devise à distribuer\nVous pouvez obtenir au maximum $bill20 billet(s) de 20 $devise\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            EntreeClient = scala.io.StdIn.readLine()
          }
          if (EntreeClient == "o") {
            montantRetrait = montantRetrait % 20
          } else {
            bill20 = EntreeClient.toInt
            montantRetrait = montantRetrait - (bill20 * 20)
          }
        }
        if (montantRetrait >= 10) {
          bill10 = montantRetrait / 10
          println(s"Il reste $montantRetrait $devise à distribuer\n Vous pouvez obtenir au maximum $bill10 billet(s) de 10 $devise")
          montantRetrait = montantRetrait % 10
        }
      }
      println("Veuillez retirer la somme demandée :")
      if (bill500 != 0) println(s"$bill500 billet(s) de 500 $devise")
      if (bill200 != 0) println(s"$bill200 billet(s) de 200 $devise")
      if (bill100 != 0) println(s"$bill100 billet(s) de 100 $devise")
      if (bill50 != 0) println(s"$bill50 billet(s) de 50 $devise")
      if (bill20 != 0) println(s"$bill20 billet(s) de 20 $devise")
      if (bill10 != 0) println(s"$bill10 billet(s) de 10 $devise")


      // Update account balance
      comptes(id) -= montantConverti

      // Display the new account balance
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", comptes(id))

    }
    def changepin(id : Int, codespin : Array[String]): Unit = {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      var newPin = scala.io.StdIn.readLine()
      while (newPin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères")
        println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        newPin= scala.io.StdIn.readLine()
      }
      codespin(id) = newPin

    }





}