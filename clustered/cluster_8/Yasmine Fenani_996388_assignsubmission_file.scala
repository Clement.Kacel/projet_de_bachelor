//Assignment: Yasmine Fenani_996388_assignsubmission_file

import scala.io.StdIn._
import scala.math._
object Main {
 
  //pour un depot
  def depot(idtf : Int, comptes : Array[Double]) : Unit = {
     // pour le choix de la devise
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      var choixdevise = readInt()
      //pour un depot en CHF
    if (choixdevise == 1) {
        println("Indiquez le montant du dépôt >")
        var depot_chf = readInt
        while (!(depot_chf % 10 == 0)){
          depot_chf = readInt
        }
        comptes(idtf) = depot_chf + comptes(idtf)
       println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > "+comptes(idtf))
        }
    //pour un depot en euros
        if (choixdevise == 2) {
        println("Indiquez le montant du dépôt >")
          var depot_eur = readInt
          while (!(depot_eur % 10 == 0)){
          depot_eur = readInt
        }
          comptes(idtf) = depot_eur*0.97 + comptes(idtf)
          println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > "+comptes(idtf))

        }
  }

  //pour le choix de la devise
  def retrait(idtf : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      var choixdevise = readInt()
      while (!((choixdevise==1)||(choixdevise==2))) {
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
        choixdevise = readInt()
      }
    //pour un retrait en CHF
      if (choixdevise ==1) {
        println("Indiquez le montant du retrait >")
        var montantretrait = readInt()
        //si le montant n'est pas un multiple de 10
        while ((montantretrait%10!=0)||(montantretrait> comptes(idtf)*0.1)) {
          if (montantretrait%10!=0) {
            println("Le montant doit être un multiple de 10.")
            montantretrait = readInt()
          }
          //si le motant est trop haut
          else if (montantretrait> comptes(idtf)*0.1) {
            println("Votre plafond de retrait autorisé est de : "+math.floor(comptes(idtf)*0.1*100)/100)
            montantretrait = readInt()
            }
        }
        //pour demamder le choix de la coupure
        var coupure = 0
        if (montantretrait >= 200) {
        println("En 1) grosses coupures, 2) petites coupures >")
         coupure = readInt()
          while (!((coupure==1)||(coupure==2))) {
            println("En 1) grosses coupures, 2) petites coupures >")
            coupure = readInt()
          }
        }
        //pour les grosses coupures
         
        if (coupure==1){
          println("Veuillez retirer la somme demandée :")
          if (montantretrait >= 500){
            //pour billet 500
            var billet : Int = 1
            billet = montantretrait / 500
            println(billet + " billet(s) de 500 CHF")
            comptes(idtf) = comptes(idtf) - billet*500
            montantretrait = montantretrait%500
          } //pour billet 200
          if (montantretrait >= 200){
            var billet : Int = 1
            billet = montantretrait / 200
            println(billet + " billet(s) de 200 CHF")
            comptes(idtf) = comptes(idtf) - billet*200
            montantretrait = montantretrait%200
        } //pour billet 100
          if (montantretrait >= 100){
            var billet : Int = 1
            billet = montantretrait / 100
            println(billet + " billet(s) de 100 CHF")
            comptes(idtf) = comptes(idtf) - billet*100
            montantretrait = montantretrait%100
      }  //pour billet 50
          if (montantretrait >= 50){
            var billet : Int = 1
            billet = montantretrait / 50
            println(billet + " billet(s) de 50 CHF")
            comptes(idtf) = comptes(idtf) - billet*50
            montantretrait = montantretrait%50
        } //pour billet 20
          if (montantretrait >= 20){
            var billet : Int = 1
            billet = montantretrait / 20
            println(billet + " billet(s) de 20 CHF")
            comptes(idtf) = comptes(idtf) - billet*20
            montantretrait = montantretrait%20 
        } //pour billet 10
          if (montantretrait >= 10){
            var billet : Int = 1
            billet = montantretrait / 10
            println(billet + " billet(s) de 10 CHF")
            comptes(idtf) = comptes(idtf) - billet*10
            montantretrait = montantretrait%10
          }
          println("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(idtf))
        }
        else {
          //pour les petites coupures
          //distribution
          println("Veuillez retirer la somme demandée :")
           if (montantretrait >= 100){
            //pour billet 100
            var billet : Int = 1
            billet = montantretrait / 100
            println(billet + " billet(s) de 100 CHF")
            comptes(idtf) = comptes(idtf) - billet*100
            montantretrait = montantretrait%100
      }   //pour billet 50
          if (montantretrait >= 50){
            var billet : Int = 1
            billet = montantretrait / 50
            println(billet + " billet(s) de 50 CHF")
            comptes(idtf) = comptes(idtf) - billet*50
            montantretrait = montantretrait%50
        }  //pour billet 20
          if (montantretrait >= 20){
            var billet : Int = 1
            billet = montantretrait / 20
            println(billet + " billet(s) de 20 CHF")
            comptes(idtf) = comptes(idtf) - billet*20
            montantretrait = montantretrait%20 
        }  //pour billet 10
          if (montantretrait >= 10){
            var billet : Int = 1
            billet = montantretrait / 10
            println(billet + " billet(s) de 10 CHF")
            comptes(idtf) = comptes(idtf) - billet*10
            montantretrait = montantretrait%10
          }
          println("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(idtf))
        }
      } 
        // pour un retrait en euros 
        if (choixdevise ==2) {
        println("Indiquez le montant du retrait >")
        var montantretrait = readInt()
        while ((montantretrait%10!=0)||(montantretrait> (comptes(idtf)*0.1)/0.97)) {
          //si le montant n'est pas un multiple de 10
          if (montantretrait%10!=0) {
            println("Le montant doit être un multiple de 10.")
            montantretrait = readInt()
          }
          //si le montant est trop haut
          else if (montantretrait> (comptes(idtf)*0.1)/0.97) {
            println("Votre plafond de retrait autorisé est de : "+math.floor(((comptes(idtf)*0.1)/0.97)*100)/100)
            montantretrait = readInt()
            }
        }
         
          
      //Distribution
          println("Veuillez retirer la somme demandée :")
           //billet de 100
          if (montantretrait >= 100){
            var billet : Int = 1
            billet = montantretrait / 100
            println(billet + " billet(s) de 100 EUR")
            comptes(idtf) = comptes(idtf) - billet*100*0.97
            montantretrait = montantretrait%100
      }    //billet de 50
          if (montantretrait >= 50){
            var billet : Int = 1
            billet = montantretrait / 50
            println(billet + " billet(s) de 50 EUR")
            comptes(idtf) = comptes(idtf) - billet*50*0.97
            montantretrait = montantretrait%50
        }  //billet de 20
          if (montantretrait >= 20){
            var billet : Int = 1
            billet = montantretrait / 20
            println(billet + " billet(s) de 20 EUR")
            comptes(idtf) = comptes(idtf) - billet*20*0.97
            montantretrait = montantretrait%20 
        }  //billet de 10
          if (montantretrait >= 10){
            var billet : Int = 1
            billet = montantretrait / 10
            println(billet + " billet(s) de 10 EUR")
            comptes(idtf) = comptes(idtf) - billet*10*0.97
            montantretrait = montantretrait%10
          }
          println("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est > " + math.floor(comptes(idtf)*100)/100 )

      }

  }

  //pour changer le code 
  def changepin(idtf : Int, codespin : Array[String]) : Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var nvcodespin = readLine()
    while (nvcodespin.length < 8) {
     // si le code ne respecte pas la règle des 8 caractères
      println("Votre code pin ne contient pas au moins 8 caractères")
      nvcodespin = readLine()
    }
     codespin(idtf)= nvcodespin
  }
  
  
  //pour commemcer 
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var tentatives = 0
    var codepin_saisir = "INTRO"
    var idtf = 0
    while (true) {
    while ((codespin(idtf)!=codepin_saisir)&&(tentatives==0)){
    tentatives = 3
    
      //pour s'identifier 
      println("Saisissez votre code identifiant >")
    idtf = readInt()
    if (idtf >= nbclients) {
       // si l'identifient est invalide
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
    } 
     
      //pour le con PIN
      codepin_saisir = readLine("Saisissez votre code pin >")
    while(!(codespin(idtf)==codepin_saisir)&&(tentatives != 0)) {
         // si il y a trop de tentatives
      if ((tentatives == 2)||(tentatives ==3)) {
      tentatives -=1
      println("Code pin erroné, il vous reste " + tentatives + " tentatives")
      codepin_saisir = readLine() }
      else {
        tentatives = 0
      println("Trop d’erreurs, abandon de l’identification")
      }
   }
  }
      //pour choisir l'operation
     println("Choisissez votre opération : \n 1) Dépôt\n 2) Retrait\n 3) Consultation du compte\n 4) Changement du code pin\n 5) Terminer\nVotre choix :")
    var choix = readInt()
    while (!((choix==1)||(choix==2)||(choix==3)||(choix==4)||(choix==5))) {
      println("Choisissez votre opération : \n 1) Dépôt\n 2) Retrait\n 3) Consultation du compte\n 4) Changement du code pin\n 5) Terminer\nVotre choix :")
      choix = readInt() 
    }
     while (choix !=5){
      
       //pour un depot
       if (choix==1) {
      depot(idtf,comptes)

      }
      // pour un retrait
      if (choix==2){
      retrait(idtf,comptes)
    }
      
       //pour consulter le compte
      if (choix==3) {
        println("Le montant disponible sur votre compte est de : "+comptes(idtf))

      }

       //pour changer le code 
  if (choix==4) {
          changepin(idtf,codespin)
        } 

       
       //pour rechoisir l'opération
         println("Choisissez votre opération : \n 1) Dépôt\n 2) Retrait\n 3) Consultation du compte\n 4) Changement du code pin\n 5) Terminer\nVotre choix :")
      choix = readInt()
       while (!((choix==1)||(choix==2)||(choix==3)||(choix==4)||(choix==5))) {
      println("Choisissez votre opération : \n 1) Dépôt\n 2) Retrait\n 3) Consultation du compte\n 4) Changement du code pin\n 5) Terminer\nVotre choix :")
      choix = readInt() 
    }
     }
      
      //pour finir 
     if (choix==5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
       tentatives = 0
       codepin_saisir = "INTRO"
        } 
    }
  } 
}