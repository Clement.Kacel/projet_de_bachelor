//Assignment: Gaëtan Meyer_996524_assignsubmission_file

import scala.io.StdIn._
object Main {
  def depot(id : Int, compte : Array[Double]): Unit = {
    var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
    var montantD = readLine("Indiquez le montant du dépôt > ").toDouble
    while(montantD % 10 != 0){
      println("Le montant doit être un multiple de 10 ")
      montantD = readLine("Indiquez le montant du dépôt > ").toDouble
    }
    if(devise == 1){
      compte(id) += montantD
    } else if(devise == 2){
      montantD = montantD * 0.95
      compte(id) += montantD
    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", compte(id))
  }
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    var coupureTab = Array(500,200,100,50,20,10)
    var cTab = Array(100,50,20,10)
    var coupureTab2 = new Array[Int](6)
    var ok = ""
    var o = "o"
    var i = 0
    var nbrBillet = 0.0.toInt
    var boucle2 = false
    
    var devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
    while(!((devise == 1) || (devise == 2))){
      devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
    }
    if(devise == 1){
      var montantR = readLine("Indiquez le montant à retirer > ").toInt
      while((montantR % 10 != 0) || (montantR > (comptes(id) - (comptes(id) * 0.1)))){
        if(montantR % 10 != 0){
          println("Le montantR doit être un multiple de 10")
          montantR = readLine("Indiquez le montant à retirer > ").toInt
        } else if(montantR > (comptes(id) - (comptes(id) * 0.1))){
          println("Votre plafond autorisé est de : " + (comptes(id) - (comptes(id) * 0.1)) + " CHF")
          montantR = readLine("Indiquez le montant à retirer > ").toInt
        }
      }
      comptes(id) -= montantR
      if(montantR > 200){
        var coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
        while(!(coupure == 1 || coupure == 2)){
          coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
        }
        if(coupure == 1){
          while(!boucle2){
            println("Il vous reste " + montantR + " CHF à distribuer")
            nbrBillet = montantR / coupureTab(i)
            println("Vous pouvez obtenir au maximum " + nbrBillet + " billet(s) de " + coupureTab(i) + " CHF")
            ok = readLine("Tapez o pour ok ou une autre valeur inferieure à celle proposée > ")
            if(ok == o){
              coupureTab2(i) = nbrBillet
              montantR -= nbrBillet * coupureTab(i)
              i += 1
              nbrBillet = montantR / coupureTab(i)
            } else {
              if(ok.toInt > 0){
                coupureTab2(i) = ok.toInt
              }
              montantR -= ok.toInt * coupureTab(i)
              i += 1
              nbrBillet = montantR / coupureTab(i)
            }
            if(coupureTab(i) == 10){
              coupureTab2(i) = nbrBillet
              for(i <- 0 until coupureTab2.length){
                if(coupureTab2(i) > 0){
                  println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + coupureTab(i) + " CHF")
                }
              }
              boucle2 = true
            }
            if(montantR == 0){
              for(i <- 0 until coupureTab2.length){
                if(coupureTab2(i) > 0){
                  println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + coupureTab(i) + " CHF")
                } 
              }
              boucle2 = true
            }
          }
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        } else if(coupure == 2){
          while(!boucle2){
            println("Il vous reste " + montantR + " CHF à distribuer")
            nbrBillet = montantR / cTab(i)
            println("Vous pouvez obtenir au maximum " + nbrBillet + " billet(s) de " + cTab(i) + " CHF")
            ok = readLine("Tapez o pour ok ou une autre valeur inferieure à celle proposée > ")
            if(ok == o){
              coupureTab2(i) = nbrBillet
              montantR -= nbrBillet * cTab(i)
              i += 1
              nbrBillet = montantR / cTab(i)
            } else {
              if(ok.toInt > 0){
                coupureTab2(i) = ok.toInt
              }
              montantR -= ok.toInt * cTab(i)
              i += 1
              nbrBillet = montantR / cTab(i)
            }
            if(cTab(i) == 10){
              coupureTab2(i) = nbrBillet
              for(i <- 0 until coupureTab2.length){
                if(coupureTab2(i) > 0){
                  println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + cTab(i) + " CHF")
                }
              }
              boucle2 = true
            }
            if(montantR == 0){
              for(i <- 0 until coupureTab2.length){
                if(coupureTab2(i) > 0){
                  println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + cTab(i) + " CHF")
                }
              }
              boucle2 = true
            }
          }
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        }
      } else {
        while(!boucle2){
          println("Il vous reste " + montantR + " CHF à distribuer")
          nbrBillet = montantR / cTab(i)
          println("Vous pouvez obtenir au maximum " + nbrBillet + " billet(s) de " + cTab(i) + " CHF")
          ok = readLine("Tapez o pour ok ou une autre valeur inferieure à celle proposée > ")
          if(ok == o){
            coupureTab2(i) = nbrBillet
            montantR -= nbrBillet * cTab(i)
            i += 1
            nbrBillet = montantR / cTab(i)
          } else {
            if(ok.toInt > 0){
              coupureTab2(i) = ok.toInt
            }
            montantR -= ok.toInt * cTab(i)
            i += 1
            nbrBillet = montantR / cTab(i)
          }
          if(cTab(i) == 10){
            coupureTab2(i) = nbrBillet
            for(i <- 0 until coupureTab2.length){
              if(coupureTab2(i) > 0){
                println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + cTab(i) + " CHF")
              }
            }
            boucle2 = true
          }
          if(montantR == 0){
            for(i <- 0 until coupureTab2.length){
              if(coupureTab2(i) > 0){
                println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + cTab(i) + " CHF")
              }
            }
            boucle2 = true
          }
        }
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n",comptes(id))
      }
    } else if(devise == 2){
      var montantR = readLine("Indiquez le montant à retirer > ").toInt
      while((montantR % 10 != 0) || (montantR > (comptes(id) - (comptes(id) * 0.1)))){
        if(montantR % 10 != 0){
          println("Le montantR doit être un multiple de 10")
          montantR = readLine("Indiquez le montant à retirer > ").toInt
        } else if(montantR >= (comptes(id) - (comptes(id) * 0.1))){
          println("Votre plafond autorisé est de : " + (montantR > (comptes(id) - (comptes(id) * 0.1))) + " EUR")
          montantR = readLine("Indiquez le montant à retirer > ").toInt
        }
      }
      comptes(id) -= montantR * 0.95
      while(!boucle2){
        println("Il vous reste " + montantR + " EUR à distribuer")
        nbrBillet = montantR / cTab(i)
        println("Vous pouvez obtenir au maximum " + nbrBillet + " billet(s) de " + cTab(i) + " EUR")
        ok = readLine("Tapez o pour ok ou une autre valeur inferieure à celle proposée > ")
        if(ok == o){
          coupureTab2(i) = nbrBillet
          montantR -= nbrBillet * cTab(i)
          i += 1
          nbrBillet = montantR / cTab(i)
        } else {
          if(ok.toInt > 0){
            coupureTab2(i) = ok.toInt
          }
          montantR -= ok.toInt * cTab(i)
          i += 1
          nbrBillet = montantR / cTab(i)
        }
        if(cTab(i) == 10){
          coupureTab2(i) = nbrBillet
          for(i <- 0 until coupureTab2.length){
            if(coupureTab2(i) > 0){
              println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + cTab(i) + " EUR")
            }
          }
          boucle2 = true
        }
        if(montantR == 0){
          for(i <- 0 until coupureTab2.length){
            if(coupureTab2(i) > 0){
              println("Veuillez retirer la somme demandée : " + coupureTab2(i) + " billet(s) de " + cTab(i) + " EUR")
            }
          }
          boucle2 = true
        }
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
    }
  }
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var pinMod = readLine("Saissisez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toCharArray
    while(pinMod.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères ")
      pinMod = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toCharArray
    }
    codespin(id) = pinMod.mkString
  }
  
  def main(args: Array[String]): Unit = {
    var compte = Array.fill(100)(1200.0)
    var codespin = Array.fill(100)("INTRO1234")
    var nbclients = 100
    var choix = 0
    var pin = ""
    var tentative = 2
    var boucle = false
    var code = false
    var operation = false

    while(!boucle){
      var identifiant = readLine("Saisissez votre identifiant > ").toInt
      if(identifiant < nbclients){
        pin = readLine("Saisissez votre code pin > ")
        while(!(code || pin == codespin(identifiant))){
          println("Code pin erronée, il vous reste " + tentative  + " tentative(s)")
          pin = readLine("Saisissez votre code pin > ")
          if(pin != codespin(identifiant)){
            tentative -= 1
            if(tentative == 0){
              println("Trop d'erreurs, abandon de l'identification")
              code = true
            }
          } else {
            tentative = 2
            code = true
          } 
        }
        if(pin == codespin(identifiant)){
          while(!operation){
            choix = readLine("Choisissez votre opération : \n1) Dépôt \n2) Retrait \n3) Consultation du compte \n4) Changement du code pin \n5) Terminer \nVotre choix : ").toInt
            if(choix == 1){
              depot(identifiant, compte)
            } else if(choix == 2){
              retrait(identifiant, compte)
            } else if(choix == 3){
              printf("Le montant disponible sur votre compte est de : %.2f CHF\n ", compte(identifiant))
            } else if(choix == 4){
              changepin(identifiant, codespin)
            } else if(choix == 5){
              println("Fin des opérations, n'oubliez pas de récupérer votre carte")
              operation = true
            }
          }
        }
        code = false
        tentative = 2
        operation = false
      } else {
        println("Cet identifiant n'est pas valable")
        boucle = true
      }
    }
  }
}
