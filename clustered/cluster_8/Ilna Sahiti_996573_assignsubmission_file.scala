//Assignment: Ilna Sahiti_996573_assignsubmission_file

import io.StdIn._
object Main {
  def main(args: Array[String]): Unit = {

    //Déclaration des variables fixes
    var tentatives = 3
    var devise = 0
    var coupure = 2 // on démarre à deux car le type de coupure attribué si on ne donne pas le choix
    var montant_autorise = 0.0
    var proposition = ""
    var autorisation = "off"
    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var pincorrect = false



    while(true){
      var choix = -1
      pincorrect = false
      var id_client = readLine("Saisissez votre code identifiant >").toInt
      while (id_client > nbclients - 1 || id_client < 0) {
        println("Cet identifiant n’est pas valable.")
        System.exit(0)
      }
      pincorrect = entrerpin(id_client,codespin)

      if (pincorrect) {

        while (choix != 5) {

          choix = readLine("Choisissez votre opération : \n  1) Dépôt\n  2) Retrait\n  3) Consultation du compte\n  4) Changement du code pin\n  5) Terminer\nVotre choix : ").toInt
          //Le code pin a été validé
          if (choix == 1) { //l'opération 1
            depot(id_client, comptes)
          } else if (choix == 2) { //l'opération 2
            retrait(id_client, comptes)
          } else if (choix == 3) { // l'opération 3
            printf("Le solde disponible sur votre compte est de :  %.2f \n", comptes(id_client))
          } else if (choix == 4) {
            changepin(id_client, codespin)
          } else if (choix == 5) {
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          }
        }

          } else { // Après 3 tentatives infructueuses
            println("Trop d’erreurs, abandon de l’identification")
          }

      }
    def depot(id: Int, comptes: Array[Double]): Unit = {
      devise = readLine("Indiquez la devise du dépôt : 1) CHF; 2) EUR > ").toInt
      var depot = readLine("Indiquez le montant du dépôt > ").toInt
      while (depot % 10 != 0) { // Vérification si multiple de 10
        depot = readLine("Indiquez à nouveau montant du dépôt (celui-ci doit être un multiple de 10) > ").toInt
      }
      if (devise == 1) {
        comptes(id) = depot + comptes(id)
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f \n", comptes(id))
      } else {
        comptes(id) = depot * 0.95 + comptes(id)
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f \n", comptes(id))
      }
    }
      def entrerpin(id: Int,codespin: Array[String]): Boolean = {
        tentatives = 3
        var pin = readLine("Saisissez votre code pin > ")
        while ((pin != codespin(id)) && (tentatives > 1)) { // le > 1 est pour se restraindre à seulement 3 tentatives
          tentatives = tentatives - 1
          pin = readLine("Code pin erroné, il vous reste " + tentatives + " tentatives : ")
        }
        if (pin == codespin(id))  true
        else false
      }

    def retrait(id: Int, comptes: Array[Double]): Unit = {
      var billet_500 = 0
      var billet_200 = 0
      var billet_100 = 0
      var billet_50 = 0
      var billet_20 = 0
      var billet_10 = 0

      devise = readLine("Indiquez la devise : 1) CHF, 2) EUR > ").toInt
      //Choix de devise imposé
      while ((devise != 1) && (devise != 2))
        {
          devise = readLine("Indiquez la devise : 1) CHF, 2) EUR > ").toInt}

        if (devise == 1) {
          montant_autorise = comptes(id) * 0.1
        } else {
          montant_autorise = 1.05 * comptes(id) * 0.1
        }
      var montant_retrait = readLine("Indiquez le montant du retrait > ").toInt
      while ((montant_retrait % 10 != 0) || (montant_retrait.toDouble > montant_autorise)) {
        if (montant_retrait % 10 != 0) println("Le montant doit être un multiple de 10.")
        if (montant_retrait > montant_autorise) println("Votre plafond de retrait autorisé est de : " + montant_autorise)
        montant_retrait = readLine("Indiquez à nouveau le montant du retrait > ").toInt
      }
      if (devise == 1) comptes(id) = comptes(id) - montant_retrait
      else comptes(id) = comptes(id) - 0.95 * montant_retrait
      if ((devise == 1) && (montant_retrait >= 200)) {
        coupure = readLine("En 1) grosses coupures 2) petites coupures > ").toInt
        while ((coupure != 1) && (coupure != 2)) {
          coupure = readLine("En 1) grosses coupures 2) petites coupures > ").toInt
        }
      }
      if (devise == 1) {
        if (coupure == 1) {
          if (montant_retrait >= 500) {
            println("Il reste " + montant_retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montant_retrait / 500 + " billet(s) de 500 CHF ")
            while (autorisation != "on") {
              proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
              if (proposition.forall(Character.isDigit)) {
                if (proposition.toInt <= montant_retrait / 500) {
                  billet_500 = proposition.toInt
                  autorisation = "on"
                }
              } else {
                billet_500 = montant_retrait / 500
                autorisation = "on"
              }
            }
            montant_retrait = montant_retrait - billet_500 * 500
            autorisation = "off"
          }
          if (montant_retrait >= 200) {
            println("Il reste " + montant_retrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montant_retrait / 200 + " billet(s) de 200 CHF ")
            while (autorisation != "on") {
              proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
              if (proposition.forall(Character.isDigit)) {
                if (proposition.toInt <= montant_retrait / 200) {
                  billet_200 = proposition.toInt
                  autorisation = "on"
                }
              } else {
                billet_200 = montant_retrait / 200
                autorisation = "on"
              }
            }
            montant_retrait = montant_retrait - billet_200 * 200
            autorisation = "off"
          }
        }
        if (montant_retrait >= 100) {
          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + montant_retrait / 100 + " billet(s) de 100 CHF ")
          while (autorisation != "on") {
            proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
            if (proposition.forall(Character.isDigit)) {
              if (proposition.toInt <= montant_retrait / 100) {
                billet_100 = proposition.toInt
                autorisation = "on"
              }
            } else {
              billet_100 = montant_retrait / 100
              autorisation = "on"
            }
          }
          montant_retrait = montant_retrait - billet_100 * 100
          autorisation = "off"
        }
        if (montant_retrait >= 50) {
          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + montant_retrait / 50 + " billet(s) de 50 CHF ")
          while (autorisation != "on") {
            proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
            if (proposition.forall(Character.isDigit)) {
              if (proposition.toInt <= montant_retrait / 50) {
                billet_50 = proposition.toInt
                autorisation = "on"
              }
            } else {
              billet_50 = montant_retrait / 50
              autorisation = "on"
            }
          }
          montant_retrait = montant_retrait - billet_50 * 50
          autorisation = "off"
        }
        if (montant_retrait >= 20) {
          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + montant_retrait / 20 + " billet(s) de 20 CHF ")
          while (autorisation != "on") {
            proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
            if (proposition.forall(Character.isDigit)) {
              if (proposition.toInt <= montant_retrait / 20) {
                billet_20 = proposition.toInt
                autorisation = "on"
              }
            } else {
              billet_20 = montant_retrait / 20
              autorisation = "on"
            }
          }
          montant_retrait = montant_retrait - billet_20 * 20
          autorisation = "off"
        }
        if (montant_retrait >= 10) {
          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous recevez donc " + montant_retrait / 10 + " billet(s) de 10 CHF")
          billet_10 = montant_retrait / 10
          montant_retrait = montant_retrait - billet_10 * 10
        }
        println("Veuillez retirer la somme demandée :")
        if (billet_500 > 0) println(s"$billet_500 billet(s) de 500 CHF")
        if (billet_200 > 0) println(s"$billet_200 billet(s) de 200 CHF")
        if (billet_100 > 0) println(s"$billet_100 billet(s) de 100 CHF")
        if (billet_50 > 0) println(s"$billet_50 billet(s) de 50 CHF")
        if (billet_20 > 0) println(s"$billet_20 billet(s) de 20 CHF")
        if (billet_10 > 0) println(s"$billet_10 billet(s) de 10 CHF")
      } else {
        if (montant_retrait >= 100) {
          println("Il reste " + montant_retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + montant_retrait / 100 + " billet(s) de 100 EUR ")
          while (autorisation != "on") {
            proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
            if (proposition.forall(Character.isDigit)) {
              if (proposition.toInt <= montant_retrait / 100) {
                billet_100 = proposition.toInt
                autorisation = "on"
              }
            } else {
              billet_100 = montant_retrait / 100
              autorisation = "on"
            }
          }
          montant_retrait = montant_retrait - billet_100 * 100
          autorisation = "off"
        }
        if (montant_retrait >= 50) {
          println("Il reste " + montant_retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + montant_retrait / 50 + " billet(s) de 50 EUR ")
          while (autorisation != "on") {
            proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
            if (proposition.forall(Character.isDigit)) {
              if (proposition.toInt <= montant_retrait / 50) {
                billet_50 = proposition.toInt
                autorisation = "on"
              }
            } else {
              billet_50 = montant_retrait / 50
              autorisation = "on"
            }
          }
          montant_retrait = montant_retrait - billet_50 * 50
          autorisation = "off"
        }
        if (montant_retrait >= 20) {
          println("Il reste " + montant_retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + montant_retrait / 20 + " billet(s) de 20 EUR ")
          while (autorisation != "on") {
            proposition = readLine("Tapez o pour ok ou une autre valeur inférieur à celle proposée : ")
            if (proposition.forall(Character.isDigit)) {
              if (proposition.toInt <= montant_retrait / 20) {
                billet_100 = proposition.toInt
                autorisation = "on"
              }
            } else {
              billet_20 = montant_retrait / 20
              autorisation = "on"
            }
          }
          montant_retrait = montant_retrait - billet_20 * 20
          autorisation = "off"
        }
        if (montant_retrait >= 10) {
          println("Il reste " + montant_retrait + " EUR à distribuer")
          println("Vous recevez donc " + montant_retrait / 10 + " billet(s) de 10 EUR")
          billet_10 = montant_retrait / 10
          montant_retrait = montant_retrait - billet_10 * 10
        }
        println("Veuillez retirer la somme demandée :")
        if (billet_100 > 0) println(s"$billet_100 billet(s) de 100 EUR")
        if (billet_50 > 0) println(s"$billet_50 billet(s) de 50 EUR")
        if (billet_20 > 0) println(s"$billet_20 billet(s) de 20 EUR")
        if (billet_10 > 0) println(s"$billet_10 billet(s) de 10 EUR")
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f  CHF\n", comptes(id))
    }

    def changepin(id: Int, codespin: Array[String]): Unit = {
      codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > \n")
      while (codespin(id).length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères")
        codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > \n")
      }
    }


  }



}
