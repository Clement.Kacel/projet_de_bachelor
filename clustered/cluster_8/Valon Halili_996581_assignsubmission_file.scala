//Assignment: Valon Halili_996581_assignsubmission_file

import scala.io.StdIn._

object Main {
  val nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")

    def convertirEURtoCHF(montantEUR: Int): Double = montantEUR * 0.95
    def convertirCHFtoEUR(montantCHF: Double): Double = montantCHF * 1.05

    def main(args: Array[String]): Unit = {
      while (true) {
        val id = demanderIdentifiant()
        if (id >= 0) {
          menuPrincipal(id)
        }
      }
    }
      // start avec id
      def demanderIdentifiant(): Int = {
      println("Saisissez votre code identifiant > ")
      val id = readLine().toInt

      if (id >= nbclients) {
        println("Cet identifiant n’est pas valable.")
        return -1
      }
      // code pin
      var tentativesRestantes = 3
      var codePinSaisi = false

      while (!codePinSaisi && tentativesRestantes > 0) {
        println("Saisissez votre code pin > ")
        val codePin = readLine()

        if (codePin == codespin(id)) {
          codePinSaisi = true
        } else {
          tentativesRestantes -= 1
          println("Code pin erroné, il vous reste " + tentativesRestantes + " tentatives > ")
        }
      }

      if (tentativesRestantes == 0) {
        println("Trop d’erreurs, abandon de l’identification")
        return -1
        }
      id
    }
    // menu principal
    def menuPrincipal(id: Int): Unit = {
    var continuer = true
    while (continuer) {
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")

      val choix = readLine("Choisissez votre opération : ").toInt

      if (choix == 1) {
        depot(id, comptes)
      } else if (choix == 2) {
        retrait(id, comptes)
      } else if (choix == 3) {
        consultationCompte(id)
      }
        else if (choix == 4) {
          changepin(id, codespin)
      } else if (choix == 5) {
        operationTerminer()
      } 
    }
  }
   // opération dépôt
    def depot(id: Int, comptes: Array[Double]): Unit = {
      println("Votre choix : Dépôt")
      println("1) CHF ; 2) EUR")

          var deviseDepot = 0
          var deviseValide = false

          // choix devise CHF/EUR
          do {
            val deviseInput = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
            if (deviseInput == "1") {
              deviseDepot = 1
              deviseValide = true
            } else if (deviseInput == "2") {
              deviseDepot = 2
              deviseValide = true
            } 
          } while (!deviseValide)

          var montantDepot = 0
          var montantValide = false

          // saisie montant dépôt
          do {
            val montantInput = readLine("Indiquez le montant du dépôt > ")
              montantDepot = montantInput.toInt
              if (montantDepot % 10 == 0) {
                montantValide = true
              } else {
                println("Le montant doit être un multiple de 10.")
              }
          } while (!montantValide)

          // effectuer dépôt
          if (deviseDepot == 2) {
            val montantDepotCHF = convertirEURtoCHF(montantDepot)
            comptes(id) += montantDepotCHF
          } else {
            comptes(id) += montantDepot
          }
          // affichage new solde
          printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
        }
   // opération retrait
    def retrait(id : Int, comptes : Array[Double]) : Unit = {
          println("Votre choix : Retrait")
          println("1) CHF ; 2) EUR")

          var deviseRetrait = 0
          var deviseRetraitValide = false

          // choix devise retrait CHF/EUR
          while (!deviseRetraitValide) {
            val deviseRetraitInput = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ")
            if (deviseRetraitInput == "1") {
              deviseRetrait = 1
              deviseRetraitValide = true
            } else if (deviseRetraitInput == "2") {
              deviseRetrait = 2
              deviseRetraitValide = true
            } 
          }

          // choix coupures CHF
          var choixCoupures = 0
          var choixCoupuresValide = false

          if (deviseRetrait == 1) {
            while (!choixCoupuresValide) {
              val choixCoupuresInput = readLine("En 1) grosses coupures ; 2) petites coupures ? > ")
              if (choixCoupuresInput == "1" || choixCoupuresInput == "2") {
                choixCoupures = choixCoupuresInput.toInt
                choixCoupuresValide = true
              } 
            }
          } else {
            // si devise EUR no choix grosses/petites coupures
            choixCoupures = 2
            choixCoupuresValide = true
          }
          // montant retrait autorisé
          val plafondRetraitAutorise = (comptes(id) * 0.1).toInt

          var montantRetrait = 0
          var montantValide = false

      // saisie montant retrait
      while (!montantValide) {
        val montantInput = readLine("Indiquez le montant du retrait (Plafond autorisé : " + plafondRetraitAutorise + ") > ")
        montantRetrait = montantInput.toInt

        if (montantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        } else if (montantRetrait > plafondRetraitAutorise) {
          montantValide = false
        } else {
          montantValide = true
        }
      }
          // coupures en fonction de la devise
          var coupureUnitaire = 0

          if (deviseRetrait == 1) {
            coupureUnitaire = 10
          } else {
            coupureUnitaire = 10
          }
      
          // répartir montant avec coupures disponibles
          var reste = montantRetrait

      // conversion du montant EUR si devise retrait = EUR
      if (deviseRetrait == 2) {
      val montantRetraitCHF = convertirEURtoCHF(montantRetrait)
      comptes(id) -= montantRetraitCHF
      } else {
      comptes(id) -= montantRetrait
      }
          // afficher coupures disponibles
          var coupure = if (deviseRetrait == 1) 500 else 100
          var coupuresRetirees = 0

          while (reste > 0 && coupure >= coupureUnitaire) {
            val nombreCoupuresMax = reste / coupure
            if (nombreCoupuresMax > 0) {
              println("Il reste " + reste + { if (deviseRetrait == 1) " CHF" else " EUR" } + " à distribuer")
              println("Vous pouvez obtenir au maximum " + nombreCoupuresMax + " billet(s) de " + coupure + ".")
              val validationCoupures = readLine("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
              if (validationCoupures == "o" || validationCoupures.toInt >= nombreCoupuresMax) {
                reste -= nombreCoupuresMax * coupure
                coupuresRetirees = nombreCoupuresMax
              } else {
                // alternatives si l'utilisateur choisit une valeur inférieure
                var j = 0
                while (j < coupuresRetirees && coupure <= reste) {
                  val alternativeCoupure = coupuresRetirees * coupure
                  val nombreAlternative = reste / alternativeCoupure
                  println("" + nombreAlternative + " billet(s) de " + alternativeCoupure + { if (deviseRetrait == 1) " CHF" else " EUR" } + ".")
                  j += 1
                }
                val choixAlternative = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (choixAlternative == "o" || choixAlternative.toInt <= reste) {
                  reste -= choixAlternative.toInt
                  coupuresRetirees += 1
                }
              }
            }

            // passer à la next coupure
            coupure = if (coupure == 500) 200 else if (coupure == 200) 100 else if (coupure == 100) 50 else 10
          }

          // message final si le montant a été retiré complètement
          if (reste == 0) {
            println("Veuillez retirer la somme demandée : ")
            // afficher billets retirés
            println("" + coupuresRetirees + " billet(s) de " + coupure + { if (deviseRetrait == 1) " CHF" else " EUR" } + ".")
          }

  // message sur le solde retrait
  printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))     
  }

  // opération consultation compte
  def consultationCompte(id: Int): Unit = {
  println("Votre choix : Consultation du compte.")
  printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
}

  // opération changer code pin
  def changepin(id: Int, codespin: Array[String]): Unit = {
    var codePinValide = false
    while (!codePinValide) {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      val nouveauCodePin = readLine()

      if (nouveauCodePin.length >= 8) {
        codespin(id) = nouveauCodePin
        codePinValide = true
        println("Votre code pin a été changé avec succès.")
      } else {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      }
    }
  }
      // opération terminer
      def operationTerminer(): Boolean = {
      println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
      
      // demander id à la fin pour simuler nouvelle session utilisateur
      demanderIdentifiant()
      false
    }
}