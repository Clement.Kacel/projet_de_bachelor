//Assignment: François-Sammy Chiaramonte_996422_assignsubmission_file

import scala.io.StdIn._
import scala.collection.mutable.ArrayBuffer
object Main {

val NbUtilisateurs = 100
var ValeursDesComptes = Array.fill[Double](NbUtilisateurs+1)(1200.0)
  val CodeDesComptes = Array.fill[String](NbUtilisateurs+1)("INTRO1234")
  val IdComptes = ArrayBuffer.range(1, 101)
  
  def main(args: Array[String]): Unit = {
    while(true) {
    val Identifiant = readLine("Saisissez votre code identifiant : ").toInt
    
    if (Identifiant < 1 || Identifiant > NbUtilisateurs) {
      println("Cet identifiant n'est pas valable")
      sys.exit
    }
    else {
      var NBtentatives = 0
      var CodePin = readLine("Quel est votre code pin : ")
      while(NBtentatives < 2 && CodePin != CodeDesComptes(Identifiant)) {
        NBtentatives = NBtentatives + 1
        println("Code pin erroné, il vous reste " + (3 - NBtentatives) + " tentatives : ")
        CodePin = readLine("Quel est votre code pin : ").toString
        
      }
      if (CodePin == CodeDesComptes(Identifiant)) {
        ChoixOption(Identifiant)
      }
      else{
         println("Trop d'erreurs, abandon de l'identification")
      }
    }
  }
}
  def ChoixOption(Identifiant: Int): Unit = {
    var PropositionMenu = true
    while(PropositionMenu) {
      println("Choisissez votre opération : ")
      println("1) Dépot ")
      println("2) Retrait ")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      val choix = readLine("Votre choix : ").toInt


       if(choix == 1){
        Depot(Identifiant, ValeursDesComptes)
      }
      else if(choix == 4){
        modificationpin(Identifiant, CodeDesComptes)
      }
      else if(choix == 3){
         consultercompte(Identifiant: Int, ValeursDesComptes(Identifiant))
      }
      else if(choix == 2){
         Retrait(Identifiant, ValeursDesComptes)
      }
      else if (choix == 5){
        println("Fin des opérations, n'oubliez pas de récuperer votre carte. ")
        PropositionMenu = false
      }
      else{
        println("Veuillez choisir une option entre 1 et 5")
      } 
    }
  }

    def Depot(Identifiant: Int, ValeursDesComptes: Array[Double]): Unit ={
     var Devise = readLine("Indiquez la devise du dépot : 1) CHF ; 2) EUR ").toInt
      while(!(Devise == 1 || Devise == 2)){
        Devise = readLine("Indiquez la devise du dépot : 1) CHF ; 2) EUR ").toInt
      }
     var Depot = readLine("Indiquez le montant du dépôt : ").toDouble
      while (!(Depot % 10.0 == 0.0)){
        println("Le dépot doit être un multiple de 10")
        Depot =readLine("Veuillez indiquer le montant du dépot : ").toDouble
      }
      if (Devise == 2){
        Depot = Depot * 0.95
        ValeursDesComptes(Identifiant) = ValeursDesComptes(Identifiant) + Depot
        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ValeursDesComptes(Identifiant))
      }
      if (Devise == 1){
         ValeursDesComptes(Identifiant) = ValeursDesComptes(Identifiant) + Depot
         printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , ValeursDesComptes(Identifiant))
      }
      
    }

  def consultercompte(Identifiant: Int, ValeursDesComptes: Double): Unit = {
    printf("Le montant disponible sur votre compte est de : %.2f CHF \n " , ValeursDesComptes)
  }

  def modificationpin(Identifiant: Int, CodeDesComptes : Array[String]): Unit = {
    var NouveauCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : ")
    while (NouveauCodePin.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      NouveauCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : ")
    }
    CodeDesComptes(Identifiant) = NouveauCodePin
    println("Votre code pin a bien été modifié")
  }


  def Retrait(Identifiant: Int, ValeursDesComptes: Array[Double]): Unit ={
    var Devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR ").toInt
    
    
    
  while (!(Devise == 1 || Devise == 2)){
    Devise = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR ").toInt
  }

    var MontantAutoriser = (ValeursDesComptes(Identifiant) * 0.1).toDouble

    
    var MTRetrait = readLine("Indiquez le montant du retrait : ").toDouble
    while ((MTRetrait % 10.0 != 0.0) || MTRetrait > MontantAutoriser){
      if(MTRetrait > MontantAutoriser){
        println("Votre plafond de retrait autorisé est de : " + MontantAutoriser)
      MTRetrait = readLine("Veuillez indiquer le montant du retrait : ").toDouble
      }
      if(MTRetrait % 10.0 != 0.0){
        println("Le retrait doit être un multiple de 10")
        MTRetrait = readLine("Veuillez indiquer le montant du retrait : ").toDouble
    }
    }

    if(Devise == 1 && MTRetrait > 200){
      var ChoixCoupures = readLine("En 1) grosses coupures, 2) petites coupures : ").toInt
      while(!(ChoixCoupures == 1 || ChoixCoupures == 2)){
        ChoixCoupures = readLine("En 1) grosses coupures, 2) petites coupures : ").toInt
      }

      val CoupuresDispoCHF: Array[Int] =
      if (ChoixCoupures == 1){
         Array (500, 200, 100, 50, 20, 10)
      }else {
        Array (100, 50, 20, 10)
      }

      var Z = 0
      var MontantRestantARetirer = MTRetrait
      var BilletDonnerAUtilisateur = ArrayBuffer[String]()
      

       while(Z < CoupuresDispoCHF.length && MontantRestantARetirer > 0){
         var Coupures = CoupuresDispoCHF(Z) 
         var NbCoupures = (MontantRestantARetirer / Coupures).toInt

          if(Coupures == 10 && NbCoupures > 0){
            MontantRestantARetirer = MontantRestantARetirer - Coupures * NbCoupures
            BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + "CHF")
          }
         
           else if(MontantRestantARetirer / CoupuresDispoCHF(Z) >= 1){
            println("Vous pouvez obtenir au maximum " + NbCoupures + " billets de " + Coupures + " CHF")
              var Confirmation = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString
           
               if(Confirmation == "o"){
                  MontantRestantARetirer = MontantRestantARetirer - (NbCoupures * Coupures)
                 println("Il reste " + MontantRestantARetirer + " CHF à retirer")
                 BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + " CHF")
               }else if(Confirmation.nonEmpty && Confirmation.forall(_.isDigit)){
                 var ChoixNbCoupures = Confirmation.toInt
                 if(ChoixNbCoupures <= NbCoupures && ChoixNbCoupures >= 0){
                   NbCoupures = ChoixNbCoupures
                   MontantRestantARetirer = MontantRestantARetirer - (NbCoupures * Coupures)

                   if(ChoixNbCoupures > 0){
                      println("Il reste " + MontantRestantARetirer + " CHF à retirer")
                      BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + " CHF")
                   }
                   else{
                     println("Il reste " + MontantRestantARetirer + " CHF à retirer")
                   }
                 }else{
                     println("Veuillez saisir une valeur inférieure ou égale à " + NbCoupures)
                   }
                 }
           }
                 
           Z += 1
       }
         
      ValeursDesComptes(Identifiant) = ValeursDesComptes(Identifiant) - MTRetrait
      println("Veuillez retirer la somme demandé : ")
      for(i <- BilletDonnerAUtilisateur){
        println(i)
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ValeursDesComptes(Identifiant))
       }

    if(Devise == 1 && MTRetrait < 200){
        
      var CoupuresDispoCHF: Array[Int] = Array (100, 50, 20, 10)
          
        var Z = 0
        var MontantRestantARetirer = MTRetrait
        var BilletDonnerAUtilisateur = ArrayBuffer[String]()


         while(Z < CoupuresDispoCHF.length && MontantRestantARetirer > 0){
           var Coupures = CoupuresDispoCHF(Z) 
           var NbCoupures = (MontantRestantARetirer / Coupures).toInt

            if(Coupures == 10 && NbCoupures > 0){
              MontantRestantARetirer = MontantRestantARetirer - Coupures * NbCoupures
              BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + "CHF")
            }

             else if(MontantRestantARetirer / CoupuresDispoCHF(Z) >= 1){
              println("Vous pouvez obtenir au maximum " + NbCoupures + " billets de " + Coupures + " CHF")
                var Confirmation = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString


                 if(Confirmation == "o"){
                    MontantRestantARetirer = MontantRestantARetirer - (NbCoupures * Coupures)
                   println("Il reste " + MontantRestantARetirer + " CHF à retirer")
                   BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + " CHF")
                 }else if(Confirmation.nonEmpty && Confirmation.forall(_.isDigit)){
                   var ChoixNbCoupures = Confirmation.toInt
                   if(ChoixNbCoupures <= NbCoupures && ChoixNbCoupures >= 0){
                     NbCoupures = ChoixNbCoupures
                     MontantRestantARetirer = MontantRestantARetirer - (NbCoupures * Coupures)

                     if(ChoixNbCoupures > 0){
                        println("Il reste " + MontantRestantARetirer + " CHF à retirer")
                        BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + " CHF")
                     }
                     else{
                       println("Il reste " + MontantRestantARetirer + " CHF à retirer")
                     }
                   }else{
                       println("Veuillez saisir une valeur inférieure ou égale à " + NbCoupures)
                     }
                   }
             }

             Z += 1
           }

        ValeursDesComptes(Identifiant) = ValeursDesComptes(Identifiant) - MTRetrait
        println("Veuillez retirer la somme demandé : ")
        for(i <- BilletDonnerAUtilisateur){
          println(i)
        }
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ValeursDesComptes(Identifiant))
         }

    
    if(Devise == 2){

    var CoupuresDispoEUR: Array[Int] = Array (100, 50, 20, 10)


      var Z = 0
      var MontantRestantARetirer = MTRetrait
      var BilletDonnerAUtilisateur = ArrayBuffer[String]()


       while(Z < CoupuresDispoEUR.length && MontantRestantARetirer > 0){
         var Coupures = CoupuresDispoEUR(Z) 
         var NbCoupures = (MontantRestantARetirer / Coupures).toInt

          if(Coupures == 10 && NbCoupures > 0){
            MontantRestantARetirer = MontantRestantARetirer - Coupures * NbCoupures
            BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + "EUR")
          }

           else if(MontantRestantARetirer / CoupuresDispoEUR(Z) >= 1){
            println("Vous pouvez obtenir au maximum " + NbCoupures + " billets de " + Coupures + " EUR")
              var Confirmation = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée : ").toString


               if(Confirmation == "o"){
                  MontantRestantARetirer = MontantRestantARetirer - (NbCoupures * Coupures)
                 println("Il reste " + MontantRestantARetirer + " EUR à retirer")
                 BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + " EUR")
               }else if(Confirmation.nonEmpty && Confirmation.forall(_.isDigit)){
                 var ChoixNbCoupures = Confirmation.toInt
                 if(ChoixNbCoupures <= NbCoupures && ChoixNbCoupures >= 0){
                   NbCoupures = ChoixNbCoupures
                   MontantRestantARetirer = MontantRestantARetirer - (NbCoupures * Coupures)

                   if(ChoixNbCoupures > 0){
                      println("Il reste " + MontantRestantARetirer + " EUR à retirer")
                      BilletDonnerAUtilisateur += (NbCoupures + " billet(s) de " + Coupures + " EUR")
                   }
                   else{
                     println("Il reste " + MontantRestantARetirer + " EUR à retirer")
                   }
                 }else{
                     println("Veuillez saisir une valeur inférieure ou égale à " + NbCoupures)
                   }
                 }
           }

           Z += 1
         }
      ValeursDesComptes(Identifiant) = ValeursDesComptes(Identifiant) - (MTRetrait * 0.95)
      println("Veuillez retirer la somme demandé : ")
      for(i <- BilletDonnerAUtilisateur){
        println(i)
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", ValeursDesComptes(Identifiant))
       } 
  }
}
