//Assignment: Alam Ahmed_996498_assignsubmission_file

object Main {
  import scala.io.StdIn._
  def main(args: Array[String]): Unit = {
    var comptes = Array.fill(100)(1200.0)
    var codespin = Array.fill(100)("INTRO1234")
    var nbclients = 100
    var idClient = 0
    // Programme principal
    var choixOperation = ""
    var etape = 0
    while(choixOperation != "6"){
      if(etape == 0){
        idClient = readLine("Saisissez votre code identifiant > ").toInt
        if (idClient < 0 || idClient >= nbclients){
          //println("Cet identifiant n'est pas valable.")
          etape = 2
        }else{
          var pinCorrect = false
          var tentativesCodePin = 0

          while (!pinCorrect && tentativesCodePin < 3){
            print("Saisissez votre code pin > ")
            val pinEntre = readLine()
            if(codespin(idClient) == pinEntre){
              pinCorrect = true
              etape = 1

            }else{
              tentativesCodePin += 1
              println(s"Code pin erroné, il vous reste ${3 - tentativesCodePin} tentatives.")
            }
          }
          if(!pinCorrect) {
            println("Trop d'erreurs, abandon de l'identification")
            etape = 0
          }
        } 
      }else if(etape == 1){
        println("\nChoisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer")
        choixOperation = readLine("Votre choix : ")

        if(choixOperation == "1"){
          depot(idClient, comptes)
        }else if(choixOperation == "2"){
          retrait(idClient, comptes)
        }else if(choixOperation == "3"){
          printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(idClient), " CHF")
        }else if(choixOperation == "4"){
          ChangementPin(idClient, codespin )
        }else if(choixOperation == "5"){
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          etape = 0
        }
      }else if(etape == 2){
        println("Cet identifiant n'est pas valable.")
        choixOperation = "6"
      }
      
    }
   
  }//ferme def main

  //--------------------------------------Depot--------------------------------------------------------
  def depot(id: Int, comptes: Array[Double]): Unit = {
    print("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
    val devise = readLine().toInt
    print("Indiquez le montant du dépôt > ")
    var montant = readLine().toDouble

    while ((montant % 10 != 0) || (montant < 10)) {
      println("Le montant doit être un multiple de 10.")
      montant = readLine().toDouble
    }

    if (devise == 2) {
      comptes(id) += (montant * 0.95)
    } else if (devise == 1) {
      comptes(id) += montant
    }

    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }
//-----------------------------------------Retrait---------------------------------------------------
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    val plafondRetraitAutorise = 0.1
    var billet500 = 0
    var billet200 = 0
    var billet100 = 0
    var billet50 = 0
    var billet20 = 0
    var billet10 = 0
    var nbCoupuresMax = 0
    //CHoix devise
    var devise = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt
     while((devise != 1)&&(devise!= 2)){
     devise = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt
     }
    //Choix du montant
    var montant = readLine("Indiquez le montant du retrait > ").toDouble
    var montantDemande = montant

      while(montantDemande % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
        montantDemande = readLine("Indiquez le montant du retrait > ").toDouble
      }
      val montantMaxRetrait = (comptes(id) * plafondRetraitAutorise).toInt
      while(montantDemande > montantMaxRetrait) {
        println("Votre plafond de retrait autorisé est de : "+ montantMaxRetrait+" CHF")
        montantDemande = readLine("Indiquez le montant du retrait > ").toDouble
      }
    var petitescoupures = Array(100, 50, 20)
    var choixcoupures = 0

    if(devise == 1){
        if(montant >= 200){
          choixcoupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt  
           while((choixcoupures!= 1)&&(choixcoupures!=2)){
              choixcoupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt 
           }
          if(choixcoupures == 1){
            var coupuresDisponibles = Array(500, 200, 100, 50, 20)
            for (coupure <- coupuresDisponibles){

              nbCoupuresMax = (montantDemande / coupure).toInt

              if (nbCoupuresMax >= 1){
                println("Il reste "+ montantDemande+ " CHF à distribuer")
                print("Vous pouvez obtenir au maximum " + nbCoupuresMax+ " billet(s) de "+coupure+ " CHF. Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
                val choixUtilisateur = readLine()

                if (choixUtilisateur == "o"){
                  montantDemande -= nbCoupuresMax * coupure
                  if(coupure == 500){
                    billet500 = nbCoupuresMax 
                  }else if(coupure == 200){
                    billet200 = nbCoupuresMax 
                  }else if(coupure == 100){
                    billet100 = nbCoupuresMax 
                  }else if(coupure == 50){
                    billet50 = nbCoupuresMax 
                  }else if(coupure == 20){
                    billet20 = nbCoupuresMax 

                  }
                }else if((0 <= (choixUtilisateur).toInt)&&((choixUtilisateur).toInt <= nbCoupuresMax)){
                  montantDemande -= (choixUtilisateur).toInt * coupure

                  if(coupure == 500){
                    billet500 = (choixUtilisateur).toInt
                  }else if(coupure == 200){
                    billet200 = (choixUtilisateur).toInt 
                  }else if(coupure == 100){
                    billet100 = (choixUtilisateur).toInt 
                  }else if(coupure == 50){
                    billet50 = (choixUtilisateur).toInt 
                  }else if(coupure == 20){
                    billet20 = (choixUtilisateur).toInt 
                  }
                }else{
                  println("Choix invalide. Saisie annulée.")
                }
              }
            }
              billet10 = (montantDemande / 10).toInt
          }
        }else if((montant < 200)||(choixcoupures == 2)){
          for (coupure <- petitescoupures){
              nbCoupuresMax = (montantDemande / coupure).toInt

              if (nbCoupuresMax > 0){
                println("Il reste "+ montantDemande+ " CHF à distribuer")
                print("Vous pouvez obtenir au maximum " + nbCoupuresMax+ " billet(s) de "+coupure+ " CHF. Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")
                val choixUtilisateur = readLine()

                if (choixUtilisateur == "o"){
                  montantDemande -= nbCoupuresMax * coupure
                  if(coupure == 100){
                    billet100 = nbCoupuresMax 
                  }else if(coupure == 50){
                    billet50 = nbCoupuresMax 
                  }else if(coupure == 20){
                    billet20 = nbCoupuresMax
                  }
                }else if((0 <= (choixUtilisateur).toInt)&&((choixUtilisateur).toInt <= nbCoupuresMax)){
                  montantDemande -= (choixUtilisateur).toInt * coupure
                  if(coupure == 100){
                    billet100 = (choixUtilisateur).toInt 
                  }else if(coupure == 50){
                    billet50 = (choixUtilisateur).toInt 
                  }else if(coupure == 20){
                    billet20 = (choixUtilisateur).toInt 
                  }
                }else{
                  println("Choix invalide. Saisie annulée.")
                }
              }
          }
              billet10 = (montantDemande / 10).toInt
        } 
      }else if(devise == 2){
        for (coupure <- petitescoupures){
          nbCoupuresMax = (montantDemande / coupure).toInt

          if (nbCoupuresMax > 0){
            println("Il reste "+ montantDemande+ " CHF à distribuer")
            print("Vous pouvez obtenir au maximum " + nbCoupuresMax+ " billet(s) de "+coupure+ " CHF. Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée > ")

            val choixUtilisateur = readLine()

            if (choixUtilisateur == "o"){
              montantDemande -= nbCoupuresMax * coupure
              if(coupure == 100){
               billet100 = nbCoupuresMax 
              }else if(coupure == 50){
                billet50 = nbCoupuresMax 
              }else if(coupure == 20){
               billet20 = nbCoupuresMax
              }
            }else if((0 <= (choixUtilisateur).toInt)&&((choixUtilisateur).toInt <= nbCoupuresMax)){
              montantDemande -= (choixUtilisateur).toInt * coupure
              if(coupure == 100){
                billet100 = (choixUtilisateur).toInt 
              }else if(coupure == 50){
                billet50 = (choixUtilisateur).toInt 
              }else if(coupure == 20){
                billet20 = (choixUtilisateur).toInt 
              }
            }else{
              println("Choix invalide. Saisie annulée.")
            }
          }
        }
        billet10 = (montantDemande / 10).toInt
      }
      //Affichage du retrait CHF
    if(devise == 1){
       println("Veuillez retirer la somme demandée : ")
       if(billet500 > 0){
         println((billet500).toString +" billet(s) de 500 CHF")
       }
       if(billet200 > 0){
         println((billet200).toString +" billet(s) de 200 CHF")
       }
       if(billet100 > 0){
         println((billet100).toString +" billet(s) de 100 CHF")
       }
       if(billet50 > 0){
         println((billet50).toString +" billet(s) de 50 CHF")
       }
       if(billet20 > 0){
         println((billet20).toString +" billet(s) de 20 CHF")
       }
       if(billet10 > 0){
       println((billet10).toString +" billet(s) de 10 CHF")
       }
       comptes(id) -= montant 
    }else if(devise ==2){
      println("Veuillez retirer la somme demandée : ")
       if(billet100 > 0){
         println((billet100).toString +" billet(s) de 100 EUR")
       }
       if(billet50 > 0){
         println((billet50).toString +" billet(s) de 50 EUR")
       }
       if(billet20 > 0){
         println((billet20).toString +" billet(s) de 20 EUR")
       }
       if(billet10 > 0){
       println((billet10).toString +" billet(s) de 10 EUR")
       }
       comptes(id) -= (montant*0.95) 
    }
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id), " CHF")
  }
  //-------------------------------Changement mdp--------------------------------------------------
  def ChangementPin(id: Int, codespin: Array[String]) : Unit = {
    var NouveauPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var NouveauPinTaille = NouveauPin.toCharArray
   while(NouveauPinTaille.length < 8){
     println("Votre code pin ne contient pas au moins 8 caractères")
     NouveauPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
     NouveauPinTaille = NouveauPin.toCharArray
   }
    codespin(id) = NouveauPin
    println("Votre mot de passe a été mis à jour")
  }
}