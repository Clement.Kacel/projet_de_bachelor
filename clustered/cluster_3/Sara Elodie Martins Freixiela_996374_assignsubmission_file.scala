//Assignment: Sara Elodie Martins Freixiela_996374_assignsubmission_file

import scala.io.StdIn._

object Main {
  val nbClients = 100 
  var montantDisponible = 1200.0
  var comptes = Array.fill(nbClients)(montantDisponible)
  var codespinClient = Array.fill(nbClients)("INTRO1234")
  var valeurDeTaux = 0.95
  var codespin1 = new Array[String](nbClients)
  def verifierIdentification(identifiantAverifier: Int, codePinAverifier: String): Boolean = {
    var continuer = true
    var identifiant = identifiantAverifier
    var codepin = codePinAverifier
    if (codePinAverifier == codespinClient(identifiantAverifier)){
      return true
    } else return false 
  }
  def depot(identifiant:Int, comptes:Array[Double]) : Unit = {
    var montantDepotAverifier = 0.0
    var deviseAverifer = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR : ").toInt
    montantDepotAverifier = readLine("Indiquez le montant du dépôt : ").toDouble
    while (!(montantDepotAverifier % 10 == 0)) {
      println("Le montant doit être un multiple de 10.")
      montantDepotAverifier = readDouble()
    }
    if (montantDepotAverifier % 10 == 0){
      if (deviseAverifer == 1){ 
        comptes(identifiant) = comptes(identifiant) + montantDepotAverifier
      }
      else if (deviseAverifer == 2){ 
        var EUR = montantDepotAverifier
        var totalCHF = EUR * valeurDeTaux
        comptes(identifiant) = comptes(identifiant) + totalCHF
      }
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(identifiant))
    }
  } // fermer méthode dépôt
  def retrait(identifiant:Int, comptes:Array[Double]) : Unit = {
    var montantRetraitAverifier = 0.0
    var coupure = 0
    var nbBillets = 0
    var coupureAcceptee = 0
    var valeurAcceptee = 0
    var coupure6 = 500
    var coupure7 = 200
    var coupure8 = 100
    var coupure9 = 50
    var coupure10 = 20
    var nbBillets6 = 0
    var nbBillets7 = 0
    var nbBillets8 = 0
    var nbBillets9 = 0
    var nbBillets10 = 0
    var deviseAverifer = readLine("Indiquez la devise : 1) CHF ; 2) EUR : ").toInt
      while (!(deviseAverifer == 1 || deviseAverifer == 2)){ 
        println("Votre sélection n'est pas correcte.")
        println("Indiquez la devise : 1) CHF ; 2) EUR : ")
        deviseAverifer = readInt()
      }
      if (deviseAverifer == 1 || deviseAverifer == 2){
        println("Indiquez le montant du retrait : ")
        montantRetraitAverifier = readDouble()
      }
      var retraitAutorise = (comptes(identifiant) * 0.1).toDouble 
      while (montantRetraitAverifier > retraitAutorise){
        println("Votre plafond de retrait autorisé est de : " + retraitAutorise + "CHF. Veuillez saisir un autre montant : ") 
        montantRetraitAverifier = readDouble()
      }
      while (!(montantRetraitAverifier % 10 == 0)){ 
        println("Le montant doit être un multiple de 10. Veuillez saisir un autre montant : ")
        montantRetraitAverifier = readDouble()
      }
      if (deviseAverifer == 1){ 
        println("En 1) grosses coupures, 2) petites coupures : ")
        var choixCoupures = readInt()
        while ((choixCoupures != 1) && (choixCoupures != 2)){ 
            (montantRetraitAverifier >= 200)
            println("En 1) grosses coupures, 2) petites coupures : ")
            choixCoupures = readInt()
        }
        if ((montantRetraitAverifier >= 200) && (choixCoupures == 1)){ 
          coupure = 500
          var reste = montantRetraitAverifier.toInt 
          if (montantRetraitAverifier >= 500){ 
            if (montantRetraitAverifier % coupure != montantRetraitAverifier){ 
              reste = (montantRetraitAverifier % coupure).toInt
              nbBillets = ((montantRetraitAverifier - reste) / coupure).toInt
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets + " billet(s) de " + coupure + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){ 
                    reste = (montantRetraitAverifier - (nbBillets * coupure)).toInt
                  }
                  else { 
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets){ 
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets)
                      valeurAcceptee = readInt()
                    }
                    if (valeurAcceptee < nbBillets) { 
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure)).toInt 
                      nbBillets = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 500.")
                    }
                  }
                montantRetraitAverifier = reste 
              } while ((valeurAcceptee > nbBillets) && (reste > coupure))
            }
          }
          var coupure1 = 200
          var nbBillets1 = 0
          if (reste >= 200){ 
            if (reste % coupure1 != reste){ 
              reste = (montantRetraitAverifier % coupure1).toInt 
              nbBillets1 = ((montantRetraitAverifier - reste) / coupure1).toInt 
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets1 + " billet(s) de " + coupure1 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets1 * coupure1)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets1){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets1)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets1) {
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure1)).toInt
                        nbBillets1 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 200.")
                      }
                }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets1) && (reste > coupure1))
            }
          }
          var coupure2 = 100
          var nbBillets2 = 0
          if (reste >= 100){ 
            if (reste % coupure2 != reste){ 
              reste = (montantRetraitAverifier % coupure2).toInt
              nbBillets2 = ((montantRetraitAverifier - reste) / coupure2).toInt 
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets2 + " billet(s) de " + coupure2 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets2 * coupure2)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets2){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets2)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets2){
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure2)).toInt
                        nbBillets2 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 100.")
                      }
                  }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets2) && (reste > coupure2))
            }
          }
          var coupure3 = 50
          var nbBillets3 = 0
          if (reste >= 50){ 
            if (reste % coupure3 != reste){ 
              reste = (montantRetraitAverifier % coupure3).toInt
              nbBillets3 = ((montantRetraitAverifier - reste) / coupure3).toInt
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets3 + " billet(s) de " + coupure3 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets3 * coupure3)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets3){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à" + nbBillets3)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets3){
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure3)).toInt
                        nbBillets3 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 50.")
                      }
                  }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets3) && (reste > coupure3))
            }
          }
          var coupure4 = 20
          var nbBillets4 = 0
          if (reste >= 20){
            if (reste % coupure4 != reste){ 
              reste = (montantRetraitAverifier % coupure4).toInt
              nbBillets4 = ((montantRetraitAverifier - reste) / coupure4).toInt 
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets4 + " billet(s) de " + coupure4 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets4 * coupure4)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets4){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets4)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets4){
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure4)).toInt
                        nbBillets4 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 20.")
                      }
                  }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets4) && (reste > coupure4))
            }
          } 
          if (reste == montantRetraitAverifier){ // PROBLEME AVEC CETTE CONDITION
            coupure6 = 500
            reste = montantRetraitAverifier.toInt
            nbBillets6 = 0
            if (montantRetraitAverifier >= 500){
              if (montantRetraitAverifier % coupure6 != montantRetraitAverifier){
                reste = (montantRetraitAverifier % coupure6).toInt
                nbBillets6 = ((montantRetraitAverifier - reste) / coupure6).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets6 + " billet(s) de " + coupure6 + " CHF.")
                reste = (montantRetraitAverifier - (nbBillets6 * coupure6)).toInt
                montantRetraitAverifier = reste
              }
            }
            coupure7 = 200
            nbBillets7 = 0
            if (reste >= 200){
              if (reste % coupure7 != reste){
                reste = (montantRetraitAverifier % coupure7).toInt
                nbBillets7 = ((montantRetraitAverifier - reste) / coupure7).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets7 + " billet(s) de " + coupure7 + " CHF.")
                reste = (montantRetraitAverifier - (nbBillets7 * coupure7)).toInt
                montantRetraitAverifier = reste
              }
            }
            coupure8 = 100
            nbBillets8 = 0
            if (reste >= 100){
              if (reste % coupure8 != reste){
                reste = (montantRetraitAverifier % coupure8).toInt
                nbBillets8 = ((montantRetraitAverifier - reste) / coupure8).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets8 + " billet(s) de " + coupure8 + " CHF.")
                reste = (montantRetraitAverifier - (nbBillets8 * coupure8)).toInt
                montantRetraitAverifier = reste
              }
            }
            coupure9 = 50
            nbBillets9 = 0
            if (reste >= 50){
              if (reste % coupure9 != reste){
                reste = (montantRetraitAverifier % coupure9).toInt
                nbBillets9 = ((montantRetraitAverifier - reste) / coupure9).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets9 + " billet(s) de " + coupure9 + " CHF.")
                reste = (montantRetraitAverifier - (nbBillets9 * coupure9)).toInt
                montantRetraitAverifier = reste
              }
            }
            coupure10 = 20
            nbBillets10 = 0
            if (reste >= 20){
              if (reste % coupure10 != reste){
                reste = (montantRetraitAverifier % coupure10).toInt
                nbBillets10 = ((montantRetraitAverifier - reste) / coupure10).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets10 + " billet(s) de " + coupure10 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                if (coupureAcceptee == "o"){
                  reste = (montantRetraitAverifier - (nbBillets10 * coupure10)).toInt
                }
                else {
                  valeurAcceptee = coupureAcceptee.toInt
                  while (valeurAcceptee > nbBillets10){
                    println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets10)
                    valeurAcceptee = readInt()
                  }
                    if (valeurAcceptee < nbBillets10){
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure10)).toInt
                      nbBillets10 = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 20.")
                    }
                }
                montantRetraitAverifier = reste
              }
            }
          }
          var coupure5 = 10
          var nbBillets5 = 0
              if (reste >= 10){ 
                if (reste % coupure5 != reste){ 
                  reste = (montantRetraitAverifier % coupure5).toInt
                  nbBillets5 = ((montantRetraitAverifier - reste) / coupure5).toInt
                do {
                  println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                  println("Vous pouvez obtenir au maximum " + nbBillets5 + " billet(s) de " + coupure5 + " CHF.")
                  println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                  var coupureAcceptee = readLine()
                    if (coupureAcceptee == "o"){
                      reste = (montantRetraitAverifier - (nbBillets5 * coupure5)).toInt
                    }
                    else {
                      valeurAcceptee = coupureAcceptee.toInt
                      while (valeurAcceptee > nbBillets5){
                        println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets5)
                        valeurAcceptee = readInt()
                      }
                      if (valeurAcceptee < nbBillets5) {
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure5)).toInt
                        nbBillets5 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 10.")
                      }
                  }
                } while ((valeurAcceptee > nbBillets5) && (reste > coupure5))
            }
          }
          println("Veuillez retirer la somme demandée : ")
          if (nbBillets != 0) println(nbBillets + " billet(s) de " + coupure + " CHF.")
          if (nbBillets1 != 0) println(nbBillets1 + " billet(s) de " + coupure1 + " CHF.")
          if (nbBillets2 != 0) println(nbBillets2 + " billet(s) de " + coupure2 + " CHF.")
          if (nbBillets3 != 0) println(nbBillets3 + " billet(s) de " + coupure3 + " CHF.")
          if (nbBillets4 != 0) println(nbBillets4 + " billet(s) de " + coupure4 + " CHF.")
          if (nbBillets6 != 0) println(nbBillets6 + " billet(s) de " + coupure6 + " CHF.")
          if (nbBillets7 != 0) println(nbBillets7 + " billet(s) de " + coupure7 + " CHF.")
          if (nbBillets8 != 0) println(nbBillets8 + " billet(s) de " + coupure8 + " CHF.")
          if (nbBillets9 != 0) println(nbBillets9 + " billet(s) de " + coupure9 + " CHF.")
          if (nbBillets10 != 0) println(nbBillets10 + " billet(s) de " + coupure10 + " CHF.")
          if (nbBillets5 != 0) println(nbBillets5 + " billet(s) de " + coupure5 + " CHF.")
          montantRetraitAverifier = (nbBillets * coupure) + (nbBillets1 * coupure1) + (nbBillets2 * coupure2) + (nbBillets3 * coupure3) + (nbBillets4 * coupure4) + (nbBillets5 * coupure5) + (nbBillets6 * coupure6) + (nbBillets7 * coupure7) + (nbBillets8 * coupure8) + (nbBillets9 * coupure9) + (nbBillets10 * coupure10)
        } // fermer montant > 200
        if ((montantRetraitAverifier < 200) || (choixCoupures == 2)){ 
          coupure = 100
          var reste = montantRetraitAverifier.toInt
          nbBillets = 0
          if (montantRetraitAverifier >= 100){
            if (montantRetraitAverifier % coupure != montantRetraitAverifier){
              reste = (montantRetraitAverifier % coupure).toInt
              nbBillets = ((montantRetraitAverifier - reste) / coupure).toInt
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets + " billet(s) de " + coupure + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets * coupure)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets){
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure)).toInt
                        nbBillets = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 100.")
                      }
                  }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets) && (reste > coupure))
            }
          }
          var coupure1 = 50
          var nbBillets1 = 0
          if (reste >= 50){
            if (reste % coupure1 != reste){
              reste = (montantRetraitAverifier % coupure1).toInt
              nbBillets1 = ((montantRetraitAverifier - reste) / coupure1).toInt
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets1 + " billet(s) de " + coupure1 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets1 * coupure1)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets1){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets1)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets1) {
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure1)).toInt
                        nbBillets1 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 50.")
                      }
                  }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets1) && (reste > coupure1))
            }
          }
          var coupure2 = 20
          var nbBillets2 = 0
          if (reste >= 20){
            if (reste % coupure2 != reste){
              reste = (montantRetraitAverifier % coupure2).toInt
              nbBillets2 = ((montantRetraitAverifier - reste) / coupure2).toInt
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets2 + " billet(s) de " + coupure2 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                    if (coupureAcceptee == "o"){
                      reste = (montantRetraitAverifier - (nbBillets2 * coupure2)).toInt
                    }
                    else {
                      valeurAcceptee = coupureAcceptee.toInt
                      while (valeurAcceptee > nbBillets2){
                        println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets2)
                        valeurAcceptee = readInt()
                      }
                        if (valeurAcceptee < nbBillets2) {
                          reste = (montantRetraitAverifier - (valeurAcceptee * coupure2)).toInt
                          nbBillets2 = valeurAcceptee
                          println("Vous avez choisi " + valeurAcceptee + " billet(s) de 20.")
                        }
                    }
                montantRetraitAverifier = reste
              } while ((valeurAcceptee > nbBillets2) && (reste > coupure2))
            }
          }
          if (reste == montantRetraitAverifier){
            coupure8 = 100
            nbBillets8 = 0
            if (reste >= 100){
              if (reste % coupure8 != reste){
                reste = (montantRetraitAverifier % coupure8).toInt
                nbBillets8 = ((montantRetraitAverifier - reste) / coupure8).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets8 + " billet(s) de " + coupure8 + " CHF.")
                reste = (montantRetraitAverifier - (nbBillets8 * coupure8)).toInt
                montantRetraitAverifier = reste
              }
            }
            coupure9 = 50
            nbBillets9 = 0
            if (reste >= 50){
              if (reste % coupure9 != reste){
                reste = (montantRetraitAverifier % coupure9).toInt
                nbBillets9 = ((montantRetraitAverifier - reste) / coupure9).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets9 + " billet(s) de " + coupure9 + " CHF.")
                reste = (montantRetraitAverifier - (nbBillets9 * coupure9)).toInt
                montantRetraitAverifier = reste
              }
            }
            coupure10 = 20
            nbBillets10 = 0
            if (reste >= 20){
              if (reste % coupure10 != reste){
                reste = (montantRetraitAverifier % coupure10).toInt
                nbBillets10 = ((montantRetraitAverifier - reste) / coupure10).toInt
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets10 + " billet(s) de " + coupure10 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                if (coupureAcceptee == "o"){
                  reste = (montantRetraitAverifier - (nbBillets10 * coupure10)).toInt
                }
                else {
                  valeurAcceptee = coupureAcceptee.toInt
                  while (valeurAcceptee > nbBillets10){
                    println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets10)
                    valeurAcceptee = readInt()
                  }
                    if (valeurAcceptee < nbBillets10){
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure10)).toInt
                      nbBillets10 = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 20.")
                    }
                }
                montantRetraitAverifier = reste
              }
            }
          }
          var coupure3 = 10
          var nbBillets3 = 0

          if (reste >= 10){
            if (reste % coupure3 != reste){
              reste = (montantRetraitAverifier % coupure3).toInt
              nbBillets3 = ((montantRetraitAverifier - reste) / coupure3).toInt
              do {
                println("Il reste " + montantRetraitAverifier + " CHF à distribuer.")
                println("Vous pouvez obtenir au maximum " + nbBillets3 + " billet(s) de " + coupure3 + " CHF.")
                println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
                var coupureAcceptee = readLine()
                  if (coupureAcceptee == "o"){
                    reste = (montantRetraitAverifier - (nbBillets3 * coupure3)).toInt
                  }
                  else {
                    valeurAcceptee = coupureAcceptee.toInt
                    while (valeurAcceptee > nbBillets3){
                      println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets3)
                      valeurAcceptee = readInt()
                    }
                      if (valeurAcceptee < nbBillets3) {
                        reste = (montantRetraitAverifier - (valeurAcceptee * coupure3)).toInt
                        nbBillets3 = valeurAcceptee
                        println("Vous avez choisi " + valeurAcceptee + " billet(s) de 10.")
                      }
                  }
              } while ((valeurAcceptee > nbBillets3) && (reste > coupure3))
            }
          }
          println("Veuillez retirer la somme demandée : ")
          if (nbBillets != 0) println(nbBillets + " billet(s) de " + coupure + " CHF.")
          if (nbBillets1 != 0) println(nbBillets1 + " billet(s) de " + coupure1 + " CHF.")
          if (nbBillets2 != 0) println(nbBillets2 + " billet(s) de " + coupure2 + " CHF.")
          if (nbBillets8 != 0) println(nbBillets8 + " billet(s) de " + coupure8 + " CHF.")
          if (nbBillets9 != 0) println(nbBillets9 + " billet(s) de " + coupure9 + " CHF.")
          if (nbBillets10 != 0) println(nbBillets10 + " billet(s) de " + coupure10 + " CHF.")
          if (nbBillets3 != 0) println(nbBillets3 + " billet(s) de " + coupure3 + " CHF.")

          montantRetraitAverifier = (nbBillets * coupure) + (nbBillets1 * coupure1) + (nbBillets2 * coupure2) + (nbBillets3 * coupure3) + (nbBillets8 * coupure8) + (nbBillets9 * coupure9) + (nbBillets10 * coupure10)
        }
        comptes(identifiant) = comptes(identifiant) - montantRetraitAverifier
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(identifiant))
      } // fermer devise 1
    if (deviseAverifer == 2){
        var coupure = 100
        var reste = montantRetraitAverifier.toInt
        var nbBillets = 0
        if (montantRetraitAverifier >= 100){
          if (montantRetraitAverifier % coupure != montantRetraitAverifier){
            reste = (montantRetraitAverifier % coupure).toInt
            nbBillets = ((montantRetraitAverifier - reste) / coupure).toInt
            do {
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets + " billet(s) de " + coupure + " EUR.")
              println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
              var coupureAcceptee = readLine()
                if (coupureAcceptee == "o"){
                  reste = (montantRetraitAverifier - (nbBillets * coupure)).toInt
                }
                else {
                  valeurAcceptee = coupureAcceptee.toInt
                  while (valeurAcceptee > nbBillets){
                    println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets)
                    valeurAcceptee = readInt()
                  }
                    if (valeurAcceptee < nbBillets){
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure)).toInt
                      nbBillets = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 100.")
                    }
                }
              montantRetraitAverifier = reste
            } while ((valeurAcceptee > nbBillets) && (reste > coupure))
          }
        }
        var coupure1 = 50
        var nbBillets1 = 0
        if (reste >= 50){
          if (reste % coupure1 != reste){
            reste = (montantRetraitAverifier % coupure1).toInt
            nbBillets1 = ((montantRetraitAverifier - reste) / coupure1).toInt
            do {
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets1 + " billet(s) de " + coupure1 + " EUR.")
              println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
              var coupureAcceptee = readLine()
                if (coupureAcceptee == "o"){
                  reste = (montantRetraitAverifier - (nbBillets1 * coupure1)).toInt
                }
                else {
                  valeurAcceptee = coupureAcceptee.toInt
                  while (valeurAcceptee > nbBillets1){
                    println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à" + nbBillets1)
                    valeurAcceptee = readInt()
                  }
                    if (valeurAcceptee < nbBillets1){
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure1)).toInt
                      nbBillets1 = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 50.")
                    }
                }
              montantRetraitAverifier = reste
            } while ((valeurAcceptee > nbBillets1) && (reste > coupure1))
          }
        }
        var coupure2 = 20
        var nbBillets2 = 0
        if (reste >= 20){
          if (reste % coupure2 != reste){
            reste = (montantRetraitAverifier % coupure2).toInt
            nbBillets2 = ((montantRetraitAverifier - reste) / coupure2).toInt
            do {
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets2 + " billet(s) de " + coupure2 + " EUR.")
              println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
              var coupureAcceptee = readLine()
                if (coupureAcceptee == "o"){
                  reste = (montantRetraitAverifier - (nbBillets2 * coupure2)).toInt
                }
                else {
                  valeurAcceptee = coupureAcceptee.toInt
                  while (valeurAcceptee > nbBillets2){
                    println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets2)
                    valeurAcceptee = readInt()
                  }
                    if (valeurAcceptee < nbBillets2){
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure2)).toInt
                      nbBillets2 = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 20.")
                    }
                }
              montantRetraitAverifier = reste
            } while ((valeurAcceptee > nbBillets2) && (reste > coupure2))
          }
        }
        if (reste == montantRetraitAverifier){
          coupure8 = 100
          nbBillets8 = 0
          if (reste >= 100){
            if (reste % coupure8 != reste){
              reste = (montantRetraitAverifier % coupure8).toInt
              nbBillets8 = ((montantRetraitAverifier - reste) / coupure8).toInt
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets8 + " billet(s) de " + coupure8 + " EUR.")
              reste = (montantRetraitAverifier - (nbBillets8 * coupure8)).toInt
              montantRetraitAverifier = reste
            }
          }
          coupure9 = 50
          nbBillets9 = 0
          if (reste >= 50){
            if (reste % coupure9 != reste){
              reste = (montantRetraitAverifier % coupure9).toInt
              nbBillets9 = ((montantRetraitAverifier - reste) / coupure9).toInt
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets9 + " billet(s) de " + coupure9 + " EUR.")
              reste = (montantRetraitAverifier - (nbBillets9 * coupure9)).toInt
              montantRetraitAverifier = reste
            }
          }
          coupure10 = 20
          nbBillets10 = 0
          if (reste >= 20){
            if (reste % coupure10 != reste){
              reste = (montantRetraitAverifier % coupure10).toInt
              nbBillets10 = ((montantRetraitAverifier - reste) / coupure10).toInt
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets10 + " billet(s) de " + coupure10 + " EUR.")
              println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
              var coupureAcceptee = readLine()
              if (coupureAcceptee == "o"){
                reste = (montantRetraitAverifier - (nbBillets10 * coupure10)).toInt
              }
              else {
                valeurAcceptee = coupureAcceptee.toInt
                while (valeurAcceptee > nbBillets10){
                  println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets10)
                  valeurAcceptee = readInt()
                }
                  if (valeurAcceptee < nbBillets10){
                    reste = (montantRetraitAverifier - (valeurAcceptee * coupure10)).toInt
                    nbBillets10 = valeurAcceptee
                    println("Vous avez choisi " + valeurAcceptee + " billet(s) de 20.")
                  }
              }
              montantRetraitAverifier = reste
            }
          }
        }
        var coupure3 = 10
        var nbBillets3 = 0
        if (reste >= 10){
          if (reste % coupure3 != reste){
            reste = (montantRetraitAverifier % coupure3).toInt
            nbBillets3 = ((montantRetraitAverifier - reste) / coupure3).toInt
            do {
              println("Il reste " + montantRetraitAverifier + " EUR à distribuer.")
              println("Vous pouvez obtenir au maximum " + nbBillets3 + " billet(s) de " + coupure3 + " EUR.")
              println("Tapez o pour ok ou une valeur inférieur à celle proposée.")
              var coupureAcceptee = readLine()
                if (coupureAcceptee == "o"){
                  reste = (montantRetraitAverifier - (nbBillets3 * coupure3)).toInt
                }
                else {
                  valeurAcceptee = coupureAcceptee.toInt
                  while (valeurAcceptee > nbBillets3){
                    println("Ce n'est pas possible. Veuillez saisir une valeur inférieur ou égale à " + nbBillets3)
                    valeurAcceptee = readInt()
                  }
                    if (valeurAcceptee < nbBillets3) {
                      reste = (montantRetraitAverifier - (valeurAcceptee * coupure3)).toInt
                      nbBillets3 = valeurAcceptee
                      println("Vous avez choisi " + valeurAcceptee + " billet(s) de 10.")
                    }
                }
            } while ((valeurAcceptee > nbBillets3) && (reste > coupure3))
          }
        }
        println("Veuillez retirer la somme demandée : ")
        if (nbBillets != 0) println(nbBillets + " billet(s) de " + coupure + " EUR.")
        if (nbBillets1 != 0) println(nbBillets1 + " billet(s) de " + coupure1 + " EUR.")
        if (nbBillets2 != 0) println(nbBillets2 + " billet(s) de " + coupure2 + " EUR.")
        if (nbBillets8 != 0) println(nbBillets8 + " billet(s) de " + coupure8 + " EUR.")
        if (nbBillets9 != 0) println(nbBillets9 + " billet(s) de " + coupure9 + " EUR.")
        if (nbBillets10 != 0) println(nbBillets10 + " billet(s) de " + coupure10 + " EUR.")
        if (nbBillets3 != 0) println(nbBillets3 + " billet(s) de " + coupure3 + " EUR.")

      montantRetraitAverifier = (nbBillets * coupure) + (nbBillets1 * coupure1) + (nbBillets2 * coupure2) + (nbBillets3 * coupure3) + (nbBillets8 * coupure8) + (nbBillets9 * coupure9) + (nbBillets10 * coupure10)
      var EUR = montantRetraitAverifier
      var totalCHF = EUR * valeurDeTaux
      comptes(identifiant) -= totalCHF
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(identifiant))
    }
  } // méthode fermer retrait
  def changepin(id: Int, codespinClient: Array[String]): Unit = {
    var identifiant = id
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) : ")
    var nouveauCodePin = readLine()
    while (nouveauCodePin.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères.")
      nouveauCodePin = readLine()
    }
    if (nouveauCodePin.length >= 8){
      codespinClient(id) = nouveauCodePin
    }
  } // fermer méthode changepin      
  def Menu(): Unit = {
    println("Choisissez votre opération : ")
    println("1. Dépôt")
    println("2. Retrait")
    println("3. Consultation du compte")
    println("4. Changement du code pin")
    println("5. Terminer")
  }
  def main(args: Array[String]): Unit = {
    var boucle = true
    do{
    var identifiant = readLine("Saisissez votre code identifiant : ").toInt
    var codepin = " "
    if (identifiant < nbClients){
      var essais = 0
      var nbTentativesRestantes = 3
      var correcteIdentite = false
      while ((codepin != codespinClient(identifiant))&&(essais<3)&&(nbTentativesRestantes>=1)) {
        codepin = readLine("Saisissez votre code pin : ")
        correcteIdentite = verifierIdentification(identifiant, codepin)
        essais += 1
        nbTentativesRestantes = 3 - essais
        if (codepin != codespinClient(identifiant)){
          println("Code pin erroné, il vous reste " + nbTentativesRestantes + " tentative(s).")
        }
      }
      if (correcteIdentite){
        println("Votre saisie est correcte.")
      } else { 
        println("Trop d'erreurs, abandon de l'identification.")
        return
      }
    } else {
      println("Cette identifiant n'est pas valable.")
      return
    }
    var choix = 0
        do {
          Menu()
          choix = readLine("Entrez votre choix : ").toInt
          if (choix == 1){
            depot(identifiant, comptes)
          } else if (choix == 2){
            retrait(identifiant, comptes)
          } else if (choix == 3){
            printf("Le montant disponible sur votre compte est de : %.2f CHF\n", comptes(identifiant))
          } else if (choix == 4){
            changepin(identifiant, codespinClient)
          }
        } while (choix != 5)
        if (choix == 5){
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
        }
    } while (boucle == true)
    } //Args
  } //main 