//Assignment: Joao Vulliamy_996417_assignsubmission_file

import io.StdIn._


object Main {
  // Espace pour les méthode

                                //DEPOT
  def depot(id: Int, comptes: Array[Double]):Unit={

    // variable concernant dépôt
    var devise_depot = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
    var montant_depot = readLine("Indiquez le montant du dépôt >").toInt

    //tester si montant divisible par 10
    while((montant_depot % 10) != 0){
      println("Le montant doit être un multiple de 10")
      montant_depot = readLine("Indiquez le montant du dépôt >").toInt
    }
    // opérations du dépôt
    if(devise_depot == 1){ // devise en chf
      comptes(id) += (montant_depot).toDouble
      
    }else if(devise_depot == 2){ // devise en Euro
        var montant_depot_Double = montant_depot.toDouble * 0.95 // convertir montant dispo en double et en CHF
        comptes(id) += montant_depot_Double
      }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
    
  }

                            // RETRAIT
  
  def retrait(id: Int, comptes: Array[Double]):Unit={
    // opération de retrait
    var montant_retrait = 0
    var montant_autorise = 0.0
    var montant_retrait_chf = 0.0    // montant d'euro converti en chf

    //variable concernant retrait
    var devise_retrait = readLine("Indiquez la devise : 1) CHF; 2) : EUR >").toInt

    // vérifier choix devise
    while ((devise_retrait != 1) && (devise_retrait != 2)){
      devise_retrait = readLine("Indiquez la devise : 1) CHF; 2) : EUR >").toInt
    }

    if (devise_retrait == 1){ // calcul du montant de retrait et montant max en chf
      montant_retrait = readLine("Indiquez le montant du retrait >").toInt
      montant_autorise = comptes(id) * 0.1

      //vérifier que montant retrait divisible par 10 et inférieur au montant autorisé
      while((montant_retrait % 10) != 0 || montant_retrait > montant_autorise){
        if(montant_retrait > montant_autorise){
          println("Votre plafond de retrait autorisé est de : " + montant_autorise)

        } else{
          println("Le montant doit être un multiple de 10") 
        }

        montant_retrait = readLine("Indiquez le montant du retrait >").toInt
      }




    }else if(devise_retrait == 2){ // calcul du montant de retrait et montant max en EUR
        montant_retrait = readLine("Indiquez le montant du retrait >").toInt
        montant_retrait_chf = montant_retrait * 0.95       // montant d'euro converti en chf
        montant_autorise = (comptes(id) * 1.05) * 0.1

        //vérifier que montant retrait divisible par 10 et inférieur au montant autorisé
        while((montant_retrait % 10) != 0 || montant_retrait_chf > montant_autorise){
          if(montant_retrait_chf > montant_autorise){
            println("Votre plafond de retrait autorisé est de : " + montant_autorise + " EUR")
          } else{
            println("Le montant doit être un multiple de 10")
          }

          montant_retrait = readLine("Indiquez le montant du retrait >").toInt
          montant_retrait_chf =  montant_retrait * 0.95       // montant d'euro converti en chf
        }

      }                

    //opération retrait

    // variable coupure
    var coupure500 = 500
    var coupure200 = 200
    var coupure100 = 100
    var coupure50 = 50
    var coupure20 = 20
    var coupure10 = 10

    //variable conservation des coupures (tot2 pour choix d'une valeur )
    var nb_coupure500 = 0
    var nb_coupure500_tot_2 = 0

    var nb_coupure200 = 0
    var nb_coupure200_tot_2 = 0

    var nb_coupure100 = 0
    var nb_coupure100_tot_2 = 0

    var nb_coupure50 = 0
    var nb_coupure50_tot_2 = 0

    var nb_coupure20 = 0
    var nb_coupure20_tot_2 = 0

    var nb_coupure10 = 0



    if(devise_retrait == 1){
      comptes(id) -= montant_retrait  // calcul du montant restant de CHF sur le compte
      var coupure = 2      //par défaut, on prend les petites coupures

      //choix coupure si montant >= 200
      if(montant_retrait >= 200){
        coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
        while ((coupure != 1) && (coupure != 2)){
          coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
        }
      }

      // si grosses coupures
      if (coupure == 1){

        if (montant_retrait >= 500){                                     // si montant à retirer est au moins égal à 500

          nb_coupure500 = (montant_retrait / coupure500).toInt      // nombre de coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure500 + " billet(s) de 500 CHF")
          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          if(selection == "o"){
            montant_retrait -= coupure500 * nb_coupure500


          } else {
            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure500 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure500 * nb_coupure500

            }else if (selection_int < nb_coupure500 && selection_int >= 0){   
              montant_retrait -= coupure500 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure500_tot_2 = selection_int
              nb_coupure500 = 0
            }
          }

        }

        if (montant_retrait >= 200){                                     // si montant à retirer est au moins égal à 200

          nb_coupure200 = (montant_retrait / coupure200).toInt      // nombre de coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure200 + " billet(s) de 200 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure200 * nb_coupure200 

          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure200 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure200 * nb_coupure200 

            }else if (selection_int < nb_coupure200 && selection_int >= 0){   
              montant_retrait -= coupure200 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure200_tot_2 = selection_int
              nb_coupure200 = 0
            }
          }

        }

        if (montant_retrait >= 100){                                     // si montant à retirer est au moins égal à 100

          nb_coupure100 = (montant_retrait / coupure100).toInt      // nombre de coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure100 + " billet(s) de 100 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure100 * nb_coupure100 

          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure100 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure100 * nb_coupure100 

            }else if (selection_int < nb_coupure100 && selection_int >= 0){   
              montant_retrait -= coupure100 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur et mettre le nombre de coupure calculé à 0
              nb_coupure100_tot_2 = selection_int
              nb_coupure100 = 0
            }
          }

        }

        if (montant_retrait >= 50){                                     // si montant à retirer est au moins égal à 50

          nb_coupure50 = (montant_retrait / coupure50).toInt      // nbr coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure50 + " billet(s) de 50 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure50 * nb_coupure50 

          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure50 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure50 * nb_coupure50 

            }else if (selection_int < nb_coupure50 && selection_int >= 0){   
              montant_retrait -= coupure50 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure50_tot_2 = selection_int
              nb_coupure50 = 0
            }
          }

        }

        if (montant_retrait >= 20){                                     // si montant à retirer est au moins égal à 20

          nb_coupure20 = (montant_retrait / coupure20).toInt      // nombre de coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure20 + " billet(s) de 20 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure20 * nb_coupure20 


          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure20 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure20 * nb_coupure20 
              
            }else if (selection_int < nb_coupure20 && selection_int >= 0){   
              montant_retrait -= coupure20 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure20_tot_2 = selection_int
              nb_coupure20 = 0

            }
          }

        }

        if (montant_retrait >= 10){                                     // si montant à retirer est au moins égal à 10

          nb_coupure10 = (montant_retrait / coupure10).toInt      // nombre de coupure à recevoir possible en Int

          // si on arrive au coupure de 10, l'utilisateur n'a pas de choix à faire
          montant_retrait -= coupure10 * nb_coupure10   

        }
        
      }

      // si petite coupure
      else if(coupure == 2){ 
        if (montant_retrait >= 100){                                     // si montant à retirer est au moins égal à 100

          nb_coupure100 = (montant_retrait / coupure100).toInt      // nombre de coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure100 + " billet(s) de 100 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure100 * nb_coupure100 

          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure100 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure100 * nb_coupure100 

            }else if (selection_int < nb_coupure100 && selection_int >= 0){   
              montant_retrait -= coupure100 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure100_tot_2 = selection_int
              nb_coupure100 = 0
            }
          }

        }

        if (montant_retrait >= 50){                                     // si montant à retirer est au moins égal à 50

          nb_coupure50 = (montant_retrait / coupure50).toInt      // nbr coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure50 + " billet(s) de 50 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure50 * nb_coupure50 

          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure50 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure50 * nb_coupure50 

            }else if (selection_int < nb_coupure50 && selection_int >= 0){   
              montant_retrait -= coupure50 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure50_tot_2 = selection_int
              nb_coupure50 = 0
            }
          }

        }

        if (montant_retrait >= 20){                                     // si montant à retirer est au moins égal à 20

          nb_coupure20 = (montant_retrait / coupure20).toInt      // nombre de coupure à recevoir possible en Int

          println("Il reste " + montant_retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb_coupure20 + " billet(s) de 20 CHF")

          var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(selection == "o"){
            montant_retrait -= coupure20 * nb_coupure20

          } else {

            var selection_int = selection.toInt 

            // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
            while (selection != "o" && (selection_int >= nb_coupure20 || selection_int < 0)){
              selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
              selection_int = selection.toInt
              }  
            }
            // le retrait de coupure doit être inférieur, mais doit être un nombre positif
            if(selection == "o"){
              montant_retrait -= coupure20 * nb_coupure20

            }else if (selection_int < nb_coupure20 && selection_int >= 0){   
              montant_retrait -= coupure20 * selection_int

              // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
              nb_coupure20_tot_2 = selection_int
              nb_coupure20 = 0

            }
          }

        }

        if (montant_retrait >= 10){                                     // si montant à retirer est au moins égal à 10

          nb_coupure10 = (montant_retrait / coupure10).toInt      // nombre de coupure à recevoir possible en Int

          // si on arrive au coupure de 10, l'utilisateur n'a pas de choix à faire
          montant_retrait -= coupure10 * nb_coupure10   

        }
      
      }



    //retrait demandé en euro
    }else if (devise_retrait == 2){ 
      comptes(id) -= montant_retrait_chf  // calcul du montant restant de CHF

      if (montant_retrait >= 100){                                     // si montant à retirer est au moins égal à 100

        nb_coupure100 = (montant_retrait / coupure100).toInt      // nombre de coupure à recevoir possible en Int

        println("Il reste " + montant_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_coupure100 + " billet(s) de 100 EUR")

        var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(selection == "o"){
          montant_retrait -= coupure100 * nb_coupure100 

        } else {

          var selection_int = selection.toInt 

          // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
          while (selection != "o" && (selection_int >= nb_coupure100 || selection_int < 0)){
            selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
            selection_int = selection.toInt
            }  
          }
          // le retrait de coupure doit être inférieur, mais doit être un nombre positif
          if(selection == "o"){
            montant_retrait -= coupure100 * nb_coupure100 

          }else if (selection_int < nb_coupure100 && selection_int >= 0){   
            montant_retrait -= coupure100 * selection_int

            // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
            nb_coupure100_tot_2 = selection_int
            nb_coupure100 = 0
          }
        }

      }

      if (montant_retrait >= 50){                                     // si montant à retirer est au moins égal à 50

        nb_coupure50 = (montant_retrait / coupure50).toInt      // nbr coupure à recevoir possible en Int

        println("Il reste " + montant_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_coupure50 + " billet(s) de 50 EUR")

        var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(selection == "o"){
          montant_retrait -= coupure50 * nb_coupure50 

        } else {

          var selection_int = selection.toInt 

          // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
          while (selection != "o" && (selection_int >= nb_coupure50 || selection_int < 0)){
            selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
            selection_int = selection.toInt
            }  
          }
          // le retrait de coupure doit être inférieur, mais doit être un nombre positif
          if(selection == "o"){
            montant_retrait -= coupure50 * nb_coupure50 

          }else if (selection_int < nb_coupure50 && selection_int >= 0){   
            montant_retrait -= coupure50 * selection_int

            // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
            nb_coupure50_tot_2 = selection_int
            nb_coupure50 = 0
          }
        }

      }

      if (montant_retrait >= 20){                                     // si montant à retirer est au moins égal à 20

        nb_coupure20 = (montant_retrait / coupure20).toInt      // nombre de coupure à recevoir possible en Int

        println("Il reste " + montant_retrait + " EUR à distribuer")
        println("Vous pouvez obtenir au maximum " + nb_coupure20 + " billet(s) de 20 EUR")

        var selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(selection == "o"){
          montant_retrait -= coupure20 * nb_coupure20

        } else {

          var selection_int = selection.toInt 

          // vérifier que soit, l'utilisateur entre 'o', soit il rentre un nombre plus petit de coupure
          while (selection != "o" && (selection_int >= nb_coupure20 || selection_int < 0)){
            selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

            if (selection != "o"){ //choix et transformation du nombre de coupure en Int, si 'o' pas sélectionné
            selection_int = selection.toInt
            }  
          }
          // le retrait de coupure doit être inférieur, mais doit être un nombre positif
          if(selection == "o"){
            montant_retrait -= coupure20 * nb_coupure20
          
          }else if (selection_int < nb_coupure20 && selection_int >= 0){   
            montant_retrait -= coupure20 * selection_int

            // conservez le nombre de coupure qu'on a réellement pris en choisissant une valeur
            nb_coupure20_tot_2 = selection_int
            nb_coupure20 = 0

          }
        }

      }

      if (montant_retrait >= 10){                                     // si montant à retirer est au moins égal à 10

        nb_coupure10 = (montant_retrait / coupure10).toInt      // nombre de coupure à recevoir possible en Int

        // si on arrive au coupure de 10, l'utilisateur n'a pas de choix à faire
        montant_retrait -= coupure10 * nb_coupure10

      }
      
    }
    println("Veuillez retirer la somme demandée :")

    // pour réussir le calcul correctement, valeurs de nb_coupure ont été mise à 0 si "o" n'a pas été choisi
    
    //affichage si billet de 500
    
    if(nb_coupure500 > 0 && nb_coupure500_tot_2 == 0){
      println(nb_coupure500 + " billet(s) de 500 CHF" )
    }else if(nb_coupure500 == 0 && nb_coupure500_tot_2 > 0){
      println(nb_coupure500_tot_2 + " billet(s) de 500 CHF" )
    }

    //affichage si billet de 200
    if(nb_coupure200 > 0 && nb_coupure200_tot_2 == 0){
      println(nb_coupure200 + " billet(s) de 200 CHF" )
    }else if(nb_coupure200 == 0 && nb_coupure200_tot_2 > 0){
      println(nb_coupure200_tot_2 + " billet(s) de 200 CHF" )
    }

    //affichage si billet de 100
    if (devise_retrait == 1){
      if(nb_coupure100 > 0 && nb_coupure100_tot_2 == 0){
        println(nb_coupure100 + " billet(s) de 100 CHF" )
      }else if(nb_coupure100 == 0 && nb_coupure100_tot_2 > 0){
        println(nb_coupure100_tot_2 + " billet(s) de 100 CHF" )
      }
    }else{//euro
      if(nb_coupure100 > 0 && nb_coupure100_tot_2 == 0){
        println(nb_coupure100 + " billet(s) de 100 EUR" )
      }else if(nb_coupure100 == 0 && nb_coupure100_tot_2 > 0){
        println(nb_coupure100_tot_2 + " billet(s) de 100 EUR" )
      }
    }

    //affichage si billet de 50
    if (devise_retrait == 1){
      if(nb_coupure50 > 0 && nb_coupure50_tot_2 == 0){
        println(nb_coupure50 + " billet(s) de 50 CHF" )
      }else if(nb_coupure50 == 0 && nb_coupure50_tot_2 > 0){
        println(nb_coupure50_tot_2 + " billet(s) de 50 CHF" )
      }
    }else{//euro
      if(nb_coupure50 > 0 && nb_coupure50_tot_2 == 0){
        println(nb_coupure50 + " billet(s) de 50 EUR" )
      }else if(nb_coupure50 == 0 && nb_coupure50_tot_2 > 0){
        println(nb_coupure50_tot_2 + " billet(s) de 50 EUR" )
      }
    }

    //affichage si billet de 20
    if (devise_retrait == 1){
      if(nb_coupure20 > 0 && nb_coupure20_tot_2 == 0){
        println(nb_coupure20 + " billet(s) de 20 CHF" )
      } else if(nb_coupure20 == 0 && nb_coupure20_tot_2 > 0){
        println(nb_coupure20_tot_2 + " billet(s) de 20 CHF" )
      }
    }else{//euro
      if(nb_coupure20 > 0 && nb_coupure20_tot_2 == 0){
        println(nb_coupure20 + " billet(s) de 20 EUR" )
      } else if(nb_coupure20 == 0 && nb_coupure20_tot_2 > 0){
        println(nb_coupure20_tot_2 + " billet(s) de 20 EUR" )
      }
    }

    //affichage si billet de 10
    if (devise_retrait == 1){
      if(nb_coupure10 > 0){
        println(nb_coupure10 + " billet(s) de 10 CHF" )
      }
    }else{//euro
      if(nb_coupure10 > 0 ){
        println(nb_coupure10 + " billet(s) de 10 EUR" )
      }
    }

    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))

  }
                            // CHANGEMENT DE CODE PIN
  
  def changepin(id: Int, codespin: Array[String]):Unit={
    var nouveau_pin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    while (nouveau_pin.size < 8){
      println("Votre code pin ne contient pas au moins 8 caractères")
      nouveau_pin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    }

    codespin(id) = nouveau_pin
    println("Votre code pin a été modifié.")
    
  }

      
  
  
  
  def main(args: Array[String]): Unit = {

    // déclaration tableau comptes et codespin
    val nbclients = 100
    val comptes = Array.fill(nbclients)(1200.0)
    val codespin = Array.fill(nbclients)("INTRO1234")
    
    var On = true // tant que le programme est en marche, la valeur sera true

    while(On == true){

        
  
        //rentrer id
        val id = readLine("Saisissez votre code identifiant >").toInt
  
        if(id > nbclients-1){
          println("Cet identifiant n’est pas valable.")
          On = false // le programme doit s'arrêter
        }else{
          var tentatives = 3
          var entree_pin = readLine("Saisissez votre code pin > ")
          while(codespin(id) != entree_pin && tentatives > 1 ){
            tentatives -= 1
            entree_pin = readLine("Code pin erroné, il vous reste " + tentatives + " tentatives. > ")          
          }

          if(codespin(id) == entree_pin){

            println("Choisissez votre opération:") 
            println("    1) Dépôt")                     //appuie sur 1 pour dépôt
            println("    2) Retrait")                   //appuie sur 2 pour Retrait
            println("    3) Consultation du compte")    //appuie sur 3 pour Consultation
            println("    4) Changement du code pin")    //appuie sur 4 pour changer le code pin          
            println("    5) Terminer")                  //appuie sur 5 pour Terminer
            
            var choix_Op = readLine("Votre choix: ").toInt
             

            while(choix_Op != 5){
              
              if (choix_Op == 1){// opération pour dépôt
                depot(id, comptes)
                
              } else if(choix_Op == 2){// opération de retrait
                  retrait(id, comptes)
                }else if(choix_Op == 3){ // opération de consultation
                  printf("Le montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
                }else if(choix_Op == 4){
                  changepin(id, codespin)
                }




              println("Choisissez votre opération:") 
              println("    1) Dépôt")                     //appuie sur 1 pour dépôt
              println("    2) Retrait")                   //appuie sur 2 pour Retrait
              println("    3) Consultation du compte")    //appuie sur 3 pour Consultation
              println("    4) Changement du code pin")    //appuie sur 4 pour changer le code pin          
              println("    5) Terminer")                  //appuie sur 5 pour Terminer

              choix_Op = readLine("Votre choix: ").toInt
            }
            println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
            


            
          }else{
            println("Trop d’erreurs, abandon de l’identification")
          }



          
        }
      
    }
  }
}
