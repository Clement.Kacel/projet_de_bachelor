//Assignment: Helian Alain Romano Ferraris_996525_assignsubmission_file

import scala.io.StdIn._
import scala.math

object Main {
  
  var montantdepot = 0
  var euroenchf = 0.0
  var montantdispo = 1200.0  
  var devise = "1"
  var montantretrait = 0
  var opération = "0"
  var coupure = "1"

  var Uniquecodepin = ""

  var nbclients = 100
  var id = 0
  var comptes= Array.fill(nbclients)(1200.0)
  var codepins = Array[String]()
  for (_ <- 0 until nbclients) {
  codepins = codepins :+ "INTRO1234" 
    }
  
 def depot(id : Int, comptes : Array[Double]) : Unit ={
   
   var devise=readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toString
     while ((!(devise== "1"))&&(!(devise== "2"))) {
     devise=readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toString
     }


     var montantdepot = readLine("Indiquez le montant du dépôt > ").toInt


   while ((!(montantdepot % 10 == 0 ))||(montantdepot<10)) {
      println("Le montant doit être un multiple de 10")
     montantdepot = readLine("Indiquez le montant du dépôt > ").toInt }

   if (devise=="2") { (euroenchf = montantdepot*0.95) //montant depot en euro en chf
   montantdispo = montantdispo+euroenchf //montantdispo en chf
   printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f CHF.\n", montantdispo)

      comptes(id) = montantdispo


   } else if (devise =="1"){ (montantdispo = montantdispo + montantdepot) //montantdispo en chf
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", montantdispo) 
                     comptes(id) = montantdispo
                           
    }
   
 } // def depot

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
   
    devise=readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toString
         while ((!(devise== "1"))&&(!(devise== "2"))) {
         devise=readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toString
         }

         montantretrait = readLine("Indiquez le montant du retrait > ").toInt
          var montantrestant = montantretrait

    if (devise == "1") {

      var autoriséretrait = montantdispo*0.10

      while ((!(montantretrait % 10 == 0 ))||(montantretrait > autoriséretrait)) {

        if (!(montantretrait % 10 == 0 )) {

        println("Le montant doit être un multiple de 10")
      montantretrait = readLine("Indiquez le montant du retrait > ").toInt 

        }else if (montantretrait > autoriséretrait) {
       printf("Votre plafond de retrait autorisé est de : %.2f CHF.\n", autoriséretrait)
       montantretrait = readLine("Indiquez le montant du retrait > ").toInt 
        }

      } // accolade pour while ((!(montantretrait % 10 == 0 ))||(montantretrait > autoriséretrait))


    } //verif montant retrait autorisé pour chf

    else if (devise == "2") {

      var autoriséretrait = montantdispo*0.10
      autoriséretrait = autoriséretrait*1.05



      while ((!(montantretrait % 10 == 0 ))||(montantretrait > autoriséretrait)) {

        if (!(montantretrait % 10 == 0 )) {

        println("Le montant doit être un multiple de 10")
      montantretrait = readLine("Indiquez le montant du retrait > ").toInt 

        }else if (montantretrait > autoriséretrait) {
       printf("Votre plafond de retrait autorisé est de : %.2f EUR.\n", autoriséretrait)
       montantretrait = readLine("Indiquez le montant du retrait > ").toInt 
        }

      } // accolade pour while ((!(montantretrait % 10 == 0 ))||(montantretrait > autoriséretrait))

    } //verif montant retrait autorisé pour eur



             if (devise == "1") {
               if (montantretrait >= 200) { 

                    var coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toString

                    while((!(coupure == "1")) && (!(coupure == "2"))) {
                coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toString
                     } // accolade pour le  while(!(coupure == 1) && !(coupure == 2))

                    if (coupure == "1") {


             var coupureschf = List(500, 200, 100, 50, 20, 10)

               var montantrestant = montantretrait

                      var bill500 = 0
                       var bill200 = 0
                       var bill50 = 0
                       var bill100 = 0
                       var bill20 = 0
                       var bill10 = 0

               for (autrecoupure <- coupureschf) {
                 if (montantrestant >= autrecoupure) {
                   var maxnb = montantrestant / autrecoupure


                    println("Il reste "+montantrestant+" CHF à distribuer.") 
                   println("Vous pouvez obtenir au maximum "+maxnb+ " billet(s) de "+autrecoupure+" CHF.")



         if (autrecoupure== 10) {      

           var billdistrib = maxnb * autrecoupure
           bill10 += maxnb
           montantrestant -= billdistrib

         } //.. if (autrecoupure== 10)


                    else  if (autrecoupure==500){//..

           var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

       while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                      option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                    }

                    if (option == "o") {
                      var billdistrib = maxnb * autrecoupure
                     bill500 += maxnb
                      montantrestant -= billdistrib

                    } else {
                      var option2 = option.toInt
                      var billdistrib = option2 * autrecoupure
                      bill500 += option2
                      montantrestant -= billdistrib

                    }

         }
                   else  if (autrecoupure==200){//..

                        var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                    while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                   option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                 }

                                 if (option == "o") {
                                   var billdistrib = maxnb * autrecoupure
                                   bill200 += maxnb
                                   montantrestant -= billdistrib

                                 } else {
                                   var option2 = option.toInt
                                   var billdistrib = option2 * autrecoupure
                                  bill200 += option2
                                   montantrestant -= billdistrib

                                 }

                      }
                   else  if (autrecoupure==100){//..

                        var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                    while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                   option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                 }

                                 if (option == "o") {
                                   var billdistrib = maxnb * autrecoupure
                                   bill100 += maxnb
                                   montantrestant -= billdistrib

                                 } else {
                                   var option2 = option.toInt
                                   var billdistrib = option2 * autrecoupure
                                bill100 += option2
                                   montantrestant -= billdistrib

                                 }

                      }


                   else  if (autrecoupure==50){//..

                        var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                    while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                   option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                 }

                                 if (option == "o") {
                                   var billdistrib = maxnb * autrecoupure
                                   bill50 += maxnb
                                   montantrestant -= billdistrib

                                 } else {
                                   var option2 = option.toInt
                                   var billdistrib = option2 * autrecoupure
                                  bill50 += option2
                                   montantrestant -= billdistrib

                                 }

                      }

                   else  if (autrecoupure==20){//..

                        var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                    while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                   option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                 }

                                 if (option == "o") {
                                   var billdistrib = maxnb * autrecoupure
                                   bill20 += maxnb
                                   montantrestant -= billdistrib

                                 } else {
                                   var option2 = option.toInt
                                   var billdistrib = option2 * autrecoupure
                                  bill20 += option2
                                   montantrestant -= billdistrib

                                 }

                      }



                 } //..
               }

               println("Veuillez retirer la somme demandée: ")

                      if (bill500>0) {
              println (bill500+ " billet(s) de 500 CHF")}

                      if (bill200>0) {
                        println (bill200+ " billet(s) de 200 CHF")}

                      if (bill100>0) {
                        println (bill100+ " billet(s) de 100 CHF")}

                      if (bill50>0) {
                        println (bill50+ " billet(s) de 50 CHF")}

                      if (bill20>0) {
                        println (bill20+ " billet(s) de 20 CHF")}

                      if (bill10>0) {
                        println (bill10+ " billet(s) de 10 CHF")}

                      montantdispo = (montantdispo - montantretrait)
                      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", montantdispo)

                       comptes(id) = montantdispo
                       } // if (coupure == 1) chf grosse coupure

         else if (coupure == "2") {//chf petite coupure début

           var coupureschf = List(100, 50, 20, 10)


             var montantrestant = montantretrait

                            var bill50 = 0
                            var bill100 = 0
                            var bill20 = 0
                            var bill10 = 0

                    for (autrecoupure <- coupureschf) {
                      if (montantrestant >= autrecoupure) {
                        var maxnb = montantrestant / autrecoupure


                         println("Il reste "+montantrestant+" CHF à distribuer.") 
                        println("Vous pouvez obtenir au maximum "+maxnb+ " billet(s) de "+autrecoupure+" CHF.")



              if (autrecoupure== 10) {      

                var billdistrib = maxnb * autrecoupure
                bill10 += maxnb
                montantrestant -= billdistrib

              } //.. if (autrecoupure== 10)

                        else  if (autrecoupure==100){//..

                             var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                         while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                        option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                      }

                                      if (option == "o") {
                                        var billdistrib = maxnb * autrecoupure
                                        bill100 += maxnb
                                        montantrestant -= billdistrib

                                      } else {
                                        var option2 = option.toInt
                                        var billdistrib = option2 * autrecoupure
                                     bill100 += option2
                                        montantrestant -= billdistrib

                                      }

                           }


                        else  if (autrecoupure==50){//..

                             var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                         while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                        option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                      }

                                      if (option == "o") {
                                        var billdistrib = maxnb * autrecoupure
                                        bill50 += maxnb
                                        montantrestant -= billdistrib

                                      } else {
                                        var option2 = option.toInt
                                        var billdistrib = option2 * autrecoupure
                                       bill50 += option2
                                        montantrestant -= billdistrib

                                      }

                           }

                        else  if (autrecoupure==20){//..

                             var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                         while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                        option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                      }

                                      if (option == "o") {
                                        var billdistrib = maxnb * autrecoupure
                                        bill20 += maxnb
                                        montantrestant -= billdistrib

                                      } else {
                                        var option2 = option.toInt
                                        var billdistrib = option2 * autrecoupure
                                       bill20 += option2
                                        montantrestant -= billdistrib

                                      }

                           }



                      } //..
                    }

                    println("Veuillez retirer la somme demandée: ")


                           if (bill100>0) {
                             println (bill100+ " billet(s) de 100 CHF")}

                           if (bill50>0) {
                             println (bill50+ " billet(s) de 50 CHF")}

                           if (bill20>0) {
                             println (bill20+ " billet(s) de 20 CHF")}

                           if (bill10>0) {
                             println (bill10+ " billet(s) de 10 CHF")}

                           montantdispo = (montantdispo - montantretrait)
                           printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", montantdispo)
           comptes(id) = montantdispo
         } //chf petite coupure fin


                  } //if (montantretrait >= 200) 
         else {
           var coupureschf = List(100, 50, 20, 10)


             var montantrestant = montantretrait

           var bill50 = 0
                         var bill100 = 0
                         var bill20 = 0
                         var bill10 = 0

                 for (autrecoupure <- coupureschf) {
                   if (montantrestant >= autrecoupure) {
                     var maxnb = montantrestant / autrecoupure


                      println("Il reste "+montantrestant+" CHF à distribuer.") 
                     println("Vous pouvez obtenir au maximum "+maxnb+ " billet(s) de "+autrecoupure+" CHF.")



           if (autrecoupure== 10) {      

             var billdistrib = maxnb * autrecoupure
             bill10 += maxnb
             montantrestant -= billdistrib

           } //.. if (autrecoupure== 10)

                     else  if (autrecoupure==100){//..

                          var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                      while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                     option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                   }

                                   if (option == "o") {
                                     var billdistrib = maxnb * autrecoupure
                                     bill100 += maxnb
                                     montantrestant -= billdistrib

                                   } else {
                                     var option2 = option.toInt
                                     var billdistrib = option2 * autrecoupure
                                  bill100 += option2
                                     montantrestant -= billdistrib

                                   }

                        }


                     else  if (autrecoupure==50){//..

                          var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                      while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                     option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                   }

                                   if (option == "o") {
                                     var billdistrib = maxnb * autrecoupure
                                     bill50 += maxnb
                                     montantrestant -= billdistrib

                                   } else {
                                     var option2 = option.toInt
                                     var billdistrib = option2 * autrecoupure
                                    bill50 += option2
                                     montantrestant -= billdistrib

                                   }

                        }

                     else  if (autrecoupure==20){//..

                          var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                      while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                     option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                   }

                                   if (option == "o") {
                                     var billdistrib = maxnb * autrecoupure
                                     bill20 += maxnb
                                     montantrestant -= billdistrib

                                   } else {
                                     var option2 = option.toInt
                                     var billdistrib = option2 * autrecoupure
                                    bill20 += option2
                                     montantrestant -= billdistrib

                                   }

                        }



                   } //..
                 }

                 println("Veuillez retirer la somme demandée: ")


                        if (bill100>0) {
                          println (bill100+ " billet(s) de 100 CHF")}

                        if (bill50>0) {
                          println (bill50+ " billet(s) de 50 CHF")}

                        if (bill20>0) {
                          println (bill20+ " billet(s) de 20 CHF")}

                        if (bill10>0) {
                          println (bill10+ " billet(s) de 10 CHF")}

                        montantdispo = (montantdispo - montantretrait)
                        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", montantdispo)
           comptes(id) = montantdispo

         } // else chf retrait<200 petite coupure par défaut

                 } // if (devise ==1)

             else if (devise == "2") { //devise en euro

               var coupureseuro = List(100, 50, 20, 10)

               var bill50 = 0
                var bill100 = 0
                var bill20 = 0
                var bill10 = 0

                 var montantrestant = montantretrait


                 for (autrecoupure <- coupureseuro) {
                   if (montantrestant >= autrecoupure) {
                     var maxnb = montantrestant / autrecoupure
                     println("Il reste "+montantrestant+" EUR à distribuer.")
                     println("Vous pouvez obtenir au maximum "+maxnb+ " billet(s) de "+autrecoupure+" EUR.")

                   if (autrecoupure== 10) {      

                      var billdistrib = maxnb * autrecoupure
                      bill10 += maxnb
                      montantrestant -= billdistrib

                    } //.. if (autrecoupure== 10)

                              else  if (autrecoupure==100){//..

                                   var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                               while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                              option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                            }

                                            if (option == "o") {
                                              var billdistrib = maxnb * autrecoupure
                                              bill100 += maxnb
                                              montantrestant -= billdistrib

                                            } else {
                                              var option2 = option.toInt
                                              var billdistrib = option2 * autrecoupure
                                           bill100 += option2
                                              montantrestant -= billdistrib

                                            }

                                 }


                              else  if (autrecoupure==50){//..

                                   var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                               while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                              option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                            }

                                            if (option == "o") {
                                              var billdistrib = maxnb * autrecoupure
                                              bill50 += maxnb
                                              montantrestant -= billdistrib

                                            } else {
                                              var option2 = option.toInt
                                              var billdistrib = option2 * autrecoupure
                                             bill50 += option2
                                              montantrestant -= billdistrib

                                            }

                                 }

                              else  if (autrecoupure==20){//..

                                   var option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")

                               while (option != "o" && (option.toIntOption.getOrElse(-1) < 0 || option.toIntOption.getOrElse(-1) >= maxnb)) {
                                              option = readLine("Tapez o pour OK ou une autre valeur inférieure à celle proposée > ")
                                            }

                                            if (option == "o") {
                                              var billdistrib = maxnb * autrecoupure
                                              bill20 += maxnb
                                              montantrestant -= billdistrib

                                            } else {
                                              var option2 = option.toInt
                                              var billdistrib = option2 * autrecoupure
                                             bill20 += option2
                                              montantrestant -= billdistrib
     }
     }
     }
     }
                 println("Veuillez retirer la somme demandée: ")


               if (bill100>0) {
                 println (bill100+ " billet(s) de 100 EUR")}

               if (bill50>0) {
                 println (bill50+ " billet(s) de 50 EUR")}

               if (bill20>0) {
                 println (bill20+ " billet(s) de 20 EUR")}

               if (bill10>0) {
                 println (bill10+ " billet(s) de 10 EUR")}



               montantdispo = (montantdispo - montantretrait*0.95)
               printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", montantdispo)
               comptes(id) = montantdispo
             } // devise ==2 euro

    
  } // def retrait

  def changepin(id : Int, codespins : Array[String]) : Unit = {


       var miseajourpin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")

     while (miseajourpin.length < 8) {

      println("Votre code pin ne contient pas au moins 8 caractères")
       miseajourpin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
     }

       codespins(id) = miseajourpin
       Uniquecodepin = codepins(id)
   } // def


  
def main(args: Array[String]): Unit = {
   


   while (true) {


  
 while (Uniquecodepin != codepins(id)) {
    // print(Uniquecodepin)
  
id = readLine("Saisissez votre code identifiant > ").toInt


  
  if ((id>=nbclients)||(id<0)) {
println("Cet identifiant n’est pas valable.")
    sys.exit(0)
  }
  
  montantdispo = comptes(id)

Uniquecodepin = readLine("Saisissez votre code pin > ").toString

    

if (Uniquecodepin != codepins(id)) {
println("Code pin erroné, il vous reste 2 tentatives.") 
  Uniquecodepin = readLine("Saisissez votre code pin > ").toString

if (Uniquecodepin != codepins(id)) {
                  println("Code pin erroné, il vous reste 1 tentatives") 
  Uniquecodepin = readLine("Saisissez votre code pin > ").toString


 if (Uniquecodepin != codepins(id)) {
         println("Trop d’erreurs, abandon de l’identification.")
        
            }
           }
          }
    
   } //...
     
 //while (opération !="5") {
    
  opération = readLine("Choisissez votre opération :" + "\n" + "1) Dépôt"+ "\n" + "2) Retrait"+ "\n" + "3) Consultation du compte"+ "\n" + "4) Changement du code pin"+ "\n" + "5) Terminer"+"\n"+"Votre choix :").toString


  while ((!(opération=="1"))&&(!(opération=="2"))&&(!(opération=="3"))&&(!(opération=="4"))&&(!(opération=="5"))) {


  opération = readLine("Choisissez votre opération :" + "\n" + "1) Dépôt"+ "\n" + "2) Retrait"+ "\n" + "3 Consultation du compte"+ "\n" + "4) Changement du code pin"+ "\n" + "5) Terminer"+"\n"+"Votre choix :").toString
  }






   //operation de depot
if (opération == "1") {

depot(id, comptes)


   } //accolade pour le if(operation ==1)

   //opération de consultation du compte

   if (opération =="3") { printf("Le montant disponible sur votre compte est de : %.2f CHF.\n", montantdispo)


   }//accolade pour le if(opération ==3)
     
     if (opération =="4") { //opération 4 debut
        changepin(id, codepins)

     } ///opération 4 fin




     
   if (opération =="2") {
     retrait(id, comptes)
     

   } // pour le if (opération ==2)

else if (opération == "5") {println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
                            Uniquecodepin = ""
                           }
//}
   } //while (true) boucle infinie

     
 }
}

