//Assignment: Nai Grace Marguerite Dougrou_996384_assignsubmission_file

import scala.io.StdIn._


object Main {
  val nbclients : Int = 100 // NOMBRE DE CLIENTS
    var codespin : Array[String]= Array.fill(nbclients)("INTRO1234")
    var comptes = Array.fill(nbclients)(1200.0)

    // MÉTHODE DE DÉPÔT

    def depot( user : Int, comptes : Array[Double]) : Unit = {


      // DEVISES
      val CHF : Int = 1
      val EUR : Int = 2
      val taux : Double = 0.95

    // AFFICHAGE A L'ECRAN

      println ("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")

      var devisedepot : Int = readInt()

      while (devisedepot != 1 && devisedepot != 2 ) {

      println ("Nous vous prions de choisir entre 1 et 2. Merci.")

      devisedepot = readInt()
      }
      println ("Indiquez le montant du dépôt >")

      // POUR EFFECTUER LE DÉPÔT

      var montantdepot : Int = readInt()

      var depotfinal : Double = montantdepot * taux


      // CONDITIONS RECQUISES POUR LE DEPOT

      while (!(montantdepot % 10 == 0)) {

       println ("Le montant du dépôt doit être un multiple de 10. Veuillez réessayer.")

        montantdepot = readInt()

        }

      if (devisedepot == EUR && montantdepot % 10 == 0) {

      depotfinal = montantdepot * taux //pour le taux de change

      comptes(user) = comptes(user) + depotfinal

      println (" Votre dépôt a bien été pris en compte. Le nouveau montant disponible sur votre compte s'élève à " + (comptes(user)) + " CHF.")

      } else if (devisedepot == CHF && montantdepot % 10 == 0) {

      comptes(user) = comptes(user) + montantdepot

      println (" Votre dépôt a bien été pris en compte. Le nouveau montant disponible sur votre compte s'élève à " + (comptes(user)) + " CHF.")

       }

     } 

    // MÉTHODE CHANGEMENT DE PIN

    def changepin( user : Int, codespin : Array[String]) : Unit = {

  println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >") 

    var newpassword = readLine() // POUR LA SAISIE AU CLAVIER


      if (newpassword.length < 8) {

      do {
    println("Votre code pin doit contenir au moins 8 caractères.")

      println("Saisissez votre nouveau code pin")

      newpassword = readLine()

      } while (newpassword.length < 8) // BOUCLE TANT QUE LE CODE COMPTE MOINS DE 8 CARACTERES

      } 

      if (newpassword.length >= 8) {

          codespin(user) = newpassword // REDÉFINITION DU CODE PIN

      }


      }
  // MÉTHODE DE RETRAIT trèèèèèès longue...

    def retrait( user: Int, comptes : Array[Double]) : Unit = {

      // DEVISES DISPONIBLES

            val CHF : Int = 1
            val EUR : Int = 2

          // TAUX DE CHANGE

            val taux : Double = 0.95

          // POUR DEFINIR LE PLAFOND

            comptes(user) = comptes(user)
            var plafond = comptes(user)/10

          // COUPURES

          val grosses = 1
          val petites = 2
          val ok = "o"

            // ON INITIALISE LES VARIABLES QUI VONT STOCKER LE NOMBRE DE BILLET DE CHAQUE COUPURE 

            var nb500 = 0
            var nb200 = 0
            var nb100 = 0
            var nb50 = 0
            var nb20 = 0
            var nb10 = 0

            // ON INITIALISE LA VARIABLE BILLET QUI VA STOCKER LA VALEUR DE LA COUPURE 

            var billet = 0


          println ("Indiquez la devise : 1) CHF ; 2) EUR >")

          var choixdevise = readInt()

          while (choixdevise != 1 && choixdevise != 2 ) {

          println ("Nous vous prions de choisir entre 1 et 2. Merci.")
            choixdevise = readInt()

            }

            println ("Indiquez le montant que vous souhaitez retirer >")


            var montantretrait = readInt()

            // LE RESTE EST LE MONTANT RESTANT À DISTRIBUER
            var reste = montantretrait



            while (!(montantretrait % 10 == 0)) {

            println ("Le montant du retiré doit être un multiple de 10. Veuillez réessayer.")

            montantretrait = readInt()
            reste = montantretrait


            if (montantretrait > plafond) {

           do {
            println ("Vous ne pouvez pas retirer cette somme. Votre plafond de retrait autorisé est de "+(plafond)+" CHF. Réessayez.")

              montantretrait = readInt()
              reste = montantretrait

              } while ( montantretrait > plafond )

            }

              }

            while ( montantretrait > plafond ) {

      println ("Vous ne pouvez pas retirer cette somme. Votre plafond de retrait autorisé est de "+ (plafond) + " CHF. Réessayez.")

            montantretrait = readInt()
            reste = montantretrait

              if (!(montantretrait % 10 == 0)) {

                do {

                println ("Le montant du retiré doit être un multiple de 10. Veuillez réessayer.")

                montantretrait = readInt()
                reste = montantretrait


                } while (!(montantretrait % 10 == 0))


              }

            }

            if (choixdevise == CHF && montantretrait <= plafond && montantretrait >= 200) {

              println ("En 1) grosses coupures, 2) petites coupures >")

              var choixcoupure = readInt()

              while (choixcoupure != 1 && choixcoupure != 2 ) {

              println("Nous vous prions de choisir entre 1 et 2. Merci.")

              choixcoupure = readInt()

               }

            if (choixcoupure == grosses) {

              // BILLET 500
                  billet = 500

                  // si le reste est supérieur ou égal à la coupure alors on rentre dans la condition
                  if (reste >= billet) {
                    println("il reste " + reste + " à distribuer")
                    println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")

                    // on initialise la variable inputValide à false car on ne sait pas si l'utilisateur va rentrer une valeur
                    var inputValide = false

                    while (!inputValide) { // tant que l'utilisateur n'a pas rentré une valeur valide on continue la boucle

                      var input = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")

                      // on initialise la variable nombreBillet à 0 car on ne sait pas combien de billet l'utilisateur va demander
                      var nombreBillet = 0

                      // si l'utilisateur a rentré o alors on lui donne le nombre de billet maximum qu'il peut avoir
                      //et on passe la variable inputValide à true pour sortir de la boucle
                      if (input == "o") {
                        nombreBillet = reste / billet
                        inputValide = true


                      // Si l'utilisateur a rentré une valeur inférieure à celle proposée alors on lui donne le nombre de billet qu'il a demandé
                        //et on passe la variable inputValide à true pour sortir de la boucle
                      } else if (input.toInt <= (reste / billet) ) {
                        nombreBillet = input.toInt
                        inputValide = true


                        // Si l'utilisateur a rentré une valeur supérieure à celle proposée alors on lui dit que son choix est invalide
                        //et on lui redemande de rentrer une valeur
                      } else {
                        println("Choix invalide")
                      }

                      //Enfin si l'utilisateur a rentré une valeur valide, on lui donne le nombre de billet qu'il a demandé
                      //et on soustrait le montant des billets distribués au reste
                      if (inputValide) {
                        nb500 += nombreBillet
                        reste -= nombreBillet * billet
                      }
                    }
                  }

                  //On fait la MÊME chose pour les autres coupures avec le montant restant

                  // BILLET 200
                  billet = 200

                  if (reste >= billet) {

                    println("il reste " + reste + " à distribuer")
                    println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                    var inputValide = false

                    while (!inputValide) {
                      var input = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                      var nombreBillet = 0
                      if (input == "o") {
                        nombreBillet = reste / billet
                        inputValide = true

                      } else if (input.toInt <= (reste / billet) ) {
                        nombreBillet = input.toInt
                        inputValide = true

                      } else {
                        println("Choix invalide")
                      }
                      if (inputValide) {
                        nb200 += nombreBillet
                        reste -= nombreBillet * billet
                      }
                    }
                  }


                // BILLET 100
                billet = 100
                if (reste >= billet) {

                  println("il reste " + reste + " à distribuer")
                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                  var inputValide = false

                  while (!inputValide) {
                    var input = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var nombreBillet = 0
                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true

                    } else if (input.toInt <= (reste / billet) ) {

                      nombreBillet = input.toInt
                       inputValide = true

                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb100 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                // BILLET 50
                billet = 50
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")

                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet) + " CHF")

                  var inputValide = false

                  while (!inputValide) {

                    var input = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var nombreBillet = 0
                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else if (input.toInt <= (reste / billet) ) {
                      nombreBillet = input.toInt
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb50 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                // BILLET 20
                billet = 20
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")
                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                  var inputValide = false
                  while (!inputValide) {
                    var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var nombreBillet = 0
                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else if (input.toInt <= (reste / billet)) {
                      nombreBillet = input.toInt
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb20 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                // BILLET 10 ATTENTION!
                /*
                Si le/la client-e n’a pris aucune coupure avant d’arriver
                à la distribution de la coupure de 10 CHF ou 10 EUR (c.à.d. s’il/elle a sélectionné la valeur 0 à toutes
                les coupures avant d’atteindre la coupure de 10 CHF ou 10 EUR), il/elle sera obligé de prendre la
                proposition faite par le programme sans pouvoir la modifier, sinon, le montant
                distribué sera plus petit que le montant demandé, ce qui ne serait pas cohérent.
                 */
                billet = 10
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")
                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")

                  var inputValide = false

                  while (!inputValide) {

                    var input = scala.io.StdIn.readLine("Tapez o pour ok  > ")
                    var nombreBillet = 0

                    //Ici les conditions sont différentes car on ne veut pas que l'utilisateur puisse rentrer une valeur
                    //Il n'a pas le choix que de prendre le nombre de billet maximum sinon il ne pourra PAS sortir de la boucle

                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb10 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }


                // AFFICHAGE DES BILLETS QUE L'UTILISATEUR DOIT RETIRER

                println('\n'+ "Veuillez retirer la somme demandée :" +'\n')
                if (nb500 > 0) println((nb500) + " billet(s) de 500 CHF")
                if (nb200 > 0) println((nb200) + " billet(s) de 200 CHF")
                if (nb100 > 0) println((nb100) + " billet(s) de 100 CHF")
                if (nb50 > 0) println((nb50) + " billet(s) de 50  CHF")
                if (nb20 > 0) println((nb20) + " billet(s) de 20  CHF")
                if (nb10 > 0) println((nb10) + " billet(s) de 10  CHF")



                // MISE A JOUR DU COMPTE
              comptes(user) = comptes(user) - montantretrait
                println('\n')

               printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", (comptes(user)))




            }

              // SI L'UTILISATEUR VEUT DE PETITES COUPURES

            if (montantretrait >= 200 && choixcoupure == petites && montantretrait <= plafond) {


                      // BILLET 100
                    billet = 100
                    if (reste >= billet) {
                      println("il reste " + reste + " à distribuer")
                      println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                      var inputValide = false
                      while (!inputValide) {
                        var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                        var nombreBillet = 0
                        if (input == "o") {
                          nombreBillet = reste / billet
                          inputValide = true
                        } else if (input.toInt <= (reste / billet) ) {
                          nombreBillet = input.toInt
                          inputValide = true
                        } else {
                          println("Choix invalide")
                        }
                        if (inputValide) {
                          nb100 += nombreBillet
                          reste -= nombreBillet * billet
                        }
                      }
                    }

                    // BILLET 50

                    billet = 50
                    if (reste >= billet) {
                      println("il reste " + reste + " à distribuer")

                      println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")

                      var inputValide = false

                      while (!inputValide) {

                        var input = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                        var nombreBillet = 0
                        if (input == "o") {
                          nombreBillet = reste / billet
                          inputValide = true
                        } else if (input.toInt <= (reste / billet) ) {
                          nombreBillet = input.toInt
                          inputValide = true
                        } else {
                          println("Choix invalide")
                        }
                        if (inputValide) {
                          nb50 += nombreBillet
                          reste -= nombreBillet * billet
                        }
                      }
                    }

                    // BILLET 20

                    billet = 20
                    if (reste >= billet) {
                      println("il reste " + reste + " à distribuer")
                      println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                      var inputValide = false
                      while (!inputValide) {
                        var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                        var nombreBillet = 0
                        if (input == "o") {
                          nombreBillet = reste / billet
                          inputValide = true
                        } else if (input.toInt <= (reste / billet)) {
                          nombreBillet = input.toInt
                          inputValide = true
                        } else {
                          println("Choix invalide")
                        }
                        if (inputValide) {
                          nb20 += nombreBillet
                          reste -= nombreBillet * billet
                        }
                      }
                    }


                    billet = 10
                    if (reste >= billet) {
                      println("il reste " + reste + " à distribuer")
                      println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")

                      var inputValide = false

                      while (!inputValide) {

                        var input = scala.io.StdIn.readLine("Tapez o pour ok  > ")
                        var nombreBillet = 0

                        if (input == "o") {
                          nombreBillet = reste / billet
                          inputValide = true
                        } else {
                          println("Choix invalide")
                        }
                        if (inputValide) {
                          nb10 += nombreBillet
                          reste -= nombreBillet * billet
                        }
                      }
                    }

                   println('\n'+ "Veuillez retirer la somme demandée :" +'\n')
                   if (nb100 > 0) println((nb100) + " billet(s) de 100 CHF")
                   if (nb50 > 0) println((nb50) + " billet(s) de 50  CHF")
                   if (nb20 > 0) println((nb20) + " billet(s) de 20  CHF")
                   if (nb10 > 0) println((nb10) + " billet(s) de 10  CHF")

                   // MISE A JOUR DU COMPTE

              comptes(user) =  comptes(user) - montantretrait

                 printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", (comptes(user)))





              }

              } 

               // SI LE MONTANT DU RETRAIT EST INFÉRIEUR À 200

              if (choixdevise == CHF && montantretrait < 200 ) {

                // BILLET 100
                billet = 100
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")
                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                  var inputValide = false
                  while (!inputValide) {
                    var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var nombreBillet = 0
                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else if (input.toInt <= (reste / billet) ) {
                      nombreBillet = input.toInt
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb100 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                // BILLET 50
                billet = 50
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")

                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")

                  var inputValide = false

                  while (!inputValide) {

                    var input = readLine ("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var nombreBillet = 0
                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else if (input.toInt <= (reste / billet) ) {
                      nombreBillet = input.toInt
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb50 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                // BILLET 20
                billet = 20
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")
                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")
                  var inputValide = false
                  while (!inputValide) {
                    var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var nombreBillet = 0
                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else if (input.toInt <= (reste / billet)) {
                      nombreBillet = input.toInt
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb20 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                // BILLET 10

                billet = 10
                if (reste >= billet) {
                  println("il reste " + reste + " à distribuer")
                  println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " CHF")

                  var inputValide = false

                  while (!inputValide) {

                    var input = readLine("Tapez o pour ok  > ")
                    var nombreBillet = 0


                    if (input == "o") {
                      nombreBillet = reste / billet
                      inputValide = true
                    } else {
                      println("Choix invalide")
                    }
                    if (inputValide) {
                      nb10 += nombreBillet
                      reste -= nombreBillet * billet
                    }
                  }
                }

                println('\n'+ "Veuillez retirer la somme demandée :" +'\n')
                if (nb100 > 0) println((nb100) + " billet(s) de 100 CHF")
                if (nb50 > 0) println((nb50) + " billet(s) de 50  CHF")
                if (nb20 > 0) println((nb20) + " billet(s) de 20  CHF")
                if (nb10 > 0) println((nb10) + " billet(s) de 10  CHF")


                // MISE A JOUR DU COMPTE

                comptes(user) =  comptes(user) - montantretrait
                println('\n')

                 printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", (comptes(user)))




              } 

            // L'UTILISATEUR A CHOISI DES EUROS
            // LE PRINCIPE EST LE MÊME QU'AVEC LES CHF!

            if (choixdevise == EUR) {


              var reste = montantretrait
              var nb100 = 0
              var nb50 = 0
              var nb20 = 0
              var nb10 = 0

              var billet = 0

              // BILLET 100
              billet = 100
              if (reste >= billet) {
                println("il reste " + reste + " à distribuer")
                println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " EUR")
                var inputValide = false
                while (!inputValide) {
                  var input = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  var nombreBillet = 0
                  if (input == "o") {
                    nombreBillet = reste / billet
                    inputValide = true
                  } else if (input.toInt <= (reste / billet) ) {
                    nombreBillet = input.toInt
                    inputValide = true
                  } else {
                    println("Choix invalide")
                  }
                  if (inputValide) {
                    nb100 += nombreBillet
                    reste -= nombreBillet * billet
                  }
                }
              }

              // BILLET 50
              billet = 50
              if (reste >= billet) {
                println("il reste " + reste + " à distribuer")
                println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet) + " EUR")
                var inputValide = false
                while (!inputValide) {
                  var input = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  var nombreBillet = 0
                  if (input == "o") {
                    nombreBillet = reste / billet
                    inputValide = true
                  } else if (input.toInt <= (reste / billet) ) {
                    nombreBillet = input.toInt
                    inputValide = true
                  } else {
                    println("Choix invalide")
                  }
                  if (inputValide) {
                    nb50 += nombreBillet
                    reste -= nombreBillet * billet
                  }
                }
              }

              // BILLET 20
              billet = 20
              if (reste >= billet) {
                println("il reste " + reste + " à distribuer")
                println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " EUR")
                var inputValide = false
                while (!inputValide) {
                  var input = scala.io.StdIn.readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  var nombreBillet = 0
                  if (input == "o") {
                    nombreBillet = reste / billet
                    inputValide = true
                  } else if (input.toInt <= (reste / billet)) {
                    nombreBillet = input.toInt
                    inputValide = true
                  } else {
                    println("Choix invalide")
                  }
                  if (inputValide) {
                    nb20 += nombreBillet
                    reste -= nombreBillet * billet
                  }
                }
              }

              // BILLET 10
              billet = 10
              if (reste >= billet) {
                println("il reste " + reste + " à distribuer")
                println("Vous pouvez obtenir au maximum " + (reste / billet) + " billet(s) de " + (billet)+ " EUR")
                var inputValide = false
                while (!inputValide) {
                  var input = scala.io.StdIn.readLine("Tapez o pour ok  > ")
                  var nombreBillet = 0
                  if (input == "o") {
                    nombreBillet = reste / billet
                    inputValide = true
                  } else {
                    println("Choix invalide")
                  }
                  if (inputValide) {
                    nb10 += nombreBillet
                    reste -= nombreBillet * billet
                  }
                }
              }


              println('\n'+ "Veuillez retirer la somme demandée :" +'\n')
              if (nb100 > 0) println((nb100) + " billet(s) de 100 EUR")
              if (nb50 > 0) println((nb50) + " billet(s) de 50  EUR")
              if (nb20 > 0) println((nb20) + " billet(s) de 20  EUR")
              if (nb10 > 0) println((nb10) + " billet(s) de 10  EUR")

              // MISE A JOUR DU COMPTE

              var deduction : Double = 0
              deduction = montantretrait*taux
              comptes(user) = comptes(user) - deduction

               printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", (comptes(user)))


            }
    }


  def main(args: Array[String]): Unit = {



    var boucle = true // POUR LA GROSSE BOUCLE
    var codeentre : String = "0"
    var choix = 0 // CHOIX DE L'UTILISATEUR
    var tentatives = 3 // TENTATIVES MAX POUR LE CODE


while(boucle == true) { 

  choix = 0
  
  println("Saisissez votre code identifiant >")

  var user : Int = readInt()

  if (user < nbclients) {

  while (codeentre != codespin(user) && tentatives > 0) {


       println("Veuillez saisir votre code PIN > ")

      codeentre = readLine()

     if (codeentre != codespin(user) && tentatives > 0) {

       tentatives = tentatives - 1

       println ( "Mot de passe incorrect. Veuillez réessayer. Il vous reste " + (tentatives) + " tentatives.")


     }

    }
         // APRÈS 3 TENTATIVES INFRUCTUEUSES
      if (tentatives == 0 && codeentre != codespin(user)) {

   println ('\n'+"Trop d’erreurs, abandon de l’identification")

        tentatives = tentatives + 3

      }

  if (codeentre == codespin(user)) {

  while(choix != 5) {

    print('\n') // Pour mieux voir les différents réaffichages

    println (" Choississez l'opération que vous souhaitez effectuer.") 

    println ( " 1: Dépôt " + '\n' + " 2: Retrait " + '\n' + " 3: Consultation du compte " + '\n'+ " 4: Changement de code pin "+ '\n'+" 5: Terminer")


    // VALEURS DES OPERATIONS

    val Depot = 1
    val Retrait = 2
    val Consultation = 3
    val Changementdepin = 4
    val Terminer = 5

     choix = readInt() // CHOIX DE L'UTILISATEUR

     if (codeentre == codespin(user) && choix == 1 ) {

     depot( user : Int, comptes : Array[Double])

     }

     if (codeentre == codespin(user) && choix == 2 ) {

       retrait( user : Int, comptes : Array[Double])
     }

     if (codeentre == codespin(user) && choix == 3 ) {


      printf("Le montant disponible sur votre compte est de %.2f \n : ",(comptes(user)))

     }
     if (codeentre == codespin(user) && choix == 4 ) {

       changepin(user : Int, codespin : Array[String])

     }

     if (codeentre == codespin(user) && choix == 5 ) {

       println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
       tentatives = 3
     }

  }

}

} else if (user > nbclients) {

    println("Cet identifiant n’est pas valable.")
    boucle = false

}



  }
}
}
