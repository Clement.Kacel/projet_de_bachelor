//Assignment: Boris Angelin Bagnoud_996558_assignsubmission_file

import scala.io.StdIn._

object Main {


  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes: Array[Double] = Array.fill(nbclients)(1200.0)
    var codespin: Array[String] = Array.fill(nbclients)("INTRO1234")


    var soldeCompte = 1200.0
    val codePIN = "INTRO1234"
    var PINCorrect = false
    var bool500 = true
    var bool200 = true
    var bool100 = true
    var bool50 = true
    var bool20 = true

    while (true) {
      var tentativesPIN = 3
      var choixUtilisateur = 0
      var demandePIN = false
      PINCorrect = false
      println("Saisissez votre code identifiant >")
      var IDutilisateur = readInt()
      if (IDutilisateur > nbclients-1){
        println("Cet identifiant n’est pas valable.")
        System.exit(0)
      }
      if (!demandePIN) {
        while (tentativesPIN > 0 && !PINCorrect) {
          println("Saisissez votre code pin >")
          val PIN = readLine()

          if (PIN == codespin(IDutilisateur)) {
            PINCorrect = true
            demandePIN = true
          } else {
            tentativesPIN -= 1
            if (tentativesPIN == 0) {
              println("Trop d’erreurs, abandon de l’identification")
            } else {
              println("Code pin erroné, il vous reste " +  tentativesPIN + " tentatives >")
            }
          }
        }
      }


    while (choixUtilisateur != 5 && tentativesPIN != 0) {
      bool500 = true
      bool200 = true
      bool100 = true
      bool50 = true
      bool20 = true
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("votre choix : ")
      choixUtilisateur = readInt()

      while (choixUtilisateur < 1 || choixUtilisateur > 5) {
        println("Choix invalide")
        choixUtilisateur = readInt()
      }

        // DEPOT-----------------------------------------------------------------------------------------------------
        if (choixUtilisateur == 1 ) {
          depot(IDutilisateur,comptes)

          //RETRAIT------------------------------------------------------------------------------------
        } else if (choixUtilisateur == 2 ) {
          retrait(IDutilisateur,comptes)

          // CONSULTATION ---------------------------------------------------------------------
        } else if (choixUtilisateur == 3 ) {
          println("Le solde disponible sur votre compte est de : " + comptes(IDutilisateur))

          // CODES PIN -------------------------------------------------------------
        } else if (choixUtilisateur == 4 ) {
          changepin(IDutilisateur,codespin)

          //FIN -------------------------------------------------------------------------------
        }

    }
      if (choixUtilisateur == 5) {
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
      }
  }
    def depot(id : Int, comptes : Array[Double]): Unit = {

      var deviseDepot = 0
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
      deviseDepot = readInt()

      var montantDepot = 0
      do {
        println("Indiquez le montant du dépôt >")
        montantDepot = readInt()

        if (montantDepot % 10 != 0) {
          println("Le montant doit être un multiple de 10")
        }
      } while (montantDepot % 10 != 0)

      if (deviseDepot == 1) {
        comptes(id) += montantDepot
      } else {
        comptes(id) += montantDepot * 0.95
      }

      println("Votre dépôt a été traité, le nouveau solde de votre compte est : " + comptes(id))

    }

    def retrait(id : Int, comptes : Array[Double]): Unit = {

      var deviseRetrait = 0
      while (deviseRetrait != 1 && deviseRetrait != 2) {
        println("Indiquez la devise : 1) CHF, 2) EUR >")
        deviseRetrait = readLine().toInt

        if (deviseRetrait != 1 && deviseRetrait != 2) {
          println("Choix invalide")
        }
      }

      var montantRetrait = 0
      val plafondRetrait = comptes(id) * 0.1

      do {
        println("Indiquez le montant du retrait >")
        montantRetrait = readInt()

        if (montantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10")
        } else if (montantRetrait > plafondRetrait) {
          println(s"Votre plafond de retrait autorisé est de : $plafondRetrait")
        }
      } while (montantRetrait % 10 != 0 || montantRetrait > plafondRetrait)

      var typeCoupure = 0
      if (montantRetrait < 200) typeCoupure = 2
      if (deviseRetrait == 1 && montantRetrait >= 200) {
        do {
          println("En 1) grosses coupures, 2) petites coupures >")
          typeCoupure = readInt()

          if (typeCoupure != 1 && typeCoupure != 2) {
            println("Choix invalide")
          }
        } while (typeCoupure != 1 && typeCoupure != 2)
      }
      var montantRestant = montantRetrait
      var phraseFinale = "Veuillez retirer la somme demandée :\n"

      if (deviseRetrait == 1) { //CHF

        if (typeCoupure == 1) { //GROSSES COUPURES

          while (montantRestant > 0) {

            if (montantRestant / 500 >= 1 && bool500) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 500) + " billet(s) de 500 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 500) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 500
                montantRestant -= nombreBillets * 500
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 500
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 500 CHF\n"
              println(bool200)

            }
            if (montantRestant / 200 >= 1 && bool200) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 200) + " billet(s) de 200 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 200) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 200
                montantRestant -= nombreBillets * 200
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 200
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 200 CHF\n"


            }
            if (montantRestant / 100 >= 1 && bool100) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 100) + " billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 100) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 100
                montantRestant -= nombreBillets * 100
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 100
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 100 CHF\n"

            }
            if (montantRestant / 50 >= 1 && bool50) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 50) + " billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 50) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 50
                montantRestant -= nombreBillets * 50
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 50
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 50 CHF\n"


            }
            if (montantRestant / 20 >= 1 && bool20) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 20) + " billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 20) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 20
                montantRestant -= nombreBillets * 20
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 20
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 20 CHF\n"


            }
            if (montantRestant / 10 >= 1) {
              println(s"Il reste $montantRestant CHF à distribuer")
              val nombreBillets = montantRestant / 10
              montantRestant -= nombreBillets * 10
              println(s"Vous recevez donc $nombreBillets billets de 10 CHF.")
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 10 CHF\n"
            }

          }

        } else { // PETITES COUPURES

          while (montantRestant > 0) {

            if (montantRestant / 100 >= 1 && bool100) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 100) + " billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 100) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 100
                montantRestant -= nombreBillets * 100
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 100
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 100 CHF\n"

            }
            if (montantRestant / 50 >= 1 && bool50) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 50) + " billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 50) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                 reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 50
                montantRestant -= nombreBillets * 50
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 50
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 50 CHF\n"


            }
            if (montantRestant / 20 >= 1 && bool20) {
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + (montantRestant / 20) + " billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              var reponse = readLine()
              while (reponse != "o" && reponse.toInt > montantRestant / 20) {
                println("Valeur invalide")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                reponse = readLine()
              }
              var nombreBillets = 0
              if (reponse == "o") {
                nombreBillets = montantRestant / 20
                montantRestant -= nombreBillets * 20
              }
              else {
                nombreBillets = reponse.toInt
                montantRestant -= nombreBillets * 20
              }
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 20 CHF\n"


            }
            if (montantRestant / 10 >= 1) {
              println(s"Il reste $montantRestant CHF à distribuer")
              val nombreBillets = montantRestant / 10
              montantRestant -= nombreBillets * 10
              println(s"Vous recevez donc $nombreBillets billets de 10 CHF.")
              if (nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 10 CHF\n"
            }
          }
        }
      }
      else { // EUR

        while (montantRestant > 0) {

          if (montantRestant / 100 >= 1 && bool100) {
            println("Il reste " + montantRestant + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + (montantRestant / 100) + " billet(s) de 100 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var reponse = readLine()
            while(reponse != "o" && reponse.toInt > montantRestant / 100){
              println("Valeur invalide")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              reponse = readLine()
            }
            var nombreBillets = 0
            if (reponse == "o") {
              nombreBillets = montantRestant / 100
              montantRestant -= nombreBillets * 100
            }
            else {
              nombreBillets = reponse.toInt
              montantRestant -= nombreBillets * 100
            }
            if(nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 100 EUR\n"

          }
          if (montantRestant / 50 >= 1 && bool50) {
            println("Il reste " + montantRestant + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + (montantRestant / 50) + " billet(s) de 50 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var reponse = readLine()
            while(reponse != "o" && reponse.toInt > montantRestant / 50){
              println("Valeur invalide")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              reponse = readLine()
            }
            var nombreBillets = 0
            if (reponse == "o") {
              nombreBillets = montantRestant / 50
              montantRestant -= nombreBillets * 50

            }
            else {
              nombreBillets = reponse.toInt
              montantRestant -= nombreBillets * 50
            }
            if(nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 50 EUR\n"
          }
          if (montantRestant / 20 >= 1 && bool20) {
            println("Il reste " + montantRestant + " EUR à distribuer")
            println("Vous pouvez obtenir au maximum " + (montantRestant / 20) + " billet(s) de 20 EUR")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            var reponse = readLine()
            while(reponse != "o" && reponse.toInt > montantRestant / 20){
              println("Valeur invalide")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              reponse = readLine()
            }
            var nombreBillets = 0
            if (reponse == "o") {
              nombreBillets = montantRestant / 20
              montantRestant -= nombreBillets * 20
            }
            else {
              nombreBillets = reponse.toInt
              montantRestant -= nombreBillets * 20
            }
            if(nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 20 EUR\n"
          }
          if (montantRestant / 10 >= 1) {
            println(s"Il reste $montantRestant EUR à distribuer")
            val nombreBillets = montantRestant / 10
            montantRestant -= nombreBillets * 10
            println(s"Vous recevez donc $nombreBillets billets de 10 EUR.")
            if(nombreBillets != 0) phraseFinale +=  + nombreBillets + " billet(s) de 10 EUR\n"
          }
        }
      }


      // Mise à jour du solde
      if (deviseRetrait == 1) {
        comptes(id) -= montantRetrait
      } else {
        comptes(id) -= montantRetrait * 0.95
      }

      print(phraseFinale)

      comptes(id) = math.floor(comptes(id) * 100) / 100
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))


    }

    def changepin(id : Int, codespin : Array[String]): Unit = {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      var nouveauCode = readLine()
      while(nouveauCode.length <8){
        println("Votre code pin ne contient pas au moins 8 caractères")
        println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        nouveauCode = readLine()
      }
      codespin(id) = nouveauCode

    }
  }
}
