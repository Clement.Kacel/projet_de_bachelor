//Assignment: Jules Gaspard Henry_996614_assignsubmission_file


import io.StdIn._
object Main {

  var nbclients = 100 // Nombre de clients
  var comptes : Array[Double] = Array.fill(nbclients)(1200.0) // Tableau des comptes
  var codespin: Array[String] = Array.fill(nbclients)("INTRO1234") // Tableau des codes pins
  var id = 0 // Identifiant du client
  var choix_op = 0 // Choix de l'opération
  var choixpin = "" // Choix du code PIN
  var stop = false // Boolean pour recommencer 
  var re = false // Boolean pour arrêter le programme
  var tentative = 0 // Compteur de tentatives
  val liste_coupures: Array[Int] = Array(500, 200, 100, 50, 20, 10) // Coupures de billets disponibles
  var devise_depot = 0 // Devise pour le dépôt
  var montant_depot = 0 // Montant du dépôt 
  var devise_retrait = 0 // Devise pour le retrait
  var montant_retrait = 0 // Montant du retrait
  var coupures = 2 // Choix des coupures pour le retrait
  var n = 0 // Variable auxiliaire pour les calculs
  var choix_coupures = "" // Choix des coupures de billets
  var total_retrait = "Veuillez retirer la somme demandée :" // Message pour le retrait
  var nvcode = "" // Nouveau code PIN




  def choix_id(): Unit = {

    // Demande l'identifiant client et vérifie sa validité

    id = readLine("Saisissez votre code identifiant >").toInt

    if(id > nbclients-1 || id < 0){

      println("\nCet identifiant n’est pas valable.")
      re = true

    }

  }

  def choix_pin(): Unit = {

    // Gère la saisie et la vérification du code PIN

    stop = false
    tentative = 0
    choixpin = readLine("\nSaisissez votre code pin >")

    while(choixpin!=codespin(id) && !re && !stop){

      if (tentative >= 2) {

        println("\nTrop d’erreurs, abandon de l’identification\n")
        stop = true

      }

      else {
        tentative += 1
        println("\nCode pin erroné, il vous reste " + (3 - tentative) + " tentatives >")
        choixpin = readLine("\nSaisissez votre code pin >")
      }

    }

  }

  def operation(): Unit = {

    // Permet à l'utilisateur de choisir une opération (dépôt, retrait, etc.)

    choix_op = 0
    println("\nChoisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer")
    choix_op = readLine("Votre choix:").toInt

    while ((choix_op < 1) || (choix_op > 5)) {


      println("\nChoisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer")
      choix_op = readLine("Votre choix:").toInt


    }


  }

  def depot(id:Int,comptes : Array[Double]): Unit = {

    // Gère le processus de dépôt

   devise_depot = 0
   montant_depot = 0

   while (devise_depot < 1 || devise_depot > 2) {

    devise_depot = readLine("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt

   }

   montant_depot = readLine("Indiquez le montant du dépôt >").toInt

   while (montant_depot % 10 != 0 || montant_depot == 0) {

    println("Le montant doit être un multiple de 10")
    montant_depot = readLine("Indiquez le montant du dépôt >").toInt

   }

   if ((devise_depot == 1) && (montant_depot % 10 == 0)) {

    comptes(id) = comptes(id) + montant_depot
    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

   }

   else if ((devise_depot == 2) && (montant_depot % 10 == 0)) {

    comptes(id) = comptes(id) + (montant_depot * 0.95)
    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))


   }

  }



  def retrait(id : Int, comptes : Array[Double]): Unit = {

    // Gère le processus de retrait

    devise_retrait = 0
    montant_retrait = 0
    coupures = 2
    n = 0
    choix_coupures = ""
    total_retrait = "Veuillez retirer la somme demandée :"

    while (devise_retrait < 1 || devise_retrait > 2) {

      devise_retrait = readLine("\nIndiquez la devise : 1) CHF ; 2) EUR >").toInt

    }

    montant_retrait = readLine("Indiquez le montant du retrait >").toInt

    while (montant_retrait % 10 != 0 || montant_retrait == 0) {

      println("Le montant doit être un multiple de 10")
      montant_retrait = readLine("Indiquez le montant du retrait >").toInt

    }

    if (devise_retrait == 1) {

      while ((montant_retrait > (comptes(id) / 10.0)) || montant_retrait == 0 || montant_retrait % 10 != 0) {

        printf("Votre plafond de retrait autorisé est de : %.2f \n", comptes(id) / 10)
        montant_retrait = readLine("Indiquez le montant du retrait >").toInt

      }
    }

    else if (devise_retrait == 2) {

      while (montant_retrait > ((comptes(id) / 10.0) * 1.05) || montant_retrait == 0 || montant_retrait % 10 != 0) {

        printf("Votre plafond de retrait autorisé est de : %.2f \n", (comptes(id) * 1.05) / 10)
        montant_retrait = readLine("Indiquez le montant du retrait >").toInt

      }
    }


    if (devise_retrait == 1) {

      comptes(id) = comptes(id) - montant_retrait

      if (montant_retrait >= 200) {

        coupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt

        while (coupures < 1 || coupures > 2) {

          coupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt

        }

      }

      else {

        coupures = 2

      }

      if (coupures == 1) {

        for (i <- liste_coupures) {

          if (montant_retrait >= i) {


            n = montant_retrait / i
            choix_coupures = readLine(s"\nIl reste $montant_retrait CHF à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

            while (choix_coupures != "o" && choix_coupures.toInt >= n) {

              choix_coupures = readLine(s"\nIl reste $montant_retrait CHF à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

            }


            if (i == 10) {

              while (choix_coupures != "o") {


                choix_coupures = readLine(s"\nIl reste $montant_retrait CHF à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

              }


            }

            if (choix_coupures != "o" && choix_coupures.toInt != 0) {

              montant_retrait = montant_retrait - (choix_coupures.toInt * i)
              total_retrait = total_retrait + s"\n$choix_coupures billet(s) de $i CHF "

            }

            if (choix_coupures == "o") {

              montant_retrait = montant_retrait - (n * i)
              total_retrait = total_retrait + s"\n$n billet(s) de $i CHF "

            }


          }
        }
      }


      if (coupures == 2) {

        for (i <- liste_coupures.slice(2, 6)) {

          if (montant_retrait >= i) {


            n = montant_retrait / i
            choix_coupures = readLine(s"\nIl reste $montant_retrait CHF à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

            while (choix_coupures != "o" && choix_coupures.toInt >= n) {

              choix_coupures = readLine(s"\nIl reste $montant_retrait CHF à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

            }


            if (i == 10) {

              while (choix_coupures != "o") {


                choix_coupures = readLine(s"\nIl reste $montant_retrait CHF à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

              }


            }


            if (choix_coupures != "o" && choix_coupures.toInt != 0) {

              montant_retrait = montant_retrait - (choix_coupures.toInt * i)
              total_retrait = total_retrait + s"\n$choix_coupures billet(s) de $i CHF "

            }


            if (choix_coupures == "o") {

              montant_retrait = montant_retrait - (n * i)
              total_retrait = total_retrait + s"\n$n billet(s) de $i CHF "

            }


          }


        }


      }


    }

    else if (devise_retrait == 2) {

      comptes(id) = comptes(id) - (montant_retrait * 0.95)

      for (i <- liste_coupures.slice(2, 6)) {

        if (montant_retrait >= i) {


          n = montant_retrait / i
          choix_coupures = readLine(s"\nIl reste $montant_retrait EUR à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

          while (choix_coupures != "o" && choix_coupures.toInt >= n) {

            choix_coupures = readLine(s"\nIl reste $montant_retrait EUR à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

          }


          if (i == 10) {

            while (choix_coupures != "o") {


              choix_coupures = readLine(s"\nIl reste $montant_retrait EUR à distribuer\nVous pouvez obtenir au maximum $n billet (s) de $i EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")

            }


          }


          if (choix_coupures != "o" && choix_coupures.toInt != 0) {

            montant_retrait = montant_retrait - (choix_coupures.toInt * i)
            total_retrait = total_retrait + s"\n$choix_coupures billet(s) de $i EUR "

          }


          if (choix_coupures == "o") {

            montant_retrait = montant_retrait - (n * i)
            total_retrait = total_retrait + s"\n$n billet(s) de $i EUR "

          }


        }


      }


    }

    println(s"\n$total_retrait")


  }

  def consultation(id:Int,comptes : Array[Double]): Unit = {

    // Permet de consulter le solde du compte


    printf("\nLe montant disponible sur votre compte est de : %.2f \n", comptes(id))


  }


  def terminer(): Unit = {

    // Termine la session actuelle

    stop = true
    println("\nFin des opérations, n’oubliez pas de récupérer votre carte\n")


  }


  def changepin(id : Int, codespin : Array[String]): Unit = {

    // Permet de changer le code PIN

    nvcode = ""

    nvcode = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")

    while(nvcode.length<8){

      println("\nVotre code pin ne contient pas au moins 8 caractères")
      nvcode = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")

    }

    codespin(id) = nvcode


  }


  def main(args: Array[String]): Unit = {

    while (!re) {


      choix_id()
      if(!re)choix_pin()


      while (!stop && !re) {

        operation()
        if (choix_op == 1) depot(id, comptes)
        if (choix_op == 2) retrait(id, comptes)
        if (choix_op == 3) consultation(id, comptes)
        if (choix_op == 4) changepin(id, codespin)
        if (choix_op == 5) terminer()



      }

    }



 }


}
