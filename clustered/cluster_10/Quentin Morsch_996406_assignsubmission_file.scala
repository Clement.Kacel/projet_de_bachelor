//Assignment: Quentin Morsch_996406_assignsubmission_file

// Projet Bancomat Introduction à la programmation

import scala.io.StdIn._
import scala.math._

object Main {
    //Définition des variables initiales
    var boucle = 3
    val nbclients = 100
    var i = -1
    var codespin: Array[String] = Array.fill(nbclients)("INTRO1234")
    var comptes: Array[Double] = Array.fill(nbclients)(1200.0)
    var identifiant: Array[Int] = Array.fill(nbclients){i += 1; i}
    var pinok = 0
    var PIN_demande =""
    var nombre_erreur = 0
    var erreur_restante = 0
    var choix = 0
    var id = 0
    var dstableau = 0
    var devise = ""
    var test_multiple = 1
    var montant = 0
    var action = ""
    var choixdevise = 0
    var montant_max_retrait = 0.0
    var respet = 0

    //Vérification PIN
    def verifpin(): Unit = {
        while ((pinok != 1) && (nombre_erreur < 3) && (pinok != 2)) {
            PIN_demande = readLine("Saississez votre code pin \n>>  ")
            if (PIN_demande != codespin(id)) {
                nombre_erreur += 1
                if (nombre_erreur >= 3) {
                    println("Trop d'erreurs, abandon de l'identification \n")
                    pinok = 2
                }
                else if (nombre_erreur < 3) {
                    erreur_restante = 3 - nombre_erreur
                    println("Code pin erroné, il vous reste " + erreur_restante + " tentative(s) \n")
                }
            }
            else if (PIN_demande == codespin(id)) {
                pinok = 1
            }
            reset()
        }
    }

    //Dépôt
    def depot(id: Int, comptes : Array[Double]): Unit = {
        typeaction()
        selecdevise()
        multiple()
        if (choixdevise == 1) {
            comptes(id) += montant
        }
        else {
            comptes(id) += montant * 0.95
        }
        printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
        reset()
    }

    //Réinitialisation des variables
    def reset(): Unit = {
        choix = 0
        choixdevise = 0
        test_multiple = 1
        respet = 0
    }

    //Retrait
    def retrait(id : Int, comptes : Array[Double]): Unit = {
        typeaction()
        selecdevise()
        while ((test_multiple != 0) || montant > montant_max_retrait) {
            multiple()
            //Définition du plafond
            if (choixdevise == 1) {
                montant_max_retrait = comptes(id) * 0.1
            }
            else {
                montant_max_retrait = comptes(id) * 0.1 /* * 0.95 --> Conversion pour le plafond ? */
            }

            //test du plafond
            if (montant > montant_max_retrait) {
                println("Votre plafond de retrait autorisé est de : " + montant_max_retrait + " " + devise)
                test_multiple = 1
            }

            //Mise à jour du solde de compte
            if (choixdevise == 1) {
                comptes(id) -= montant
            }
            else {
                comptes(id) -= 0.95 * montant
            }
            //selection type de coupure
            if ((choixdevise == 1) && (montant >= 200)) {
                choixdevise = 0
                while ((choixdevise != 1) && (choixdevise != 2))
                    choixdevise = readLine("En 1) grosses coupures 2) petites coupures \n>>  ").toInt
            }
            else {
                choixdevise = 2
            }

            //Grosse coupure
            var nombre_500: Int = 0
            var nombre_200: Int = 0
            var test_coupure = montant
            var test = 0

            while (choixdevise == 1) {

                //Billet de 500

                while (test_coupure >= 500) {
                    nombre_500 += 1
                    test_coupure -= 500
                }
                println("Il reste " + montant + " " + devise + " à distribuer")
                println("Vous pouvez obtenir au maximum " + nombre_500 + " billet(s) de 500 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                var quantite = readLine()
                var test = 0
                while (test == 0) {
                    if (quantite == "o") {
                        montant -= nombre_500 * 500
                        test = 1
                    }
                    else if (quantite.toInt >= nombre_500) {
                        println("Il reste " + montant + " " + devise + " à distribuer")
                        println("Vous pouvez obtenir au maximum " + nombre_500 + " billet(s) de 500 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                        quantite = readLine()
                    }
                    else if (quantite.toInt < nombre_500) {
                        montant -= quantite.toInt * 500
                        nombre_500 = quantite.toInt
                        test = 1
                    }
                }
                test = 0
                test_coupure = montant

                //Billet de 200

                while (test_coupure >= 200) {
                    nombre_200 += 1
                    test_coupure -= 200
                }
                println("Il reste " + montant + " " + devise + " à distribuer")
                println("Vous pouvez obtenir au maximum " + nombre_200 + " billet(s) de 200 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                quantite = readLine()
                while (test == 0) {
                    if (quantite == "o") {
                        montant -= nombre_200 * 200
                        test = 1
                    }
                    else if (quantite.toInt >= nombre_200) {
                        println("Il reste " + montant + " " + devise + " à distribuer")
                        println("Vous pouvez obtenir au maximum " + nombre_200 + " billet(s) de 200 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                        quantite = readLine()
                    }
                    else if (quantite.toInt < nombre_200) {
                        montant -= quantite.toInt * 200
                        nombre_200 = quantite.toInt
                        test = 1
                    }
                }
                test = 0
                test_coupure = montant
                choixdevise = 2 // Sortie de la boucle pour le passage au petite coupure
            }

            //Petite coupure
            var nombre_100: Int = 0
            var nombre_50: Int = 0
            var nombre_20: Int = 0
            var nombre_10: Int = 0

            while (choixdevise == 2) {

                //Billet de 100

                while (test_coupure >= 100) {
                    nombre_100 += 1
                    test_coupure -= 100
                }
                println("Il reste " + montant + " " + devise + " à distribuer")
                println("Vous pouvez obtenir au maximum " + nombre_100 + " billet(s) de 100 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                var quantite = readLine()
                while (test == 0) {
                    if (quantite == "o") {
                        montant -= nombre_100 * 100
                        test = 1
                    }
                    else if (quantite.toInt >= nombre_100) {
                        println("Il reste " + montant + " " + devise + " à distribuer")
                        println("Vous pouvez obtenir au maximum " + nombre_100 + " billet(s) de 100 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                        quantite = readLine()
                    }
                    else if (quantite.toInt < nombre_100) {
                        montant -= quantite.toInt * 100
                        nombre_100 = quantite.toInt
                        test = 1
                    }
                }
                test = 0
                test_coupure = montant

                //Billet de 50

                while (test_coupure >= 50) {
                    nombre_50 += 1
                    test_coupure -= 50
                }
                println("Il reste " + montant + " " + devise + " à distribuer")
                println("Vous pouvez obtenir au maximum " + nombre_50 + " billet(s) de 50 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                quantite = readLine()
                while (test == 0) {
                    if (quantite == "o") {
                        montant -= nombre_50 * 50
                        test = 1
                    }
                    else if (quantite.toInt >= nombre_50) {
                        println("Il reste " + montant + " " + devise + " à distribuer")
                        println("Vous pouvez obtenir au maximum " + nombre_50 + " billet(s) de 50 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                        quantite = readLine()
                    }
                    else if (quantite.toInt < nombre_50) {
                        montant -= quantite.toInt * 50
                        nombre_50 = quantite.toInt
                        test = 1
                    }
                }
                test = 0
                test_coupure = montant

                //Billet de 20

                while (test_coupure >= 20) {
                    nombre_20 += 1
                    test_coupure -= 20
                }
                println("Il reste " + montant + " " + devise + " à distribuer")
                println("Vous pouvez obtenir au maximum " + nombre_20 + " billet(s) de 20 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                quantite = readLine()
                while (test == 0) {
                    if (quantite == "o") {
                        montant -= nombre_20 * 20
                        test = 1
                    }
                    else if (quantite.toInt >= nombre_20) {
                        println("Il reste " + montant + " " + devise + " à distribuer")
                        println("Vous pouvez obtenir au maximum " + nombre_20 + " billet(s) de 20 " + devise + "\n Tapez o pour ok ou une autre valeur inférieur à celle proposée >> ")
                        quantite = readLine()
                    }
                    else if (quantite.toInt < nombre_20) {
                        montant -= quantite.toInt * 20
                        nombre_20 = quantite.toInt
                        test = 1
                    }
                }
                test = 0
                test_coupure = montant
                choixdevise = 0 //Sortie de la boucle pour le passage au coupure de 10
            }

            //Billet de 10

            while (test_coupure >= 10) {
                nombre_10 += 1
                test_coupure -= 10
            }
            println("Il reste " + montant + " " + devise + " à distribuer")
            println("Vous pouvez obtenir " + nombre_10 + " billet(s) de 10 \n")
            test_coupure = montant

            //Affichage du résultat
            println("Veuillez retirer la somme demandée : ")
            if (nombre_500 >= 1) {
                println(nombre_500 + " billet(s) de 500 " + devise)
            }
            if (nombre_200 >= 1) {
                println(nombre_200 + " billet(s) de 200 " + devise)
            }
            if (nombre_100 >= 1) {
                println(nombre_100 + " billet(s) de 100 " + devise)
            }
            if (nombre_50 >= 1) {
                println(nombre_50 + " billet(s) de 50 " + devise)
            }
            if (nombre_20 >= 1) {
                println(nombre_20 + " billet(s) de 20 " + devise)
            }
            if (nombre_10 >= 1) {
                println(nombre_10 + " billet(s) de 10 " + devise)
            }
        }
        reset()
    }

    //Changement du code PIN
    def changepin(id : Int, codespin : Array[String]) : Unit = {
        while (respet != 1) {
            PIN_demande = readLine("Veuillez indiquer le nouveau code pin souhaité \n>> ")
            if (PIN_demande.length <= 7) {
                println("Le code doit contenir au minimum 8 caractères")
            }
            else {
                respet = 1
                codespin(id) = PIN_demande
                println("Le changement de code pin à bien été pris en compte")
            }
        }
    }

    //Selection Devise
    def selecdevise() : Unit = {
        choixdevise = 0
        while ((choixdevise != 1) && (choixdevise != 2)) {
            choixdevise = readLine("Indiquez la devise du " + action + "\n1) CHF \n2) EUR \n>> ").toInt // POSSIBLE AJOUT DU TYPE D'ACTION DANS LE PRINT
        }
        if (choixdevise == 1) {
            devise = "CHF"
        }
        else {
            devise = "EUR"
        }
    }

    //Vérification multiple de 10
    def multiple() : Unit = {
        while (test_multiple != 0) {
            montant = readLine("Indiquez le montant du "+ action + " \n>> ").toInt //POSSIBILITE D'AFFICHAGE DU TYPE D'ACTION
            test_multiple = floorMod(montant, 10)
            if (test_multiple != 0) {
                println("Le montant doit être un multiple de 10")
                montant = 1
                montant_max_retrait = 0
            }
            if (montant < 0) {
                println("le montant doit être une valeur positive")
                test_multiple = 1
            }
        }
    }

    def typeaction() : Unit = {
        val tableauaction = Array("dépôt", "retrait")
        action = tableauaction(choix-1)
    }


    def main(args: Array[String]): Unit = {
        while (boucle !=4) {
            while (dstableau == 0) {
                reset()
                println("\n ================= Identification =================")
                id = readLine("Indiquez votre identifiant \n>> ").toInt
                if (identifiant.indexOf(id) == -1) {
                    println("Cet identifiant n'est pas valable.")
                    dstableau = 1
                    boucle = 4
                    choix = 998736662
                }
                else {
                    verifpin()
                    if (pinok == 1){
                        dstableau = 1
                    }
                    else {
                        pinok = 0
                        nombre_erreur = 0
                    }
                }
            }

            //Affichage du menu principal
            while ((choix != 1) && (choix != 2) && (choix != 3) && (choix != 4) && (choix != 5) && (choix != 998736662)) {
                println("\n ================= Menu =================")
                choix = readLine("\n1) Dépôt \n2) Retrait \n3) Consultation du compte  \n4) Changement du code pin \n5) Terminer \n>> ").toInt
            }

            // Dépôt
            if ((choix == 1) && (pinok == 1)) {
                println("================= Dépôt =================")
                depot(id, comptes)
            }

            // Retrait
            if ((choix == 2) && (pinok == 1)) {
                println("================= Retrait =================")
                retrait(id, comptes)
            }

            //Consultation du compte
            if ((choix == 3) && (pinok == 1)) {
                println("================= Consultation du compte =================")
                printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
                reset()
            }

            //Changement du code pin
            if ((choix == 4) && (pinok == 1)) {
                println("================= Changement du code PIN =================")
                changepin(id, codespin)
                reset()
            }

            //Terminer
            if (choix == 5) {
                println("\n Fin des opérations, n'oubliez pas de récupérer votre carte")
                pinok = 0
                nombre_erreur = 0
                dstableau = 0
                reset()
            }
        }
    }
}