//Assignment: Magid Ismail_996605_assignsubmission_file

import scala.io.StdIn._
object Main {
  /// VARIABLES 
  var nbclients = 100
  var comptes = Array.fill[Double](nbclients)(1200)
  var choix = 0
  var code_pin = "none"
  var id = -1
  var codespin = Array.fill[String](nbclients)("INTRO1234")
  var nbr_essais_pin = 1
  var montant_depot = 0
  var montant_retrait = 0.0
  var plafond_retrait = 120.0
  var devise = 0
  var choix_coupure = 0
  var nbr_500 = 0
  var nbr_200 = 0
  var nbr_100 = 0
  var nbr_50 = 0
  var nbr_20 = 0
  var nbr_10 = 0
  var reste = 0
  var resultat = ""
  var terminer = 0
/// METHODES
// dépot
def depot(id : Int, comptes : Array[Double]) : Unit = {

  devise = readLine("Indiquez la devise du dépôt: 1) CHF ; 2) EUR > ").toInt
  while (devise < 1 || devise > 2) {
    devise = readLine("Indiquez la devise du dépôt: 1) CHF ; 2) EUR > ").toInt 
  }
  montant_depot = readLine("Indiquez le montant du dépôt > ").toInt
  while (montant_depot % 10 != 0 || montant_depot < 10) {
    println("Le montant doit être un multiple de 10")
    montant_depot = readLine("Indiquez le montant du dépôt > ").toInt
  }
  if (devise==2) comptes(id) += (montant_depot * 0.95)
  else  comptes(id) += montant_depot
  printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
}
// retrait
def retrait(id : Int, comptes : Array[Double]) : Unit = {

      plafond_retrait = comptes(id) * 0.1
      devise = readLine("Indiquez la devise: 1) CHF ; 2) EUR > ").toInt 
      while (devise < 1 || devise > 2) { devise = readLine("Indiquez la devise: 1) CHF ; 2) EUR > ").toInt }
      while (montant_retrait > plafond_retrait || montant_retrait % 10 != 0 || montant_retrait < 10) {
        montant_retrait = readLine("Indiquez le montant du retrait > ").toInt
        if (montant_retrait % 10 != 0 || montant_retrait < 10) println("Le montant doit être un multiple de 10")
        if (montant_retrait > plafond_retrait) {
          println("Votre plafond de retrait autorisé est de : "+ plafond_retrait)
        }
      }
      if (devise==1) {
        comptes(id) -=  montant_retrait
        if (montant_retrait >= 200) {
           while(choix_coupure < 1 || choix_coupure > 2) choix_coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
        } 
        ///grosses coupures chf
        if (choix_coupure == 1){
          if (montant_retrait >= 500) {
            nbr_500 = (montant_retrait/500).toInt
            montant_retrait -= 500*nbr_500
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_500+" billet(s) de 500 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_500 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o"){
              if (resultat.toInt == 0) {
                montant_retrait += 500 * nbr_500
                nbr_500 = 0
              }
              else if (resultat.toInt < nbr_500){
                montant_retrait += 500 * nbr_500
                nbr_500 = resultat.toInt
                montant_retrait -= 500*nbr_500
              }
            }
          }
          if (montant_retrait >= 200) {
            nbr_200 = (montant_retrait/200).toInt
            montant_retrait -= 200*nbr_200
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_200+" billet(s) de 200 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_200 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
             if (resultat != "o"){
               if (resultat.toInt == 0) {
                 montant_retrait += 200 * nbr_200
                 nbr_200 = 0
               }
               else if (resultat.toInt < nbr_200){
                 montant_retrait += 200 * nbr_200
                 nbr_200 = resultat.toInt
                 montant_retrait -= 200*nbr_200
               }
             }
          }
          if (montant_retrait >= 100) {
            nbr_100 = (montant_retrait/100).toInt
            montant_retrait -= 100*nbr_100
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_100+" billet(s) de 100 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_100 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o"){
              if (resultat.toInt == 0) {
                montant_retrait += 100 * nbr_100
                nbr_100 = 0
              }
              else if (resultat.toInt < nbr_100){
                montant_retrait += 100 * nbr_100
                nbr_100 = resultat.toInt
                montant_retrait -= 100*nbr_100
              }
            }
          }
          if (montant_retrait >= 50) {
            nbr_50 = (montant_retrait/50).toInt
            montant_retrait -= 50*nbr_50
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_50+" billet(s) de 50 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_50 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o") {
              if (resultat.toInt == 0) {
                montant_retrait += 50 * nbr_50
                nbr_50 = 0
              }
              else if (resultat.toInt < nbr_50){
                montant_retrait += 50 * nbr_50
                nbr_50 = resultat.toInt
                montant_retrait -= 50*nbr_50
              }
            }
          }
          if (montant_retrait >= 20) {
            nbr_20 = (montant_retrait/20).toInt
            montant_retrait -= 20*nbr_20
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_20+" billet(s) de 20 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_20 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o") {
              if (resultat.toInt == 0) {
                montant_retrait += 20 * nbr_20
                nbr_20 = 0
              }
              else if (resultat.toInt < nbr_20){
                montant_retrait += 20 * nbr_20
                nbr_20 = resultat.toInt
                montant_retrait -= 20*nbr_20
              }
            } 
          }
          if (montant_retrait >= 10) {
            nbr_10 = (montant_retrait/10).toInt
            montant_retrait -= 10*nbr_10
          }
          println("Veuillez retirer la somme demandée :")
          if (nbr_500 > 0) println(" "+nbr_500+" billet(s) de 500 CHF")
          if (nbr_200 > 0) println(" "+nbr_200+" billet(s) de 200 CHF")
          if (nbr_100 > 0) println(" "+nbr_100+" billet(s) de 100 CHF")
          if (nbr_50 > 0) println(" "+nbr_50+" billet(s) de 50 CHF")
          if (nbr_20 > 0) println(" "+nbr_20+" billet(s) de 20 CHF")
          if (nbr_10 > 0) println(" "+nbr_10+" billet(s) de 10 CHF")
          nbr_500 = 0
          nbr_200 = 0
          nbr_100 = 0
          nbr_50 = 0
          nbr_20 = 0
          nbr_10 = 0
          choix_coupure = 0
        } 
        ///petites coupures chf
        else if (choix_coupure == 2 || montant_retrait < 200) {
          if (montant_retrait >= 100) {
            nbr_100 = (montant_retrait/100).toInt
            montant_retrait -= 100*nbr_100
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_100+" billet(s) de 100 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_100 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o"){
              if (resultat.toInt == 0) {
                montant_retrait += 100 * nbr_100
                nbr_100 = 0
              }
              else if (resultat.toInt < nbr_100){
                montant_retrait += 100 * nbr_100
                nbr_100 = resultat.toInt
                montant_retrait -= 100*nbr_100
              }
            }
          }
          if (montant_retrait >= 50) {
            nbr_50 = (montant_retrait/50).toInt
            montant_retrait -= 50*nbr_50
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_50+" billet(s) de 50 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_50 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o") {
              if (resultat.toInt == 0) {
                montant_retrait += 50 * nbr_50
                nbr_50 = 0
              }
              else if (resultat.toInt < nbr_50){
                montant_retrait += 50 * nbr_50
                nbr_50 = resultat.toInt
                montant_retrait -= 50*nbr_50
              }
            }
          }
          if (montant_retrait >= 20) {
            nbr_20 = (montant_retrait/20).toInt
            montant_retrait -= 20*nbr_20
            println("Il reste " + montant_retrait + "CHF à distribuer")
            println("Vous pouvez obtenir au maximum "+nbr_20+" billet(s) de 20 CHF")
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while (resultat != "o" && (resultat.toInt >= nbr_20 || resultat.toInt < 0)) {
              resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (resultat != "o") {
              if (resultat.toInt == 0) {
                montant_retrait += 20 * nbr_20
                nbr_20 = 0
              }
              else if (resultat.toInt < nbr_20){
                montant_retrait += 20 * nbr_20
                nbr_20 = resultat.toInt
                montant_retrait -= 20*nbr_20
              }
            } 
          }
          if (montant_retrait >= 10) {
            nbr_10 = (montant_retrait/10).toInt
            montant_retrait -= 10*nbr_10
          }
          println("Veuillez retirer la somme demandée :")
          if (nbr_100 > 0) println(" "+nbr_100 + " billet(s) de 100 CHF")
          if (nbr_50 > 0) println(" "+nbr_50 + " billet(s) de 50 CHF")
          if (nbr_20 > 0) println(" "+nbr_20 + " billet(s) de 20 CHF")
          if (nbr_10 > 0) println(" "+nbr_10 + " billet(s) de 10 CHF")
          nbr_100 = 0
          nbr_50 = 0
          nbr_20 = 0
          nbr_10 = 0
          choix_coupure = 0
        }
      }
      ///devise en euro
      else if (devise == 2) {
        comptes(id) -=  montant_retrait * 0.95
        if (montant_retrait >= 100) {
          nbr_100 = (montant_retrait/100).toInt
          montant_retrait -= 100*nbr_100
          println("Il reste " + montant_retrait + "EURO à distribuer")
          println("Vous pouvez obtenir au maximum "+nbr_100+" billet(s) de 100 EURO")
          resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          while (resultat != "o" && (resultat.toInt >= nbr_100 || resultat.toInt < 0)) {
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }
          if (resultat != "o"){
            if (resultat.toInt == 0) {
              montant_retrait += 100 * nbr_100
              nbr_100 = 0
            }
            else if (resultat.toInt < nbr_100){
              montant_retrait += 100 * nbr_100
              nbr_100 = resultat.toInt
              montant_retrait -= 100*nbr_100
            }
          }
        }
        if (montant_retrait >= 50) {
          nbr_50 = (montant_retrait/50).toInt
          montant_retrait -= 50*nbr_50
          println("Il reste " + montant_retrait + "EURO à distribuer")
          println("Vous pouvez obtenir au maximum "+nbr_50+" billet(s) de 50 EURO")
          resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          while (resultat != "o" && (resultat.toInt >= nbr_50 || resultat.toInt < 0)) {
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }
          if (resultat != "o") {
            if (resultat.toInt == 0) {
              montant_retrait += 50 * nbr_50
              nbr_50 = 0
            }
            else if (resultat.toInt < nbr_50){
              montant_retrait += 50 * nbr_50
              nbr_50 = resultat.toInt
              montant_retrait -= 50*nbr_50
            }
          }
        }
        if (montant_retrait >= 20) {
          nbr_20 = (montant_retrait/20).toInt
          montant_retrait -= 20*nbr_20
          println("Il reste " + montant_retrait + "EURO à distribuer")
          println("Vous pouvez obtenir au maximum "+nbr_20+" billet(s) de 20 EURO")
          resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          while (resultat != "o" && (resultat.toInt >= nbr_20 || resultat.toInt < 0)) {
            resultat = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          }
          if (resultat != "o") {
            if (resultat.toInt == 0) {
              montant_retrait += 20 * nbr_20
              nbr_20 = 0
            }
            else if (resultat.toInt < nbr_20){
              montant_retrait += 20 * nbr_20
              nbr_20 = resultat.toInt
              montant_retrait -= 20*nbr_20
            }
          } 
        }
        if (montant_retrait >= 10) {
          nbr_10 = (montant_retrait/10).toInt
          montant_retrait -= 10*nbr_10
        } 
        println("Veuillez retirer la somme demandée :")
        if (nbr_100 > 0) println(" "+nbr_100 + " billet(s) de 100 EURO")
        if (nbr_50 > 0) println(" "+nbr_50 + " billet(s) de 50 EURO")
        if (nbr_20 > 0) println(" "+nbr_20 + " billet(s) de 20 EURO")
        if (nbr_10 > 0) println(" "+nbr_10 + " billet(s) de 10 EURO")
        nbr_100 = 0
        nbr_50 = 0
        nbr_20 = 0
        nbr_10 = 0
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }
// Changement code pin
def changepin(id : Int, codespin : Array[String]) : Unit = {

  codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toString
  while (codespin(id).length() < 8){
    println("Votre code pin ne contient pas au moins 8 caractères")
    codespin(id) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ").toString
  }
}

  ///// DEBUT PROGRAMME !

  def main(args: Array[String]): Unit = {
    while (terminer == 0) {
       /// IDENTIFICATION DU COMPTE
      id = readLine("saisissez votre identifiant > ").toInt
      if (id >= nbclients) {
        println("Cet identifiant n’est pas valable.")
        sys.exit()
      }

      /// CODE PIN
       if (code_pin != codespin(id)) code_pin = readLine("Saisissez votre code pin >").toString
        while (code_pin != codespin(id) && nbr_essais_pin != 3) {
        if (code_pin != codespin(id)) code_pin = readLine("Code pin erroné, il vous reste " + (3 - nbr_essais_pin) +" tentatives >").toString
        nbr_essais_pin += 1
        if (nbr_essais_pin == 3 && code_pin != codespin(id)){
         println("Trop d’erreurs, abandon de l’identification")
          id = readLine("saisissez votre identifiant > ").toInt
          if (id >= nbclients) {
            println("Cet identifiant n’est pas valable.")
            sys.exit()
          }
        nbr_essais_pin = 1
        code_pin = "none"
        if (code_pin != codespin(id)) code_pin = readLine("Saisissez votre code pin >").toString
        }
      }
        nbr_essais_pin = 1

    /// MENU
    while (choix !=5){
      println("Choisissez votre opération:")
      println("    1) Dépot")
      println("    2) Retrait")
      println("    3) Consultation du compte")
      println("    4) Changement du code pin")
      println("    5) Terminer")
      choix = readLine("Votre choix: ").toInt

      /// CHANGEMENT CODE PIN
      if (choix == 4) changepin(id, codespin)

        /// DEPOT
      if (choix == 1) depot(id, comptes)

        /// RETRAIT
      if (choix == 2) retrait(id, comptes) 

        /// CONSULTATION DU COMPTE
      if (choix == 3){
         printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
      }
    }

    /// TERMINER
    if (choix == 5) println("Fin des opérations, n’oubliez pas de récupérer votre carte.")

    /// FIN PROGRAMME
    choix = 0
    code_pin = "none"
    }
  }
}