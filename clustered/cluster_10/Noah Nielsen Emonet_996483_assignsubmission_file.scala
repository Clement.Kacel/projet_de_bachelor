//Assignment: Noah Nielsen Emonet_996483_assignsubmission_file

import scala.io.StdIn._

object Main {

    var montantretrait = 0
    var tableaucompte = new Array[Double](100)
    tableaucompte = Array.fill(100)(1200.0)
    var tableaucodepin = new Array[String](100)
    tableaucodepin = Array.fill(100)("INTRO1234")
    val identifiantClient = Array.tabulate(100)(i => i)
    var id = 0
    var code = ("code")
    var choix = 0
  



    def operation() : Unit = {
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")
      choix = readInt()
    }

    def demandeidetpin(): Unit = {
      println("Saisissez votre code identifiant >")
      id = readInt()
      if (id > 99) {
        println("Cet identifiant n’est pas valable.")
        System.exit(0)
      }
      println("Saisissez votre code pin")
      code = readLine()
      if (code != tableaucodepin(id)){//vérifie que le code est le bon
      println("Code pin erroné, il vous reste 2 tentatives")
      println("Saisissez votre code pin")
      code = readLine()
      }
      if (code != tableaucodepin(id)){
      println("Code pin erroné, il vous reste 1 tentative")
      println("Saisissez votre code pin")
      code = readLine()
      }
      if (code != tableaucodepin(id))
        println("Trop d’erreurs, abandon de l’identification")
    }

    def depot(id: Int, tableaucompte: Array[Double]): Unit = {
      var devise = 0
      

      while(choix == 1){
      println("veuillez indiquer la devise 1)CHF 2)EUR")
      devise = readInt()
      while (devise > 2 || devise < 1) {
        println("veuillez indiquer la devise 1)CHF 2)EUR")
        devise = readInt()
      }
      println("Indiquez le montant du dépôt.")
      var montantdepot = readInt()

      while (montantdepot % 10 != 0) { //oblige montantdepot à etre un multiple de 10
        println("Le montant doit être un multiple de 10")
        println("Indiquez le montant du dépôt.")
        montantdepot = readInt()
      }
      if (devise == 1) {
        tableaucompte(id) = montantdepot + tableaucompte(id) // mets à jour montant total
        println(f"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : ${tableaucompte(id)}%.2f CHF")
        println("Choisissez votre opération :")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
        println("Votre choix : ")
        choix = readInt()
      }
      if (devise == 2) {
        tableaucompte(id) = montantdepot * 0.95 + tableaucompte(id) // mets à jour montant total
        println(f"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: ${tableaucompte(id)}%.2f CHF")
        println("Choisissez votre opération :")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
        println("Votre choix : ")
        choix = readInt()
      }
    }
    }




    def retrait(id: Int, tableaucompte: Array[Double], montantretrait: Int): Unit = { // Retrait
    var BilletDe10 = 0
    var BilletDe20 = 0
    var BilletDe50 = 0
    var BilletDe100 = 0
    var BilletDe200 = 0
    var BilletDe500 = 0
    var devise = 0
    var TypeCoupures = 0
    var DeviseEnTexte = "devise"
    var MontantRetraitTotal = 0
    

    

    while(choix == 2){
    println("veuillez indiquer la devise 1)CHF 2)EUR")
    devise = readInt()
    while (devise > 2 || devise < 1) {
      println("veuillez indiquer la devise 1)CHF 2)EUR")
      devise = readInt()
    }
        if (devise == 1) {
          DeviseEnTexte = "CHF"
        }
        if (devise == 2) {
          DeviseEnTexte = "EUR"
        }

        println("Indiquez le montant du retrait")
        var montantretrait = readInt()
        while (montantretrait % 10 != 0 || montantretrait > tableaucompte(id) / 10) {//oblige le montant à être multiple de 10 et supérieur à 1 dixieme du total du compte
          if (montantretrait % 10 != 0) {
            println("Le montant doit être un multiple de 10")
          }
          if (montantretrait >= tableaucompte(id) / 10) {
            println("Votre plafond de retrait autorisé est de : " + tableaucompte(id) / 10)
          }
          println("Indiquez le montant du retrait")
          montantretrait = readInt()
        }
          MontantRetraitTotal = montantretrait
        if (devise == 1 && montantretrait >= 200) {
          println("En 1) grosses coupures, 2) petites coupures")
          TypeCoupures = readInt()
          while (TypeCoupures > 2 || devise < 1) {//oblige la répétition si valeur différente de 1 ou 2
            println("En 1) grosses coupures, 2) petites coupures")
            TypeCoupures = readInt()
          }
        }

        while (montantretrait > 0) {
          if (TypeCoupures == 1 && devise == 1) {
            if (montantretrait >= 500 && TypeCoupures == 1) {
              println("Il reste " + montantretrait + " CHF à distribuer")
              println(f"Vous pouvez obtenir au maximum ${montantretrait / 500}%.0f billet(s) de 500 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
              var input = readLine()//input sert à éviter des problèmes de comparaisons entre des Int et des stringdans mes conditions
              if (input == "o") {
                BilletDe500 = montantretrait / 500.toInt
              } else {
                BilletDe500 = input.toInt
              }
              while (BilletDe500 > montantretrait / 500.toInt) {
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
                var input = readLine()
                if (input == "o") {
                  BilletDe500 = montantretrait / 500.toInt
                } else {
                  BilletDe500 = input.toInt
                }
              }
              montantretrait = montantretrait - 500 * BilletDe500

      }
      }
        if(montantretrait >= 200 && TypeCoupures == 1 && devise == 1){
          println(s"Il reste $montantretrait CHF à distribuer")
          println(s"Vous pouvez obtenir au maximum ${montantretrait/200} billet(s) de 200 CHF")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            BilletDe200 = montantretrait/200.toInt
          } else{
            BilletDe200 = input.toInt
          }
          while(BilletDe200 > montantretrait/200.toInt){
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
              var input = readLine()
              if(input == "o"){
                BilletDe200 = montantretrait/200.toInt
              } else{
                BilletDe200 = input.toInt
            }
          }
          montantretrait = montantretrait - 200*BilletDe200
        }


            if(montantretrait >= 100){
              println(s"Il reste $montantretrait $DeviseEnTexte à distribuer")
              println(f"Vous pouvez obtenir au maximum ${montantretrait/100} billet(s) de 100 $DeviseEnTexte")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
              var input = readLine()
              if(input == "o"){
                BilletDe100 = montantretrait/100.toInt
              }else{
                BilletDe100 = input.toInt
              }
              while(BilletDe100 > montantretrait/100.toInt){
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
                var input = readLine()
                if(input == "o"){
                  BilletDe100 = montantretrait/100.toInt
                }else{
                  BilletDe100 = input.toInt
                }
              }
              montantretrait = montantretrait - 100*BilletDe100

      }
        if(montantretrait >= 50){
          println(s"Il reste $montantretrait $DeviseEnTexte à distribuer")
          println(f"Vous pouvez obtenir au maximum ${montantretrait/50} billet(s) de 50 $DeviseEnTexte")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            BilletDe50 = montantretrait/50.toInt
          }else{
            BilletDe50 = input.toInt
          }
          while(BilletDe50 > montantretrait/50){
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
              var input = readLine()
              if(input == "o"){
                BilletDe50 = montantretrait/50.toInt
              }else{
                BilletDe50 = input.toInt
            }
          }
          montantretrait = montantretrait - 50*BilletDe50
        }

        if(montantretrait >= 20){
          println(s"Il reste $montantretrait $DeviseEnTexte à distribuer")
          println(f"Vous pouvez obtenir au maximum ${montantretrait/20} billet(s) de 20 $DeviseEnTexte")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
          var input = readLine()
          if(input == "o"){
            BilletDe20 = montantretrait/20.toInt
          }else{
            BilletDe20 = input.toInt
          }
          while(BilletDe20 > montantretrait/20.toInt){
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
              var input = readLine()
              if(input == "o"){
                BilletDe20 = montantretrait/20.toInt
              }else{
                BilletDe20 = input.toInt
            }
          }
          montantretrait = montantretrait - 20*BilletDe20
        }

      if(montantretrait >= 10){
        println(s"Il reste $montantretrait $DeviseEnTexte à distribuer")
        println(f"Vous pouvez obtenir au maximum ${montantretrait/10} billet(s) de 10 $DeviseEnTexte")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
        var input = readLine()
        if(input == "o"){
          BilletDe10 = montantretrait/10.toInt
        }else{
          BilletDe10 = input.toInt
        }
        while(BilletDe10 > montantretrait/10.toInt || BilletDe10 != montantretrait/10.toInt){//empêche l'utilisateur de retirer moins que ce qu'il a demandé au total
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            var input = readLine()
            if(input == "o"){
              BilletDe10 = montantretrait/10.toInt
            }else{
              BilletDe10 = input.toInt
          }
        }
        montantretrait = montantretrait - 10*BilletDe10
      }

        if(montantretrait == MontantRetraitTotal){//Dans le cas ou l'utilisateur n'a encore retiré aucun billet de monnaie
          BilletDe10 = montantretrait/10
        }

        montantretrait = montantretrait - 10*BilletDe10

      }
      if(devise == 1){
      println("Veuillez retirer la somme demandée : ")
      if(BilletDe500>0){
      println(BilletDe500+ " billet(s) de 500 CHF")
      }
        if(BilletDe200>0){
      println(BilletDe200+ " billet(s) de 200 CHF")
      }
        if(BilletDe100>0){
      println(BilletDe100+ " billet(s) de 100 CHF")
      }
        if(BilletDe50>0){
      println(BilletDe50+ " billet(s) de 50 CHF")
      }
        if(BilletDe20>0){
      println(BilletDe20+ " billet(s) de 20 CHF")
      }
        if(BilletDe10>0){
      println(BilletDe10+ " billet(s) de 10 CHF")
      }
      println(f"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : ${tableaucompte(id)-MontantRetraitTotal}%.2f CHF")
        tableaucompte(id) = tableaucompte(id) - MontantRetraitTotal

      }
      if(devise == 2){
      println("Veuillez retirer la somme demandée : ")
      if(BilletDe100>0){
      println(BilletDe100+ " billet(s) de 100 EUR")
      }
        if(BilletDe50>0){
      println(BilletDe50+ " billet(s) de 50 EUR")
      }
        if(BilletDe20>0){
      println(BilletDe20+ " billet(s) de 20 EUR")
      }
        if(BilletDe10>0){
      println(BilletDe10+ " billet(s) de 10 EUR")
      }
      println(f"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : ${tableaucompte(id)-MontantRetraitTotal*0.95}%.2f CHF")
        tableaucompte(id) = tableaucompte(id) - MontantRetraitTotal * 0.95
      }
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")
      choix = readInt()
      }
    }




  def changepin(id : Int, codespin : Array[String]) : Unit = {
    
    while(choix == 4){
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    tableaucodepin(id) = readLine()
    while(tableaucodepin(id).length < 8){
    println("Votre code pin ne contient pas au moins 8 caractères") 
    tableaucodepin(id) = readLine()
    }
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")
      choix = readInt()
  }
  }
  



  
  def main(args: Array[String]): Unit = {


    while(code != tableaucodepin(id)){
      demandeidetpin()  
    }
    operation()

    
    while(choix != 6){
    if(choix == 1){//dépôt
    depot(id, tableaucompte)
    }
    if(choix == 3){//consulter 
      println(f"Le montant disponible sur votre compte est de : ${tableaucompte(id)}%.2f CHF")
      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")
      choix = readInt()
      
    } 

      
    if(choix == 2){ // Retrait
            retrait(id, tableaucompte, montantretrait)
        }
    if(choix == 4){//changement pin
      changepin(id, tableaucodepin)
    }
      
    
    if(choix == 5){//fin des opérations
      println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
      demandeidetpin()
      while(code != tableaucodepin(id)){
      demandeidetpin()}
      if(code == tableaucodepin(id)){
        operation()
      }
  
    }
    }



      //opération de fin
  }
}
