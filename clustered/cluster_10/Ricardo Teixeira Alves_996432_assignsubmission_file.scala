//Assignment: Ricardo Teixeira Alves_996432_assignsubmission_file

import scala.io.StdIn._

object Main { 
  def depot(idclient : Int, valeurcompte : Array[Double]) : Unit={
    var montantcompte = valeurcompte(idclient)
    // Euro ou CHF
    var deviseDepot = 0
    do {
      println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
      deviseDepot = readInt()
    } while (deviseDepot != 1 && deviseDepot != 2)

    var montantDepot = 0
    do {
      println("Indiquez le montant du dépôt > ")
      montantDepot = readInt()
      if (montantDepot % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
    } while (montantDepot % 10 != 0)

    // Taux de change
    val montantEnCHF = if (deviseDepot == 2) montantDepot * 0.95 else montantDepot

    // Montant final
    montantcompte += montantEnCHF
    println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + montantcompte + " CHF")
  valeurcompte(idclient) = montantcompte
  }
  
             
  def retrait(idclient : Int, valeurcompte : Array[Double]) : Unit={
  var montantcompte = valeurcompte(idclient)
  val coupure500 = 500
    val coupure200 = 200
    val coupure100 = 100
    val coupure50 = 50
    val coupure20 = 20
    val coupure10 = 10
    var nb500 = 0
    var nb200 = 0
    var nb100 = 0
    var nb50 = 0
    var nb20 = 0
    var nb10 = 0
    var montantRetrait = 0
    var montantRestant = 0
    var deviseRetrait = 0
    var saisie = ""
    var saisie2 = 10000
    var nombreMaxCoupures = 0

    do {
      println("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ")
      deviseRetrait = readInt()
    } while (deviseRetrait != 1 && deviseRetrait != 2)

    // Valeur Retrait

    do {
      println("Indiquez le montant du retrait > ")
      montantRetrait = readInt()
      if (montantRetrait % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
      if(montantRetrait > montantcompte * 0.1){
        println("Votre plafond de retrait autorisé est de : " + (montantcompte*0.1).toString)
      }
    } while(montantRetrait > montantcompte * 0.1 || montantRetrait % 10 != 0)


    if (deviseRetrait == 1) {
      montantcompte -= montantRetrait
      montantRestant = montantRetrait

      if (montantRestant >= coupure500) {
        nombreMaxCoupures = montantRestant / coupure500

        if (nombreMaxCoupures > 0) {
          saisie = ""
          saisie2 = 10000
          println("Il reste " + montantRestant.toString + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure500.toString + " CHF")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure500 * nombreMaxCoupures
              nb500 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
                saisie2 = saisie.toInt
                if (saisie2 < nombreMaxCoupures) {
                  montantRestant -= coupure500 * saisie2
                  nb500 = saisie2
                  saisie = "o"
                }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }


      if (montantRestant >= coupure200) {
        nombreMaxCoupures = montantRestant / coupure200

        if (nombreMaxCoupures > 0) {
          saisie = ""
          saisie2 = 10000
          println("Il reste " + montantRestant.toString + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure200.toString + " CHF")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure200 * nombreMaxCoupures
              nb200 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
                saisie2 = saisie.toInt
                if (saisie2 < nombreMaxCoupures) {
                  montantRestant -= coupure200 * saisie2
                  nb200 = saisie2
                  saisie = "o"
                }
              }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }

      if (montantRestant >= coupure100) {
        nombreMaxCoupures = montantRestant / coupure100

        if (nombreMaxCoupures > 0) {
          saisie = ""
          saisie2 = 10000
          println("Il reste " + montantRestant.toString + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure100.toString + " CHF") 
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure100 * nombreMaxCoupures
              nb100 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
                saisie2 = saisie.toInt
                if (saisie2 < nombreMaxCoupures) {
                  montantRestant -= coupure100 * saisie2
                  nb100 = saisie2
                  saisie = "o"
                }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }

      if (montantRestant >= coupure50) {
        nombreMaxCoupures = montantRestant / coupure50

        if (nombreMaxCoupures > 0) {
          saisie = ""
          saisie2 = 10000
          println("Il reste " + montantRestant.toString + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure50.toString + " CHF")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure50 * nombreMaxCoupures
              nb50 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
                saisie2 = saisie.toInt
                if (saisie2 < nombreMaxCoupures) {
                  montantRestant -= coupure50 * saisie2
                  nb50 = saisie2
                  saisie = "o"
                }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }

      if (montantRestant >= coupure20) {
        nombreMaxCoupures = montantRestant / coupure20

        if (nombreMaxCoupures > 0) {
          saisie = ""
          saisie2 = 10000
          println("Il reste " + montantRestant.toString + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure20.toString + " CHF")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure20 * nombreMaxCoupures
              nb20 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
                saisie2 = saisie.toInt
                if (saisie2 < nombreMaxCoupures) {
                  montantRestant -= coupure20 * saisie2
                  nb20 = saisie2
                  saisie = "o"
                }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }

      if (montantRestant >= coupure10) {
        val nombreMaxCoupures = montantRestant / coupure10

        if (nombreMaxCoupures > 0) {
          println("Il reste " + montantRestant.toString + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure10.toString + " CHF")
          nb10 = nombreMaxCoupures.toInt
          montantRestant -= coupure10 * nombreMaxCoupures
        }
      }

      if (montantRestant == 0) {
        println("Veuillez retirer la somme demandée :")
        if(nb500 != 0){
          println(nb500 + " billet(s) de " + coupure500.toString + " CHF")
        }
        if(nb200 != 0){
          println(nb200 + " billet(s) de " + coupure200.toString + " CHF")
        }
        if(nb100 != 0){
          println(nb100 + " billet(s) de " + coupure100.toString + " CHF")
        }
        if(nb50 != 0){
          println(nb50 + " billet(s) de " + coupure50.toString + " CHF")
        }
        if(nb20 != 0){
          println(nb20 + " billet(s) de " + coupure20.toString + " CHF")
        }
        if(nb10 != 0){
          println(nb10 + " billet(s) de " + coupure10.toString + " CHF")
        }
      }

    }
    if (deviseRetrait == 2) {
      montantcompte -= montantRetrait*0.95
      montantRestant = montantRetrait 
      if (montantRestant >= coupure200) {
        val nombreMaxCoupures = montantRestant / coupure200

        if (nombreMaxCoupures > 0) {
          saisie = ""
          saisie2 = 10000
          println("Il reste " + montantRestant.toString + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure100.toString + " EUR")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure100 * nombreMaxCoupures
              nb100 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
              saisie2 = saisie.toInt
              if(saisie2< nombreMaxCoupures){
                montantRestant -= coupure100 * saisie2
                nb100 = saisie2
              }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }
      if (montantRestant >= coupure50) {
        saisie = ""
        saisie2 = 10000
        val nombreMaxCoupures = montantRestant / coupure50

        if (nombreMaxCoupures > 0) {
          println("Il reste " + montantRestant.toString + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure50.toString + " EUR")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure50 * nombreMaxCoupures
              nb50 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
              saisie2 = saisie.toInt
              if(saisie2< nombreMaxCoupures){
                montantRestant -= coupure50 * saisie2
                nb50 = saisie2
                saisie = "o"
              }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }

      if (montantRestant >= coupure20) {
        saisie = ""
        saisie2 = 10000
        val nombreMaxCoupures = montantRestant / coupure20

        if (nombreMaxCoupures > 0) {
          println("Il reste " + montantRestant.toString + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure20.toString + " EUR")
          do {
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            saisie = readLine()

            if (saisie == "o") {
              montantRestant -= coupure20 * nombreMaxCoupures
              nb20 = nombreMaxCoupures.toInt
              saisie2 = 0
            } else {
              saisie2 = saisie.toInt
              if(saisie2< nombreMaxCoupures){
                montantRestant -= coupure20 * saisie2
                nb20 = saisie2
                saisie = "o"
              }
            }
          } while (saisie != "o" && saisie2 >= nombreMaxCoupures)
        }
      }

      if (montantRestant >= coupure10) {
        val nombreMaxCoupures = montantRestant / coupure10

        if (nombreMaxCoupures > 0) {
          println("Il reste " + montantRestant.toString + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nombreMaxCoupures.toString + " billet(s) de " + coupure10.toString + " EUR")
          nb10 = nombreMaxCoupures.toInt
          montantRestant -= coupure10 * nombreMaxCoupures
        }
      }

      if (montantRestant == 0) {
        println("Veuillez retirer la somme demandée :")
        if(nb200 != 0){
          println(nb200 + " billet(s) de " + coupure200.toString + " EUR")
        }
        if(nb100 != 0){
          println(nb100 + " billet(s) de " + coupure100.toString + " EUR")
        }
        if(nb50 != 0){
          println(nb50 + " billet(s) de " + coupure50.toString + " EUR")
        }
        if(nb20 != 0){
          println(nb20 + " billet(s) de " + coupure20.toString + " EUR")
        }
        if(nb10 != 0){
          println(nb10 + " billet(s) de " + coupure10.toString + " EUR")
        }
      }
    }
  valeurcompte(idclient) = montantcompte
  }
  
  def changepin(idclient : Int, codespin : Array[String]) : Unit = {
    var nvcodepin = " "
    while(nvcodepin.length < 8 ){
      nvcodepin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
      if(nvcodepin.length < 8){
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
    }
    codespin(idclient) = nvcodepin
  }
  
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    var idclient = 2 

    // Tableaux des comptes et des codes PIN
    val valeurcompte= Array.fill(nbclients)(1200.0)
    val codespin = Array.fill(nbclients)("INTRO1234")
    idclient = idclient-1
    
    var horslim = false
    while(idclient <= nbclients && idclient>=1 && horslim == false) {
      idclient = readLine("Saisissez votre identifiant").toInt
      if (idclient > nbclients || idclient<1) {
        println("Cet identifiant n'est pas valable.") 
        horslim = true
      }
      else if(horslim == false){
     
       
      // tentatives
      var tentativesRestantes = 3
      var codebon = false
      var saisiePin = ""
        
      // Boucle principale
      var quitter = false
      while (!quitter) {
        
        // Menu principal
        println("\nChoisissez votre opération :")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
  
        println("Votre choix : ")
        val choix = readInt()
      
        // PIN
        if (choix != 5 && tentativesRestantes > 0) {
  
          if(!codebon){
            
            do {
              println("Saisissez votre code pin > ")
              saisiePin = readLine()
              if (saisiePin != codespin(idclient)) {
                tentativesRestantes -= 1
                println("Code pin erroné, il vous reste " + tentativesRestantes + " tentatives")
              }
            } while (saisiePin != codespin(idclient) && tentativesRestantes > 0)
  
            // après 3 tentatives 
            if (tentativesRestantes == 0) {
              println("Trop d’erreurs, abandon de l’identification.")
              quitter = true
            }
            if(saisiePin == codespin(idclient)){
              tentativesRestantes = 3
              codebon = true
            }
          }
        }
  
        // Choix 1 = dépôt 
        if (choix == 1 && !quitter) {
          depot(idclient,valeurcompte)
        }
        // Choix 3 = consultation
        if (choix == 3 && !quitter) {
          printf("Votre montant disponible sur votre compte est de : %.2f \n", valeurcompte(idclient))
          print(" CHF.")
        }
        // Choix 2 = retrait
        if (choix == 2 && !quitter) {
          retrait(idclient,valeurcompte)
        }

        // choix 4 = changement du code pin
        if ( choix == 4){
          changepin(idclient,codespin)
        }
  // Choix 5 = fin des opérations
        if (choix == 5) {
          println("Fin des opérations, n'oubliez pas de récupérer votre carte")
          quitter = true
          tentativesRestantes = 3
          saisiePin = ""
      }
    }
  }
  }
}
}
