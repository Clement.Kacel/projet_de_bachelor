//Assignment: Victor Jay Charles Vincent_996449_assignsubmission_file

import io.StdIn._
object Main {

  //initialisation des variables

  var codespin = Array.fill(100)("INTRO1234") ; //pin
  var compteur_pin = false ;
  var saisie = " " ;
  var tentative = 3 ;

  var devise = 0 ;
  var comptes = Array.fill(100)(1200.0) ; //montant
  var depot = 0.0 ;

  var retrait = 0 ;
  var retrait_autorise = 10.0/100.0*comptes(0) ;
  var coupure = 0 ;

  var billet = 0 ;
  var moduleur = 0 ;
  var soustrayant = 0 ;
  var selection = " " ;
  var distributeur = " " ;
  var cagnote_str = "" ;
  var cagnote_int = 0 ;    
  var cagnote = 0.0 ;

  var operations = true ;
  var boucle = true ;
  var pas_passer = false ;

  var id = 0 ;
  var identification = true ;
  var identification_rate = false ;

  var boucle_princ = 0 ;

  // déclaration des méthodes

  def depot(id:Int,comptes:Array[Double]): Unit = {
    compteur_pin = true ;
    devise = readLine("Indiquez la devise du dépôt : 1)CHF ; 2)EUR > ").toInt ;
    depot = readLine("Indiquez le montant du dépôt > ").toInt ;
    if (depot % 10 == 0) {
      if ((devise == 2)) {
        depot = depot * 0.95 ;
      }
      comptes(id) = comptes(id) + depot ;
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", comptes(id)) ;
      println('\n') ;
      devise = 0 ;
      coupure = 0 ;
      } else {
      while (depot % 10 != 0) {
        depot = readLine("Le montant doit être un multiple de 10 ").toInt ;
      }
      comptes(id) = comptes(id) + depot ;
      printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", comptes(id)) ;
      println('\n') ;
      devise = 0 ;
      coupure = 0 ;
    }
  }

  def retrait(id:Int,comptes:Array[Double]): Unit = {
    compteur_pin = true ;
    while ((devise != 1)&&(devise != 2)) {
      devise = readLine("Indiquez la devise:1 CHF,2:EUR > ").toInt ;
    }
    retrait = readLine("Indiquez le montant du retrait > ").toInt ;
    retrait_autorise = 10.0/100.0*comptes(id) ;
    while ((retrait % 10 != 0)||(retrait > retrait_autorise)) {
      if (retrait % 10 != 0) {
        retrait = readLine("Le montant doit être un multiple de 10 ").toInt ;
      }
      else if (retrait > retrait_autorise) {
        println("Votre plafond de retrait autorisé est de : " + retrait_autorise) ;
        retrait = readLine("Indiquez le montant du retrait > ").toInt ;
      }
    }
    if ((retrait % 10 == 0)&&(retrait<=retrait_autorise)) {
      if (devise == 2) {
        retrait = retrait ;
      }
      if (devise == 1) {
        if (retrait>=200) {
          while ((coupure != 1)&&(coupure != 2)) {
            coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt ;
          }
          //opération de coupure en CHF
          if (coupure == 1) {
            //grosses coupures en CHF
            var billet = 500 ;
            var moduleur = 0 ;
            var soustrayant = 0 ;
            while (billet>=10) {
              if (billet >= 100) {
                moduleur = 100 ;
                soustrayant = 50 ;
              } else if (billet < 100) {
                moduleur = 10 ;
                soustrayant = 5 ;
              }
              if (billet%moduleur == 0) {
                retrait = retrait - cagnote_int ;
                if (retrait/billet != 0) {
                  println("Il reste " + retrait + " à distribuer ") ;
                  println("Vous pouvez obtenir au maximum " + retrait/billet + " billet(s) de " + billet) ;
                  if (billet != 10) {
                    selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ; 
                  }
                }
                if ((selection == "o")||(billet == 10)) {
                  if (retrait/billet != 0) {
                    cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " CHF" + '\n' ;
                  }
                  cagnote_int = (retrait/billet)*billet ;
                  cagnote = cagnote + (retrait/billet)*billet ;
                } else {
                  while ((selection != "o")&&(boucle==true)) {
                    if (selection == "o") {
                      if (retrait/billet != 0) {
                        cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " CHF" + '\n' ;
                      }
                      cagnote_int = (retrait/billet)*billet ;
                      cagnote = cagnote + (retrait/billet)*billet ;
                      boucle = false ;
                    }
                    if (boucle == true) {
                      if ((selection.toInt > retrait/billet)||(selection.toInt == retrait/billet)) {
                        selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ;
                      } else {
                        if ((selection.toInt < retrait/billet)&&(selection.toInt != 0)) {
                          if (retrait/billet != 0) {
                            cagnote_str = cagnote_str + selection.toInt + " billet(s) de " + billet + " CHF" + '\n' ;
                          }
                          cagnote_int = selection.toInt*billet ;
                          cagnote = cagnote + selection.toInt*billet ;
                        }
                        cagnote_int = selection.toInt*billet ;
                        cagnote = cagnote + selection.toInt*billet ;
                        boucle = false ;
                      }
                    }
                  }
                  boucle = true ;
                }
                billet = billet/2 ;
                } else {
                billet = billet - soustrayant ;
              }
              if (billet < 10) {
                println(" ") ;
                println("Veuillez retirer la somme demandée : " + '\n' + cagnote_str) ;
                cagnote_str = "" ;
                cagnote_int = 0 ;
              }
            }
          }
          if (coupure == 2) {
            //petites coupures en CHF
            billet = 100 ;
            moduleur = 0 ;
            while (billet>=10) {
              if (billet >= 100) {
                  moduleur = 100 ;
              } else if (billet < 100) {
                  moduleur = 10 ;
              }
              if (billet%moduleur == 0) {
                retrait = retrait - cagnote_int ;
                if (retrait/billet != 0) {
                  println("Il reste " + retrait + " à distribuer ") ;
                  println("Vous pouvez obtenir au maximum " + retrait/billet + " billet(s) de " + billet) ;
                  if (billet != 10) {
                    selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ; 
                  }
                }
                if ((selection == "o")||(billet == 10)) {
                  if (retrait/billet != 0) {
                    cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " CHF" + '\n' ;
                  }
                  cagnote_int = (retrait/billet)*billet ;
                  cagnote = cagnote + (retrait/billet)*billet ;
                } else {
                  while ((selection != "o")&&(boucle==true)) {
                    if (selection == "o") {
                      if (retrait/billet != 0) {
                        cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " CHF" + '\n' ;
                      }
                      cagnote_int = (retrait/billet)*billet ;
                      cagnote = cagnote + (retrait/billet)*billet ;
                      boucle = false ;
                    }
                    if (boucle == true) {
                      if ((selection.toInt > retrait/billet)||(selection.toInt == retrait/billet)) {
                        selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ;
                      } else {
                        if ((selection.toInt < retrait/billet)&&(selection.toInt != 0)) {
                          if (retrait/billet != 0) {
                            cagnote_str = cagnote_str + selection.toInt + " billet(s) de " + billet + " CHF" + '\n' ;
                          }
                          cagnote_int = selection.toInt*billet ;
                          cagnote = cagnote + selection.toInt*billet ;
                        }
                        cagnote_int = selection.toInt*billet ;
                        cagnote = cagnote + selection.toInt*billet ;
                        boucle = false ;
                      }
                    }
                  }
                  boucle = true ;
                }
                billet = billet/2 ;
              } else {
                billet = billet - 5 ;
              }
              if (billet < 10) {
                println(" ") ;
                println("Veuillez retirer la somme demandée : " + '\n' + cagnote_str) ;
                cagnote_str = "" ;
                cagnote_int = 0 ;
              }
            }
          }
        } else {  // (retrait < 200)
          var billet = 100 ;
          var moduleur = 0 ;
          while (billet >= 10) {
            if (billet >= 100) {
              moduleur = 100 ;
            } else if (billet < 100) {
              moduleur = 10 ;
            }
            if ((billet % moduleur == 0)&&(retrait>=billet)) {
              retrait = retrait - cagnote_int ;
              if (retrait/billet != 0) {
                println("Il reste " + retrait + " à distribuer ") ;
                println("Vous pouvez obtenir au maximum " + retrait/billet + " billet(s) de " + billet) ;
                if (billet != 10) {
                  selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ; 
                }
              }
              if ((selection == "o")||(billet == 10)) {
                if (retrait/billet != 0) {
                  cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " CHF" + '\n' ;
                }
                cagnote_int = (retrait/billet)*billet ;
                cagnote = cagnote + (retrait/billet)*billet ;
              } else {
                while ((selection != "o")&&(boucle==true)) {
                  if (selection == "o") {
                    if (retrait/billet != 0) {
                      cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " CHF" + '\n' ;
                    }
                    cagnote_int = (retrait/billet)*billet ;
                    cagnote = cagnote + (retrait/billet)*billet ;
                    boucle = false ;
                  }
                  if (boucle == true) {
                    if ((selection.toInt > retrait/billet)||(selection.toInt == retrait/billet)) {
                      selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ;
                    } else {
                      if ((selection.toInt < retrait/billet)&&(selection.toInt != 0)) {
                        if (retrait/billet != 0) {
                          cagnote_str = cagnote_str + selection.toInt + " billet(s) de " + billet + " CHF" + '\n' ;
                        }
                        cagnote_int = selection.toInt*billet ;
                        cagnote = cagnote + selection.toInt*billet ;
                      }
                      cagnote_int = selection.toInt*billet ;
                      cagnote = cagnote + selection.toInt*billet ;
                      boucle = false ;
                    }
                  }
                }
                boucle = true ;
              }
              billet = billet/2 ;
            } else if (billet % moduleur != 0) {
              billet = billet-5 ;
            } else if (retrait < billet) {
              billet = billet/2 ;
            }
            if (billet < 10) {
              println(" ") ;
              println("Veuillez retirer la somme demandée : " + '\n' + cagnote_str) ;
              cagnote_str = "" ;
              cagnote_int = 0 ;
            }
          }
        }
      }
      else { // else if (devise==2)
        //petites coupures en EUR
        var billet = 100 ;
        var moduleur = 0 ;
        while (billet>=10) {
          if (billet >= 100) {
            moduleur = 100 ;
          } else if (billet < 100) {
            moduleur = 10 ;
          }
          if ((billet%moduleur == 0)&&(retrait >= billet)) {
            retrait = retrait - cagnote_int ;
            if (retrait/billet != 0) {
              println("Il reste " + retrait + " à distribuer ") ;
              println("Vous pouvez obtenir au maximum " + retrait/billet + " billet(s) de " + billet) ;
              if (billet != 10) {
                selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ; 
              }
            }
            if ((selection == "o")||(billet == 10)) {
              if (retrait/billet != 0) {
                cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " EUR" + '\n' ;
              }
              cagnote_int = (retrait/billet)*billet ;
              cagnote = cagnote + (retrait/billet)*billet ;
            } else {
              while ((selection != "o")&&(boucle==true)) {
                if (selection == "o") {
                  if (retrait/billet != 0) {
                    cagnote_str = cagnote_str + retrait/billet + " billet(s) de " + billet + " EUR" + '\n' ;
                  }
                  cagnote_int = (retrait/billet)*billet ;
                  cagnote = cagnote + (retrait/billet)*billet ;
                  boucle = false ;
                }
                if (boucle == true) {
                  if ((selection.toInt > retrait/billet)||(selection.toInt == retrait/billet)) {
                    selection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ") ;
                  } else {
                    if ((selection.toInt < retrait/billet)&&(selection.toInt != 0)) {
                      if (retrait/billet != 0) {
                        cagnote_str = cagnote_str + selection.toInt + " billet(s) de " + billet + " EUR" + '\n' ;
                      }
                      cagnote_int = selection.toInt*billet ;
                      cagnote = cagnote + selection.toInt*billet ;
                      pas_passer = true ;
                    }
                    if (pas_passer == false) {
                      cagnote_int = selection.toInt*billet ;
                      cagnote = cagnote + selection.toInt*billet ;
                    }
                    pas_passer = false ;
                    boucle = false ;
                  }
                }
              }
              boucle = true ;
            }
            billet = billet/2 ;
          } else if (billet%moduleur != 0) {
            billet = billet - 5 ;
          } else if (retrait < billet) {
            billet = billet/2 ;
          }
          if (billet < 10) {
            println(" ") ;
            println("Veuillez retirer la somme demandée : " + '\n' + cagnote_str) ;
            cagnote_str = "" ;
            cagnote_int = 0 ;
          }
        }
      }
      if (devise == 1) {
        comptes(id) = comptes(id) - cagnote ;
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", comptes(id)) ;
        println ('\n') ;
        devise = 0 ;
      }
      if (devise == 2) {
        comptes(id) = comptes(id) - cagnote*0.95 ;
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f ", comptes(id)) ;
        println ('\n') ;
        devise = 0 ;
      }
    }
    cagnote = 0.0 ;
  }
  
  def changepin(id:Int,codespin:Array[String]): Unit = {
    codespin(id) = readLine("Saississez votre nouveau code pin (il doit contenir au moins 8 caractères) > ") ;
    while (codespin(id).length < 8) {
      codespin(id) = readLine("Votre code pin ne contient pas au moins 8 caractères ") ;
    }
  }
  
  def main(args: Array[String]): Unit = {

    //identification du client

    while (identification == true) {
      boucle_princ = boucle_princ + 1 ;
      id = readLine("Saississez votre identifiant > ").toInt ;
      if (id >= comptes.length) {
        println("Cet identifiant n'est pas valable ") ;
        operations = false ;
        identification = false ;
      } 

      if (identification == true) {
        if (compteur_pin == false) {
          saisie = readLine("Saisissez votre code pin > ") ;
        } else {
          compteur_pin = true ;
        }

        if (identification_rate == true) {
          tentative = 3 ;
          identification_rate = false ;
          operations = true ;
        }
        while ((saisie != codespin(id))&&(tentative != 1)) {
          tentative = tentative-1 ;
          saisie = readLine("Code pin erroné, il vous reste " + tentative + " tentatives > ") ;
          if (tentative == 1) {
            if (saisie != codespin(id)) {
              println("Trop d'erreurs, abandon de l'identification ") ;
              operations = false ;
              identification = true ;
              identification_rate = true ;
            }
          }
        }
        if (identification_rate == false) {
          identification = false ;
        }
      } else if ((identification == false)&&(id<comptes.length)) {
        identification = true ;
      }

      if ((identification == false)&&(boucle_princ != 1)&&(id<comptes.length)) {
        if (boucle_princ % 2 == 0) {
          operations = true ;
        } else if (boucle_princ % 2 != 0) {
          operations = true ;
        }
      }

      if (operations == true) {
        tentative = 3 ;
      }
      
      while (operations==true) {

        //initialisation de l'interface

        var interface = readLine("Choisissez votre opération :" + '\n' + '\t' + "1) Dépôt" + '\n' + '\t' + "2) Retrait" + '\n' + '\t' + "3) Consultation du compte" + '\n' + '\t' + "4) Changement du code pin" + '\n' + '\t' + "5) Terminer" + '\n' + "Votre choix : " ).toInt ;

        if (interface == 1) {

          //opération de dépôt
          depot(id,comptes) ;
        }

        if (interface == 2) {

          //opération de retrait
          retrait(id,comptes) ;
        }

        if (interface == 3) {

          //opération de consultation

          compteur_pin = true ;
          printf("Le montant disponible sur votre compte est de : %.2f ", comptes(id)) ;
          println('\n') ;
          
        }

        if (interface == 4) {
          
          //Changement du code pin
          changepin(id,codespin) ;
        }

        if (interface == 5) {
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.") ;
          operations = false ;
          identification = true ;
          compteur_pin = false ;
        }
      }
    }
  }
}