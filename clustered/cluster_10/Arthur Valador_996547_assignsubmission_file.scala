//Assignment: Arthur Valador_996547_assignsubmission_file

object Main {
import scala.io.StdIn._

  //def depot
  def depot(id : Int, comptes : Array[Double]) : Unit = {
  var depot = 0
  val conversionchfeneuro = 1.05
  val conversioneuroenchf = 0.95
  var choixdevisedepot = 0
    choixdevisedepot = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
  // Dépôt en CHF
  if (choixdevisedepot == 1) {

  while ((!(depot % 10 == 0)) || (!(depot >= 10))) {
  depot = readLine("Indiquez le montant du dépôt >").toInt
  if ((!(depot % 10 == 0)) || (!(depot >= 10)))        
  println("Le montant doit être un multiple de 10")
  else if ((depot % 10 == 0) && (depot >= 10)) {
  comptes(id) += depot
  printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }//else if 43
    }//while 40


    }//choix devise CHF / 38
    //Dépôt EUR
    else if (choixdevisedepot == 2) {

    while ((!(depot % 10 == 0)) || (!(depot >= 10))) {
    depot = readLine("Indiquez le montant du dépôt >").toInt
    if ((!(depot % 10 == 0)) || (!(depot >= 10))) println("Le montant doit être un multiple de 10")
    else if ((depot % 10 == 0) && (depot >= 10)) {
    comptes(id) += depot * conversioneuroenchf
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
   }//else if /57
   }//while /54
   }// else if choix devise eur /53

    depot = 0
}//def depôt 

  //def retrait
  def retraitt(id : Int, comptes : Array[Double]) : Unit = {
    var choixdeviseretrait = 0
    var retrait = 0
    val conversionchfeneuro = 1.05
    val conversioneuroenchf = 0.95
    var retraitinitial = 0
    var choixcoupures = 0
    var choixbillets = ""
    var billets10 = 0
    var billets20 = 0
    var billets50 = 0
    var billets100 = 0 
    var billets200 = 0 
    var billets500 = 0
    while ((!(choixdeviseretrait == 1)) && (!(choixdeviseretrait == 2))) {
      choixdeviseretrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
      }//while /
      //Retrait en CHF
      if (choixdeviseretrait == 1) {

      while ((!(retrait % 10 == 0)) || (!(retrait >= 10)) || (!(retrait <= comptes(id) / 10))) {
      retrait = readLine("Indiquez le montant du retrait >").toInt
      if ((!(retrait % 10 == 0)) || (!(retrait >= 10))) println("Le montant doit être un multiple de 10")
      if (!(retrait <= comptes(id) / 10)) println("Votre plafond de retrait autorisé est de : " + comptes(id) / 10)
      }//while 

      retraitinitial = retrait

      //choix des coupures possible ?
      if (retrait >= 200) {  
      //choix coupures
      while ((!(choixcoupures == 1)) && (!(choixcoupures == 2))) {
      choixcoupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
      }//while choix coupures
      }//if retrait >= 200
      //Grosses coupures
      if (choixcoupures == 1) { 

      if (retrait >= 500) {
      billets500 = retrait / 500
      while (choixbillets != "o")  {   
      choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets500 + " billet(s) de 500 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
      if (choixbillets == "o") {
      retrait -= billets500 * 500 }
      if (choixbillets != "o") {
      if ((choixbillets).toInt >= billets500) { 
      println("Répétez la saisie") }
      if ((choixbillets).toInt < billets500) {
      billets500 = (choixbillets).toInt
      choixbillets = "o"
      if (billets500 > 0) { 
      retrait -= billets500 * 500 }
      }//if pour accorder valeur choixbillets à billets500
      }//if choix billets != o
      }//while choixbillets différent de o
      }//if retrait >= 500
      choixbillets = ""

      if (retrait >= 200) {
        billets200 = retrait / 200
        while (choixbillets != "o")  {   
        choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets200 + " billet(s) de 200 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if (choixbillets == "o") {
        retrait -= billets200 * 200 }
        if (choixbillets != "o") {
        if ((choixbillets).toInt >= billets200) { 
        println("Répétez la saisie") }
        if ((choixbillets).toInt < billets200) {
        billets200 = (choixbillets).toInt
        choixbillets = "o"
        if (billets200 > 0) { 
        retrait -= billets200 * 200 }
        }//if pour accorder valeur choixbillets à billets200
        }//if choix billets != o
        }//while choixbillets différent de o
        }//if retrait >= 200
        choixbillets = ""

        if (retrait >= 100) {
        billets100 = retrait / 100
        while (choixbillets != "o")  {   
        choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if (choixbillets == "o") {
        retrait -= billets100 * 100 }
        if (choixbillets != "o") {
        if ((choixbillets).toInt >= billets100) { 
        println("Répétez la saisie") }
        if ((choixbillets).toInt < billets100) {
        billets100 = (choixbillets).toInt
        choixbillets = "o"
        if (billets100 > 0) { 
        retrait -= billets100 * 100 }
        }//if pour accorder valeur choixbillets à billets200
        }//if choix billets != o
        }//while choixbillets différent de o
        }//if retrait >= 100
        choixbillets = ""

        if (retrait >= 50) {
        billets50 = retrait / 50
        while (choixbillets != "o")  {   
        choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if (choixbillets == "o") {
        retrait -= billets50 * 50 }
        if (choixbillets != "o") {
        if ((choixbillets).toInt >= billets50) { 
        println("Répétez la saisie") }
        if ((choixbillets).toInt < billets50) {
        billets50 = (choixbillets).toInt
        choixbillets = "o"
        if (billets50 > 0) { 
        retrait -= billets50 * 50 }
        }//if pour accorder valeur choixbillets à billets200
        }//if choix billets != o
        }//while choixbillets différent de o
        }//if retrait >= 50
        choixbillets = ""

        if (retrait >= 20) {
        billets20 = retrait / 20
        while (choixbillets != "o")  {   
        choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        if (choixbillets == "o") {
        retrait -= billets20 * 20 }
        if (choixbillets != "o") {
        if ((choixbillets).toInt >= billets20) { 
        println("Répétez la saisie") }
        if ((choixbillets).toInt < billets20) {
        billets20 = (choixbillets).toInt
        choixbillets = "o"
        if (billets20 > 0) { 
        retrait -= billets20 * 20 }
        }//if pour accorder valeur choixbillets à billets200
        }//if choix billets != o
        }//while choixbillets différent de o
        }//if retrait >= 20
        choixbillets = ""

        if (retrait >= 10) {
        billets10 = retrait / 10  
        println("Il reste " + retrait + " CHF à distribuer \n Vous  obtiendrez " + billets10 + " billet(s) de 10 CHF")
        retrait -= billets10 * 10 
        }//if retrait >= 10
        choixbillets = ""

      }//if choixcoupures == 1 
        //petites coupures
        else {

          if (retrait >= 100) {
            billets100 = retrait / 100
            while (choixbillets != "o")  {   
            choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (choixbillets == "o") {
            retrait -= billets100 * 100 }
            if (choixbillets != "o") {
            if ((choixbillets).toInt >= billets100) { 
            println("Répétez la saisie") }
            if ((choixbillets).toInt < billets100) {
            billets100 = (choixbillets).toInt
            choixbillets = "o"
            if (billets100 > 0) { 
            retrait -= billets100 * 100 }
            }//if pour accorder valeur choixbillets à billets200
            }//if choix billets != o
            }//while choixbillets différent de o
            }//if retrait >= 100
            choixbillets = ""

            if (retrait >= 50) {
            billets50 = retrait / 50
            while (choixbillets != "o")  {   
            choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (choixbillets == "o") {
            retrait -= billets50 * 50 }
            if (choixbillets != "o") {
            if ((choixbillets).toInt >= billets50) { 
            println("Répétez la saisie") }
            if ((choixbillets).toInt < billets50) {
            billets50 = (choixbillets).toInt
            choixbillets = "o"
            if (billets50 > 0) { 
            retrait -= billets50 * 50 }
            }//if pour accorder valeur choixbillets à billets200
            }//if choix billets != o
            }//while choixbillets différent de o
            }//if retrait >= 50
            choixbillets = ""

            if (retrait >= 20) {
            billets20 = retrait / 20
            while (choixbillets != "o")  {   
            choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 CHF \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (choixbillets == "o") {
            retrait -= billets20 * 20 }
            if (choixbillets != "o") {
            if ((choixbillets).toInt >= billets20) { 
            println("Répétez la saisie") }
            if ((choixbillets).toInt < billets20) {
            billets20 = (choixbillets).toInt
            choixbillets = "o"
            if (billets20 > 0) { 
            retrait -= billets20 * 20 }
            }//if pour accorder valeur choixbillets à billets200
            }//if choix billets != o
            }//while choixbillets différent de o
            }//if retrait >= 20
            choixbillets = ""

            if (retrait >= 10) {
            billets10 = retrait / 10  
            println("Il reste " + retrait + " CHF à distribuer \n Vous  obtiendrez " + billets10 + " billet(s) de 10 CHF")
            retrait -= billets10 * 10 
            }//if retrait >= 10
            choixbillets = ""

      }//else petites coupures

        println("Veuillez retirer la somme demandée :")
        if (billets500 > 0) { println(billets500 + " billet(s) de 500 CHF") }
        if (billets200 > 0) { println(billets200 + " billet(s) de 200 CHF") }
        if (billets100 > 0) { println(billets100 + " billet(s) de 100 CHF") }
        if (billets50 > 0) { println(billets50 + " billet(s) de 50 CHF") }
        if (billets20 > 0) { println(billets20 + " billet(s) de 20 CHF") }
        if (billets10 > 0) { println(billets10 + " billet(s) de 10 CHF") }



        comptes(id) -= retraitinitial  
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

      }//if retrait chf

        //retrait en euros
        if (choixdeviseretrait == 2) {

        while ((!(retrait % 10 == 0)) || (!(retrait >= 10)) || (!(retrait <= comptes(id) / 10))) {
        retrait = readLine("Indiquez le montant du retrait >").toInt
        if ((!(retrait % 10 == 0)) || (!(retrait >= 10))) println("Le montant doit être un multiple de 10")
        if (!(retrait <= comptes(id) / 10)) println("Votre plafond de retrait autorisé est de : " + comptes(id) / 10)
        }//while 

        retraitinitial = retrait

          if (retrait >= 100) {
            billets100 = retrait / 100
            while (choixbillets != "o")  {   
            choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets100 + " billet(s) de 100 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (choixbillets == "o") {
            retrait -= billets100 * 100 }
            if (choixbillets != "o") {
            if ((choixbillets).toInt >= billets100) { 
            println("Répétez la saisie") }
            if ((choixbillets).toInt < billets100) {
            billets100 = (choixbillets).toInt
            choixbillets = "o"
            if (billets100 > 0) { 
            retrait -= billets100 * 100 }
            }//if pour accorder valeur choixbillets à billets200
            }//if choix billets != o
            }//while choixbillets différent de o
            }//if retrait >= 100
            choixbillets = ""

            if (retrait >= 50) {
            billets50 = retrait / 50
            while (choixbillets != "o")  {   
            choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets50 + " billet(s) de 50 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (choixbillets == "o") {
            retrait -= billets50 * 50 }
            if (choixbillets != "o") {
            if ((choixbillets).toInt >= billets50) { 
            println("Répétez la saisie") }
            if ((choixbillets).toInt < billets50) {
            billets50 = (choixbillets).toInt
            choixbillets = "o"
            if (billets50 > 0) { 
            retrait -= billets50 * 50 }
            }//if pour accorder valeur choixbillets à billets200
            }//if choix billets != o
            }//while choixbillets différent de o
            }//if retrait >= 50
            choixbillets = ""

            if (retrait >= 20) {
            billets20 = retrait / 20
            while (choixbillets != "o")  {   
            choixbillets = readLine("Il reste " + retrait + " CHF à distribuer \n Vous pouvez obtenir au maximum " + billets20 + " billet(s) de 20 EUR \n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            if (choixbillets == "o") {
            retrait -= billets20 * 20 }
            if (choixbillets != "o") {
            if ((choixbillets).toInt >= billets20) { 
            println("Répétez la saisie") }
            if ((choixbillets).toInt < billets20) {
            billets20 = (choixbillets).toInt
            choixbillets = "o"
            if (billets20 > 0) { 
            retrait -= billets20 * 20 }
            }//if pour accorder valeur choixbillets à billets200
            }//if choix billets != o
            }//while choixbillets différent de o
            }//if retrait >= 20
            choixbillets = ""

            if (retrait >= 10) {
            billets10 = retrait / 10  
            println("Il reste " + retrait + " CHF à distribuer \n Vous  obtiendrez " + billets10 + " billet(s) de 10 EUR")
            retrait -= billets10 * 10 
            }//if retrait >= 10
            choixbillets = ""

          println("Veuillez retirer la somme demandée :")
          if (billets100 > 0) { println(billets100 + " billet(s) de 100 EUR") }
          if (billets50 > 0) { println(billets50 + " billet(s) de 50 EUR") }
          if (billets20 > 0) { println(billets20 + " billet(s) de 20 EUR") }
          if (billets10 > 0) { println(billets10 + " billet(s) de 10 EUR") }



          comptes(id) -= retraitinitial * conversioneuroenchf
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

        }//if choixdeviseretrait == 2

      choixdeviseretrait = 0
      choixcoupures = 0
      billets500 = 0
      billets200 = 0
      billets100 = 0
      billets50 = 0
      billets20 = 0
      billets10 = 0
  }//def retrait

  //def changement de pin (fonctionne)
  def changepin(id: Int, codespin: Array[String]): Unit = {
    var nouveaupin = ""
    while (nouveaupin.length < 8) {
      nouveaupin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString
      if (nouveaupin.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères")
      } else {
      codespin(id) = nouveaupin
      }
       }//while changement de pin
  }//def changepin
  
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn._
    //Comptes
    var idnombre = 0
    var id = 0
    var comptes = Array.fill(100)(1200.0)
    var nbclients = 100
    //variables choix
    var choix = 0
    //variables retrait
    var choixdeviseretrait = 0
    var retrait = 0
    var retraitinitial = 0
    var choixcoupures = 0
    var choixbillets = ""
    var billets10 = 0
    var billets20 = 0
    var billets50 = 0
    var billets100 = 0 
    var billets200 = 0 
    var billets500 = 0
    // variables code pin
    var nbtentativespin = 3
    var codespin = Array.fill(100)("INTRO1234")
    var pinsaisi = "" 

    while (idnombre < nbclients) {
    // Demande d'identifiant
    while ((!(codespin(id) == pinsaisi)) && (idnombre < nbclients)) {
    idnombre = readLine("Saisissez votre identifiant >").toInt
    if (idnombre > nbclients) { 
    println("Cet identifiant n’est pas valable.") 
    } else {
    id = idnombre
    // Demande du code pin
    while ((!(codespin(id) == pinsaisi)) && (!(nbtentativespin == 0))) {
    pinsaisi = readLine("Saisissez votre code pin >").toString
    if (!(codespin(id) == pinsaisi)) {
    nbtentativespin -= 1
    println("Code pin erroné, il vous reste " + nbtentativespin + " tentatives >")
    }//if
    }//while code pin
    if (nbtentativespin == 0) {  
    println("Trop d'erreurs, abandon de l'identification")
    }//if carte bloquée
    }//else
    nbtentativespin = 3
    }//while demande d'identifiant et pin
    if (idnombre < nbclients) {
    //réintialisation des variables
    idnombre = 0
    pinsaisi = ""
    
    while (!(choix == 5)) {
    
    choix = readLine("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix : ").toInt

    //Dépôt 
    if (choix == 1) {
    depot(id, comptes)
    }//if choix == 1

    //Retrait
    else if (choix == 2) {
    retraitt(id, comptes)
    }//if choix == 2

   //Consultation du compte
   else if (choix == 3) {
   printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
     }//if choix == 3

    //Changement de pin
    else if (choix == 4) {
    changepin(id, codespin)
    }//if choix == 4
      
    }//while choix != 5

     if (choix == 5) { println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
     }//if choix == 5
     //réintialisation de la var choix
     choix = 0
     }//if idnombre < nbclients 
     }//while choix != -10  
    
      
    }//Unit
}//Main
