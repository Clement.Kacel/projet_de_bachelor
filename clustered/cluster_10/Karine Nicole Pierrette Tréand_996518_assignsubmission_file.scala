//Assignment: Karine Nicole Pierrette Tréand_996518_assignsubmission_file

import scala.io.StdIn._
object Main {
 def main(args: Array[String]): Unit = {

   var actionChoice = 0
   var firstAction = true

   var id = -1



   val nbclients = 100




   var comptes = Array.fill(nbclients)(1200.0)

   var codespin = Array.fill(nbclients)("INTRO1234")





   //choix d'action
   while (actionChoice==0) {

     if (firstAction) {
       id = Selogin(codespin)
       firstAction = false
     }




     println("Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changer pin \n 5) Terminer")
     actionChoice=readLine("Votre choix : ").toInt


     //Dépot
     if (actionChoice==1) {
       depot(id,comptes)
     } //depot


     //Retrait
     else if (actionChoice==2) {

       retrait(id,comptes)
     }

     //Consultation du compte
       else if (actionChoice==3) {
       printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
       actionChoice=0

     } 

     //terminer
     else if (actionChoice==4) {

       changepin(id,codespin)
       actionChoice=0


     }


     //terminer
     else if (actionChoice==5) {
     println("Fin des opérations, n'oubliez pas de récupérer votre carte")
       firstAction = true
       actionChoice=0
     }








def retrait(id: Int, comptes: Array[Double]): Unit = {

       var withdrawalCurrency = 0
       var withdrawalAmmount = 1
       val withdrawalLimit = comptes(id) / 10
       var withdrawalSize = 0
       var withdrawalRest = 0
       var nb500 = 0
       var nb200 = 0
       var nb100 = 0
       var nb50 = 0
       var nb20 = 0
       var nb10 = 0
       var done = false

       //choix de la devise
       while (withdrawalCurrency != 1 && withdrawalCurrency != 2) {
         withdrawalCurrency = readLine("Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
       }

       //choix du montant du retrait
       while ((withdrawalAmmount % 10 != 0 || withdrawalAmmount > withdrawalLimit) || withdrawalAmmount<0) {
         withdrawalAmmount = readLine("Indiquez le montant du retrait > ").toInt
         if (withdrawalAmmount % 10 != 0 || withdrawalAmmount<0) print("Le montant doit être un multiple de 10. ")
         if (withdrawalAmmount > withdrawalLimit) println("Votre plafond de retrait est de : " + withdrawalLimit)
       }
       withdrawalRest = withdrawalAmmount

       //retrait en CHF
       if (withdrawalCurrency == 1) {
         if (withdrawalAmmount >= 200) {
           while (withdrawalSize != 1 && withdrawalSize != 2) {
             withdrawalSize = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
           }
         } else withdrawalSize = 2

           //500 CHF
           while (!done && withdrawalAmmount / 500 != 0 && withdrawalSize==1) {
             println("il reste " + withdrawalAmmount + " CHF à distribuer")
             println("Vous pouvez obtenir au maximum " + (withdrawalAmmount / 500) + " billet(s) de 500 CHF")
             println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
             val input = readLine()
             if (input == "o") {
               nb500 = withdrawalAmmount / 500
               done = true
             } else if (input.toInt < (withdrawalAmmount / 500)) {
               nb500 = input.toInt
               done = true
             }
           }
           withdrawalRest = withdrawalAmmount - nb500 * 500
           if (withdrawalRest != 0) done = false

           //200 CHF
           while (!done && withdrawalRest / 200 != 0 && withdrawalSize==1) {
             println("il reste " + withdrawalRest + " CHF à distribuer")
             println("Vous pouvez obtenir au maximum " + (withdrawalRest / 200) + " billet(s) de 200 CHF")
             println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
             val nbInput = readLine()
             if (nbInput == "o") {
               nb200 = withdrawalRest / 200
               done = true
             } else if (nbInput.toInt < (withdrawalRest / 200)) {
               nb200 = nbInput.toInt
               done = true
             }
           }
           withdrawalRest -= nb200 * 200
           if (withdrawalRest != 0) done = false

           //100 CHF
           while (!done && withdrawalRest / 100 != 0) {
             println("il reste " + withdrawalRest + " CHF à distribuer")
             println("Vous pouvez obtenir au maximum " + (withdrawalRest / 100) + " billet(s) de 100 CHF")
             println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
             val input = readLine()
             if (input == "o") {
               nb100 = withdrawalRest / 100
               done = true
             } else if (input.toInt < (withdrawalRest / 100)) {
               nb100 = input.toInt
               done = true
             }
           }
           withdrawalRest -= nb100 * 100
           if (withdrawalRest != 0) done = false

           //50 CHF
           while (!done && withdrawalRest / 50 != 0) {
             println("il reste " + withdrawalRest + " CHF à distribuer")
             println("Vous pouvez obtenir au maximum " + (withdrawalRest / 50) + " billet(s) de 50 CHF")
             println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
             val input = readLine()
             if (input == "o") {
               nb50 = withdrawalRest / 50
               done = true
             } else if (input.toInt < (withdrawalRest / 50)) {
               nb50 = input.toInt
               done = true
             }
           }
           withdrawalRest -= nb50 * 50
           if (!(withdrawalRest == 0)) done = false

           //20 CHF
           while (!done && withdrawalRest / 20 != 0) {
             println("il reste " + withdrawalRest + " CHF à distribuer")
             println("Vous pouvez obtenir au maximum " + (withdrawalRest / 20) + " billet(s) de 20 CHF")
             println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
             val input = readLine()
             if (input == "o") {
               nb20 = withdrawalRest / 20
               done = true
             } else if (input.toInt < (withdrawalRest / 20)) {
               nb20 = input.toInt
               done = true
             }
           }
           withdrawalRest -= nb20 * 20
           if (withdrawalRest != 0) done = false

           //10 CHF
           while (!done && withdrawalRest / 10 != 0) {
             println("il reste " + withdrawalRest + " CHF à distribuer")
             println("Vous pouvez obtenir " + (withdrawalRest / 10) + " billet(s) de 10 CHF")
             println("Tapez o pour ok > ")
             val input = readLine()
             if (input == "o") {
               nb10 = withdrawalRest / 10
               done = true
             }
           }

         //message de fin CHF
         println("Veuillez retirer la sommme demandée :")
         if (!(nb500 == 0)) println(nb500 + " billet(s) de 500 CHF")
         if (!(nb200 == 0)) println(nb200 + " billet(s) de 200 CHF")
         if (!(nb100 == 0)) println(nb100 + " billet(s) de 100 CHF")
         if (!(nb50 == 0)) println(nb50 + " billet(s) de 50 CHF")
         if (!(nb20 == 0)) println(nb20 + " billet(s) de 20 CHF")
         if (!(nb10 == 0)) println(nb10 + " billet(s) de 10 CHF")
         comptes(id) -= withdrawalAmmount
         printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))



         //retrait en EUR
       } else if (withdrawalCurrency == 2) {

         //100EUR
         while (!done && withdrawalRest / 100 != 0) {
           println("il reste " + withdrawalRest + " EUR à distribuer")
           println("Vous pouvez obtenir au maximum " + (withdrawalRest / 100) + " billet(s) de 100 EUR")
           println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
           val input = readLine()
           if (input == "o") {
             nb100 = withdrawalRest / 100
             done = true
           } else if (input.toInt < withdrawalRest / 100) {
             nb100 = input.toInt
             done = true
           }
         }
         withdrawalRest -= nb100 * 100
         if (withdrawalRest != 0) done = false

         //50EUR
         while (!done && withdrawalRest / 50 != 0) {
           println("il reste " + withdrawalRest + " EUR à distribuer")
           println("Vous pouvez obtenir au maximum " + (withdrawalRest / 50) + " billet(s) de 50 EUR")
           println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
           val input = readLine()
           if (input == "o") {
             nb50 = withdrawalRest / 50
             done = true
           } else if (input.toInt < (withdrawalRest / 50)) {
             nb50 = input.toInt
             done = true
           }
         }
         withdrawalRest -= nb50 * 50
         if (withdrawalRest != 0) done = false
         var retrait =

         //20EUR
         while (!done && withdrawalRest / 20 != 0) {
           println("il reste " + withdrawalRest + " EUR à distribuer")
           println("Vous pouvez obtenir au maximum " + (withdrawalRest / 20) + " billet(s) de 20 EUR")
           println("Tapez o pour ok ou une autre valeure inférieure à celle proposée > ")
           val input = readLine()
           if (input == "o") {
             nb20 = withdrawalRest / 20
             done = true
           } else if (input.toInt < (withdrawalRest / 20)) {
             nb20 = input.toInt
             done = true
           }
         }
         withdrawalRest -= nb20 * 20
         if (withdrawalRest != 0) done = false

         //10EUR
         while (!done && withdrawalRest / 10 != 0) {
           println("il reste " + withdrawalRest + " EUR à distribuer")
           println("Vous pouvez obtenir " + (withdrawalRest / 10) + " billet(s) de 10 EUR")
           println("Tapez o pour ok")
           val input = readLine()
           if (input == "o") {
             nb10 = withdrawalRest / 10
             done = true
           }
         }


       //message de fin EUR
       println("Veuillez retirer la sommme demandée :")
       if (!(nb500 == 0)) println(nb500 + " billet(s) de 500 EUR")
       if (!(nb200 == 0)) println(nb200 + " billet(s) de 200 EUR")
       if (!(nb100 == 0)) println(nb100 + " billet(s) de 100 EUR")
       if (!(nb50 == 0)) println(nb50 + " billet(s) de 50 EUR")
       if (!(nb20 == 0)) println(nb20 + " billet(s) de 20 EUR")
       if (!(nb10 == 0)) println(nb10 + " billet(s) de 10 EUR")

       comptes(id) -= (withdrawalAmmount * 0.95)
       printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
       }
       actionChoice=0



}







   }//While main





   def Selogin(codespin:Array[String]):Int = {
     var id = readLine("Entrez votre ID ?: ").toInt

     var pinJuste = codespin(id)

     if(id > nbclients){
       print("wrong login....")
       System.exit(0)
     }
     else{

       if(!demanderPin(pinJuste)){
         Selogin(codespin)
       }

     }

     return id

   }//Login



   //PIN pour première action
   def demanderPin(pinJuste:String):Boolean = {

     var pinInput = ""
       var tryNb = 2

       while (pinInput!=pinJuste){
         pinInput = readLine("Saisissez votre code PIN > ")
         if ((pinInput!=pinJuste) && (tryNb>0)) {
           tryNb -= 1
           println("Code pin erroné, il vous reste " + (tryNb+1) + " tentatives > ")
         } 

         else if (pinInput != pinJuste && tryNb==0) {
           println("Pour votre protection, les opérations bancaires vont s'interrompre, récupérez votre carte.")
           return false
         }
         else if (pinInput == pinJuste){
           return true
         } 
       }
     return true

   }//PIN premiere 





   def depot(id: Int,comptes:Array[Double]){


       var depositCurrency = 0
       var depositAmmount = 1

       //devise dépot
       while (depositCurrency != 1 && depositCurrency != 2) {
         depositCurrency = readLine("Indiquez la devise du dépot : 1) CHF ; 2) EUR > ").toInt
       }

       //choix montant dépot
       while (depositAmmount % 10 != 0 || depositAmmount<0){
         depositAmmount = readLine("Indiquez le montant du dépôt > ").toInt
         if (depositAmmount%10!=0 || depositAmmount<0) print("Le montant doit être un multiple de 10. ")
       }

       //dépot en CHF
       if (depositCurrency == 1) {
         comptes(id) += depositAmmount
         printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
       }

         //dépot en EUR
       else if (depositCurrency == 2) {
         comptes(id) += (depositAmmount * 0.95)
         printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
       }
       actionChoice=0


   }//Depot



   def changepin(id:Int,codespin:Array[String]): Unit = {

     var newPin = ""
     while(newPin.length < 8){

       newPin = readLine("Entrez votre nouveau pin (8 characters minimum): ")
       if (newPin.length < 8){
         println("Le pin doit comporter au moins 8 caractères")
       }
       else{
         println("Le nouveau pin est : " + newPin)
         codespin(id) = newPin
       }

     }


   }//Changepin




 }
}1