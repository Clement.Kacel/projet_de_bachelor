//Assignment: Julien Laurent A Art_996548_assignsubmission_file

import io.StdIn._
import math._ 


object Main {

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    
    println("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères)>")
    var nouveaupin = readLine()
    while (nouveaupin.length < 8){
      println("\nVotre code pin ne contient pas au moins 8 caractères")
      nouveaupin = readLine() }
    codespin(id) = nouveaupin  
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {

    var depot = 0
    var choixdeviseD = "d"

    println("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    choixdeviseD = readLine()

      if (choixdeviseD == "1") {
        println("\nIndiquez le montant du dépôt >")
        depot = readInt()
          while (depot%10 != 0) {
            println("\nLe montant doit être un multiple de 10")
            depot = readInt() }
        comptes(id)= comptes(id) + depot
        printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id)) 
      }

      if (choixdeviseD == "2") {
        println("\nIndiquez le montant du dépôt >")
        depot = readInt()
          while (depot%10 != 0) {
            println("\nLe montant doit être un multiple de 10")
            depot = readInt() }
        comptes(id)= comptes(id) + depot*0.95
        printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id))
      }
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {

    var choixdeviseR = "r"
    var retrait = 0
    var retraitautorise = 0.0
    var choixcoupure = "g"
    var reste = 0
    var billetde500 = 0
    var billetde200 = 0
    var billetde100 = 0
    var billetde50 = 0
    var billetde20 = 0
    var billetde10 = 0
    var nbcoupure = 0

    println("\nIndiquez la devise du retrait : 1 CHF, 2 EUR >")
    choixdeviseR = readLine()

    while (choixdeviseR != "1" && choixdeviseR != "2") {
      println("\nIndiquez la devise du retrait : 1 CHF, 2 EUR >")
      choixdeviseR = readLine() }

    if (choixdeviseR == "1") {
      println("\nIndiquez le montant du retrait >")
      retrait = readInt()
      while (retrait%10 != 0) {
        println("\nLe montant doit être un multiple de 10")
        retrait = readInt() }
      retraitautorise = comptes(id)*0.1
      while (retrait > retraitautorise) {
        println("\nVotre plafond de retrait autorisé est de : "+retraitautorise)
        retrait = readInt()}

      if (retrait >= 200) {
          while (choixcoupure != "1" && choixcoupure != "2") {
          println("\nEn 1) grosses coupures, 2) petites coupures >")
          choixcoupure = readLine() } 

          var confirmation ="k"
          reste = retrait
          billetde500 = 0
          billetde200 = 0
          billetde100 = 0
          billetde50 = 0
          billetde20 = 0
          billetde10 = 0
          nbcoupure = 0

          if (choixcoupure == "1") {

            if (reste >= 500) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde500 = reste/500
              println("\nVous pouvez obtenir au maximum "+billetde500+" billet(s) de 500 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde500 )) {
                println("\nVous pouvez obtenir au maximum "+billetde500+" billet(s) de 500 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde500*500) }
              else {        
                billetde500 = confirmation.toInt
                reste = reste - (billetde500*500) } }

            if (reste >= 200) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde200 = reste/200
              println("\nVous pouvez obtenir au maximum "+billetde200+" billet(s) de 200 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde200 )) {
                println("\nVous pouvez obtenir au maximum "+billetde200+" billet(s) de 200 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde200*200) }
              else {        
                billetde200 = confirmation.toInt
                reste = reste - (billetde200*200) } }

            if (reste >= 100) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde100 = reste/100
              println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde100 )) {
                println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde100*100) }
              else {        
                billetde100 = confirmation.toInt
                reste = reste - (billetde100*100) } }

            if (reste >= 50) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde50 = reste/50
              println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde50 )) {
                println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde50*50) }
              else {        
                billetde50 = confirmation.toInt
                reste = reste - (billetde50*50) } }

            if (reste >= 20) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde20 = reste/20
              println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde20 )) {
                println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde20*20) }
              else {        
                billetde20 = confirmation.toInt
                reste = reste - (billetde20*20) } }

            if (reste > 0) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde10 = reste/10
              println ("\nVous pouvez obtenir au maximum "+billetde10+" billet(s) de 10 CHF") }

          }

          if (choixcoupure == "2") {

            if (reste >= 100) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde100 = reste/100
              println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde100 )) {
                println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde100*100) }
              else {        
                  billetde100 = confirmation.toInt
                  reste = reste - (billetde100*100) } }

            if (reste >= 50) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde50 = reste/50
              println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde50 )) {
                println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde50*50) }
              else {        
                billetde50 = confirmation.toInt
                reste = reste - (billetde50*50) } }

            if (reste >= 20) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde20 = reste/20
              println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine()
              while ((confirmation != "o")&&(confirmation.toInt >= billetde20 )) {
                println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 CHF")
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                confirmation = readLine() }
              if (confirmation == "o" ) {
                reste = reste - (billetde20*20) }
              else {        
                billetde20 = confirmation.toInt
                reste = reste - (billetde20*20) } }

            if (reste > 0) {
              println("\nIl reste "+reste+ " CHF à distribuer")
              billetde10 = reste/10
              println ("\nVous pouvez obtenir au maximum "+billetde10+" billet(s) de 10 CHF") }

          }
      }

      if (retrait < 200) {

        var confirmation ="k"
        reste = retrait
        billetde500 = 0
        billetde200 = 0
        billetde100 = 0
        billetde50 = 0
        billetde20 = 0
        billetde10 = 0
        nbcoupure = 0

          if (reste >= 100) {
            println("\nIl reste "+reste+ " CHF à distribuer")
            billetde100 = reste/100
            println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            confirmation = readLine()
            while ((confirmation != "o")&&(confirmation.toInt >= billetde100 )) {
              println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine() }
            if (confirmation == "o" ) {
              reste = reste - (billetde100*100) }
            else {        
              billetde100 = confirmation.toInt
              reste = reste - (billetde100*100) } }

          if (reste >= 50) {
            println("\nIl reste "+reste+ " CHF à distribuer")
            billetde50 = reste/50
            println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            confirmation = readLine()
            while ((confirmation != "o")&&(confirmation.toInt >= billetde50 )) {
              println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine() }
            if (confirmation == "o" ) {
              reste = reste - (billetde50*50) }
            else {        
              billetde50 = confirmation.toInt
              reste = reste - (billetde50*50) } }

          if (reste >= 20) {
            println("\nIl reste "+reste+ " CHF à distribuer")
            billetde20 = reste/20
            println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 CHF")
            println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            confirmation = readLine()
            while ((confirmation != "o")&&(confirmation.toInt >= billetde20 )) {
              println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 CHF")
              println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              confirmation = readLine() }
            if (confirmation == "o" ) {
              reste = reste - (billetde20*20) }
            else {        
              billetde20 = confirmation.toInt
              reste = reste - (billetde20*20) } }

          if (reste > 0) {
            println("\nIl reste "+reste+ " CHF à distribuer")
            billetde10 = reste/10
            println ("\nVous pouvez obtenir au maximum "+billetde10+" billet(s) de 10 CHF") }


      } 

      println("\nVeuillez retirer la somme demandée :")
      if (billetde500 > 0) {
        println(billetde500+" billet(s) de 500 CHF") }
      if (billetde200 > 0) {
        println(billetde200+" billet(s) de 200 CHF") }
      if (billetde100 > 0) {
        println(billetde100+" billet(s) de 100 CHF") }
      if (billetde50 > 0) {
        println(billetde50+" billet(s) de 50 CHF") }
      if (billetde20 > 0) {
        println(billetde20+" billet(s) de 20 CHF") }
      if (billetde10 > 0) {
        println(billetde10+" billet(s) de 10 CHF") }

      comptes(id)= comptes(id) - retrait
      printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id))

    }

    if (choixdeviseR == "2") {

      println("\nIndiquez le montant du retrait >")
      retrait = readInt()
      while (retrait%10 != 0) {
        println("\nLe montant doit être un multiple de 10")
        retrait = readInt() }
      retraitautorise = comptes(id)*0.1
      while (retrait > retraitautorise) {
        println("\nVotre plafond de retrait autorisé est de : "+retraitautorise)
        retrait = readInt()}

      var confirmation ="k"
      reste = retrait
      billetde100 = 0
      billetde50 = 0
      billetde20 = 0
      billetde10 = 0
      nbcoupure = 0

      if (reste >= 100) {
        println("\nIl reste "+reste+ " EUR à distribuer")
        billetde100 = reste/100
        println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while ((confirmation != "o")&&(confirmation.toInt >= billetde100 )) {
          println("\nVous pouvez obtenir au maximum "+billetde100+" billet(s) de 100 EUR")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine() }
        if (confirmation == "o" ) {
          reste = reste - (billetde100*100) }
        else {        
          billetde100 = confirmation.toInt
          reste = reste - (billetde100*100) } }

      if (reste >= 50) {
        println("\nIl reste "+reste+ " EUR à distribuer")
        billetde50 = reste/50
        println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while ((confirmation != "o")&&(confirmation.toInt >= billetde50 )) {
          println("\nVous pouvez obtenir au maximum "+billetde50+" billet(s) de 50 EUR")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine() }
        if (confirmation == "o" ) {
          reste = reste - (billetde50*50) }
        else {        
          billetde50 = confirmation.toInt
          reste = reste - (billetde50*50) } }

      if (reste >= 20) {
        println("\nIl reste "+reste+ " EUR à distribuer")
        billetde20 = reste/20
        println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 EUR")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
        confirmation = readLine()
        while ((confirmation != "o")&&(confirmation.toInt >= billetde20 )) {
          println("\nVous pouvez obtenir au maximum "+billetde20+" billet(s) de 20 EUR")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          confirmation = readLine() }
        if (confirmation == "o" ) {
          reste = reste - (billetde20*20) }
        else {        
          billetde20 = confirmation.toInt
          reste = reste - (billetde20*20) } }

      if (reste > 0) {
        println("\nIl reste "+reste+ " EUR à distribuer")
        billetde10 = reste/10
        println ("\nVous pouvez obtenir au maximum "+billetde10+" billet(s) de 10 EUR") }

      println("\nVeuillez retirer la somme demandée :")
      if (billetde100 > 0) {
        println(billetde100+" billet(s) de 100 EUR") }
      if (billetde50 > 0) {
        println(billetde50+" billet(s) de 50 EUR") }
      if (billetde20 > 0) {
        println(billetde20+" billet(s) de 20 EUR") }
      if (billetde10 > 0) {
        println(billetde10+" billet(s) de 10 EUR") }

      comptes(id)= comptes(id) - retrait*0.95
      printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id) )

    }
  }

  def main(args: Array[String]): Unit = {

    var nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var id = 0
    var boucle = 0

    while(boucle == 0){

      var pin = "a"
      var tentative = 2

      println("\nSaisissez votre code identifiant>\n")
      id = readInt()
      if(id >= nbclients){
        println("\nCet identifiant n'est pas valable.\n")
        System.exit(0)
      }
      if(id < nbclients){
        println("\nSaisissez votre code pin>\n")
        pin = readLine()
        while (pin != codespin(id) && tentative > 0){
          printf("\nCode pin erroné, il vous reste "+tentative+" tentatives>\n")
          tentative = tentative-1
          pin = readLine()
        }
      }
      if (tentative == 0) {
        println("\nTrops d'erreurs, abandon de l'identification\n") 
      }
        var operation = 0     
        while (operation != 5 && tentative != 0){
          println("\nChoisissez votre opération:\n\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\n\nVotre choix:\n")
          operation = readInt()
          if (operation == 1){
            depot(id, comptes)
          } 
          else if (operation == 2){
            retrait(id, comptes)
          }
          else if (operation == 3){
            printf("\nLe montant disponible sur votre compte est de: %.2f\n",comptes(id))
          } 
          else if (operation == 4){
            changepin(id, codespin)
          }
        }
        if (operation == 5){
          println("\nFin des opérations, n’oubliez pas de récupérer votre carte.\n")
        }
    }  
  }  
}
