//Assignment: Isimbi Elsa Usanase_996459_assignsubmission_file

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.io.StdIn.readLine
import scala.util.{Try, Success, Failure}

object Main {

  def menu(): Int = {

    var choix_du_menu = 0

    println("Choisissez votre opération :")
    println("1) Dépôt")
    println("2) Retrait")
    println("3) Consultation du compte")
    println("4) Changement du code pin")
    println("5) Terminer")
    choix_du_menu = readLine("Votre choix: ").toInt




    return choix_du_menu
  }

  def depot(id : Int, comptes : Array[Double]) : Unit ={

    var choixMonnaieDepot  = readLine("Indiquez la devise du dépôt : 1) CHF 2) EUR >").toInt
    var montantDepose = readLine("Indiquez le montant du dépôt >").toDouble
    while(montantDepose%10!=0){
      montantDepose=readLine("Le montant doit être un multiple de 10. Indiquez le montant du dépôt >").toInt
    }
    if (choixMonnaieDepot==2){
      montantDepose=montantDepose*0.95
    }
    comptes(id) = comptes(id)+montantDepose
    println("Votre dépôt a été pris en compte,\n le nouveau montant disponible sur votre compte est de "+comptes(id) + "CHF / " +comptes(id)*1.05+" EUR ")

  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = {

    var choixMonnaieRetrait  = readLine("Indiquez la devise : 1) CHF 2) EUR >").toInt
    while((choixMonnaieRetrait!=1)&&(choixMonnaieRetrait!=2)){
      choixMonnaieRetrait  = readLine("Indiquez la devise : 1) CHF 2) EUR >").toInt
    }
    var montantRetire = readLine("Indiquez le montant du retrait >").toDouble
    while(montantRetire%10!=0){
      montantRetire=readLine("Le montant doit être un multiple de 10. Indiquez le montant du retrait >").toInt
    }
    while (montantRetire>(comptes(id)/10)){
      montantRetire= readLine("Votre plafond de retrait autorisé est de : " +(comptes(id)/10)+ " CHF,\n Indiquez le montant du retrait >").toDouble
    }

    var choixCoupure = 0

    if (montantRetire>200){
      if (choixMonnaieRetrait == 1) {
        choixCoupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
        while ((choixCoupure != 1) && (choixCoupure != 2)) {
          choixCoupure = readLine("Indique la forme du retrait.\n En 1) grosses coupures, 2) petites coupures >").toInt
        }
      }
    }


    if ((montantRetire<=200)&&(choixMonnaieRetrait==1)){
      var petiteCoupure = ArrayBuffer[Int](100,50,20,10)
      var argentAdistrib=montantRetire
      var compteur_billet= 0
      var saut_de_coupure = ""
      var i = 0
      var tableCompteur =ArrayBuffer[String]()
      var coupurePriseAvant10 = false



      while((i < petiteCoupure.size)&&argentAdistrib>0){

        if(argentAdistrib.toInt/petiteCoupure(i)>=1){
          println("")
          println("Il reste "+ argentAdistrib+ " CHF à distribuer")
          compteur_billet= argentAdistrib.toInt/petiteCoupure(i)
          println("Vous pouvez obtenir au maximum "+ compteur_billet + " billet(s) de "+petiteCoupure(i)+  " CHF")

          if (petiteCoupure(i) == 10 && (coupurePriseAvant10==false)) {
            println("Vous devez prendre la proposition faite par le programme pour cette coupure.")
            argentAdistrib -= (compteur_billet * petiteCoupure(i))
            val x = compteur_billet + " billet(s) de " + petiteCoupure(i) + " CHF"
            tableCompteur += x
            i += 1

          }else{
            saut_de_coupure = readLine("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée >")


            if (saut_de_coupure=="o"){
              argentAdistrib=argentAdistrib-(compteur_billet*petiteCoupure(i))
              var x = compteur_billet + " billet(s) de "+petiteCoupure(i)+  " CHF"
              coupurePriseAvant10 = true
              tableCompteur+=x
              i=i+1

            }
            else if (petiteCoupure(i) == 10 && (coupurePriseAvant10==false)) {
              println("Vous devez prendre la proposition faite par le programme pour cette coupure.")
              argentAdistrib -= (compteur_billet * petiteCoupure(i))
              val x = compteur_billet + " billet(s) de " + petiteCoupure(i) + " CHF"
              tableCompteur += x
              i += 1
            }
            else{
              Try(saut_de_coupure.toInt) match {
                case Success(sautInt) if sautInt < compteur_billet =>
                  argentAdistrib = argentAdistrib-(sautInt * petiteCoupure(i))
                  coupurePriseAvant10 = true
                  if (sautInt!=0){
                    var x = compteur_billet + " billet(s) de "+petiteCoupure(i)+  " CHF"
                    tableCompteur+=x
                  }
                  if (sautInt==0){
                    coupurePriseAvant10=false
                  }
                  i=i+1

                case _ =>
                  System.exit(0)
              }
            }
          }


        }else{
          i=i+1
        }

      }
      println("")
      println("Veuillez retirer la somme demandée: ")

      for(i<-tableCompteur){
        println(i)
      }

      comptes(id)=comptes(id)-montantRetire
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))



    }
    else if ((choixCoupure==1)&&(choixMonnaieRetrait==1)) {
      var petiteCoupure = ArrayBuffer[Int](500,200,100,50,20,10)
      var argentAdistrib=montantRetire
      var compteur_billet= 0
      var saut_de_coupure = ""
      var i = 0
      var tableCompteur =ArrayBuffer[String]()
      var coupurePriseAvant10 = false



      while((i < petiteCoupure.size)&&argentAdistrib>0){

        if(argentAdistrib.toInt/petiteCoupure(i)>=1){
          println("")
          println("Il reste "+ argentAdistrib+ " CHF à distribuer")
          compteur_billet= argentAdistrib.toInt/petiteCoupure(i)
          println("Vous pouvez obtenir au maximum "+ compteur_billet + " billet(s) de "+petiteCoupure(i)+  " CHF")

          if (petiteCoupure(i) == 10 && (coupurePriseAvant10==false)) {
            println("Vous devez prendre la proposition faite par le programme pour cette coupure.")
            argentAdistrib -= (compteur_billet * petiteCoupure(i))
            val x = compteur_billet + " billet(s) de " + petiteCoupure(i) + " CHF"
            tableCompteur += x
            i += 1

          }else{
            saut_de_coupure = readLine("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée >")


            if (saut_de_coupure=="o"){
              argentAdistrib=argentAdistrib-(compteur_billet*petiteCoupure(i))
              var x = compteur_billet + " billet(s) de "+petiteCoupure(i)+  " CHF"
              coupurePriseAvant10 = true
              tableCompteur+=x
              i=i+1

            }
            else if (petiteCoupure(i) == 10 && (coupurePriseAvant10==false)) {
              println("Vous devez prendre la proposition faite par le programme pour cette coupure.")
              argentAdistrib -= (compteur_billet * petiteCoupure(i))
              val x = compteur_billet + " billet(s) de " + petiteCoupure(i) + " CHF"
              tableCompteur += x
              i += 1
            }
            else{
              Try(saut_de_coupure.toInt) match {
                case Success(sautInt) if sautInt < compteur_billet =>
                  argentAdistrib = argentAdistrib-(sautInt * petiteCoupure(i))
                  coupurePriseAvant10 = true
                  if (sautInt!=0){
                    var x = compteur_billet + " billet(s) de "+petiteCoupure(i)+  " CHF"
                    tableCompteur+=x
                  }
                  if (sautInt==0){
                    coupurePriseAvant10=false
                  }
                  i=i+1

                case _ =>
                  System.exit(0)
              }
            }
          }


        }
        else{
          i=i+1
        }

      }
      println("")
      println("Veuillez retirer la somme demandée: ")

      for(i<-tableCompteur){
        println(i)
      }

      comptes(id)=comptes(id)-montantRetire
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))



    }
    else if (choixMonnaieRetrait==2){
      var petiteCoupure = ArrayBuffer[Int](100,50,20,10)
      var argentAdistrib=montantRetire
      var compteur_billet= 0
      var saut_de_coupure = ""
      var i = 0
      var tableCompteur =ArrayBuffer[String]()
      var coupurePriseAvant10 = false



      while((i < petiteCoupure.size)&&argentAdistrib>0){

        if(argentAdistrib.toInt/petiteCoupure(i)>=1){
          println("")
          println("Il reste "+ argentAdistrib+ " EUR à distribuer")
          compteur_billet= argentAdistrib.toInt/petiteCoupure(i)
          println("Vous pouvez obtenir au maximum "+ compteur_billet + " billet(s) de "+petiteCoupure(i)+  " EUR")

          if (petiteCoupure(i) == 10 && (coupurePriseAvant10==false)) {
            println("Vous devez prendre la proposition faite par le programme pour cette coupure.")
            argentAdistrib -= (compteur_billet * petiteCoupure(i))
            val x = compteur_billet + " billet(s) de " + petiteCoupure(i) + " EUR"
            tableCompteur += x
            i += 1

          }else{
            saut_de_coupure = readLine("Tapez 'o' pour ok ou une autre valeur inférieure à celle proposée >")


            if (saut_de_coupure=="o"){
              argentAdistrib=argentAdistrib-(compteur_billet*petiteCoupure(i))
              var x = compteur_billet + " billet(s) de "+petiteCoupure(i)+  " EUR"
              coupurePriseAvant10 = true
              tableCompteur+=x
              i=i+1

            }
            else if (petiteCoupure(i) == 10 && (coupurePriseAvant10==false)) {
              println("Vous devez prendre la proposition faite par le programme pour cette coupure.")
              argentAdistrib -= (compteur_billet * petiteCoupure(i))
              val x = compteur_billet + " billet(s) de " + petiteCoupure(i) + " EUR"
              tableCompteur += x
              i += 1
            }
            else{
              Try(saut_de_coupure.toInt) match {
                case Success(sautInt) if sautInt < compteur_billet =>
                  argentAdistrib = argentAdistrib-(sautInt * petiteCoupure(i))
                  coupurePriseAvant10 = true
                  if (sautInt!=0){
                    var x = compteur_billet + " billet(s) de "+petiteCoupure(i)+  " EUR"
                    tableCompteur+=x
                  }
                  if (sautInt==0){
                    coupurePriseAvant10=false
                  }
                  i=i+1

                case _ =>
                  System.exit(0)
              }
            }
          }


        }else{
          i=i+1
        }
      }
      println("")
      println("Veuillez retirer la somme demandée: ")

      for(i<-tableCompteur){
        println(i)
      }

      montantRetire=montantRetire*0.95
      comptes(id)=comptes(id)-montantRetire
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))


    }

  }

  def changepin(id : Int, codespin : Array[String]) : Unit ={

    var newPin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >").toString
    while (newPin.length<8){
      newPin = readLine("Votre code pin ne contient pas au moins 8 caractères \n Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    }
    codespin(id) = newPin
    println("Code pin modifié avec succès !")


  }




  def main(args: Array[String]): Unit = {
    println("")
    println("")
    println("")



    var nbclients = 100
    var montantInitial = 1200.0
    var pin = "INTRO1234"

    var comptes: Array[Double] = Array.fill[Double](nbclients)(montantInitial)
    var codespin: Array[String] = Array.fill[String](nbclients)(pin)

    while(true){

    var nbessai = 3
    var authenReussie = false
    var idClient = 0


      while (authenReussie==false) {
         idClient = readLine("Saisissez votre code identifiant >").toInt

        if (idClient >= nbclients || idClient < 0) {
          println("Cet identifiant n’est pas valable.")
          System.exit(0)
        } else {

          var essai_codePIN = ""
          var codePINjuste = codespin(idClient)
          essai_codePIN = readLine("Saisissez votre code pin >").toString
          var nbTentatives = 0

          while (essai_codePIN != codePINjuste && nbTentatives < nbessai) {
            essai_codePIN = readLine(s"Code pin erroné, il vous reste ${nbessai - nbTentatives} tentatives >").toString
            nbTentatives += 1
          }

          if (essai_codePIN == codePINjuste) {
            authenReussie = true
            println("Authentification réussie.")
          } else {
            println("Trop d’erreurs, abandon de l’identification")
          }
        }
      }
      println("")




      var choix_menu=menu()

      while (choix_menu!=5){

        println("")

        if (choix_menu==1){
          println("OPÉRATION DE DÉPÔT")

          depot(idClient,comptes)

          println("")
          choix_menu=menu()
        }

        if (choix_menu==3){
          println("OPÉRATION DE CONSULTATION")
          println("Le montant disponible sur votre compte est de : "+ comptes(idClient)+" CHF / " +comptes(idClient)*1.05+" EUR ")
          println(idClient)

          println("")
          choix_menu=menu()


        }

        if (choix_menu==2){
          println("OPÉRATION DE RETRAIT")

          retrait(idClient,comptes)

          println("")
          choix_menu=menu()
        }

        if (choix_menu==4){
          println("MODIFICATION DU CODE PIN")

          changepin(idClient,codespin)


          println("")
          choix_menu=menu()

        }

        if (choix_menu==5){
          println("")
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")


        }

      }

    }










  }

}

