//Assignment: Katell Lauranne Ninon Vanzo_996598_assignsubmission_file

import scala.io.StdIn.{readLine,readInt,readDouble}



object Main {

  def depot(id : Int, comptes : Array[Double]) : Unit =
{
      println("Indiquez la devise du dépôt : 1) CHF 2) EUR >")
     var deviseChoisie = readInt
     println("Indiquez le montant du dépôt")
     var montantDepot = readDouble
     while (montantDepot % 10 != 0)
     {println("Le montant doit être un multiple de 10")
     println("Indiquez le montant du dépôt")
     montantDepot = readDouble
     }
     if (deviseChoisie == 1)
     {comptes(id) = comptes(id) + montantDepot
     }
     if(deviseChoisie == 2)
     {montantDepot = montantDepot*0.95
      comptes(id) = comptes(id) + montantDepot
     }
}

  def retrait(id : Int, comptes : Array[Double]) : Unit =
  {
    var plafondRetrait = 0.1*comptes(id)
     var plafondRetraitEnEuro = 0.1*0.95*comptes(id)
    println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
    var choixDevise = readInt
    while (!(choixDevise == 1 || choixDevise == 2))
     {println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
     choixDevise = readInt
     }
    if (choixDevise == 1)
     {println("Indiquez le montant du retrait")
      var montantRetrait = readInt
      while (montantRetrait % 10 != 0)
      {println("Le montant doit être divisible par 10")
       println("Indiquez le montant du retrait")
       montantRetrait = readInt
      }
      while (montantRetrait > plafondRetrait)
      {println("Votre plafond de retrait autorisé est de : "+plafondRetrait+" CHF")
      println("Indiquez le montant du retrait")
      montantRetrait = readInt
      }
      comptes(id) = comptes(id) - montantRetrait
      if (montantRetrait >= 200)
      {println("En 1) Grosses coupures, 2) petites coupures")
       var choixCoupures = readInt
      while (choixCoupures != 1 && choixCoupures != 2)
       {println("En 1) Grosses coupures, 2) petites coupures")
       choixCoupures = readInt
       }
       if (choixCoupures == 1)
       {var nbrBilletsCinqCent = montantRetrait / 500
       if (nbrBilletsCinqCent != 0)
        {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCinqCent+" billet(s) de 500 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
        var nbrChoisi = readInt
        if (nbrChoisi == 0)
         {montantRetrait = montantRetrait - (nbrBilletsCinqCent*500)
         }
        while (nbrChoisi > nbrBilletsCinqCent)
         {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
         nbrChoisi = readInt
         }
        if (nbrChoisi < nbrBilletsCinqCent)
         {montantRetrait = montantRetrait - (500*nbrChoisi)
         }
        }
        var nbrBilletsDeuxCent = montantRetrait / 200
       if (nbrBilletsDeuxCent != 0)
        {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsDeuxCent+" billet(s) de 200 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
        var nbrChoisi = readInt
        if (nbrChoisi == 0)
         {montantRetrait = montantRetrait - (nbrBilletsDeuxCent*200)
         }
        while (nbrChoisi > nbrBilletsDeuxCent)
         {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
         nbrChoisi = readInt
         }
        if (nbrChoisi < nbrBilletsDeuxCent)
         {montantRetrait = montantRetrait - (200*nbrChoisi)
         }
        }
       var nbrBilletsCent = montantRetrait / 100
       if (nbrBilletsCent != 0)
        {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCent+" billet(s) de 100 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
        var nbrChoisi = readInt
        if (nbrChoisi == 0)
         {montantRetrait = montantRetrait - (nbrBilletsCent*100)
         }
        while (nbrChoisi > nbrBilletsCent)
         {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
         nbrChoisi = readInt
         }
        if (nbrChoisi < nbrBilletsCent)
         {montantRetrait = montantRetrait - (100*nbrChoisi)
         }
        }
       var nbrBilletsCinquante = montantRetrait / 50
       if (nbrBilletsCinquante != 0)
        {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCinquante+" billet(s) de 50 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
        var nbrChoisi = readInt
        if (nbrChoisi == 0)
         {montantRetrait = montantRetrait - (nbrBilletsCinquante*50)
         }
        while (nbrChoisi > nbrBilletsCinquante)
         {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
         nbrChoisi = readInt
         }
        if (nbrChoisi < nbrBilletsCinquante)
         {montantRetrait = montantRetrait - (50*nbrChoisi)
         }
        }
       var nbrBilletsVingt = montantRetrait / 20
       if (nbrBilletsVingt != 0)
        {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsVingt+" billet(s) de 20 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
        var nbrChoisi = readInt
        if (nbrChoisi == 0)
         {montantRetrait = montantRetrait - (nbrBilletsVingt*20)
         }
        while (nbrChoisi > nbrBilletsVingt)
         {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
         nbrChoisi = readInt
         }
        if (nbrChoisi < nbrBilletsVingt)
         {montantRetrait = montantRetrait - (20*nbrChoisi)
         }
        }
       var nbrBilletsDix = montantRetrait / 10
       if (nbrBilletsDix != 0)
        {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsDix+" billet(s) de 10 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
        var nbrChoisi = readInt
        if (nbrChoisi == 0)
         {montantRetrait = montantRetrait - (nbrBilletsDix*10)
         }
        while (nbrChoisi > nbrBilletsDix)
         {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
         nbrChoisi = readInt
         }
        if (nbrChoisi < nbrBilletsDix)
         {montantRetrait = montantRetrait - (10*nbrChoisi)
         }
        }
       println("Veuillez retirer la somme demandée : "+nbrBilletsCinqCent+" billet(s) de 500 CHF ; "+nbrBilletsDeuxCent+" billet(s) de 200 CHF ;"+nbrBilletsCent+" billet(s) de 100 CHF ; "+nbrBilletsCinquante+" billet(s) de 50 CHF ; "+nbrBilletsVingt+" billet(s) de 20 CHF ; "+nbrBilletsDix+" billet(s) de 10 CHF.")
       }
       if (choixCoupures == 2)
         {var nbrBilletsCent = montantRetrait / 100
          if (nbrBilletsCent != 0)
          {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCent+" billet(s) de 100 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
           var nbrChoisi = readInt
           if (nbrChoisi == 0)
           {montantRetrait = montantRetrait - (nbrBilletsCent*100)
           }
           while (nbrChoisi > nbrBilletsCent)
           {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
           nbrChoisi = readInt
           }
           if (nbrChoisi < nbrBilletsCent)
           {montantRetrait = montantRetrait - (100*nbrChoisi)
           }
          }
           var nbrBilletsCinquante = montantRetrait / 50
           if (nbrBilletsCinquante != 0)
           {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCinquante+" billet(s) de 50 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
            var nbrChoisi = readInt
            if (nbrChoisi == 0)
            {montantRetrait = montantRetrait - (nbrBilletsCinquante*50)
            }
            while (nbrChoisi > nbrBilletsCinquante)
            {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
            nbrChoisi = readInt
            }
            if (nbrChoisi < nbrBilletsCinquante)
            {montantRetrait = montantRetrait - (50*nbrChoisi)
            }
           }
            var nbrBilletsVingt = montantRetrait / 20
            if (nbrBilletsVingt != 0)
            {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsVingt+" billet(s) de 20 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
             var nbrChoisi = readInt
             if (nbrChoisi == 0)
             {montantRetrait = montantRetrait - (nbrBilletsVingt*20)
             }
             while (nbrChoisi > nbrBilletsVingt)
             {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
             nbrChoisi = readInt
             }
             if (nbrChoisi < nbrBilletsVingt)
             {montantRetrait = montantRetrait - (20*nbrChoisi)
             }
            }
             var nbrBilletsDix = montantRetrait / 10
             if (nbrBilletsDix != 0)
             {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsDix+" billet(s) de 10 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
              var nbrChoisi = readInt
              if (nbrChoisi == 0)
              {montantRetrait = montantRetrait - (nbrBilletsDix*10)
              }
              while (nbrChoisi > nbrBilletsDix)
              {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
              nbrChoisi = readInt
              }
              if (nbrChoisi < nbrBilletsDix)
              {montantRetrait = montantRetrait - (10*nbrChoisi)
              }
             }
              println("Veuillez retirer la somme demandée :"+nbrBilletsCent+" billet(s) de 100 CHF ; "+nbrBilletsCinquante+" billet(s) de 50 CHF ; "+nbrBilletsVingt+" billet(s) de 20 CHF ; "+nbrBilletsDix+" billet(s) de 10 CHF.")
         
         
        
       
      }
     }
      else
      {var nbrBilletsCent = montantRetrait / 100
          if (nbrBilletsCent != 0)
          {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCent+" billet(s) de 100 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
           var nbrChoisi = readInt
           if (nbrChoisi == 0)
           {montantRetrait = montantRetrait - (nbrBilletsCent*100)
           }
           while (nbrChoisi > nbrBilletsCent)
           {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
            nbrChoisi = readInt
           }
           if (nbrChoisi < nbrBilletsCent)
           {montantRetrait = montantRetrait - (100*nbrChoisi)
           }
          }
           var nbrBilletsCinquante = montantRetrait / 50
           if (nbrBilletsCinquante != 0)
           {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCinquante+" billet(s) de 50 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
            var nbrChoisi = readInt
            if (nbrChoisi == 0)
            {montantRetrait = montantRetrait - (50*nbrBilletsCinquante)
            }
            while (nbrChoisi > nbrBilletsCinquante)
            {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
            nbrChoisi = readInt
            }
            if (nbrChoisi < nbrBilletsCinquante)
            {montantRetrait = montantRetrait - (50*nbrChoisi)
            }
           }
            var nbrBilletsVingt = montantRetrait / 20
            if (nbrBilletsVingt != 0)
            {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsVingt+" billet(s) de 20 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
             var nbrChoisi = readInt
             if (nbrChoisi == 0)
             {montantRetrait = montantRetrait - (nbrBilletsVingt*20)
             }
             while (nbrChoisi > nbrBilletsVingt)
             {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
              nbrChoisi = readInt
             }
             if (nbrChoisi < nbrBilletsVingt)
             {montantRetrait = montantRetrait - (20*nbrChoisi)
             }
            }
             var nbrBilletsDix = montantRetrait / 10
             if (nbrBilletsDix != 0)
             {println("Il reste "+ montantRetrait+" CHF à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsDix+" billet(s) de 10 CHF. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
              var nbrChoisi = readInt
              if (nbrChoisi == 0)
              {montantRetrait = montantRetrait - (nbrBilletsDix*10)
              }
              while (nbrChoisi > nbrBilletsDix)
              {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
              nbrChoisi = readInt
              }
              if (nbrChoisi < nbrBilletsDix)
              {montantRetrait = montantRetrait - (10*nbrChoisi)
              }
             }
              println("Veuillez retirer la somme demandée :"+nbrBilletsCent+" billet(s) de 100 CHF ; "+nbrBilletsCinquante+" billet(s) de 50 CHF ; "+nbrBilletsVingt+" billet(s) de 20 CHF ; "+nbrBilletsDix+" billet(s) de 10 CHF.")
         }
        
      
     }
    if (choixDevise == 2)
     {println("Indiquez le montant du retrait >")
      var montantRetrait = readInt
     while (montantRetrait % 10 != 0) 
       {println("Le montant doit être un multiple de 10")
       println("Indiquez le montant du retrait")
       montantRetrait = readInt
       }
     while (montantRetrait > plafondRetraitEnEuro)
       {println("Votre plafond de retrait autorisé est de : "+plafondRetraitEnEuro+" euros")
       println("Indiquez le montant du retrait")
       montantRetrait = readInt
       }
     comptes(id) = comptes(id) - montantRetrait
     var nbrBilletsCent = montantRetrait / 100
          if (nbrBilletsCent != 0)
          {println("Il reste "+ montantRetrait+" € à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCent+" billet(s) de 100 €. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
           var nbrChoisi = readInt
           if (nbrChoisi == 0)
           {montantRetrait = montantRetrait - (nbrBilletsCent*100)
           }
           while (nbrChoisi > nbrBilletsCent)
           {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
            var nbrChoisi = readInt
           }
           if (nbrChoisi < nbrBilletsCent)
           {montantRetrait = montantRetrait - (100*nbrChoisi)
           }
          }
           var nbrBilletsCinquante = montantRetrait / 50
           if (nbrBilletsCinquante != 0)
           {println("Il reste "+ montantRetrait+" € à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsCinquante+" billet(s) de 50 €. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
            var nbrChoisi = readInt
            if (nbrChoisi == 0)
            {montantRetrait = montantRetrait - (nbrBilletsCinquante*50)
            }
            while (nbrChoisi > nbrBilletsCinquante)
            {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
            var nbrChoisi = readInt
            }
            if (nbrChoisi < nbrBilletsCinquante)
            {montantRetrait = montantRetrait - (50*nbrChoisi)
            }
           }
            var nbrBilletsVingt = montantRetrait / 20
            if (nbrBilletsVingt != 0)
            {println("Il reste "+ montantRetrait+" € à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsVingt+" billet(s) de 20 €. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
             var nbrChoisi = readInt
             if (nbrChoisi == 0)
             {montantRetrait = montantRetrait - (nbrBilletsVingt*20)
             }
             while (nbrChoisi > nbrBilletsVingt)
             {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
              var nbrChoisi = readInt
             }
             if (nbrChoisi < nbrBilletsVingt)
             {montantRetrait = montantRetrait - (20*nbrChoisi)
             }
            }
             var nbrBilletsDix = montantRetrait / 10
             if (nbrBilletsDix != 0)
             {println("Il reste "+ montantRetrait+" € à distribuer. Vous pouvez obtenir au maximum "+ nbrBilletsDix+" billet(s) de 10 €. Tapez 0 pour ok ou une autre valeur inférieure à celle proposée.")
              var nbrChoisi = readInt
              if (nbrChoisi == 0)
              {montantRetrait = montantRetrait - (nbrBilletsDix*10)
              }
              while (nbrChoisi > nbrBilletsDix)
              {println ("Le nombre de billets choisi doit être inférieur ou égal au nombre proposé.")
               var nbrChoisi = readInt
              }
              if (nbrChoisi < nbrBilletsDix)
              {montantRetrait = montantRetrait - (10*nbrChoisi)
              }
             }
              println("Veuillez retirer la somme demandée :"+nbrBilletsCent+" billet(s) de 100 € ; "+nbrBilletsCinquante+" billet(s) de 50 € ; "+nbrBilletsVingt+" billet(s) de 20 € ; "+nbrBilletsDix+" billet(s) de 10 €.")
             }
          
  }
   
  

  def changepin(id : Int, codespin : Array[String]) : Unit =
  {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    codespin(id) = readLine
    while (codespin(id).length < 8)
    {println ("Votre code pin ne contient pas au moins 8 caractères")
    codespin(id) = readLine
    }
  }
  
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes = Array.fill(nbclients+1)(1200.00)
    var codespin = Array.fill(nbclients+1)("INTRO1234")

    while (nbclients == 100)
    {
    println("Saisissez votre code identifiant >")
    var id = readInt
    if(id>nbclients)
    {println("Cet identifiant n'est pas valable")
    System.exit(0)
    }
    
     println ("Saississez votre code PIN >")
     var codePINtape : String = readLine
     while (codePINtape != codespin(id))
     {println("Code PIN erroné, il vous reste 2 essais.")
      codePINtape = readLine
       if (codePINtape != codespin(id))
       {println("Code PIN erroné, il vous reste 1 essai.")
        codePINtape = readLine
        if (codePINtape != codespin(id))
        {println ("Trop d'erreurs, abandon de l'identification.")
         println("Saisissez votre code identifiant >")
    id = readInt
    if(id>nbclients)
    {println("Cet identifiant n'est pas valable")
    System.exit(0)
    }
    println ("Saississez votre code PIN >")
    codePINtape = readLine
        }
       }
     }   
    
    println ("Choisissez votre opération :" + '\n' +" 1) Dépôt" + '\n'+" 2) Retrait " + '\n'+ " 3) Consultation du compte" + '\n'+ " 4) Changement du code pin " + '\n'+ " 5) Terminer " + '\n'+ "Votre choix : ")
    var choixOperation = readInt
    
    while(choixOperation != 5)  
  {
    
    if (choixOperation == 1)
    { depot(id,comptes)
      println("Votre dépôt a été prix en compte, le nouveau montant disponible sur votre compte est de "+ comptes(id) + " CHF.")
     println("Choisissez votre opération :" + '\n' +" 1) Dépôt" + '\n'+" 2) Retrait " + '\n'+ " 3) Consultation du compte" + '\n'+ " 4) Changement du code pin " + '\n'+ " 5) Terminer " + '\n'+ "Votre choix : ")
     choixOperation = readInt
     } 
   if (choixOperation == 2)
   {
     retrait(id,comptes)
      println("Choisissez votre opération :" + '\n' +" 1) Dépôt" + '\n'+" 2) Retrait " + '\n'+ " 3) Consultation du compte" + '\n'+ " 4) Changement du code pin " + '\n'+ " 5) Terminer " + '\n'+ "Votre choix : ")
      choixOperation = readInt
   }
    if (choixOperation == 3)
    {
      println("Le montant disponible sur votre compte est de "+ comptes(id)+" CHF")
     println("Choisissez votre opération :" + '\n' +" 1) Dépôt" + '\n'+" 2) Retrait " + '\n'+ " 3) Consultation du compte" + '\n'+ " 4) Changement du code pin " + '\n'+ " 5) Terminer " + '\n'+ "Votre choix : ")
     choixOperation = readInt
    }
    if (choixOperation == 4)
    {
      changepin(id,codespin)
      println("Choisissez votre opération :" + '\n' +" 1) Dépôt" + '\n'+" 2) Retrait " + '\n'+ " 3) Consultation du compte" + '\n'+ " 4) Changement du code pin " + '\n'+ " 5) Terminer " + '\n'+ "Votre choix : ")
      choixOperation = readInt
     }
    }
    
    if (choixOperation == 5)
    {
      println("Fin des opérations, n'oubliez pas de récuperer votre carte.")
    }

    }
    
  }
}

