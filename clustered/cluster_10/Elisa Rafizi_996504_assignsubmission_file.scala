//Assignment: Elisa Rafizi_996504_assignsubmission_file

import scala.io.StdIn._

object Main {
  val nbreClients = 100
  var comptes = Array.fill[Double](nbreClients)(1200.0)
  var codespin = Array.fill[String](nbreClients)("INTRO1234")

// identifiant 
  def main(args: Array[String]): Unit = {
    var continuer = true

    while (continuer) {
      val id = readLine("\n Saisissez votre code identifiant >").toInt

      if (id >= nbreClients) {
        println("\n Cet identifiant n'est pas valable.")
        continuer = false

// Code Pin 

      } else {
        val n = 3
        var tentatives = n
        var codepin = false

        while (tentatives > 0 && !codepin) {
          val saisiepin = readLine("\n Saisissez votre code pin >")

          if (saisiepin == codespin(id)) {
            codepin = true
          } else {
            tentatives -= 1
            println(s"\n Code pin erroné, il vous reste " + tentatives + " tentatives >")
          }
        }

        if (!codepin) {
          println("\n Trop d'erreurs, abandon de l'identification")
        } else {
          var action = 0

// choix d'action 

          while (action != 5) {
            action = readLine("\n Choisissez votre opération :\n 1) Dépôt\n 2) Retrait\n 3) Consultation du compte\n 4) Changement du code pin\n 5) Terminer\nVotre choix :").toInt

            action match {
              case 1 => depot(id, comptes)
              case 2 => retrait(id, action, comptes, codespin)
              case 3 => consultationDuCompte(id)
              case 4 => changepin(id, codespin)
              case 5 => println("\n Fin des opérations, n'oubliez pas de récupérer votre carte.")
              case _ => println("\n Choix invalide. Veuillez réessayer.")
            } // fin action match 
          } // fin while 
        } // fin else 
      } // fin else 


    } // fin while continuer 

    // Consultation du compte 3

      def consultationDuCompte(id: Int): Unit = {
        printf("\n Le montant sur votre compte est de : %.2f CHF.\n", comptes(id))
    } // fin consultation compte 

    // Changement du code pin 4

      def changepin(id : Int, codespin : Array[String]) : Unit = {
      var nouveauPin = ""
      while (nouveauPin.length < 8) {
        nouveauPin = readLine("\n Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        if (nouveauPin.length < 8) {
          println("\n Votre code pin ne contient pas au moins 8 caractères")
        }
      }

      codespin(id) = nouveauPin
      println("\n Votre code pin a été changé avec succès.")
    } // fin changement pin 

// Dépôt 1

  def depot(id : Int, comptes : Array[Double]) : Unit = {
    val devise = readLine("\n Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
    var montantDepot = readLine("\n Indiquez le montant du dépôt > ").toDouble

    while (montantDepot % 10 != 0) {
      println("\n Le montant doit être un multiple de 10")
      montantDepot = readLine("\n Indiquez le montant du dépôt > ").toDouble
    }

    if (devise == 2) {
      montantDepot = montantDepot * 0.95
    }

    comptes(id) += montantDepot
    printf("\n Le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", comptes(id))
  } // fin def dépôt 

// Retrait 2

  def retrait(id : Int, action : Int, comptes : Array[Double], codespin : Array[String]) : Unit = {
    if(action == 2){

      var devise = 0

    //devise du retrait 
      while ((devise != 1) && (devise != 2)){
      devise = readLine("\n Indiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
      } 

      var MontantRetrait = readLine("\n Indiquez le montant du retrait > ").toDouble

        val PlafondCHF = 0.1 * comptes(id)
        val PlafondEUR = PlafondCHF * 1.05



      if(devise==1){
        while(MontantRetrait > PlafondCHF){
          printf("\n Votre plafond de retrait autorisé est de : %.2f CHF.\n", PlafondCHF)
      MontantRetrait = readLine("\n Indiquez le montant du retrait > ").toDouble
      }
      }

      else if (devise==2){
        while(MontantRetrait > PlafondEUR){
          printf("\n Votre plafond de retrait autorisé est de : %.2f CHF.\n", PlafondEUR)
      MontantRetrait = readLine("\n Indiquez le montant du retrait > ").toDouble
      }
      }

      while(MontantRetrait % 10 != 0 ) {
        println("\n Le montant doit être un multiple de 10 ")
        MontantRetrait = readLine("\n Indiquez le montant du retrait > ").toDouble
      }

    var coupures = 0
    var continuerCoupures = true
    var MontantRestant = MontantRetrait
    var coupure500 = 0
    var coupure200 = 0
    var coupure100 = 0
    var coupure50 = 0
    var coupure20 = 0
    var coupure10 = 0

    if((devise == 1)&&(MontantRetrait >= 200)){ 

    while ((coupures != 1 ) && (coupures != 2)){
      coupures = readLine("\n En 1) grosses coupures, 2) petites coupures >").toInt
    }//fin demande coupures CHF

    //grosses coupures = 1 

    if((coupures == 1)&&(MontantRetrait >= 200)){

      if(MontantRestant >= 500){
        println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 500).toInt + " billet(s) de 500 CHF.")
        val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(choix.toLowerCase == "o"){
          coupure500 += (MontantRestant / 500).toInt
          MontantRestant = MontantRestant - (500 * coupure500)
        } else if(choix.nonEmpty) {
          coupure500 += (choix).toInt
          MontantRestant -= (choix.toInt * 500).toInt
          continuerCoupures = false
        }
        } //fin coupure 500 CHF

      if(MontantRestant >= 200){
        println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 200).toInt + " billet(s) de 200 CHF.")
        val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(choix.toLowerCase == "o"){
          coupure200 += (MontantRestant / 200).toInt
          MontantRestant = MontantRestant - (200 * coupure200)
        } else if(choix.nonEmpty) {
          coupure200 += (choix).toInt 
          MontantRestant -= (choix.toInt * 200).toInt
          continuerCoupures = false
        }
        } //fin coupure 200 CHF

      if(MontantRestant >= 100){
        println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 100).toInt + " billet(s) de 100 CHF.")
        val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(choix.toLowerCase == "o"){
          coupure100 += (MontantRestant / 100).toInt
          MontantRestant = MontantRestant - (100 * coupure100)
        } else if(choix.nonEmpty) {
          coupure100 += (choix).toInt 
          MontantRestant -= (choix.toInt * 100).toInt
          continuerCoupures = false
        }
        } //fin coupure 100 CHF

      if(MontantRestant >= 50){
        println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 50).toInt + " billet(s) de 50 CHF.")
        val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(choix.toLowerCase == "o"){
          coupure50 += (MontantRestant / 50).toInt
          MontantRestant = MontantRestant - (50 * coupure50)
        } else if(choix.nonEmpty) {
          coupure50 += (choix).toInt 
          MontantRestant -= (choix.toInt * 50).toInt
          continuerCoupures = false
        }
        } //fin coupure 50 CHF

      if(MontantRestant >= 20){
        println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 20).toInt + " billet(s) de 20 CHF.")
        val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

        if(choix.toLowerCase == "o"){
          coupure20 += (MontantRestant / 20).toInt
          MontantRestant = MontantRestant - (20 * coupure200)
        } else if(choix.nonEmpty) {
          coupure20 += (choix).toInt 
          MontantRestant -= (choix.toInt * 20).toInt
          continuerCoupures = false
        }
        } //fin coupure 20 CHF

      if(MontantRestant >= 10){
        println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 10).toInt + " billet(s) de 10 CHF.")
        val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


        if((coupure500 == 0) && (coupure200 == 0) && (coupure100 == 0) && (coupure50 == 0) && (coupure20 == 0)){
          coupure10 += (MontantRetrait / 10).toInt
          MontantRestant = MontantRestant - (10 * coupure10)

        }

        else if(choix.toLowerCase == "o"){
          coupure10 += (MontantRestant / 10).toInt
          MontantRestant = MontantRestant - (10 * coupure10)
        } else if(choix.nonEmpty) {
          coupure10 += (choix).toInt 
          MontantRestant -= (choix.toInt * 10).toInt
          continuerCoupures = false
        }
        } //fin coupure 10 CHF

    }// fin grosses coupures CHF
          }  //fin devise retrait CHF grosses coupures 


    // début petites coupures 

    if((MontantRetrait < 200)&&(devise == 1)){

    if(MontantRestant >= 100){
      println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 100).toInt + " billet(s) de 100 CHF.")
      val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

      if(choix.toLowerCase == "o"){
        coupure100 += (MontantRestant / 100).toInt
        MontantRestant = MontantRestant - (100 * coupure100)
      } else if(choix.nonEmpty) {
        coupure100 += (choix).toInt 
        MontantRestant -= (choix.toInt * 100).toInt
        continuerCoupures = false
      }
      } //fin coupure 100 CHF

    if(MontantRestant >= 50){
      println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 50).toInt + " billet(s) de 50 CHF.")
      val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

      if(choix.toLowerCase == "o"){
        coupure50 += (MontantRestant / 50).toInt
        MontantRestant = MontantRestant - (50 * coupure50)
      } else if(choix.nonEmpty) {
        coupure50 += (choix).toInt 
        MontantRestant -= (choix.toInt * 50).toInt
        continuerCoupures = false
      }
      } //fin coupure 50 CHF

    if(MontantRestant >= 20){
      println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 20).toInt + " billet(s) de 20 CHF.")
      val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

      if(choix.toLowerCase == "o"){
        coupure20 += (MontantRestant / 20).toInt
        MontantRestant = MontantRestant - (20 * coupure20)
      } else if(choix.nonEmpty) {
        coupure20 += (choix).toInt 
        MontantRestant -= (choix.toInt * 20).toInt
        continuerCoupures = false
      }
      } //fin coupure 20 CHF

    if(MontantRestant >= 10){
      println("\n Il reste " + MontantRestant + " CHF à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 10).toInt + " billet(s) de 10 CHF.")
      val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")


      if((coupure500 == 0) && (coupure200 == 0) && (coupure100 == 0) && (coupure50 == 0) && (coupure20 == 0)){
        coupure10 += (MontantRetrait / 10).toInt
        MontantRestant = MontantRestant - (10 * coupure10)

      }

      else if(choix.toLowerCase == "o"){
        coupure10 += (MontantRestant / 10).toInt
        MontantRestant = MontantRestant - (10 * coupure10)
      } else if(choix.nonEmpty) {
        coupure10 += (choix).toInt 
        MontantRestant -= (choix.toInt * 10).toInt
        continuerCoupures = false
      }
     } //fin coupure 10 CHF

    }//fin petites coupures CHF

    // début coupures EUR

    if(devise == 2){

        if(MontantRestant >= 100){
          println("\n Il reste " + MontantRestant + " EUR à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 100).toInt + " billet(s) de 100 EUR.")
          val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(choix.toLowerCase == "o"){
            coupure100 += (MontantRestant / 100).toInt
            MontantRestant = MontantRestant - (100 * coupure100)
          } else if(choix.nonEmpty){
            coupure100 += (choix).toInt 
            MontantRestant -= (choix.toInt * 100).toInt
            continuerCoupures = false
          }
          } //fin coupure 100 EUR

        if(MontantRestant >= 50){
          println("\n Il reste " + MontantRestant + " EUR à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 50).toInt + " billet(s) de 50 EUR.")
          val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(choix.toLowerCase == "o"){
            coupure50 += (MontantRestant / 50).toInt
            MontantRestant = MontantRestant - (50 * coupure50)
          } else if(choix.nonEmpty) {
            coupure50 += (choix).toInt 
            MontantRestant -= (choix.toInt * 50).toInt
            continuerCoupures = false
          }
          } //fin coupure 50 EUR

        if(MontantRestant >= 20){
          println("\n Il reste " + MontantRestant + " EUR à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 20).toInt + " billet(s) de 20 EUR.")
          val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(choix.toLowerCase == "o"){
            coupure20 += (MontantRestant / 20).toInt
            MontantRestant = MontantRestant - (20 * coupure20)
          } else if(choix.nonEmpty) {
            coupure20 += (choix).toInt 
            MontantRestant -= (choix.toInt * 20).toInt
            continuerCoupures = false
          }
          } //fin coupure 20 EUR

        if(MontantRestant >= 10){
        
          println("\n Il reste " + MontantRestant + " EUR à distribuer. \n Vous pouvez obtenir au maximum " + (MontantRestant / 10).toInt + " billet(s) de 10 EUR.")

          if((coupure500 == 0) && (coupure200 == 0) && (coupure100 == 0) && (coupure50 == 0) && (coupure20 == 0)){
            coupure10 += (MontantRetrait / 10).toInt
            MontantRestant = MontantRestant - (10 * coupure10)

          }
          else {

          val choix : String = readLine("\n Tapez o pour ok ou une autre valeur inférieure à celle proposée >")

          if(choix.toLowerCase == "o"){
            coupure10 += (MontantRestant / 10).toInt
            MontantRestant = MontantRestant - (10 * coupure10)
          } else if(choix.nonEmpty) {
            coupure10 += (choix).toInt 
            MontantRestant -= (choix.toInt * 10).toInt
            continuerCoupures = false
          }
         }
        }//fin coupure 10 EUR

    } //fin coupure euros

    println("\n Veuillez retirer la somme demandée : ")

    if(devise == 1){
    if(coupure500 != 0){
    println(coupure500 + " billet(s) de 500CHF")
    } //fin retrait 500
    if(coupure200 != 0){
      println(coupure200 + " billet(s) de 200CHF")
    } //fin retrait 200
    if(coupure100 != 0){
      println(coupure100 + " billet(s) de 100CHF")
    } //fin retrait 100
    if(coupure50 != 0){
      println(coupure50 + " billet(s) de 50CHF")
    } //fin retrait 50
    if(coupure20 != 0){
      println(coupure20 + " billet(s) de 20CHF")
    } //fin retrait 20
    if(coupure10 != 0){
      println(coupure10 + " billet(s) de 10CHF")
    } //fin retrait 10
    } // fin retrait devise 1

    else if(devise == 2){
    if(coupure100 != 0){
      println(coupure100 + " billet(s) de 100EUR")
    } //fin retrait 100
    if(coupure50 != 0){
      println(coupure50 + " billet(s) de 50EUR")
    } //fin retrait 50
    if(coupure20 != 0){
      println(coupure20 + " billet(s) de 20EUR")
    } //fin retrait 20
    if(coupure10 != 0){
      println(coupure10 + " billet(s) de 10EUR")
    } //fin retrait 10
    } //fin rettrait devise 2

    if(devise == 1){
    comptes(id) -= MontantRetrait
      printf("\n Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.\n", comptes(id))
    } //fin décla argent compte CHF

    if(devise == 2){
    comptes(id) = (comptes(id) - (MontantRetrait * 0.95))
    printf("\n Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f EUR.\n", comptes(id))
    } //fin décla argent compte EUR

  } // fin retrait 

  } // fin def main 
}
}

