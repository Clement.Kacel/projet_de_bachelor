//Assignment: Valentine Victoria Venti_996546_assignsubmission_file

import scala.io.StdIn._

object Main{

  def depot(identifiant: Int, Comptes: Array[Double]): Unit = {
    println ("Indiquez la devise du dépot : 1) CHF ; 2) EUR > ")
    var devise = readInt ()
    println ("Indiquez le montant du dépôt > ")
    var montantdepot = readInt ()
    while((montantdepot %10) != 0){
      println ("Le montant doit être un multiple de 10")
      montantdepot = readInt ()
    }
    if (devise == 1){
      Comptes(identifiant) = Comptes(identifiant) + montantdepot
    } else {
      Comptes(identifiant) = Comptes(identifiant) + montantdepot*0.95
    }
    printf ("Votre dépot a été pris en compte, le nouveau montant disponible sur le compte : " + identifiant + " est de : %.2f \n ", Comptes(identifiant))
  }
  def retrait(identifiant: Int, Comptes: Array[Double]): Unit = {
    var nombre = 0
    var billet500 = 0
    var billet200 = 0
    var billet100 = 0
    var billet50 = 0
    var billet20 = 0
    var billet10 = 0
    println ("indiquez la devise : 1 CHF, 2 EUR > ")
    var devise = readInt ()
    while ((devise != 1) && (devise != 2)){
      println ("indiquez la devise : 1 CHF, 2 EUR > pp")
      devise = readInt ()
    }
    println ("indiquez le montant du retrait > ")
    var montantretrait = readInt ()
    if(devise == 1){
      while(((montantretrait %10) != 0) || (montantretrait > (0.1* Comptes(identifiant)))){
        if ((montantretrait %10) != 0)  {
          println ("Le montant doit être un multiple de 10")
        }
        if (montantretrait > (0.1* Comptes(identifiant))) {
          println ("Votre plafond de retrait autorisé est de : " + 0.1* Comptes(identifiant))
        }
        montantretrait = readInt ()
      }
    } else {
      while(((montantretrait %10) != 0)|| (montantretrait*0.95 > (0.1* Comptes(identifiant)))) {
        if ((montantretrait %10) != 0)  {
          println ("Le montant doit être un multiple de 10")
        }
        if (montantretrait*0.95 > (0.1*Comptes(identifiant))) {
          println ("Le plafond de retrait autorisé  sur le compte : " + identifiant + " est de : " + 0.1* Comptes(identifiant)*0.95)
        }
        montantretrait = readInt ()
      }
    }
    if (devise == 1) {
      Comptes(identifiant) = Comptes(identifiant) - montantretrait
      if (montantretrait >= 200) {
        println("En 1) grosses coupures, 2) petites coupures > ")
        var coupures = readInt()
        while ((coupures != 1) && (coupures != 2)) {
          println("En 1) grosses coupures, 2) petites coupures > ")
          coupures = readInt()
        }
        if (coupures == 1) {
          var accepte = ""
          if (montantretrait >= 500) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 500 + " billet(s) de 500CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/500) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet500 = nombre
              montantretrait = montantretrait - nombre * 500
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet500 = montantretrait / 500
            montantretrait = montantretrait - (montantretrait / 500) * 500
          }
          if (montantretrait >= 200) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 200 + " billet(s) de 200CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/200) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet200 = nombre
              montantretrait = montantretrait - nombre * 200
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet200 = montantretrait / 200
            montantretrait = montantretrait - (montantretrait / 200) * 200
          }
          if (montantretrait >= 100) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 100 + " billet(s) de 100CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/100) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet100 = nombre
              montantretrait = montantretrait - nombre * 100
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet100 = montantretrait / 100
            montantretrait = montantretrait - (montantretrait / 100) * 100
          }
          if (montantretrait >= 50) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 50 + " billet(s) de 50CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/50) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet500 = nombre
              montantretrait = montantretrait - nombre * 50
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet50 = montantretrait / 50
            montantretrait = montantretrait - (montantretrait / 50) * 50
          }
          if (montantretrait >= 20) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 20 + " billet(s) de 20 CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/20) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet500 = nombre
              montantretrait = montantretrait - nombre * 20
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet20 = montantretrait / 20
            montantretrait = montantretrait - (montantretrait / 20) * 20
          }
          if (montantretrait >= 10) {
            println("Il reste " + montantretrait + " CHF à distribuer")

            billet10 = montantretrait / 10
            montantretrait = montantretrait - (montantretrait / 10) * 10
          }
        } else {
          var accepte = ""
          if (montantretrait >= 100) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 100 + " billet(s) de 100")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/100) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet500 = nombre
              montantretrait = montantretrait - nombre * 100
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet100 = montantretrait / 100
            montantretrait = montantretrait - (montantretrait / 100) * 100
          }
          if (montantretrait >= 50) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 50 + " billet(s) de 50CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/50) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet500 = nombre
              montantretrait = montantretrait - nombre * 50
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet50 = montantretrait / 50
            montantretrait = montantretrait - (montantretrait / 50) * 50
          }
          if (montantretrait >= 20) {
            println("Il reste " + montantretrait + " CHF à distribuer")
            println("Vous pouvez obtenir au maximum " + montantretrait / 20 + " billet(s) de 20 CHF")
            accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
            if (accepte != "o") {
              nombre = accepte.toInt
              while (nombre > montantretrait/20) {
                accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
                nombre = accepte.toInt
              }
              billet500 = nombre
              montantretrait = montantretrait - nombre * 20
            }
          }
          if (accepte == "o") {
            accepte = "n"
            billet20 = montantretrait / 20
            montantretrait = montantretrait - (montantretrait / 20) * 20
            println("Il reste à distribuer " + montantretrait)
          }
          if (montantretrait >= 10) {
            println("Il reste " + montantretrait + " CHF à distribuer")

            billet10 = montantretrait / 10
            montantretrait = montantretrait - (montantretrait / 10) * 10
          }
        }
      } else {
        var accepte = ""
        if (montantretrait >= 100) {
          println("Il reste " + montantretrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + montantretrait / 100 + " billet(s) de 100")
          accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
          if (accepte != "o") {
            nombre = accepte.toInt
            while (nombre > montantretrait/100) {
              accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
              nombre = accepte.toInt
            }
            billet500 = nombre
            montantretrait = montantretrait - nombre * 100
          }
        }
        if (accepte == "o") {
          accepte = "n"
          billet100 = montantretrait / 100
          montantretrait = montantretrait - (montantretrait / 100) * 100
        }
        if (montantretrait >= 50) {
          println("Il reste " + montantretrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + montantretrait / 50 + " billet(s) de 50CHF")
          accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
          if (accepte != "o") {
            nombre = accepte.toInt
            while (nombre > montantretrait/50) {
              accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
              nombre = accepte.toInt
            }
            billet500 = nombre
            montantretrait = montantretrait - nombre * 50
          }
        }
        if (accepte == "o") {
          accepte = "n"
          billet50 = montantretrait / 50
          montantretrait = montantretrait - (montantretrait / 50) * 50
        }
        if (montantretrait >= 20) {
          println("Il reste " + montantretrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + montantretrait / 20 + " billet(s) de 20 CHF")
          accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
          if (accepte != "o") {
            nombre = accepte.toInt
            while (nombre > montantretrait/20) {
              accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
              nombre = accepte.toInt
            }
            billet500 = nombre
            montantretrait = montantretrait - nombre * 20
          }
        }
        if (accepte == "o") {
          accepte = "n"
          billet20 = montantretrait / 20
          montantretrait = montantretrait - (montantretrait / 20) * 20
          println("Il reste à distribuer " + montantretrait)
        }
        if (montantretrait >= 10) {
          println("Il reste " + montantretrait + " CHF à distribuer")

          billet10 = montantretrait / 10
          montantretrait = montantretrait - (montantretrait / 10) * 10
        }
      }
      if (billet500 > 0){
        println(billet500 + " billet(s) de 500 CHF")
      }
      if (billet200 > 0){
        println(billet200 + " billet(s) de 200 CHF")
      }
      if (billet100 > 0){
        println(billet100 + " billet(s) de 100 CHF")
      }
      if (billet50 > 0){
        println(billet50 + " billet(s) de 50 CHF")
      }
      if (billet20 > 0){
        println(billet20 + " billet(s) de 20 CHF")
      }
      if (billet10 > 0){
        println(billet10 + " billet(s) de 10 CHF")
      }
    }else {
      Comptes(identifiant) = Comptes(identifiant) - montantretrait*0.95
      var accepte = ""
      if(montantretrait >= 100){
        println ("Il reste " + montantretrait + " EUR à distribuer")
        println ("Vous pouvez obtenir au maximum " + montantretrait / 100 + " billet(s) de 100EUR")
        accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
        if(accepte != "o"){
          nombre = accepte.toInt
          while (nombre > montantretrait/100) {
            accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
            nombre = accepte.toInt
          }
          billet100 = nombre
          montantretrait = montantretrait - nombre * 100
        }
      }
      if(accepte == "o"){
        accepte = "n"
        billet100 = montantretrait / 100
        montantretrait = montantretrait - (montantretrait / 100) * 100
      }
      if(montantretrait >= 50){
        println ("Il reste " + montantretrait + " EUR à distribuer")
        println ("Vous pouvez obtenir au maximum " + montantretrait / 50 + " billet(s) de 50EUR")
        accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
        if(accepte != "o"){
          nombre = accepte.toInt
          while (nombre > montantretrait/50) {
            accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
            nombre = accepte.toInt
          }
          billet500 = nombre
          montantretrait = montantretrait - nombre * 50
        }
      }
      if(accepte == "o"){
        accepte = "n"
        billet50 = montantretrait / 50
        montantretrait = montantretrait - (montantretrait / 50) * 50
      }
      if(montantretrait >= 20){
        println ("Il reste " + montantretrait + " EUR à distribuer")
        println ("Vous pouvez obtenir au maximum " + montantretrait / 20 + " billet(s) de 20 EUR")
        accepte = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée  ")
        if(accepte != "o"){
          nombre = accepte.toInt
          while (nombre > montantretrait/20) {
            accepte = readLine(" Vous ne pouvez pas avoir autant de billets, veuillez saisir un nouveau nombre plus petit")
            nombre = accepte.toInt
          }
          billet500 = nombre
          montantretrait = montantretrait - nombre * 20
        }
      }
      if(accepte == "o"){
        accepte = "n"
        billet20 = montantretrait / 20
        montantretrait = montantretrait - (montantretrait / 20) * 20
        println("Il reste à distribuer " + montantretrait)
      }
      if(montantretrait >= 10){
        println ("Il reste " + montantretrait + " EUR à distribuer")

        billet10 = montantretrait / 10
        montantretrait = montantretrait - (montantretrait / 10) * 10
      }
      println("Veuillez retirer la somme demandée : ")
      if (billet100 > 0){
        println(billet100 + " billet(s) de 100 EUR")
      }
      if (billet50 > 0){
        println(billet50 + " billet(s) de 50 EUR")
      }
      if (billet20 > 0){
        println(billet20 + " billet(s) de 20 EUR")
      }
      if (billet10 > 0){
        println(billet10 + " billet(s) de 10 EUR")
      }
    }
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur le compte : " + identifiant + " est de : %.2f \n ", Comptes(identifiant))
  }
  def changepin(identifiant: Int, Codespin: Array[String]): Unit = {
    var nouveauPin = readLine("Entrez un nouveau Pin > ")
    while (nouveauPin.length < 8){
      println("Le nouveau Pin doit contenir au moins 8 caractères")
      nouveauPin = readLine("Entrez un nouveau Pin > ")
    }
    Codespin(identifiant) = nouveauPin
    println("Le nouveau Pin du compte " + identifiant + " est " +nouveauPin)
  }

  def main(args: Array[String]): Unit = {

    var choix = 0
    var tentative = 3
    val nbclients = 100
    val Comptes = Array.fill(nbclients)(1200.0)
    val Codespin = Array.fill(nbclients)("INTRO1234")
    var operation = 1
    var identifiant = -1


    while (operation == 1) {
      identifiant = readLine("Saississez votre identifiant > ").toInt
      if (identifiant > 99 || identifiant < 0) {
        operation = 0
        println("Cet identifiant n'est pas valable.")
      
      } else {
        if (tentative == 3) {
          var saisirlecodePIN = readLine("Veuillez entrer le code pin du compte  " + identifiant + " > ")
          while ((Codespin(identifiant) != saisirlecodePIN) && (tentative != 1)) {
            if (tentative > 0) {
              tentative = tentative - 1
              saisirlecodePIN = readLine("Code pin erroné, il vous reste " + tentative + " tentatives > ")
            }
          }
          if (Codespin(identifiant) == saisirlecodePIN) {
            operation = 2
            tentative = 3
          } else {
            tentative = 3
            println("Trop d'erreurs, abandon de l'identification")
          }
        }
      }
    if(operation == 2){
      while(choix != 5){
        println("Choisissez l'opération qui vous intéresse : \n 1) Dépot \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \nVotre choix : ")
        choix = readInt()

        if(choix == 1){
          depot(identifiant, Comptes)
        }
        else if(choix == 2){
          retrait(identifiant, Comptes)
        }
        else if(choix == 3){
          printf ("Le montant disponible sur votre compte est de :  %.2f \n ", Comptes(identifiant))
        }
        else if(choix == 4){
          changepin(identifiant, Codespin)
        }
        else if(choix == 5){
          println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
          operation = 1
        }
      }
      choix = 0
     }
    }

  }
}