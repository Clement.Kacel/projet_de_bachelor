//Assignment: Cristopher William Cadena Burbano_996382_assignsubmission_file

import io.StdIn._
object Main {
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    var comptes = Array.fill(nbclients)(1200.0)
    var codespin = Array.fill(nbclients)("INTRO1234")
    var choice = 0
    var id = 0
    var authenticated = false
    var attempts = 0
    var max_attempts = 2
    var userpin = ""
    var deposit_choice = ""
    var deposit_amount = 0.0
    var withdraw_choice = ""
    var withdraw_amount = 0.0
    var withdraw_amount_total = 0.0
    var bills = ""
    var accept = ""
    var bill_500 = 0
    var bill_200 = 0
    var bill_100 = 0
    var bill_50 = 0
    var bill_20 = 0
    var bill_10 = 0
    var newpin = ""

    while (id < nbclients) {
      println ("Saisissez votre code identifiant >")
      id = readInt()  

      if (id > nbclients) {
        println ("Cet identifiant n’est pas valable.")
      }

        println ("Saisissez votre code pin >")
        userpin = readLine()
        while (userpin != codespin(id) && attempts < max_attempts) {
          println ("Code pin erroné, il vous reste " + (max_attempts - attempts) + " tentatives")
          attempts += 1
          println ("Saisissez votre code pin >")
          userpin = readLine()
          if (attempts == max_attempts) {
            println ("Trop d’erreurs, abandon de l’identification")
          } else {
            authenticated = true
            println("Choisissez votre opération :")
            println("1) Dépôt")
            println("2) Retrait")
            println("3) Consultation du compte")
            println("4) Changement du code pin")
            println("5) Terminer")
            println("Votre choix : ")
            choice = readInt()
          }
        }      

          if (choice == 1 && authenticated) {
            depot(id, comptes)
          }

          if (choice == 2 && authenticated) {
            retrait(id, comptes)
          }

          if (choice == 3 && authenticated) {
            println("Le montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
          }

          if (choice == 4 && authenticated) {
            changepin(id, codespin)
          }

          if (choice == 5 && authenticated) {
          println("Fin des opérations, n’oubliez pas de récuperer votre carte.")
        }

        def depot(id : Int, comptes : Array[Double]): Unit = {
          println("Indiquez la devise du depôt : 1) CHF; 2) EUR >")
          deposit_choice = readLine()

          println("Indiquez le montant du depôt >")
          deposit_amount = readInt()

          while(!(deposit_amount % 10 == 0)){
            println("Le montant doit être un multiple de 10")
            println("Indiquez le montant du depôt >")
            deposit_amount = readInt()
          }
          if (deposit_choice == "1"){
            comptes(id) += deposit_amount 
          } else {
            deposit_amount = deposit_amount * 0.95
            comptes(id) += deposit_amount
          }
          printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
        }

        def retrait(id : Int, comptes : Array[Double]): Unit = {
          println("Indiquez la devise du depôt : 1) CHF; 2) EUR >")
          withdraw_choice = readLine()
          while (!(withdraw_choice == "1" || withdraw_choice == "2")) {
            println ("Indiquez la devise du depôt : 1) CHF; 2) EUR >")
            withdraw_choice = readLine()
          }
          println("Indiquez le montant du retrait >")
          withdraw_amount = readInt()

          while(!(withdraw_amount % 10 == 0)){
            println("Le montant doit être un multiple de 10")
            println("Indiquez le montant du retrait >")
            withdraw_amount = readInt()
          }

          if (withdraw_amount > 0.1 * comptes(id)) {
            println("Votre plafond de retrait autorisé est de : " + (0.1 * comptes(id)) + " CHF")
            println("Indiquez le montant du retrait >")
            withdraw_amount = readInt()
          }
          withdraw_amount_total = withdraw_amount
          if (withdraw_choice == "1" && withdraw_amount >= 200) {
            println("En 1) grosses coupures, 2) petites coupures")
            bills = readLine()
            while (!(bills == "1" || bills == "2")) {
            println("En 1) grosses coupures, 2) petites coupures")
            bills = readLine()
            }

            while (withdraw_amount != 0) {
              if (bills == "1" && withdraw_amount >= 500) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 500 + " billet(s) de 500 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_500 = withdraw_amount.toInt / 500
                } else {
                  bill_500 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_500 * 500
              }          

              if (bills == "1" && withdraw_amount >= 200) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 200 + " billet(s) de 200 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_200 = withdraw_amount.toInt / 200
                } else {
                  bill_200 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_200 * 200
              }

              if (bills == "1" && withdraw_amount >= 100) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 100 + " billet(s) de 100 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_100 = withdraw_amount.toInt / 100
                } else {
                  bill_100 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_100 * 100
              }

              if (bills == "1" && withdraw_amount >= 50) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 50 + " billet(s) de 50 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_50 = withdraw_amount.toInt / 50
                } else {
                  bill_50 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_50 * 50
              }

              if (bills == "1" && withdraw_amount >= 20) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 20 + " billet(s) de 20 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_20 = withdraw_amount.toInt / 20
                } else {
                  bill_20 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_20 * 20
              }
              if (bills == "1" && withdraw_amount >= 10) {
                bill_10 = withdraw_amount.toInt / 10
                withdraw_amount = withdraw_amount.toInt - bill_10 * 10
              }

              if (bills == "1"){
                println("Veuillez retirer la somme demandée :")
                if (bill_500 >= 1) {
                  println(bill_500 + " billet(s) de 500 CHF")
                }
                if (bill_200 >= 1) {
                  println(bill_200 + " billet(s) de 200 CHF")
                }
                if (bill_100 >= 1) {
                  println(bill_100 + " billet(s) de 100 CHF")
                }
                if (bill_50 >= 1) {
                  println(bill_50 + " billet(s) de 50 CHF")
                }
                if (bill_20 >= 1) {
                  println(bill_20 + " billet(s) de 20 CHF")
                }
                if (bill_10 >= 1) {
                  println(bill_10 + " billet(s) de 10 CHF")
                }
                comptes(id) = comptes(id) - withdraw_amount_total
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
              }

              if (bills == "2" && withdraw_amount >= 500) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 100 + " billet(s) de 100 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_100 = withdraw_amount.toInt / 100
                } else {
                  bill_100 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_100 * 100
              }

              if (bills == "2" && withdraw_amount >= 50) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 50 + " billet(s) de 50 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_50 = withdraw_amount.toInt / 50
                } else {
                  bill_50 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_50 * 50
              }

              if (bills == "2" && withdraw_amount >= 20) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 20 + " billet(s) de 20 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_20 = withdraw_amount.toInt / 20
                } else {
                  bill_20 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_20 * 20
              }
              if (bills == "2" && withdraw_amount >= 10) {
                bill_10 = withdraw_amount.toInt / 10
                withdraw_amount = withdraw_amount.toInt - bill_10 * 10
              }

              if (bills == "2"){
                println("Veuillez retirer la somme demandée :")
                if (bill_100 >= 1) {
                  println(bill_100 + " billet(s) de 100 CHF")
                }
                if (bill_50 >= 1) {
                  println(bill_50 + " billet(s) de 50 CHF")
                }
                if (bill_20 >= 1) {
                  println(bill_20 + " billet(s) de 20 CHF")
                }
                if (bill_10 >= 1) {
                  println(bill_10 + " billet(s) de 10 CHF")
                }
                comptes(id) = comptes(id) - withdraw_amount_total
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
              }
            }
          }
          if (withdraw_choice == "1" && withdraw_amount < 200){
            while (withdraw_amount != 0) {
              if (withdraw_amount >= 100) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 100 + " billet(s) de 100 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_100 = withdraw_amount.toInt / 100
                } else {
                  bill_100 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_100 * 100
              }

              if (withdraw_amount >= 50) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 50 + " billet(s) de 50 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_50 = withdraw_amount.toInt / 50
                } else {
                  bill_50 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_50 * 50
              }

              if (withdraw_amount >= 20) {
                println("Il reste " + withdraw_amount.toInt + " CHF à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 20 + " billet(s) de 20 CHF" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_20 = withdraw_amount.toInt / 20
                } else {
                  bill_20 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_20 * 20
              }
              if (withdraw_amount >= 10) {
                bill_10 = withdraw_amount.toInt / 10
                withdraw_amount = withdraw_amount.toInt - bill_10 * 10
              }

              if (withdraw_amount == 0){
                println("Veuillez retirer la somme demandée :")
                if (bill_100 >= 1) {
                  println(bill_100 + " billet(s) de 100 CHF")
                }
                if (bill_50 >= 1) {
                  println(bill_50 + " billet(s) de 50 CHF")
                }
                if (bill_20 >= 1) {
                  println(bill_20 + " billet(s) de 20 CHF")
                }
                if (bill_10 >= 1) {
                  println(bill_10 + " billet(s) de 10 CHF")
                }
                comptes(id) = comptes(id) - withdraw_amount_total
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
              }
            }
          }
          if (withdraw_choice == "2"){
            while (withdraw_amount != 0) {
              if (withdraw_amount >= 100) {
                println("Il reste " + withdraw_amount.toInt + " EUR à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 100 + " billet(s) de 100 EUR" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_100 = withdraw_amount.toInt / 100
                } else {
                  bill_100 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_100 * 100
              }

              if (withdraw_amount >= 50) {
                println("Il reste " + withdraw_amount.toInt + " EUR à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 50 + " billet(s) de 50 EUR" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_50 = withdraw_amount.toInt / 50
                } else {
                  bill_50 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_50 * 50
              }

              if (withdraw_amount >= 20) {
                println("Il reste " + withdraw_amount.toInt + " EUR à distribuer")
                println("Vous pouvez obtenir au maximun " + withdraw_amount.toInt / 20 + " billet(s) de 20 EUR" )
                println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
                accept = readLine()

                if (accept == "o") {
                  bill_20 = withdraw_amount.toInt / 20
                } else {
                  bill_20 = accept.toInt
                }
                withdraw_amount = withdraw_amount.toInt - bill_20 * 20
              }
              if (withdraw_amount >= 10) {
                bill_10 = withdraw_amount.toInt / 10
                withdraw_amount = withdraw_amount.toInt - bill_10 * 10
              }

              if (withdraw_amount == 0){
                println("Veuillez retirer la somme demandée :")
                if (bill_100 >= 1) {
                  println(bill_100 + " billet(s) de 100 EUR")
                }
                if (bill_50 >= 1) {
                  println(bill_50 + " billet(s) de 50 EUR")
                }
                if (bill_20 >= 1) {
                  println(bill_20 + " billet(s) de 20 EUR")
                }
                if (bill_10 >= 1) {
                  println(bill_10 + " billet(s) de 10 EUR")
                }
                comptes(id) = comptes(id) - withdraw_amount_total * 0.95
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n" , comptes(id))
              }
            } 
          }
        }

        def changepin(id : Int, codespin : Array[String]) : Unit = {
          println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
          newpin = readLine()

          if (newpin.length < 8) {
            while (newpin.length < 8) {
              println("Votre code pin ne contient pas au moins 8 caractères")
              println("Saisissez votre nouveau code pin (il doit contenir au moins 8 carractères) >")
              newpin = readLine()
            } 
          } else {
            codespin(id) = newpin
          }
      }  
    }
  }
}

