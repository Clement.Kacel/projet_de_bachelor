//Assignment: Enzo Gérard Poupard-Kuimi_996589_assignsubmission_file

import scala.io.StdIn.readLine
object Main {
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    var entree = 0
    var verifid = false
    val codespin = Array.fill(nbclients)("INTRO1234")
    val comptes = Array.fill(nbclients)(1200.0)
    var sortie1 = false
    var sortie2 = false
    var code = false
    var id = 150
    var fin = false
    while (!fin) {
      sortie1 = false
      while (!sortie1) {
        id = readLine("\nSaisissez votre code identifiant > ").toInt
        if ((id > 99) || (id < 0)) {
          verifid = false
          println("Cet identifiant n’est pas valable. ")
          sortie1 = true
          fin = true
        }
        else {
          verifid = true
        }
        if (entree == 4) {
          sortie1 = true
        }
        if ((entree != 4) && verifid) { // tant que le client n'entre pas 4 le programme recommence
          var tentative = 3
          var pin = readLine("Saisissez votre code pin > ") //h Saisie du code PIN une et une seule fois
          code = false
          var sortie = false
          if (pin ==  codespin(id)) {
            code = true
            sortie1 = true
          }
          else {
            while (!sortie) { //h tant que la code est faux
              if (tentative != 1) {
                tentative -= 1
                println("Code pin erroné, il vous reste " + tentative + " tentatives >")
                pin = readLine("\nSaisissez votre code pin > ")
                if (pin ==  codespin(id)) {
                  code = true
                  sortie = true
                  sortie1 = true
                }
                if ((tentative == 1) && !code) {
                  print("Trop d’erreurs, abandon de l’identification")
                  sortie = true //h pour arrêter mon programme si il n'y a plus de tentatives
                }
              }
            }
          }
        }
      }
      if (verifid) {
        entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
        while ((entree != 1) && (entree != 2) && (entree != 3) && (entree != 4) && (entree != 5)) {
          entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
        }
      }
      if (code) { //h un bout de code pour chaque cas
        while (entree != 5) {
          def depot(id: Int, comptes: Array[Double]): Unit = {
            print("\n_______________Dépôt_______________ \n") //h Pour le dépôt
            val devise = readLine("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR > ").toInt
            var montant = readLine("\nIndiquez le montant du dépôt > ").toInt
            var multiple = montant % 10
            while (multiple != 0) {
              montant = readLine("\nMerci de donner un multiple de 10,\nindiquez le montant du dépôt > ").toInt
              if ((montant % 10) == 0) {
                multiple = 0
              }
            }
            if (devise == 1) {
              comptes(id) += montant
            }
            if (devise == 2) {
              comptes(id) += (montant * 0.95)
            }
            printf("Votre dépôt a été pris en compte,\nle nouveau montant disponible en CHF sur votre compte est de : %.2f\n", comptes(id))
            entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            while ((entree != 1) && (entree != 2) && (entree != 3) && (entree != 4) && (entree != 5)) {
              entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            }
          }
          def retrait(id: Int, comptes: Array[Double]): Unit = {
            print("\n_______________Retrait_______________ \n") //h Pour le retrait
            var devise = 0
            while ((devise != 1) && (devise != 2)) {
              devise = readLine("\nIndiquez la devise du retrait : 1) CHF ; 2) EUR > ").toInt
            }
            var montant_re = 5
            var multiple = montant_re % 10
            while (multiple != 0) {
              montant_re = readLine("\nLe montant doit être un multiple de 10,\nindiquez le montant du retrait > ").toInt
              if (montant_re % 10 == 0) {
                multiple = 0
              }
            }
            if (devise == 1) {
              val plafond = 0.1 * comptes(id)
              while (montant_re > plafond) {
                println("\nVotre plafond de retrait autorisé est de : " + plafond)
                montant_re = readLine("\nIndiquez le montant du retrait > ").toInt
              }
              var coupures = 0
              while ((coupures != 1) && (coupures != 2)) {
                if (montant_re >= 200) {
                  coupures = readLine("1) grosses coupures, 2) petites coupures > ").toInt
                }
                else {
                  coupures = 2
                }
              }
              if (coupures == 1) {
                println("\n_______________Grosses coupures - CHF_______________")
                var billet500 = 0
                var billet200 = 0
                var billet100 = 0
                var billet50 = 0
                var billet20 = 0
                var billet10 = 0
                var COUP = montant_re
                if ((montant_re != 0) && (COUP / 500 >= 1)) {
                  while (COUP >= 500) { // Billet de 500 CHF
                    billet500 += 1
                    COUP -= 500
                  }
                  var choix = ""
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet500 + " billet(s) de 500 CHF")
                  choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix == "o") {
                    montant_re -= billet500 * 500
                  }
                  else {
                    while ((choix.toInt > billet500) || (choix.toInt < 0)) {
                      choix = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix.toInt * 500
                    COUP = montant_re
                    billet500 = choix.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 200 >= 1)) {
                  while (COUP >= 200) { // Billet de 200 CHF
                    billet200 += 1
                    COUP -= 200
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet200 + " billet(s) de 200 CHF")
                  var choix1 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix1 == "o") {
                    montant_re -= billet200 * 200
                  }
                  else {
                    while ((choix1.toInt > billet200) || (choix1.toInt < 0)) {
                      choix1 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix1.toInt * 200
                    COUP = montant_re
                    billet200 = choix1.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 100 >= 1)) {
                  while (COUP >= 100) { // Billet de 100 CHF
                    billet100 += 1
                    COUP -= 100
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet100 + " billet(s) de 100 CHF")
                  var choix2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix2 == "o") {
                    montant_re -= billet100 * 100
                  }
                  else {
                    while ((choix2.toInt > billet100) || (choix2.toInt < 0)) {
                      choix2 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix2.toInt * 100
                    COUP = montant_re
                    billet100 = choix2.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 50 >= 1)) {
                  while (COUP >= 50) { // Billet de 50 CHF
                    billet50 += 1
                    COUP -= 50
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet50 + " billet(s) de 50 CHF")
                  var choix3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix3 == "o") {
                    montant_re -= billet50 * 50
                  }
                  else {
                    while ((choix3.toInt > billet50) || (choix3.toInt < 0)) {
                      choix3 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix3.toInt * 50
                    COUP = montant_re
                    billet50 = choix3.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 20 >= 1)) {
                  while (COUP >= 20) { // Billet de 20 CHF
                    billet20 += 1
                    COUP -= 20
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet20 + " billet(s) de 20 CHF")
                  var choix4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix4 == "o") {
                    montant_re -= billet20 * 20
                  }
                  else {
                    while ((choix4.toInt > billet20) || (choix4.toInt < 0)) {
                      choix4 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix4.toInt * 20
                    COUP = montant_re
                    billet20 = choix4.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 10 >= 1)) {
                  while (COUP >= 10) { // Billet de 10 CHF
                    billet10 += 1
                    COUP -= 10
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet10 + " billet(s) de 10 CHF")
                  montant_re -= billet10 * 10
                }
                println("\nVeuillez retirer la somme demandée : \n")
                if (billet500 != 0) {
                  println(billet500 + " billet(s) de 500 CHF")
                }
                if (billet200 != 0) {
                  println(billet200 + " billet(s) de 200 CHF")
                }
                if (billet100 != 0) {
                  println(billet100 + " billet(s) de 100 CHF")
                }
                if (billet50 != 0) {
                  println(billet50 + " billet(s) de 50 CHF")
                }
                if (billet20 != 0) {
                  println(billet20 + " billet(s) de 20 CHF")
                }
                if (billet10 != 0) {
                  println(billet10 + " billet(s) de 10 CHF")
                }
                comptes(id) -= (billet500 * 500 + billet200 * 200 + billet100 * 100 + billet50 * 50 + billet20 * 20 + billet10 * 10)
                printf("\nVotre retrait a été pris en compte,\nle nouveau montant disponible en CHF sur votre compte est de : %.2f \n", comptes(id))
              }
              if (coupures == 2) {
                println("\n_______________Petites coupures - CHF_______________")
                var billet100 = 0
                var billet50 = 0
                var billet20 = 0
                var billet10 = 0
                var COUP = montant_re
                if ((montant_re != 0) && (COUP / 100 >= 1)) {
                  while (COUP >= 100) { // Billet de 100 CHF
                    billet100 += 1
                    COUP -= 100
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet100 + " billet(s) de 100 CHF")
                  var choix2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix2 == "o") {
                    montant_re -= billet100 * 100
                  }
                  else {
                    while ((choix2.toInt > billet100) || (choix2.toInt < 0)) {
                      choix2 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix2.toInt * 100
                    COUP = montant_re
                    billet100 = choix2.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 50 >= 1)) {
                  while (COUP >= 50) { // Billet de 50 CHF
                    billet50 += 1
                    COUP -= 50
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet50 + " billet(s) de 50 CHF")
                  var choix3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix3 == "o") {
                    montant_re -= billet50 * 50
                  }
                  else {
                    while ((choix3.toInt > billet50) || (choix3.toInt < 0)) {
                      choix3 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix3.toInt * 50
                    COUP = montant_re
                    billet50 = choix3.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 20 >= 1)) {
                  while (COUP >= 20) { // Billet de 20 CHF
                    billet20 += 1
                    COUP -= 20
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet20 + " billet(s) de 20 CHF")
                  var choix4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  if (choix4 == "o") {
                    montant_re -= billet20 * 20
                  }
                  else {
                    while ((choix4.toInt > billet20) || (choix4.toInt < 0)) {
                      choix4 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    }
                    montant_re -= choix4.toInt * 20
                    COUP = montant_re
                    billet20 = choix4.toInt
                  }
                }
                if ((montant_re != 0) && (COUP / 10 >= 1)) {
                  while (COUP >= 10) { // Billet de 10 CHF
                    billet10 += 1
                    COUP -= 10
                  }
                  println("\nIl reste " + montant_re + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum de " + billet10 + " billet(s) de 10 CHF")
                  montant_re -= billet10 * 10
                }
                println("\nVeuillez retirer la somme demandée : \n")
                if (billet100 != 0) {
                  println(billet100 + " billet(s) de 100 CHF")
                }
                if (billet50 != 0) {
                  println(billet50 + " billet(s) de 50 CHF")
                }
                if (billet20 != 0) {
                  println(billet20 + " billet(s) de 20 CHF")
                }
                if (billet10 != 0) {
                  println(billet10 + " billet(s) de 10 CHF")
                }
                comptes(id) -= (billet100 * 100 + billet50 * 50 + billet20 * 20 + billet10 * 10)
                printf("\nVotre retrait a été pris en compte,\nle nouveau montant disponible en CHF sur votre compte est de : %.2f \n", comptes(id))
              }

            }
            if (devise == 2) {
              println("\n_______________Petites coupures - EUR_______________")
              var billet100 = 0
              var billet50 = 0
              var billet20 = 0
              var billet10 = 0
              var COUP = montant_re
              if ((montant_re != 0) && (COUP / 100 >= 1)) {
                while (COUP >= 100) { // Billet de 100 EUR
                  billet100 += 1
                  COUP -= 100
                }
                println("\nIl reste " + montant_re + " EUR à distribuer")
                println("Vous pouvez obtenir au maximum de " + billet100 + " billet(s) de 100 EUR")
                var choix2 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (choix2 == "o") {
                  montant_re -= billet100 * 100
                }
                else {
                  while ((choix2.toInt > billet100) || (choix2.toInt < 0)) {
                    choix2 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  }
                  montant_re -= choix2.toInt * 100
                  COUP = montant_re
                  billet100 = choix2.toInt
                }
              }
              if ((montant_re != 0) && (COUP / 50 >= 1)) {
                while (COUP >= 50) { // Billet de 50 EUR
                  billet50 += 1
                  COUP -= 50
                }
                println("\nIl reste " + montant_re + " EUR à distribuer")
                println("Vous pouvez obtenir au maximum de " + billet50 + " billet(s) de 50 EUR")
                var choix3 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (choix3 == "o") {
                  montant_re -= billet50 * 50
                }
                else {
                  while ((choix3.toInt > billet50) || (choix3.toInt < 0)) {
                    choix3 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  }
                  montant_re -= choix3.toInt * 50
                  COUP = montant_re
                  billet50 = choix3.toInt
                }
              }
              if ((montant_re != 0) && (COUP / 20 >= 1)) {
                while (COUP >= 20) { // Billet de 20 EUR
                  billet20 += 1
                  COUP -= 20
                }
                println("\nIl reste " + montant_re + " EUR à distribuer")
                println("Vous pouvez obtenir au maximum de " + billet20 + " billet(s) de 20 EUR")
                var choix4 = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                if (choix4 == "o") {
                  montant_re -= billet20 * 20
                }
                else {
                  while ((choix4.toInt > billet20) || (choix4.toInt < 0)) {
                    choix4 = readLine("Merci d'entrer une valeur correct. Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  }
                  montant_re -= choix4.toInt * 20
                  COUP = montant_re
                  billet20 = choix4.toInt
                }
              }
              if ((montant_re != 0) && (COUP / 10 >= 1)) {
                while (COUP >= 10) { // Billet de 10 EUR
                  billet10 += 1
                  COUP -= 10
                }
                println("\nIl reste " + montant_re + " EUR à distribuer")
                println("Vous pouvez obtenir au maximum de " + billet10 + " billet(s) de 10 EUR")
                montant_re -= billet10 * 10
              }
              println("\nVeuillez retirer la somme demandée : \n")
              if (billet100 != 0) {
                println(billet100 + " billet(s) de 100 EUR")
              }
              if (billet50 != 0) {
                println(billet50 + " billet(s) de 50 EUR")
              }
              if (billet20 != 0) {
                println(billet20 + " billet(s) de 20 EUR")
              }
              if (billet10 != 0) {
                println(billet10 + " billet(s) de 10 EUR")
              }
              comptes(id) -= ((billet100 * 100 + billet50 * 50 + billet20 * 20 + billet10 * 10) * 0.95)
              printf("\nVotre retrait a été pris en compte,\nle nouveau montant disponible en CHF sur votre compte est de : %.2f \n", comptes(id))

            }
            entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            while ((entree != 1) && (entree != 2) && (entree != 3) && (entree != 4) && (entree != 5)) {
              entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            }
          }

          if (entree == 3) {
            print("\n_______________Consultation du compte_______________ \n") //h Consultation du compte
            printf("Le montant disponible en CHF sur votre compte est de : %.2f\n", comptes(id))
            entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            while ((entree != 1) && (entree != 2) && (entree != 3) && (entree != 4) && (entree != 5)) {
              entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            }
          }
          def changepin(id: Int, codespin: Array[String]): Unit = {
            sortie2 = false
            while (!sortie2) {
              val nvpin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
              val nombrecar = nvpin.length
              if (nombrecar >= 8) {
                sortie2 = true
                codespin(id) = nvpin

              }
              else {
                println("Votre code pin ne contient pas au moins 8 caractères")
              }
            }
            entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            while ((entree != 1) && (entree != 2) && (entree != 3) && (entree != 4) && (entree != 5)) {
              entree = readLine("Choisissez votre opération : \n   1) Dépôt \n   2) Retrait \n   3) Consultation du compte \n   4) Changement du code pin   \n   5) Terminer \nVotre choix : ").toInt
            }
          }
          if (entree == 1) {
            depot(id, comptes)
          }
          if (entree == 2) {
            retrait(id, comptes)
          }
          if (entree == 4) {
            changepin(id, codespin)
          }
        }
      }
      println("\nFin des opérations, n’oubliez pas de récupérer votre carte.")
    }
  }
}

