//Assignment: Diego Julio Pinedo Torrejon_996509_assignsubmission_file

import scala.io.StdIn._

object Main {

  //début méthode depot

    def depot(id: Int, comptes : Array[Double]):Unit = {
      var montantDepot = 0.0
      val tauxE = 0.95
      val tauxF = 1.05
      var choixDevise = 0
      var currentMontant = 0.0
        do{
          print("Indiquez la devise du dépôt : 1) CHF; 2) EUR > ")
          choixDevise = readInt()
        } while (choixDevise < 1 || choixDevise > 2)

      while ((montantDepot % 10 != 0) || (montantDepot <= 0)){
        if(montantDepot <= 0){
          print("Indiquez le montant du dépôt > ")
          montantDepot = readDouble()
        } else if (montantDepot % 10 != 0){
          println("Le montant doit être un multiple de 10")
          print("Indiquez le montant du dépôt > ")
          montantDepot = readDouble()
        }
      }
      if (choixDevise == 2){
        montantDepot = montantDepot * tauxE
        currentMontant += montantDepot
        comptes(id) +=  currentMontant

      }else {
        currentMontant += montantDepot
        comptes(id) +=  currentMontant
      }
    }
  // fin méthode dépot

  // debut méthode retrait
    def retrait(id:Int, comptes:Array[Double]):Unit = {
      print("Indiquez la devise: 1 CHF, 2  : EUR > ")
          var choixDevise = readInt()
          while ((choixDevise < 1) || (choixDevise > 2)) {
            print("Indiquez la devise: 1 CHF, 2  : EUR > ")
            choixDevise = readInt()
          }
          var montantRetrait = 0.0

          do {
            print("Indiquez le montant du retrait > ")
            montantRetrait = readDouble()
          } while (montantRetrait <= 0)

      // Verification si montantRetrait est multiple de 10 et pas plus du 10% de comptes(id)
          while ((montantRetrait % 10 != 0) || (montantRetrait >  comptes(id) * 0.10)) {
            if (montantRetrait % 10 != 0) {
              println("Le montant doit être un multiple de 10.")
              print("Indiquez le montant du retrait > ")
              montantRetrait = readDouble()
            } else if (montantRetrait > comptes(id) * 0.10) {
              println("Votre plafond de retrait autorisé est de : " + (comptes(id) * 0.10))
              print("Indiquez le montant du retrait > ")
              montantRetrait = readDouble()
            }
          }

      //choix Devise CHF et coupures si montantRetrai est plus ou égal à 200
          if (choixDevise == 1) {
            if(montantRetrait >= 200) {
              print("En 1) grosses coupures, 2) petite coupures > ") // choix de coupures
              var choixCoupures = readInt()

              while ((choixCoupures < 1) || (choixCoupures > 2)) {
                print("En 1) grosses coupures, 2) petite coupures > ")
                choixCoupures = readInt()
              }
      // choix grosses coupures          
              if (choixCoupures == 1) {
                val billsCHF = Array(500.0,200.0,100.0,50.0,20.0,10.0)
                var countMaxBillsToWithdraw = new Array[Int](billsCHF.length)
                var billIndex = -1
                // trouver le billet plus grand disponible selon le montantRetrait
                for (i <- 0 to billsCHF.length-1; if billIndex == -1) {
                  if (billsCHF(i).toInt <= montantRetrait) {
                    billIndex = i
                  }
                }

                var currentMontant = comptes(id) // copie de la valeur originale
                do {
                  var billValue = billsCHF(billIndex).toInt
                  var maxBills = Math.floor((montantRetrait) / billValue).toInt

                  if (maxBills != 0) {
                    println("Il reste " + (montantRetrait) + " CHF à distribuer")
                    println("Vous pouvez obtenir au maximum "+ maxBills + " billet(s) de " + billValue + " CHF")
                    var userSelection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                    var billsToRemove = 0.0
                    var newBillQuantityToRetrieve = 0.0

                    if (userSelection == "o") {
                      billsToRemove = (maxBills * billValue)
                      countMaxBillsToWithdraw(billIndex) = maxBills

                    } else {
                      if ((billIndex == billsCHF.length-1) && (userSelection.toDouble == 0)){
                        billsToRemove = (maxBills * billValue)
                        countMaxBillsToWithdraw(billIndex) = maxBills
                      } else {
                        newBillQuantityToRetrieve = userSelection.toDouble
                        billsToRemove = (newBillQuantityToRetrieve * billValue)
                        countMaxBillsToWithdraw(billIndex) = newBillQuantityToRetrieve.toInt
                      }
                    }
                    montantRetrait = montantRetrait - billsToRemove
                    currentMontant = currentMontant - billsToRemove

                  }
                  billIndex = billIndex + 1
                } 
                while (montantRetrait > 0 && billIndex <= billsCHF.length-1)

                println("Veuillez retirer la somme demandée: ")

                for (i <- 0 to countMaxBillsToWithdraw.length-1) {
                  if (countMaxBillsToWithdraw(i) > 0) {
                    printf(s"${countMaxBillsToWithdraw(i)} billet(s) de %.0f CHF \n", billsCHF(i)) //print element Array si la valeur n'est pas 0
                  }
                }
                // mise à jour du comptes(id) = currentMontant et print du comptes(id)
                comptes(id) = currentMontant
                printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
          }
      // fin grosses coupures

      // début petit coupures
            if (choixCoupures == 2) {
                  val billsCHF = Array(100.0,50.0,20.0,10.0)
                  var countMaxBillsToWithdraw = new Array[Int](billsCHF.length)
                  var billIndex = -1
                  // trouver le billet plus grand disponible selon le montantRetrait
                  for (i <- 0 to billsCHF.length-1; if billIndex == -1) {
                    if (billsCHF(i).toInt <= montantRetrait) {
                      billIndex = i
                    }
                  }

                  var currentMontant = comptes(id) // copie de la valeur originale
                  do {
                    var billValue = billsCHF(billIndex).toInt
                    var maxBills = Math.floor((montantRetrait) / billValue).toInt


                    if (maxBills != 0) {
                      println("Il reste " + (montantRetrait) + " CHF à distribuer")
                      println("Vous pouvez obtenir au maximum "+ maxBills + " billet(s) de " + billValue)
                      var userSelection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                      var billsToRemove = 0.0
                      var newBillQuantityToRetrieve = 0.0

                      if (userSelection == "o") {
                        billsToRemove = (maxBills * billValue)
                        countMaxBillsToWithdraw(billIndex) = maxBills
                      } else {
                        if ((billIndex == billsCHF.length-1) && (userSelection.toDouble == 0)){
                          billsToRemove = (maxBills * billValue)
                          countMaxBillsToWithdraw(billIndex) = maxBills
                        } else {
                          newBillQuantityToRetrieve = userSelection.toDouble
                          billsToRemove = (newBillQuantityToRetrieve * billValue)
                          countMaxBillsToWithdraw(billIndex) = newBillQuantityToRetrieve.toInt
                        }
                      }
                      montantRetrait = montantRetrait - billsToRemove
                      currentMontant = currentMontant - billsToRemove

                    }
                    billIndex = billIndex + 1
                  } 
                  while (montantRetrait > 0 || billIndex <= billsCHF.length-1)

                  println("Veuillez retirer la somme demandée: ")

                  for (i <- 0 to countMaxBillsToWithdraw.length-1) {
                    if (countMaxBillsToWithdraw(i) > 0) {
                      printf(s"${countMaxBillsToWithdraw(i)} billet(s) de %.0f CHF \n", billsCHF(i)) //print element Array si la valeur n'est pas 0
                    }
                  }


              // mise à jour du comptes(id) = currentMontant et print de comptes(id)
              comptes(id) = currentMontant
              printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
            }
      //fin petit coupures
        } else { // fin > 200 et début else < 200, pas de choix de coupures
              val billsCHF = Array(100.0,50.0,20.0,10.0)
              var countMaxBillsToWithdraw = new Array[Int](billsCHF.length)
              var billIndex = -1
              // trouver le billet plus grand disponible selon le montantRetrait
              for (i <- 0 to billsCHF.length-1; if billIndex == -1) {
                if (billsCHF(i).toInt <= montantRetrait) {
                  billIndex = i
                }
              }

              var currentMontant = comptes(id) // copie de la valeur originale
              do {
                var billValue = billsCHF(billIndex).toInt
                var maxBills = Math.floor((montantRetrait) / billValue).toInt


                if (maxBills != 0) {
                  println("Il reste " + (montantRetrait) + " CHF à distribuer")
                  println("Vous pouvez obtenir au maximum "+ maxBills + " billet(s) de " + billValue + " CHF")
                  var userSelection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
                  var billsToRemove = 0.0
                  var newBillQuantityToRetrieve = 0.0

                  if (userSelection == "o") {
                        billsToRemove = (maxBills * billValue)
                        countMaxBillsToWithdraw(billIndex) = maxBills
                  } else {
                    if ((billIndex == billsCHF.length-1) && (userSelection.toDouble == 0)){
                      billsToRemove = (maxBills * billValue)
                      countMaxBillsToWithdraw(billIndex) = maxBills
                    } else {
                      newBillQuantityToRetrieve = userSelection.toDouble
                      billsToRemove = (newBillQuantityToRetrieve * billValue)
                      countMaxBillsToWithdraw(billIndex) = newBillQuantityToRetrieve.toInt
                    }
                  }
                    montantRetrait = montantRetrait - billsToRemove
                    currentMontant = currentMontant - billsToRemove

                  }
                    billIndex = billIndex + 1
                } 
                  while (montantRetrait > 0 || billIndex <= billsCHF.length-1)

                  println("Veuillez retirer la somme demandée: ")

                  for (i <- 0 to countMaxBillsToWithdraw.length-1) {
                    if (countMaxBillsToWithdraw(i) > 0) {
                      printf(s"${countMaxBillsToWithdraw(i)} billet(s) de %.0f CHF \n", billsCHF(i)) //print element Array si la valeur n'est pas 0
                    }
                  }


              // mise à jour du comptes(id) = currentMontant et print du comptes(id)
              comptes(id) = currentMontant
              printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))

        } // fin else < 200 pas de coupures
      }      
      // fin  if choixDevise == 1 => if montantRetrait >= 200 else < 200 pas  de coupures

      // début choixDevise == 2 EURO
        if (choixDevise == 2) {
          val tauxE = 0.95
          val tauxF = 1.05 
          val billsEUR = Array(100.0,50.0,20.0,10.0)
          var countMaxBillsToWithdraw = new Array[Int](billsEUR.length)
          var billIndex = -1
          // trouver le billet plus grand disponible selon le montantRetrait
          for (i <- 0 to billsEUR.length-1; if billIndex == -1) {
            if (billsEUR(i).toInt <= montantRetrait) {
              billIndex = i
            }
          }

          var currentMontantToRemoveEUR = 0.0 // copie de la valeur originale
          do {
            var billValue = billsEUR(billIndex).toInt
            var maxBills = Math.floor((montantRetrait) / billValue).toInt


            if (maxBills != 0) {
              println("Il reste " + (montantRetrait) + " EUR à distribuer")
              println("Vous pouvez obtenir au maximum "+ maxBills + " billet(s) de " + billValue + " EUR")
              var userSelection = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              var billsToRemove = 0.0
              var newBillQuantityToRetrieve = 0.0

              if (userSelection == "o") {
                    billsToRemove = (maxBills * billValue)
                    countMaxBillsToWithdraw(billIndex) = maxBills
              } else {
                if ((billIndex == billsEUR.length-1) && (userSelection.toDouble == 0)){
                  billsToRemove = (maxBills * billValue)
                  countMaxBillsToWithdraw(billIndex) = maxBills
                } else {
                  newBillQuantityToRetrieve = userSelection.toDouble
                  billsToRemove = (newBillQuantityToRetrieve * billValue)
                  countMaxBillsToWithdraw(billIndex) = newBillQuantityToRetrieve.toInt
                }
                  }
                  montantRetrait = montantRetrait - billsToRemove
                  currentMontantToRemoveEUR = currentMontantToRemoveEUR + (billsToRemove * tauxE)

                }
                billIndex = billIndex + 1
              } 
              while (montantRetrait > 0 || billIndex <= billsEUR.length-1)

              println("Veuillez retirer la somme demandée: ")

              for (i <- 0 to countMaxBillsToWithdraw.length-1) {
                if (countMaxBillsToWithdraw(i) > 0) {
                  printf(s"${countMaxBillsToWithdraw(i)} billet(s) de %.0f EUR \n", billsEUR(i)) //print element Array si la valeur n'est pas 0
                }
              }


          // mise à jour du comptes(id) = currentMontant et print du comptes(id)
          comptes(id) = comptes(id) - currentMontantToRemoveEUR
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
        }
      // Fin choixDevise == 2 EUR 
    }

  //fin méthode retrait

  // début méthode changement de Pin

  def changepin(id:Int,codespin:Array[String]): Unit = {
    print("Veuillez introduire un nouveau code PIN: ")
    var nouveauCodePin = readLine()

    //Verification au moins 8 caractères  
    while (nouveauCodePin.length < 8) {
      println("Votre code PIN doit contenir au moins 8 caractères. ")
      print("Veuillez introduire un nouveau code PIN: ")
      nouveauCodePin = readLine()
    }

    codespin(id) = nouveauCodePin
    println("Votre code PIN a été mis à jour.")
    
  }
  //fin méthode changement de Pin
    
  def main(args: Array[String]): Unit = {
    
    // code devient codespin
        var nbclients = 100
        var codespin = Array.fill(nbclients)("INTRO1234")
        var codeUserInput = " "
        var codeEssai = 2
        
    
    // montant devient comptes
        var comptes = Array.fill(nbclients)(1200.0)
        var choix1 = 0
        var id = 0

        while ((choix1 !=5) && (codeUserInput != codespin(id))){
          print("Saisissez votre code identifiant > ")
          id = readInt()

          if((id < 0) || (id >= nbclients)){
            println("Cet identifiant n’est pas valable.")
            choix1 = 5
          } else {
            print("Saissisez votre code pin > ")
            codeUserInput = readLine()

            // Pin security check
              while ((codeUserInput != codespin(id)) && (codeEssai != 0)) {
                println("Code pin erroné, il vous reste " + codeEssai + " tentatives > ")
                print("Saissisez votre code pin > ")
                codeUserInput = readLine()
                codeEssai -= 1
                if (codeEssai == 0) {
                  println("Trop d'erreurs, abandon de l'idéntification")
                }
              }
            }

          
        }
        
    
 //début while Loop
        while (choix1 != 5) {
          println("Choisissez votre opération")
          println("1) Dépot")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code pin")
          println("5) Terminer")
          choix1 = readLine("Votre choix: ").toInt

          // 1. Depot de EUR ou CHF avec fonction depot
        
          if(choix1 == 1){
                depot(id,comptes)
                println(s"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte avec id $id est de : ${comptes(id)}")
          }
       
          // 3. operation de consultation sans fonction
       
          if (choix1 == 3){
              println(s"Le montant disponible sur votre compte avec id $id est de : ${comptes(id)}")
          }
            
          // 2. Opération de retrait avec fonction retrait
        
          if (choix1 == 2) {
              retrait(id, comptes)
          }

          // 4. changement de pin code selon ID

          if (choix1 == 4) {
              changepin(id, codespin)
              println(s"Votre code PIN avec id $id est : ${codespin(id)}")
          }
          
        } // Fin de While loop
    
        //5. Fin des opérations
        println("Fin des opérations, n'oubliez de récupérer votre carte")
  }
}
