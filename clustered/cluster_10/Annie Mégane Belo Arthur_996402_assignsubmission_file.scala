//Assignment: Annie Mégane Belo Arthur_996402_assignsubmission_file

import scala.io.StdIn._
object Main {

  def depot(id: Int, Comptes: Array[Double]): Unit = {
    var devise = 2
    var depot : Double = 0.0

    println("Indiquez la devise du dépôt.")
    devise = readLine("1. CHF; 2. EUR > ").toInt
    while (devise != 1 && devise != 2) {
      println("Indiquez la devise du dépôt.")
      devise = readLine("1. CHF; 2. EUR > ").toInt
    }
    // Si choix devise = 1,  Saisie du montant à déposer
    if (devise == 1) {
      depot = readLine("Indiquez le montant du dépôt:").toInt
      while (depot % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
        depot = readLine("Indiquez le montant du dépôt").toInt
      }
      // Mise à jour du compte en banque
      Comptes(id) = Comptes(id) + depot
    }
    // Choix de la devise, si choix = 2
    if (devise == 2){
      depot = readLine("Indiquez le montant du dépôt:").toInt
      while (depot % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
        depot = readLine("Indiquez le montant du dépôt").toInt
      }
      depot = depot * 0.95
      Comptes(id) = Comptes(id) + depot
      // Mise à jour du compte en banque
    }
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur le compte " + id + " est de : %.2f \n ", Comptes(id))
  }
  def retrait(id: Int, Comptes: Array[Double]): Unit = {
    var devise = 2
    var retrait : Int = 0
    var coupure : Int = 0
    var reste : Int = retrait
    var nbbillet = 0
    var ok : String = "o"
    var billets500 : Int = 0
    var billets200 : Int = 0
    var billets100 : Int = 0
    var billets50 : Int = 0
    var billets20 : Int = 0
    var billets10 : Int = 0

    // Choix de la devise
    println("Indiquez la devise du retrait ")
    devise = readLine("1. CHF; 2. EUR > ").toInt
    while (devise != 1 && devise != 2) {
      println("Indiquez la devise du retrait")
      devise = readLine("1. CHF; 2. EUR > ").toInt
    }
    // Si choix devise = 1
    if (devise == 1) {
      retrait = readLine("Indiquez le montant du retrait > ").toInt
      while (retrait % 10 != 0 || retrait > 0.1*Comptes(id)) {
        if (retrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        }
        else if (retrait > Comptes(id)*0.1) {
          println("Votre plafond de retrait autorisé est de: " + Comptes(id)*0.1 + "CHF")
        }
        retrait = readLine("Indiquez le montant du retrait > ").toInt
      }
      Comptes(id) = Comptes(id) - retrait
    } else {
      retrait = readLine("Indiquez le montant du retrait > ").toInt
      while (retrait % 10 != 0 || retrait > 0.1*Comptes(id)*0.95) {
        if (retrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        }
        else if (retrait > 0.95*Comptes(id)*0.1) {
          println("Votre plafond de retrait autorisé est de: " + Comptes(id)*0.1 * 0.95+ "EUR")
        }
        retrait = readLine("Indiquez le montant du retrait > ").toInt
      }
      Comptes(id) = Comptes(id) - retrait * 1.05
    }
    // Choix 1) Grosses coupures 2) petites coupures
    if ( devise == 1) {
      if(retrait > 100){
        println("1) grosse coupure; 2) petite coupure")
        coupure = readLine("Entrez votre choix de coupure > ").toInt
      } else {
        coupure = 2
      }
    } else {
      coupure = 2
    }
    // Pour retrait CHF  Grosses coupures
    if(devise == 1) {
      if (coupure == 1) {
        if (retrait >= 500){
          println( "Il vous reste: " + retrait + "CHF")
          billets500 = retrait / 500
          println("Vous pouvez obtenir au maximum " + retrait / 500 + " billets de 500CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 500 * billets500
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets500){
              println("Vous pouvez obtenir au maximum " + retrait / 500 + " billets de 500CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets500
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 500 * nbbillet
          }
        }
        if (retrait >= 200){
          println( "Il vous reste: " + retrait + "CHF")
          billets200 = retrait / 200
          println("Vous pouvez obtenir au maximum " + retrait / 200 + " billets de 200CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 200 * billets200
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets200){
              println("Vous pouvez obtenir au maximum " + retrait / 200 + " billets de 200CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets200
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 200 * nbbillet
          }
        }
        if (retrait >= 100){
          println( "Il vous reste: " + retrait + "CHF")
          billets100 = retrait / 100
          println("Vous pouvez obtenir au maximum " + retrait / 100 + " billets de 100CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 100 * billets100
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets100){
              println("Vous pouvez obtenir au maximum " + retrait / 100 + " billets de 100CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets100
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 100 * nbbillet
          }
        }
        if (retrait >= 50){
          println( "Il vous reste: " + retrait + "CHF")
          billets50 = retrait / 50
          println("Vous pouvez obtenir au maximum " + retrait / 50 + " billets de 50CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 50 * billets50
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets50){
              println("Vous pouvez obtenir au maximum " + retrait / 50 + " billets de 50CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets50
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 50 * nbbillet
          }
        }
        if (retrait >= 20){
          println( "Il vous reste: " + retrait + "CHF")
          billets20 = retrait / 20
          println("Vous pouvez obtenir au maximum " + retrait / 20 + " billets de 20CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 20 * billets20
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets20){
              println("Vous pouvez obtenir au maximum " + retrait / 20 + " billets de 20CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets20
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 20 * nbbillet
          }
        }
        if (retrait >= 10){
          println( "Il vous reste: " + retrait + "CHF")
          billets10 = retrait / 10
          retrait = retrait - 10 * billets10
        }
      }
      // Pour retrait CHF Petites coupure
      else {
        if (retrait >= 100){
          println( "Il vous reste: " + retrait + "CHF")
          billets100 = retrait / 100
          println("Vous pouvez obtenir au maximum " + retrait / 100 + " billets de 100CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 100 * billets100
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets100){
              println("Vous pouvez obtenir au maximum " + retrait / 100 + " billets de 100CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets100
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 100 * nbbillet
          }
        }
        if (retrait >= 50){
          println( "Il vous reste: " + retrait + "CHF")
          billets50 = retrait / 50
          println("Vous pouvez obtenir au maximum " + retrait / 50 + " billets de 50CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 50 * billets50
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets50){
              println("Vous pouvez obtenir au maximum " + retrait / 50 + " billets de 50CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets50
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 50 * nbbillet
          }
        }
        if (retrait >= 20){
          println( "Il vous reste: " + retrait + "CHF")
          billets20 = retrait / 20
          println("Vous pouvez obtenir au maximum " + retrait / 20 + " billets de 20CHF")
          ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
          if ( ok == "o"){
            retrait = retrait - 20 * billets20
          } else {
            nbbillet = ok.toInt
            while(nbbillet > billets20){
              println("Vous pouvez obtenir au maximum " + retrait / 20 + " billets de 20CHF")
              ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
              if(ok == "o"){
                nbbillet = billets20
              } else {
                nbbillet = ok.toInt
              }
            }
            retrait = retrait - 20 * nbbillet
          }
        }
        if (retrait >= 10){
          println( "Il vous reste: " + retrait + "CHF")
          billets10 = retrait / 10
          retrait = retrait - 10 * billets10
        }
      }
      if (billets500 > 0){
        println(billets500 + " billet(s) de 500 CHF")
      }
      if (billets200 > 0){
        println(billets200 + " billet(s) de 200 CHF")
      }
      if (billets100 > 0){
        println(billets100 + " billet(s) de 100 CHF")
      }
      if (billets50 > 0){
        println(billets50 + " billet(s) de 50 CHF")
      }
      if (billets20 > 0){
        println(billets20 + " billet(s) de 20 CHF")
      }
      if (billets10 > 0){
        println(billets10 + " billet(s) de 10 CHF")
      }
    } else {
      if (retrait >= 100){
        println( "Il vous reste: " + retrait + "EUR")
        billets100 = retrait / 100
        println("Vous pouvez obtenir au maximum " + retrait / 100 + " billets de 100EUR")
        ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
        if ( ok == "o"){
          retrait = retrait - 100 * billets100
        } else {
          nbbillet = ok.toInt
          while(nbbillet > billets100){
            println("Vous pouvez obtenir au maximum " + retrait / 100 + " billets de 100EUR")
            ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
            if(ok == "o"){
              nbbillet = billets100
            } else {
              nbbillet = ok.toInt
            }
          }
          retrait = retrait - 100 * nbbillet
        }
      }
      if (retrait >= 50){
        println( "Il vous reste: " + retrait + "EUR")
        billets50 = retrait / 50
        println("Vous pouvez obtenir au maximum " + retrait / 50 + " billets de 50EUR")
        ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
        if ( ok == "o"){
          retrait = retrait - 50 * billets50
        } else {
          nbbillet = ok.toInt
          while(nbbillet > billets50){
            println("Vous pouvez obtenir au maximum " + retrait / 50 + " billets de 50EUr")
            ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
            if(ok == "o"){
              nbbillet = billets50
            } else {
              nbbillet = ok.toInt
            }
          }
          retrait = retrait - 50 * nbbillet
        }
      }
      if (retrait >= 20){
        println( "Il vous reste: " + retrait + "EUR")
        billets20 = retrait / 20
        println("Vous pouvez obtenir au maximum " + retrait / 20 + " billets de 20EUr")
        ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
        if ( ok == "o"){
          retrait = retrait - 20 * billets20
        } else {
          nbbillet = ok.toInt
          while(nbbillet > billets20){
            println("Vous pouvez obtenir au maximum " + retrait / 20 + " billets de 20EUR")
            ok = readLine("Tapez o ou une autre valeur inférieure à celle proposée: ")
            if(ok == "o"){
              nbbillet = billets20
            } else {
              nbbillet = ok.toInt
            }
          }
          retrait = retrait - 20 * nbbillet
        }
      }
      if (retrait >= 10){
        println( "Il vous reste: " + retrait + "EUR")
        billets10 = retrait / 10
        retrait = retrait - 10 * billets10
      }

      if (billets100 > 0){
        println(billets100 + " billet(s) de 100 EUR")
      }
      if (billets50 > 0){
        println(billets50 + " billet(s) de 50 EUR")
      }
      if (billets20 > 0){
        println(billets20 + " billet(s) de 20 EUR")
      }
      if (billets10 > 0){
        println(billets10 + " billet(s) de 10 EUR")
      }
    }
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur le compte : " + id + " est de : %.2f \n ", Comptes(id))

  }
  def changepin(id: Int, Codespin: Array[String]): Unit = {
    var nouveaupin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    while(nouveaupin.length < 8){
      println("Votre code pin ne contient pas au moins 8 caractères ")
      nouveaupin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    }
    Codespin(id) = nouveaupin
    println("Le nouveau code pin du compte "+ id + " est : " + nouveaupin)
  }

  def main(args: Array[String]): Unit = {
    var choix = 0 // Choix opération
    var id = -1  // Choix de l'identifiant
    var nombreclients = 100
    val Comptes = Array.fill(nombreclients)(1200.0) // Montant dans sur les comptes
    val Codespin = Array.fill(nombreclients)("INTRO1234") // Code pin dans les comptes
    var pin = ""
    var tentative = 3



    while (choix != 100){
      id = readLine("Saisissez votre identifiant > ").toInt
      if (id < 0 || id > 99) {
        choix = 100 // valeur pour fermer le programme
        println("Cet identifiant n'est pas valable")
      } else {
        pin = readLine("Saisissez votre code pin > ")
        while (pin != Codespin(id) && tentative > 1) {
          tentative = tentative - 1
          println("Il vous reste " + tentative + " tentatives")
          pin = readLine("Saisissez à nouveau votre code pin > ")
        }
        if (tentative == 1 && pin != Codespin(id)) {
          println("Trop d'erreurs, abandon de l'identification")
          choix = 6 //Terminer le programme
          tentative = 3 //On recommence les tentatives pour la personne suivantes
        } else {
          tentative = 3 //On recommence les tentatives pour la personne suivantes
          println("Choisissez votre opération: \n 1. Dépôt\n 2. Retrait\n 3. Consultation du compte\n 4. Changement du pin\n 5. Terminer")
          choix = readLine("Entrez votre choix:").toInt
          while(choix == 1 || choix == 2 || choix == 3 || choix == 4 ) {
            if(choix == 1){
              depot(id, Comptes)
              println("Choisissez votre opération: \n 1. Dépôt\n 2. Retrait\n 3. Consultation du compte\n 4. Changement du pin\n 5. Terminer")
              choix = readLine("Entrez votre choix:").toInt
            } else if ( choix == 2){
              retrait(id, Comptes)
              println("Choisissez votre opération: \n 1. Dépôt\n 2. Retrait\n 3. Consultation du compte\n 4. Changement du pin\n 5. Terminer")
              choix = readLine("Entrez votre choix:").toInt
            } else if (choix == 3) {
              printf("Le montant disponible sur votre compte est de : %.2f \n ", Comptes(id))
              println("Choisissez votre opération: \n 1. Dépôt\n 2. Retrait\n 3. Consultation du compte\n 4. Changement du pin\n 5. Terminer")
              choix = readLine("Entrez votre choix:").toInt
            } else {
              changepin(id, Codespin)
              println("Choisissez votre opération: \n 1. Dépôt\n 2. Retrait\n 3. Consultation du compte\n 4. Changement du pin\n 5. Terminer")
              choix = readLine("Entrez votre choix:").toInt
            }
          }
          if (choix == 5) {
            println("Fin des opérations, n'oublié pas de récupérer votre carte.")
          }
        }
      }

    }

  }
}
