//Assignment: Yasser Saidali_996603_assignsubmission_file

    //libs
    import scala.io.StdIn.readInt
    import scala.io.StdIn.readLine
    object Main {
      //variables billets
      var billet500 = 0
      var billet200 = 0
      var billet100 = 0
      var billet50 = 0
      var billet20 = 0
      var billet10 = 0

      var billetBoucle: Int=0
      var MaxbilletBoucle: Int = 0
      var MontantBoucle: Int=0
      var a_retirer:Int = 0
      var multiple_de_10:Int = 2
      var devise_retirer:Int = 0
      var peut_sortir=false
      var vari_plafond:Double = 0.0
      var ChaineDevise:String = ""
      def main(args: Array[String]): Unit = {

        var choix = 0 // choix de l'utilisateur

        var Pin = 0 

        var nbclients=100
        var changer_utilisateur:Boolean = false

        var tentatives = 3 // nombre de tentative pour le PIN

        var comptes= Array.fill(nbclients)(1200.00) //solde du compte bancaire du client

        var codespin = Array.fill(nbclients)("INTRO1234") // code de l'énoncé en val car la valeur ne change jamais

        var condition_de_sortie:Boolean = false
        while (!condition_de_sortie) {
          changer_utilisateur=false
          println("Saisissez votre code identifiant >")
          var code_id = readInt()
          if(code_id>nbclients-1 || code_id<0){
            println("Cet identifiant n’est pas valable. ")
            condition_de_sortie = true
          }
          else{
            if(Pin==0)
            {
              //si c'est la première fois que on arrive sur le programme, alors on doit demander le code pin du client
              var Pin_saisie: String = ""
              while(Pin_saisie!=codespin(code_id) && changer_utilisateur==false)
              {
                //tant que on saisit pas le bon code pin qui est INTRO1234 alors on le redemande
                print("Saisissez votre code pin >")
                Pin_saisie = readLine()
                if(Pin_saisie!=codespin(code_id))
                {
                  tentatives-=1  //on décrémente le nombre de tentatives de 1
                  var erreur_de_trop = (tentatives==0)
                  if(erreur_de_trop)
                  {
                    println("Trop d’erreurs, abandon de l’identification")
                    tentatives=3
                    changer_utilisateur=true
                  }
                  else
                  {
                    print("Code pin erroné, il vous reste ")
                    print(tentatives)
                    println(" tentatives >")
                  }
                }
                else
                {//Bon code PIN
                  Pin=1  //maintenant Pin vaut 1 comme ça on re rentre plus dans le if(Pin==0)
                }
              }

            }
            while(Pin==1)
            {
              do
              {
                println("Choisissez votre opération :")
                println("1) Dépôt")
                println("2) Retrait")
                println("3) Consultation du compte")
                println("4) Changement du code pin")
                println("5) Terminer")
                choix = readLine("Votre choix :").toInt

                if(choix==5)
                {
                  //fin des opérations 
                  println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
                  tentatives=3 
                  Pin=0
                }
              }while((choix<1)||(choix>5))

              /*
              SELECTION DES DIFFERENTS CHOIX
              */
              if(choix==1)
              {
                depot(code_id,comptes)
              }
              //RETRAIT
              else if(choix==2)
              {
                retrait(code_id,comptes)
              }
              else if(choix==3)
              {
                printf("Le montant disponible sur votre compte est de : %.2f", comptes(code_id))
                println(" CHF")
              }
              else if(choix==4)
              {
                changepin(code_id,codespin)
              }
            }
          }
        }
      }

      def depot(id : Int, comptes : Array[Double]) : Unit = 
      {
        //si on choisit 1 alors on demande le montant à déposer
        var a_deposer:Int = 0
        var devise_deposer:Int =0

        do
        {
          //demander la devise du depot tant que la reponse est ni 1 ni 2
          devise_deposer = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
        }while(devise_deposer!=1 && devise_deposer!=2)

        do
        {
          a_deposer = readLine("Indiquez le montant du dépôt >").toInt
          multiple_de_10 = a_deposer%10
          if(multiple_de_10!=0)
          {
            //controler que c'ets un multiple de 10
            println("Le montant doit être un multiple de 10")
          }
        }while(a_deposer<=0 || multiple_de_10!=0)

        //Ici, le montant est bon

        if(devise_deposer==1) {
          //depot en CHF
          comptes(id) = comptes(id) + a_deposer
        }
        else
        { //depot en EUR
          var conversion_depot = a_deposer * 0.95
          comptes(id) = comptes(id) +  conversion_depot //conversion
        }

        printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > %.2f" , comptes(id))
        println(" CHF")
        multiple_de_10=2
      }

      def changepin(id : Int, codespin : Array[String]) : Unit = 
      {
        var nouveauCodePin = ""
        nouveauCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
        var tailleduPin = nouveauCodePin.length
        while(tailleduPin<8){
          println("Votre code pin ne contient pas au moins 8 caractères")
          nouveauCodePin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
          tailleduPin = nouveauCodePin.length
        }
        println("Changement du code pin effectué avec succès.")
        codespin(id) = nouveauCodePin
      }

       def retrait(id : Int, comptes : Array[Double]) : Unit = 
      {
        billet500 = 0
        billet200 = 0
        billet100 = 0
        billet50 = 0
        billet20 = 0
        billet10 = 0

        billetBoucle=0
        MaxbilletBoucle = 0
        MontantBoucle=0
        a_retirer = 0


        vari_plafond = (comptes(id)/10)

        devise_retirer = 0

        //tant que la devise du retrait c'est 0 ou n'importe quelle autre chose que 1 ou 2
        while(devise_retirer!=1 && devise_retirer!=2){
          devise_retirer = readLine("Indiquez la devise :1 CHF, 2 : EUR > ").toInt
        }

        multiple_de_10=2
          while(a_retirer<=0 || multiple_de_10!=0 || vari_plafond<a_retirer)
        {
          a_retirer = readLine("Indiquez le montant du retrait >").toInt
          multiple_de_10 = a_retirer%10
          if (multiple_de_10!=0) {
            println("Le montant doit être un multiple de 10.")
          }

          if(vari_plafond<a_retirer){
            printf("Votre plafond de retrait autorisé est de : %.2f \n", vari_plafond)
          }
        }

        var a_retirer_debut = a_retirer
        var proposition_coupure:Int = 0

        if(devise_retirer==1){  
          //le choix de la devise a été 1 donc en franc suisse
          ChaineDevise = "CHF"
        }
        else
        {
          //en EURO
          ChaineDevise = "EUR"
        }
          if(a_retirer>=200 && devise_retirer==1)
          {
            do
            {
              proposition_coupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt

            }while(proposition_coupure!=1 && proposition_coupure!=2)
          }
          else
          {
            proposition_coupure = 2
          }

          var passage_boucle = 0 

          while (a_retirer>0){ 
            //tant qu'il reste de l'argent

           if(passage_boucle==0)
            {
              //Si c'est la première fois qu'on entre dans la boucle, il y a 2 cas à distinguer : 
              if(devise_retirer==1 && proposition_coupure==1)
              {
                //si on retire en CHF et que on veut des grosses coupures alors le premier billet de boucle est le billet de 500 et le montant est 500
                MontantBoucle = 500
                billetBoucle = billet500
              }
              else
              {
                //Par contre, si on est en EURO ou en petite coupure le premier billet de boucle est celui de 100 et pareil pour le montant
                MontantBoucle = 100
                billetBoucle = billet100
              }
            }
            else if(passage_boucle==1)
            {
              if(devise_retirer==1 && proposition_coupure==1)
              {
                //si on retire en CHF et que on veut des grosses coupures alors le deuxième billet de boucle est le billet de 200 et le montant est 200
                MontantBoucle = 200
                billetBoucle = billet200
              }
              else
              {
                //Par contre, si on est en EURO ou en petite coupure le deuième billet de boucle est celui de 100 et pareil pour le montant
                MontantBoucle = 50
                billetBoucle = billet50
              }
            }
            else if(passage_boucle==2)
            {
              if(devise_retirer==1 && proposition_coupure==1)
              {
                //si on retire en CHF et que on veut des grosses coupures alors le troisième billet de boucle est le billet de 100 et le montant est 100
                MontantBoucle = 100
                billetBoucle = billet100
              }
              else
              {
                //Par contre, si on est en EURO ou en petite coupure le troisième billet de boucle est celui de 20 et pareil pour le montant
                MontantBoucle = 20
                billetBoucle = billet20
              }
            }
            else if(passage_boucle==3)
            {
              if(devise_retirer==1 && proposition_coupure==1)
              {
                //si on retire en CHF et que on veut des grosses coupures alors le quartième billet de boucle est le billet de 50 et le montant est 50
                MontantBoucle = 50
                billetBoucle = billet50
              }
              else
              {
                //Par contre, si on est en EURO ou en petite coupure le quartième billet de boucle est celui de 10 et pareil pour le montant
                MontantBoucle = 10
                billetBoucle = billet10
              }
            }
            else if(passage_boucle==4)
            {
              //sinon ici c'est forcément que on est dans les grosses coupures en CHF car sinon le passage boucle s'arrête à 3
              MontantBoucle = 20
              billetBoucle = billet20
            }
            else if(passage_boucle==5)
            {
              MontantBoucle = 10
              billetBoucle = billet10
            }


          if (a_retirer >= MontantBoucle){
              //affichage du montant qu'il reste à distribuer selon la devise 
              print("Il reste ")
              print(a_retirer)
              print(" ")
              print(ChaineDevise)
              println(" à distribuer")

              //le maximum a retirer correspond à la somme qu'il reste avec la division entière (comme expliqué dans le sujet) avec le montant à diviser
            //par exemple si il reste 2400CHF à retirer : maxbilletboucle = 2400/500  = 4 donc le max de billet à retirer pour 2400 CHF est 4 
              MaxbilletBoucle = a_retirer/MontantBoucle


              print("Vous pouvez obtenir au maximum ")
              print(MaxbilletBoucle)
              print(" billet(s) de ")
              print(MontantBoucle)
              println(" " + ChaineDevise)

                if(MontantBoucle==10)
                {
                  //si on est sur des billets de 10, alors on laisse pas le choix, on les retire
                  billet10 = MaxbilletBoucle    //le nombre de billet de 10 devient le nombre max de billet de 10 qu'on peut avoir
                  //on soustrait le nombre max de billets de 10 qu'on peut avoir avec la somme a retirer
                  a_retirer = a_retirer - (MaxbilletBoucle*10)
                }
            else
            {
              var different_billet:String = "n"
              different_billet = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              if(different_billet=="o")
              {
                billetBoucle = a_retirer/MontantBoucle
              }
              else
              {
                while(different_billet!="o" && peut_sortir==false)
                {
                  //Si c'est différent de o, c'est que c'est une valeur numérique
                  var val_numerique:Int = 0
                  val_numerique = different_billet.toInt
                  while(val_numerique<0 || val_numerique>MaxbilletBoucle)
                  {
                    //tant que c'est une valeur <0 ou supérieur au nombre max
                    val_numerique = readLine("Vous avez réalisé une mauvaise saisie, veuillez entrer une valeur positive et inférieure au nombre " + MaxbilletBoucle + " > ").toInt
                  }

                  //ici, la valeur est bonne
                  peut_sortir=true
                  billetBoucle = val_numerique


                }

              }
              println("ici")
              //on remet en mémoire les valeurs des billets 
              if(passage_boucle==0)
              {
                if(devise_retirer==1 && proposition_coupure==1){
                  billet500 = billetBoucle}
                else{
                  billet100 = billetBoucle}
              }
              else if(passage_boucle==1)
              {
                if(devise_retirer==1 && proposition_coupure==1){
                  billet200 = billetBoucle}
                else{
                  billet50 = billetBoucle}
              }
              else if(passage_boucle==2)
              {
                if(devise_retirer==1 && proposition_coupure==1){
                  billet100 = billetBoucle}
                else{
                  billet20 = billetBoucle}
              }
              else if(passage_boucle==3)
              {
                if(devise_retirer==1 && proposition_coupure==1){
                  billet50 = billetBoucle}
                else{
                  billet10 = billetBoucle}
              }
              else if(passage_boucle==4){
                billet20 = billetBoucle}
              else if(passage_boucle==5){
                billet10 = billetBoucle}

              a_retirer -= (billetBoucle*MontantBoucle)
            }


           }
            passage_boucle = passage_boucle + 1  //on incrémente pour passer à l'autre billet
            peut_sortir=false
          }


            println("Veuillez retirer la somme demandée :")
            if (billet500>0){
              println(billet500 + " billet(s) de 500 " + ChaineDevise)
            }
            if (billet200>0){
              println(billet200 + " billet(s) de 200 " + ChaineDevise)
            }
            if (billet100>0){
              println(billet100 + " billet(s) de 100 " + ChaineDevise)
            }
            if (billet50>0){
              println(billet50 + " billet(s) de 50 " + ChaineDevise)
            }
            if (billet20>0){
              println(billet20 + " billet(s) de 20 " + ChaineDevise)
            }
            if (billet10>0){
              println(billet10 + " billet(s) de 10 " + ChaineDevise)
            }

        if(devise_retirer==1)
        {
          //CHF
          comptes(id) = comptes(id) - a_retirer_debut
        }
        else
        {
          //EUR
          comptes(id) = comptes(id) - (a_retirer_debut*0.95)
        }

        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de :  %.2f", comptes(id))
        println(" CHF")
      }

    }

