//Assignment: Alex Tim Deflorin_996469_assignsubmission_file

import io.StdIn._
object Main {

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
      var devise = 0
      var retrait = 0
      var coupure = 0
      var nb500 = 0
      var nb200 = 0
      var nb100 = 0
      var nb50 = 0
      var nb20 = 0
      var nb10 = 0
      var choixRetrait = ""
    do {
        println("Indiquez la devise du retrait : 1) CHF ; 2) EUR >")
        devise = readInt()
      } while (devise != 1 && devise != 2)
      do {
          if(((retrait % 10) != 0)){
            println("Le montant doit être un multiple de 10.")
          }
          if(retrait > (0.1*comptes(id))){
            println("Votre plafond de retrait autorisé est de : " + (0.1*comptes(id)))
          }
          println("Indiquez le montant du retrait >")
          retrait = readInt()
        if((retrait < 200) && (devise ==1)){
          coupure = 2
        }
      } while (((retrait % 10) != 0) || (retrait > (0.1*comptes(id))))
      if(devise == 1 && (retrait >= 200)){
        do {
          println("En 1) grosses coupures, 2) petites coupures >")
          coupure = readInt()
        } while ( coupure != 1 && coupure != 2)
      } 
    if(coupure == 1){
        if(retrait>=500){
          nb500 = (retrait/500) 
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb500 + " billet(s) de 500 CHF ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb500 = choixRetrait.toInt
            while(nb500 > retrait/500){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb500 = choixRetrait.toInt
            }
            retrait -= nb500 * 500
            comptes(id) -= nb500 * 500
          }else {
            retrait -= nb500 * 500 
            comptes(id) -= nb500 * 500
          }
        }
        if(retrait>=200){
          nb200 = (retrait/200) 
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb200 + " billet(s) de 200 CHF ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb200 = choixRetrait.toInt
            while(nb200 > retrait/200){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb200 = choixRetrait.toInt
            }
            retrait -= nb200 * 200
            comptes(id)-= nb200 * 200
          }else {
            retrait -= nb200 * 200 
            comptes(id) -= nb200 * 200
          }
        }
        if(retrait>=100){
          nb100 = (retrait/100) 
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb100 + " billet(s) de 100 CHF ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb100 = choixRetrait.toInt
            while(nb100 > retrait/100){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb100 = choixRetrait.toInt
            }
            retrait -= nb100 * 100
            comptes(id)-= nb100 * 100
          }else {
            retrait -= nb100 * 100 
            comptes(id)-= nb100 * 100
          }
        }
        if(retrait>=50){
          nb50 = (retrait/50) 
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb50 + " billet(s) de 50 CHF ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb50= choixRetrait.toInt
            while(nb50 > retrait/50){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb50 = choixRetrait.toInt
            }
            retrait -= nb50 * 50
            comptes(id)-= nb50 * 50
          }else {
            retrait -= nb50 * 50 
            comptes(id)-= nb50 * 50
          }
        }
        if(retrait>=20){
          nb20 = (retrait/20) 
          println("Il reste " + retrait + " CHF à distribuer")
          println("Vous pouvez obtenir au maximum " + nb20 + " billet(s) de 20 CHF ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb20= choixRetrait.toInt
            while(nb20 > retrait/20){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb20 = choixRetrait.toInt
            }
            retrait -= nb20 * 20
            comptes(id) -= nb20 * 20
          }else {
            retrait -= nb20 * 20 
            comptes(id) -= nb20 * 20
          } 
        }

        if(retrait>=10){
          nb10 = (retrait/10)
          println("Il reste " + retrait + " CHF à distribuer")
          println("Veuillez retirer la somme demandée :")
          if(nb500 >= 1) println(nb500 + " billet(s) de 500 CHF")
          if(nb200 >= 1) println(nb200 + " billet(s) de 200 CHF")
          if(nb100 >= 1) println(nb100 + " billet(s) de 100 CHF")
          if(nb50 >= 1) println(nb50 + " billet(s) de 50 CHF")
          println(nb10 + " billets de 10 CHF" )
          comptes(id)-= nb10 * 10
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de               CHF %.2f\n ", comptes(id))
        }

        if(retrait==0){
          println("Veuillez retirer la somme demandée :")
          if(nb500 >= 1) println(nb500 + " billet(s) de 500 CHF")
          if(nb200 >= 1) println(nb200 + " billet(s) de 200 CHF")
          if(nb100 >= 1) println(nb100 + " billet(s) de 100 CHF")
          if(nb50 >= 1) println(nb50 + " billet(s) de 50 CHF")
          comptes(id)-= nb10 * 10
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de               CHF %.2f\n ", comptes(id))

        }
        retrait = 0
        nb500 = 0
        nb200 = 0
        nb100 = 0
        nb50 = 0
        nb20 = 0
        nb10 = 0
        coupure = 0
    }
    if(coupure == 2){
      if(retrait>=100){
        nb100 = (retrait/100) 
        println("Il reste " + retrait + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb100 + " billet(s) de 100 CHF ")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixRetrait = readLine()
        if (choixRetrait != "o"){
          nb100 = choixRetrait.toInt
          while(nb100 > retrait/100){
            println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
            choixRetrait = readLine()
            nb100 = choixRetrait.toInt
          }
          retrait -= nb100 * 100
          comptes(id) -= nb100 * 100
        }else {
          retrait -= nb100 * 100 
          comptes(id) -= nb100 * 100
        }
      }
      if(retrait>=50){
        nb50 = (retrait/50) 
        println("Il reste " + retrait + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb50 + " billet(s) de 50 CHF ")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixRetrait = readLine()
        if (choixRetrait != "o"){
          nb50= choixRetrait.toInt
          while(nb50 > retrait/50){
            println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
            choixRetrait = readLine()
            nb50 = choixRetrait.toInt
          }
          retrait -= nb50 * 50
          comptes(id)-= nb50 * 50
        }else {
          retrait -= nb50 * 50 
          comptes(id)-= nb50 * 50
        }
      }
      if(retrait>=20){
        nb20 = (retrait/20) 
        println("Il reste " + retrait + " CHF à distribuer")
        println("Vous pouvez obtenir au maximum " + nb20 + " billet(s) de 20 CHF ")
        println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
          choixRetrait = readLine()
        if (choixRetrait != "o"){
          nb20= choixRetrait.toInt
          while(nb20 > retrait/20){
            println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
            choixRetrait = readLine()
            nb20 = choixRetrait.toInt
          }
          retrait -= nb20 * 20
          comptes(id)-= nb20 * 20
        }else {
          retrait -= nb20 * 20 
          comptes(id)-= nb20 * 20
        } 
      }

      if(retrait>=10){
        nb10 = (retrait/10)
        println("Il reste " + retrait + " CHF à distribuer")
        comptes(id) -= nb10*10
        println("Veuillez retirer la somme demandée :")
        if(nb500 >= 1) println(nb500 + " billet(s) de 500 CHF")
        if(nb200 >= 1) println(nb200 + " billet(s) de 200 CHF")
        if(nb100 >= 1) println(nb100 + " billet(s) de 100 CHF")
        if(nb50 >= 1) println(nb50 + " billet(s) de 50 CHF")
        if(nb20 >= 1) println(nb20 + " billet(s) de 20 CHF")
        if(nb10 >= 1) println(nb10 + " billets de 10 CHF" )
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de               CHF %.2f\n ", comptes(id))
      }

      if(retrait==0){
        println("Veuillez retirer la somme demandée :")
        if(nb500 >= 1) println(nb500 + " billet(s) de 500 CHF")
        if(nb200 >= 1) println(nb200 + " billet(s) de 200 CHF")
        if(nb100 >= 1) println(nb100 + " billet(s) de 100 CHF")
        if(nb50 >= 1) println(nb50 + " billet(s) de 50 CHF")
        if(nb20 >= 1) println(nb20 + " billet(s) de 20 CHF")
        if(nb10 >= 1) println(nb10 + " billet(s) de 10 CHF")
        comptes(id)-= nb10 * 10
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de               CHF %.2f\n ", comptes(id))

      }
      retrait = 0
      nb500 = 0
      nb200 = 0
      nb100 = 0
      nb50 = 0
      nb20 = 0
      nb10 = 0 
      coupure = 0
    }
    if(devise == 2){
        if(retrait>=100){
          nb100 = (retrait/100) 
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nb100 + " billet(s) de 100 EUR")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb100 = choixRetrait.toInt
            while(nb100 > retrait/100){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb100 = choixRetrait.toInt
            }
            retrait -= nb100 * 100
            comptes(id)-= nb100 * 100 * 0.95
          }else {
            retrait -= nb100 * 100 
            comptes(id) -= nb100 * 100 * 0.95
          }
        }
        if(retrait>=50){
          nb50 = (retrait/50) 
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nb50 + " billet(s) de 50 EUR ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb50= choixRetrait.toInt
            while(nb50 > retrait/50){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb50 = choixRetrait.toInt
            }
            retrait -= nb50 * 50
            comptes(id)-= nb50 * 50 * 0.95
          }else {
            retrait -= nb50 * 50 
            comptes(id) -= nb50 * 50 * 0.95
          }
        }
        if(retrait>=20){
          nb20 = (retrait/20) 
          println("Il reste " + retrait + " EUR à distribuer")
          println("Vous pouvez obtenir au maximum " + nb20 + " billet(s) de 20 EUR ")
          println("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            choixRetrait = readLine()
          if (choixRetrait != "o"){
            nb20= choixRetrait.toInt
            while(nb20 > retrait/20){
              println("Il est impossible d'avoir ce nombre de billets, saississez une valeur plus petite !")
              choixRetrait = readLine()
              nb20 = choixRetrait.toInt
            }
            retrait -= nb20 * 20
            comptes(id) -= nb20 * 20 * 0.95
          }else {
            retrait -= nb20 * 20 
            comptes(id)-= nb20 * 20 * 0.95
          } 
        }

        if(retrait>=10){
          nb10 = (retrait/10)
          println("Il reste " + retrait + " EUR à distribuer")
          comptes(id) -= nb10*10 * 0.95
          println("Veuillez retirer la somme demandée :")
          if(nb100 >= 1) println(nb100 + " billet(s) de 100 EUR")
          if(nb50 >= 1) println(nb50 + " billet(s) de 50 EUR")
          if(nb20 >= 1) println(nb20 + " billet(s) de 20 EUR")
          if(nb10 >= 1) println(nb10 + " billet(s) de 10 EUR")
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de               CHF %.2f\n ", comptes(id))
        }

        if(retrait==0){
          println("Veuillez retirer la somme demandée :")
          if(nb100 >= 1) println(nb100 + " billet(s) de 100 EUR")
          if(nb50 >= 1) println(nb50 + " billet(s) de 50 EUR")
          if(nb20 >= 1) println(nb20 + " billet(s) de 20 EUR")
          if(nb10 >= 1) println(nb10 + " billet(s) de 10 EUR")
          comptes(id) -= nb10 * 10 * 0.95
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de               CHF %.2f\n ", comptes(id))

        }
        retrait = 0
        nb500 = 0
        nb200 = 0
        nb100 = 0
        nb50 = 0
        nb20 = 0
        nb10 = 0     
        coupure = 0

      }

    }

    

    
  
 
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    var devise = readInt()
    println("Indiquez le montant du dépôt >")
    var depots = readInt()
    while ((depots % 10) != 0 ) { 
      println("Le montant doit être un multiple de 10")
      depots = readInt()
    }
    if (devise == 2){
      comptes(id) = comptes(id)+depots*0.95
    }else {
      comptes(id)= comptes(id) + depots
    }
  }
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var pin3 = ""
    do {
println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      pin3 = readLine()
      if(pin3.length < 8){
        println("Votre code pin ne contient pas au moins 8 caractères")
      }
      }while(pin3.length<8)
      codespin(id) = pin3    
    
    
  }

  def main(args: Array[String]): Unit = {
    var choix = 0
    val pin = "INTRO1234"
    var pin2 = ""
    var compteurOperation = 0
    var essaiePin = 0
    var montant = 1200.0
    var depots = 0
    val nbClients = 100
    var comptes: Array[Double] = Array.fill(nbClients)(1200.0)
    val codespin: Array[String] = Array.fill(nbClients)("INTRO1234")
    var identifiant = 0
    var testeidpin = 0

    while (choix!=6) {
      if(testeidpin == 0 ){
        println("Saisissez votre code identifiant >")
        identifiant = readInt()
        if (identifiant >= nbClients || identifiant < 0 ){
          println("Cet identifiant n’est pas valable.")
          testeidpin = 1
          choix = 6
        } else {
         testeidpin = 1 
        }
      }


      if(identifiant < nbClients && identifiant >= 0 ){
        compteurOperation = 0 
        if(testeidpin == 1){ 
        do {
          if(compteurOperation == 0) {
            println("Saisissez votre code pin >")
            pin2 = readLine()
            essaiePin += 1
            }
          
          if(pin2 != codespin(identifiant) && essaiePin <= 2){
          println("Code pin erroné, il vous reste " + (3-essaiePin) +  " tentatives              >")  
          }
          
      } while (pin2 != codespin(identifiant) && essaiePin <= 2)
      }

        
        if((essaiePin >= 3) && (pin2 != codespin(identifiant)) ){
        println("Trop d’erreurs, abandon de l’identification")
        testeidpin = 0
        essaiePin = 0
        }
        compteurOperation += 1
        if(pin2 == codespin(identifiant)){
          testeidpin = 2
        }
        }



      if (testeidpin == 2 ) {
        print("Choisissez votre opération : " + "\n" + "1) Dépôt" +  "\n" + "2) Retrait" + "\n" + "3) Consultation du compte" + "\n" + "4) Changement du code pin" + "\n"+ "5) Terminer" + "\n"            + "Votre choix         :")
      choix = readInt()
      if(choix == 1 && (pin2 == codespin(identifiant))){
        depot(identifiant, comptes)
      }
      if(choix == 2 && pin2 == codespin(identifiant)) {
        retrait(identifiant, comptes)
      }
      if(choix == 3 && (pin2 == codespin(identifiant))) {
        printf("Le montant disponible sur votre compte est de : %.2f\n ", comptes(identifiant))
        compteurOperation += 1
      }
      if(choix == 4){
        changepin(identifiant, codespin)
        testeidpin = 2
      }
      if(choix==5){
        compteurOperation = 0
        testeidpin = 0
        essaiePin = 0
      }

      }

    }



        
        }
      }



