//Assignment: Noé Nicolas Claude Chavanne_996437_assignsubmission_file

import io.StdIn._
object Main {
var nbclients = 100
var comptes = Array.fill(nbclients) (1200.0)
var id = 0
var codespin = Array.fill(nbclients) ("INTRO1234")
def depot(id : Int, comptes : Array[Double]) : Unit = {
  val devisedepot = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt 
  var devisedepotecrit = "abc"
  var montantdepot = readLine("Indiquez le montant du dépôt >").toInt
  while (montantdepot%10!=0) {
  println("Le montant doit être un multiple de 10.")
    montantdepot = readLine("Indiquez le montant du dépôt >").toInt
  }
  if (devisedepot==1){
    devisedepotecrit=" CHF."
    comptes(id) += montantdepot}

  else if (devisedepot==2){devisedepotecrit=" EUR."
  comptes(id) += montantdepot*0.95}
  printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f", comptes(id))
  println (" CHF.")
}
def retrait(id : Int, comptes : Array[Double]) : Unit = {
   //choix de la devise pour le retrait
      var deviseretrait = 0
      while ((deviseretrait!=1)&&(deviseretrait!=2)) {deviseretrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt}
      var deviseretraitecrit = "abc"
      if (deviseretrait==1){deviseretraitecrit="CHF"}
      else if (deviseretrait==2){deviseretraitecrit="EUR"}
    //indication du montant qui doit respecter 2 contraintes : être un mutiple de 10 et être inférieur ou égal à 10% du montant disponible sur le compte
      var montantretrait = readLine("Indiquez le montant du retrait >").toInt
      while (montantretrait%10!=0) {
      println("Le montant doit être un multiple de 10.")
      montantretrait = readLine("Indiquez le montant du retrait >").toInt
      }
      val montantretraitautorise = comptes(id)*0.1
      while (montantretrait>montantretraitautorise){
      println("Votre plafond de retrait autorisé est de : " + montantretraitautorise + " " + deviseretraitecrit + ".")
      montantretrait = readLine("Indiquez le montant du retrait >").toInt
      }
      //je répète ceci dans le cas où l'utilisateur se retrompe
      while (montantretrait%10!=0) {
      println("Le montant doit être un multiple de 10.")
      montantretrait = readLine("Indiquez le montant du retrait >").toInt
      }
      while (montantretrait>montantretraitautorise){
      println("Votre plafond de retrait autorisé est de : " + montantretraitautorise + " " + deviseretraitecrit + ".")
      montantretrait = readLine("Indiquez le montant du retrait >").toInt
          }
    //déclaration de toutes les variables utiles pour les calculs du retrait
      var billet500retire = "0"
      var billet200retire = "0"
      var billet100retire = "0"
      var billet50retire = "0"
      var billet20retire = "0"
      var billet500quonpeutretirer = 0
      var billet200quonpeutretirer = 0
      var billet100quonpeutretirer = 0
      var billet50quonpeutretirer = 0
      var billet20quonpeutretirer = 0
      var montantretire500 = 0
      var montantretire200 = 0
      var montantretire100 = 0
      var montantretire50 = 0
      var montantretire20 = 0
      var montantretire10 = 0
      var billets10final = 0
      var montantaretirer = montantretrait
      var choixdecoupure = 0
    //si l'utilisateur choisit de retirer un montant en supérieur ou égal à 200 CHF, il a le choix entre grosses ou petites coupures : choix qu'il n'a pas s'il retire un montant en EUR.
      if ((montantretrait >= 200)&&(deviseretrait==1)){
          while ((choixdecoupure!=1)&&(choixdecoupure!=2))
          choixdecoupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
          if (choixdecoupure==1) {
          if (montantaretirer >=500) {
              billet500quonpeutretirer = montantaretirer/500
              billet500retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet500quonpeutretirer + " billet(s) de 500 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
               while ((billet500retire!="o")&&(billet500retire.toInt >= billet500quonpeutretirer)){
               billet500retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet500quonpeutretirer + " billet(s) de 500 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
               }
              if (billet500retire=="o") {montantretire500 = billet500quonpeutretirer*500}
              else {montantretire500 = (billet500retire.toInt)*500} 
              montantaretirer -= montantretire500 
          }  
          if (montantaretirer>=200) {
              billet200quonpeutretirer = montantaretirer/200
              billet200retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet200quonpeutretirer + " billet(s) de 200 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while ((billet200retire!="o")&&(billet200retire.toInt >= billet200quonpeutretirer)){
              billet200retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet200quonpeutretirer + " billet(s) de 200 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
              if (billet200retire=="o") {montantretire200 = billet200quonpeutretirer*200
              }
              else {montantretire200 = (billet200retire.toInt)*200}
              montantaretirer -= montantretire200 }
              }}
          if (montantaretirer>=100){
              billet100quonpeutretirer = montantaretirer/100
              billet100retire = readLine("Il reste " + montantaretirer + " " + deviseretraitecrit + " à distribuer \n" + "Vous pouvez obtenir au maximum " + billet100quonpeutretirer + " billet(s) de 100 " + deviseretraitecrit + "\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while ((billet100retire!="o")&&(billet100retire.toInt >= billet100quonpeutretirer)){
              billet100retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet100quonpeutretirer + " billet(s) de 100 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
              if (billet100retire == "o") {montantretire100 = billet100quonpeutretirer*100}
              else {montantretire100 = (billet100retire.toInt)*100}
              montantaretirer -= montantretire100 }
          if (montantaretirer>=50) {
              billet50quonpeutretirer = montantaretirer/50
              billet50retire = readLine("Il reste "  + montantaretirer + " " + deviseretraitecrit + " à distribuer \n" + "Vous pouvez obtenir au maximum " + billet50quonpeutretirer + " billet(s) de 50 " + deviseretraitecrit + "\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while ((billet50retire!="o")&&(billet50retire.toInt >= billet50quonpeutretirer)){
              billet50retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet50quonpeutretirer + " billet(s) de 50 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
              if (billet50retire=="o") {montantretire50 = billet50quonpeutretirer*50}
              else {montantretire50 = (billet50retire.toInt)*50}
              montantaretirer -= montantretire50 } 
          if (montantaretirer>=20) {
              billet20quonpeutretirer = montantaretirer/20
              billet20retire = readLine("Il reste " + montantaretirer + " " + deviseretraitecrit + " à distribuer \n" + "Vous pouvez obtenir au maximum " + billet20quonpeutretirer + " billet(s) de 20 " + deviseretraitecrit + "\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
              while ((billet20retire!="o")&&(billet20retire.toInt >= billet20quonpeutretirer)){
              billet20retire = readLine("Il reste " + montantaretirer + " CHF à distribuer \n" + "Vous pouvez obtenir au maximum " + billet20quonpeutretirer + " billet(s) de 20 CHF\n" + "Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
              if (billet20retire=="o") {montantretire20 = billet20quonpeutretirer*20}
              else {montantretire20 = (billet20retire.toInt)*20}
              montantaretirer -= montantretire20 }
          if (montantaretirer<montantretrait) { 
          if (montantaretirer>=10){billets10final = montantaretirer/10}  
              println("Veuillez retirer la somme demandée :")
              if (billet500retire == "o") {println(s"$billet500quonpeutretirer billet(s) de 500 $deviseretraitecrit")}
              else if ((billet500retire !="o")&&(billet500retire > "0")) {println(s"$billet500retire billet(s) de 500 $deviseretraitecrit")}
              if (billet200retire == "o") {println(s"$billet200quonpeutretirer billet(s) de 200 $deviseretraitecrit")}
              else if ((billet200retire !="o")&&(billet200retire > "0")) {println(s"$billet200retire billet(s) de 200 $deviseretraitecrit")}
              if (billet100retire == "o") {println(s"$billet100quonpeutretirer billet(s) de 100 $deviseretraitecrit")}
              else if ((billet100retire !="o")&&(billet100retire > "0")) {println(s"$billet100retire billet(s) de 100 $deviseretraitecrit")}
              if (billet50retire == "o") {println(s"$billet50quonpeutretirer billet(s) de 50 $deviseretraitecrit")}
              else if ((billet50retire !="o")&&(billet50retire > "0")) {println(s"$billet50retire billet(s) de 50 $deviseretraitecrit")}
              if (billet20retire == "o") {println(s"$billet20quonpeutretirer billet(s) de 20 $deviseretraitecrit")}
              else if ((billet20retire !="o")&&(billet20retire > "0")) {println(s"$billet20retire billet(s) de 20 $deviseretraitecrit")}
              if (billets10final > 0) {println(s"$billets10final billet(s) de 10 $deviseretraitecrit")}}
  //situation où e/la client-e n’aurait pris aucune coupure avant d’arriver à la distribution de la coupure de 10 CHF ou 10 EUR, il/elle sera obligé de prendre la proposition faite par le programme sans pouvoir la modifier 
          else if (montantaretirer==montantretrait){
              println("Veuillez retirer la somme demandée :")
              if ((deviseretrait == 1)&&(choixdecoupure == 1)) {
              billet500quonpeutretirer = montantaretirer/500
              montantretire500 = 500*billet500quonpeutretirer
              montantaretirer -= montantretire500
              if (billet500quonpeutretirer>0) {println(s"$billet500quonpeutretirer billet(s) de 500 $deviseretraitecrit")}
              billet200quonpeutretirer = montantaretirer/200
              montantretire200 = 200*billet200quonpeutretirer
              montantaretirer -= montantretire200
              if (billet200quonpeutretirer>0) {println(s"$billet200quonpeutretirer billet(s) de 200 $deviseretraitecrit")}
        }
              billet100quonpeutretirer = montantaretirer/100
              montantretire100 = 100*billet100quonpeutretirer
              montantaretirer -= montantretire100
              if (billet100quonpeutretirer>0) {println(s"$billet100quonpeutretirer billet(s) de 100 $deviseretraitecrit")}
              billet50quonpeutretirer = montantaretirer/50
              montantretire50 = 50*billet50quonpeutretirer
              montantaretirer -= montantretire50
              if (billet50quonpeutretirer>0) {println(s"$billet50quonpeutretirer billet(s) de 50 $deviseretraitecrit")}
              billet20quonpeutretirer =  montantaretirer/20
              montantretire20 = 20*billet20quonpeutretirer
              montantaretirer -= montantretire20
              if (billet20quonpeutretirer>0) {println(s"$billet20quonpeutretirer billet(s) de 20 $deviseretraitecrit")}
              var billet10quonpeutretirer = montantaretirer/10
              montantretire10 = 10*billet10quonpeutretirer
              montantaretirer -= montantretire10
              if (billet10quonpeutretirer>0) {println(s"$billet10quonpeutretirer billet(s) de 10 $deviseretraitecrit")}
        }
          if (deviseretrait==1) {comptes(id) -= montantretrait}
          else if (deviseretrait==2) {comptes(id) -= (montantretrait*0.95) }
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f", comptes(id))
          println(" CHF.")

}
def changepin(id : Int, codespin : Array[String]) : Unit = {
var nouveaucodepin = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
while (nouveaucodepin.length < 8){
nouveaucodepin = readLine("Votre code pin ne contient pas au moins 8 caractères >") }
if (nouveaucodepin.length >= 8){
  codespin(id) = nouveaucodepin
}
}
 def main(args: Array[String]): Unit = {

  print ("Saisissez votre code identifiant > ")
  id = readInt()
  if (id >= nbclients) {
  println ("Cet identifiant n’est pas valable.")
  }
  else if (id < nbclients) {
  var nbtentatives = 0
  var tentativesrestantes = 0
  var codepin = readLine("Saisissez votre code pin >") 
  while ((nbtentatives < 2)&&(codepin != codespin(id))) {
  nbtentatives += 1 
  tentativesrestantes = 3 - nbtentatives
  print("Code pin erroné, il vous reste " + tentativesrestantes + " tentatives >")
  codepin = readLine()}
  if ((nbtentatives >= 2) && (codepin != codespin(id))) {
           println("Trop d’erreurs, abandon de l’identification")
          main(args)
  }
  else {
  val phrasechoixoperation ="Choisissez votre opération :\n" +"   1) Dépôt\n" + "   2) Retrait\n" + "   3) Consultation du compte\n" + "   4) Changement du code pin\n"  + "   5) Terminer\n" + "Votre choix :"  
  var choixoperation = readLine(phrasechoixoperation).toInt  
    while (choixoperation != 5) {
  if (choixoperation == 1) {
    depot(id, comptes)
    } else if (choixoperation == 2) {
    retrait(id, comptes) 
    } else if (choixoperation == 3) {
      printf("Le montant disponible sur votre compte est de : %.2f", comptes(id))
      println(" CHF.")
    } else if (choixoperation == 4) {
    changepin(id, codespin)
    } 
    choixoperation = readLine(phrasechoixoperation).toInt
    }
    if (choixoperation == 5) {
    println("Fin des opérations, n’oubliez pas de récupérer votre carte. ")
    main(args)
    }

  }}
}}
