//Assignment: Logeaine Ghiath M Kaki_996508_assignsubmission_file

import io.StdIn._

object Main {

  //Remplacement des variables du compte et du code pin
  var montantCompte = Array.fill(100)(1200.0)
  val codespin = Array.fill(100)("INTRO1234")
  var nbclients = 100
  var tentatives: Int = 3
  var codePinTentatives: String = ""
  var run = true
  val tauxConversionEURtoCHF = 0.95
  val tauxConversionCHFtoEUR = 1.05

 
def printMenu(): Unit = {
     println("Choisissez votre opération :")
     println("\t1) Dépôt")
     println("\t2) Retrait ")
     println("\t3) Consultation du compte")
     println("\t4) Changement du code pin")
     println("\t5) Terminer ")
  }


  def processAccount(numIdentifiant: Int): Unit = {   
    var processAccountRunning = true
    while (processAccountRunning) {
      printMenu()
      var choixClient = readLine("Votre choix : ").toInt
      choixClient match {
        case 1 => depot(numIdentifiant, montantCompte)
        case 2 => retrait(numIdentifiant, montantCompte)
        case 3 => consultationCompte(numIdentifiant, montantCompte)
        case 4 => changepin(numIdentifiant, codespin)
        case 5 => processAccountRunning = false
        case _ => println("Choix invalide")     
      }
    }
       println ("Fin des opérations, n’oubliez pas de récupérer votre carte.")
  }

  

  def main(args: Array[String]): Unit = {
     // Boucle 
    while (run) {
    
     //Numéro d'identifiant
          var numIdentifiant = readLine ("Saisissez votre code identifiant >").toInt
    if (numIdentifiant >= nbclients) {
          println("Cet identifiant n'est pas valable.")
          run = false
    } else {   
       tentatives = 3
    
     //Code pin
    while (tentatives>0) {
          val codePinTentatives = readLine("Saisissez votre code pin >") 

    // Après avoir saisis, le code pin est juste
    if (codePinTentatives == codespin(numIdentifiant)) {  
      processAccount(numIdentifiant)
      tentatives = 0
    } else {  
      
    // Après avoir saisis, le code pin est faux
        tentatives -= 1 
          println("Code pin erroné, il vous reste " + tentatives + " tentatives >")

    if (tentatives == 0) {
          println("Trop d’erreurs, abandon de l’identification")     
          }
        } 
      }//Fin while (a>0)
    }
  } //Fin boucle
}//FIN def main




 

     // 1) Opération de dépôt
   def depot(id : Int, comptes : Array[Double]) : Unit = {
         var deviseDepot = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
         var montantDepot = readLine("Indiquez le montant du dépôt >").toDouble
     if (montantDepot % 10 == 0) {
     if (deviseDepot == 2) {
         val montantConverti =  montantDepot * tauxConversionEURtoCHF 
                  comptes(id) += montantConverti
         println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de" + comptes(id) + "CHF")
      } else { 
                   comptes(id) +=  montantDepot
          println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de" + comptes(id) + "CHF")  
              }        
      } else {   
           println("Le montant doit être un multiple de 10")
    }       
 }//FIN def depot


def propositionCoupures (coupures: List[(Int, String)], montantRetrait: Int) : List[(Int, Int, String)] = {
         var listMatch: List[(Int, Int, String)] = List()
         var restant = montantRetrait
  for ((valeur, nom) <- coupures) {
         val nombreCoupures = restant / valeur
  if (nombreCoupures > 0) {
        listMatch = listMatch :+ (valeur, nombreCoupures, nom)
        restant -= nombreCoupures * valeur
      }
    }
  return listMatch
  } //FIN def propositionCoupures


def retraitBillet(id : Int, comptes : Array[Double], coupures: List[(Int, String)], montantRetrait: Int) : Unit = {
        var listMatch: List[(Int, Int, String)] = List()
        var restant = montantRetrait
  for ((valeur, nom) <- coupures) {
        println("Il reste " + restant + nom + " à distribuer ") 
        val nombreCoupures = restant / valeur
  if (nombreCoupures > 0) {
        println("Vous pouvez obtenir au maximum " + nombreCoupures + " billet(s) de " + valeur + nom )
        var choix = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
  if (choix == "o") {
          restant -= valeur * nombreCoupures
          listMatch = listMatch :+ (valeur, nombreCoupures, nom)          
  } else {
            var choixCoupure = choix.toInt
  if (choixCoupure == 0 && valeur == 10)  {   
              listMatch = propositionCoupures(coupures, montantRetrait)
            }
  else if (choixCoupure < nombreCoupures) {
              restant -= valeur * choixCoupure 
              listMatch = listMatch :+ (valeur, choixCoupure, nom)
         }
       }  
     }
   }//FIN for((valeur, nom) <- coupures)
  
              println ("Veuillez retirer la somme demandée :")
  for ((valeur, nbr, nom ) <- listMatch) {
              println(nbr + " billet(s) de " + valeur + nom)
  }
}//FIN def retraitBillet
  





     // 2) Opération de retrait
   def retrait(id : Int, comptes : Array[Double]) : Unit = {    
           var montantRetrait = 0
           var deviseRetrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt


  while (deviseRetrait != 1 && deviseRetrait != 2) {
         deviseRetrait = readLine("Indiquez la devise :1 CHF, 2 : EUR >").toInt
         }
  if(deviseRetrait == 1 || deviseRetrait == 2) {
          montantRetrait = readLine ("Indiquez le montant du retrait >").toInt
              println ("Devise retrait :" + deviseRetrait)
      if (deviseRetrait == 1) {
               while (montantRetrait % 10 != 0 || montantRetrait > comptes(id) * 0.1) {
          if (montantRetrait % 10 != 0) 
              println("Le montant doit être un multiple de 10.")
              else println("Votre plafond de retrait autorisé est de " + comptes(id) * 0.1+ "CHF")
                       montantRetrait = readLine("Indiquez le montant du retrait >").toInt
                    }   

            val grossesCoupures = List((500, "CHF"), (200, "CHF"), (100, "CHF"))
            val petitesCoupures = List((50, "CHF"), (20, "CHF"), (10, "CHF"))
            var coupures: List[(Int, String)] = List()

        // DeviseRetrait en CHF
             if(deviseRetrait == 1) {
                if (montantRetrait >= 200){
                   var choixCoupures = readLine ("En 1 grosses coupures, 2 petites coupures >").toInt
                        if (choixCoupures == 1) {   
                   coupures = grossesCoupures ++ petitesCoupures
         }               
       } else {            
                   coupures = petitesCoupures       
     }         
   }
      retraitBillet(id, comptes, coupures, montantRetrait)
             comptes(id) -= montantRetrait 
       }

       else if (deviseRetrait == 2) {
           println("Retrait en EUR")
       while (montantRetrait % 10 != 0 || montantRetrait > (comptes(id) * tauxConversionCHFtoEUR ) * 0.1) {
              if (montantRetrait % 10 != 0) 
                 println("Le montant doit être un multiple de 10.")

        else println("Votre plafond de retrait autorisé est de " +(comptes(id) * tauxConversionCHFtoEUR ) * 0.1+ "EUR")
             montantRetrait = readLine("Indiquez le montant du retrait >").toInt
       }   
            val coupures = List((100, "EUR"), (50, "EUR"), (10, "EUR"))
          retraitBillet(id, comptes, coupures, montantRetrait)
         comptes(id) -= montantRetrait *  tauxConversionEURtoCHF            
         }      
      }   
   }//FIN retrait




  
     // 3) Consultation du compte
def consultationCompte(id : Int, comptes : Array[Double]) : Unit = {
  printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
}


     
     // 4) Opération de changement de code pin
   def changepin(id : Int, codespin : Array[String]) : Unit = {
     var changeCodePin = ""
  do {
      changeCodePin = readLine ("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
  if (changeCodePin.length < 8) {
      println ("Votre code pin ne contient pas au moins 8 caractères")
    }
  } while(changeCodePin.length < 8)
     
     codespin(id) = changeCodePin   
   }
   
} //FIN Object Main
