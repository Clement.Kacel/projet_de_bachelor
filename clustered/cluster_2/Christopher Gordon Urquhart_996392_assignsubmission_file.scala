//Assignment: Christopher Gordon Urquhart_996392_assignsubmission_file

import io.StdIn._
object Main {
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    print("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
    var devise = readInt()
    print("\nIndiquez le montant du dépôt > ")
    var montant = readInt().toDouble
    while(montant %10 !=0){
      print("\nLe montant doit être un multiple de 10\n")
      montant = readInt()
    }
    if(devise==2){
      montant*=0.95
    }
    comptes(id)+=montant
    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n", comptes(id))
  }
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    print("\nIndiquez la devise : 1 CHF, 2 : EUR > ")
    var devise = readInt()
    while((devise !=1)&&(devise !=2)){
      print("\nIndiquez la devise : 1 CHF, 2 : EUR > ")
      devise = readInt()
    }
    print("\nIndiquez le montant du retrait > ")
    var montant = readInt().toDouble
    var retraitmax = comptes(id)*0.10
    if(devise==2){
      retraitmax*=1.05
    }
    while((montant %10 !=0)||(montant > retraitmax)){
      if(montant%10 != 0){
        print("\nLe montant doit être un multiple de 10\n")
        montant = readInt()
      }
      else if(montant > retraitmax){
        print("\nVotre plafond de retrait autorisé est de : " + retraitmax + "\n")
        montant = readInt()
      }
    }
    var coupures =2
    if((devise==1)&&(montant >= 200)){
      print("\nEn 1) grosses coupures, 2) petites coupures > ")
      coupures = readInt()
      while((coupures !=1)&&(coupures != 2)){
        print("\nEn 1) grosses coupures, 2) petites coupures > ")
        coupures = readInt()
      }
    }
    var billet500 = 0
    var billet200 = 0
    var billet100 = 0
    var billet50 = 0
    var billet20 = 0
    var billet10 = 0
    var copie = montant
    if((montant >= 500)&&(coupures==1)){
      var billets:Int=(montant/500).toInt
      print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 500 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      var choix = readLine()
      if(choix == "o"){
        billet500 = billets
      }
      else{
        billet500 = choix.toInt
      }
      montant -= billet500*500
    }
    if((montant >= 200)&&(coupures==1)){
      var billets:Int=(montant/200).toInt
      print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 200 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      var choix = readLine()
      if(choix == "o"){
        billet200 = billets
      }
      else{
        billet200 = choix.toInt
      }
      montant -= billet200*200
    }
    if(montant >= 100){
      var billets:Int=(montant/100).toInt
      if(devise == 1){
        print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 100 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      }
      if(devise == 2){
        print("\nIl reste " + montant + " EUR à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 100 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      }
      var choix = readLine()
      if(choix == "o"){
        billet100 = billets
      }
      else{
        billet100 = choix.toInt
      }
      montant -= billet100*100
    }
    if(montant >= 50){
      var billets:Int=(montant/50).toInt
      if(devise == 1){
        print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 50 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      }
      if(devise == 2){
        print("\nIl reste " + montant + " EUR à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 50 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      }
      var choix = readLine()
      if(choix == "o"){
        billet50 = billets
      }
      else{
        billet50 = choix.toInt
      }
      montant -= billet50*50
    }
    if(montant >= 20){
      var billets:Int=(montant/20).toInt
      if(devise == 1){
        print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 20 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      }
      if(devise == 2){
        print("\nIl reste " + montant + " EUR à distribuer\nVous pouvez obtenir au maximum " + billets + " billet(s) de 20 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      }
      var choix = readLine()
      if(choix == "o"){
        billet20 = billets
      }
      else{
        billet20 = choix.toInt
      }
      montant -= billet20*20
    }
    if(montant >= 10){
      var billets:Int=(montant/10).toInt
      billet10 = billets
    }
    print("\nVeuillez retirer la somme demandée : \n")
    if(billet500 != 0){
      print(billet500 + " billet(s) de 500 CHF\n")
    }
    if(billet200 != 0){
      print(billet200 + " billet(s) de 200 CHF\n")
    }
    if(billet100 != 0){
      if(devise == 1){
        print(billet100 + " billet(s) de 100 CHF\n")
      }
      else{
        print(billet100 + " billet(s) de 100 EUR\n")
      }
    }
    if(billet50 != 0){
      if(devise == 1){
        print(billet50 + " billet(s) de 50 CHF\n")
      }
      else{
        print(billet50 + " billet(s) de 50 EUR\n")
      }
    }
    if(billet20 != 0){
      if(devise == 1){
        print(billet20 + " billet(s) de 20 CHF\n")
      }
      else{
        print(billet20 + " billet(s) de 20 EUR\n")
      }
    }
    if(billet10 != 0){
      if(devise == 1){
        print(billet10 + " billet(s) de 10 CHF\n")
      }
      else{
        print(billet10 + " billet(s) de 10 EUR\n")
      }
    }
    if(devise ==2){
      copie *= 0.95
    }
    comptes(id) -= copie
    printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n", comptes(id))
  }
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    var autrecode = readLine()
    while(autrecode.length()<=7){
      println("Votre code pin doit contenir au moins 8 caractères")
      autrecode = readLine()
    }
    codespin(id)=autrecode
  }
  def main(args: Array[String]): Unit = {
    var id: Int = 5
    var nbclients=100
    val codespin = Array.fill[String](nbclients)("INTRO1234")
    val comptes = Array.fill[Double](nbclients)(1200.0)
    var lecode="ff4"
    while (true){
      var choix = 0
      while (codespin(id) != lecode){
        print("Saisissez votre code identifiant > ")
        id=readInt()
        if(id>=nbclients){
          println("Cet identifiant n'est pas valable.")
          System.exit(0)
        }
        print("\nSaisissez votre code pin > ")
        lecode = readLine()
        var erreur =3
        while((lecode != codespin(id))&&(0!=erreur)){
          erreur -=1
          if (erreur ==0){
            println("\nTrop d'erreurs, abandon de l'identification")
          }
          else{
            print("\nCode pin erroné, il vous reste " + erreur + " tentatives > ")
            lecode = readLine()
          }
        }
      }

      while(choix !=5){
        print("\nChoisissez votre opération :\n  1) Dépôt\n  2) Retrait\n  3) Consultation du compte\n  4) Changement du code pin\n  5) Terminer\nVotre choix : ")
        choix = readInt()
        if(choix == 4){
          changepin(id,codespin)
        }
        if(choix == 1){
          depot(id,comptes)
        }
        if(choix == 2){
          retrait(id,comptes)
        }
        if(choix == 3){
          printf("\nLe montant disponible sur votre compte est de : %.2f\n", comptes(id)) 
        }
        if(choix == 5){
          println("\nFin des opération, n'oubliez pas de récupérer votre carte.")
          lecode="gg6up"
        }
      }
    }
  }
}

