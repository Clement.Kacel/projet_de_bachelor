//Assignment: Jamila Assarikhi_996534_assignsubmission_file

import scala.io.StdIn._

object Main {
  def main(args: Array[String]): Unit = {
    // Déclaration des variables EURO et CHF
    val nbclients = 100
    var soldeCHF = Array.fill(nbclients)(1200.00) // Tableau pour les comptes
    var codePIN = Array.fill(nbclients)("INTRO1234") // Tableau pour les codes PIN

    // Variable pour indiquer si le code PIN a déjà été validé
    var codePINValide = false

    while (true) {
      // Réinitialiser le compteur de tentatives pour chaque nouvel identifiant
      var tentatives = 3

      // L'utilisateur doit s'identifier avec un identifiant valide
      var idClient = -1
      while (idClient < 0 || idClient >= nbclients) {
        idClient = readLine("Saisissez votre code identifiant : ").toInt
        if (idClient < 0 || idClient >= nbclients) {
          println("Cet identifiant n'est pas valable.")
        }
      }

      // Demander le code PIN
      var saisieCode = ""
      while (!codePINValide && tentatives > 0) {
        val saisieCode = readLine("Saisissez votre code PIN : ")
        tentatives -= 1

        if (saisieCode != codePIN(idClient)) {
          println(s"Code PIN erroné, il vous reste $tentatives tentatives.")
          if (tentatives == 0) {
            println("Trop d'erreurs, abandon de l’identification.")
            System.exit(0)
          }
        } else {
          codePINValide = true
        }
      }

      // Menu des opérations si le code PIN est correct
      if (codePINValide) {
        var choix = 0
        while (choix != 5) {
          println("Choisissez votre opération :")
          println("1) Dépôt")
          println("2) Retrait")
          println("3) Consultation du compte")
          println("4) Changement du code PIN")
          println("5) Terminer")
          choix = readLine("Votre choix : ").toInt

           // Effectuer l'opération sélectionnée par l'utilisateur
            if (choix == 1) {
              depot(idClient, soldeCHF)
            } else if (choix == 2) {
              retrait(idClient, soldeCHF)
            } else if (choix == 3) {
              consultation(idClient, soldeCHF)
            } else if (choix == 4) {
              changepin(idClient, codePIN)
            } else if (choix == 5) {
              println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
              codePINValide = false
            } else {
              println("Choix invalide.")
            }
          }
      }
    }
  }

  // 1. Opération de dépôt
  def depot(id: Int, comptes: Array[Double]): Unit = {
    var devise = 0
    do {
      // Sélection de la devise
      devise = readLine("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >").toInt
      if (devise != 1 && devise != 2) {
        println("Choix invalide. Veuillez sélectionner une devise valide.")
      }
    } while (devise != 1 && devise != 2)

    // Sélection du montant du dépôt (doit être un multiple de 10)
    var montantDepot = 0
    do {
      montantDepot = readLine("Indiquez le montant du dépôt : ").toInt
      if (montantDepot % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
      }
    } while (montantDepot % 10 != 0)

    // Conversion du montant en CHF si la devise est EUR
    if (devise == 2) {
      montantDepot = (montantDepot * 0.95).toInt
    }

    // Mise à jour du solde
    comptes(id) += montantDepot
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

    // Opération de retrait
    def retrait(id: Int, soldeCHF: Array[Double]): Unit = {
      // Sélection de la devise
      var devise = 0
      do {
        devise = readLine("Indiquez la devise : 1) CHF ; 2) EUR >").toInt
        if (devise != 1 && devise != 2) {
          println("Choix invalide. Veuillez sélectionner une devise valide.")
        }
      } while (devise != 1 && devise != 2)

      // Indiquer le montant du retrait
      var montantRetrait = 0

      // Vérification que le solde disponible est suffisant et que c'est un multiple de 10
      do {
        montantRetrait = readLine("Indiquez le montant du retrait :").toInt
        if (montantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        } else if (devise == 2 && montantRetrait > soldeCHF(id) * 0.1) {
          println("Votre plafond de retrait autorisé est de : " + soldeCHF(id) * 0.1 + " CHF")
        }
      } while (montantRetrait % 10 != 0 || (devise == 2 && montantRetrait > soldeCHF(id) * 0.1))

      // Répartition du montant en coupures pour CHF
      val grossesCoupures = Array(500, 200, 100, 50, 20, 10)
      val petitesCoupures = Array(100, 50, 20, 10)
      var choixCoupures = 0
      val grossesCoupuresAutorise = montantRetrait >= 200

      // Répartition du montant en coupures pour CHF
      if (devise == 1) {
        // Vérifier si le montant peut être réparti en grosses coupures
        val grossesCoupuresAutorise = montantRetrait >= 200

        // Si le montant peut être réparti en grosses coupures, demander le choix
        if (grossesCoupuresAutorise) {
          // Initialiser le montant restant à distribuer
          var montantRestant = montantRetrait

          // Demander le choix entre grosses et petites coupures
          do {
            choixCoupures = readLine("En 1) grosses coupures, 2) petites coupures >").toInt
            if (choixCoupures != 1 && choixCoupures != 2) {
              println("Choix invalide. Veuillez sélectionner 1) pour grosses coupures ou 2) pour petites coupures.")
            }
          } while (choixCoupures != 1 && choixCoupures != 2)

          // Utiliser les coupures sélectionnées
          val coupures = if (choixCoupures == 1) grossesCoupures else petitesCoupures

          // Parcourir les coupures disponibles en ordre décroissant
          for (coupure <- coupures) {
            // Calculer le nombre maximal possible pour cette coupure
            val nombreMaximal = montantRestant / coupure

            // Vérifier si la coupure est dispo
            if (nombreMaximal > 0) {
              // Afficher la proposition
              println("Il reste " + montantRestant + " CHF à distribuer")
              println("Vous pouvez obtenir au maximum " + nombreMaximal + " en billet(s) de " + coupure + " CHF >")

              // Demander la saisie de l'utilisateur
              var saisieCoupure = -1
              do {
                saisieCoupure = readLine("Tapez '0' pour ok ou une autre valeur inférieure à " + nombreMaximal + ">").toInt
              } while (saisieCoupure < 0 || saisieCoupure > nombreMaximal)

              // Mettre à jour le montant restant
              montantRestant -= saisieCoupure * coupure

              // Si la saisie est égale au nombre maximal, passer à la coupure suivante
              if (saisieCoupure > 0) {
                println("Vous avez choisi " + saisieCoupure + " de " + coupure + " CHF")
              }
            }
          }
          // Mise à jour du solde en CHF
          soldeCHF(id) -= montantRetrait
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", soldeCHF(id))

          // Afficher la demande finale
          println("Veuillez retirer la somme demandée :")
          for (coupure <- coupures) {
            val nombre = montantRetrait / coupure
            if (nombre > 0) {
              println(nombre + " billet(s) de " + coupure + " CHF")
              montantRetrait -= coupure
            }
          }
        } else {
          // Si le montant ne peut pas être réparti en grosses coupures, utiliser uniquement les petites coupures
          val coupures = petitesCoupures
          println("Il reste " + montantRetrait + " CHF à distribuer en petites coupures.")

          // Mise à jour du solde en CHF
          soldeCHF(id) -= montantRetrait
          printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", soldeCHF(id))

          // Afficher la demande finale
          println("Veuillez retirer la somme demandée :")
          for (coupure <- coupures) {
            val nombre = montantRetrait / coupure
            if (nombre > 0) {
              println(nombre + " billet(s) de " + coupure + " CHF")
              montantRetrait -= nombre * coupure
            }
          }
        }
      } else if (devise == 2) {
        // Répartition du montant en coupures pour EUR
        val coupures = Array(100, 50, 20, 10)
        var montantRestant = montantRetrait

        // Conversion du montant en CHF si la devise est EUR
        val montantRetraitCHF = (montantRetrait * 0.95).toInt
        soldeCHF(id) -= montantRetraitCHF
        printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", soldeCHF(id))

        // Afficher la demande finale pour EUR
        println("Il reste " + montantRetrait + " EURO à distribuer.")
        for (coupure <- coupures) {
          val nombre = montantRestant / coupure
          if (nombre > 0) {
            println(nombre + " billet(s) de " + coupure + " EUR")
            montantRestant -= nombre * coupure
          }
        }
      }
    }

  def changepin(id: Int, codePIN: Array[String]): Unit = {
    // Lire au clavier le nouveau code PIN
    val nouveauCodePIN = readLine("Saisissez votre nouveau code PIN (au moins 8 caractères) : ")

    // Vérifier la longueur du nouveau code PIN
    if (nouveauCodePIN.length >= 8) {
      // Mettre à jour le code PIN dans le tableau
      codePIN(id) = nouveauCodePIN
      println("Le code PIN a été mis à jour avec succès.")
    } else {
      println("Le nouveau code PIN doit contenir au moins 8 caractères. Opération annulée.")
    }
  }

  // 3. Opérations de consultation du compte 

  def consultation(id: Int, soldeCHF: Array[Double]): Unit = {
    printf("Le montant disponible sur votre compte est de : %.2f CHF\n", soldeCHF(id))
  }




}