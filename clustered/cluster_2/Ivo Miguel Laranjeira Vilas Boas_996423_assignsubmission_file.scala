//Assignment: Ivo Miguel Laranjeira Vilas Boas_996423_assignsubmission_file

import scala.io.StdIn.{readInt, readLine}

object Main {
  def main(args: Array[String]): Unit = {
    val nbClient = 100
    var comptes = Array.fill(nbClient)(1200.0)
    var codespin = Array.fill(nbClient)("INTRO1234")

    var choixU = 0
    var tentatives = 3
    var pinvalide = false
    var montantdepot = 0

    def identification(): Int = {
      println("Saisissez votre code identifiant >")
      val codeid = readLine().toInt
      if (codeid < 0 || codeid >= nbClient) {
        println("Cet identifiant n’est pas valable.")
        -1
      } else {
        var tentativesrestantes = 3
        var identree = codeid
        pinvalide = false

        while (tentativesrestantes > 0 && !pinvalide) {
          println("Saisissez votre code pin >")
          val pin = readLine()
          if (pin == codespin(identree)) {
            pinvalide = true
          } else {
            tentativesrestantes -= 1
            if (tentativesrestantes > 0) {
              println(s"Code pin erroné, il vous reste $tentativesrestantes tentatives >")
            } else {
              println("Trop d’erreurs, abandon de l’identification")
              identree = -1
            }
          }
        }
        tentatives = 3
        identree
      }
    }
    while (true) {
      var utilisateurchoisi = identification()
      while (utilisateurchoisi != -1) {
        println("\nChoisissez votre opération:\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code PIN\n5) Terminer")
        choixU = readLine().toInt
        if (choixU < 1 || choixU > 5) {
          println("Opération invalide")
        } else {
          if (!pinvalide && choixU != 5) {
            utilisateurchoisi = identification()
          }
          if (utilisateurchoisi != -1) {
            if (pinvalide) {
              choixU match {
                case 1 => depot(utilisateurchoisi, comptes)
                case 2 => retrait(utilisateurchoisi, comptes)
                case 3 => consultation(utilisateurchoisi)
                case 4 => changePin(utilisateurchoisi, codespin)
                case 5 =>
                  utilisateurchoisi = -1
                  println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
                case _ =>
              }
            }
          }
        }
      }
    }

    

   

    def depot(id: Int, comptes: Array[Double]): Unit = {
      var devise = 0
      while (devise != 1 && devise != 2) {
        println("Indiquez la devise du dépôt : \n1) CHF \n2) EUR")
        devise = readLine().toInt
        if (devise != 1 && devise != 2) {
          println("Opération invalide ")
        }
      }

      var choixvalide = false
      var montantadeposer = 0
      while (!choixvalide) {
        println("Indiquez le montant du dépôt >")
        montantadeposer = readLine().toInt
        if (montantadeposer % 10 == 0 && montantadeposer > 0) {
          choixvalide = true
        } else {
          println("Le montant du dépôt doit être un multiple de 10")
        }
      }

      if (devise == 1) {
        comptes(id) += montantadeposer
        println(s"Ton dépôt à été enregistrer. Le nouveau montant du compte est: ${comptes(id)} CHF")
      } else {
        comptes(id) += montantadeposer * 0.95
        println(s"Ton dépôt à été enregistrer. Le nouveau montant du compte est: ${comptes(id)} EUR")
      }

      
    }

    def retrait(id: Int, comptes: Array[Double]): Unit = {
      var devise = ""
      while (devise != "1" && devise != "2") {
        print("Indiquez la devise : 1) CHF, 2) EUR > \n")
        devise = readLine()
      }

      print("Indiquez le montant du retrait > \n")
      var montantretrait = readInt()
      while (montantretrait % 10 != 0) {
        print("Le montant du retrait doit être un multiple de 10. Indiquez le montant du retrait > \n")
        montantretrait = readInt()
      }

      val retraitautorise = (comptes(id) * 0.1).toInt

      while (montantretrait > retraitautorise && montantretrait % 10 != 0) {
        println(s"Votre plafond de retrait autorisé est de : $retraitautorise et le montant du retrait doit être un multiple de 10.\n")
        print("Indiquez le montant du retrait > \n")
        montantretrait = readInt()
      }

      var coupurechoix = ""
      if (devise == "1") {
        if (montantretrait < 200) {
          coupurechoix = "2"
        } else {
          while (coupurechoix != "1" && coupurechoix != "2") {
            print("En 1) grosses coupures, 2) petites coupures > \n")
            coupurechoix = readLine()
          }
        }
      }

      var coupuresdispo: Array[Int] = Array()
      if (devise == "1") {
        if (coupurechoix == "1") {
          coupuresdispo = Array(500, 200, 100, 50, 20, 10)
        } else {
          coupuresdispo = Array(100, 50, 20, 10)
        }
      } else {
        coupuresdispo = Array(100, 50, 20, 10)
      }

      var montantrestant = montantretrait
      var distribution: Array[(Int, Int)] = Array()

      for (valeurbillets <- coupuresdispo) {
        val maxcoupures = montantrestant / valeurbillets
        if (maxcoupures > 0) {
          var boucle = false
          var choixdistribution = ""
          var choixbillets = 0
          while (!boucle) {
            if (devise == "1") {
              println(s"Il reste $montantrestant CHF à distribuer")
            } else {
              println(s"Il reste $montantrestant EUR à distribuer")
            }
            if (devise == "1") {
              println(s"Vous pouvez obtenir au maximum $maxcoupures billet(s) de $valeurbillets CHF")
            } else {
              println(s"Vous pouvez obtenir au maximum $maxcoupures billet(s) de $valeurbillets EUR")
            }

            if (valeurbillets == 10) {
              if (devise == "1") {
                println("Vous devez prendre le maximum possible pour la coupure de 10 CHF.")
              } else {
                println("Vous devez prendre le maximum possible pour la coupure de 10 EUR.")
              }

              choixdistribution = "o"
            } else {
              print("Tapez o pour ok ou une autre valeur inférieure à celle proposée > \n")
              choixdistribution = readLine()
            }
            if (choixdistribution == "o") {
              boucle = true
              distribution = distribution :+ (valeurbillets, maxcoupures.toInt)
              montantrestant %= valeurbillets
            } else {
              choixbillets = choixdistribution.toInt
              if (choixbillets < maxcoupures) {
                boucle = true
                distribution = distribution :+ (valeurbillets, choixbillets)
                montantrestant -= valeurbillets * choixbillets
              }
            }
          }
        }
      }

      println("Veuillez retirer la somme demandée :\n")
      for ((denomination, count) <- distribution) {
        if (count > 0) {
          if (devise == "1") {
            println(s"$count billet(s) de $denomination CHF \n")
          } else {
            println(s"$count billet(s) de $denomination EUR \n")
          }

        }
      }

      if (devise == "1") {
        comptes(id) -= montantretrait
      } else if (devise == "2") {
        comptes(id) -= montantretrait * 0.95
      }

      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
    }

    

    def consultation(id: Int): Unit = {
      println(s"Le montant de votre compte est: ${comptes(id)} CHF")
    }

    def changePin(id: Int, codespin: Array[String]): Unit = {
      var choixvalide = false
      var newPin = ""
      while (!choixvalide) {
        println("Entrer votre nouveau code PIN:")
        newPin = readLine()
        if (newPin.length > 7) {
          choixvalide = true
        } else {
          println("Votre code pin ne contient pas au moins 8 caractères")
        }
      }
      codespin(id) = newPin
      println("Code pin changer avec succès.")
      
      
        
       
    }


  }
}



