//Assignment: Samir Bassel Trabelsi GONZALEZ_996544_assignsubmission_file

object Main {
  def depot(id : Int, comptes : Array[Double]) : Unit = { 
  import scala.io.StdIn.readLine
  var montant = 0.0
  var devise = "a"
  devise = readLine("\nIndiquez la devise du dépôt : CHF ou EUR >")
    while(devise != "CHF" && devise != "EUR"){
      devise = readLine("\nIndiquez la devise du dépôt : CHF ou EUR >")
    }
  if(devise == "CHF"){
    montant = readLine("Indiquez le montant du dépôt >").toDouble
    while(montant<10 || montant%10!=0){
      println("Le montant doit être un multiple de 10")
    montant = readLine("Indiquez le montant du dépôt >").toDouble
    }
    comptes(id)+= montant
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }
  if(devise == "EUR"){
    montant = readLine("Indiquez le montant du dépôt >").toDouble
    while((montant<10 || montant%10!=0)&& montant!=0 ){
      println("Le montant doit être un multiple de 10")
    montant = readLine("Indiquez le montant du dépôt >").toDouble
    }
    comptes(id)+= montant*0.95
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }
    }
  def retraitpc(id : Int, comptes : Array[Double], montant : Double, devise : String) : Unit = {
    import scala.io.StdIn.readLine
    var y = devise
    var reste = montant
    var choix = "a"
    var nbrdebilletsp = Array.fill(4)(-1)
    var nbrdebilletsg = Array.fill(6)(-1)
    var petitsbillets = Array[Int](100,50,20,10)
    while(reste>10){
      choix="b"
    var billets = petitsbillets.filter(x=> (reste/x).toInt >= 1)
    var billetsmax = billets.max
      for(x <- petitsbillets){
        if((reste/x).toInt < 1){
        var w =petitsbillets.indexOf(x)
        nbrdebilletsp(w) = 0
        }
      }
    var i = petitsbillets.indexOf(billetsmax)
     var n = (reste/(billetsmax)).toInt
      choix = readLine(s"Il reste $reste $y à distribuer\nVous pouvez obtenir au maximum $n billet(s) de $billetsmax $y\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      petitsbillets(i) = 0
      if(choix.toString =="o"){
      nbrdebilletsp(i) = n
    reste = reste -(billetsmax*(reste/(billetsmax)).toInt).toInt
      }
      else{
        if(choix.toInt < n)
          reste = reste-(billetsmax*(choix.toInt))
          nbrdebilletsp(i) = choix.toInt
      }
        if(reste == 10 || nbrdebilletsp(2)==0 || nbrdebilletsp(2)>0 && reste >= 10){
          nbrdebilletsp(3) = (reste/petitsbillets(3)).toInt
          reste = 0
          }
    }
    if(reste == 0){
      println("Veuillez retirer la somme demandée :")
      if(nbrdebilletsp(0)>0) println(s"${nbrdebilletsp(0)} billet(s) de 100 $y")
      if(nbrdebilletsp(1)>0) println(s"${nbrdebilletsp(1)} billet(s) de 50 $y")
      if(nbrdebilletsp(2)>0) println(s"${nbrdebilletsp(2)} billet(s) de 20 $y")
      if(nbrdebilletsp(3)>0) println(s"${nbrdebilletsp(3)} billet(s) de 10 $y")
      if(y == "EUR"){
        comptes(id)=comptes(id)-(montant*0.95)
      }
      if(y == "CHF"){
        comptes(id)=comptes(id)-montant
      }
      printf(s"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n",comptes(id))
    }
  }
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    import scala.io.StdIn.readLine
    var choix="b"
    var nbrdebilletsp = Array.fill(4)(-1)
    var nbrdebilletsg = Array.fill(6)(-1)
    var coupure = 0
    var montant = 0.0
    var devise = "a"
    var montantmax = comptes(id)*0.10
    var grosbillets = Array[Int](500,200,100,50,20,10)
    var petitsbillets = Array[Int](100,50,20,10)
    devise = readLine("\nIndiquez la devise du retrait : CHF ou EUR >")
      while(devise!="CHF" && devise !="EUR"){
        devise = readLine("\nIndiquez la devise du retrait : CHF ou EUR >")
      }
    if(devise=="CHF"){
      montant = readLine("Indiquez le montant du retrait >").toInt
      while(montant<10 || montant%10!=0 || montant>montantmax){
        if(montant>montantmax){println(s"Votre plafond de retrait autorisé est de : $montantmax CHF")}
        if(montant<10 || montant%10!=0){
          println("Le montant doit être un multiple de 10")
        }
      montant = readLine("Indiquez le montant du retrait >").toInt
      }
      if(montant>=200){
      do{coupure = readLine("En 1) grosses coupures, 2) petites coupures >").toInt}
      while(coupure !=1 && coupure !=2)
      if(coupure==1){
        while(montant>10){
          choix="b"
        var billets = grosbillets.filter(x=> (montant/x).toInt >= 1)
          for(x <- grosbillets){
            if((montant/x).toInt < 1){
            var w = grosbillets.indexOf(x)
            nbrdebilletsg(w) = 0
            }
          }
        var billetsmax = billets.max
        var i = grosbillets.indexOf(billetsmax)
         var n = (montant/(billetsmax)).toInt
          choix = readLine(s"Il reste $montant CHF à distribuer\nVous pouvez obtenir au maximum $n billet(s) de $billetsmax CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
          grosbillets(i) = 0
          if(choix.toString =="o"){
          nbrdebilletsg(i) = n
        montant = montant -(billetsmax*(montant/(billetsmax)).toInt).toInt
          }
          else{
            if(choix.toInt < n)
              montant = montant-(billetsmax*(choix.toInt))
              nbrdebilletsg(i) = choix.toInt
          }
          if(montant == 10 || nbrdebilletsg(4)==0 || nbrdebilletsg(4)>0 && montant>=10){
            nbrdebilletsg(5) = (montant/grosbillets(5)).toInt
            montant = 0
          }
        }
        if(montant == 0){
          println("Veuillez retirer la somme demandée :")
          if(nbrdebilletsg(0)>0) println(s"${nbrdebilletsg(0)} billet(s) de 500 CHF")
          if(nbrdebilletsg(1)>0) println(s"${nbrdebilletsg(1)} billet(s) de 200 CHF")
          if(nbrdebilletsg(2)>0) println(s"${nbrdebilletsg(2)} billet(s) de 100 CHF")
          if(nbrdebilletsg(3)>0) println(s"${nbrdebilletsg(3)} billet(s) de 50 CHF")
          if(nbrdebilletsg(4)>0) println(s"${nbrdebilletsg(4)} billet(s) de 20 CHF")
          if(nbrdebilletsg(5)>0) println(s"${nbrdebilletsg(5)} billet(s) de 10 CHF")
          comptes(id)=comptes(id)-montant
          printf(s"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n",comptes(id))
        }
      }
      if(coupure==2){
        retraitpc(id,comptes,montant,devise)
      }
    }
      if(montant<200 && montant>0){
        retraitpc(id,comptes,montant,devise)
      }
  }
    if(devise == "EUR"){
      montant = readLine("Indiquez le montant du retrait >").toInt
      while(montant<10 || montant%10!=0 || montant>montantmax){
        if(montant>montantmax){println(s"Votre plafond de retrait autorisé est de : $montantmax EUR")}
        if(montant<10 || montant%10!=0){
          println("Le montant doit être un multiple de 10")
        }
      montant = readLine("Indiquez le montant du retrait >").toInt
      }
    retraitpc(id,comptes,montant,devise)
    }
}
  def changepin(id : Int, codespin : Array[String]) : Unit ={
    import scala.io.StdIn.readLine
    
    var codepin = readLine("\nSaisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    while(codepin.length<8){
    codepin = readLine("Votre code pin ne contient pas au moins 8 caractères. Saisissez votre nouveau code pin >")
    }
    codespin(id) = codepin
  }
  def identification(id : Int, codespin : Array[String]) : Unit = {
    import scala.io.StdIn.readLine
    var code2 ="a"
      while(codespin(id)!=code2){
        if(id<0 || id>100){
          println("\n\n\nCet identifiant n’est pas valable.")
          System.exit (0)
          }
        code2 = readLine("Saisissez votre code pin >")
          if(codespin(id)!= code2){
         code2 = readLine("Code pin erroné, il vous reste 2 tentative(s) >")
            if(codespin(id)!= code2){
          code2 = readLine("Code pin erroné, il vous reste 1 tentative(s) >")
              if(codespin(id)!= code2){
                println("Trop d’erreurs, abandon de l’identification")
              }
            }
          }
        }
      }
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn.readLine
  var nbclients = 100
  var comptes = Array.fill(nbclients)(1200.0)
  var codespin = Array.fill(nbclients)("INTRO1234")
  var id = 1
    id = readLine("\nSaisissez votre code identifiant >").toInt
    identification(id,codespin)
    println("\nChoisissez votre opération :")
    println("  1) Dépôt")
    println("  2) Retrait")
    println("  3) Consultation du compte")
    println("  4) Changement du code pin")
    println("  5) Terminer")
   var menu = readLine("Votre choix :").toInt
  while (menu==1 || menu==2 || menu==3 || menu==4 || menu==5){
  if(menu==1){
    depot(id,comptes)
  }
  if(menu==2){
    retrait(id,comptes)
  }
  if(menu==3){
      printf("\nLe montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
          }
  if(menu==4){
    changepin(id,codespin)
  }
  while(menu==5){
    menu = 0
    id = readLine("\nSaisissez votre code identifiant >").toInt
    identification(id,codespin)
  }
    println("\nChoisissez votre opération :")
      println("  1) Dépôt")
      println("  2) Retrait")
      println("  3) Consultation du compte")
      println("  4) Changement du code pin")
      println("  5) Terminer")
     menu = readLine("Votre choix :").toInt
    }
  }
}