//Assignment: Tess Maria Aznavourian_996488_assignsubmission_file

import scala.io.StdIn.readLine
import scala.io.StdIn.readChar
import scala.compiletime.ops.string
import scala.compiletime.ops.int

object Main {
  var nbClients = 100
  var accounts: Array[Double] = Array.fill(nbClients)(1200.0)
  var codespin: Array[String] = Array.fill(nbClients)("INTRO1234")

  def displayMenu(): Unit = {
    println("1) Deposit")
    println("2) Withdrawal")
    println("3) Account consultation")
    println("4) Changing the pin code")
    println("5) Finish")
  }

  def validatePIN(id: Int): Boolean = {
    var attempts = 3
    while (attempts > 0) {
      val enteredPIN = readLine("Enter your PIN code: ")
      if (enteredPIN == codespin(id)) {
        return true
      } else {
        attempts -= 1
        println()
        println(s"Wrong pin code, you still have $attempts attempts.")
        if (attempts == 0) {
          println("Too many errors, abandonment of identification.")
          return false
        }
      }
    }
    false
  }

  def deposit(id: Int): Unit = {
    println("1) CHF")
    println("2) EUR")
    print("Your choice is : ")
    val currencyChoice = readLine()
    if (currencyChoice == "1" || currencyChoice == "2") {
      var depositAmount = readLine("Indicate the amount of the deposit > ").toInt
      if (depositAmount > 0) {
        if (depositAmount % 10 == 0) {
            if (currencyChoice == "2") {
            depositAmount = (0.95 * depositAmount).toInt
            }
            accounts(id) += depositAmount
            printf("Your deposit has been processed, the new amount available on your account is: %.2f CHF\n", accounts(id))
        } else {
            println("The amount must be a multiple of 10.")
        } 
        } else {
            println("The amount to deposit must be a positive integer.")
        }
        } else {
            println("Invalid currency choice.")
        }
  }

  def withdraw(id: Int): Unit = {

    println("1) CHF")
    println("2) EUR")
    print("Your choice is : ")
    var currencyChoice = readLine()

    while (currencyChoice != "1" && currencyChoice != "2") {
      print("Invalid choice. Please choose 1) CHF or 2) EUR : > ")
      currencyChoice = readLine()
    }

    var withdrawalAmount = 0
    var limit = (accounts(id) * 0.1).toInt

    print("Indicate the amount to withdraw : ")
    withdrawalAmount = readLine().toInt

    if (withdrawalAmount % 10 != 0) {
      println("The amount must be a multiple of 10.")
      println()
    } 

    else if (withdrawalAmount > limit) {
     println(s"Your authorized withdrawal limit is: $limit")
     println()
    }
     
      
    var remainingAmount = withdrawalAmount
    var euroDenominations = Array(100, 50, 20, 10)
    var chfDenominations = Array(500, 200, 100, 50, 20, 10)




    // The user chooses the size of the denomination
    var denominationChoice : String = ""
    if (currencyChoice == "1") {
      print("In 1) large denominations, 2) small denominations > ")
      denominationChoice = readLine()
      println()

      while (denominationChoice != "1" && denominationChoice != "2") {
        print("Invalid choice. Please choose 1) large denominations or 2) small denominations > ")
        denominationChoice = readLine()
        println()
      }

      while (denominationChoice == "2" && withdrawalAmount < 200) {
        print("Invalid choice. You cannot choose small denomination with a withdrawal amount < 200")
        print("Please choose 1) large denominations or 2) small denominations > ")
        denominationChoice = readLine()
        println()
      }
    }

    // arranging the array to work with based on the users choice
    var denominationToUse : Array[Int] = chfDenominations

    if (denominationChoice == "1" && currencyChoice == "1") {
      // Chf currency with big bills (unmodified array for chfDenomination)
      denominationToUse = chfDenominations
    } else if (denominationChoice == "2" && currencyChoice == "1") {
      // chf currency with small bills (modified array for chfDenomination)
      denominationToUse = denominationToUse.drop(2)
    } else if (currencyChoice == "2") {
      withdrawalAmount = (withdrawalAmount * 0.95).toInt
      // euro currency with big bills (only choice for euro currency)
      denominationToUse = euroDenominations
    } else {
      ()
    }
    
    var bills_to_withdraw_list : Array[Int] = Array()
    var count_of_bills_to_withdraw : Array[Int] = Array()

    // we will iterate on the array chfDenominations
    for (denomination <- denominationToUse) {
      var curr : String = ""
      
      if (currencyChoice == "1") {
        curr = "CHF"
      } 
      else {
        curr = "Euro"
      }

      val bill_count = remainingAmount / denomination
      if (bill_count > 0) {
        println(s"There is $remainingAmount $curr left to distribute.")
        println(s"You can get a maximum of $bill_count bill(s) of $denomination $curr.")
        print("Type 'o' for OK or any other value less than the proposed one > ")

        val billsToWithdraw : String = readLine()
        println()

        if (billsToWithdraw == "o") {
          remainingAmount -= (denomination * bill_count)
          bills_to_withdraw_list = bills_to_withdraw_list :+ denomination
          count_of_bills_to_withdraw = count_of_bills_to_withdraw :+ bill_count
          println()
        }

        else if (billsToWithdraw.toInt > 0 && billsToWithdraw.toInt <= bill_count) {
          remainingAmount -= (denomination * billsToWithdraw.toInt)
          bills_to_withdraw_list = bills_to_withdraw_list :+ denomination
          count_of_bills_to_withdraw = count_of_bills_to_withdraw :+ billsToWithdraw.toInt
          println()
        }

        else if (billsToWithdraw.toInt != 0) {
          println("Invalid Choice. you entered a number of bills to keep that is bigger than you're withdrawal allows.")
          println()
        }

      }
    }
      
    println("Please Withdraw the requested amount :")
    for (i <- 0 to bills_to_withdraw_list.length-1) {
      var x = bills_to_withdraw_list(i)
      var y = count_of_bills_to_withdraw(i) 
      if (currencyChoice == "1") {
        println(s"$y bill(s) of $x CHF")
      }
      else {
        println(s"$y bill(s) of $x Euros")
      }
    }

    accounts(id) -= withdrawalAmount
    var balance : Double = accounts(id)
    println(s"Your withdrawal has been processed, the new amount available on your account is: $balance")
    println()
  }

  def changepin(id: Int): Unit = {
      var newPin = readLine("Enter your new pin code (it must contain at least 8 characters): ")
      while (newPin.length < 8) {
      println("Your pin code does not contain at least 8 characters.")
      newPin = readLine("Enter your new pin code (it must contain at least 8 characters): ")
      }
      codespin(id) = newPin
      println("Pin code changed successfully.")

  }


  def main(args: Array[String]): Unit = {

    // continueFlag is used to check for user-entry erros, so that if some are found, continueFlag goes to false, and the program stops.
    var continueFlag = true

    while (continueFlag) {
      var id = readLine("Enter your login code: ").toInt
      if (id < 0 || id >= nbClients) {
        println("This identifier is not valid.")
      } else {
        if (validatePIN(id)) {
          var choice = ' '
          while (choice != '5') {
            println()
            displayMenu()
            println()
            print("Your choice is : ")
            choice = readChar()

            choice match {
              case '1' =>
                println() 
                deposit(id)
              case '2' =>
                println()
                withdraw(id)
              case '3' => 
                println()
                println(s"The amount available on your account is: ${accounts(id)}")
              case '4' =>
                println() 
                changepin(id)
              case '5' =>
                println()
                println("End of operations, don't forget to collect your card.")
                continueFlag = false
              case _ => 
                println()
                println("Invalid choice.")
            }
          }
        }
      }
    }
  }
}