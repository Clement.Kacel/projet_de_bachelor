//Assignment: Sami Azimzadeh Sadraee_996501_assignsubmission_file

import scala.io.StdIn._

object Main {
  /*méthode du dépot*/
  def depot(id : Int, comptes : Array[Double]): Unit = {
    var devise = readLine("Indiquez la devise :1 CHF, 2:EUR > ").toInt
    /*devise du dépot*/
    while ((devise != 1) && (devise != 2)) {
      devise = readLine("Indiquez la devise :1 CHF, 2:EUR > ").toInt
    }
    /*dépot*/
    var dépot = readLine("Indiquez le montant du dépot > ").toInt
    while (dépot < 10) dépot = readLine("Indiquez le montant du dépot > ").toInt
    while (dépot % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      dépot = readLine("Indiquez le montant du dépot > ").toInt
    }
    /*màj du compte*/
    var dépotDouble = dépot.toDouble
    if (devise == 2) dépotDouble *= 0.95
    comptes(id) += dépotDouble
    /*impression*/
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
  }
  
  /*méthode du retrait*/
  def retrait(id : Int, comptes : Array[Double]): Unit = {
    val billet10 = 10
    val billet20 = 20
    val billet50 = 50
    val billet100 = 100
    val billet200 = 200
    val billet500 = 500
    
    var tentative = 3
    var devise = 0
    var montantretrait = 1
    var resteretrait = 1
    var valbillet = 0
    var nbbillet = 0
    var nbbilletvoulu = "p"
    var nbretrait = 0
    
    var nbbillet10 = 0
    var nbbillet20 = 0
    var nbbillet50 = 0
    var nbbillet100 = 0
    var nbbillet200 = 0
    var nbbillet500 = 0

    /*devise du retrait*/
    devise = readLine("Indiquez la devise :1 CHF, 2:EUR > ").toInt
    while ((devise != 1) && (devise != 2)) {
      devise = readLine("Indiquez la devise :1 CHF, 2:EUR > ").toInt
    }
    var monnaie = ""
    if (devise == 1) monnaie = "CHF"
    else monnaie = "EUR"

    /*retrait*/
    montantretrait = readLine("Indiquez le montant du retrait > ").toInt
    var montantautorisé = comptes(id) * 0.1
    if (devise == 2) montantautorisé *= 1.05
    while (montantretrait < 10) {
      montantretrait = readLine("Indiquez le montant du retrait > ").toInt
    }
    while ((montantretrait > montantautorisé) || (montantretrait % 10 != 0)) {
      if (montantretrait > montantautorisé) println("Votre plafond de retrait autorisé est de : " + montantautorisé.toInt)
      if (montantretrait % 10 != 0) println("Le montant doit être un multiple de 10.")
      montantretrait = readLine("Indiquez le montant du retrait > ").toInt
    }

    /*scénarios de coupures*/
    var choixcoupure = 0
    if (devise == 2) choixcoupure = 2
    else if (devise == 1) {
      if (montantretrait >= 200) {
        while ((choixcoupure != 1) && (choixcoupure != 2)) {
          choixcoupure = readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
        }
      }
      else choixcoupure = 2
    }

    /*grosses coupures*/
    if (choixcoupure == 1) {
      resteretrait = montantretrait
      while ((resteretrait > 0) && (nbretrait < 6)) {
        nbretrait += 1
        if (nbretrait == 1) valbillet = billet500
        else if (nbretrait == 2) valbillet = billet200
        else if (nbretrait == 3) valbillet = billet100
        else if (nbretrait == 4) valbillet = billet50
        else if (nbretrait == 5) valbillet = billet20
        else if (nbretrait == 6) valbillet = billet10
        
        nbbillet = resteretrait / valbillet
        if (nbbillet != 0) {
          println("Il reste " + resteretrait + " " + monnaie + " à distribuer")
          println("Vous pouvez obtenir au maximum " + nbbillet + " billet(s) de " + valbillet + " " + monnaie)
          if (nbretrait < 6) {
            nbbilletvoulu = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            while ((nbbilletvoulu != "o") && (nbbilletvoulu.toInt > nbbillet)) {
              nbbilletvoulu = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (nbbilletvoulu != "o") nbbillet = nbbilletvoulu.toInt
          }
        }
        if (valbillet == billet500) nbbillet500 = nbbillet
        else if (valbillet == billet200) nbbillet200 = nbbillet
        else if (valbillet == billet100) nbbillet100 = nbbillet
        else if (valbillet == billet50) nbbillet50 = nbbillet
        else if (valbillet == billet20) nbbillet20 = nbbillet
        else if (valbillet == billet10) nbbillet10 = nbbillet
        resteretrait -= (nbbillet * valbillet)
      }
    }
    /*petites coupures*/
    else if (choixcoupure == 2){
      resteretrait = montantretrait
      while ((resteretrait > 0) && (nbretrait < 4)) {
        nbretrait += 1
        if (nbretrait == 1) valbillet = billet100
        else if (nbretrait == 2) valbillet = billet50
        else if (nbretrait == 3) valbillet = billet20
        else if (nbretrait == 4) valbillet = billet10

        nbbillet = resteretrait / valbillet
        if (nbbillet != 0) {
          println("Il reste " + resteretrait + " " + monnaie + " à distribuer")
          println("Vous pouvez obtenir au maximum " + nbbillet + " billet(s) de " + valbillet + " " + monnaie)
          if (nbretrait < 4) {
            nbbilletvoulu = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
          while ((nbbilletvoulu != "o") && (nbbilletvoulu.toInt > nbbillet)) {
              nbbilletvoulu = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
            }
            if (nbbilletvoulu != "o") nbbillet = nbbilletvoulu.toInt
          }
        }
        if (valbillet == billet100) nbbillet100 = nbbillet 
        else if (valbillet == billet50) nbbillet50 = nbbillet
        else if (valbillet == billet20) nbbillet20 = nbbillet
        else if (valbillet == billet10) nbbillet10 = nbbillet
        resteretrait -= (nbbillet * valbillet)
      }
    }

    /*impression*/
    println()
    println("Veuillez retirer la somme demandée :")
    if (nbbillet500 != 0) println(nbbillet500 + " billet(s) de 500 " + monnaie)
    if (nbbillet200 != 0) println(nbbillet200 + " billet(s) de 200 " + monnaie)
    if (nbbillet100 != 0) println(nbbillet100 + " billet(s) de 100 " + monnaie)
    if (nbbillet50 != 0) println(nbbillet50 + " billet(s) de 50 " + monnaie)
    if (nbbillet20 != 0) println(nbbillet20 + " billet(s) de 20 " + monnaie)
    if (nbbillet10 != 0) println(nbbillet10 + " billet(s) de 10 " + monnaie)

    /*màj du montant*/
    var retraitDouble = montantretrait.toDouble
    if (devise == 2) retraitDouble *= 0.95 
    comptes(id) -= retraitDouble
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n", comptes(id))
  }
  
  /*méthode changement de code pin*/
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var newcode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    while (newcode.size < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères")
      newcode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    }
    codespin(id) = newcode
  }

  /*méthode main*/
  def main(args: Array[String]): Unit = {
    var choix = 0
    var tentative = 3
    var code = "nul"
    val nbclients = 100
    val comptes = Array.fill(nbclients)(1200.00)
    val codespin = Array.fill(nbclients)("INTRO1234")
    var id = 0

    /*début du programme, (true) pour boucle infinie*/
    while (true) {
      tentative = 3
      choix = 0
      id = readLine("Saisissez votre code identifiant > ").toInt
      if (id > (nbclients - 1)) {
        println("Cet identifiant n'est pas valable")
        return  /*sort du programme*/
      }

      /*code pin*/
      code = readLine("Saisissez votre code pin > ")
      while ((code != codespin(id)) && (tentative != 0)) {
        tentative -= 1
        if (tentative > 0) {
          println ("Code pin erroné, il vous reste " + tentative + " tentatives >")
          code = readLine("Saisissez votre code pin > ")
        }
      }
      if (tentative == 0) println("Trop d'erreurs, abandon de l'identification")
      /*fin code pin*/

      /*boucle des choix pour un client*/
      while ((choix != 5) && (tentative != 0)) {
        println("")
        println("Choisissez votre opération :")
        println("   1) Dépôt")
        println("   2) Retrait")
        println("   3) Consultation du compte")
        println("   4) Changement du code pin")
        println("   5) Terminer")
        choix = readLine("Votre choix : ").toInt
        
        if (choix == 1) depot(id, comptes)
        else if (choix == 2) retrait(id, comptes)
        else if (choix == 3) printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))
        else if (choix == 4) changepin(id, codespin)
      }
      
      if (choix == 5) println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
    } /*while (true)*/
  }/*main*/
}
