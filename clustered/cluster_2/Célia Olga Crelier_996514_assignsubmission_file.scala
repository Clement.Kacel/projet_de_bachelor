//Assignment: Célia Olga Crelier_996514_assignsubmission_file

import io.StdIn._
object Main {
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    println("\nIndiquez la devise du dépôt: 1) CHF ; 2) EUR >")
    var devise = readInt()
    println("\nIndiquez le montant du dépôt >")
    var mdepot = readInt().toDouble
    while(mdepot %10 != 0){
      println("\nLe montant doit être un multiple de 10 >") 
      mdepot = readInt()
    }
    if(devise == 2){
      mdepot *= 0.95 
    }
    comptes(id) += mdepot
    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n", comptes(id))
  }
  def retrait (id : Int, comptes : Array[Double]) : Unit = {
    var devise = 0
    while((devise != 1) && (devise != 2)){
      println("\nIndiquez la devise : 1 CHF, 2 : EUR >") 
      devise = readInt()
    }
    var limite = 0.1 * comptes(id)
    if(devise == 2){
      limite *= 1.05
    }
    var mretrait :Double = 3
    if((mretrait %10 != 0) || (mretrait > limite )){
      println("\nIndiquez le montant du retrait >")
      mretrait = readInt().toDouble
      while((mretrait %10 != 0) || (mretrait > limite )){
        if(mretrait %10 != 0){
          println("\nLe montant doit être un multiple de 10 >")
          mretrait = readInt()
        }
        else if(mretrait > limite){
          printf("\nVotre plafond de retrait autorisé est de : %.2f\n", limite)
          mretrait = readInt()
        }
      }
    }
    var coupure = 2
    if((devise == 1)&&(mretrait >= 200)){
      println("\nEn 1) grosse coupure, 2) petites coupures")
      coupure = readInt() 
      while((coupure != 1) && (coupure != 2)){
       println("\nEn 1) grosse coupure, 2) petites coupures")
       coupure = readInt() 
      }
    }

    var copie = mretrait
    var billet500 : Int = 0
    var billet200 : Int = 0
    var billet100 : Int = 0
    var billet50 : Int = 0
    var billet20 : Int = 0
    var billet10 : Int = 0
    if((coupure == 1)&&(mretrait >= 500)){
      var billet : Int = (mretrait / 500).toInt
      println("\nIl reste " + mretrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 500 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      var billets = readLine()
      if(billets == "o"){
        billet500 = billet
        mretrait -= billet*500
      }
      else{
        billet500 = billets.toInt
        mretrait -= billets.toInt * 500
      }
    }
    if((coupure == 1)&&(mretrait >= 200)){
      var billet : Int = (mretrait / 200).toInt
      println("\nIl reste " + mretrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 200 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      var billets = readLine()
      if(billets == "o"){
        billet200 = billet
        mretrait -= billet*200
      }
      else{
        billet200 = billets.toInt
        mretrait -= billets.toInt * 200
      }
    }
    if(mretrait >= 100){
      var billet : Int = (mretrait / 100).toInt
      if(devise == 1){
        println("\nIl reste " + mretrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 100 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      }
      else{
        println("\nIl reste " + mretrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 100 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      }
      var billets = readLine()
      if(billets == "o"){
        billet100 = billet
        mretrait -= billet*100
      }
      else{
        billet100 = billets.toInt
        mretrait -= billets.toInt * 100
      }
    }
    if(mretrait >= 50){
      var billet : Int = (mretrait / 50).toInt
      if(devise == 1){
        println("\nIl reste " + mretrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 50 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      }
      else{
        println("\nIl reste " + mretrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 50 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      }
      var billets = readLine()
      if(billets == "o"){
        billet50 = billet
        mretrait -= billet*50
      }
      else{
        billet50 = billets.toInt
        mretrait -= billets.toInt * 50
      }
    }
    if(mretrait >= 20){
      var billet : Int = (mretrait / 20).toInt
      if(devise == 1){
        println("\nIl reste " + mretrait + " CHF à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 20 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      }
      else{
        println("\nIl reste " + mretrait + " EUR à distribuer\nVous pouvez obtenir au maximum " + billet + " billet(s) de 20 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée >")
      }
      var billets = readLine()
      if(billets == "o"){
        billet20 = billet
        mretrait -= billet*20
      }
      else{
        billet20 = billets.toInt
        mretrait -= billets.toInt * 20
      }
    }
    if(mretrait >= 10){
      var billet : Int = (mretrait / 10).toInt
      billet10 = billet
      mretrait -= billet*10
    }
    println("\nVeuillez retirer la somme demandée :")
    if(devise == 1){
      if(billet500 != 0){
        println(billet500 + " billet(s) de 500 CHF")
      }
      if(billet200 != 0){
        println(billet200 + " billet(s) de 200 CHF")
      }
      if(billet100 != 0){
        println(billet100 + " billet(s) de 100 CHF")
      }
      if(billet50 != 0){
        println(billet50 + " billet(s) de 50 CHF")
      }
      if(billet20 != 0){
        println(billet20 + " billet(s) de 20 CHF")
      }
      if(billet10 != 0){
        println(billet10 + " billet(s) de 10 CHF")
      }
    }
    if(devise == 2){
      if(billet100 != 0){
        println(billet100 + " billet(s) de 100 EUR")
      }
      if(billet50 != 0){
        println(billet50 + " billet(s) de 50 EUR")
      }
      if(billet20 != 0){
        println(billet20 + " billet(s) de 20 EUR")
      }
      if(billet10 != 0){
        println(billet10 + " billet(s) de 10 EUR")
      }
    }
    if(devise == 2){
      copie *= 0.95
    }
    comptes(id) -= copie
    printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n", comptes(id))
  }
  def changepin (id : Int, codespin : Array[String]) : Unit = {
    println("\nSaisissez votre nouveau code pin (il doit contenur au moins 8 caractères >") 
    var ecrit = readLine()
    while(ecrit.length() < 8){
      println("\nVotre code pin ne contient pas au moins 8 caractères")
      ecrit = readLine ()
    }
    codespin(id) = ecrit
  }
  def main(args: Array[String]): Unit = {
    var montant : Double = 1200.0 
    var nbclients = 100
    var id = 19 
    val comptes = Array.fill [Double] (nbclients) (1200.0)
    var pin = "123"
    val codespin = Array.fill [String] (nbclients) ("INTRO1234")
    var choix = 5
    while(true){
      if(choix == 5){
        pin = "123"
        while(pin != codespin(id)) {
          println("\nSaisissez votre code identifiant > ")
          id = readInt() 
          if(id >= nbclients){
            println("\nCet identifiant n'est pas valable.")
            System.exit(0)
          }
          println("\nSaisissez votre code pin >")
          pin = readLine()
          var nbtentatives = 2
          while((pin != codespin(id))&&(nbtentatives > 0)){
            println("\nCode pin éronné, il vous reste " + nbtentatives + " tentatives >") 
            pin = readLine()
            nbtentatives -=1 
            if((nbtentatives == 0)&&(pin != codespin(id))){
              println("\nTrop d'erreurs, abandon de l'identification.")
            }
          }
        } 
      }
      println("\nChoisissez votre opération :\n  1) Dépôt \n  2) Retrait \n  3) Consultation du compte \n  4) changement du code pin \n  5) Terminer \nVotre choix :")
      choix = readInt()
      if(choix == 1) {
         depot(id, comptes)
      }
      if(choix == 2){
        retrait(id, comptes)
      }
      if(choix == 3) {
        printf("\nLe montant disponible sur votre compte est de : %.2f\n", comptes(id)) 
      }
      if (choix == 4) {
        changepin(id, codespin)
      }      
      if(choix == 5) {
        println("\nFin des opérations, n'oubliez pas de récupérer votre carte.") 
      }
    }
  }
}