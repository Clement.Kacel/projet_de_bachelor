//Assignment: Loïc Tamba Kiluangu_996522_assignsubmission_file

import scala.io.StdIn.readLine
import scala.util.Try
import scala.annotation.tailrec

object Main {

  val text_choice: String = """Choisissez votre opération :
1) Dépôt
2) Retrait
3) Consultation du compte
4) Changement du code pin
5) Terminer
Votre choix : """

  val text_pin: String = "Saisissez votre code pin > "
  val text_wrong_pin = "Code pin erroné, il vous reste %d tentatives > "
  val text_exit: String = "Trop d’erreurs, abandon de l’identification"

  val text_deposit_currency: String = "Indiquez la devise du dépôt: 1) CHF ; 2) EUR > "
  val text_deposit: String = "Indiquez le montant du dépôt > "
  val text_error_deposit: String = "Le montant doit être un multiple de 10."
  val text_successful_deposit: String = "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f"

  val text_check_account: String = "Le montant disponible sur votre compte est de : %.2f"

  val text_withdrawal_currency: String = "Indiquez la devise: 1) CHF ; 2) EUR > " // slightly modified from assignment as there seemed to be a formatting error
  val text_withdrawal_amount: String = "Indiquez le montant du retrait > "
  val text_error_withdrawal: String = "Le montant doit être un multiple de 10."
  val text_max_withdrawal: String = "Votre plafond de retrait autorisé est de : %d"
  val text_size_withdrawal: String = "En 1) grosses coupures, 2) petites coupures > "
  val text_remaining_amount_withdrawal: String = "Il reste %d %s à distribuer"
  val text_amount_bills_withdrawal: String = "Vous pouvez obtenir au maximum %d billet(s) de %d %s"
  val text_validate_wihtdrawal: String = "Tapez o pour ok ou une autre valeur inférieure à celle proposée > "
  val text_withdraw_all: String = "Veuillez retirer la somme demandée :"
  val text_withdraw_one: String = "%d billet(s) de %d %s"
  val text_successful_withdrawal: String = "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f"

  val text_end_program: String = "Fin des opérations, n'oubliez pas de récupérer votre carte."

  val text_id_client: String = "Saisissez votre code identifiant > "
  val text_error_id_client: String = "Cet identifiant n’est pas valable."

  val text_change_pin: String = "Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > "
  val text_error_change_pin: String = "Votre code pin ne contient pas au moins 8 caractères"

  val max_pin_attempts: Int = 3
  val initial_pin: String = "INTRO1234"
  val eur_to_chf_rate: Double = 0.95
  val chf_to_eur_rate: Double = 1.05

  val nbclients: Int = 100
  val initial_value: Double = 1200.0
  val comptes: Array[Double] = Array.fill(nbclients)(initial_value)
  val codespin: Array[String] = Array.fill(nbclients)(initial_pin)

  def verify_login(): Int = {
    var id_client: String = readLine(text_id_client)
    while (!Try(id_client.toInt).isSuccess || id_client.toInt < 1 || id_client.toInt > nbclients) {
      println(text_error_id_client)
      id_client = readLine(text_id_client)
    }
    return id_client.toInt - 1 // because client id must be between 1 and 100 (included), so we need to remove 1 so that we can use it in arrays which are 0-indexed 
  }
  
  def verify_pin(correct_pin: String, max_pin_attempts: Int): Boolean = {
    var pin_attempts = max_pin_attempts
    var entered_pin = ""
    while(pin_attempts>0){
      entered_pin = readLine(text_pin)
      if(entered_pin == correct_pin){
        return true
      } else {
        pin_attempts = pin_attempts - 1
        if(pin_attempts>0){
          println(text_wrong_pin.format(pin_attempts))
        }
      }
    }
    println(text_exit)
    return false
  }

  def depot(id : Int, comptes: Array[Double]) : Unit = {
    var account_amount: Double = comptes(id)
    var currency: String = ""
    while(currency!="1" && currency!="2"){ //safety check
      currency = readLine(text_deposit_currency)
    }
    var amount_deposit: String = readLine(text_deposit)
    while(!Try(amount_deposit.toInt).isSuccess || amount_deposit.toInt<10 || amount_deposit.toInt%10!=0){
    // check that deposit amount is multiple of 10 and at least 10
      println(text_error_deposit)
      amount_deposit = readLine(text_deposit)
    }
    val amount_deposit_int: Int = amount_deposit.toInt

    if(currency == "1"){ // CHF
      account_amount += amount_deposit_int
    } else {
      account_amount += eur_to_chf_rate*amount_deposit_int // convert eur to chf before updating amount on account
    }
    println(text_successful_deposit.format(account_amount))
    comptes(id) = account_amount
  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    var account_amount: Double = comptes(id)
    var currency: String = ""
    while(currency!="1" && currency!="2"){
      currency = readLine(text_withdrawal_currency)
    }
    var limit_withdrawal: Int = 0
    if(currency=="1"){ // CHF
      limit_withdrawal = (account_amount*0.01).toInt*10 // goal here is nearest multiple of 10 to the actual max withdrawal amount. Ex: we don't want the limit to be 122, but rather 120.
    } else { // EUR
      limit_withdrawal = (account_amount*chf_to_eur_rate*0.01).toInt*10 // first we transform from CHF to EUR and then compute withdrawal limit
    }

    var amount_withdrawal: String = readLine(text_withdrawal_amount)
    while(!Try(amount_withdrawal.toInt).isSuccess || amount_withdrawal.toInt<10 || amount_withdrawal.toInt%10!=0 || amount_withdrawal.toInt>limit_withdrawal){
      println(text_error_withdrawal)
      println(text_max_withdrawal.format(limit_withdrawal))
      amount_withdrawal = readLine(text_withdrawal_amount)
    }
    var amount_withdrawal_int: Int = amount_withdrawal.toInt

    account_amount = account_amount - (if(currency=="1") amount_withdrawal_int else amount_withdrawal_int*eur_to_chf_rate) // reduce amount in account depending on currency

    var size_bills: String = "" // "1" is grosses coupures, "2" is petites coupures
    if(amount_withdrawal_int>=200 && currency=="1"){ // only if withdraw more than 200 in CHF
      while(size_bills!="1" && size_bills!="2"){ // safety check
        size_bills = readLine(text_size_withdrawal)
      }
    } else{
      size_bills = "2"
    }
    var possible_bills: List[Int] = List()
    if(size_bills == "1"){ // grosses coupures
      possible_bills = List(500, 200, 100, 50, 20, 10)
    } else { // petites coupures
      possible_bills = List(100, 50, 20, 10)
    }
    var current_bill_index: Int = 0
    val currency_string: String = if(currency=="1") "CHF" else "EUR"

    var withdrawals_list: List[(Int, Int)] = List()
    var validate_withdrawal: String = ""
    while(amount_withdrawal_int>0 && current_bill_index<possible_bills.length){
      if(amount_withdrawal_int/possible_bills(current_bill_index)>0){ // pour éviter les "0 billet(s) de 50 CHF" par ex
        if(current_bill_index!=possible_bills.length-1){
          println(text_remaining_amount_withdrawal.format(amount_withdrawal_int, currency_string))
          println(text_amount_bills_withdrawal.format(amount_withdrawal_int/possible_bills(current_bill_index), possible_bills(current_bill_index), currency_string))

          validate_withdrawal = readLine(text_validate_wihtdrawal)
          while (!(validate_withdrawal=="o" || validate_withdrawal == "ok" || (Try(validate_withdrawal.toInt).isSuccess && validate_withdrawal.toInt>=0 && validate_withdrawal.toInt<=amount_withdrawal_int/possible_bills(current_bill_index)))){ // safety check
            validate_withdrawal = readLine(text_validate_wihtdrawal)
          }
          if(validate_withdrawal == "o" || validate_withdrawal == "ok"){
            withdrawals_list = withdrawals_list :+ (amount_withdrawal_int/possible_bills(current_bill_index), possible_bills(current_bill_index))
            amount_withdrawal_int -= (amount_withdrawal_int/possible_bills(current_bill_index))*possible_bills(current_bill_index)
          } else if(validate_withdrawal.toInt>0){
            withdrawals_list = withdrawals_list :+ (validate_withdrawal.toInt, possible_bills(current_bill_index))
            amount_withdrawal_int -= validate_withdrawal.toInt*possible_bills(current_bill_index)
          }
        } else { // force accept if only last bill available, namely 10
          withdrawals_list = withdrawals_list :+ (amount_withdrawal_int/possible_bills(current_bill_index), possible_bills(current_bill_index))
          amount_withdrawal_int -= (amount_withdrawal_int/possible_bills(current_bill_index))*possible_bills(current_bill_index)
        }
      }
      current_bill_index+=1 // to consider the next smaller bill size
    }
    println(text_withdraw_all)
    for (e <- withdrawals_list){
      println(text_withdraw_one.format(e._1, e._2, currency_string))
    }
    println(text_successful_withdrawal.format(account_amount))
    comptes(id) = account_amount
  }

  def checkaccount(id : Int, comptes : Array[Double]): Unit = {
    println(text_check_account.format(comptes(id)))
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var new_pin: String = readLine(text_change_pin)
    while (new_pin.length<8){
      println(text_error_change_pin)
      new_pin = readLine(text_change_pin)
    }
    codespin(id) = new_pin
  }
  
  @tailrec
  def processChoice(choice: String, client_id: Int): Unit = {
    if (choice != "1" && choice != "2" && choice != "3" && choice != "4" && choice != "5") {
      val newChoice = readLine(text_choice)
      processChoice(newChoice, client_id)
    } else {
      choice match {
        case "1" =>
          depot(client_id, comptes)
          processChoice(readLine(text_choice), client_id)
        case "2" =>
          retrait(client_id, comptes)
          processChoice(readLine(text_choice), client_id)
        case "3" =>
          checkaccount(client_id, comptes)
          processChoice(readLine(text_choice), client_id)
        case "4" =>
          changepin(client_id, codespin)
          processChoice(readLine(text_choice), client_id)
        case _ =>
          println(text_end_program)
      }
    }
  }
  
  def main(args: Array[String]): Unit = {
    var client_id: Int = 0
    var choice: String = ""
    while(true){
      client_id = verify_login()
      if(verify_pin(codespin(client_id), max_pin_attempts)){
        choice = readLine(text_choice)
        processChoice(choice, client_id)
      }
    }
  }
}
