//Assignment: Hakob Hovhannisyan_996604_assignsubmission_file

import scala.io.StdIn._

object Main {
  val NBClients = 100
  

  def Depot(ID : Int, Comptes : Array[Double]) : Unit = {

    println("Indiquez la devise du dépôt. \n1) CHF \n2) EUR")
    var DeviseDepot = readInt()
    while (DeviseDepot != 1 && DeviseDepot != 2) {
      println("Veuillez choisir entre la devise CHF et EUR.")
      DeviseDepot = readInt()
    }
    println("Indiquez le montant du dépôt.")
    var MontantDepot = readInt()
    while (MontantDepot % 10 != 0) {
      println("Le montant doit être un multiple de 10.")
      MontantDepot = readInt()
    }
    if (MontantDepot % 10 == 0 && DeviseDepot == 1) {
      Comptes(ID) = MontantDepot + Comptes(ID)
      println(s"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: ${Comptes(ID)} CHF.")
    }
    if (MontantDepot % 10 == 0 && DeviseDepot == 2) {
      val EUR2CHF = 0.95
      MontantDepot = (MontantDepot * EUR2CHF).toInt
      Comptes(ID) = MontantDepot + Comptes(ID)
      println(s"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: ${Comptes(ID)} CHF.")
    }
  }
  
  def Retrait(ID: Int, Comptes: Array[Double]): Unit = {
    println("Indiquez la devise du retrait. \n1) CHF \n2) EUR")
    var Devise = readInt()

    while (Devise != 1 && Devise != 2) {
      println("Veuillez choisir entre la devise CHF et EUR.")
      Devise = readInt()
    }

    var Distribution: List[(Int, Int)] = List()

    if (Devise == 1) {
      println("Indiquez le montant du retrait")
      var MontantRetrait = readInt()

      while (MontantRetrait % 10 != 0 || MontantRetrait > Comptes(ID) / 10) {
        if (MontantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        } else {
          println(s"Votre plafond de retrait autorisé est de : ${Comptes(ID) / 10}")
        }
        MontantRetrait = readInt()
      }

      var RetraitCHF = MontantRetrait

      var CoupuresDisponibles: Array[Int] = Array()
      if (MontantRetrait >= 200) {
        println("En \n1) Grosses coupures \n2) Petites coupures")
        var TypeCoupure = readInt()

        CoupuresDisponibles =
          if (TypeCoupure == 1) Array(500, 200, 100, 50, 20, 10)
          else if (TypeCoupure == 2) Array(100, 50, 20, 10)
          else Array.empty[Int]

        for (coupure <- CoupuresDisponibles) {
          var Reste = MontantRetrait
          val NBCoupures = (Reste / coupure).toInt

          if (NBCoupures > 0) {
            var ChoixCoupure = 0
            var Saisie = ""
            do {
              println(s"Il reste $Reste CHF à distribuer. Vous pouvez obtenir au maximum $NBCoupures billet(s) de $coupure CHF.")
              if (coupure != 10) {
                print("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
              Saisie = readLine()
              if (Saisie != "o" && coupure == 10) {
                println("Vous ne pouvez pas choisir cette option. Vous allez recevoir $NBCoupures billet(s) de $Coupure CHF.")
              }
              if (Saisie == "o") {
                ChoixCoupure = NBCoupures
              }
            } while (ChoixCoupure < 0 || ChoixCoupure > NBCoupures)

            MontantRetrait = MontantRetrait - ChoixCoupure * coupure
            Reste = Reste - ChoixCoupure * coupure
            Distribution = (coupure, ChoixCoupure) :: Distribution
          }
        }
        Comptes(ID) -= RetraitCHF
      }

      if (MontantRetrait < 200) {
        var CoupuresDisponibles = Array(100, 50, 20, 10)

        for (coupure <- CoupuresDisponibles) {
          var Reste = MontantRetrait
          val NBCoupures = (Reste / coupure).toInt

          if (NBCoupures > 0) {
            var ChoixCoupure = 0
            var Saisie = ""
            do {
              println(s"Il reste $Reste CHF à distribuer. Vous pouvez obtenir au maximum $NBCoupures billet(s) de $coupure CHF.")
              if (coupure != 10) {
                print("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
              }
              Saisie = readLine()
              if (Saisie != "o" && coupure == 10) {
                println("Vous ne pouvez pas choisir cette option. Vous allez recevoir $nbCoupures billet(s) de $coupure CHF.")
              } else if (Saisie == "o") {
                ChoixCoupure = NBCoupures
              }
            } while (ChoixCoupure < 0 || ChoixCoupure > NBCoupures)

            MontantRetrait = MontantRetrait - ChoixCoupure * coupure
            Reste = Reste - ChoixCoupure * coupure
            Distribution = (coupure, ChoixCoupure) :: Distribution
          }
        }
        Comptes(ID) -= RetraitCHF
      }

      println("Veuillez retirer la somme demandée.")
      for ((coupure, nbCoupures) <- Distribution.reverse if nbCoupures > 0) {
        println(s"$nbCoupures billet(s) de $coupure CHF.")
      }
      printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF.%n", Comptes(ID))
    }

    if (Devise == 2) {
      println("Indiquez le montant du retrait")
      var MontantRetrait = readInt()

      while (MontantRetrait % 10 != 0 || MontantRetrait > Comptes(ID) / 10) {
        if (MontantRetrait % 10 != 0) {
          println("Le montant doit être un multiple de 10.")
        } else {
          println(s"Votre plafond de retrait autorisé est de : ${Comptes(ID) / 10}")
        }
        MontantRetrait = readInt()
      }
      var RetraitEUR = MontantRetrait * 1.05

      var CoupuresDisponibles = Array(100, 50, 20, 10)
      var Distribution = List[(Int, Int)]()
      for (coupure <- CoupuresDisponibles) {
        var Reste = MontantRetrait
        val NBCoupures = (Reste / coupure).toInt

        if (NBCoupures > 0) {
          var ChoixCoupure = 0
          var Saisie = ""
          do {
            println(s"Il reste $Reste EUR à distribuer. Vous pouvez obtenir au maximum $NBCoupures billet(s) de $coupure EUR.")
            if (coupure != 10) {
              print("Tapez o pour ok ou une autre valeur inférieure à celle proposée >")
            }
            Saisie = readLine()
            if (Saisie != "o" && coupure == 10) {
              println("Vous ne pouvez pas choisir cette option. Vous allez recevoir $NBCoupures billet(s) de $Coupure CHF.")
            }
            if (Saisie == "o") {
              ChoixCoupure = NBCoupures
            }
          } while (ChoixCoupure < 0 || ChoixCoupure > NBCoupures)

          MontantRetrait = MontantRetrait - ChoixCoupure * coupure
          Reste = Reste - ChoixCoupure * coupure
          Distribution = (coupure, ChoixCoupure) :: Distribution
        }
      }
      Comptes(ID) -= RetraitEUR
      println("Veuillez retirer la somme demandée.")
      for ((coupure, nbCoupures) <- Distribution.reverse if nbCoupures > 0) {
        println(s"$nbCoupures billet(s) de $coupure EUR.")
      }
      println(s"Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de ${Comptes(ID)} CHF.")
    }
  }
  


  def ChangePIN(ID : Int, CodesPIN : Array[String]) : Unit = {
    println("Saisissez votre nouveau code PIN (il doit contenir au moins 8 caractères) >")
    var NouveauCodePIN = readLine()
    while (NouveauCodePIN.length < 8) {
      println(" Votre code pin ne contient pas au moins 8 caractères.")
      var NouveauCodePIN = readLine()
    }
    if (NouveauCodePIN.length >= 8) {
      CodesPIN(ID) = NouveauCodePIN
      println(s"Votre nouveau code pin est : ${CodesPIN(ID)}")
    }  
  }

  def main(args: Array[String]): Unit = {
    var Tentatives = 3
    var Comptes: Array[Double] = Array.fill(NBClients)(1200.0)
    var CodesPIN: Array[String] = Array.fill(NBClients)("INTRO1234")
    var ID = 0
    var OperationSaisie = 0

    do {
      println("Saisissez votre code identifiant >")
      var SaisieID2 = readInt()
      var SaisieIDtab = SaisieID2.toInt
      
      if (SaisieIDtab < 0 || SaisieIDtab > NBClients) {
        println("Cet identifient n'est pas valable.")
        System.exit(0)
      }
      if (SaisieIDtab <= NBClients) {
        ID = SaisieIDtab

        println("Saisissez votre code pin >")
        var SaisieCodePIN = readLine()

        while (SaisieCodePIN != CodesPIN(ID)) {
          Tentatives -= 1
          if (Tentatives > 0) {
            println(s"Code pin erroné, il vous reste $Tentatives tentatives.")
            println("Saisissez votre code pin >")
            SaisieCodePIN = readLine()
          } else {
            println("Trop d'erreurs, abandon de l'identification")
            System.exit(0)
          }
        }
        Tentatives = 3
        
        do {
          println("Veuillez choisir une opération : \n1) Dépôt \n2) Retrait \n3) Consultation de compte \n4) Changement de code pin \n5) Terminer \nVotre choix :")
          OperationSaisie = readInt()

          if (OperationSaisie == 1) {
            Depot(ID, Comptes)
          } else if (OperationSaisie == 2) {
            Retrait(ID, Comptes)
          } else if (OperationSaisie == 3) {
            println(s"Le montant disponible sur votre compte est de : ${Comptes(ID)} CHF.")
          } else if (OperationSaisie == 4) {
            ChangePIN(ID, CodesPIN)
          } else if (OperationSaisie > 5) {
          println("Veuillez saisir un nombre entier entre 1 et 5")
          }
        } while (OperationSaisie != 5)
        

      } else if (OperationSaisie == 5) {
        println("Fin des opérations, n'oubliez pas votre carte.")
        } 
      } while (true)
    }
  }