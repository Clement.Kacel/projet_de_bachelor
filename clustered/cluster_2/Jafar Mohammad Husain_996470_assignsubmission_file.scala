//Assignment: Jafar Mohammad Husain_996470_assignsubmission_file

object Main {
  import scala.math.floor
  def main(args: Array[String]): Unit = {
    var choix: Int = 0
    var wrongAttempts: Int = 0
    val nbclients: Int = 100
    val comptes: Array[Double] = Array.fill(nbclients)(1200.0)
    val codespin: Array[String] = Array.fill(nbclients)("INTRO1234")
    var auth = false

    while (choix != 6) {
      choix = 0
      auth = false
      wrongAttempts = 0

      println("Saisissez votre code identifiant >")
      val id = scala.io.StdIn.readInt()

      if (id < 0 || id >= nbclients) {
        println("Cet identifiant n'est pas valable.")
        choix = 6
      } else {

        while (wrongAttempts < 3 && !auth) {
          println("Saisissez votre code pin >")
          val codePin = scala.io.StdIn.readLine()

          if (codePin == codespin(id)) {
            auth = true
            while (choix != 5) {
              println("Choisissez votre opération :")
              println("1) Dépôt")
              println("2) Retrait")
              println("3) Consultation du compte")
              println("4) Changement du code pin")
              println("5) Terminer")
              print("Votre choix :")
              choix = scala.io.StdIn.readInt()
              if (choix == 1) {
                depot(id = id, comptes = comptes)
              }
              if (choix == 2) {
                retrait(id = id, comptes = comptes)
              }

              if (choix == 3) {
                printf(
                  "Le montant disponible sur votre compte est de : : %.2f\n",
                  comptes(id)
                )
              }
              if (choix == 4) {
                changepin(id = id, codespin = codespin)
              }
              if (choix == 5) {
                println(
                  "Fin des opérations, n’oubliez pas de récupérer votre carte."
                )
              }
            }

          } else {
            wrongAttempts += 1
            val remainingAttempts = 3 - wrongAttempts
            if (wrongAttempts!=3) {
              println(
              s"Code pin erroné, il vous reste $remainingAttempts tentatives >"
            )
            }
            
          }
        }

        if (!auth) {
          println("Trop d'erreurs, abandon de l'identification.")
          wrongAttempts = 0 // Réinitialiser le compteur d'erreurs
        }
      }
    }
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    print("1) CHF ; 2) EUR >")
    var choixDevise = scala.io.StdIn.readInt()
    print("Indiquez le montant du dépôt >")
    var montantDepot = scala.io.StdIn.readInt()
    while (montantDepot % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      print("Indiquez le montant du dépôt >")
      montantDepot = scala.io.StdIn.readInt()
    }
    if (choixDevise == 2) {
      var montantCHF = montantDepot * 0.95
      comptes(id) += montantCHF
    } else {
      comptes(id) += montantDepot
    }
    printf(
      "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de :%.2f\n",
      comptes(id)
    )

  }



  def retrait(id: Int, comptes: Array[Double]): Unit = {
    var choixDevise2 = 0
    print("1) CHF ; 2) EUR >")

    choixDevise2 = scala.io.StdIn.readInt()
    while (choixDevise2 != 1 && choixDevise2 != 2) {
      print("1) CHF ; 2) EUR >")
      choixDevise2 = scala.io.StdIn.readInt()
    }
    print("Indiquez le montant du retrait >")
    var montantRetrait = scala.io.StdIn.readInt()
    while ((montantRetrait % 10) != 0) {
      println("Le montant doit être un multiple de 10")
      print("Indiquez le montant du dépôt >")
      montantRetrait = scala.io.StdIn.readInt()
    }
    while (montantRetrait > (comptes(id) * 10 / 100)) {
      println(
        s"Votre plafond de retrait autorisé est de ${comptes(id) * 10 / 100}"
      )
      print("Indiquez le montant du retrait >")
      montantRetrait = scala.io.StdIn.readInt()
    }

    if (choixDevise2 == 1 || choixDevise2 == 2) {
      var choixCoupure = 2
      if (montantRetrait > 200) {
        print("En 1) grosses coupures, 2) petites coupures >")
        choixCoupure = scala.io.StdIn.readInt()
        while (choixCoupure != 1 && choixCoupure != 2) {
          print("En 1) grosses coupures, 2) petites coupures >")
          choixCoupure = scala.io.StdIn.readInt()
        }
      }
      var choixDeviseString = if (choixDevise2 == 2) "EUR" else "CHF"
      if (choixCoupure == 1 || choixCoupure == 2) {
        val coupures = List(500, 200, 100, 50, 20, 10)
        var bills: List[Int] = List.fill(coupures.length)(0)

        var index = 0
        while (index < coupures.length) {
          val coupure = coupures(index)
          if (montantRetrait >= coupure) {
            if (coupure != 10) {
              println(s"il reste $montantRetrait à distribuer")
              println(
                s"Vous pouvez obtenir au maximum ${floor(montantRetrait.toDouble / coupure).toInt}  billet(s) de $coupure $choixDeviseString"
              )
            }

            val optNbBillets =
              if (index == coupures.length - 1) "o"
              else
                scala.io.StdIn.readLine(
                  "Tapez o pour ok ou une autre valeur inférieure à celle proposée >"
                )

            if (
              optNbBillets == "o" || optNbBillets.toInt <= (floor(
                montantRetrait.toDouble / coupure
              ).toInt)
            ) {
              val nbBillets =
                if (optNbBillets == "o")
                  floor(montantRetrait.toDouble / coupure).toInt
                else optNbBillets.toInt
              montantRetrait = montantRetrait - coupure * nbBillets
              bills = bills.updated(index, nbBillets)
            }
          }
          index = index + 1
        }

        var totalAmount = 0.0
        index = 0
        while (index < coupures.length) {
          val coupure = coupures(index)
          val nbBillets = bills(index)

          if (nbBillets != 0) {
            println(
              s"Vous pouvez obtenir au maximum $nbBillets billet(s) de $coupure $choixDeviseString"
            )
            totalAmount += coupure * nbBillets
          }

          index = index + 1
        }

        if (montantRetrait == 0) {
          println("Veuillez retirer la somme demandée :")
          val reductionFactor = if (choixDevise2 == 2) 0.95 else 1.0
          comptes(id) = comptes(id) - totalAmount * reductionFactor

          printf(
            "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f\n",
            comptes(id)
          )
        }
      }
    }
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {
    println("Saisissez le nouveau code pin (au moins 8 caractères) >")
    var nouveauPin = scala.io.StdIn.readLine()

    while (nouveauPin.length < 8) {
      println("Le code pin doit contenir au moins 8 caractères. Réessayez >")
      nouveauPin = scala.io.StdIn.readLine()
    }

    codespin(id) = nouveauPin
    println("Le code pin a été mis à jour avec succès.")
  }

}
