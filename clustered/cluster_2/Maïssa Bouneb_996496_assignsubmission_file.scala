//Assignment: Maïssa Bouneb_996496_assignsubmission_file

import scala.io.StdIn._
import scala.util.control.Breaks._
import scala.math._

object Main {
  // Methode verification pin
  def verificationpin(id: Int): Int = {
    val nbclients = 100
    var tentatives = 3
    var okpin = 0
    val codespin: Array[String] = new Array[String](nbclients)

    for (i <- 0 to 99) {
      codespin(i) = "INTRO1234"
    }

    do {
      println("Saisissez votre code pin >")
      val pin = readLine
      if (pin != codespin(id)) {
        tentatives -= 1
        println("Code pin erroné, il vous reste " + tentatives + " tentatives >")
      } else {
        okpin = 1
      }
    } while ((tentatives > 0) && (okpin == 0))

    if (tentatives == 0) {
      println(" Trop d'erreurs, abandon de l'identification. ")
      return 0
    } else {
      return 1
    }
  }

  // Methode depot
  def depot(id: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    val devise = readInt()
    println("Indiquez le montant du dépôt >")
    var depot = readInt()

    while (depot % 10 != 0) {
      print("Le depot doit etre un multiple de 10 ")
      depot = readInt()
    }
    if (devise == 1) {
      comptes(id) = comptes(id) + depot
    } else {
      comptes(id) = comptes(id) + (depot * 0.95)
      comptes(id) = math.floor(comptes(id) * 100) / 100
    }
    println(
      "Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id)
    )
  }

  // Methode retrait
  def retrait(id: Int, comptes: Array[Double]): Unit = {
    println("Indiquez la devise : 1 CHF, 2 : EUR >")
    var devise = readInt()
    while (devise != 1 && devise != 2) {
      println("Vous devez choisir 1 ou 2")
      devise = readInt()
    }

    println("Indiquez le montant du retrait >")
    var retrait = readInt()

    while (retrait % 10 != 0 || retrait > 0.1 * comptes(id)) {
      if (retrait % 10 != 0) {
        println("Le montant doit être un multiple de 10.")
        retrait = readInt()
      }
      if (retrait > 0.1 * comptes(id)) {
        println("Votre plafond de retrait autorisé est de:" + 0.1 * comptes(id))
        retrait = readInt()
      }
    }

    if (devise == 1) {
      val montantAvantRetrait = comptes(id) // Sauvegarder le montant avant le retrait
      comptes(id) = comptes(id) - retrait

      // ... (le reste du code pour les coupures)

      println(
        "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est> " + f"${comptes(
          id
        )}%.2f"
      )

      // Vérifier si le montant affiché est correct
      assert(comptes(id) == montantAvantRetrait - retrait) // Vérification pour s'assurer que le calcul est correct
    } else if (devise == 2) {
      val montantAvantRetrait = comptes(id) // Sauvegarder le montant avant le retrait
      comptes(id) = comptes(id) - retrait * 0.95

      // ... (le reste du code pour les coupures)

      println(
        "Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est> " + f"${comptes(
          id
        )}%.2f"
      )

      // Vérifier si le montant affiché est correct
      assert(comptes(id) == montantAvantRetrait - retrait) // Vérification pour s'assurer que le calcul est correct
    }

    // ... (le reste du code)
  }

  // Methode changer le codepin
  def changepin(id: Int, codespin: Array[String]): Unit = {
    var ok = 0
    while (ok == 0) {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >  ")
      val nouveaupin = readLine()
      if (nouveaupin.length >= 8) {
        ok = 1
        codespin(id) = nouveaupin
      } else {
        println("Votre code pin ne contient pas au moins 8 caractères ")
      }
    }
  }

  def main(args: Array[String]): Unit = {
    var okchoix = 0
    var choix = 0
    val nbclients = 100
    val nboperations: Array[Int] = new Array[Int](nbclients)
    val comptes = Array.fill(nbclients)(1200.0)
    val codespin: Array[String] = new Array[String](nbclients)

    for (i <- 0 to 99) {
      nboperations(i) = 0
    }

    for (i <- 0 to 99) {
      codespin(i) = "INTRO1234"
    }

    // Boucle continue pour tout le programme
    while (1 > 0) {
      println("Saisissez votre code identifiant >")
      val id = readInt

      if (id > 100) {
        println("Cet identifiant n'est pas valable.")
        break
      }
      var ok = 0
      nboperations(id) += 1
      if (nboperations(id) == 1)

      /// Verification code pin
      {
        var nbessaies = 1
        var okchoix = 0
        var choix = 0
        var ok = 0

        while (ok == 0) {
          if (nbessaies > 1) {
            println("Saisissez votre code identifiant >")
            val id = readInt
            if (id > 100) {
              println("Cet identifiant n'est pas valable.")
              break
            }
          }

          if (verificationpin(id) == 1) {
            ok = 1
          } else {
            ok = 0
            nbessaies = nbessaies + 1
          }
        }
      }

      do {
        println(
          """ Choisissez votre opération :
            |1) Dépôt
            |2) Retrait
            |3) Consultation du compte
            |4) Changement du code pin
            |5) Terminer
            |Votre choix : """
            .stripMargin
        )

        val choix = readInt()

        if (choix == 5) {
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
          okchoix = 1
        } else if (choix == 1) {
          depot(id, comptes)
        } else if (choix == 2) {
          retrait(id, comptes)
        } else if (choix == 3) {
          println("Le montant disponible sur votre compte est de: " + comptes(id))
        } else if (choix == 4) {
          changepin(id, codespin)
        }

      } while (okchoix != 1)
    }
  }
}
