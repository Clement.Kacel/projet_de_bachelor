//Assignment: Clément Thomas Gabriel Gavard_996536_assignsubmission_file

object Main {

  val nbclients = 100

  val comptes = Array.fill(nbclients)(1200.0)

  val codespin = Array.fill(nbclients)("INTRO1234")

  def main(args: Array[String]): Unit = {

    var id = 0

    do {

      id = scala.io.StdIn.readLine("Saisissez votre code identifiant > ").toInt

      if (id < 0 || id >= nbclients) {
        println("Cet identifiant n'est pas valable désolé.")
        System.exit(0)
      } else {

        val codepinSaisi = scala.io.StdIn.readLine("Saisissez votre code pin svp > ").toString

        if (codespin(id) == codepinSaisi) {
          println("Authentification réussie.")
          menu(id)
        } else {
          println("Malheureusement votre code pin erroné.")
        }

      }

    } while (id < 0 || id >= nbclients)

  }

  def menu(id: Int): Unit = {

    var choix = 0

    do {

      println("Choisissez votre opération :")
      println("1) Dépôt")
      println("2) Retrait")
      println("3) Consultation du compte")
      println("4) Changement du code pin")
      println("5) Terminer")
      println("Votre choix : ")

      choix = scala.io.StdIn.readLine().toInt

      choix match {
        case 1 =>
          depot(id)
        case 2 =>
          retrait(id)
        case 3 =>
          consultation(id)
        case 4 =>
          changepin(id)
        case 5 =>
          println("Fin des opérations, n'oubliez pas de récupérer votre carte svp.")
          return
      }

    } while (choix != 5)

  }

  def depot(id: Int): Unit = {

    println("Vous êtes prié d'indiquer le montant du dépôt :")
    val montant = scala.io.StdIn.readLine("Vous êtes prié d'indiquer le montant du dépôt :").toInt

    if (montant % 10 != 0) {
      println("Le montant doit être un multiple de 10.")
    } else {
      comptes(id) += montant
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id))
    }

  }

  def retrait(id: Int): Unit = {

    val montant = scala.io.StdIn.readLine("Merci, veuillez indiquer le montant du retrait :").toInt

    if (montant % 10 != 0) {
      println("Le montant doit être un multiple de 10.")
    } else {
      val plafond = comptes(id) * 0.1

      if (montant > plafond) {
        println("Votre plafond de retrait autorisé est de : " + plafond)
      } else {
        comptes(id) -= montant
        println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id))
      }
    }

  }

  def consultation(id: Int): Unit = {
    println("Le montant disponible sur votre compte est de : " + comptes(id))
  }

  def changepin(id: Int): Unit = {

    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    val codepinNouveau = scala.io.StdIn.readLine()

    if (codepinNouveau.length < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères.")
    } else {
      codespin(id) = codepinNouveau
      println("Votre code pin a été changé.")
     }
    }
  
   def retrait(id: Int, montant: Int): Unit = {

     println("Merci, veuillez indiquer le montant du retrait :")
     var montant = scala.io.StdIn.readLine("Merci, veuillez indiquer le montant du retrait :").toInt

     if (montant % 10 != 0) {
       println("Le montant doit être un multiple de 10.")
     } else {
       val devise = scala.io.StdIn.readLine("Quelle devise souhaitez-vous retirer ? (EUR, USD, CHF)")
       val coupures = scala.io.StdIn.readLine("Quelles coupures souhaitez-vous retirer ? (10, 20, 50, 100)")

       val plafond = comptes(id) * 0.1

       if (montant > plafond) {
         println("Votre plafond de retrait autorisé est de : " + plafond)
       } else {
         val montantReel = montant
         comptes(id) -= montantReel
         println("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : " + comptes(id))
       }
     }
   }

    def calculRetrait(montant: Int, devise: String, coupures: String): Int = {

      val coupuresMap = Map(
        "10" -> 10,
        "20" -> 20,
        "50" -> 50,
        "100" -> 100
      )

      var montantReel = 0;
      var montant = 0;

      var codespins = Array("1234", "5678", "9012");

      def virement(i: Int, iDestinataire: Int): Unit = {
        val montant = scala.io.StdIn.readLine("Saisissez le montant du virement:").toInt;

        if (montant > comptes(i)) {
          println("Solde insuffisant pour effectuer le virement.");
        } else {
          comptes(i) -= montant;
          comptes(iDestinataire) += montant;

          println("Le virement a réussi.");
        }
      }

      object Main {
        def main(args: Array[String]): Unit = {

          val nbclients = scala.io.StdIn.readLine("Saisissez le nombre de clients:").toInt;
          val comptes = Array.fill(nbclients)(1200.0);

          for (i <- 0 until nbclients) {
            val codepin = scala.io.StdIn.readLine("Saisissez le code pin du client n°" + (i + 1) + ": ").toString;
            if (codespins(i) != codepin) {
              println("Code pin erroné.");
            } else {
              val menu = scala.io.StdIn.readLine("Saisissez le numéro de l'opération que vous désirez effectuer:").toInt;

              if (menu == 1) {
                retrait(i);
              } else if (menu == 2) {
                depot(i);
              } else if (menu == 3) {
                val iDestinataire = scala.io.StdIn.readLine("Saisissez le numéro du compte du destinataire: ").toInt;
                virement(i, iDestinataire);
              } else {
                println("Opération non valide.");
              }
            }
          }

        def retrait(i: Int): Unit = {
          val montant = scala.io.StdIn.readLine("Saisissez le montant du retrait: ").toInt;
          if (montant > comptes(i)) {
            println("Solde insuffisant pour effectuer le retrait.");
          } else {
            comptes(i) -= montant;
            println("Le retrait a réussi.");
          }
        }

        def depot(i: Int): Unit = {
          val montant = scala.io.StdIn.readLine("Saisissez le montant du dépôt: ").toInt;
          if (montant > 0) {
            comptes(i) += montant;
            println("Le dépôt a réussi.");
          } else {
            println("Le montant du dépôt doit être supérieur à 0.");
          }
        }
      }}
