//Assignment: Adrian Cayarga Rojo_996592_assignsubmission_file

object Main {
  def main(args: Array[String]): Unit = {
    val nbclients = 100
    val comptes = Array.fill(nbclients)(1200.0)
    val codespin = Array.fill(nbclients)("INTRO1234")
    
    var continuer = true
    while (continuer) {
      val id = scala.io.StdIn.readLine("Saisissez votre code identifiant > ").toInt
      if (id < 0 || id >= nbclients) {
        println("Cet identifiant n’est pas valable.")
        continuer = false 
      } else {
        var tentatives = 3
        var mdp = false
        
        while (tentatives > 0 && !mdp) {         
          val pin = scala.io.StdIn.readLine("Saisissez votre code pin > ")
          if (pin == codespin(id)) {
            mdp = true
          } else {
            tentatives -= 1
            if (tentatives!=0) {
              println("Code pin erroné, il vous reste "+ tentatives +" tentatives >")
            }
          }
        }

        
        if (mdp) {
          var choix = 0
          while (choix != 5){
            choix = scala.io.StdIn.readLine("Choissisez votre opération:\n   1) Dépôt\n   2) Retrait\n   3) Consultation du compte\n   4) Changement de code pin\n   5) Terminer\nVotre choix: ").toInt
            if (choix == 1) {
              depot(id, comptes)
            } else if (choix == 2) {
              retrait(id, comptes)
            } else if (choix == 3) {
              printf("Le montant disponible sur votre compte est de : %.2f \n",comptes(id))
            } else if (choix == 4) {
              changepin(id, codespin)
            } else if (choix == 5) {
              println("Fin des opérations, n’oubliez pas de récupérer votre carte.") 
            } 

          } 

          
        }else {
          println("Trop d’erreurs, abandon de l’identification")
        }

        
      }
    }
  }


  
  
  def depot(id: Int, comptes: Array[Double]): Unit = {
  
  var devise = scala.io.StdIn.readLine("Indiquez la devise du dépôt : CHF ; EUR >")
  while (devise!="CHF" && devise!="EUR") {
    devise = scala.io.StdIn.readLine("Indiquez la devise du dépôt : CHF ; EUR >")
  }

  
  var montant = scala.io.StdIn.readLine("Indiquez le montant du dépôt >").toDouble
  while(montant<0 || montant % 10 != 0) {
  if (montant<0) {
    montant = scala.io.StdIn.readLine("Indiquez la montant du dépôt >").toDouble
  } 
  if (montant % 10 != 0) {
    montant = scala.io.StdIn.readLine("Le montant doit être un multiple de 10 ").toDouble
  }
  }
  
  if (devise == "EUR") {
    montant *= 0.95
  }

  comptes(id) += montant
  printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id))
}


  

  
def retrait(id: Int, comptes: Array[Double]): Unit = {
  
  var devise = scala.io.StdIn.readLine("Indiquez la devise du dépôt : CHF ; EUR > ")
  while (devise!="CHF" && devise!="EUR") {
    devise = scala.io.StdIn.readLine("Indiquez la devise du dépôt : CHF ; EUR > ")
  }
  

  
  var montant = scala.io.StdIn.readLine("Indiquez le montant du retrait >").toDouble
  while (montant % 10 != 0.0 || montant>(comptes(id) * 0.1) || montant<0) {
  if (montant % 10 != 0.0) {
    montant = scala.io.StdIn.readLine("Le montant doit être un multiple de 10 > ").toDouble
  }
  if (montant > comptes(id) * 0.1) {
    montant = scala.io.StdIn.readLine("Votre plafond de retrait autorisé est de "+ (comptes(id) * 0.1)+" > ").toDouble
  }
  if (montant<0) {
    montant = scala.io.StdIn.readLine("Indiquez le montant du retrait > ").toDouble
  }
  }
  
  var typeCoupure = 0
  if (devise == "CHF" && montant >= 200) {
    val choix = scala.io.StdIn.readLine("En 1) grosses coupures, 2) petites coupures > ").toInt
    if (choix == 1) {
      typeCoupure = 1 
    } else {
      typeCoupure = 2 
    }
  } else {
    typeCoupure = 2 
  }


  var coupures = Array(0,0,0,0,0,0)
  if (devise == "CHF") {
    if (typeCoupure == 1){
     coupures = Array(500, 200, 100, 50, 20, 10)
    }else{
     coupures = Array(100, 50, 20, 10)
  } }else {
   coupures = Array(100, 50, 20, 10)
  }

  var billets500 = 0
  var billets200 = 0
  var billets100 = 0 
  var billets50 = 0 
  var billets20 = 0 
  var billets10 = 0
  
  var montantDist = montant 
  for (coupure <- coupures if (montantDist >= coupure) ) {
    val maxBillets = (montantDist/coupure).toInt

    if (coupure == 10) {
      if (montantDist/10 > 0){ 
         billets10 = (montantDist/10).toInt 
      }
      montantDist = 0 
    } else {
      println("Il reste "+ montantDist +" à distribuer\nVous pouvez obtenir au maximum "+ maxBillets+ " billet(s) de "+ coupure +" \nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
      
      var choix = scala.io.StdIn.readLine() 
      var nbBillets = 0
      
      if (choix == "o"){ 
        nbBillets=maxBillets 
      }else if ( choix.toInt>= 0 && choix.toInt <= maxBillets) {
        nbBillets=choix.toInt 
      }else {
        nbBillets=choix.toInt 
      while (nbBillets<0 || nbBillets>maxBillets){
       choix=scala.io.StdIn.readLine("La valeur entrée est incorrecte, veuillez en indiquer une autre ou o pour ok > ")    
        if (choix == "o"){
          nbBillets=maxBillets
        }else{       
          nbBillets=choix.toInt
        }
        
      }
        
    }
     
      if (coupure == 500) {
        billets500 = nbBillets
      } else if (coupure == 200) {
        billets200 = nbBillets
      } else if (coupure == 100) {
        billets100 = nbBillets
      } else if (coupure == 50) {
        billets50 = nbBillets
      } else if (coupure == 20) {
        billets20 = nbBillets
      }

      montantDist -= nbBillets * coupure
    }
  }

  
  println("Veuillez retirer la somme demandée :")
  if (billets500 > 0) {
  println(billets500 +" billet(s) de 500")
  }
  if (billets200 > 0) {
  println(billets200 +" billet(s) de 200")
  }
  if (billets100 > 0){
  println(billets100+" billet(s) de 100")
  }
  if (billets50 > 0) {
  println(billets50+" billet(s) de 50")
  }
  if (billets20 > 0){
  println(billets20 +" billet(s) de 20")
  }
  if (billets10 > 0){
  println(billets10 +" billet(s) de 10")
  }
  

  if (devise == "EUR") {
    montant *= 0.95 
  }
  
  comptes(id) -= montant

  printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f \n",comptes(id))
}



  
  def changepin(id: Int, codespin: Array[String]): Unit = {
    var NPin = scala.io.StdIn.readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    while (NPin.length < 8) {
      NPin = scala.io.StdIn.readLine("Votre code pin ne contient pas au moins 8 caractères >")
    }
    codespin(id) = NPin
    println("Code PIN changé.")  
  }
}
