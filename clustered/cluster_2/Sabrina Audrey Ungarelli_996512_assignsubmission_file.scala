//Assignment: Sabrina Audrey Ungarelli_996512_assignsubmission_file

import io.StdIn._
object Main {
  def depot(id : Int, comptes : Array[Double]) : Unit = {
    print("\nIndiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
    var devise = readInt ()
    print("\nIndiquez le montant du dépôt > ")
    var montant = readInt().toDouble
    while(montant % 10 != 0){
      println("\nLe montant doit être un multiple de 10")
      montant = readInt ()
    }
    if(devise == 2){
      montant = montant * 0.95
    }
    comptes(id) = comptes(id) + montant
    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n", comptes(id))
  }
  def retrait(id : Int, comptes : Array[Double]) : Unit = {
    print("\nIndiquez la devise : 1 CHF, 2 : EUR > ")
    var devise = readInt ()
    while((devise != 1) && (devise != 2)){
      print("\nIndiquez la devise : 1 CHF, 2 : EUR > ")
      devise = readInt ()
    }
    var pourcent = 0.1 * comptes(id)
    if(devise == 2){
      pourcent = pourcent * 1.05
    }
    print("\nIndiquez le montant du retrait > ")
    var montant = readInt.toDouble

    while((montant % 10 != 0) || (montant > pourcent)) {
      if(montant % 10 != 0){
        println("\nLe montant doit être un multiple de 10.")
        montant = readInt ()
      }
      else if(montant > pourcent){
        printf("\nVotre plafond de retrait autorisé est de : %.2f\n", pourcent)
        montant = readInt ()
      }
    }
    var choix = 2

    if((devise == 1) && (montant >= 200)){
      print("\nEn 1) grosses coupures, 2) petites coupures > ")
      choix = readInt ()
      while((choix != 1) && (choix != 2)){
        print("\nEn 1) grosses coupures, 2) petites coupures > ")
        choix = readInt ()
      }
    }
      var montant1 = montant
      var billets10 = 0
      var billets20 = 0
      var billets50 = 0
      var billets100 = 0
      var billets200 = 0
      var billets500 = 0
      if((choix == 1) && (montant >= 500)){
        var coupures : Int = (montant / 500).toInt
        print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 500 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        var nb = readLine()
        if(nb == "o"){
          billets500 = coupures
          montant = montant - (coupures * 500)
        }
        else{
          billets500 = nb.toInt
          montant = montant - nb.toInt * 500
        }
      }
      if((choix == 1) && (montant >= 200)){
        var coupures : Int = (montant / 200).toInt
        print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 200 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        var nb = readLine()
        if(nb == "o"){
          billets200 = coupures
          montant = montant - (coupures * 200)
        }
        else{
          billets200 = nb.toInt
          montant = montant - nb.toInt * 200
        }
      }
      if(montant >= 100){
        var coupures : Int = (montant / 100).toInt
        if(devise == 1){
          print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 100 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        }
        else{
          print("\nIl reste " + montant + " EUR à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 100 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        }
        var nb = readLine()
        if(nb == "o"){
          billets100 = coupures
          montant = montant - (coupures * 100)
        }
        else{
          billets100 = nb.toInt
          montant = montant - nb.toInt * 100
        }
      }
      if(montant >= 50){
        var coupures : Int = (montant / 50).toInt
        if(devise == 1){
          print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 50 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        }
        else{
          print("\nIl reste " + montant + " EUR à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 50 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        }
        var nb = readLine()
        if(nb == "o"){
          billets50 = coupures
          montant = montant - (coupures * 50)
        }
        else{
          billets50 = nb.toInt
          montant = montant - nb.toInt * 50
        }
      }
      if(montant >= 20){
        var coupures : Int = (montant / 20).toInt
        if(devise == 1){
          print("\nIl reste " + montant + " CHF à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 20 CHF\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        }
        else{
          print("\nIl reste " + montant + " EUR à distribuer\nVous pouvez obtenir au maximum " + coupures + " billet(s) de 20 EUR\nTapez o pour ok ou une autre valeur inférieure à celle proposée > ")
        }
        var nb = readLine()
        if(nb == "o"){
          billets20 = coupures
          montant = montant - (coupures * 20)
        }
        else{
          billets20 = nb.toInt
          montant = montant - nb.toInt * 20
        }
      }
      if(montant >= 10){
        var coupures : Int = (montant / 10).toInt
        billets10 = coupures
        montant = montant - (coupures * 10)
      }
      println("\nVeuillez retirer la somme demandée")
      if(devise == 1){
        if(billets500 != 0){
          println(billets500 + " billet(s) de 500 CHF")
        }
        if(billets200 != 0){
          println(billets200 + " billet(s) de 200 CHF")
        }
        if(billets100 != 0){
          println(billets100 + " billet(s) de 100 CHF")
        }
        if(billets50 != 0){
          println(billets50 + " billet(s) de 50 CHF")
        }
        if(billets20 != 0){
          println(billets20 + " billet(s) de 20 CHF")
        }
        if(billets10 != 0){
          println(billets10 + " billet(s) de 10 CHF")
        }
      }
      if(devise == 2){
        if(billets500 != 0){
          println(billets500 + " billet(s) de 500 EUR")
        }
        if(billets200 != 0){
          println(billets200 + " billet(s) de 200 EUR")
        }
        if(billets100 != 0){
          println(billets100 + " billet(s) de 100 EUR")
        }
        if(billets50 != 0){
          println(billets50 + " billet(s) de 50 EUR")
        }
        if(billets20 != 0){
          println(billets20 + " billet(s) de 20 EUR")
        }
        if(billets10 != 0){
          println(billets10 + " billet(s) de 10 EUR")
        }
      }
      if(devise == 2){
        montant1 = montant1 * 0.95
      }
      comptes(id) = comptes(id) - montant1
      printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f\n", comptes(id))
  }
  def changepin(id : Int, codespin : Array[String]) : Unit = {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")
    var nvcode = readLine()
    while(nvcode.length() < 8) {
      println("Votre code pin ne contient pas au moins 8 caractères")
      nvcode = readLine()
    }
    codespin(id) = nvcode
  }
  def main(args: Array[String]): Unit = {
    var id = 60
    var code = "0"
    var liste = 5
    var cash : Double = 1200.0 
    var nbclients = 100
    val comptes = Array.fill[Double](nbclients)(1200.0)
    val codespin = Array.fill[String](nbclients)("INTRO1234")
    while(true) {
      if(liste == 5) {
        code = "0"
        while(code != codespin(id)) {
          println("Saisissez votre code identifiant > ")
          id = readInt ()
          if(id >= nbclients) {
            println("Cet identifiant n'est pas valable.")
            System.exit(0)
          }
          print("\nSaisissez votre code pin > ")
          code = readLine ()
          var tentatives = 2
          while((code != codespin(id)) && (tentatives >= 1)) {
            print("\nCode pin erroné, il vous reste " + tentatives + " tentatives > ")
            code = readLine ()
            tentatives = tentatives - 1
            if((tentatives == 0) && (code != codespin(id))) {
              println("Trop d'erreurs, abandon de l'identification.")
            }
          }
        }
      }
      
      print("\nChoisissez votre opération :\n  1) Dépôt \n  2) Retrait \n  3) Consultation du compte \n  4) Changement du code pin \n 5) Terminer \nVotre choix : ")
      liste = readInt ()
      if(liste == 3) {
        printf("\nLe montant disponible sur votre compte est de : %.2f\n", comptes(id))
      }
      if(liste == 5) {
        println("Fin des opérations, n'oubliez pas de récupérer votre carte.")
      }
      if(liste == 1) {
        depot(id, comptes)
      }
      if(liste == 2){
        retrait(id, comptes)
      }
      if(liste == 4){
        changepin(id, codespin)
      }
    } 
  }
}
