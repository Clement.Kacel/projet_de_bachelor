//Assignment: Diego Otero Gerpe_996609_assignsubmission_file

import scala.io.StdIn.readLine

object Main {
    
    val nb_clients = 100
    var comptes : Array[Double] = Array.fill(nb_clients)(1200.0)
    var codespin: Array[String] = Array.fill(nb_clients)("INTRO1234")
    
    val EURO_A_CHF : Double = 0.95
    val CHF_A_EURO : Double = 1.05
    
    // on definit id ici pour qu'elle soit accessible a la fonction demande_id_et_pin
    var id : Int = 0   

    def main(args: Array[String]) : Unit = {

        // permet de garder l'ecran d'identification de rester a apparaitre a chaque fois qu'un utilisateur a terminder d'utiliser la machine
        while (true) {

            var validite_de_user : Boolean = demande_id_et_pin()
            
            // tant que l'utilisateur ne veut pas quitter son compte et qu'il s'est identifier
            while (validite_de_user) {
                
                menu_a_afficher()
                var choix_menu : String = readLine("Votre Choix : ")
                
                while(choix_menu.toInt < 0 || choix_menu.toInt > 5) {
                    println("Choix incorrecte, entrer un choix baser sur les options precendentes")
                    choix_menu = readLine("Votre Choix : ")
                }
                
                choix_menu match {
                    case "1" => depot(id)
                    case "2" => retrait(id)
                    case "3" => println(s"Le montant disponible sur votre compte est de : ${comptes(id)}")
                    case "4" => changepin(id)
                    case "5" => 
                        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
                        validite_de_user = false // l'equivalent de faire "log out" sur un site, le site nous demandera apres de nous re-identifier
                }
            }
        }    
    }

    def demande_id_et_pin() : Boolean = {
        id = readLine("Saisissez votre code identifiant > ").toInt

        // tant que l'utilisateur n'a pas mis un ID correct
        while (id < 0 || id >= 100) {
            println("Cet identifiant n’est pas valable.")
            id = readLine("Saisissez votre code identifiant > ").toInt
        }

        var codepin : String = readLine("Saisissez votre code pin > ")
        var tentatives_code_pin : Int = 2

        // tant que l'utilisateur n'a pas mis son pincode correct, et qu'il lui reste des tentatives
        while (codespin(id) != codepin && tentatives_code_pin > 0) {
            println(s"Code pin erroné, il vous reste $tentatives_code_pin tentatives >")
            codepin = readLine("Saisissez votre code pin > ")
            tentatives_code_pin -= 1
        }

        // si l'utilisateur a echouer 3 fois de mettre son pin
        if (tentatives_code_pin == 0) {
            println("Pour votre protection, les opérations bancaires vont s’interrompre, récupérez votre carte.")
            println()
            return false
        }

        return true
    }
   
    // aide a convertir un montant en CHF en euro
    def convertit_chf_to_eur(amount: Int) : Double = {
        var valeur_eur : Double= amount * CHF_A_EURO
        return valeur_eur
    }
    
    // aide a convertir un montant en Euro en CHF 
    def convertit_eur_to_chf(amount: Int) : Double = {
        var valeur_chf : Double = amount * EURO_A_CHF
        return valeur_chf
    }
    
    def menu_a_afficher() : Unit = {
        println("Choisissez votre opération :")
        println("1) Dépôt")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")
        println()
    }

    def changepin(id : Int) : Unit = {
      var nouveau_pin = readLine("Entrez un nouveau code PIN : ")

      while (nouveau_pin.length < 8) {
      println("Votre code PIN doit contenir aumoins 8 characteres.")
      nouveau_pin = readLine("Entrez un nouveau code PIN qui doit contenir aumoins 8 characteres : ")
      }

      codespin(id) = nouveau_pin
      println("code PIN changer avec succés.")

    }

    def depot(id: Int) : Unit = {
        print("Indiquez la devise du dépôt : 1) CHF ; 2) EUR > ")
        var choix_devise : String = readLine()

        while (choix_devise != "1" && choix_devise != "2") {
            print("Choix de la devise invalide. Veuillez re-entrez sois 1 sois 2 : ")
            choix_devise = readLine()
        }

        print("Indiquez le montant du dépôt > ")
        var montant_depot : Int = readLine().toInt

        while (montant_depot % 10 != 0) {
            println("Le montant doit être un multiple de 10.")
            print("Indiquez le montant du dépôt > ")
            montant_depot = readLine().toInt

        }

        var monnaie : String = "CHF"
        if (choix_devise == "2") {
            montant_depot = convertit_eur_to_chf(montant_depot).toInt
            monnaie = "Euros"
        }

        comptes(id) += montant_depot
        printf(s"Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))

    }

    def retrait(id: Int) : Unit = {
        print("Indiquez la devise : 1) CHF ; 2) EUR > ")
        var choixDevise = readLine()

        while (choixDevise != "1" && choixDevise != "2") {
            print("Choix invalide. Veuillez choisir 1) CHF ou 2) EUR : > ")
            choixDevise = readLine()
        }   

        val limiteRetrait = (comptes(id) * 0.1).toInt
        var montantRetrait = readLine("Indiquez le montant du retrait > ").toInt

        while (montantRetrait % 10 != 0) {
            println("Le montant doit être un multiple de 10.")
            montantRetrait = readLine("Indiquez le montant du retrait > ").toInt
        }

        while (montantRetrait > limiteRetrait) {
            println(s"Votre limite de retrait autorisée est de : $limiteRetrait")
            montantRetrait = ("Indiquez le montant du retrait > ").toInt
        }

        val billetsEuros = Array(100, 50, 20, 10)
        val billetsCHF = Array(500, 200, 100, 50, 20, 10)
        var billetsAUtiliser = Array[Int]()

        var montantAretirerDuCompte = montantRetrait
        if (choixDevise == "1" && montantRetrait >= 200) {
            var choix_coupures = readLine("En 1) grosses coupures, 2) petites coupures > ") 
            match {
                case "1" => billetsAUtiliser = billetsCHF
                case "2" => billetsAUtiliser = billetsCHF.drop(2)
                case _ => billetsAUtiliser = billetsCHF
            }
        }

        else {
            montantAretirerDuCompte = montantRetrait
            billetsAUtiliser = billetsEuros
        }

        var billets_a_retirer : Array[Int] = Array()
        var montants_des_billets_a_retirer : Array[Int] = Array()


        // comme sa on peut dierctement retirer du compte le montant a retirer et l'afficher a l'utilisateur
        var montantRestant = montantAretirerDuCompte
        for (billet <- billetsAUtiliser if montantRestant >= billet) {

            var nom_monnaie : String = ""
            if (choixDevise == "1") {
                nom_monnaie = "CHF"
            } 
            else {
               nom_monnaie = "Euro"
            }

            val nbBilletsMax = montantRestant / billet
            println(s"Il reste $montantRestant à distribuer. \nVous pouvez obtenir un maximum de $nbBilletsMax billet(s) de $billet $nom_monnaie ")
            val reponse = readLine("Entrez le nombre de billets à retirer ou 'o' pour le maximum > ")

            var nbBillets = 0
            if (reponse == "o") {
                nbBillets = nbBilletsMax
            }
            else {
                nbBillets = reponse.toInt
            }

            montantRestant -= nbBillets * billet
            montants_des_billets_a_retirer = montants_des_billets_a_retirer :+ nbBillets
            billets_a_retirer = billets_a_retirer :+ billet
            }

            println("Veuillez retirer la somme demandée :")
            for (i <- 0 to montants_des_billets_a_retirer.length-1) {
                var x = billets_a_retirer(i)
                var y = montants_des_billets_a_retirer(i) 
                if (choixDevise == "1") {
                    println(s"$y billet(s) de $x CHF")
                }
                else {
                    println(s"$y billet(s) de $x Euros")
                }
            }

            montantAretirerDuCompte = convertit_eur_to_chf(montantAretirerDuCompte).toInt
            comptes(id) -= montantAretirerDuCompte
            val solde = comptes(id)
            println(s"Votre retrait a été traité, le nouveau montant disponible sur votre compte est : $solde")
    

    }

    

}
