//Assignment: Yodit Wanofi Tesfaye_996582_assignsubmission_file

object Main {

  def saisieIdentifiant(nbClients: Int): Int = {
    var identifiant = -1

    while (identifiant < 0 || identifiant >= nbClients) {
      println("Saisissez votre code identifiant >")
      identifiant = scala.io.StdIn.readInt()

      if (identifiant >= nbClients || identifiant < 0) {
        println("Cet identifiant n'est pas valable.")
        System.exit(0)
      }
    }

    identifiant
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    val tauxConversionEURtoCHF: Double = 0.95

    var deviseDepot = 0
    do {
      println("Indiquez la devise de depot: \n1) CHF \n2) EUR")
      deviseDepot = scala.io.StdIn.readInt()
      if (deviseDepot != 1 && deviseDepot != 2) {
        println("Veuillez choisir une option valide.")
      }
    } while (deviseDepot != 1 && deviseDepot != 2)

    var montantDepot = 0
    do {
      println("Indiquez le montant du depot > ")

      montantDepot = scala.io.StdIn.readInt()

      if (montantDepot % 10 != 0) {
        println("Le montant doit etre un multiple de 10.")
      }
    } while (montantDepot % 10 != 0)

    if (deviseDepot == 2) {
      val montantDepotCHF = montantDepot * tauxConversionEURtoCHF
      comptes(id) += montantDepotCHF
    } else {
      comptes(id) += montantDepot
    }
    printf("Votre depot a ete pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF\n", comptes(id))
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {
    val tauxConversionCHFtoEUR: Double = 1.05

    var deviseRetrait = 0
    do {
      println("Indiquez la devise de retrait : \n1) CHF \n2) EUR")
      deviseRetrait = scala.io.StdIn.readInt()

      if (deviseRetrait != 1 && deviseRetrait != 2) {
        println("Veuillez choisir une option valide.")
      }
    } while (deviseRetrait != 1 && deviseRetrait != 2)

    var montantRetrait = 0.0
    do {
      println("Indiquez le montant du retrait > ")
      montantRetrait = scala.io.StdIn.readDouble()

      if (montantRetrait % 10 != 0) {
        println("Le montant doit etre un multiple de 10.")
      } else if (montantRetrait > comptes(id) * 0.1) {
        println(s"Votre plafond de retrait autorise est de : ${comptes(id) * 0.1}")
      }
    } while (montantRetrait % 10 != 0 || montantRetrait > comptes(id) * 0.1)

    var montantRetraitCHF = montantRetrait

    if (deviseRetrait == 2) {
      montantRetraitCHF = montantRetrait / tauxConversionCHFtoEUR
    }

    var coupuresDisponibles: Array[Int] = Array()

    if (montantRetrait >= 200) {
      println("1) grosses coupures \n2) petites coupures >")
      val choixCoupures = scala.io.StdIn.readLine()
      coupuresDisponibles =
        if (choixCoupures == "1") Array(500, 200, 100, 50, 20, 10)
        else Array(100, 50, 20, 10)
    } else {
      coupuresDisponibles = Array(100, 50, 20, 10)
      println("Le montant ne permet pas d'utiliser de grosses coupures. Distribution en petites coupures.")
    }

    var coupuresDistribuees: List[(Int, Int)] = List()
    var coupure10Obligatoire = false

    for (coupure <- coupuresDisponibles) {
      var montantRestant = montantRetrait
      val nombreCoupuresMax = (montantRestant / coupure).toInt

      if (nombreCoupuresMax > 0) {
        var choixClient = 0
        do {
          println(s"Il reste $montantRestant CHF à distribuer")
          println(s"Vous pouvez obtenir au maximum $nombreCoupuresMax billet(s) de $coupure CHF")

          if (coupure == 10 && coupuresDistribuees.nonEmpty) {
            println("Vous etes oblige de prendre le montant restant en coupures de 10 CHF.")
            choixClient = nombreCoupuresMax
            coupure10Obligatoire = true
          } else {
            print("Tapez 'o' pour ok ou une autre valeur inferieure à celle proposee >")
            val saisie = scala.io.StdIn.readLine()

            if (coupure == 10 && saisie.toLowerCase() != "o") {
              println("Vous etes oblige de prendre le montant restant en coupures de 10 CHF.")
              choixClient = nombreCoupuresMax
              coupure10Obligatoire = true
            } else if (saisie.toLowerCase() == "o") {
              choixClient = nombreCoupuresMax
            } else {
              choixClient = saisie.toInt
            }
          }
        } while (choixClient < 0 || choixClient > nombreCoupuresMax)

        montantRestant -= choixClient * coupure
        montantRetrait -= choixClient * coupure

        coupuresDistribuees = (coupure, choixClient) :: coupuresDistribuees
      }
    }

    comptes(id) -= montantRetraitCHF

    println("Veuillez retirer la somme demandee:")

    for ((coupure, nombre) <- coupuresDistribuees.reverse) {
      if (nombre > 0) {
        println(s"$nombre billet(s) de $coupure CHF")
      }
    }

    if (coupure10Obligatoire) {
      comptes(id) += montantRetrait.toInt
      println(f"Votre retrait a ete pris en compte, le nouveau montant disponible sur votre compte est de : ${"%.2f".format(comptes(id))} CHF.")
    } else {
      val nouveauMontant = BigDecimal(comptes(id)).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
      println(f"Votre retrait a ete pris en compte, le nouveau montant disponible sur votre compte est de : ${"%.2f".format(nouveauMontant)} CHF.")
    }
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {
    var nouveauPin: String = ""
    do {
      println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
      nouveauPin = scala.io.StdIn.readLine()

      if (nouveauPin.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      }
    } while (nouveauPin.length < 8)

    codespin(id) = nouveauPin
    println("Votre code pin a ete modifie avec succes.")
  }

  def main(args: Array[String]): Unit = {
    val nbClients = 101
    var comptes: Array[Double] = Array.fill(nbClients)(1200.0)
    var codesPin: Array[String] = Array.fill(nbClients)("INTRO1234")

    var identifiant = saisieIdentifiant(nbClients)

    do {
      var nbTentatives = 3
      var codeSaisi: String = ""

      while (nbTentatives > 0 && codeSaisi != codesPin(identifiant)) {
        println("Saisissez votre code pin >")
        codeSaisi = scala.io.StdIn.readLine()

        if (codeSaisi != codesPin(identifiant)) {
          nbTentatives -= 1
          if (nbTentatives > 0) {
            println(s"Code pin errone, il vous reste $nbTentatives tentatives >")
          } else {
            println("Trop d'erreurs, abandon de l'identification")
            System.exit(0)
          }
        }
      }

      var choix = 0

      do {
        println("Choisissez votre operation :")
        println("1) Depot")
        println("2) Retrait")
        println("3) Consultation du compte")
        println("4) Changement du code pin")
        println("5) Terminer")

        println("Votre choix :")
        choix = scala.io.StdIn.readInt()

        if (choix == 1) {
          depot(identifiant, comptes)
        } else if (choix == 2) {
          retrait(identifiant, comptes)
        } else if (choix == 3) {
          println(s"Le montant disponible sur votre compte est de : ${comptes(identifiant)}")
        } else if (choix == 4) {
          changepin(identifiant, codesPin)
        } else if (choix == 5) {
          println("Fin des operations, n’oubliez pas de recuperer votre carte.")
        } else {
          println("Choix non valide, veuillez reessayer.")
        }

      } while (choix != 5)

      identifiant = saisieIdentifiant(nbClients)

    } while (true)
  }
}