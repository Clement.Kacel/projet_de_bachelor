//Assignment: Nicola Massimiliano Quadri_996601_assignsubmission_file

  import scala.io.StdIn._
  

  object Main {
    def main(args: Array[String]): Unit = {

var choix = 0 
var B = false
var E = true 
var tentatives = 3
var choixmax = 5
var codeidentifiant = 0
var comptes = Array.fill(100) (1200.0)
var codespin = Array.fill(100) ("INTRO1234")
var nbcomptes = 100
      while (E){
        var B = false
        while(!B && E){
          println("Saisissez votre code identifiant >")
          codeidentifiant = readInt()
          if(codeidentifiant >= nbcomptes){
            println("Cet identifiant n'est pas valable")
            E = false
          }
          if (E){
            tentatives = 3
            while (tentatives > 0 && !B){
              println("Saisissez votre code PIN >")
              var pin = readLine()
              if (pin == codespin(codeidentifiant)){
                B = true
              } else {
                tentatives -= 1
                if(tentatives == 0){
                  println("Pour votre protection, les opérations bancaires vont s'interrompre. Récupérez votre carte.")
                } else { println("Code pin erroné, il vous reste " + tentatives + " tentatives >")
                       }
              }
            }
          }
        }
        choix = 0 
        while (choix != choixmax && B){
          println("Choisissez votre opération :")
          println("1) Dépôt ")
          println("2) Retrait ")
          println("3) Consultation du compte ")
          println("4) Changement du code pin ")
          println("5) Terminer ")
          choix = readInt()
          val K = choix == 1 || choix == 2 || choix == 3 || choix == 4 || choix == 5
          if(!K ){
            println("opération invalide")
          }else{
            if(choix == 1 && B){
              depot(codeidentifiant,comptes)
            }else if(choix == 2 && B){
              retrait(codeidentifiant,comptes)
            }else if (choix == 3 && B){
              println("Le montant disponible sur votre compte est : " + comptes(codeidentifiant))
            } else if (choix == 4 && B){
              changepin(codeidentifiant,codespin)
              }else if (choix == 5){
                println("Fin des opérations, n'oubliez pas de récupérer votre carte")
            }
          }
        }
      }
    }
 def depot(id : Int, comptes : Array[Double]) : Unit = {
      var devisedepot = 3
      while (devisedepot != 2 && devisedepot != 1) {
        println("Indiquez la devise du dépôt : 1) CHF ; 2) EURO >")
        devisedepot = readInt()
      }
      var B = !true
      var montantdepot = 0;
      while (!B){
        println("Indiquez le montant du dépôt >")
        montantdepot = readInt()
        if (montantdepot % 10 == 0){
          B = true
        }
      }
      if (devisedepot == 1){
        comptes(id) += montantdepot
      } else {
        comptes(id) += 0.95 * montantdepot
      }
      comptes(id) = math.floor(comptes(id) * 100) / 100
      println("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est > " + comptes(id))
    }
 def retrait(id : Int, comptes : Array[Double]) : Unit = {
      var deviseretrait  = 3
      while (deviseretrait != 2 && deviseretrait != 1) {
        println("Indiquez la devise :1) CHF 2:) EUR")
        deviseretrait  = readInt()
      }
      var B = false
      var montantretrait = 0
      val maxretrait = comptes(id) * 0.1 
      while (!B) {
        println("indique le montant du retrait >")
        montantretrait = readInt()
        if (montantretrait%10 == 0) {
          if (montantretrait <= maxretrait){
            B = true
          } else {println("Votre plafond de retrait autorisé est de :" + maxretrait)
                 }
        } else{ println("Le montant doit être un multipme de 10")
              }
      }
      var choixcoupure = 3
      if(deviseretrait == 1) {
        if (montantretrait >= 200) {
          while (choixcoupure != 2 && choixcoupure != 1) {
            println("En, 1) Grosses coupures 2) Petites coupures >")
            choixcoupure = readInt ()
            if (choixcoupure != 2 && choixcoupure != 1) {
              println("Choix invalide.")
            }
          }
        }
      }
      var montantrestant = montantretrait
      if (deviseretrait == 1) {
        if (choixcoupure == 1) {
          while (montantrestant > 0){
            if(montantrestant / 500 >= 1){
              println((montantrestant / 500).toInt+" Billets de 500 CHF ")
              montantrestant = montantrestant % 500
            }
            else if(montantrestant / 200 >= 1){
              println((montantrestant / 200).toInt+" Billets de 200 CHF ")
              montantrestant = montantrestant % 200
            }
            else if(montantrestant / 100 >= 1){
              println((montantrestant / 100).toInt+" Billets de 100 CHF ")
              montantrestant = montantrestant % 100
            }
            else if(montantrestant / 50 >= 1){
              println((montantrestant / 50).toInt+" Billets de 50 CHF ")
              montantrestant = montantrestant % 50
            }
            else if(montantrestant / 20 >= 1){
              println((montantrestant / 20).toInt+" Billets de 20 CHF ")
              montantrestant = montantrestant % 20
            }
            else if(montantrestant / 10 >= 1) {
              println((montantrestant / 10).toInt +" Billets de 10 CHF ")
              montantrestant = montantrestant % 10
            }
          }
        }
        else {
          while (montantrestant > 0){
            if(montantrestant / 100 >= 1){
              println((montantrestant / 100).toInt +" Billets de 100 CHF ")
              montantrestant = montantrestant % 100
            }
            else if(montantrestant / 50 >= 1){
              println((montantrestant / 50).toInt +" Billets de 50 CHF ")
              montantrestant = montantrestant % 50
            }
            else if(montantrestant / 20 >= 1){
              println((montantrestant / 20).toInt +" Billets de 20 CHF ")
              montantrestant = montantrestant % 20
            }
            else if(montantrestant / 10 >= 1) {
              println((montantrestant / 10).toInt +" Billets de 10 CHF ")
              montantrestant = montantrestant % 10
            }
          }
        }
      }
      else {
        while (montantrestant > 0){
          if(montantrestant / 100 >= 1){
            println((montantrestant / 100).toInt+" Billets de 100 EUR ")
            montantrestant = montantrestant % 100
          }
          else if(montantrestant / 50 >= 1){
            println((montantrestant / 50).toInt+" Billets de 50 EUR ")
            montantrestant = montantrestant % 50
          }
          else if(montantrestant / 20 >= 1){
            println((montantrestant / 20).toInt+" Billets de 20 EUR ")
            montantrestant = montantrestant % 20
          }
          else if(montantrestant / 10 >= 1) {
            println((montantrestant / 10).toInt+" Billets de 10 EUR ")
            montantrestant = montantrestant % 10
          }
        }
      }
      if (deviseretrait == 1) { comptes(id) -= montantretrait
                            }else{ comptes(id) -= montantretrait * 0.95
                                 }
      comptes(id) = math.floor(comptes(id) * 100) / 100
      println ("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est >" + comptes(id))
    }
def changepin(id : Int, codespin : Array[String]) : Unit = {
      var nouveaupin = ""
      while (nouveaupin.length < 8){
        println("Saisissez votre nouveau code pin ( il doit contenir au moins 8 caractères >")
        nouveaupin = readLine()
        if (nouveaupin.length < 8) {
          println("Votre code pin ne contient pas au moins 8 caractères")
        }
      }
      codespin(id) = nouveaupin
    }
  }