//Assignment: Yuel Amanuel_996521_assignsubmission_file

import scala.io.StdIn._

object Main {

  val nbClients: Int = 100
  val initMontant: Double = 1200.00
  val initCodePin = "INTRO1234"
  val comptes: Array[Double] = Array.fill(nbClients)(initMontant)
  val codesPin: Array[String] = Array.fill(nbClients)(initCodePin)

  def main(args: Array[String]): Unit = while (true) {
    var identifierCode = -1
    println("Saisissez votre code identifiant >")
    identifierCode = readInt()
    if (identifierCode < 0 || identifierCode >= nbClients) {
      println("Cet identifiant n’est pas valable")
      sys.exit()
    }

    var tentative = 0
    val maxTentative = 3
    var restTentative = maxTentative
    var pinSelect = ""
    do {
      println("")
      println("Saisissez votre code pin")
      pinSelect = readLine()
      if (pinSelect != codesPin(identifierCode)) {
        tentative = tentative + 1
        restTentative = maxTentative - tentative
        println("")
        println("Code pin erroné, il vous reste " + restTentative + " tentatives")
      }
    } while (restTentative > 0 && pinSelect != codesPin(identifierCode))
    // PIN ERRONE 3 FOIS EXIT PROGRAM
    if (restTentative == 0) {
      println("")
      println("Trop d’erreurs, abandon de l’identification")
    } else {
      // verifier si option est disponible dans menu
      var optionSelect: Int = 0
      val menu = "Choisissez votre opération : \n 1) Dépôt \n 2) Retrait \n 3) Consultation du compte \n 4) Changement du code pin \n 5) Terminer \n Votre choix :"
      // OPTION MENU OK
      do {
        println(menu)
        optionSelect = readInt()

        // OPTION NUMERO 1 : DEPOT
        if (optionSelect == 1) {
          depot(identifierCode, comptes)
        }

        // OPTION NUMERO 2 : RETRAIT
        if (optionSelect == 2) {
          retrait(identifierCode, comptes)
        }

        // OPTION NUMERO 3 : CONSULTATION
        if (optionSelect == 3) {
          println("")
          printf("Le montant disponible sur votre compte est de : %.2f CHF \n", comptes(identifierCode))
        }

        // OPTION NUMERO 4 : Changement du code pin
        if (optionSelect == 4) {
          changepin(identifierCode, codesPin)
        }

        // OPTION NUMERO 5 : TERMINER
        if (optionSelect == 5) {
          println("")
          println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        }

        // REPETER MENU
      } while (1 <= optionSelect && optionSelect <= 4)
    }
  }

  def depot(id: Int, comptes: Array[Double]): Unit = {
    var devise = 1
    var montantInsere = 0
    val devCHF = 1
    var montantConverti: Double = 0.00
    val EURaCHF: Double = 0.95
    // DEVISE
    do {
      println("")
      println("Indiquez la devise du dépôt :\n 1) CHF \n 2) EUR ")
      devise = readInt()
    } while (devise == 0 || devise > 2)
    // MONTANT
    do {
      println("")
      println("Indiquez le montant du dépôt :")
      montantInsere = readInt()
      if (montantInsere % 10 != 0) {
        println("")
        println("Le montant doit être un multiple de 10")
      }
    } while (montantInsere % 10 != 0)
    // TOTAL MONTANT
    if (devise == devCHF) {
      comptes(id) = comptes(id) + montantInsere
    } else {
      montantConverti = montantInsere * EURaCHF
      comptes(id) = comptes(id) + montantConverti
    }
    println("")
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
  }

  def retrait(id: Int, comptes: Array[Double]): Unit = {
    var coupuresChoix = 2
    var positionArray = -1
    var positionArrayFinal = -1
    var billets = 0
    var coupures: Array[Int] = Array(500, 200, 100, 50)
    val coupuresGross: Array[Int] = Array(500, 200, 100, 50, 20, 10)
    val coupuresPetit: Array[Int] = Array(100, 50, 20, 10)
    val billetsFinal: Array[Int] = Array(0, 0, 0, 0, 0, 0)
    val coupuresFinal: Array[Int] = Array(0, 0, 0, 0, 0, 0)
    var coupuresDecision = "o"
    var coupuresMaxAccepte = 0
    var coupuresValeur = 0
    var devise = 1
    var montantRetrait = 0
    val devCHF = 1
    var montantRest = 0
    val devString = "CHF"
    var montantConverti: Double = 0.00
    val EURaCHF: Double = 0.95
    // DEVISE
    do {
      println("")
      println("Indiquez la devise :\n 1) CHF \n 2) EUR ")
      devise = readInt()
    } while (devise != 1 && devise != 2)
    do {
      if (devise == 1) {
        println("Indiquez le montant du retrait :")
        montantRetrait = readInt()
        if (montantRetrait % 10 != 0) {
          println("")
          println("Le montant doit être un multiple de 10")
        }
        if (montantRetrait > comptes(id) * 0.1) {
          println("")
          println("Votre plafond de retrait autorisé est de : " + comptes(id) * 0.1 + "CHF")
        }
      }

      if (devise == 2) {
        println("Indiquez le montant du retrait :")
        montantRetrait = readInt()
        if (montantRetrait % 10 != 0) {
          println("")
          println("Le montant doit être un multiple de 10")
        }
        if (montantRetrait > comptes(id) * 0.1) {
          println("")
          println("Votre plafond de retrait autorisé est de : " + comptes(id) * 0.1 * 1.05 + "EUR")
        }
      }
    } while (montantRetrait % 10 != 0 || montantRetrait > comptes(id) * 0.1)


    // COUPURES
    if (montantRetrait >= 200 && devise == devCHF) {
      do {
        println("")
        println("En :\n 1) Grosses coupures \n 2) Petites coupures ")
        coupuresChoix = readInt()
        if (coupuresChoix == 1) {
          coupures = coupuresGross
        } else {
          coupures = coupuresPetit
        }
      } while (devise == 0 || devise > 2)
    } else {
      coupures = coupuresPetit
    }
    montantRest = montantRetrait
    // NOMBRE DE COUPURES
    do {
      // OPTION DE BILLET PLUS GRAND A DONNER
      do {
        billets = 0
        positionArray = positionArray + 1
        coupuresValeur = coupures(positionArray)
        if (montantRest / coupuresValeur > 0) {
          billets = montantRest / coupuresValeur
        }
      } while (billets == 0)
      println("")
      println("Il reste " + montantRest + " " + devString + " à distribuer")
      println("Vous pouvez obtenir au maximum " + billets + " billet(s) de " + coupuresValeur + " " + devString + ".")
      println("Tapez o pour ok ou une autre valeur inférieure à celle proposée")
      coupuresDecision = readLine()
      // COUPURE MAX NO ACCEPTE
      if (coupuresDecision != "o") {

          coupuresMaxAccepte = coupuresDecision.toInt
          if (coupuresMaxAccepte < billets) {
            positionArrayFinal = positionArrayFinal + 1
            billetsFinal(positionArrayFinal) = coupuresMaxAccepte
            coupuresFinal(positionArrayFinal) = coupuresValeur
            montantRest = montantRest - coupuresMaxAccepte * coupuresValeur
          }

      }
      // COUPURE ACCEPTE
      if (coupuresDecision == "o") {
        positionArrayFinal = positionArrayFinal + 1
        montantRest = montantRest - (billets * coupuresValeur)
        billetsFinal(positionArrayFinal) = billets
        coupuresFinal(positionArrayFinal) = coupuresValeur
      }
    } while (montantRest > 0)
    // RETRAIT FINAL
    println("")
    println("Veuillez retirer la somme demandée :")
    for (i <- 0 to 5) {
      if (billetsFinal(i) > 0 && coupuresFinal(i) > 0) {
        println(billetsFinal(i) + " billet(s) de " + coupuresFinal(i) + devString + ".")
      }
    }

    // MONTANT RESTANT APRES RETRAIT
    if (devise == devCHF) {
      comptes(id) = comptes(id) - montantRetrait
    } else {
      montantConverti = montantRetrait * EURaCHF
      comptes(id) = comptes(id) - montantConverti
    }
    println("")
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
  }

  def changepin(id: Int, codespin: Array[String]): Unit = {
    println("")
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    var pinCode = ""
    do {
      pinCode = readLine()
      if (pinCode.length < 8)
        println("Votre code pin ne contient pas au moins 8 caractères")
    } while (pinCode.length < 8)

    codespin(id) = pinCode
  }
}