//Assignment: Marwan Jonas Yamnahakki_996445_assignsubmission_file

import scala.io.StdIn.readInt
import scala.io.StdIn.readLine
object Main {
def main(args: Array[String]): Unit = {
  var CodePinOK = "INTRO1234" //la valeur de notre Code Pin
  var MontantDisp = 1200.0 //montant disponible
  var nbclients=100
  var comptes=Array.fill(nbclients)(MontantDisp)
  var codespin=Array.fill(nbclients)(CodePinOK)
  var id = 0
  var CodePin = "PIN" 
  var FinProgramme= false
  while (!FinProgramme){// debut execution du programme 
    var choixId=0 
    println("Saisissez votre code identifiant") 
    choixId=readLine().toInt
    if (choixId > 100 || choixId < 1) {
          println("Cet identifiant n'est pas valable.")
      }else{
      id = choixId - 1
      var Pin = false
      var NbPinTent = 3
      while ((Pin == false) && (NbPinTent > 0)) {
        println("Entrez votre code pin :")
        CodePin = readLine().toString
        if(CodePin == codespin(id)) {Pin = true} else { 
        NbPinTent= NbPinTent - 1
        println("Code pin erroné, il vous reste " + NbPinTent + " tentatives")
        if(NbPinTent == 0) {
          println("Pour votre protection, les opérations bancaires vont") 
          println("s'interrompre. Récupérez votre carte.")
         }
        }
      }
      var ChoixOp = 0
      while (ChoixOp != 5){
       var Choix = false 
       while (Choix == false) {//affichage du menu
           println("Choisissez votre opération :")
           println("\t1) Dépôt")
           println("\t2) Retrait")
           println("\t3) Consultation du compte")
           println("\t4) Changement du code pin")
           println("\t5) Terminer")
           println("Votre choix : ")
           ChoixOp= readLine().toInt
           if((ChoixOp >= 1) && (ChoixOp <= 5)) {Choix = true} else {Choix= false}  
           //code pin juste et depot
           if(ChoixOp == 1 )  depot(id,comptes)  
           else if(ChoixOp == 2 )  retrait(id,comptes)
           else if(ChoixOp == 4 ) changepin(id,codespin)
           else if(ChoixOp == 3 ){ // consulter son compte
             printf("Le montant disponible sur votre compte est : %.2f", comptes(id))
             println(" CHF")
             }
           else if(ChoixOp == 5){ // quitter le bancomat
             println("Fin decs opérations, n'oubliez pas de récupérer votre carte")}
       }
      }
    }
} //Fin execution du programme
// Debut de définition des fonctions utilisées
// Fonction de dépot 
def depot(id:Int,comptes:Array[Double]):Unit ={ 
  var Devise = 0 // devise choisie par l'utilisateur
  var DeviseAffiche = "CH"
  while (Devise != 1 && Devise != 2) {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR ")
    Devise= readLine().toInt
  }
  var Operation = false
  var MontantDep = 0;// montant du depot
  while (!Operation){
    println("Indiquez le montant du dépôt ")
    MontantDep= readLine().toInt
    if(MontantDep % 10 == 0 && MontantDep>0){Operation= true}else{
      println("Le montant doit être un multiple de 10")
    }
  }
  if(Devise == 2) {
    DeviseAffiche = "EUR"
    comptes(id) += MontantDep * 0.95 
  }else{
    DeviseAffiche = "CHF"
    comptes(id) += MontantDep
  }
  print("Votre dépôt a été pris en compte, le nouveau montant disponible ")
  print("sur votre compte est de : " + comptes(id) +" " + "CHF" + "\n" )
}
// Fonction de retrait 
def retrait(id:Int,comptes:Array[Double]):Unit ={
  var DeviseR = 0 // devise du retrait
  var DeviseRAffiche = ""
  var DeviseRTaux = 1.0
  while (DeviseR != 1 && DeviseR != 2) {
    println("Indiquez la devise :1 CHF, 2: EUR")
    DeviseR= readLine().toInt
  }
  var Operation = false
  var Retrait = 0 // valeur du retrait
  var RetraitAutorise = comptes(id) / 10.0 // valeur du retrait max autorise
  var NbBillet500 = 0 // nombre de billets de 500 
  var NbBillet200 = 0 // nombre de billets de 200
  var NbBillet100 = 0 // nombre de billets de 100
  var NbBillet50 = 0 // nombre de billets de 50
  var NbBillet20 = 0 // nombre de billets de 20
  var NbBillet10 = 0 // nombre de billets de 10
  if ( DeviseR == 1 ){ 
    DeviseRAffiche = "CHF"
    DeviseRTaux = 1.0
     }else {
    DeviseRAffiche = "EUR"
    DeviseRTaux = 0.95
  }
  while (!Operation){
   println("Indiquez le montant du retrait ")
   Retrait= readInt()
   if(Retrait % 10 == 0 ){
      if(Retrait <= RetraitAutorise && Retrait>0){Operation= true}
      else{println("Votre plafond de retrait autorisé est de :" + RetraitAutorise)}
      }else{println("Le montant doit être un multiple de 10")}
  }
  var Coupure = 0 // grosses ou petites coupures
  if(DeviseR == 1){ //  retrait en CHF
    if(Retrait >= 200){ 
      while (Coupure != 1 && Coupure != 2) { 
        println("En, 1) Grosses coupures 2) Petites coupures >")
        Coupure = readInt()
        if(Coupure != 1 && Coupure != 2 ){println("choix pas possible")}
        }
    }else{Coupure=2}

  }else{Coupure = 2}
  println("Veuillez retirer la somme demandée :")
  var Reste = Retrait  // Initialisation du reste
  calcReste ( DeviseR,Coupure) // Calcul du reste
  //Affichage du retrait par billet
  if(NbBillet500>0){println(s"$NbBillet500 billet(s) de 500 $DeviseRAffiche")}
  if(NbBillet200>0){println(s"$NbBillet200 billet(s) de 200 $DeviseRAffiche")}
  if(NbBillet100>0){println(s"$NbBillet100 billet(s) de 100 $DeviseRAffiche")}
  if(NbBillet50>0){println(s"$NbBillet50 billet(s) de 50 $DeviseRAffiche")}
  if(NbBillet20>0){println(s"$NbBillet20 billet(s) de 20 $DeviseRAffiche")}
  if(NbBillet10>0){println(s"$NbBillet10 billet(s) de 10 $DeviseRAffiche")}
  comptes(id) -=  (Retrait * DeviseRTaux)
  print("Votre retrait a été pris en compte, le nouveau montant disponible ")
  print("sur votre compte est : "  +comptes(id)+ " " + "CHF" + "\n")
  //Fin execusion du retrait
def calculResteParBillet ( Billet : Int , Devise : Int, Coupure : Int) :Int= {// fonction qui prend comme valeur premierement la valeur du billet puis la devise choisie par l'utilisateur. 
  var NbBillet = 0
  var NbBilletDefault = Reste / Billet 
  var DeviseDisplay = ""
  if (Devise != 1 ) { DeviseDisplay = "EUR" } else { DeviseDisplay = "CHF" } 
  if ((Coupure == 2)&&(Billet==500 || Billet == 200)) {NbBillet = 0 }
//     else if((Coupure == 2)&&(Billet==200)) {NbBillet =0}
     else{
          if(NbBilletDefault >= 1){ 
           println(s"Il reste $Reste $DeviseDisplay à distribuer") 
           print("Vous pouvez obtenir au maximum ") 
           print(NbBilletDefault) 
           println(s" billet(s) de $Billet $DeviseDisplay ") 
           var selectionbillet = ""  
           do { 
              println(s"Tapez o pour ok ou une autre valeur inférieure ")
              println("à celle proposée ") 
              selectionbillet = readLine() 
              if (selectionbillet != "o") {  
                 NbBillet = selectionbillet.toInt 
                 if (NbBillet > NbBilletDefault) {
                   println("Veuillez saisir une valeur valable.")}
                  else{selectionbillet="o"} 
               }else{  
                NbBillet = NbBilletDefault} 
              }while (selectionbillet != "o")
            }
         }
  Reste = Reste - (NbBillet * Billet)  
  return NbBillet;
}
// fonction qui calcule le reste en euros ou en francs  
def calcReste ( Dev : Int , Coup : Int ) : Unit = {
var DevAff = ""
var NbBillet10Def = 0
if(Dev == 1 ) { DevAff = "CHF"} else { DevAff = "EUR"}
while ( Reste > 0 ) {
   NbBillet500=calculResteParBillet ( 500 , Dev, Coup)
   NbBillet200=calculResteParBillet ( 200 , Dev, Coup)
   NbBillet100=calculResteParBillet ( 100 , Dev, Coup)
   NbBillet50=calculResteParBillet  ( 50 , Dev , Coup)
   NbBillet20=calculResteParBillet  ( 20 , Dev , Coup)
   NbBillet10Def=Reste/10
   if(NbBillet10Def >= 1){  
    NbBillet10 = NbBillet10Def
    println(s"Il reste $Reste $DevAff à distribuer")
    println("Vous pouvez obtenir au maximum " +NbBillet10 + " billets de 10 " +DevAff)
    Reste = Reste - (NbBillet10 * 10)													
    }
} 
} // fin de la methode calcReste
}// fin methode retrait
// debut methode changement de pin
def changepin(id:Int,codespin:Array[String]):Unit={ 
  var NewCodePin :String =""
  var Pin = false
  while (Pin == false) {
    println("Saisissez votre nouveau code pin (il doit contenir au moins 8 ") 
    print("caractères) > ")
    NewCodePin = readLine().toString
    if (NewCodePin.length()<=7) {
      println("Votre code pin ne contient pas au moins 8 caractères")
    }else{
      Pin = true
      codespin(id) = NewCodePin
    }
  }
}// fin methode changement de pin

}
}
