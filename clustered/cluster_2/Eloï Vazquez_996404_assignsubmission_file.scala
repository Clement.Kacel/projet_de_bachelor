//Assignment: Eloï Vazquez_996404_assignsubmission_file

object Main {
  import io.StdIn._

  // fonction dépôt
  def depot(identifiant_Int : Int, comptes : Array[Double]) : Unit={
    var devise : String = readLine("Saisissez la devise (CHF ou EUR)> ")
    while ((devise!="CHF")&&(devise!="EUR")){
      print("Saisissez la devise (CHF ou EUR)> ")
      devise = readLine()
    }
    var montant_à_déposer = readLine("Saisissez le montant à déposer > ").toInt

    while((montant_à_déposer%10!=0)||(montant_à_déposer<=0)){
      if (montant_à_déposer%10!=0){println("Le montant doit être divisible par 10")}
      if (montant_à_déposer<=0){println("Le montant doit être positif")}
      montant_à_déposer = readLine("Saisissez le montant à déposer > ").toInt
    }
    var montant_à_déposer_Double = montant_à_déposer.toDouble

    if (devise=="EUR"){montant_à_déposer_Double = montant_à_déposer_Double*0.95}
    comptes(identifiant_Int) += montant_à_déposer_Double
    printf("Votre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f",(comptes(identifiant_Int)))
    print(" CHF.")
    print("\n")

  // fin de fonction dépôt
  }

  // fonction retrait
  def retrait(identifiant_Int : Int, comptes : Array[Double]) : Unit={

    // variables coupures
    var nb_500 = 0
    var nb_200 = 0
    var nb_100 = 0
    var nb_50 = 0
    var nb_20 = 0
    var nb_10 = 0

    var devise : String = readLine("Saisissez la devise (CHF ou EUR) > ")
    while ((devise!="CHF")&&(devise!="EUR")){
      print("Saisissez la devise (CHF ou EUR) > ")
      devise = readLine()
    }
    var montant_à_retirer = readLine("Saisissez le montant à retirer > ").toInt

    while((montant_à_retirer%10!=0)||(montant_à_retirer<=0)||(montant_à_retirer*10>comptes(identifiant_Int))){
      if (montant_à_retirer%10!=0){println("Le montant doit être divisible par 10 ")}
      if (montant_à_retirer<=0){println("Le montant doit être positif ")}
      if (montant_à_retirer*10>comptes(identifiant_Int)){println("Votre plafond de retrait autorisé est de : " + comptes(identifiant_Int)/10 + " CHF")}

      montant_à_retirer = readLine("Saisissez le montant à retirer > ").toInt
    }

    // coupures
    var coupures : String = "0"
    var tableau_coupures = Array(500,200,100,50,20)

    if ((devise=="CHF")&&(montant_à_retirer>=200)){
      coupures = readLine("1) grosses coupures, 2) petites coupures : ")

      while((coupures!="1")&&(coupures!="2")){
        coupures = readLine("1) grosses coupures, 2) petites coupures : ")
      }
    }

    if ((coupures=="2")||(devise=="EUR")||(montant_à_retirer<200)){
      tableau_coupures = Array(100,50,20)
    }

    var montant_restant = montant_à_retirer

    for (i <- tableau_coupures){
      if((montant_restant/i)!=0){
        println("Il reste " + montant_restant + " à distribuer")
        print("Vous pouvez obtenir au maximum " + (montant_restant/i) + " billet(s) de " + i)
        if (devise=="CHF"){println(" CHF.")}
        if (devise=="EUR"){println(" EUR.")}

        var o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée :")

        if (o=="o"){
          if (i==500){nb_500 = (montant_restant/i)}
          if (i==200){nb_200 = (montant_restant/i)}
          if (i==100){nb_100 = (montant_restant/i)}
          if (i==50){nb_50 = (montant_restant/i)}
          if (i==20){nb_20 = (montant_restant/i)}
          montant_restant = montant_restant - (montant_restant/i)*i
          print((montant_restant/500) + (montant_restant/200) + (montant_restant/100) + (montant_restant/50) + (montant_restant/20) + (montant_restant/10))
          if (((i==20)&&(montant_restant!=0))||((montant_restant/500==0)&&(montant_restant/200==0)&&(montant_restant/100==0)&&(montant_restant/50==0)&&(montant_restant/20==0)&&(montant_restant/10!=0))){nb_10 = (montant_restant/10)}
        }
        else{
          var o_Int = o.toInt
          while ((o_Int>(montant_restant/i)||(o_Int<0))&&(o!="o")){
            if ((o_Int<0)||(o_Int>(montant_restant/i))){
              print("Vous ne pouvez pas retirer " + (o_Int) + " billet de " + i)
              if (devise=="CHF"){println(" CHF.")}
              if (devise=="EUR"){println(" EUR.")}
              println("Il reste " + montant_restant + " à distribuer")
              print("Vous pouvez obtenir au maximum " + (montant_restant/i) + " billet(s) de " + i)
              if (devise=="CHF"){println(" CHF.")}
              if (devise=="EUR"){println(" EUR.")}
              o = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée :")

              if (o!="o"){
                o_Int = o.toInt
              }
              if (o=="o"){
                if (i==500){nb_500 = (montant_restant/i)}
                if (i==200){nb_200 = (montant_restant/i)}
                if (i==100){nb_100 = (montant_restant/i)}
                if (i==50){nb_50 = (montant_restant/i)}
                if (i==20){nb_20 = (montant_restant/i)}
                montant_restant = montant_restant - (montant_restant/i)*i
                if (((i==20)&&(montant_restant!=0))||((montant_restant/500==0)&&(montant_restant/200==0)&&(montant_restant/100==0)&&(montant_restant/50==0)&&(montant_restant/20==0)&&(montant_restant/10!=0))){nb_10 = (montant_restant/10)}
              }
            }
          }    
        }
        if (o!="o"){
          var o_Int = o.toInt
          if (i==500){nb_500 = o_Int}
          if (i==200){nb_200 = o_Int}
          if (i==100){nb_100 = o_Int}
          if (i==50){nb_50 = o_Int}
          if (i==20){nb_20 = o_Int}
          montant_restant = montant_restant - o_Int*i
          if (((i==20)&&(montant_restant!=0))||((montant_restant/500==0)&&(montant_restant/200==0)&&(montant_restant/100==0)&&(montant_restant/50==0)&&(montant_restant/20==0)&&(montant_restant/10!=0))){nb_10 = (montant_restant/10)}
        }
      }
    }
    var montant_à_retirer_Double = montant_à_retirer.toDouble
    if (devise=="EUR"){montant_à_retirer_Double = montant_à_retirer_Double*0.95}

    var nbbillets_coupures = Array(nb_500,nb_200,nb_100,nb_50,nb_20,nb_10)
    var nb_coupures = 0

    // tableau qui contient la valeur 10
    var tableau_coupures_2 = Array(500,200,100,50,20,10)

    print("\n")
    println("Veuillez retirer la somme demandée : ")

    // affichage des quantités de billets à retirer
    for (i <- 0 to 5){
      if(nbbillets_coupures(i)!=0){
        print(nbbillets_coupures(i) + " billet(s) de " + tableau_coupures_2(i))
        if (devise=="CHF"){println(" CHF.")}
        if (devise=="EUR"){println(" EUR.")}
      }
    }
    comptes(identifiant_Int) = comptes(identifiant_Int) - montant_à_retirer_Double
    print("\n")
    printf("Votre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de %.2f",(comptes(identifiant_Int)))
    println(" CHF.")
    println("\n")
    println("\n")

  // fin de fonction retrait
  }

  // fonction changement de code 
  def changepin(identifiant_Int : Int, codespin : Array[String]) : Unit={
    codespin(identifiant_Int) = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) >")
    while (codespin(identifiant_Int).length<8){
      codespin(identifiant_Int) = readLine("Votre code pin ne contient pas au moins 8 caractères > ")
    }

  // fin fonction changement de code
  }

  // main
  def main(args: Array[String]): Unit = {
    var nbclients = 100
    var comptes = Array.tabulate(nbclients)(i => 12000.0)
    var codespin = Array.tabulate(nbclients)(i => "INTRO1234")
    var sortie_programme = 0
    var choix : String = "0"


    while(sortie_programme == 0){
      // identifiant => sortie s'il est pas valable
      var identifiant_String = readLine("Saisissez votre code identifiant >")
      var identifiant_Int = identifiant_String.toInt
      if ((identifiant_Int >= nbclients)||(identifiant_Int < 0)){
        println("Cet identifiant n'est pas valable")
        sortie_programme = 2
      }
      while(sortie_programme < 2){
        // identifiant si le code pin a été faux 3 fois => sortie s'il est pas valable
        if (sortie_programme != 0){
          var identifiant_String = readLine("Saisissez votre code identifiant >")
          var identifiant_Int = identifiant_String.toInt
          if ((identifiant_Int >= nbclients)||(identifiant_Int < 0)){
            println("Cet identifiant n'est pas valable")
            sortie_programme = 2
          }
        }
        if (sortie_programme != 2){
          // code pin
          var code_pin : String = readLine("Saisissez votre code pin >")
          var code_pin_tentatives = 0
          while((code_pin_tentatives < 2)&&(code_pin != codespin(identifiant_Int))){
            code_pin_tentatives += 1
            code_pin = readLine("code pin erroné, il vous reste "+(3-code_pin_tentatives)+" tentatives > ")
          }
          // si code pin faux => sortie
          if (code_pin != codespin(identifiant_Int)){
            println("Trop d'erreurs, abandon de l'identification")
            sortie_programme = 1
          }
          else{
            sortie_programme = 0
          }
        }
        // choix
        while(sortie_programme < 1){
          println("Choisissez votre opération : ")
          println("    1)  Dépôt")
          println("    2)  Retrait")
          println("    3)  Consultation du compte")
          println("    4)  Changement du code pin")
          println("    5)  Terminer")
          var choix = readLine("Votre choix : ")

          // boucle qui force à choisir un choix valable
          while ((choix!="1")&&(choix!="2")&&(choix!="3")&&(choix!="4")&&(choix!="5")){choix = readLine("Entrez un choix valable : ")}

          // depot
          if (choix=="1"){
            depot(identifiant_Int, comptes)
          }

          // retrait
          if (choix=="2"){
            retrait(identifiant_Int, comptes)
          }

          // consultation du compte
          if (choix=="3"){
            printf("Le montant disponible sur votre compte est de %.2f",(comptes(identifiant_Int)))
            println(" CHF.")
          }

          // changement de code pin
          if (choix=="4"){
            changepin(identifiant_Int, codespin)
          }

          // choix terminer
          if (choix=="5"){
            println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
            sortie_programme = 1
          }
        // fin de la boucle du choix => revient au choix
        }
       // fin de la boucle qui dit si le code pin est bon => revient à l'identifiant et code
      }
    // sortie du programme
    }
  }
}