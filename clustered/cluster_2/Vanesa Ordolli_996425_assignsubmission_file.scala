//Assignment: Vanesa Ordolli_996425_assignsubmission_file

import scala.io.StdIn._


/**
 * EXERCICE 2 : Introduction à la programmation
 * Version 1.0
 */
object main {
  val nbclients = 100

  var comptes = Array.fill(nbclients) {1200.0}
  var codespin = Array.fill(nbclients) {"INTRO1234"}

  def verifierId() : Int = {
    var numberOfAttempts = 3

    var codeIdentifiant = readLine("Saisissez votre code identifiant > ").toInt
    if(codeIdentifiant >= nbclients){
      println("Cet identifiant n’est pas valable.")
      System.exit(0)
    }else{
      var pinEntered = readLine("Saisissez votre code pin > ")
      numberOfAttempts -= 1
      while (numberOfAttempts > 0 && pinEntered != codespin(codeIdentifiant)) {
        if (pinEntered != codespin(codeIdentifiant)) {
          printf("Code pin erroné, il vous reste %d tentatives >\n", numberOfAttempts)
        }
        pinEntered = readLine("Saisissez votre code pin > ")
        numberOfAttempts -= 1
      }
      if (numberOfAttempts <= 0) {
        println("Trop d’erreurs, abandon de l’identification")
        verifierId()
      }
    }
    return codeIdentifiant
  }


  def depot(id : Int, comptes : Array[Double]) : Unit = {
    println("Indiquez la devise du dépôt : 1) CHF ; 2) EUR >")
    var choixDevise = readInt()

    println("Indiquez le montant du dépôt >")
    var choixMontantDeposit = readInt()

    while (choixMontantDeposit % 10 != 0) {
      println("Le montant doit être un multiple de 10")
      println("Indiquez le montant du dépôt >")
      choixMontantDeposit = readInt()
    }

    if (choixDevise == 1) {
      comptes(id) += choixMontantDeposit
    }else if (choixDevise == 2) {
      var montantConverted = choixMontantDeposit * 0.95
      comptes(id) += montantConverted
    }
    printf("\nVotre dépôt a été pris en compte, le nouveau montant disponible sur votre compte est de: %.2f CHF\n", comptes(id))


  }

  def retrait(id : Int, comptes : Array[Double]) : Unit = {

    def afficherBillets(nbCoupures : Array[Int], coupures: Array[Int], deviseString : String): Unit = {
      if(nbCoupures.length == coupures.length){
        for(i<-0 until nbCoupures.length){
          if(nbCoupures(i) > 0){
            printf("%d billet(s) de %d %s\n", nbCoupures(i), coupures(i), deviseString)
          }
        }
      }
    }

    println("Indiquez la devise :1 CHF, 2 : EUR > ")
    var deviseRetraitChoix = readInt()


    while (deviseRetraitChoix != 1 && deviseRetraitChoix != 2) {
      println("Indiquez la devise :1 CHF, 2 : EUR > ")
      deviseRetraitChoix = readInt()
    }

    var deviseString = ""
    if (deviseRetraitChoix == 1) {
      deviseString = "CHF"
    } else if (deviseRetraitChoix == 2) {
      deviseString = "EUR"
    }

    println("Indiquez le montant du retrait > ")
    var montantDuRetrait = readInt()

    var plafondDeRetrait = 0.1 * comptes(id)

    while (montantDuRetrait % 10 != 0 || montantDuRetrait > plafondDeRetrait) {

      if (montantDuRetrait % 10 != 0) {
        println("Le montant doit être un multiple de 10")
      }
      if (montantDuRetrait > plafondDeRetrait) {
        printf("Votre plafond de retrait autorisé est de : %.2f \n", plafondDeRetrait)
      }

      println("Indiquez le montant du retrait >")
      montantDuRetrait = readInt()
    }
    var montantRetraitRestant = montantDuRetrait

    var choixCoupures = 2

    if (montantDuRetrait >= 200 && deviseRetraitChoix == 1) { //montant est plus grand que 200 et c'est en CHF then we ask coupures
      println("En 1) grosses coupures, 2) petites coupures > ")
      choixCoupures = readInt()
      while (choixCoupures != 1 && choixCoupures != 2) {
        println("En 1) grosses coupures, 2) petites coupures > ")
        choixCoupures = readInt()
      }
    }

    if (choixCoupures == 1) { //big coupures => seulement CHF
      var grossesCoupures = Array[Int](500, 200, 100, 50, 20, 10)
      var nbGrosses = Array.fill(grossesCoupures.length) {0}

      for (coupure <- grossesCoupures) {
        if (montantRetraitRestant >= coupure) {
          var nbCoupure = montantRetraitRestant / coupure
          val nbCoupureMax = montantRetraitRestant / coupure
          if(coupure == 10){
          }else{
            printf("Il reste %d CHF à distribuer\n", montantRetraitRestant)
            printf("Vous pouvez obtenir au maximum %d billet(s) de %d CHF\n", nbCoupure, coupure)

            var oForOK = ""
            var infValue = nbCoupure
            while (oForOK != "o" && infValue >= nbCoupureMax) {
              oForOK = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              if (oForOK != "o") {
                infValue = oForOK.toInt
                if (infValue < nbCoupureMax) {
                  nbCoupure = infValue
                }
              }
            }
          }
          montantRetraitRestant -= nbCoupure * coupure
          nbGrosses(grossesCoupures.indexOf(coupure)) = nbCoupure
        }
      }

      afficherBillets(nbGrosses, grossesCoupures, deviseString)
    }else if (choixCoupures == 2){ //possibilité de EUR et CHF

      var petitesCoupures = Array[Int](100, 50, 20, 10)
      var nbPetites = Array.fill(petitesCoupures.length) {0}

      for (coupure <- petitesCoupures) {
        if (montantRetraitRestant >= coupure) {
          var nbCoupure = montantRetraitRestant / coupure
          val nbCoupureMax = nbCoupure
          if(coupure == 10){
          }else{
            printf("Il reste %d CHF à distribuer\n", montantRetraitRestant)
            printf("Vous pouvez obtenir au maximum %d billet(s) de %d %s\n", nbCoupure, coupure, deviseString)

            var oForOK = ""
            var infValue = nbCoupure
            while (oForOK != "o" && infValue >= nbCoupureMax) {
              oForOK = readLine("Tapez o pour ok ou une autre valeur inférieure à celle proposée > ")
              if(oForOK != "o"){
                infValue = oForOK.toInt
                if (infValue < nbCoupureMax) {
                  nbCoupure = infValue
                }
              }

            }
          }
          montantRetraitRestant -= nbCoupure * coupure
          nbPetites(petitesCoupures.indexOf(coupure)) = nbCoupure
        }
      }
      afficherBillets(nbPetites, petitesCoupures, deviseString)
    }
    if(deviseRetraitChoix == 1){
      comptes(id) -= montantDuRetrait
    }else if(deviseRetraitChoix == 2){
      comptes(id) -= montantDuRetrait * 0.95
    }
    printf("\nVotre retrait a été pris en compte, le nouveau montant disponible sur votre compte est de : %.2f CHF \n", comptes(id))
  }

  def changepin(id : Int, codespin : Array[String]) : Unit = {
    var newPinCode = ""
    while (newPinCode.length < 8) {
      newPinCode = readLine("Saisissez votre nouveau code pin (il doit contenir au moins 8 caractères) > ")

      if (newPinCode.length < 8) {
        println("Votre code pin ne contient pas au moins 8 caractères.")
      }
    }
    codespin(id) = newPinCode
    println("Nouveau code pin enregistré.")
  }

  def main(args: Array[String]): Unit = {
    var id = verifierId()

    var terminer = false

    while(!terminer){
      println("\nChoisissez votre opération :\n1) Dépôt\n2) Retrait\n3) Consultation du compte\n4) Changement du code pin\n5) Terminer\nVotre choix :")
      var choixOperation = readInt()

      if(choixOperation == 1){
        depot(id, comptes)

      }else if(choixOperation == 2){
        retrait(id, comptes)

      }else if (choixOperation == 3){
        printf("Le montant disponible sur votre compte est de : %.2f \n", comptes(id))

      }else if (choixOperation == 4){
        changepin(id, codespin)

      }else if (choixOperation == 5){
        println("Fin des opérations, n’oubliez pas de récupérer votre carte.")
        id = verifierId()
      }else{
        //do nothing to repeat the choice
      }
    }

  }//end main
} //end object
