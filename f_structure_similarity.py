import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer


keywords = ['if', 'else', 'do', 'while', 'def', 'match', 'println', 'var', 'val', 'case']

def extract_features_from_code(content: str) -> np.ndarray:
   
    features = {key: 0 for key in keywords}
    

    for keyword in keywords:
        features[keyword] = content.count(keyword)

    feature_vector = np.array(list(features.values()))
    
    return feature_vector

def structural_similarity(code1: str, code2: str) -> float:

    vector1 = extract_features_from_code(code1)
    vector2 = extract_features_from_code(code2)
    
   
    similarity = cosine_similarity([vector1], [vector2])[0, 0]
    
    return similarity * 100

