@echo off
setlocal

REM Check if Python is available
where python >nul 2>nul
if errorlevel 1 (
    echo Python is not installed or not in the PATH.
    exit /b 1
)

REM Check if chardet module is installed
python -c "import chardet" >nul 2>nul
if errorlevel 1 (
    echo chardet module is not installed. Installing...
    python -m pip install chardet
)

REM Check for argument
if "%~1"=="" (
    echo Usage: %0 folder_path
    exit /b 1
)

REM Run Python script to convert folder to UTF-8
python convert_to_utf8.py "%~1"

REM Check if the conversion was successful
if errorlevel 1 (
    echo Conversion failed.
    exit /b 1
)

echo Conversion completed successfully.

endlocal
