from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def cosine_similarity_perc(str1: str, str2: str) -> float:
    vectorizer = CountVectorizer().fit_transform([str1, str2])
    
    vectors = vectorizer.toarray()
    
    cosine_sim = cosine_similarity(vectors)
    
    similarity = cosine_sim[0, 1]
    
    return similarity * 100
