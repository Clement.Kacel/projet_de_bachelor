import re

def check_var(content):
    # Split the content into lines
    lines = content.split('\n')
    
    # Store declarations and their line numbers
    declarations = []
    used_vars = set()
    
    # Step 1: Collect all variable declarations
    for line_number, line in enumerate(lines, start=1):
        matches = re.findall(r'\b(val|var)\s+(\w+)', line)
        for match in matches:
            declarations.append((line_number, match[1]))  # Store line number and variable name
    
    # Step 2: Collect all variable usage
    for line in lines:
        # Capture variables used in expressions, assignments, etc.
        variables = re.findall(r'\b\w+\b', line)
        used_vars.update(variables)
    
    # Filter out keywords and numbers from used_vars
    keywords_and_numbers = {'if', 'else', 'while', 'for', 'def', 'return', 'import', 'from', 'val', 'var', 'case', 'match', 'array'}
    used_vars = {var for var in used_vars if var not in keywords_and_numbers and not re.match(r'^\d+$', var)}
    
    # Step 3: Check each declared variable if it's used elsewhere in the code
    unused_vars_lines = []
    for line_number, var_name in declarations:
        # Check if variable is used in any other line except its declaration line
        variable_used = False
        for i, line in enumerate(lines, start=1):
            if i != line_number and re.search(r'\b' + re.escape(var_name) + r'\b', line):
                variable_used = True
                break
        
        if not variable_used:
            unused_vars_lines.append(line_number)
    
    return unused_vars_lines
    
