import re

def check_unused_methods_scala(content):
    # Split the content into lines
    lines = content.split('\n')
    
    # Store method declarations and their line numbers
    method_declarations = []
    used_methods = set()
    
    # Step 1: Collect all method declarations in Scala
    for line_number, line in enumerate(lines, start=1):
        # Match method declarations in Scala
        matches = re.findall(r'\bdef\s+(\w+)\s*\(.*?\)\s*:', line)
        for match in matches:
            # Skip 'main' method
            if match.lower() != 'main':
                method_declarations.append((line_number, match))  # Store line number and method name
    
    # Step 2: Collect all method usage (excluding their declaration)
    for line in lines:
        for _, method_name in method_declarations:
            # We search for the method name being called, considering it might not always be followed by parentheses
            pattern = r'\b' + re.escape(method_name) + r'\b\s*(\(|\{)?'
            if re.search(pattern, line) and 'def ' not in line:
                used_methods.add(method_name)
    
    # Step 3: Identify unused methods
    unused_methods_lines = []
    for line_number, method_name in method_declarations:
        if method_name not in used_methods:
            unused_methods_lines.append(line_number)
    
    return unused_methods_lines
