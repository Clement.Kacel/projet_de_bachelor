import re

def mark_unused_methods(scala_code):
    # Emoji to mark unused methods
    unused_method_marker = "🚫"
    
    # Regular expressions to match method definitions and calls
    method_def_pattern = re.compile(r'^\s*def\s+(\w+)\s*\(')
    method_call_pattern = re.compile(r'\b(\w+)\s*\(')
    
    # Extract lines
    code_lines = scala_code.splitlines()
    
    # Sets to keep track of defined methods and method calls
    defined_methods = set()
    method_calls = set()
    
    # Track defined methods
    for line in code_lines:
        match = method_def_pattern.search(line)
        if match:
            method_name = match.group(1)
            defined_methods.add(method_name)
    
    # Track method calls
    for line in code_lines:
        matches = method_call_pattern.findall(line)
        for method_name in matches:
            method_calls.add(method_name)
    
    # Identify unused methods
    unused_methods = defined_methods - method_calls
    
    # Annotate lines with unused methods
    modified_lines = []
    for line in code_lines:
        # Check if line contains a definition of an unused method
        stripped_line = line.strip()
        if any(stripped_line.startswith(f"def {method}(") for method in unused_methods):
            modified_lines.append(f"{unused_method_marker} {line}")
        else:
            modified_lines.append(line)
    
    # Return modified code as a single string
    return "\n".join(modified_lines)
