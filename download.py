import zipfile
import os
import io
from flask import send_file, Flask, request, redirect, url_for, flash

def create_zip_from_directory(directory_path):
    zip_buffer = io.BytesIO()
    
    with zipfile.ZipFile(zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zip_file:
        for root, dirs, files in os.walk(directory_path):
            for file in files:
                file_path = os.path.join(root, file)
                relative_path = os.path.relpath(file_path, directory_path)
                zip_file.write(file_path, relative_path)
    
    zip_buffer.seek(0)
    return zip_buffer

def download_zip_of_directory(directory_path):
    zip_buffer = create_zip_from_directory(directory_path)
    return send_file(
        zip_buffer,
        as_attachment=True,
        download_name='Automatic_feedback.zip',
        mimetype='application/zip'
    )
