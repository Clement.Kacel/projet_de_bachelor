import os
import chardet

def detect_encoding(file_path):
    with open(file_path, 'rb') as file:
        raw_data = file.read()
        result = chardet.detect(raw_data)
        return result['encoding']

def convert_file_to_utf8(file_path):
    encoding = detect_encoding(file_path)
    if encoding and encoding.lower() != 'utf-8':
        try:
            with open(file_path, 'r', encoding=encoding, errors='ignore') as file:
                content = file.read()
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(content)
            print(f"Converted {file_path} to UTF-8")
        except Exception as e:
            print(f"Failed to convert {file_path}: {e}")

def convert_folder_to_utf8(folder_path):
    for root, _, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            convert_file_to_utf8(file_path)

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Usage: python convert_to_utf8.py <folder_path>")
        sys.exit(1)
    folder_path = sys.argv[1]
    if not os.path.isdir(folder_path):
        print(f"Error: {folder_path} is not a valid directory.")
        sys.exit(1)
    convert_folder_to_utf8(folder_path)


