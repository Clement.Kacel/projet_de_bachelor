import os
import shutil
from flask import Blueprint, render_template, request, redirect, url_for, flash, session
import zipfile
home_bp = Blueprint('home', __name__)
import pymysql
import os
from .add_db import add_data_to_db  

UPLOAD_FOLDER = 'uploads'
EXTRACT_FOLDER = 'extracted'
ALLOWED_EXTENSIONS = {'zip'}

os.makedirs(UPLOAD_FOLDER, exist_ok=True)
os.makedirs(EXTRACT_FOLDER, exist_ok=True)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def extract_zip(file_path, extract_to):
    with zipfile.ZipFile(file_path, 'r') as zip_ref:
        for file_info in zip_ref.infolist():
            
            if file_info.filename.startswith('/') or file_info.filename.startswith('\\'):
                continue

            extracted_path = os.path.join(extract_to, file_info.filename)

            if file_info.is_dir():
                os.makedirs(extracted_path, exist_ok=True)
            else:
                os.makedirs(os.path.dirname(extracted_path), exist_ok=True)
                with zip_ref.open(file_info) as source, open(extracted_path, 'wb') as target:
                    shutil.copyfileobj(source, target)


db_config = {
        'host': 'localhost',
        'user': 'root',
        'db': 'projet_b'
    }

@home_bp.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        if 'zip_file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        file = request.files['zip_file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            zip_path = os.path.join(UPLOAD_FOLDER, file.filename)
            file.save(zip_path)
            # print("Zip Path: ", zip_path)
            if not os.path.exists(zip_path):
                flash("Error in saving file.")
                return redirect(request.url)
            
            extraction_path = os.path.join(EXTRACT_FOLDER, os.path.splitext(file.filename)[0])
            # print("EXtraction_path : ", extraction_path)
            os.makedirs(extraction_path, exist_ok=True)
            extract_zip(zip_path, extraction_path)

            session['file_uploaded'] = True

            flash('Successfully processed the ZIP file.')
                
            conn = None
            cursor = None
            
            try:
                conn = pymysql.connect(**db_config)
                cursor = conn.cursor()

                add_data_to_db(conn, cursor)
                flash('Data has been successfully added to the database.')

                
            except pymysql.MySQLError as e:
                print(f"Error connecting to MySQL: {e}")
                flash('An error occurred while processing your request.')

                if conn:
                    conn.rollback()  

            finally:
                if cursor:
                    cursor.close()
                
                return redirect(url_for('home.home'))

        else:
            flash('Invalid file type. Please upload a ZIP file.')
            session['file_uploaded'] = False
            return redirect(request.url)
    
  
    
    return render_template('home.html')


from functools import wraps
from flask import redirect, url_for, flash

def require_file_upload(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not session.get('file_uploaded', False):
            flash("Please upload a file first.")
            return redirect(url_for('home.home'))
        return f(*args, **kwargs)
    return decorated_function
