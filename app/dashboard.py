from flask import Blueprint, render_template
import pymysql
import numpy as np

dashboard_bp = Blueprint('dashboard', __name__)

db_config = {
    'host': 'localhost',
    'user': 'root',
    'db': 'projet_b'
}

def fetch_assignments():
    conn = pymysql.connect(**db_config)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    
    cursor.execute("SELECT Name, Num_Lines, Num_Ifs, Num_Loops, Num_Var, Num_Methods, unused_variables, unused_methods, match_if_else, missing_brackets, array_error FROM assignement")
    assignments = cursor.fetchall()
    
    cursor.close()
    conn.close()
    
    return assignments

def fetch_error_counts():
    conn = pymysql.connect(**db_config)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    
    # Count occurrences of each error type
    cursor.execute("""
        SELECT
            SUM(CASE WHEN TRIM(IFNULL(unused_variables, '[]')) <> '[]' THEN 1 ELSE 0 END) AS unused_variables_count,
            SUM(CASE WHEN TRIM(IFNULL(unused_methods, '[]')) <> '[]' THEN 1 ELSE 0 END) AS unused_methods_count,
            SUM(CASE WHEN TRIM(IFNULL(match_if_else, '[]')) <> '[]' THEN 1 ELSE 0 END) AS match_if_else_count,
            SUM(CASE WHEN TRIM(IFNULL(missing_brackets, '[]')) <> '[]' THEN 1 ELSE 0 END) AS missing_brackets_count
        FROM assignement;

    """)
    
    result = cursor.fetchone()
    cursor.close()
    conn.close()

    # Calculate counts for clean assignments
    error_counts = {
        'Variables': result['unused_variables_count'],
        'Methods': result['unused_methods_count'],
        'Control Statement': result['match_if_else_count'],
        'Identation': result['missing_brackets_count']
    }


    return {
        'Variables': error_counts['Variables'],
        'Methods': error_counts['Methods'],
        'Control Statement': error_counts['Control Statement'],
        'Identation': error_counts['Identation']
    }

def fetch_clean_assignments_ratio():
    conn = pymysql.connect(**db_config)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    
    # Count occurrences of each error type and total assignments
    cursor.execute("""
        SELECT
            COUNT(*) AS total_assignments,
            SUM(CASE WHEN TRIM(IFNULL(unused_variables, '[]')) <> '[]' THEN 1 ELSE 0 END) AS unused_variables_count,
            SUM(CASE WHEN TRIM(IFNULL(unused_methods, '[]')) <> '[]' THEN 1 ELSE 0 END) AS unused_methods_count,
            SUM(CASE WHEN TRIM(IFNULL(missing_brackets, '[]')) <> '[]' THEN 1 ELSE 0 END) AS missing_brackets_count
        FROM assignement
    """)
    
    result = cursor.fetchone()
    
    cursor.close()
    conn.close()
    
    total_assignments = result['total_assignments']
    total_errors = (
        result['unused_variables_count'] +
        result['unused_methods_count'] +
        result['missing_brackets_count']
    )
    
    clean_assignments = total_assignments - total_errors
    return {
        'Clean Assignments': clean_assignments,
        'Assignments with Errors': total_errors
    }

def calculate_percentiles(assignments):
    num_assignments = len(assignments)
    
    # Extract data
    num_loops = [assignment['Num_Loops'] for assignment in assignments]
    num_methods = [assignment['Num_Methods'] for assignment in assignments]
    num_vars = [assignment['Num_Var'] for assignment in assignments]
    
    # Calculate 75th percentiles
    percentile_75_loops = np.percentile(num_loops, 75)
    percentile_25_methods = np.percentile(num_methods, 25)
    percentile_75_vars = np.percentile(num_vars, 75)
    
    return {
        'percentile_75_num_loops': percentile_75_loops,
        'percentile_25_num_methods': percentile_25_methods,
        'percentile_75_num_vars': percentile_75_vars
    }

def calculate_comparisons(assignments, percentiles):
    counts = {
        'above_75th_percentile_num_var': 0,
        'below_75th_percentile_num_var': 0,
        'above_75th_percentile_num_loops': 0,
        'below_75th_percentile_num_loops': 0,
        'above_25th_percentile_num_methods': 0,
        'below_25th_percentile_num_methods': 0
    }
    
    for assignment in assignments:        
        # Number of variables
        if assignment['Num_Var'] > percentiles['percentile_75_num_vars']:
            counts['above_75th_percentile_num_var'] += 1
        else:
            counts['below_75th_percentile_num_var'] += 1
        
        # Number of loops
        if assignment['Num_Loops'] > percentiles['percentile_75_num_loops']:
            counts['above_75th_percentile_num_loops'] += 1
        else:
            counts['below_75th_percentile_num_loops'] += 1
        
        # Number of methods
        if assignment['Num_Methods'] <= percentiles['percentile_25_num_methods']:
            counts['below_25th_percentile_num_methods'] += 1
        else:
            counts['above_25th_percentile_num_methods'] += 1
    
    return counts


def categorize_errors(assignments):
    percentiles = calculate_percentiles(assignments)

    # Initialize error count per module
    error_counts = {
        'module_2': 0,  # Types (unused variable, brackets errors)
        'module_3': 0,  # Instruction conditionnelle (if/else instead of match case)
        'module_4': 0,  # Instructions itératives (infinite loops, high number of loops)
        'module_5': 0,  # Collections: tableaux et listes (bad or unused arrays)
        'module_6': 0   # Méthodes (unused or too few methods, brackets errors)
    }
    
    comparisons = calculate_comparisons(assignments, percentiles)

    for assignment in assignments:

        if assignment.get('unused_methods') and assignment.get('unused_methods') != "[]":
            error_counts['module_6'] += 1

        if assignment.get('unused_variables') and assignment.get('unused_variables') != "[]":
            error_counts['module_2'] += 1

        if assignment.get('match_if_else') and assignment.get('match_if_else') != "[]":
            error_counts['module_3'] += 1

       # Check for excessive loops
        if assignment['Num_Loops'] > percentiles['percentile_75_num_loops']:
            error_counts['module_4'] += 1
        
        # Check for unused or poorly defined arrays (e.g., high number of variables)
        if assignment['Num_Var'] > percentiles['percentile_75_num_vars']:
            error_counts['module_2'] += 1 
        
        # Check for low number of methods
        if assignment['Num_Methods'] <= percentiles['percentile_25_num_methods']:
            error_counts['module_6'] += 1


        if assignment.get('array_error') and assignment.get('array_error') != "[]":
            error_counts['module_5'] +=1

    return error_counts


@dashboard_bp.route('/')
def dashboard():
    assignments = fetch_assignments()

    # Data preparation for charts
    names = [assignment['Name'] for assignment in assignments]
    lines = [assignment['Num_Lines'] for assignment in assignments]
    if_else_statements = [assignment['Num_Ifs'] for assignment in assignments]
    while_loops = [assignment['Num_Loops'] for assignment in assignments]
    vars_ = [assignment['Num_Var'] for assignment in assignments]
    methods = [assignment['Num_Methods'] for assignment in assignments]
    
    error_counts = fetch_error_counts()
    # Prepare data for the chart
    labels = list(error_counts.keys())
    values = list(error_counts.values())



      # Fetch clean assignments ratio
    clean_assignments_data = fetch_clean_assignments_ratio()
    clean_assignments_labels = list(clean_assignments_data.keys())
    clean_assignments_values = list(clean_assignments_data.values())


    categorize_error = categorize_errors(assignments)



    return render_template('dashboard.html',
                           names=names,
                           lines=lines,
                           if_else_statements=if_else_statements,
                           while_loops=while_loops,
                           vars=vars_,
                           methods=methods,
                           labels=labels, 
                           values=values,
                           clean_assignments_labels=clean_assignments_labels,
                           clean_assignments_values=clean_assignments_values,
                           categorize_error= categorize_error)
