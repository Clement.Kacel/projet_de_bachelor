import os
from flask import Blueprint, render_template, request, redirect, url_for, flash
import re
from sklearn.cluster import KMeans
import numpy as np
import shutil
import json
import math
comparison_bp = Blueprint('comparison', __name__)
from .home import require_file_upload
from sklearn.feature_extraction.text import TfidfVectorizer
import math
from check_unused_var import check_var
from check_brackets import check_missing_brackets
from check_unused_methods import mark_unused_methods
from check_infinite_loop import mark_infinite_while_loops
from f_code_similarity import cosine_similarity_perc
from f_structure_similarity import structural_similarity

EXTRACT_FOLDER = '/extracted'
CLUSTERED_FOLDER = 'clustered'
CLUSTER_RESULTS_FILE = 'clustering_results.json'
os.makedirs(CLUSTERED_FOLDER, exist_ok=True)
vectorizer = TfidfVectorizer()
import matplotlib.pyplot as plt
from .add_db import add_data_to_db
import pymysql
db_config = {
        'host': 'localhost',
        'user': 'root',
        'db': 'projet_b'
    }
def fetch_data_from_db():
    connection = pymysql.connect(**db_config)
    try:
        with connection.cursor() as cursor:
            sql = "SELECT Name, Content FROM assignement"
            cursor.execute(sql)
            result = cursor.fetchall()
    finally:
        connection.close()
    return result

from sklearn.decomposition import PCA

def apply_pca(features, n_components=0.95):
    pca = PCA(n_components=n_components)
    reduced_features = pca.fit_transform(features)
    return reduced_features

def plot_pca_variance(features, threshold=0.75):
    """
    Plots the cumulative explained variance to determine the optimal number of PCA components.
    
    Parameters:
    features (np.ndarray): The feature matrix where each row is a sample and each column is a feature.
    threshold (float): The cumulative variance threshold for determining the optimal number of components.
    """
    pca = PCA()
    pca.fit(features)
    
    explained_variance_ratio = pca.explained_variance_ratio_
    
    cumulative_explained_variance = np.cumsum(explained_variance_ratio)
    
    n_components = len(explained_variance_ratio)
    
    optimal_components = np.argmax(cumulative_explained_variance >= threshold) + 1

    plt.figure(figsize=(12, 6))
    
    plt.plot(range(1, n_components + 1), cumulative_explained_variance, marker='o', linestyle='--')
    plt.axhline(y=threshold, color='r', linestyle='--', label=f'{int(threshold*100)}% Variance')
    plt.axvline(x=optimal_components, color='g', linestyle='--', label=f'Optimal Components: {optimal_components}')
    
    plt.xlabel('Number of Components')
    plt.ylabel('Cumulative Explained Variance')
    plt.title('Cumulative Explained Variance vs. Number of Components')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.show()

def extract_features_from_content(content):
    try:
        num_lines = content.count('\n')
        num_while_loops = len(re.findall(r'\bwhile\b|\bdo\b', content))
        num_for_loops = len(re.findall(r'\bfor\b', content))
        num_if_statements = len(re.findall(r'\bif\b|\belse\b', content))
        num_vars = len(re.findall(r'\bvar\b|\bval\b', content))
        num_methods = len(re.findall(r'\bdef\b', content))

        basic_features = [num_lines, num_while_loops, num_methods, num_vars, num_for_loops, num_if_statements]
        
        tfidf_features = vectorizer.transform([content]).toarray().flatten()
        
        features = np.concatenate([basic_features, tfidf_features])
    except Exception as e:
        print(f"Error processing content: {e}")
        features = None
    return features

import re

def extract_basic_metrics(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()

            num_lines = content.count('\n') + 1 
            num_while_loops = len(re.findall(r'\bwhile\b|\bdo\b', content))
            num_for_loops = len(re.findall(r'\bfor\b', content))
            num_if_statements = len(re.findall(r'\bif\b|\belse\b', content))
            num_vars = len(re.findall(r'\bvar\b|\bval\b', content))
            num_methods = len(re.findall(r'\bdef\b', content))
            return num_lines, num_while_loops, num_for_loops, num_if_statements, num_vars, num_methods
    except Exception as e:
        print(f"Error processing file {file_path}: {e}")
        return 0, 0, 0, 0, 0  

def calculate_cluster_statistics(cluster_files):
    cluster_stats = {
        'num_files': 0,
        'avg_lines': 0,
        'avg_while_loops': 0,
        'avg_for_loops': 0,
        'avg_if_statements': 0,
        'avg_vars': 0,
        'avg_methods': 0
    }

    if not cluster_files:
        return cluster_stats

    total_lines = 0
    total_while_loops = 0
    total_for_loops = 0
    total_if_statements = 0
    total_vars = 0
    total_methods = 0
    for file_path in cluster_files:
        num_lines, num_while_loops, num_for_loops, num_if_statements, num_vars, num_methods = extract_basic_metrics(file_path)
        total_lines += num_lines
        total_while_loops += num_while_loops
        total_for_loops += num_for_loops
        total_if_statements += num_if_statements
        total_vars += num_vars
        total_methods += num_methods

    num_files = len(cluster_files)
    cluster_stats['num_files'] = num_files
    cluster_stats['avg_lines'] = round(total_lines / num_files)
    cluster_stats['avg_while_loops'] = round(total_while_loops / num_files)
    cluster_stats['avg_for_loops'] = round(total_for_loops / num_files)
    cluster_stats['avg_if_statements'] = round(total_if_statements / num_files)
    cluster_stats['avg_vars'] = round(total_vars / num_files)
    cluster_stats['avg_methods'] = round(total_methods / num_files)

    return cluster_stats


def find_scala_files(directory):
    files = []
    for root, dirs, filenames in os.walk(directory):
        for file in filenames:
            if file.endswith('.scala'):
                student_name = os.path.basename(root)  
                full_path = os.path.join(root, file)
                files.append((student_name, full_path))
    print(files)
    return files

def prepend_comment_to_file(file_path, comment):
    try:
        with open(file_path, 'r+', encoding='utf-8') as file:
            content = file.read()
            file.seek(0, 0) 
            file.write(comment + '\n\n' + content)
    except IOError as e:
        print(f"Error writing to file {file_path}: {e}")

def cluster_assignments(features, n_clusters):
    if len(features) == 0:
        raise ValueError("No features to cluster")

    kmeans = KMeans(n_clusters=n_clusters)
    clusters = kmeans.fit_predict(features)
    return clusters


def save_clustering_results(cluster_info, number_of_cluster):
    serializable_cluster_info = [
        (content_id, int(cluster)) 
        for content_id, cluster in cluster_info
    ]
    
    data_to_save = {
        'cluster_info': serializable_cluster_info,
        'number_of_cluster': number_of_cluster
    }
    
    with open(CLUSTER_RESULTS_FILE, 'w') as f:
        json.dump(data_to_save, f)

def load_clustering_results():
    if os.path.exists(CLUSTER_RESULTS_FILE):
        with open(CLUSTER_RESULTS_FILE, 'r', encoding='utf-8') as f:
            data_loaded = json.load(f)
            return data_loaded['cluster_info'], data_loaded['number_of_cluster']
    return None, None


import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

def find_optimal_k_elbow(data, k_range):
    inertia_values = []
    
    for k in k_range:
        kmeans = KMeans(n_clusters=k, random_state=42)
        kmeans.fit(data)
        inertia_values.append(kmeans.inertia_)
    
    k_range_list = list(k_range)
    inertia_values = np.array(inertia_values)
    inertia_diff = np.diff(inertia_values)
    inertia_diff2 = np.diff(inertia_diff)
    
    elbow_index = np.argmax(inertia_diff2) + 1 
    return k_range_list[elbow_index]


def process_clustering():
    data = fetch_data_from_db()
    
    all_contents = [row[1] for row in data]  

    vectorizer.fit(all_contents)
    
    features = []
    file_paths = []
    ids = []

    for row in data:
        Name, content = row
        file_features = extract_features_from_content(content)
        if file_features is not None:
            features.append(file_features)
            ids.append(Name)
    
    if not features:
        raise ValueError("No valid content to cluster.")
    
    features = np.array(features)

   
    reduced_features = apply_pca(features)
    k_range = range(1,26)
    res = find_optimal_k_elbow(reduced_features, k_range) + 10
    clusters = cluster_assignments(reduced_features, n_clusters=res)
    
    cluster_info = list(zip(ids, clusters))
    


    
    for cluster_dir in os.listdir(CLUSTERED_FOLDER):
        cluster_path = os.path.join(CLUSTERED_FOLDER, cluster_dir)
        if os.path.isdir(cluster_path):
            shutil.rmtree(cluster_path)
    
    for (Name, cluster) in cluster_info:
        cluster_number = cluster + 1
        cluster_dir = os.path.join(CLUSTERED_FOLDER, f'cluster_{cluster_number}')
        os.makedirs(cluster_dir, exist_ok=True)
        
        content_file_path = os.path.join(cluster_dir, f'{Name}.scala')
        with open(content_file_path, 'w', encoding='utf-8') as file:
            file.write(next(content for cid, content in data if cid == Name))
    
    save_clustering_results(cluster_info, res)
    return cluster_info, res


def add_line_numbers_and_reddot(content, line_numbers):
    lines = content.split('\n')
    new_lines = []

    for index, line in enumerate(lines):
        line_number = index + 1 
        
        if line_number in line_numbers:
            new_lines.append(f'⚠️{line}') 
        else:
            new_lines.append(f'<span class="line-number">{line_number:4}</span>  {line}')
    
    return '\n'.join(new_lines)

def add_missing_bracket_smiley(content, line_numbers):
    lines = content.split('\n')
    
    new_lines = []
    for index, line in enumerate(lines):
        line_number = index + 1  
        if line_number in line_numbers:
            new_lines.append(f'⛔ {line}')  
            new_lines.append(line)
    
    return '\n'.join(new_lines)


@comparison_bp.route('/', defaults={'cluster_num': 1, 'file_index_1': 0, 'file_index_2': 1})
@comparison_bp.route('/<int:cluster_num>/<int:file_index_1>/<int:file_index_2>')
@require_file_upload
def index(cluster_num, file_index_1, file_index_2):
    try : 
        cluster_info, number_of_cluster = load_clustering_results()
        if cluster_info is None:
            cluster_info, number_of_cluster = process_clustering()
        cluster_dir = os.path.join(CLUSTERED_FOLDER, f'cluster_{cluster_num}')
        if not os.path.exists(cluster_dir):
            flash(f'Cluster {cluster_num} does not exist.')
            return redirect(url_for('comparison.index'))  


        cluster_files = [os.path.join(cluster_dir, f) for f in os.listdir(cluster_dir) if f.endswith('.scala')]
        num_files = len(cluster_files)
        if num_files < 2:
            flash(f'Not enough files to compare in cluster {cluster_num}.')


        file_index_1 = file_index_1 % num_files
        file_index_2 = file_index_2 % num_files
        

        file_path_1 = cluster_files[file_index_1]
        file_path_2 = cluster_files[file_index_2]

        content1 = read_file_content(file_path_1)
        content2 = read_file_content(file_path_2)
        prev_file_index_1 = (file_index_1 - 1) % num_files
        next_file_index_1 = (file_index_1 + 1) % num_files

        prev_file_index_2 = (file_index_2 - 1) % num_files
        next_file_index_2 = (file_index_2 + 1) % num_files

        if prev_file_index_1 == file_index_2:
            prev_file_index_1 = (prev_file_index_1 - 1) % num_files
        if next_file_index_1 == file_index_2:
            next_file_index_1 = (next_file_index_1 + 1) % num_files

        if prev_file_index_2 == file_index_1:
            prev_file_index_2 = (prev_file_index_2 - 1) % num_files
        if next_file_index_2 == file_index_1:
            next_file_index_2 = (next_file_index_2 + 1) % num_files
        
      
        
        similarity = round(cosine_similarity_perc(content1, content2))
        struct_similarity = round(structural_similarity(content1,content2))
        array_unused_var = check_var(content1)
        array_unused_var2 = check_var(content2)

        content1 = add_line_numbers_and_reddot(content1, array_unused_var)
        content2 = add_line_numbers_and_reddot(content2, array_unused_var2)

        array_brackets = check_missing_brackets(content1)
        array_brackets2 = check_missing_brackets(content2)
        content1= add_missing_bracket_smiley(content1, array_brackets)
        content2 = add_missing_bracket_smiley(content2, array_brackets2)
        
        content1 = mark_unused_methods(content1)
        content2 = mark_unused_methods(content2)

        content1 = mark_infinite_while_loops(content1)
        content2 = mark_infinite_while_loops(content2)

        
        print("Type of number_of_cluster:", type(number_of_cluster))

        cluster_stats = calculate_cluster_statistics(cluster_files)
        return render_template('comparison.html', cluster_stats = cluster_stats, similarity = similarity, struct_similarity=struct_similarity, 
                                number_of_cluster=number_of_cluster,
                                array_unused_var =array_unused_var,
                                array_unused_var2 = array_unused_var2,
                                content1=content1,
                                content2=content2,
                                cluster_num=cluster_num,
                                file_index_1=file_index_1,
                                file_index_2=file_index_2,
                                prev_file_index_1=prev_file_index_1,
                                next_file_index_1=next_file_index_1,
                                prev_file_index_2=prev_file_index_2,
                                next_file_index_2=next_file_index_2)
    except ValueError as e:
        flash(str(e))
        return f"Error{e}"
    except Exception as e:

        cluster_info, number_of_cluster = process_clustering()
        

def read_file_content(file_path):
    try:
        print(f"Reading file: {file_path}") 
        with open(file_path, 'r', encoding='utf-8') as file:
            return file.read()
    except UnicodeDecodeError:
        return "Error reading file content."
    except FileNotFoundError:
        return "File not found."
    except Exception as e:
        return f"Error: {e}"
