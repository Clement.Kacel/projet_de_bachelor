from flask import Flask

def create_app():
    app = Flask(__name__)

    from .feedback import feedback_bp
    from .home import home_bp
    from .comparison import comparison_bp
    from .dashboard import dashboard_bp
    from .reset import reset_bp

    app.register_blueprint(home_bp, url_prefix='/')
    app.register_blueprint(comparison_bp, url_prefix='/comparison')
    app.register_blueprint(dashboard_bp, url_prefix='/dashboard')
    app.register_blueprint(reset_bp)
    app.register_blueprint(feedback_bp, url_prefix='/feedback')
    return app
