# reset.py
import os
from flask import Blueprint, redirect, url_for, flash, session
import shutil
import pymysql
reset_bp = Blueprint('reset', __name__)

# Define directories
CLUSTERED_FOLDER = 'clustered'
EXTRACTED_FOLDER = 'extracted'
CLUSTERING_FILE_JSON = 'clustering_results.json'
# Database configuration
db_config = {
    'host': 'localhost',
    'user': 'root',
    'db': 'projet_b'
}

@reset_bp.route('/reset', methods=['POST'])
def reset():
    try:
                # Remove all folders in the 'clustered' directory
        for folder_name in os.listdir(CLUSTERED_FOLDER):
            folder_path = os.path.join(CLUSTERED_FOLDER, folder_name)
            if os.path.isdir(folder_path):
                shutil.rmtree(folder_path)
        
        for folder_name in os.listdir(EXTRACTED_FOLDER):
            folder_path = os.path.join(EXTRACTED_FOLDER, folder_name)
            if os.path.isdir(folder_path):
                shutil.rmtree(folder_path)

        # Delete the clustering result JSON file if it exists
        if os.path.exists(CLUSTERING_FILE_JSON):
            os.remove(CLUSTERING_FILE_JSON)


         # Connect to the database and truncate the table
        conn = pymysql.connect(**db_config)
        cursor = conn.cursor()

        cursor.execute('TRUNCATE TABLE assignement')
        conn.commit()

        # Close the database connection
        cursor.close()
        conn.close()

        flash('The data has been reset successfully.')

        session.pop('file_uploaded', None)


        # Redirect to the upload page
        return redirect(url_for('home.home'))
    except Exception as e:
        flash(f"An error occurred while resetting: {e}")
        return redirect(url_for('home.home'))
