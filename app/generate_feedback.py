from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus.flowables import HRFlowable
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
import os
from datetime import datetime
from reportlab.lib.colors import red, green

def generate_feedback_pdf(assignment_name, feedback_data, folder_path, unused_vars, unused_methods, match_ifs, missing_brackets, unused_array):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)


    pdf_file_path = os.path.join(folder_path, 'automatic__feedback.pdf')
    doc = SimpleDocTemplate(pdf_file_path, pagesize=A4)
    styles = getSampleStyleSheet()
    emoji_style = ParagraphStyle(
        'EmojiStyle',
        fontName='Helvetica',
        fontSize=12,
        leading=14
    )
    elements = []

    title = f"Automatic Personnalized Feedback: {assignment_name}"
    elements.append(Paragraph(title, styles['Title']))
    elements.append(Spacer(1, 12))

    date_str = f"Date: {datetime.now().strftime('%Y-%m-%d')}"
    elements.append(Paragraph(date_str, styles['Normal']))
    elements.append(Spacer(1, 12))


    
    feedback_lines = feedback_data.split('\n')
    for line in feedback_lines:
            elements.append(Paragraph(line, styles['Normal']))
            elements.append(Spacer(1, 12))
            

    elements.append(HRFlowable(width="100%", thickness=1, color="black"))

   


    #----------------------- Unused Variables Feedback ----------------------------------------#
    # Unused Variables Section
    elements.append(Paragraph("Unused Variables: ", styles['Heading3']))
    elements.append(Spacer(1, 12))
    if unused_vars:
        for line in unused_vars:
            elements.append(Paragraph(f"⚠ You have an unused variable at line {line-2} in your code.", emoji_style))
        
    else:
        elements.append(Paragraph("<font color='green'>✓</font> No unused variables in your code. Well done!", emoji_style))
    elements.append(Spacer(1, 12))
    elements.append(HRFlowable(width="100%", thickness=1, color="black"))

    #------------------------------------------------------------------------------------------#



    #----------------------- Unused Method Feedback ----------------------------------------#
    # Unused Variables Section
    elements.append(Paragraph("Unused function: ", styles['Heading3']))
    elements.append(Spacer(1, 12))
    if unused_methods:
        for line in unused_methods:
            elements.append(Paragraph(f"⚠ You have an unused function at line {line-2} in your code.", emoji_style))
        
    else:
        elements.append(Paragraph("✓ No unused function in your code. Well done!", emoji_style))
    elements.append(Spacer(1, 12))
    elements.append(HRFlowable(width="100%", thickness=1, color="black"))
    #------------------------------------------------------------------------------------------#

  


    #----------------------- match vs if/else Feedback ----------------------------------------#
    # Unused Variables Section
    elements.append(Paragraph("Match case vs If/else statement: ", styles['Heading3']))
    elements.append(Spacer(1, 12))
    if match_ifs:
        for line in match_ifs:
            elements.append(Paragraph(f"⚠ You could replace your succession of if/else statement by a match statement at line {line-2} in your code.", emoji_style))
        
    else:
        elements.append(Paragraph("✓ Well done! Nothing to report.", emoji_style))
    elements.append(Spacer(1, 12))
    elements.append(HRFlowable(width="100%", thickness=1, color="black"))
    #------------------------------------------------------------------------------------------#

    

    #----------------------- Missing brackets Feedback ----------------------------------------#
    # Unused Variables Section
    elements.append(Paragraph("Missing Brackets: ", styles['Heading3']))
    elements.append(Spacer(1, 12))
    if missing_brackets:
        for line in missing_brackets:
            elements.append(Paragraph(f"⚠ You open brackets at {line-2} in your code but you dont close them. Make sure to close brackets so that your code can run!", emoji_style))
        
    else:
        elements.append(Paragraph("✓ No missing brackets in your code. Well done!", emoji_style))
    elements.append(Spacer(1, 12))
    elements.append(HRFlowable(width="100%", thickness=1, color="black"))
    #------------------------------------------------------------------------------------------#
  

    #   #----------------------- Unused Variables Feedback ----------------------------------------#
    # # Unused Variables Section
    # elements.append(Paragraph("Array: ", styles['Heading3']))
    # elements.append(Spacer(1, 12))
    # if unused_vars:
    #     for line in unused_vars:
    #         elements.append(Paragraph(f"⚠ You have an unused/ poorly defined array at line {line} in your code.", emoji_style))
        
    # else:
    #     elements.append(Paragraph("<font color='green'>✓</font> No array error. Well done!", emoji_style))
    # elements.append(Spacer(1, 12))
    # elements.append(HRFlowable(width="100%", thickness=1, color="black"))

    # Build the PDF
    doc.build(elements)

    return pdf_file_path
