import os
import re
import pymysql
import json
from f_perso_feedback import get_feedback
from f_var_val import check_var
from f_brackets import check_missing_brackets
from f_if_else_match import check_if_replacement
from f_unused_method import check_unused_methods_scala
from f_array import find_array_issues
import json


db_config = {
    'host': 'localhost',
    'user': 'root',
    'db': 'projet_b'
}

extracted_dir = 'extracted'

def count_occurrences(content, pattern):
    try:
        return len(re.findall(pattern, content))
    except re.error as e:
        return 0

def extract_statistics(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
        
        num_lines = content.count('\n') + 1
        num_while_loops = count_occurrences(content, r'\bwhile\b')
        num_for_loops = count_occurrences(content, r'\bfor\b')
        num_if_statements = count_occurrences(content, r'\bif\b')
        num_vars = count_occurrences(content, r'\b(val|var)\b')
        num_methods = count_occurrences(content, r'\bdef\b')
        num_classes = count_occurrences(content, r'\bclass\b')
        
        cyclomatic_complexity = num_if_statements + num_for_loops + num_while_loops
        
        return {
            'num_lines': num_lines,
            'num_vars': num_vars,
            'num_methods': num_methods,
            'num_if_else': num_if_statements,
            'num_loops': num_while_loops + num_for_loops,
            'num_classes': num_classes,
            'cyclomatic_complexity': cyclomatic_complexity,
        }

def analyze_code(content):  

    analysis = {
        'too_many_vars': False,
        'unused_functions': False,
        'too_many_ifs_else': False,
        'missing_comments': False,
        'missing_brackets': False,
        'infinite_loops': False,
        'high_cyclomatic_complexity': False,
        'unused_var': [],
        'array_error':[]
    }
    
    num_vars = count_occurrences(content, r'\b(val|var)\b')
    num_methods = count_occurrences(content, r'\bdef\b')
    num_ifs = count_occurrences(content, r'\bif\b')
    num_elses = count_occurrences(content, r'\belse\b')
    num_comments = count_occurrences(content, r'//|/*|*/')
    num_brackets = count_occurrences(content, r'{}')
    num_loops = count_occurrences(content, r'\bwhile\b|\bfor\b')
    
    
    unused_vars_lines = check_var(content)
    analysis['unused_var'] = unused_vars_lines

    unused_methods_lines = check_unused_methods_scala(content)
    analysis['unused_method'] = unused_methods_lines

    missing_brackets_lines = check_missing_brackets(content)
    analysis['missing_bracket'] = missing_brackets_lines

    match_if_lines = check_if_replacement(content)
    analysis['match_if'] = match_if_lines

    error_array = find_array_issues(content)
    analysis['array_error'] = error_array
    return analysis




def add_data_to_db(conn, cursor):
    def count_occurrences(content, pattern):
        try:
            return len(re.findall(pattern, content))
        except re.error as e:
            return 0

    for root, dirs, files in os.walk(extracted_dir):        
        for file_name in files:
            
            
            if file_name.endswith('.scala'):
                scala_file_path = os.path.join(root, file_name)
                
                try:        
                            folder_name = os.path.basename(root)
                            with open(scala_file_path, 'r', encoding='utf-8') as file:
                                content = file.read()
                                content = f"//Assignment: {folder_name}\n\n{content}"
                            num_lines = content.count('\n') + 1
                            num_ifs = count_occurrences(content, r'\bif\b')
                            num_methods = count_occurrences(content, r'\bdef\b')
                            num_loops = count_occurrences(content, r'\b(for|while)\b')
                            num_var = count_occurrences(content, r'\b(val|var)\b')

                            analysis = analyze_code(content)

                            

                            cursor.execute("SELECT * FROM assignement WHERE Name = %s", (folder_name,))
                            result = cursor.fetchone()
                            
                            if result:
                                print(f"Entry for {folder_name} already exists. Skipping...")
                                continue

                            unused_vars_json = json.dumps(analysis['unused_var'])
                            unused_methods_json = json.dumps(analysis['unused_method'])
                            missing_brackets_json = json.dumps(analysis['missing_bracket'])
                            match_ifs_json = json.dumps(analysis['match_if'])
                            error_array_json = json.dumps(analysis['array_error'])
                            # Insert data into the database
                            cursor.execute('''
                                INSERT INTO assignement (Name, Content, Num_Lines, Num_Ifs, Num_Methods, Num_Loops, Num_Var, unused_variables, unused_methods, match_if_else, missing_brackets, array_error)
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                            ''', (folder_name, content, num_lines, num_ifs, num_methods, num_loops, num_var, unused_vars_json, unused_methods_json, match_ifs_json, missing_brackets_json, error_array_json))
                            conn.commit()

                except Exception as e:
                            print(f"Error reading file: {scala_file_path}: {e}")
                            print(files)
                            try:
                                os.remove(scala_file_path)
                                print(f"Removed problematic file: {scala_file_path}")
                            except Exception as delete_exception:
                                print(f"Error removing file: {scala_file_path}: {delete_exception}")

    try:
        conn.commit()
        print("Data committed to the database")
    except pymysql.MySQLError as e:
        print(f"Error committing data to the database: {e}")
        conn.rollback()
    finally:
        cursor.close()
        conn.close()
        print("Resources closed successfully")