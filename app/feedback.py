from flask import Blueprint, render_template, redirect, url_for, flash, request
import pymysql
import os

feedback_bp = Blueprint('feedback', __name__)
from .generate_feedback import generate_feedback_pdf
from download import download_zip_of_directory
from f_perso_feedback import get_feedback
# Database configuration
db_config = {
    'host': 'localhost',
    'user': 'root',
    'db': 'projet_b',
    'charset': 'utf8mb4',
    'cursorclass': pymysql.cursors.DictCursor
}

# Predefined error types
error_types = [
    "Val vs Var",
    "How to use functions",
    "If/Else vs Match",
    "Why comment the code",
    "Loops vs Higher-order Functions"
]

@feedback_bp.route('/', methods=['GET', 'POST'])
def feedback():
    conn = None
    cursor = None

    try:
        # Establish database connection
        conn = pymysql.connect(**db_config)
        cursor = conn.cursor()

        # Insert error types into the error_resources table if they don't already exist
        for error in error_types:
            cursor.execute(
                "INSERT IGNORE INTO resources (error_type) VALUES (%s)", (error,)
            )
        conn.commit()

        if request.method == 'POST':
            # Determine which form was submitted
            if 'save_links' in request.form:
                # Update the resource links in the database
                for error_id, resource_link in request.form.items():
                    if error_id != 'save_links':
                        cursor.execute(
                            "UPDATE resources SET resource_link = %s WHERE id = %s",
                            (resource_link, error_id)
                        )
                conn.commit()
                flash('Resource links updated successfully!', 'success')

            elif 'generate_feedback' in request.form:
                
                # Fetch all assignments with feedback from the database
                cursor.execute("SELECT Name, Feedback, unused_variables, unused_methods, match_if_else, missing_brackets, array_error FROM assignement")
                assignments = cursor.fetchall()
                
                for assignment in assignments:
                    get_feedback(assignment['Name']) 
                extracted_base_path = 'extracted'
                
                for root, dirs, files in os.walk(extracted_base_path):

                    for folder_name in dirs:
                        for assignment in assignments:
                            assignment_name = assignment['Name']
                            feedback = assignment['Feedback'] 
                            unused_vars_str = assignment['unused_variables'] 
                            unused_vars = [int(line.strip()) for line in unused_vars_str.strip('[]').split(',') if line.strip()] if unused_vars_str else []

                            unused_array_str = assignment['array_error']
                            unused_array = [int(line.strip()) for line in unused_array_str.strip('[]').split(',') if line.strip()] if unused_array_str else []

                            unused_methods_str = assignment['unused_methods']  
                            unused_methods = [int(line.strip()) for line in unused_methods_str.strip('[]').split(',') if line.strip()] if unused_methods_str else []

                            match_ifs_str = assignment['match_if_else']  
                            match_ifs = [int(line.strip()) for line in match_ifs_str.strip('[]').split(',') if line.strip()] if match_ifs_str else []

                            missing_brackets_str = assignment['missing_brackets'] 
                            missing_brackets = [int(line.strip()) for line in missing_brackets_str.strip('[]').split(',') if line.strip()] if missing_brackets_str else []

                            if folder_name == assignment_name:
                                folder_path = os.path.join(root, folder_name)
                                feedback_file_path = os.path.join(folder_path, 'feedback.pdf')

                                feedback_end = generate_feedback_pdf(assignment_name, feedback, folder_path, unused_vars, unused_methods, match_ifs, missing_brackets, unused_array)

                flash("Feedback files generated successfully!", "success")

            elif 'download_zip' in request.form:
                flash("Zip file downloading!", "success")
                directory_path = 'extracted'
                return download_zip_of_directory(directory_path)
                

    except pymysql.MySQLError as e:
        print(f"Error connecting to MySQL: {e}")
        flash('An error occurred while processing your request.', 'error')
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()

    # Fetch error types and resource links for display
    try:
        conn = pymysql.connect(**db_config)
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM resources")
        error_resources = cursor.fetchall()
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()

    return render_template('feedback.html', error_resources=error_resources)
